.class public abstract Lcom/android/settings/MiuiRestrictedSettingsFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiRestrictedSettingsFragment.java"


# instance fields
.field private cff:Landroid/view/View;

.field private cfg:Z

.field private cfh:Z

.field private cfi:Landroid/widget/TextView;

.field private cfj:Lcom/android/settingslib/n;

.field private cfk:Z

.field private cfl:Z

.field private final cfm:Ljava/lang/String;

.field private cfn:Landroid/content/RestrictionsManager;

.field private cfo:Landroid/content/BroadcastReceiver;

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfl:Z

    new-instance v0, Lcom/android/settings/ld;

    invoke-direct {v0, p0}, Lcom/android/settings/ld;-><init>(Lcom/android/settings/MiuiRestrictedSettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfo:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfm:Ljava/lang/String;

    return-void
.end method

.method private bXF()V
    .locals 5

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfh:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfn:Landroid/content/RestrictionsManager;

    invoke-virtual {v0}, Landroid/content/RestrictionsManager;->hasRestrictionsProvider()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfn:Landroid/content/RestrictionsManager;

    invoke-virtual {v0}, Landroid/content/RestrictionsManager;->createLocalApprovalIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfh:Z

    new-instance v1, Landroid/os/PersistableBundle;

    invoke-direct {v1}, Landroid/os/PersistableBundle;-><init>()V

    const-string/jumbo v2, "android.request.mesg"

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f120e2b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "android.content.extra.REQUEST_BUNDLE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/16 v1, 0x3015

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method private bXH()Landroid/view/View;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a0043

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bXL(Lcom/android/settings/MiuiRestrictedSettingsFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    return v0
.end method

.method static synthetic bXM(Lcom/android/settings/MiuiRestrictedSettingsFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    return p1
.end method

.method static synthetic bXN(Lcom/android/settings/MiuiRestrictedSettingsFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfh:Z

    return p1
.end method


# virtual methods
.method public bDe()Lcom/android/settingslib/n;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfm:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfj:Lcom/android/settingslib/n;

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfj:Lcom/android/settingslib/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfj:Lcom/android/settingslib/n;

    iget v0, v0, Lcom/android/settingslib/n;->userId:I

    const/16 v1, -0x2710

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfj:Lcom/android/settingslib/n;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    iput v1, v0, Lcom/android/settingslib/n;->userId:I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfj:Lcom/android/settingslib/n;

    return-object v0
.end method

.method protected bWZ()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bWY()V

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cff:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bXC()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bDe()Lcom/android/settingslib/n;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cff:Landroid/view/View;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bXZ(Landroid/app/Activity;Landroid/view/View;Lcom/android/settingslib/n;Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cff:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bWE(Landroid/view/View;)V

    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWZ()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfi:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfi:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bWE(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected bXB()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bXJ()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bXG()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfk:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfl:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bXC()Z
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bXB()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->mUserManager:Landroid/os/UserManager;

    iget-object v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfm:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v2}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/UserManager;->hasBaseUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfk:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfl:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bXD()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfi:Landroid/widget/TextView;

    return-object v0
.end method

.method public bXE(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfl:Z

    return-void
.end method

.method protected bXG()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfh:Z

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected bXI()Landroid/widget/TextView;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method protected bXJ()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfm:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "restrict_if_overridable"

    iget-object v2, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->mUserManager:Landroid/os/UserManager;

    iget-object v2, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfn:Landroid/content/RestrictionsManager;

    invoke-virtual {v0}, Landroid/content/RestrictionsManager;->hasRestrictionsProvider()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :cond_2
    return v0
.end method

.method protected bXK(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string/jumbo v1, "restrict_if_overridable"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->mUserManager:Landroid/os/UserManager;

    iget-object v2, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v1

    :goto_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfn:Landroid/content/RestrictionsManager;

    invoke-virtual {v0}, Landroid/content/RestrictionsManager;->hasRestrictionsProvider()Z

    move-result v0

    :cond_1
    return v0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bXH()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cff:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bXI()Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfi:Landroid/widget/TextView;

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x3015

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfh:Z

    iput-boolean v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfh:Z

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "restrictions"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/RestrictionsManager;

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfn:Landroid/content/RestrictionsManager;

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->mUserManager:Landroid/os/UserManager;

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfk:Z

    if-eqz p1, :cond_0

    const-string/jumbo v0, "chsc"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfh:Z

    const-string/jumbo v0, "chrq"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfo:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfo:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfm:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bXK(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->bXF()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "chrq"

    iget-boolean v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfg:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "chsc"

    iget-boolean v1, p0, Lcom/android/settings/MiuiRestrictedSettingsFragment;->cfh:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method
