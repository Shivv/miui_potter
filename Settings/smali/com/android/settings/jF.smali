.class final Lcom/android/settings/jF;
.super Ljava/lang/Object;
.source "GxzwNewFingerprintFragment.java"

# interfaces
.implements Lcom/android/settings/bx;


# instance fields
.field private final cpU:Ljava/lang/Runnable;

.field private cpV:Ljava/lang/Runnable;

.field final synthetic cpW:Lcom/android/settings/GxzwNewFingerprintFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/GxzwNewFingerprintFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/jG;

    invoke-direct {v0, p0}, Lcom/android/settings/jG;-><init>(Lcom/android/settings/jF;)V

    iput-object v0, p0, Lcom/android/settings/jF;->cpV:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/settings/jH;

    invoke-direct {v0, p0}, Lcom/android/settings/jH;-><init>(Lcom/android/settings/jF;)V

    iput-object v0, p0, Lcom/android/settings/jF;->cpU:Ljava/lang/Runnable;

    return-void
.end method

.method private cax()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/android/settings/jF;->cay()V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v0

    const/16 v1, 0xc

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v3, "core_scan_output_%02d"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v5}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bON(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOx(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/MutedVideoView;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOP(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/net/Uri;Lcom/android/settings/MutedVideoView;)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v3, "core_scan_output_%02d_error"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v5}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bON(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOH(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/net/Uri;)Landroid/net/Uri;

    :goto_0
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOo(Lcom/android/settings/GxzwNewFingerprintFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOn(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207de

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOm(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOl(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207dc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOl(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOS(Lcom/android/settings/GxzwNewFingerprintFragment;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v3, "edge_scan_output_%02d"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v5}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v5

    add-int/lit8 v5, v5, -0xc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bON(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOx(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/MutedVideoView;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOP(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/net/Uri;Lcom/android/settings/MutedVideoView;)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v3, "edge_scan_output_%02d_error"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v5}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v5

    add-int/lit8 v5, v5, -0xc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bON(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOH(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/net/Uri;)Landroid/net/Uri;

    goto/16 :goto_0
.end method

.method private cay()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOp(Lcom/android/settings/GxzwNewFingerprintFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOv(Lcom/android/settings/GxzwNewFingerprintFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOx(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/MutedVideoView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/settings/MutedVideoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOn(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207e0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOm(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207df

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOK(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public bKv()V
    .locals 3

    const/16 v1, 0xc

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOw(Lcom/android/settings/GxzwNewFingerprintFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOK(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOx(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/MutedVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v0

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOq(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v0

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOx(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/MutedVideoView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/settings/MutedVideoView;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOc(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/jF;->cpU:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOF(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOL(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOT(Lcom/android/settings/GxzwNewFingerprintFragment;Z)V

    return-void
.end method

.method public bKw()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOc(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOc(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1200a6

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOT(Lcom/android/settings/GxzwNewFingerprintFragment;Z)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-virtual {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->finish()V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOF(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z

    return-void
.end method

.method public bKx(I)V
    .locals 7

    const/16 v5, 0xc

    const/4 v4, 0x1

    const/4 v6, 0x0

    const-class v0, Lcom/android/settings/NewFingerprintInternalActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOu(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOw(Lcom/android/settings/GxzwNewFingerprintFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOh(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/jF;->cpV:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/android/settings/jF;->cay()V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOp(Lcom/android/settings/GxzwNewFingerprintFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v4}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOF(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOM(Lcom/android/settings/GxzwNewFingerprintFragment;I)I

    :cond_1
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOu(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v0

    if-ne p1, v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/jF;->cax()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v6}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOE(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOn(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207e0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOy(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v1

    sub-int/2addr v1, p1

    mul-int/lit8 v1, v1, 0x14

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOy(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v2

    div-int/2addr v1, v2

    int-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOD(Lcom/android/settings/GxzwNewFingerprintFragment;I)I

    const-class v0, Lcom/android/settings/NewFingerprintInternalActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOy(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOy(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v1

    sub-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x14

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOy(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v2

    div-int/2addr v1, v2

    int-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOG(Lcom/android/settings/GxzwNewFingerprintFragment;I)I

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v0

    if-gt v0, v5, :cond_4

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOq(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v0

    if-le v0, v5, :cond_3

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v4}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOK(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z

    :cond_3
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v3, "core_scan_output_%02d"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v5}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bON(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOx(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/MutedVideoView;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOP(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/net/Uri;Lcom/android/settings/MutedVideoView;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, p1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOJ(Lcom/android/settings/GxzwNewFingerprintFragment;I)I

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOq(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOD(Lcom/android/settings/GxzwNewFingerprintFragment;I)I

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0, v6}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOA(Lcom/android/settings/GxzwNewFingerprintFragment;I)I

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOt(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOO(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/app/AlertDialog;)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOI(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOm(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207df

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOl(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v3, "edge_scan_output_%02d"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v5}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I

    move-result v5

    add-int/lit8 v5, v5, -0xc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bON(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOx(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/MutedVideoView;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOP(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/net/Uri;Lcom/android/settings/MutedVideoView;)V

    goto :goto_0
.end method

.method public onEnrollmentHelp(ILjava/lang/CharSequence;)V
    .locals 4

    const/16 v0, 0x401

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOn(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207de

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOm(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOl(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1207dc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOl(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOS(Lcom/android/settings/GxzwNewFingerprintFragment;)V

    return-void

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOz(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Ljava/lang/String;

    const-class v0, Lcom/android/settings/NewFingerprintInternalActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "helpMsgId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; helpString="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOh(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/jF;->cpV:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/jF;->cpW:Lcom/android/settings/GxzwNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOl(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
