.class Lcom/android/settings/p;
.super Ljava/lang/Object;
.source "TrustedCredentialsSettings.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final bvb:Lcom/android/settings/k;

.field private final bvc:Ljava/lang/String;

.field private bvd:Z

.field public bve:I

.field private final bvf:Landroid/security/IKeyChainService;

.field private final bvg:Landroid/net/http/SslCertificate;

.field private final bvh:Ljava/lang/String;

.field private final bvi:Ljava/lang/String;

.field private final bvj:Lcom/android/settings/TrustedCredentialsSettings$Tab;

.field private final bvk:Ljava/security/cert/X509Certificate;


# direct methods
.method private constructor <init>(Landroid/security/IKeyChainService;Lcom/android/settings/k;Lcom/android/settings/TrustedCredentialsSettings$Tab;Ljava/lang/String;Ljava/security/cert/X509Certificate;I)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p6, p0, Lcom/android/settings/p;->bve:I

    iput-object p1, p0, Lcom/android/settings/p;->bvf:Landroid/security/IKeyChainService;

    iput-object p2, p0, Lcom/android/settings/p;->bvb:Lcom/android/settings/k;

    iput-object p3, p0, Lcom/android/settings/p;->bvj:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    iput-object p4, p0, Lcom/android/settings/p;->bvc:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/settings/p;->bvk:Ljava/security/cert/X509Certificate;

    new-instance v0, Landroid/net/http/SslCertificate;

    invoke-direct {v0, p5}, Landroid/net/http/SslCertificate;-><init>(Ljava/security/cert/X509Certificate;)V

    iput-object v0, p0, Lcom/android/settings/p;->bvg:Landroid/net/http/SslCertificate;

    iget-object v0, p0, Lcom/android/settings/p;->bvg:Landroid/net/http/SslCertificate;

    invoke-virtual {v0}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/p;->bvg:Landroid/net/http/SslCertificate;

    invoke-virtual {v1}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getOName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/p;->bvg:Landroid/net/http/SslCertificate;

    invoke-virtual {v2}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/http/SslCertificate$DName;->getUName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iput-object v1, p0, Lcom/android/settings/p;->bvh:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/p;->bvi:Ljava/lang/String;

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/p;->bvj:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    iget-object v1, p0, Lcom/android/settings/p;->bvf:Landroid/security/IKeyChainService;

    iget-object v2, p0, Lcom/android/settings/p;->bvc:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhq(Lcom/android/settings/TrustedCredentialsSettings$Tab;Landroid/security/IKeyChainService;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/p;->bvd:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    iput-object v1, p0, Lcom/android/settings/p;->bvh:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/settings/p;->bvi:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iput-object v0, p0, Lcom/android/settings/p;->bvh:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/p;->bvi:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/p;->bvg:Landroid/net/http/SslCertificate;

    invoke-virtual {v0}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/http/SslCertificate$DName;->getDName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/p;->bvh:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/p;->bvi:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "TrustedCredentialsSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Remote exception while checking if alias "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/p;->bvc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is deleted."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/p;->bvd:Z

    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/security/IKeyChainService;Lcom/android/settings/k;Lcom/android/settings/TrustedCredentialsSettings$Tab;Ljava/lang/String;Ljava/security/cert/X509Certificate;ILcom/android/settings/p;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/settings/p;-><init>(Landroid/security/IKeyChainService;Lcom/android/settings/k;Lcom/android/settings/TrustedCredentialsSettings$Tab;Ljava/lang/String;Ljava/security/cert/X509Certificate;I)V

    return-void
.end method

.method static synthetic bid(Lcom/android/settings/p;)Lcom/android/settings/k;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/p;->bvb:Lcom/android/settings/k;

    return-object v0
.end method

.method static synthetic bie(Lcom/android/settings/p;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/p;->bvc:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bif(Lcom/android/settings/p;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/p;->bvd:Z

    return v0
.end method

.method static synthetic big(Lcom/android/settings/p;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/p;->bvh:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bih(Lcom/android/settings/p;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/p;->bvi:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bii(Lcom/android/settings/p;)Lcom/android/settings/TrustedCredentialsSettings$Tab;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/p;->bvj:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    return-object v0
.end method

.method static synthetic bij(Lcom/android/settings/p;)Ljava/security/cert/X509Certificate;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/p;->bvk:Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method static synthetic bik(Lcom/android/settings/p;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/p;->bvd:Z

    return p1
.end method


# virtual methods
.method public bhY(Lcom/android/settings/p;)I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/p;->bvh:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/p;->bvh:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/p;->bvi:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/p;->bvi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bhZ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/p;->bvc:Ljava/lang/String;

    return-object v0
.end method

.method public bia()I
    .locals 1

    iget v0, p0, Lcom/android/settings/p;->bve:I

    return v0
.end method

.method public bib()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/p;->bvd:Z

    return v0
.end method

.method public bic()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/p;->bvj:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    sget-object v1, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bux:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/p;

    invoke-virtual {p0, p1}, Lcom/android/settings/p;->bhY(Lcom/android/settings/p;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/android/settings/p;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    check-cast p1, Lcom/android/settings/p;

    iget-object v0, p0, Lcom/android/settings/p;->bvc:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/p;->bvc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/p;->bvc:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
