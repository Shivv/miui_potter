.class final Lcom/android/settings/dX;
.super Landroid/os/AsyncTask;
.source "ChooseLockPattern.java"


# instance fields
.field final synthetic chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

.field final synthetic chX:Lcom/android/internal/widget/LockPatternUtils;

.field final synthetic chY:Z

.field final synthetic chZ:Z


# direct methods
.method constructor <init>(Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;Lcom/android/internal/widget/LockPatternUtils;ZZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    iput-object p2, p0, Lcom/android/settings/dX;->chX:Lcom/android/internal/widget/LockPatternUtils;

    iput-boolean p3, p0, Lcom/android/settings/dX;->chY:Z

    iput-boolean p4, p0, Lcom/android/settings/dX;->chZ:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bZM([B)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bkA(Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bkA(Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-static {v0, v1}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bkF(Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-virtual {v0}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-virtual {v0}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aH;->bva(Landroid/content/Context;)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/dX;->chZ:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-virtual {v0, p1}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bkt([B)V

    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/android/settings/ChooseLockPattern;->-get0()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-virtual {v0}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-virtual {v0}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-static {v0, p1}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bkH(Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;[B)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/dX;->doInBackground([Ljava/lang/Void;)[B

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[B
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-virtual {v2}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-virtual {v2}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-static {v3}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bkx(Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;)Lcom/android/settings/bM;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/bn;->bFm(Lcom/android/settings/bM;)J

    move-result-wide v4

    if-eqz v2, :cond_0

    const-string/jumbo v3, "has_challenge"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/dX;->chX:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v3, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    iget-object v3, v3, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bwq:Ljava/util/List;

    iget-object v6, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-static {v6}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bkE(Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;)I

    move-result v6

    iget-boolean v7, p0, Lcom/android/settings/dX;->chY:Z

    invoke-static {v2, v3, v6, v7}, Lcom/android/settings/bn;->bFn(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;IZ)V

    iget-boolean v2, p0, Lcom/android/settings/dX;->chZ:Z

    if-nez v2, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/dX;->chX:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    iget-object v2, v2, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bwq:Ljava/util/List;

    iget-object v3, p0, Lcom/android/settings/dX;->chW:Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;

    invoke-static {v3}, Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;->bkE(Lcom/android/settings/ChooseLockPattern$ChooseLockPatternFragment;)I

    move-result v3

    invoke-static {v0, v2, v4, v5, v3}, Lcom/android/settings/bn;->bFo(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;JI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "ChooseLockPattern"

    const-string/jumbo v2, "critical: no token returned for known good pattern"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/android/settings/dX;->bZM([B)V

    return-void
.end method
