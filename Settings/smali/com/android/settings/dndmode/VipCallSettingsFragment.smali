.class public Lcom/android/settings/dndmode/VipCallSettingsFragment;
.super Lcom/android/settings/dndmode/MiuiPreferenceFragment;
.source "VipCallSettingsFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# static fields
.field private static final lM:Ljava/util/HashMap;


# instance fields
.field private lL:Landroid/service/notification/ZenModeConfig;

.field private lN:Landroid/preference/PreferenceCategory;

.field lO:Ljava/util/concurrent/Future;

.field private lP:Landroid/view/MenuItem;

.field private lQ:Lmiui/preference/RadioButtonPreferenceCategory;

.field lR:Ljava/util/concurrent/ExecutorService;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lM:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lR:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private lk()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lN:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lP:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lP:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method private ll()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.contacts.action.GET_MULTIPLE_PHONES"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "vnd.android.cursor.dir/phone_v2"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.include_unknown_numbers"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private lm()V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lL:Landroid/service/notification/ZenModeConfig;

    iget v1, v1, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lk()V

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f03006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/dndmode/i;

    invoke-direct {v2, p0, v1}, Lcom/android/settings/dndmode/i;-><init>(Lcom/android/settings/dndmode/VipCallSettingsFragment;[Ljava/lang/String;)V

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_1

    new-instance v3, Lmiui/preference/RadioButtonPreference;

    iget-object v4, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Lmiui/preference/RadioButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v2}, Lmiui/preference/RadioButtonPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v4, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lQ:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v4, v3}, Lmiui/preference/RadioButtonPreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lq()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lQ:Lmiui/preference/RadioButtonPreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lL:Landroid/service/notification/ZenModeConfig;

    iget v1, v1, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    invoke-virtual {v0, v1}, Lmiui/preference/RadioButtonPreferenceCategory;->setCheckedPosition(I)V

    return-void
.end method

.method private ln(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/android/settings/dndmode/k;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/dndmode/k;-><init>(Lcom/android/settings/dndmode/VipCallSettingsFragment;Ljava/lang/String;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/dndmode/k;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private lp()V
    .locals 2

    new-instance v0, Lcom/android/settings/dndmode/h;

    invoke-direct {v0, p0}, Lcom/android/settings/dndmode/h;-><init>(Lcom/android/settings/dndmode/VipCallSettingsFragment;)V

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lR:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lO:Ljava/util/concurrent/Future;

    return-void
.end method

.method private lq()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lN:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lP:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lP:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method private lr(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f12060e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/dndmode/j;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/dndmode/j;-><init>(Lcom/android/settings/dndmode/VipCallSettingsFragment;Ljava/lang/String;)V

    const v2, 0x7f12060d

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic ls(Lcom/android/settings/dndmode/VipCallSettingsFragment;)Landroid/service/notification/ZenModeConfig;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lL:Landroid/service/notification/ZenModeConfig;

    return-object v0
.end method

.method static synthetic lt()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lM:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic lu(Lcom/android/settings/dndmode/VipCallSettingsFragment;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic lv(Lcom/android/settings/dndmode/VipCallSettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lk()V

    return-void
.end method

.method static synthetic lw(Lcom/android/settings/dndmode/VipCallSettingsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->ln(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lx(Lcom/android/settings/dndmode/VipCallSettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lq()V

    return-void
.end method

.method static synthetic ly(Lcom/android/settings/dndmode/VipCallSettingsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lr(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public lo(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lN:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/dndmode/b;

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dndmode/b;-><init>(Lcom/android/settings/dndmode/VipCallSettingsFragment;Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lcom/android/settings/dndmode/b;->lz(Landroid/database/Cursor;)V

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lN:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    if-nez p3, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "com.android.contacts.extra.PHONE_URIS"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_2

    array-length v1, v0

    if-nez v1, :cond_3

    :cond_2
    return-void

    :cond_3
    new-instance v1, Lcom/android/settings/dndmode/l;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/dndmode/l;-><init>(Lcom/android/settings/dndmode/VipCallSettingsFragment;[Landroid/os/Parcelable;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/dndmode/l;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15004d

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lL:Landroid/service/notification/ZenModeConfig;

    const-string/jumbo v0, "key_vip_options_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lQ:Lmiui/preference/RadioButtonPreferenceCategory;

    const-string/jumbo v0, "key_vip_list_custom_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lN:Landroid/preference/PreferenceCategory;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->setHasOptionsMenu(Z)V

    invoke-direct {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lm()V

    invoke-direct {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lp()V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 9

    const/4 v5, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->mContext:Landroid/content/Context;

    sget-object v2, Lmiui/provider/ExtraTelephony$Phonelist;->CONTENT_URI:Landroid/net/Uri;

    new-array v3, v5, [Ljava/lang/String;

    const-string/jumbo v4, "_id"

    aput-object v4, v3, v8

    const-string/jumbo v4, "number"

    aput-object v4, v3, v7

    const-string/jumbo v4, "type = ? and sync_dirty <> ?"

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v6, "3"

    aput-object v6, v5, v8

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0x7f12060f

    invoke-interface {p1, v1, v1, v1, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lP:Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lP:Landroid/view/MenuItem;

    const v3, 0x7f08010b

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setShowAsAction(I)V

    iget-object v2, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lP:Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lL:Landroid/service/notification/ZenModeConfig;

    iget v3, v3, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    :goto_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lo(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lN:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->ll()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onResume()V

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onStop()V

    iget-object v0, p0, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lO:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    return-void
.end method
