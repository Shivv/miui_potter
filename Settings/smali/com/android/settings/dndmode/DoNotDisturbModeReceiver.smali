.class public Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DoNotDisturbModeReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private kY(Landroid/content/Context;)J
    .locals 2

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->getEndTimeForQuietMode(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Lcom/android/settings/dndmode/a;->le(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private kZ(Landroid/content/Context;)J
    .locals 2

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->getStartTimeForQuietMode(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Lcom/android/settings/dndmode/a;->le(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private la(Landroid/content/Context;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->getQuietRepeatType(Landroid/content/Context;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Lcom/android/settings/dndmode/e;

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->getQuietRepeatType(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/android/settings/dndmode/e;-><init>(I)V

    invoke-virtual {v0}, Lcom/android/settings/dndmode/e;->mh()Z

    move-result v0

    if-nez v0, :cond_0

    return v4

    :sswitch_0
    return v5

    :sswitch_1
    invoke-static {v0}, Lcom/android/settings/dndmode/c;->lT(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v4

    :cond_0
    return v5

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_1
        0x7f -> :sswitch_0
    .end sparse-switch
.end method

.method private lb(Landroid/content/Context;)Z
    .locals 4

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->getNextAutoStartTime(Landroid/content/Context;)J

    move-result-wide v0

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->la(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_1
    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->isAutoTimerOfQuietModeEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/android/settings/dndmode/a;->lc(Landroid/content/Context;)V

    const-string/jumbo v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/android/settings/dndmode/a;->ld(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->getNextAutoStartTime(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-lez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->la(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {p1, v7, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setQuietMode(Landroid/content/Context;ZI)V

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->kZ(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Landroid/provider/MiuiSettings$AntiSpam;->setNextAutoStartTime(Landroid/content/Context;J)V

    :cond_2
    :goto_0
    const-string/jumbo v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-static {p1, v8, v9}, Landroid/provider/MiuiSettings$AntiSpam;->setNextAutoStartTime(Landroid/content/Context;J)V

    invoke-static {p1, v8, v9}, Landroid/provider/MiuiSettings$AntiSpam;->setNextAutoEndTime(Landroid/content/Context;J)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->getNextAutoEndTime(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-lez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->lb(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {p1, v6, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setQuietMode(Landroid/content/Context;ZI)V

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->kY(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Landroid/provider/MiuiSettings$AntiSpam;->setNextAutoEndTime(Landroid/content/Context;J)V

    goto :goto_0

    :cond_6
    const-string/jumbo v1, "com.android.settings.dndm.AUTO_TIME_TURN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->isAutoTimerOfQuietModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/android/settings/dndmode/a;->ld(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->la(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p1, v7, v0}, Landroid/provider/MiuiSettings$AntiSpam;->setQuietMode(Landroid/content/Context;ZI)V

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->kZ(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setNextAutoStartTime(Landroid/content/Context;J)V

    goto :goto_1

    :cond_7
    const-string/jumbo v1, "com.android.settings.dndm.AUTO_TIME_TURN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->isAutoTimerOfQuietModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/android/settings/dndmode/a;->ld(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->lb(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p1, v6, v0}, Landroid/provider/MiuiSettings$AntiSpam;->setQuietMode(Landroid/content/Context;ZI)V

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/DoNotDisturbModeReceiver;->kY(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setNextAutoEndTime(Landroid/content/Context;J)V

    :cond_8
    invoke-static {p1}, Lcom/android/settings/dndmode/a;->lc(Landroid/content/Context;)V

    goto :goto_1
.end method
