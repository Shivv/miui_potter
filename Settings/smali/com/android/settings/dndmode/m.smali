.class final Lcom/android/settings/dndmode/m;
.super Ljava/lang/Object;
.source "AutoTimeSettingsFragment.java"

# interfaces
.implements Lmiui/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field final synthetic nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Lmiui/widget/TimePicker;II)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lL(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    mul-int/lit8 v1, p2, 0x3c

    add-int/2addr v1, p3

    invoke-static {v0, v1}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lN(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;I)I

    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lF(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lJ(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setStartTimeForQuietMode(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lK(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Lcom/android/settings/dndmode/LabelPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    iget-object v2, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v2}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lJ(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lO(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lF(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$AntiSpam;->isAutoTimerOfQuietModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lF(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/dndmode/a;->lc(Landroid/content/Context;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    mul-int/lit8 v1, p2, 0x3c

    add-int/2addr v1, p3

    invoke-static {v0, v1}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lM(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;I)I

    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lF(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lG(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setEndTimeForQuietMode(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lH(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Lcom/android/settings/dndmode/LabelPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    iget-object v2, p0, Lcom/android/settings/dndmode/m;->nf:Lcom/android/settings/dndmode/AutoTimeSettingsFragment;

    invoke-static {v2}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lG(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lO(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto :goto_0
.end method
