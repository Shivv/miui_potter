.class public Lcom/android/settings/dndmode/RepeatPreference;
.super Landroid/preference/ListPreference;
.source "RepeatPreference.java"


# instance fields
.field private mA:Lcom/android/settings/dndmode/e;

.field private mB:Lcom/android/settings/dndmode/e;

.field private mC:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/settings/dndmode/e;

    invoke-direct {v0, v3}, Lcom/android/settings/dndmode/e;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    new-instance v0, Lcom/android/settings/dndmode/e;

    invoke-direct {v0, v3}, Lcom/android/settings/dndmode/e;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mB:Lcom/android/settings/dndmode/e;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mC:Ljava/lang/String;

    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    aget-object v2, v0, v5

    aput-object v2, v1, v3

    aget-object v2, v0, v6

    aput-object v2, v1, v4

    aget-object v2, v0, v7

    aput-object v2, v1, v5

    const/4 v2, 0x5

    aget-object v2, v0, v2

    aput-object v2, v1, v6

    const/4 v2, 0x6

    aget-object v2, v0, v2

    aput-object v2, v1, v7

    const/4 v2, 0x7

    aget-object v2, v0, v2

    const/4 v3, 0x5

    aput-object v2, v1, v3

    aget-object v0, v0, v4

    const/4 v2, 0x6

    aput-object v0, v1, v2

    invoke-virtual {p0, v1}, Lcom/android/settings/dndmode/RepeatPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Lcom/android/settings/dndmode/RepeatPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic mf(Lcom/android/settings/dndmode/RepeatPreference;)Lcom/android/settings/dndmode/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mB:Lcom/android/settings/dndmode/e;

    return-object v0
.end method

.method static synthetic mg(Lcom/android/settings/dndmode/RepeatPreference;)V
    .locals 0

    invoke-super {p0}, Landroid/preference/ListPreference;->onClick()V

    return-void
.end method


# virtual methods
.method public mc(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mC:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/android/settings/dndmode/RepeatPreference;->mC:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public md(Lcom/android/settings/dndmode/e;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    invoke-virtual {v0, p1}, Lcom/android/settings/dndmode/e;->ml(Lcom/android/settings/dndmode/e;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mB:Lcom/android/settings/dndmode/e;

    invoke-virtual {v0, p1}, Lcom/android/settings/dndmode/e;->ml(Lcom/android/settings/dndmode/e;)V

    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/dndmode/e;->mj(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/RepeatPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public me()Lcom/android/settings/dndmode/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    return-object v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0a024c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/dndmode/RepeatPreference;->mC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onBindView(Landroid/view/View;)V

    return-void
.end method

.method protected onClick()V
    .locals 6

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/dndmode/c;->lU(Landroid/content/Context;)Z

    move-result v1

    :goto_0
    iget-object v1, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    invoke-virtual {v1}, Lcom/android/settings/dndmode/e;->mm()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_1

    const v1, 0x7f03000b

    :goto_1
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    const/4 v2, -0x1

    const/4 v1, 0x0

    :goto_2
    array-length v5, v4

    if-ge v1, v5, :cond_3

    aget v5, v4, v1

    if-ne v3, v5, :cond_2

    :goto_3
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/android/settings/dndmode/p;

    invoke-direct {v3, p0, v4}, Lcom/android/settings/dndmode/p;-><init>(Lcom/android/settings/dndmode/RepeatPreference;[I)V

    invoke-virtual {v2, v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v1, 0x7f03000c

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method protected onDialogClosed(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    iget-object v1, p0, Lcom/android/settings/dndmode/RepeatPreference;->mB:Lcom/android/settings/dndmode/e;

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/e;->ml(Lcom/android/settings/dndmode/e;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/dndmode/e;->mj(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/RepeatPreference;->mc(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/RepeatPreference;->callChangeListener(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dndmode/RepeatPreference;->mB:Lcom/android/settings/dndmode/e;

    iget-object v1, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/e;->ml(Lcom/android/settings/dndmode/e;)V

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/dndmode/RepeatPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/RepeatPreference;->mA:Lcom/android/settings/dndmode/e;

    invoke-virtual {v1}, Lcom/android/settings/dndmode/e;->mi()[Z

    move-result-object v1

    new-instance v2, Lcom/android/settings/dndmode/q;

    invoke-direct {v2, p0}, Lcom/android/settings/dndmode/q;-><init>(Lcom/android/settings/dndmode/RepeatPreference;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method
