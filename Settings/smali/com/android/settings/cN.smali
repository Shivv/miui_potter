.class public Lcom/android/settings/cN;
.super Ljava/lang/Object;
.source "ApkIconLoader.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static final cep:Ljava/util/concurrent/ConcurrentHashMap;


# instance fields
.field private ceq:Lcom/android/settings/cR;

.field private cer:Z

.field private final ces:Landroid/os/Handler;

.field private cet:Z

.field private final ceu:Ljava/util/concurrent/ConcurrentHashMap;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/android/settings/cN;->cep:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/cN;->ceu:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/settings/cN;->ces:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/cN;->mContext:Landroid/content/Context;

    return-void
.end method

.method private bWm(Landroid/widget/ImageView;Ljava/lang/String;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/android/settings/cN;->cep:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cO;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/settings/cO;->create()Lcom/android/settings/cO;

    move-result-object v0

    if-nez v0, :cond_0

    return v3

    :cond_0
    sget-object v1, Lcom/android/settings/cN;->cep:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iput v3, v0, Lcom/android/settings/cO;->cev:I

    return v3

    :cond_2
    iget v1, v0, Lcom/android/settings/cO;->cev:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/cO;->bWt()Z

    move-result v1

    if-eqz v1, :cond_3

    return v4

    :cond_3
    invoke-virtual {v0, p1}, Lcom/android/settings/cO;->bWv(Landroid/widget/ImageView;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v4
.end method

.method private bWn()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/cN;->ceu:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/cN;->ceu:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/cQ;

    iget-object v1, v1, Lcom/android/settings/cQ;->cex:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/cN;->bWm(Landroid/widget/ImageView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/cN;->ceu:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/cN;->bWo()V

    :cond_2
    return-void
.end method

.method private bWo()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/settings/cN;->cer:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/android/settings/cN;->cer:Z

    iget-object v0, p0, Lcom/android/settings/cN;->ces:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method static synthetic bWp(Lcom/android/settings/cN;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cN;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic bWq()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    sget-object v0, Lcom/android/settings/cN;->cep:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic bWr(Lcom/android/settings/cN;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cN;->ces:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bWs(Lcom/android/settings/cN;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cN;->ceu:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method


# virtual methods
.method public bWl(Landroid/widget/ImageView;Ljava/lang/String;)Z
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/android/settings/cN;->bWm(Landroid/widget/ImageView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/cN;->ceu:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-instance v1, Lcom/android/settings/cQ;

    invoke-direct {v1, p2}, Lcom/android/settings/cQ;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/settings/cN;->ceu:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/android/settings/cN;->cet:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/cN;->bWo()V

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cN;->ceu:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    sget-object v0, Lcom/android/settings/cN;->cep:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    return v1

    :pswitch_0
    iput-boolean v1, p0, Lcom/android/settings/cN;->cer:Z

    iget-boolean v0, p0, Lcom/android/settings/cN;->cet:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/cN;->ceq:Lcom/android/settings/cR;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/cR;

    invoke-direct {v0, p0}, Lcom/android/settings/cR;-><init>(Lcom/android/settings/cN;)V

    iput-object v0, p0, Lcom/android/settings/cN;->ceq:Lcom/android/settings/cR;

    iget-object v0, p0, Lcom/android/settings/cN;->ceq:Lcom/android/settings/cR;

    invoke-virtual {v0}, Lcom/android/settings/cR;->start()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/cN;->ceq:Lcom/android/settings/cR;

    invoke-virtual {v0}, Lcom/android/settings/cR;->bWx()V

    :cond_1
    return v2

    :pswitch_1
    iget-boolean v0, p0, Lcom/android/settings/cN;->cet:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/cN;->bWn()V

    :cond_2
    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public pause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/cN;->cet:Z

    return-void
.end method

.method public stop()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/cN;->pause()V

    iget-object v0, p0, Lcom/android/settings/cN;->ceq:Lcom/android/settings/cR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/cN;->ceq:Lcom/android/settings/cR;

    invoke-virtual {v0}, Lcom/android/settings/cR;->quit()Z

    iput-object v1, p0, Lcom/android/settings/cN;->ceq:Lcom/android/settings/cR;

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/cN;->clear()V

    return-void
.end method
