.class public Lcom/android/settings/deletionhelper/a;
.super Ljava/lang/Object;
.source "AutomaticStorageManagerSwitchBarController.java"

# interfaces
.implements Lcom/android/settings/widget/t;


# instance fields
.field private bmZ:Landroid/support/v7/preference/Preference;

.field private bna:Landroid/app/FragmentManager;

.field private bnb:Lcom/android/settings/core/instrumentation/e;

.field private bnc:Lcom/android/settings/widget/SwitchBar;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/widget/SwitchBar;Lcom/android/settings/core/instrumentation/e;Landroid/support/v7/preference/Preference;Landroid/app/FragmentManager;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/android/settings/deletionhelper/a;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/SwitchBar;

    iput-object v0, p0, Lcom/android/settings/deletionhelper/a;->bnc:Lcom/android/settings/widget/SwitchBar;

    invoke-static {p3}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/e;

    iput-object v0, p0, Lcom/android/settings/deletionhelper/a;->bnb:Lcom/android/settings/core/instrumentation/e;

    invoke-static {p4}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    iput-object v0, p0, Lcom/android/settings/deletionhelper/a;->bmZ:Landroid/support/v7/preference/Preference;

    invoke-static {p5}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/FragmentManager;

    iput-object v0, p0, Lcom/android/settings/deletionhelper/a;->bna:Landroid/app/FragmentManager;

    invoke-direct {p0}, Lcom/android/settings/deletionhelper/a;->baX()V

    return-void
.end method

.method private baX()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/deletionhelper/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "automatic_storage_manager_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/deletionhelper/a;->bnc:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/SwitchBar;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/deletionhelper/a;->bnc:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/SwitchBar;->aBx(Lcom/android/settings/widget/t;)V

    return-void
.end method

.method private baY()V
    .locals 3

    const-string/jumbo v0, "ro.storage_manager.enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/settings/deletionhelper/ActivationWarningFragment;->baQ()Lcom/android/settings/deletionhelper/ActivationWarningFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deletionhelper/a;->bna:Landroid/app/FragmentManager;

    const-string/jumbo v2, "ActivationWarningFragment"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/deletionhelper/ActivationWarningFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public baW()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deletionhelper/a;->bnc:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/SwitchBar;->aBy(Lcom/android/settings/widget/t;)V

    return-void
.end method

.method public gG(Landroid/widget/Switch;Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/deletionhelper/a;->bnb:Lcom/android/settings/core/instrumentation/e;

    iget-object v1, p0, Lcom/android/settings/deletionhelper/a;->mContext:Landroid/content/Context;

    const/16 v2, 0x1e9

    invoke-virtual {v0, v1, v2, p2}, Lcom/android/settings/core/instrumentation/e;->ajV(Landroid/content/Context;IZ)V

    iget-object v0, p0, Lcom/android/settings/deletionhelper/a;->bmZ:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, p2}, Landroid/support/v7/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/deletionhelper/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "automatic_storage_manager_enabled"

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/android/settings/deletionhelper/a;->baY()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
