.class Lcom/android/settings/bD;
.super Landroid/content/BroadcastReceiver;
.source "MiuiWirelessSettings.java"


# instance fields
.field final synthetic bSF:Lcom/android/settings/MiuiWirelessSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/MiuiWirelessSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/MiuiWirelessSettings;Lcom/android/settings/bD;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bD;-><init>(Lcom/android/settings/MiuiWirelessSettings;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v0, "availableArray"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const-string/jumbo v1, "tetherArray"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string/jumbo v2, "erroredArray"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-static {v3, v0, v1, v2}, Lcom/android/settings/MiuiWirelessSettings;->bLM(Lcom/android/settings/MiuiWirelessSettings;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0, v3}, Lcom/android/settings/MiuiWirelessSettings;->bLI(Lcom/android/settings/MiuiWirelessSettings;Z)Z

    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiWirelessSettings;->bLL(Lcom/android/settings/MiuiWirelessSettings;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "android.intent.action.MEDIA_UNSHARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0, v2}, Lcom/android/settings/MiuiWirelessSettings;->bLI(Lcom/android/settings/MiuiWirelessSettings;Z)Z

    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiWirelessSettings;->bLL(Lcom/android/settings/MiuiWirelessSettings;)V

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    const-string/jumbo v1, "connected"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/MiuiWirelessSettings;->bLJ(Lcom/android/settings/MiuiWirelessSettings;Z)Z

    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiWirelessSettings;->bLL(Lcom/android/settings/MiuiWirelessSettings;)V

    goto :goto_0

    :cond_4
    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiWirelessSettings;->bLE(Lcom/android/settings/MiuiWirelessSettings;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "android.bluetooth.adapter.extra.STATE"

    const/high16 v1, -0x80000000

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiWirelessSettings;->bLL(Lcom/android/settings/MiuiWirelessSettings;)V

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiWirelessSettings;->bLF(Lcom/android/settings/MiuiWirelessSettings;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothPan;

    if-eqz v0, :cond_5

    invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothPan;->setBluetoothTethering(Z)V

    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0, v2}, Lcom/android/settings/MiuiWirelessSettings;->bLG(Lcom/android/settings/MiuiWirelessSettings;Z)Z

    goto :goto_1

    :sswitch_1
    iget-object v0, p0, Lcom/android/settings/bD;->bSF:Lcom/android/settings/MiuiWirelessSettings;

    invoke-static {v0, v2}, Lcom/android/settings/MiuiWirelessSettings;->bLG(Lcom/android/settings/MiuiWirelessSettings;Z)Z

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_0
    .end sparse-switch
.end method
