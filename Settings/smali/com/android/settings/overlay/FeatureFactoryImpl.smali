.class public Lcom/android/settings/overlay/FeatureFactoryImpl;
.super Lcom/android/settings/overlay/a;
.source "FeatureFactoryImpl.java"


# instance fields
.field private aUS:Lcom/android/settings/applications/ApplicationFeatureProvider;

.field private aUT:Lcom/android/settings/gestures/f;

.field private aUU:Lcom/android/settings/dashboard/k;

.field private aUV:Lcom/android/settings/enterprise/v;

.field private aUW:Lcom/android/settings/localepicker/a;

.field private aUX:Lcom/android/settings/security/d;

.field private aUY:Lcom/android/settings/dashboard/suggestions/a;

.field private aUZ:Lcom/android/settings/users/UserFeatureProvider;

.field private mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

.field private mSearchFeatureProvider:Lcom/android/settings/search2/SearchFeatureProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/overlay/a;-><init>()V

    return-void
.end method


# virtual methods
.method public aIl(Landroid/content/Context;)Lcom/android/settings/enterprise/v;
    .locals 7

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUV:Lcom/android/settings/enterprise/v;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Lcom/android/settings/enterprise/i;

    new-instance v2, Lcom/android/settings/enterprise/z;

    const-string/jumbo v3, "device_policy"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/admin/DevicePolicyManager;

    invoke-direct {v2, v3}, Lcom/android/settings/enterprise/z;-><init>(Landroid/app/admin/DevicePolicyManager;)V

    new-instance v3, Lcom/android/settings/applications/PackageManagerWrapperImpl;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/settings/applications/PackageManagerWrapperImpl;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v4

    new-instance v5, Lcom/android/settings/vpn2/ConnectivityManagerWrapperImpl;

    const-string/jumbo v6, "connectivity"

    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    invoke-direct {v5, v6}, Lcom/android/settings/vpn2/ConnectivityManagerWrapperImpl;-><init>(Landroid/net/ConnectivityManager;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/enterprise/i;-><init>(Landroid/content/Context;Lcom/android/settings/enterprise/q;Lcom/android/settings/applications/PackageManagerWrapper;Landroid/os/UserManager;Lcom/android/settings/vpn2/ConnectivityManagerWrapper;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUV:Lcom/android/settings/enterprise/v;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUV:Lcom/android/settings/enterprise/v;

    return-object v0
.end method

.method public aIm()Lcom/android/settings/core/instrumentation/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/core/instrumentation/e;

    invoke-direct {v0}, Lcom/android/settings/core/instrumentation/e;-><init>()V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-object v0
.end method

.method public aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUU:Lcom/android/settings/dashboard/k;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/dashboard/k;

    invoke-direct {v0, p1}, Lcom/android/settings/dashboard/k;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUU:Lcom/android/settings/dashboard/k;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUU:Lcom/android/settings/dashboard/k;

    return-object v0
.end method

.method public aIo(Landroid/content/Context;)Lcom/android/settings/overlay/c;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public aIp(Landroid/content/Context;)Lcom/android/settings/applications/ApplicationFeatureProvider;
    .locals 6

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUS:Lcom/android/settings/applications/ApplicationFeatureProvider;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/settings/applications/ApplicationFeatureProviderImpl;

    new-instance v3, Lcom/android/settings/applications/PackageManagerWrapperImpl;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/android/settings/applications/PackageManagerWrapperImpl;-><init>(Landroid/content/pm/PackageManager;)V

    new-instance v4, Lcom/android/settings/applications/IPackageManagerWrapperImpl;

    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/android/settings/applications/IPackageManagerWrapperImpl;-><init>(Landroid/content/pm/IPackageManager;)V

    new-instance v5, Lcom/android/settings/enterprise/z;

    const-string/jumbo v0, "device_policy"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-direct {v5, v0}, Lcom/android/settings/enterprise/z;-><init>(Landroid/app/admin/DevicePolicyManager;)V

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/android/settings/applications/ApplicationFeatureProviderImpl;-><init>(Landroid/content/Context;Lcom/android/settings/applications/PackageManagerWrapper;Lcom/android/settings/applications/IPackageManagerWrapper;Lcom/android/settings/enterprise/q;)V

    iput-object v2, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUS:Lcom/android/settings/applications/ApplicationFeatureProvider;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUS:Lcom/android/settings/applications/ApplicationFeatureProvider;

    return-object v0
.end method

.method public aIq()Lcom/android/settings/gestures/f;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUT:Lcom/android/settings/gestures/f;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/gestures/c;

    invoke-direct {v0}, Lcom/android/settings/gestures/c;-><init>()V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUT:Lcom/android/settings/gestures/f;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUT:Lcom/android/settings/gestures/f;

    return-object v0
.end method

.method public aIr()Lcom/android/settings/localepicker/a;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUW:Lcom/android/settings/localepicker/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/localepicker/b;

    invoke-direct {v0}, Lcom/android/settings/localepicker/b;-><init>()V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUW:Lcom/android/settings/localepicker/a;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUW:Lcom/android/settings/localepicker/a;

    return-object v0
.end method

.method public aIs(Landroid/content/Context;)Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/fuelgauge/PowerUsageFeatureProviderImpl;

    invoke-direct {v0, p1}, Lcom/android/settings/fuelgauge/PowerUsageFeatureProviderImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    return-object v0
.end method

.method public aIt()Lcom/android/settings/search2/SearchFeatureProvider;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mSearchFeatureProvider:Lcom/android/settings/search2/SearchFeatureProvider;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/search2/SearchFeatureProviderImpl;

    invoke-direct {v0}, Lcom/android/settings/search2/SearchFeatureProviderImpl;-><init>()V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mSearchFeatureProvider:Lcom/android/settings/search2/SearchFeatureProvider;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->mSearchFeatureProvider:Lcom/android/settings/search2/SearchFeatureProvider;

    return-object v0
.end method

.method public aIu()Lcom/android/settings/security/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUX:Lcom/android/settings/security/d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/security/b;

    invoke-direct {v0}, Lcom/android/settings/security/b;-><init>()V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUX:Lcom/android/settings/security/d;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUX:Lcom/android/settings/security/d;

    return-object v0
.end method

.method public aIv(Landroid/content/Context;)Lcom/android/settings/dashboard/suggestions/a;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUY:Lcom/android/settings/dashboard/suggestions/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/dashboard/suggestions/b;

    invoke-direct {v0, p1}, Lcom/android/settings/dashboard/suggestions/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUY:Lcom/android/settings/dashboard/suggestions/a;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUY:Lcom/android/settings/dashboard/suggestions/a;

    return-object v0
.end method

.method public aIw(Landroid/content/Context;)Lcom/android/settings/overlay/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public aIx(Landroid/content/Context;)Lcom/android/settings/users/UserFeatureProvider;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUZ:Lcom/android/settings/users/UserFeatureProvider;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/users/UserFeatureProviderImpl;

    invoke-direct {v0, p1}, Lcom/android/settings/users/UserFeatureProviderImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUZ:Lcom/android/settings/users/UserFeatureProvider;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/overlay/FeatureFactoryImpl;->aUZ:Lcom/android/settings/users/UserFeatureProvider;

    return-object v0
.end method
