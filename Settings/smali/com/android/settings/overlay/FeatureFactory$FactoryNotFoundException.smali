.class public final Lcom/android/settings/overlay/FeatureFactory$FactoryNotFoundException;
.super Ljava/lang/RuntimeException;
.source "FeatureFactory.java"


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    const-string/jumbo v0, "Unable to create factory. Did you misconfigure Proguard?"

    invoke-direct {p0, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method
