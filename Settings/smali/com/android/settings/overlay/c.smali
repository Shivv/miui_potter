.class public interface abstract Lcom/android/settings/overlay/c;
.super Ljava/lang/Object;
.source "SurveyFeatureProvider.java"


# direct methods
.method public static aIX(Landroid/app/Activity;Landroid/content/BroadcastReceiver;)V
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot unregister receiver if activity is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p0}, Landroid/support/v4/content/b;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/content/b;->eai(Landroid/content/BroadcastReceiver;)V

    return-void
.end method


# virtual methods
.method public abstract aIS(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract aIT(Landroid/content/Context;Ljava/lang/String;)J
.end method

.method public abstract aIU(Landroid/app/Activity;)Landroid/content/BroadcastReceiver;
.end method

.method public abstract aIV(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract aIW(Landroid/app/Activity;Ljava/lang/String;)Z
.end method
