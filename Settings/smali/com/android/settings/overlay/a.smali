.class public abstract Lcom/android/settings/overlay/a;
.super Ljava/lang/Object;
.source "FeatureFactory.java"


# static fields
.field protected static aUR:Lcom/android/settings/overlay/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;
    .locals 2

    sget-object v0, Lcom/android/settings/overlay/a;->aUR:Lcom/android/settings/overlay/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/overlay/a;->aUR:Lcom/android/settings/overlay/a;

    return-object v0

    :cond_0
    const v0, 0x7f120452

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "No feature factory configured"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/overlay/a;

    sput-object v0, Lcom/android/settings/overlay/a;->aUR:Lcom/android/settings/overlay/a;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v0, Lcom/android/settings/overlay/a;->aUR:Lcom/android/settings/overlay/a;

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/android/settings/overlay/FeatureFactory$FactoryNotFoundException;

    invoke-direct {v1, v0}, Lcom/android/settings/overlay/FeatureFactory$FactoryNotFoundException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public abstract aIl(Landroid/content/Context;)Lcom/android/settings/enterprise/v;
.end method

.method public abstract aIm()Lcom/android/settings/core/instrumentation/e;
.end method

.method public abstract aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;
.end method

.method public abstract aIo(Landroid/content/Context;)Lcom/android/settings/overlay/c;
.end method

.method public abstract aIp(Landroid/content/Context;)Lcom/android/settings/applications/ApplicationFeatureProvider;
.end method

.method public abstract aIq()Lcom/android/settings/gestures/f;
.end method

.method public abstract aIr()Lcom/android/settings/localepicker/a;
.end method

.method public abstract aIs(Landroid/content/Context;)Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;
.end method

.method public abstract aIt()Lcom/android/settings/search2/SearchFeatureProvider;
.end method

.method public abstract aIu()Lcom/android/settings/security/d;
.end method

.method public abstract aIv(Landroid/content/Context;)Lcom/android/settings/dashboard/suggestions/a;
.end method

.method public abstract aIw(Landroid/content/Context;)Lcom/android/settings/overlay/b;
.end method

.method public abstract aIx(Landroid/content/Context;)Lcom/android/settings/users/UserFeatureProvider;
.end method
