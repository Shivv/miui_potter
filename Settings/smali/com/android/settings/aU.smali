.class public Lcom/android/settings/aU;
.super Ljava/lang/Object;
.source "CompatibilityUtils.java"


# static fields
.field private static bHY:Ljava/lang/String;

.field private static bHZ:Ljava/lang/String;

.field private static bIa:Landroid/app/INotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "notification"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    move-result-object v0

    sput-object v0, Lcom/android/settings/aU;->bIa:Landroid/app/INotificationManager;

    const-string/jumbo v0, "app_uid"

    sput-object v0, Lcom/android/settings/aU;->bHY:Ljava/lang/String;

    const-string/jumbo v0, "high_priority_setting"

    sput-object v0, Lcom/android/settings/aU;->bHZ:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byH()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static byI(Ljava/lang/String;I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static byJ(Landroid/content/Context;Ljava/lang/String;IZ)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
