.class public abstract Lcom/android/settings/MiuiTwoStatePreference;
.super Landroid/preference/Preference;
.source "MiuiTwoStatePreference.java"


# instance fields
.field protected cgR:Z

.field private cgS:Z

.field private cgT:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/MiuiTwoStatePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/MiuiTwoStatePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgR:Z

    return v0
.end method

.method protected onClick()V
    .locals 2

    invoke-super {p0}, Landroid/preference/Preference;->onClick()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiTwoStatePreference;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiTwoStatePreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTwoStatePreference;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/android/settings/MiuiTwoStatePreference$SavedState;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    :cond_1
    check-cast p1, Lcom/android/settings/MiuiTwoStatePreference$SavedState;

    invoke-virtual {p1}, Lcom/android/settings/MiuiTwoStatePreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v0, p1, Lcom/android/settings/MiuiTwoStatePreference$SavedState;->cgU:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTwoStatePreference;->setChecked(Z)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiTwoStatePreference;->isPersistent()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Lcom/android/settings/MiuiTwoStatePreference$SavedState;

    invoke-direct {v1, v0}, Lcom/android/settings/MiuiTwoStatePreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiTwoStatePreference;->isChecked()Z

    move-result v0

    iput-boolean v0, v1, Lcom/android/settings/MiuiTwoStatePreference$SavedState;->cgU:Z

    return-object v1
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgR:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTwoStatePreference;->getPersistedBoolean(Z)Z

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTwoStatePreference;->setChecked(Z)V

    return-void

    :cond_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgR:Z

    if-eq v0, p1, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgS:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    :cond_0
    iput-boolean p1, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgR:Z

    iput-boolean v1, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgS:Z

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiTwoStatePreference;->persistBoolean(Z)Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiTwoStatePreference;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTwoStatePreference;->notifyDependencyChange(Z)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiTwoStatePreference;->notifyChanged()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldDisableDependents()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgT:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgR:Z

    :goto_0
    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/preference/Preference;->shouldDisableDependents()Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/MiuiTwoStatePreference;->cgR:Z

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method
