.class public Lcom/android/settings/OldmanModeSettings;
.super Lmiui/app/Activity;
.source "OldmanModeSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final synthetic ccf:[I


# instance fields
.field private cca:Landroid/widget/Button;

.field private ccb:Ljava/lang/String;

.field private ccc:Landroid/widget/TextView;

.field private ccd:Ljava/lang/String;

.field private cce:Lcom/android/settings/OldmanModeSettings$JeejenStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method private bUc()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccb:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/OldmanModeSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccb:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccb:Ljava/lang/String;

    goto :goto_0
.end method

.method private bUd()V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->ccj:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    iput-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cce:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const-string/jumbo v0, "com.jeejen.family.miui"

    aput-object v0, v4, v2

    const-string/jumbo v0, "com.jeejen.family"

    aput-object v0, v4, v1

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.category.HOME"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/OldmanModeSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/high16 v5, 0x20000

    invoke-virtual {v3, v0, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    array-length v6, v4

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_6

    aget-object v7, v4, v3

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v7, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    iput-object v7, p0, Lcom/android/settings/OldmanModeSettings;->ccd:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccb:Ljava/lang/String;

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/OldmanModeSettings;->bUc()V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccb:Ljava/lang/String;

    invoke-static {v0, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->cch:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    iput-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cce:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    return-void

    :cond_5
    sget-object v0, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->cci:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    iput-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cce:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    goto :goto_2

    :cond_6
    return-void

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method private bUe(Ljava/lang/String;)V
    .locals 11

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/OldmanModeSettings;->ccb:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/OldmanModeSettings;->bUc()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/OldmanModeSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v1, p0, Lcom/android/settings/OldmanModeSettings;->ccb:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/OldmanModeSettings;->ccb:Ljava/lang/String;

    invoke-virtual {v4, v1}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.category.HOME"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v3, 0x20000

    invoke-virtual {v4, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-array v7, v6, [Landroid/content/ComponentName;

    move v3, v0

    move v1, v0

    :goto_0
    if-ge v3, v6, :cond_3

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v10, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v7, v3

    iget v8, v0, Landroid/content/pm/ResolveInfo;->match:I

    if-le v8, v1, :cond_2

    iget v1, v0, Landroid/content/pm/ResolveInfo;->match:I

    :cond_2
    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {p1, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v0

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.category.HOME"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v3, Landroid/content/ComponentName;

    iget-object v5, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v5, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1, v7, v3}, Landroid/content/pm/PackageManager;->addPreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    return-void

    :cond_4
    move-object v0, v2

    goto :goto_1
.end method

.method private static synthetic bUf()[I
    .locals 3

    sget-object v0, Lcom/android/settings/OldmanModeSettings;->ccf:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/OldmanModeSettings;->ccf:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->values()[Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->cch:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    invoke-virtual {v1}, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->cci:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    invoke-virtual {v1}, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->ccj:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    invoke-virtual {v1}, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    sput-object v0, Lcom/android/settings/OldmanModeSettings;->ccf:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cca:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/android/settings/OldmanModeSettings;->bUf()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/OldmanModeSettings;->cce:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    invoke-virtual {v1}, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "market://details?id=com.jeejen.family&ref=com.miui.home_setting&back=true"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/OldmanModeSettings;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.applications.PreferredSettings"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.category.HOME"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.category.HOME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v3, "preferred_app_intent_filter"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v0, "preferred_app_intent"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/OldmanModeSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f12054f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "preferred_label"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cce:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    sget-object v2, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->cch:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    if-ne v0, v2, :cond_3

    const-string/jumbo v0, "com.miui.home"

    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v0}, Lcom/android/settings/OldmanModeSettings;->bUe(Ljava/lang/String;)V

    :cond_1
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/android/settings/OldmanModeSettings;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "OldmanModeSettings"

    const-string/jumbo v3, "Fail to start activity"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string/jumbo v0, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.DefaultLauncherSettings"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/android/settings/OldmanModeSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "com.miui.home"

    const-string/jumbo v2, "com.miui.home.settings.DefaultLauncherSettings"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccd:Ljava/lang/String;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/OldmanModeSettings;->bUd()V

    const v0, 0x7f0d0117

    invoke-virtual {p0, v0}, Lcom/android/settings/OldmanModeSettings;->setContentView(I)V

    const v0, 0x7f0a012f

    invoke-virtual {p0, v0}, Lcom/android/settings/OldmanModeSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccc:Landroid/widget/TextView;

    const v0, 0x7f0a0021

    invoke-virtual {p0, v0}, Lcom/android/settings/OldmanModeSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cca:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cca:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/android/settings/OldmanModeSettings;->bUf()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/OldmanModeSettings;->cce:Lcom/android/settings/OldmanModeSettings$JeejenStatus;

    invoke-virtual {v1}, Lcom/android/settings/OldmanModeSettings$JeejenStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccc:Landroid/widget/TextView;

    const v1, 0x7f120c19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cca:Landroid/widget/Button;

    const v1, 0x7f120c18

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccc:Landroid/widget/TextView;

    const v1, 0x7f120c14

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cca:Landroid/widget/Button;

    const v1, 0x7f120c13

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->ccc:Landroid/widget/TextView;

    const v1, 0x7f120c16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/OldmanModeSettings;->cca:Landroid/widget/Button;

    const v1, 0x7f120c15

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
