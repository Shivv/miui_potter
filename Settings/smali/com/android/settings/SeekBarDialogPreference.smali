.class public Lcom/android/settings/SeekBarDialogPreference;
.super Lcom/android/settings/CustomDialogPreference;
.source "SeekBarDialogPreference.java"


# instance fields
.field private final bRY:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/CustomDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f0d0132

    invoke-virtual {p0, v0}, Lcom/android/settings/SeekBarDialogPreference;->dly(I)V

    invoke-virtual {p0}, Lcom/android/settings/SeekBarDialogPreference;->bKT()V

    invoke-virtual {p0}, Lcom/android/settings/SeekBarDialogPreference;->dlx()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SeekBarDialogPreference;->bRY:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/SeekBarDialogPreference;->dlr(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public bKT()V
    .locals 1

    const v0, 0x104000a

    invoke-virtual {p0, v0}, Lcom/android/settings/SeekBarDialogPreference;->dlz(I)V

    const/high16 v0, 0x1040000

    invoke-virtual {p0, v0}, Lcom/android/settings/SeekBarDialogPreference;->dlp(I)V

    return-void
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/CustomDialogPreference;->onBindDialogView(Landroid/view/View;)V

    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/SeekBarDialogPreference;->bRY:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/SeekBarDialogPreference;->bRY:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
