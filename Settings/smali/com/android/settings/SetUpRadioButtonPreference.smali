.class public Lcom/android/settings/SetUpRadioButtonPreference;
.super Lmiui/preference/RadioButtonPreference;
.source "SetUpRadioButtonPreference.java"


# instance fields
.field private cbA:Z

.field private cbx:Landroid/content/res/ColorStateList;

.field private cby:Landroid/content/res/ColorStateList;

.field private cbz:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/SetUpRadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->bTB()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->bTB()V

    return-void
.end method

.method private bTA()Landroid/content/res/ColorStateList;
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-array v0, v6, [I

    const v1, 0x10100a0

    aput v1, v0, v7

    invoke-virtual {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    new-array v4, v5, [[I

    new-array v5, v5, [I

    aput-object v0, v4, v7

    sget v0, Lmiui/R$attr;->textColorChecked:I

    invoke-virtual {v2, v0, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    aput v0, v5, v7

    new-array v0, v6, [I

    aput-object v0, v4, v6

    sget v0, Lmiui/R$attr;->preferencePrimaryTextColor:I

    invoke-virtual {v2, v0, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    aput v0, v5, v6

    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v0
.end method

.method private bTB()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->bTA()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbx:Landroid/content/res/ColorStateList;

    const v0, 0x7f0d0157

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpRadioButtonPreference;->setWidgetLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public bTC(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbz:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    :goto_0
    iput-object p1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbz:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpRadioButtonPreference;->bTy(Z)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->notifyChanged()V

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbz:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public bTy(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbA:Z

    return-void
.end method

.method public bTz(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpRadioButtonPreference;->bTC(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 6

    const v5, 0x1020001

    const/16 v4, 0x8

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lmiui/preference/RadioButtonPreference;->onBindView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v2

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v1, v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cby:Landroid/content/res/ColorStateList;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cby:Landroid/content/res/ColorStateList;

    :cond_0
    iget-object v1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cby:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/widget/TextView;->getDrawableState()[I

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/SetUpRadioButtonPreference;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbx:Landroid/content/res/ColorStateList;

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_1
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    const v0, 0x7f0a0133

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbA:Z

    if-eqz v1, :cond_5

    move v1, v3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbA:Z

    if-eqz v1, :cond_7

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbz:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbz:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    :goto_3
    return-void

    :cond_3
    move v2, v3

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbx:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v1}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_2

    :cond_6
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpRadioButtonPreference;->cbz:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_7
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3
.end method
