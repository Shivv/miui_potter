.class Lcom/android/settings/language/MiuiLanguageAndInputSettings$SummaryProvider;
.super Ljava/lang/Object;
.source "MiuiLanguageAndInputSettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final blQ:Lcom/android/settings/dashboard/C;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SummaryProvider;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SummaryProvider;->blQ:Lcom/android/settings/dashboard/C;

    return-void
.end method


# virtual methods
.method public jt(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SummaryProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZh(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SummaryProvider;->blQ:Lcom/android/settings/dashboard/C;

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
