.class public Lcom/android/settings/language/LanguageAndInputSettings;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "LanguageAndInputSettings.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;


# instance fields
.field private blw:Lcom/android/internal/hardware/AmbientDisplayConfiguration;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/android/settings/language/-$Lambda$Vmt4sq59gHNLRct1FZS3zYxE7jc;->$INST$0:Lcom/android/settings/language/-$Lambda$Vmt4sq59gHNLRct1FZS3zYxE7jc;

    sput-object v0, Lcom/android/settings/language/LanguageAndInputSettings;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    new-instance v0, Lcom/android/settings/language/LanguageAndInputSettings$1;

    invoke-direct {v0}, Lcom/android/settings/language/LanguageAndInputSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/language/LanguageAndInputSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method

.method private static aYP(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Lcom/android/internal/hardware/AmbientDisplayConfiguration;)Ljava/util/List;
    .locals 7

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/android/settings/language/PhoneLanguagePreferenceController;

    invoke-direct {v0, p0}, Lcom/android/settings/language/PhoneLanguagePreferenceController;-><init>(Landroid/content/Context;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/inputmethod/SpellCheckerPreferenceController;

    invoke-direct {v0, p0}, Lcom/android/settings/inputmethod/SpellCheckerPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/language/UserDictionaryPreferenceController;

    invoke-direct {v0, p0}, Lcom/android/settings/language/UserDictionaryPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/language/TtsPreferenceController;

    new-instance v1, Landroid/speech/tts/TtsEngines;

    invoke-direct {v1, p0}, Landroid/speech/tts/TtsEngines;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/language/TtsPreferenceController;-><init>(Landroid/content/Context;Landroid/speech/tts/TtsEngines;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/inputmethod/VirtualKeyboardPreferenceController;

    invoke-direct {v0, p0}, Lcom/android/settings/inputmethod/VirtualKeyboardPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/inputmethod/PhysicalKeyboardPreferenceController;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/inputmethod/PhysicalKeyboardPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/inputmethod/GameControllerPreferenceController;

    invoke-direct {v0, p0}, Lcom/android/settings/inputmethod/GameControllerPreferenceController;-><init>(Landroid/content/Context;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/gestures/d;

    const-string/jumbo v1, "gesture_assist_input_summary"

    invoke-direct {v0, p0, p1, v1}, Lcom/android/settings/gestures/d;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/gestures/j;

    const-string/jumbo v1, "gesture_swipe_down_fingerprint_input_summary"

    invoke-direct {v0, p0, p1, v1}, Lcom/android/settings/gestures/j;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/gestures/l;

    const-string/jumbo v1, "gesture_double_twist_input_summary"

    invoke-direct {v0, p0, p1, v1}, Lcom/android/settings/gestures/l;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/gestures/a;

    const-string/jumbo v1, "gesture_double_tap_power_input_summary"

    invoke-direct {v0, p0, p1, v1}, Lcom/android/settings/gestures/a;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/gestures/i;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    const-string/jumbo v5, "gesture_pick_up_input_summary"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/gestures/i;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Lcom/android/internal/hardware/AmbientDisplayConfiguration;ILjava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/gestures/k;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    const-string/jumbo v5, "gesture_double_tap_screen_input_summary"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/gestures/k;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Lcom/android/internal/hardware/AmbientDisplayConfiguration;ILjava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/applications/defaultapps/h;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/defaultapps/h;-><init>(Landroid/content/Context;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v6
.end method

.method static synthetic aYQ(Landroid/app/Activity;Lcom/android/settings/dashboard/C;)Lcom/android/settings/dashboard/D;
    .locals 1

    new-instance v0, Lcom/android/settings/language/LanguageAndInputSettings$SummaryProvider;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/language/LanguageAndInputSettings$SummaryProvider;-><init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V

    return-object v0
.end method

.method static synthetic aYR(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Lcom/android/internal/hardware/AmbientDisplayConfiguration;)Ljava/util/List;
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/android/settings/language/LanguageAndInputSettings;->aYP(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Lcom/android/internal/hardware/AmbientDisplayConfiguration;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "LanguageAndInputSettings"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f150071

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x2ee

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/language/LanguageAndInputSettings;->blw:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    invoke-direct {v0, p1}, Lcom/android/internal/hardware/AmbientDisplayConfiguration;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/language/LanguageAndInputSettings;->blw:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/language/LanguageAndInputSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/LanguageAndInputSettings;->blw:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    invoke-static {p1, v0, v1}, Lcom/android/settings/language/LanguageAndInputSettings;->aYP(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Lcom/android/internal/hardware/AmbientDisplayConfiguration;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onAttach(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/language/LanguageAndInputSettings;->IH:Lcom/android/settings/dashboard/s;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/s;->DH(I)V

    return-void
.end method

.method setAmbientDisplayConfig(Lcom/android/internal/hardware/AmbientDisplayConfiguration;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/language/LanguageAndInputSettings;->blw:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    return-void
.end method
