.class public Lcom/android/settings/language/MiuiLanguageAndInputSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiLanguageAndInputSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/hardware/input/InputManager$InputDeviceListener;
.implements Lcom/android/settings/inputmethod/KeyboardLayoutDialogFragment$OnSetupKeyboardLayoutsListener;
.implements Lcom/android/settings/search/Indexable;
.implements Lcom/android/settingslib/d/b;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;


# instance fields
.field private blA:I

.field private blB:Landroid/app/admin/DevicePolicyManager;

.field private blC:Landroid/preference/PreferenceCategory;

.field private blD:Landroid/os/Handler;

.field private blE:Landroid/preference/PreferenceCategory;

.field private final blF:Ljava/util/ArrayList;

.field private blG:Landroid/hardware/input/InputManager;

.field private blH:Landroid/view/inputmethod/InputMethodManager;

.field private final blI:Ljava/util/ArrayList;

.field private blJ:Lcom/android/settingslib/d/c;

.field private blK:Landroid/content/Intent;

.field private blL:Landroid/preference/PreferenceCategory;

.field private blM:Lmiui/preference/ValuePreference;

.field private blN:Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;

.field private blO:Z

.field private bly:Lcom/android/settings/applications/defaultapps/h;

.field private blz:Landroid/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$1;

    invoke-direct {v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    new-instance v0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$2;

    invoke-direct {v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings$2;-><init>()V

    sput-object v0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blA:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blI:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blF:Ljava/util/ArrayList;

    return-void
.end method

.method private static aYT(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/android/internal/app/LocalePicker;->getAllAssetLocales(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v1}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    invoke-virtual {v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLabel()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v0, :cond_1

    invoke-static {p0, v2, v1}, Lcom/android/settings/dc;->bYL(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private static aYU()Z
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Landroid/view/InputDevice;->getDeviceIds()[I

    move-result-object v2

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget v3, v2, v0

    invoke-static {v3}, Landroid/view/InputDevice;->getDevice(I)Landroid/view/InputDevice;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/view/InputDevice;->isVirtual()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/view/InputDevice;->getVibrator()Landroid/os/Vibrator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private aYV()Ljava/util/HashMap;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "previously_enabled_subtypes"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/d/d;->cno(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method private aYW(Landroid/view/inputmethod/InputMethodInfo;)V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYV()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, v1}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYY(Ljava/util/HashMap;)V

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v0}, Lcom/android/settingslib/d/d;->cnk(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/HashSet;)V

    return-void
.end method

.method private aYX(Landroid/view/inputmethod/InputMethodInfo;)V
    .locals 3

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blH:Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYV()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYY(Ljava/util/HashMap;)V

    return-void
.end method

.method private aYY(Ljava/util/HashMap;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p1}, Lcom/android/settingslib/d/d;->cnu(Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "previously_enabled_subtypes"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private aYZ(Landroid/hardware/input/InputDeviceIdentifier;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "keyboardLayout"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/inputmethod/KeyboardLayoutDialogFragment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/inputmethod/KeyboardLayoutDialogFragment;

    invoke-direct {v0, p1}, Lcom/android/settings/inputmethod/KeyboardLayoutDialogFragment;-><init>(Landroid/hardware/input/InputDeviceIdentifier;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/android/settings/inputmethod/KeyboardLayoutDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "keyboardLayout"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/inputmethod/KeyboardLayoutDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private aZa()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blH:Landroid/view/inputmethod/InputMethodManager;

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v2, "current_input_method"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blJ:Lcom/android/settingslib/d/c;

    invoke-virtual {v2, v0}, Lcom/android/settingslib/d/c;->cna(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    monitor-enter p0

    :try_start_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private aZb()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYU()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blC:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blC:Landroid/preference/PreferenceCategory;

    const-string/jumbo v3, "vibrate_input_devices"

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "vibrate_input_devices"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    :goto_1
    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blC:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method private aZc()V
    .locals 9

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blF:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-static {}, Landroid/view/InputDevice;->getDeviceIds()[I

    move-result-object v4

    move v0, v1

    :goto_0
    array-length v2, v4

    if-ge v0, v2, :cond_4

    aget v2, v4, v0

    invoke-static {v2}, Landroid/view/InputDevice;->getDevice(I)Landroid/view/InputDevice;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/view/InputDevice;->isVirtual()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    invoke-virtual {v5}, Landroid/view/InputDevice;->isFullKeyboard()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v5}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v6

    iget-object v2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blG:Landroid/hardware/input/InputManager;

    invoke-virtual {v2, v6}, Landroid/hardware/input/InputManager;->getCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v7, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blG:Landroid/hardware/input/InputManager;

    invoke-virtual {v7, v2}, Landroid/hardware/input/InputManager;->getKeyboardLayout(Ljava/lang/String;)Landroid/hardware/input/KeyboardLayout;

    move-result-object v2

    :goto_1
    new-instance v7, Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWz()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8, v3}, Landroid/preference/PreferenceScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {v5}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/hardware/input/KeyboardLayout;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    :goto_2
    new-instance v2, Lcom/android/settings/language/MiuiLanguageAndInputSettings$5;

    invoke-direct {v2, p0, v6}, Lcom/android/settings/language/MiuiLanguageAndInputSettings$5;-><init>(Lcom/android/settings/language/MiuiLanguageAndInputSettings;Landroid/hardware/input/InputDeviceIdentifier;)V

    invoke-virtual {v7, v2}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blF:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object v2, v3

    goto :goto_1

    :cond_3
    const v2, 0x7f12089e

    invoke-virtual {v7, v2}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blF:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    :goto_3
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getOrder()I

    move-result v3

    const/16 v4, 0x3e8

    if-ge v3, v4, :cond_5

    iget-object v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blF:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blF:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_4
    if-ge v1, v2, :cond_7

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blF:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOrder(I)V

    iget-object v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_5
    return-void

    :cond_8
    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_5
.end method

.method private aZd()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZc()V

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZb()V

    return-void
.end method

.method private aZe()V
    .locals 13

    const/4 v7, 0x0

    const/4 v12, 0x0

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blL:Landroid/preference/PreferenceCategory;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v10, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blI:Ljava/util/ArrayList;

    monitor-enter v10

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blI:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/d/a;

    iget-object v2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blL:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blB:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getPermittedInputMethodsForCurrentUser()Ljava/util/List;

    move-result-object v11

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    iget-boolean v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blO:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blJ:Lcom/android/settingslib/d/c;

    invoke-virtual {v0}, Lcom/android/settingslib/d/c;->cne()Ljava/util/List;

    move-result-object v0

    move-object v9, v0

    :goto_1
    if-nez v9, :cond_3

    move v6, v7

    :goto_2
    move v8, v7

    :goto_3
    if-ge v8, v6, :cond_5

    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    if-eqz v11, :cond_4

    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    :goto_4
    new-instance v0, Lcom/android/settingslib/d/a;

    iget-boolean v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blO:Z

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settingslib/d/a;-><init>(Landroid/content/Context;Landroid/view/inputmethod/InputMethodInfo;ZZLcom/android/settingslib/d/b;)V

    iget-object v2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blI:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_3

    :cond_2
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blH:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    move-result-object v0

    move-object v9, v0

    goto :goto_1

    :cond_3
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    move v6, v0

    goto :goto_2

    :cond_4
    const/4 v4, 0x1

    goto :goto_4

    :cond_5
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blI:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/settings/language/MiuiLanguageAndInputSettings$4;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings$4;-><init>(Lcom/android/settings/language/MiuiLanguageAndInputSettings;Ljava/text/Collator;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :goto_5
    if-ge v7, v6, :cond_6

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blI:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/d/a;

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blL:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    invoke-static {v0}, Lcom/android/settingslib/d/d;->cnj(Landroid/preference/Preference;)V

    invoke-virtual {v0}, Lcom/android/settingslib/d/a;->cmP()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    :cond_6
    monitor-exit v10

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZa()V

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blJ:Lcom/android/settingslib/d/c;

    invoke-virtual {v1}, Lcom/android/settingslib/d/c;->cne()Ljava/util/List;

    move-result-object v1

    invoke-static {p0, v0, v1, v12}, Lcom/android/settingslib/d/d;->cnv(Landroid/preference/PreferenceFragment;Landroid/content/ContentResolver;Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method private aZf(Landroid/preference/Preference;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/inputmethod/UserDictionaryList;->alG(Landroid/content/Context;)Ljava/util/TreeSet;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;-><init>(Lcom/android/settings/language/MiuiLanguageAndInputSettings;Ljava/util/TreeSet;)V

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method static synthetic aZg()Z
    .locals 1

    invoke-static {}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYU()Z

    move-result v0

    return v0
.end method

.method static synthetic aZh(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYT(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aZi(Lcom/android/settings/language/MiuiLanguageAndInputSettings;Landroid/hardware/input/InputDeviceIdentifier;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYZ(Landroid/hardware/input/InputDeviceIdentifier;)V

    return-void
.end method

.method static synthetic aZj(Lcom/android/settings/language/MiuiLanguageAndInputSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZa()V

    return-void
.end method


# virtual methods
.method public akC(Lcom/android/settingslib/d/a;)V
    .locals 4

    invoke-virtual {p1}, Lcom/android/settingslib/d/a;->cmM()Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settingslib/d/a;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYX(Landroid/view/inputmethod/InputMethodInfo;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blH:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v3

    invoke-static {p0, v2, v3, v0}, Lcom/android/settingslib/d/d;->cnn(Landroid/preference/PreferenceFragment;Landroid/content/ContentResolver;Ljava/util/List;Z)V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blJ:Lcom/android/settingslib/d/c;

    invoke-virtual {v0}, Lcom/android/settingslib/d/c;->cnb()V

    invoke-virtual {p1}, Lcom/android/settingslib/d/a;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYW(Landroid/view/inputmethod/InputMethodInfo;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blI:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/d/a;

    invoke-virtual {v0}, Lcom/android/settingslib/d/a;->cmP()V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    return-void
.end method

.method public akx(Landroid/hardware/input/InputDeviceIdentifier;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/settings/Settings$KeyboardLayoutPickerActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string/jumbo v1, "input_device_identifier"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blK:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v0

    if-eqz v0, :cond_0

    return v2

    :cond_0
    instance-of v0, p1, Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/preference/Preference;->getFragment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_2
    const-string/jumbo v0, "current_input_method"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, v2}, Landroid/view/inputmethod/InputMethodManager;->showInputMethodPicker(Z)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blC:Landroid/preference/PreferenceCategory;

    const-string/jumbo v4, "vibrate_input_devices"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "vibrate_input_devices"

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x39

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blK:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blK:Landroid/content/Intent;

    const-string/jumbo v1, "input_device_identifier"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputDeviceIdentifier;

    iput-object v2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blK:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYZ(Landroid/hardware/input/InputDeviceIdentifier;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150073

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blH:Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v1}, Lcom/android/settingslib/d/c;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blJ:Lcom/android/settingslib/d/c;

    const v0, 0x7f12085f

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blA:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v1}, Landroid/app/Activity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v2, "phone_language"

    invoke-virtual {p0, v2}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_1
    const-string/jumbo v0, "hard_keyboard"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "keyboard_settings_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blL:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "game_controller_settings_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blC:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v0, "android.settings.INPUT_METHOD_SETTINGS"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blO:Z

    iget-boolean v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blO:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blE:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blL:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blL:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blL:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    const-string/jumbo v0, "input"

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blG:Landroid/hardware/input/InputManager;

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZd()V

    const-string/jumbo v0, "spellcheckers_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {v3}, Lcom/android/settingslib/d/d;->cnj(Landroid/preference/Preference;)V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v4, Lcom/android/settings/SubSettings;

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string/jumbo v4, ":settings:show_fragment"

    const-class v5, Lcom/android/settings/inputmethod/SpellCheckersSettings;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v4, ":settings:show_fragment_title_resid"

    const v5, 0x7f121103

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    const-string/jumbo v0, "textservices"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v0}, Landroid/view/textservice/TextServicesManager;->getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;

    move-result-object v4

    invoke-virtual {v0}, Landroid/view/textservice/TextServicesManager;->isSpellCheckerEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_3

    :cond_2
    if-nez v4, :cond_8

    :cond_3
    :goto_2
    const-string/jumbo v0, "input_assistance"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_4
    const-string/jumbo v0, "input_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blD:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;

    iget-object v3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blD:Landroid/os/Handler;

    invoke-direct {v0, p0, v3, v1}, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;-><init>(Lcom/android/settings/language/MiuiLanguageAndInputSettings;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blN:Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v3, "device_policy"

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blB:Landroid/app/admin/DevicePolicyManager;

    const-string/jumbo v0, "input_device_identifier"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputDeviceIdentifier;

    iget-boolean v2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blO:Z

    if-eqz v2, :cond_6

    if-eqz v0, :cond_6

    invoke-direct {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYZ(Landroid/hardware/input/InputDeviceIdentifier;)V

    :cond_6
    const-string/jumbo v0, "default_autofill"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blz:Landroid/preference/Preference;

    new-instance v0, Lcom/android/settings/applications/defaultapps/h;

    invoke-direct {v0, v1}, Lcom/android/settings/applications/defaultapps/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bly:Lcom/android/settings/applications/defaultapps/h;

    return-void

    :cond_7
    const-string/jumbo v0, "phone_language"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blM:Lmiui/preference/ValuePreference;

    goto/16 :goto_1

    :cond_8
    array-length v0, v4

    if-nez v0, :cond_4

    goto :goto_2

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method public onInputDeviceAdded(I)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZd()V

    return-void
.end method

.method public onInputDeviceChanged(I)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZd()V

    return-void
.end method

.method public onInputDeviceRemoved(I)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZd()V

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blG:Landroid/hardware/input/InputManager;

    invoke-virtual {v0, p0}, Landroid/hardware/input/InputManager;->unregisterInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;)V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blN:Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;->pause()V

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blJ:Lcom/android/settingslib/d/c;

    invoke-virtual {v1}, Lcom/android/settingslib/d/c;->cne()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blF:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/android/settingslib/d/d;->cnn(Landroid/preference/PreferenceFragment;Landroid/content/ContentResolver;Ljava/util/List;Z)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 8

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blN:Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;->aZk()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blG:Landroid/hardware/input/InputManager;

    invoke-virtual {v0, p0, v1}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    const-string/jumbo v0, "spellcheckers_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "textservices"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v0}, Landroid/view/textservice/TextServicesManager;->isSpellCheckerEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    const v0, 0x7f12120f

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blO:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blM:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aYT(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blM:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v0}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blM:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v4}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    :cond_1
    const-string/jumbo v0, "key_user_dictionary_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_b

    new-instance v1, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const-string/jumbo v0, "key_user_dictionary_settings"

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const v0, 0x7f1213b0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(I)V

    const-string/jumbo v0, "spellcheckers_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "spellcheckers_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getOrder()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOrder(I)V

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_2
    iget-boolean v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blO:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blJ:Lcom/android/settingslib/d/c;

    invoke-virtual {v0}, Lcom/android/settingslib/d/c;->cne()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    :goto_3
    if-nez v6, :cond_8

    move v2, v3

    :goto_4
    move v5, v3

    :goto_5
    if-ge v5, v2, :cond_2

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/view/inputmethod/InputMethodInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "AOSP"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v3, v4

    :cond_2
    if-eqz v3, :cond_a

    invoke-direct {p0, v1}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZf(Landroid/preference/Preference;)V

    :cond_3
    :goto_6
    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZd()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blJ:Lcom/android/settingslib/d/c;

    invoke-virtual {v0}, Lcom/android/settingslib/d/c;->cnb()V

    invoke-direct {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZe()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bly:Lcom/android/settings/applications/defaultapps/h;

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blz:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/defaultapps/h;->cz(Landroid/preference/Preference;)V

    return-void

    :cond_4
    invoke-virtual {v0}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellChecker()Landroid/view/textservice/SpellCheckerInfo;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/textservice/SpellCheckerInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_5
    const v0, 0x7f1210fe

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blM:Lmiui/preference/ValuePreference;

    invoke-virtual {v0}, Lmiui/preference/ValuePreference;->getOrder()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->blH:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    goto :goto_3

    :cond_8
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move v2, v0

    goto :goto_4

    :cond_9
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_5

    :cond_a
    invoke-virtual {p0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_6

    :cond_b
    move-object v1, v0

    goto/16 :goto_2
.end method
