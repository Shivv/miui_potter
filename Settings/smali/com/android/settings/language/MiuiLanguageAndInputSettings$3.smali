.class final Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;
.super Ljava/lang/Object;
.source "MiuiLanguageAndInputSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic blR:Lcom/android/settings/language/MiuiLanguageAndInputSettings;

.field final synthetic blS:Ljava/util/TreeSet;


# direct methods
.method constructor <init>(Lcom/android/settings/language/MiuiLanguageAndInputSettings;Ljava/util/TreeSet;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;->blR:Lcom/android/settings/language/MiuiLanguageAndInputSettings;

    iput-object p2, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;->blS:Ljava/util/TreeSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v3, -0x1

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;->blS:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    if-gt v0, v6, :cond_1

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;->blS:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v1, "locale"

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;->blS:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-class v0, Lcom/android/settings/UserDictionarySettings;

    move-object v2, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;->blR:Lcom/android/settings/language/MiuiLanguageAndInputSettings;

    iget-object v1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$3;->blR:Lcom/android/settings/language/MiuiLanguageAndInputSettings;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    return v6

    :cond_1
    const-class v0, Lcom/android/settings/inputmethod/UserDictionaryList;

    move-object v2, v0

    goto :goto_0
.end method
