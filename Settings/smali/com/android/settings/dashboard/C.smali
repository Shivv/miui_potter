.class public Lcom/android/settings/dashboard/C;
.super Ljava/lang/Object;
.source "SummaryLoader.java"


# static fields
.field public static final SUMMARY_PROVIDER_FACTORY:Ljava/lang/String; = "SUMMARY_PROVIDER_FACTORY"


# instance fields
.field private final Ld:Landroid/app/Activity;

.field private final Le:Ljava/lang/String;

.field private final Lf:Lcom/android/settings/dashboard/n;

.field private final Lg:Landroid/os/Handler;

.field private Lh:Z

.field private Li:Landroid/util/ArraySet;

.field private Lj:Lcom/android/settings/dashboard/E;

.field private final Lk:Landroid/util/ArrayMap;

.field private final Ll:Lcom/android/settings/dashboard/G;

.field private Lm:Z

.field private final Ln:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/C;->Lk:Landroid/util/ArrayMap;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/C;->Li:Landroid/util/ArraySet;

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/C;->Lf:Lcom/android/settings/dashboard/n;

    iput-object p2, p0, Lcom/android/settings/dashboard/C;->Le:Ljava/lang/String;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/C;->Lg:Landroid/os/Handler;

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "SummaryLoader"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/settings/dashboard/C;->Ln:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Ln:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/settings/dashboard/G;

    iget-object v1, p0, Lcom/android/settings/dashboard/C;->Ln:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/G;-><init>(Lcom/android/settings/dashboard/C;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/C;->Ll:Lcom/android/settings/dashboard/G;

    iput-object p1, p0, Lcom/android/settings/dashboard/C;->Ld:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Lf:Lcom/android/settings/dashboard/n;

    invoke-interface {v0, p2}, Lcom/android/settings/dashboard/n;->De(Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v2, p0, Lcom/android/settings/dashboard/C;->Ll:Lcom/android/settings/dashboard/G;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lcom/android/settings/dashboard/G;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private Fe(Lcom/android/settingslib/drawer/Tile;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p1, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    return-object v0
.end method

.method private Ff(Lcom/android/settingslib/drawer/Tile;)Lcom/android/settings/dashboard/D;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Ld:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object v2

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/C;->Fe(Lcom/android/settingslib/drawer/Tile;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v2

    :cond_1
    const-string/jumbo v1, "com.android.settings.FRAGMENT_CLASS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    return-object v2

    :cond_2
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "SUMMARY_PROVIDER_FACTORY"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/F;

    iget-object v1, p0, Lcom/android/settings/dashboard/C;->Ld:Landroid/app/Activity;

    invoke-interface {v0, v1, p0}, Lcom/android/settings/dashboard/F;->kP(Landroid/app/Activity;Lcom/android/settings/dashboard/C;)Lcom/android/settings/dashboard/D;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    :goto_0
    return-object v2

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method private Fg(Lcom/android/settingslib/drawer/DashboardCategory;Landroid/content/ComponentName;)Lcom/android/settingslib/drawer/Tile;
    .locals 5

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    if-nez v0, :cond_1

    :cond_0
    return-object v4

    :cond_1
    iget-object v0, p1, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    iget-object v0, p1, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v3, v0, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    return-object v0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-object v4
.end method

.method private declared-synchronized Fh(Lcom/android/settingslib/drawer/Tile;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/C;->Ff(Lcom/android/settingslib/drawer/Tile;)Lcom/android/settings/dashboard/D;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/dashboard/C;->Lk:Landroid/util/ArrayMap;

    iget-object v2, p1, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized Fi(Z)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/settings/dashboard/C;->Lm:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput-boolean p1, p0, Lcom/android/settings/dashboard/C;->Lm:Z

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Lk:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-interface {v0, p1}, Lcom/android/settings/dashboard/D;->jt(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string/jumbo v2, "SummaryLoader"

    const-string/jumbo v3, "Problem in setListening"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method static synthetic Fj(Lcom/android/settings/dashboard/C;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Le:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Fk(Lcom/android/settings/dashboard/C;)Lcom/android/settings/dashboard/n;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Lf:Lcom/android/settings/dashboard/n;

    return-object v0
.end method

.method static synthetic Fl(Lcom/android/settings/dashboard/C;Lcom/android/settingslib/drawer/DashboardCategory;Landroid/content/ComponentName;)Lcom/android/settingslib/drawer/Tile;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/dashboard/C;->Fg(Lcom/android/settingslib/drawer/DashboardCategory;Landroid/content/ComponentName;)Lcom/android/settingslib/drawer/Tile;

    move-result-object v0

    return-object v0
.end method

.method static synthetic Fm(Lcom/android/settings/dashboard/C;Lcom/android/settingslib/drawer/Tile;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/C;->Fh(Lcom/android/settingslib/drawer/Tile;)V

    return-void
.end method

.method static synthetic Fn(Lcom/android/settings/dashboard/C;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/C;->Fi(Z)V

    return-void
.end method


# virtual methods
.method public Fa(Z)V
    .locals 5

    const/4 v4, 0x2

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settings/dashboard/C;->Lh:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/settings/dashboard/C;->Lh:Z

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Li:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/android/settings/dashboard/C;->Ld:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Li:Landroid/util/ArraySet;

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Li:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->clear()V

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Ll:Lcom/android/settings/dashboard/G;

    invoke-virtual {v0, v4}, Lcom/android/settings/dashboard/G;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/settings/dashboard/C;->Ll:Lcom/android/settings/dashboard/G;

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v4, v0, v2}, Lcom/android/settings/dashboard/G;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public Fb()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Ln:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/C;->Fi(Z)V

    return-void
.end method

.method public Fc(Lcom/android/settings/dashboard/E;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/C;->Lj:Lcom/android/settings/dashboard/E;

    return-void
.end method

.method public Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Lk:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/android/settings/dashboard/C;->Lg:Landroid/os/Handler;

    new-instance v2, Lcom/android/settings/dashboard/L;

    invoke-direct {v2, p0, v0, p2}, Lcom/android/settings/dashboard/L;-><init>(Lcom/android/settings/dashboard/C;Landroid/content/ComponentName;Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method updateSummaryIfNeeded(Lcom/android/settingslib/drawer/Tile;Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p1, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p2, p1, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Lj:Lcom/android/settings/dashboard/E;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/C;->Lj:Lcom/android/settings/dashboard/E;

    invoke-interface {v0, p1}, Lcom/android/settings/dashboard/E;->BZ(Lcom/android/settingslib/drawer/Tile;)V

    :cond_1
    return-void
.end method
