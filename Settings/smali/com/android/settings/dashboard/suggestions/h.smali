.class public Lcom/android/settings/dashboard/suggestions/h;
.super Ljava/lang/Object;
.source "SuggestionRanker.java"


# static fields
.field private static final Iw:Ljava/util/Map;


# instance fields
.field private final Ix:Lcom/android/settings/dashboard/suggestions/c;

.field private final Iy:Ljava/util/Map;

.field Iz:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/dashboard/suggestions/SuggestionRanker$1;

    invoke-direct {v0}, Lcom/android/settings/dashboard/suggestions/SuggestionRanker$1;-><init>()V

    sput-object v0, Lcom/android/settings/dashboard/suggestions/h;->Iw:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/dashboard/suggestions/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/dashboard/suggestions/j;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/suggestions/j;-><init>(Lcom/android/settings/dashboard/suggestions/h;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/suggestions/h;->Iz:Ljava/util/Comparator;

    iput-object p1, p0, Lcom/android/settings/dashboard/suggestions/h;->Ix:Lcom/android/settings/dashboard/suggestions/c;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/suggestions/h;->Iy:Ljava/util/Map;

    return-void
.end method

.method static synthetic BS(Lcom/android/settings/dashboard/suggestions/h;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/h;->Iy:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public BR(Ljava/util/List;Ljava/util/List;)V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/h;->Iy:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/h;->Ix:Lcom/android/settings/dashboard/suggestions/c;

    invoke-virtual {v0, p2}, Lcom/android/settings/dashboard/suggestions/c;->BB(Ljava/util/List;)Ljava/util/Map;

    move-result-object v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-object v4, p0, Lcom/android/settings/dashboard/suggestions/h;->Iy:Ljava/util/Map;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-virtual {p0, v1}, Lcom/android/settings/dashboard/suggestions/h;->getRelevanceMetric(Ljava/util/Map;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/h;->Iz:Ljava/util/Comparator;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method getRelevanceMetric(Ljava/util/Map;)D
    .locals 8

    const-wide/16 v0, 0x0

    if-nez p1, :cond_0

    return-wide v0

    :cond_0
    sget-object v2, Lcom/android/settings/dashboard/suggestions/h;->Iw:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/android/settings/dashboard/suggestions/h;->Iw:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    mul-double/2addr v0, v6

    add-double/2addr v0, v2

    move-wide v2, v0

    goto :goto_0

    :cond_1
    return-wide v2
.end method
