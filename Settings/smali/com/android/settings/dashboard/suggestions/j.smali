.class final Lcom/android/settings/dashboard/suggestions/j;
.super Ljava/lang/Object;
.source "SuggestionRanker.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic IB:Lcom/android/settings/dashboard/suggestions/h;


# direct methods
.method constructor <init>(Lcom/android/settings/dashboard/suggestions/h;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/suggestions/j;->IB:Lcom/android/settings/dashboard/suggestions/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public BU(Lcom/android/settingslib/drawer/Tile;Lcom/android/settingslib/drawer/Tile;)I
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/j;->IB:Lcom/android/settings/dashboard/suggestions/h;

    invoke-static {v0}, Lcom/android/settings/dashboard/suggestions/h;->BS(Lcom/android/settings/dashboard/suggestions/h;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/j;->IB:Lcom/android/settings/dashboard/suggestions/h;

    invoke-static {v0}, Lcom/android/settings/dashboard/suggestions/h;->BS(Lcom/android/settings/dashboard/suggestions/h;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    cmpg-double v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settingslib/drawer/Tile;

    check-cast p2, Lcom/android/settingslib/drawer/Tile;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/dashboard/suggestions/j;->BU(Lcom/android/settingslib/drawer/Tile;Lcom/android/settingslib/drawer/Tile;)I

    move-result v0

    return v0
.end method
