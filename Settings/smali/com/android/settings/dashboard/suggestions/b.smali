.class public Lcom/android/settings/dashboard/suggestions/b;
.super Ljava/lang/Object;
.source "SuggestionFeatureProviderImpl.java"

# interfaces
.implements Lcom/android/settings/dashboard/suggestions/a;


# instance fields
.field private final In:Lcom/android/settings/dashboard/suggestions/h;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/android/settings/dashboard/suggestions/h;

    new-instance v2, Lcom/android/settings/dashboard/suggestions/c;

    new-instance v3, Lcom/android/settings/dashboard/suggestions/e;

    invoke-direct {v3, v0}, Lcom/android/settings/dashboard/suggestions/e;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v3}, Lcom/android/settings/dashboard/suggestions/c;-><init>(Lcom/android/settings/dashboard/suggestions/e;)V

    invoke-direct {v1, v2}, Lcom/android/settings/dashboard/suggestions/h;-><init>(Lcom/android/settings/dashboard/suggestions/c;)V

    iput-object v1, p0, Lcom/android/settings/dashboard/suggestions/b;->In:Lcom/android/settings/dashboard/suggestions/h;

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/suggestions/b;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-void
.end method


# virtual methods
.method public Bu(Landroid/content/Context;Lcom/android/settingslib/z;Lcom/android/settingslib/drawer/Tile;)V
    .locals 4

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/b;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0, p1, p3}, Lcom/android/settings/dashboard/suggestions/b;->Bv(Landroid/content/Context;Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/util/Pair;

    const/16 v3, 0x183

    invoke-virtual {v0, p1, v3, v1, v2}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/suggestions/b;->Bx(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p2, p3, v0}, Lcom/android/settingslib/z;->cru(Lcom/android/settingslib/drawer/Tile;Z)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p3, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    iget-object v0, p3, Lcom/android/settingslib/drawer/Tile;->category:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/android/settingslib/z;->crB(Ljava/lang/String;)V

    return-void
.end method

.method public Bv(Landroid/content/Context;Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;
    .locals 2

    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, "unknown_suggestion"

    return-object v0

    :cond_1
    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public Bw(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public Bx(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public By(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public Bz(Ljava/util/List;Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/b;->In:Lcom/android/settings/dashboard/suggestions/h;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/dashboard/suggestions/h;->BR(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method
