.class public Lcom/android/settings/dashboard/e;
.super Ljava/lang/Object;
.source "DashboardData.java"


# instance fields
.field private Jn:Ljava/util/List;

.field private Jo:Ljava/util/List;

.field private Jp:Lcom/android/settings/dashboard/conditional/e;

.field private Jq:I

.field private Jr:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/dashboard/e;->Jq:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/dashboard/e;->Jp:Lcom/android/settings/dashboard/conditional/e;

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/dashboard/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/dashboard/e;->Jq:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/dashboard/e;->Jp:Lcom/android/settings/dashboard/conditional/e;

    invoke-static {p1}, Lcom/android/settings/dashboard/d;->CJ(Lcom/android/settings/dashboard/d;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/e;->Jn:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/dashboard/d;->CK(Lcom/android/settings/dashboard/d;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/e;->Jo:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/dashboard/d;->CN(Lcom/android/settings/dashboard/d;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/e;->Jr:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/dashboard/d;->CM(Lcom/android/settings/dashboard/d;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/dashboard/e;->Jq:I

    invoke-static {p1}, Lcom/android/settings/dashboard/d;->CL(Lcom/android/settings/dashboard/d;)Lcom/android/settings/dashboard/conditional/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/e;->Jp:Lcom/android/settings/dashboard/conditional/e;

    return-void
.end method

.method static synthetic CT(Lcom/android/settings/dashboard/e;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/e;->Jn:Ljava/util/List;

    return-object v0
.end method

.method static synthetic CU(Lcom/android/settings/dashboard/e;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/e;->Jo:Ljava/util/List;

    return-object v0
.end method

.method static synthetic CV(Lcom/android/settings/dashboard/e;)Lcom/android/settings/dashboard/conditional/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/e;->Jp:Lcom/android/settings/dashboard/conditional/e;

    return-object v0
.end method

.method static synthetic CW(Lcom/android/settings/dashboard/e;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/e;->Jq:I

    return v0
.end method

.method static synthetic CX(Lcom/android/settings/dashboard/e;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/e;->Jr:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public CO(Ljava/util/List;)Lcom/android/settings/dashboard/e;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/e;->Jn:Ljava/util/List;

    return-object p0
.end method

.method public CP(Ljava/util/List;)Lcom/android/settings/dashboard/e;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/e;->Jo:Ljava/util/List;

    return-object p0
.end method

.method public CQ(Lcom/android/settings/dashboard/conditional/e;)Lcom/android/settings/dashboard/e;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/e;->Jp:Lcom/android/settings/dashboard/conditional/e;

    return-object p0
.end method

.method public CR(I)Lcom/android/settings/dashboard/e;
    .locals 0

    iput p1, p0, Lcom/android/settings/dashboard/e;->Jq:I

    return-object p0
.end method

.method public CS(Ljava/util/List;)Lcom/android/settings/dashboard/e;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/e;->Jr:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/android/settings/dashboard/d;
    .locals 2

    new-instance v0, Lcom/android/settings/dashboard/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/d;-><init>(Lcom/android/settings/dashboard/e;Lcom/android/settings/dashboard/d;)V

    return-object v0
.end method
