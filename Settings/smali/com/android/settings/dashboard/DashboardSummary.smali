.class public Lcom/android/settings/dashboard/DashboardSummary;
.super Lcom/android/settings/core/InstrumentedFragment;
.source "DashboardSummary.java"

# interfaces
.implements Lcom/android/settingslib/drawer/b;
.implements Lcom/android/settings/dashboard/conditional/j;
.implements Lcom/android/settings/dashboard/conditional/p;


# instance fields
.field private IK:Z

.field private IL:Lcom/android/settings/dashboard/o;

.field private IM:Lcom/android/settings/dashboard/conditional/h;

.field private IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

.field private IO:Lcom/android/settings/dashboard/n;

.field private final IP:Landroid/os/Handler;

.field private IQ:Landroid/support/v7/widget/al;

.field private IR:Lcom/android/settings/dashboard/suggestions/f;

.field private IS:Lcom/android/settings/dashboard/suggestions/a;

.field private IT:Lcom/android/settingslib/z;

.field private IU:Lcom/android/settings/dashboard/suggestions/d;

.field private IV:Lcom/android/settings/dashboard/C;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/core/InstrumentedFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IP:Landroid/os/Handler;

    return-void
.end method

.method static synthetic Cg(Lcom/android/settings/dashboard/DashboardSummary;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IP:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic Ch(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settings/dashboard/suggestions/a;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IS:Lcom/android/settings/dashboard/suggestions/a;

    return-object v0
.end method

.method static synthetic Ci(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settingslib/z;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IT:Lcom/android/settingslib/z;

    return-object v0
.end method

.method static synthetic Cj(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settings/dashboard/suggestions/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IU:Lcom/android/settings/dashboard/suggestions/d;

    return-object v0
.end method


# virtual methods
.method public Bl()V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string/jumbo v2, "DashboardSummary"

    const-string/jumbo v3, "onConditionsChanged"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/settings/dashboard/DashboardSummary;->IQ:Landroid/support/v7/widget/al;

    invoke-virtual {v2}, Landroid/support/v7/widget/al;->dxY()I

    move-result v2

    if-gt v2, v0, :cond_1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    iget-object v3, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v3}, Lcom/android/settings/dashboard/conditional/h;->AT()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/dashboard/o;->Dl(Ljava/util/List;)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IP:Landroid/os/Handler;

    new-instance v2, Lcom/android/settings/dashboard/-$Lambda$hpQHAEqSIVVBUshEF8GcoDA4AVw;

    invoke-direct {v2, v1, p0}, Lcom/android/settings/dashboard/-$Lambda$hpQHAEqSIVVBUshEF8GcoDA4AVw;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public Ca()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IK:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->rebuildUI()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IK:Z

    return-void
.end method

.method synthetic Ce()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/DashboardSummary;->updateCategoryAndSuggestion(Ljava/util/List;)V

    return-void
.end method

.method synthetic Cf()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->dqo(I)V

    :cond_0
    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x23

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/overlay/a;->aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IO:Lcom/android/settings/dashboard/n;

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/overlay/a;->aIv(Landroid/content/Context;)Lcom/android/settings/dashboard/suggestions/a;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IS:Lcom/android/settings/dashboard/suggestions/a;

    new-instance v1, Lcom/android/settings/dashboard/C;

    const-string/jumbo v2, "com.android.settings.category.ia.homepage"

    invoke-direct {v1, v0, v2}, Lcom/android/settings/dashboard/C;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IV:Lcom/android/settings/dashboard/C;

    invoke-static {v0, v3}, Lcom/android/settings/dashboard/conditional/h;->AS(Landroid/content/Context;Z)Lcom/android/settings/dashboard/conditional/h;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getLifecycle()Lcom/android/settings/core/lifecycle/a;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v1, v2}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    new-instance v1, Lcom/android/settingslib/z;

    const-string/jumbo v2, "suggestions"

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const v3, 0x7f1500ef

    invoke-direct {v1, v0, v2, v3}, Lcom/android/settingslib/z;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;I)V

    iput-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IT:Lcom/android/settingslib/z;

    new-instance v0, Lcom/android/settings/dashboard/suggestions/d;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/dashboard/suggestions/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IU:Lcom/android/settings/dashboard/suggestions/d;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IV:Lcom/android/settings/dashboard/C;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/C;->Fb()V

    invoke-super {p0}, Lcom/android/settings/core/InstrumentedFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/core/InstrumentedFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/drawer/a;->cdN(Lcom/android/settingslib/drawer/b;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IV:Lcom/android/settings/dashboard/C;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/C;->Fa(Z)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->AT()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->AK()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/dashboard/DashboardSummary;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->AA()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/android/settings/core/instrumentation/e;->ajX(Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/o;->onPause()V

    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    invoke-super {p0}, Lcom/android/settings/core/InstrumentedFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/drawer/a;->cdM(Lcom/android/settingslib/drawer/b;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IV:Lcom/android/settings/dashboard/C;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/C;->Fa(Z)V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getMetricsCategory()I

    move-result v1

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->AT()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->AK()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/dashboard/DashboardSummary;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->AA()I

    move-result v0

    invoke-virtual {v3, v4, v1, v0}, Lcom/android/settings/core/instrumentation/e;->ajW(Landroid/content/Context;II)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IQ:Landroid/support/v7/widget/al;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string/jumbo v0, "scroll_position"

    iget-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IQ:Landroid/support/v7/widget/al;

    invoke-virtual {v1}, Landroid/support/v7/widget/al;->dyC()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    invoke-virtual {v0, p1}, Lcom/android/settings/dashboard/o;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    const v0, 0x7f0a0116

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    iput-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    new-instance v0, Landroid/support/v7/widget/al;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IQ:Landroid/support/v7/widget/al;

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IQ:Landroid/support/v7/widget/al;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/al;->setOrientation(I)V

    if-eqz p2, :cond_0

    const-string/jumbo v0, "scroll_position"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IQ:Landroid/support/v7/widget/al;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/al;->dsA(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    iget-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IQ:Landroid/support/v7/widget/al;

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->setLayoutManager(Landroid/support/v7/widget/a;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    invoke-virtual {v0, v2}, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->setHasFixedSize(Z)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    new-instance v1, Lcom/android/settings/dashboard/c;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/settings/dashboard/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->dpM(Landroid/support/v7/widget/d;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    invoke-virtual {v0, p0}, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->setListener(Lcom/android/settings/dashboard/conditional/p;)V

    const-string/jumbo v0, "DashboardSummary"

    const-string/jumbo v1, "adapter created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settings/dashboard/o;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v2}, Lcom/android/settings/dashboard/conditional/h;->AT()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, p2, v2}, Lcom/android/settings/dashboard/o;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/util/List;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    iget-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->setAdapter(Landroid/support/v7/widget/b;)V

    new-instance v0, Lcom/android/settings/dashboard/suggestions/f;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    iget-object v3, p0, Lcom/android/settings/dashboard/DashboardSummary;->IT:Lcom/android/settingslib/z;

    iget-object v4, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/settings/dashboard/suggestions/f;-><init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;Lcom/android/settingslib/z;Lcom/android/settings/dashboard/suggestions/g;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IR:Lcom/android/settings/dashboard/suggestions/f;

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    new-instance v1, Lcom/android/settings/dashboard/j;

    invoke-direct {v1}, Lcom/android/settings/dashboard/j;-><init>()V

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->setItemAnimator(Landroid/support/v7/widget/u;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IV:Lcom/android/settings/dashboard/C;

    iget-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/C;->Fc(Lcom/android/settings/dashboard/E;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IN:Lcom/android/settings/dashboard/conditional/FocusRecyclerView;

    invoke-static {v0}, Lcom/android/settings/dashboard/conditional/q;->Bm(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->rebuildUI()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    if-eqz p1, :cond_0

    const-string/jumbo v0, "DashboardSummary"

    const-string/jumbo v1, "Listening for condition changes"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0, p0}, Lcom/android/settings/dashboard/conditional/h;->AU(Lcom/android/settings/dashboard/conditional/j;)V

    const-string/jumbo v0, "DashboardSummary"

    const-string/jumbo v1, "conditions refreshed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->AV()V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "DashboardSummary"

    const-string/jumbo v1, "Stopped listening for condition changes"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IM:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0, p0}, Lcom/android/settings/dashboard/conditional/h;->AW(Lcom/android/settings/dashboard/conditional/j;)V

    goto :goto_0
.end method

.method rebuildUI()V
    .locals 4

    new-instance v0, Lcom/android/settings/dashboard/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/b;-><init>(Lcom/android/settings/dashboard/DashboardSummary;Lcom/android/settings/dashboard/b;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/settings/dashboard/DashboardSummary;->IP:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/dashboard/-$Lambda$hpQHAEqSIVVBUshEF8GcoDA4AVw;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p0}, Lcom/android/settings/dashboard/-$Lambda$hpQHAEqSIVVBUshEF8GcoDA4AVw;-><init>(BLjava/lang/Object;)V

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method updateCategoryAndSuggestion(Ljava/util/List;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/dashboard/DashboardSummary;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IO:Lcom/android/settings/dashboard/n;

    const-string/jumbo v2, "com.android.settings.category.ia.homepage"

    invoke-interface {v1, v2}, Lcom/android/settings/dashboard/n;->De(Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    invoke-virtual {v1, v0, p1}, Lcom/android/settings/dashboard/o;->Dm(Ljava/util/List;Ljava/util/List;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/dashboard/DashboardSummary;->IL:Lcom/android/settings/dashboard/o;

    invoke-virtual {v1, v0}, Lcom/android/settings/dashboard/o;->Dn(Ljava/util/List;)V

    goto :goto_0
.end method
