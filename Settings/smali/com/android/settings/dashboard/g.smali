.class Lcom/android/settings/dashboard/g;
.super Ljava/lang/Object;
.source "DashboardData.java"


# instance fields
.field public final Ju:Z

.field public final Jv:Ljava/lang/Object;

.field public final Jw:I

.field public final type:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;IIZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    iput p2, p0, Lcom/android/settings/dashboard/g;->type:I

    iput p3, p0, Lcom/android/settings/dashboard/g;->Jw:I

    iput-boolean p4, p0, Lcom/android/settings/dashboard/g;->Ju:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/android/settings/dashboard/g;

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/android/settings/dashboard/g;

    iget v1, p0, Lcom/android/settings/dashboard/g;->type:I

    iget v3, p1, Lcom/android/settings/dashboard/g;->type:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/android/settings/dashboard/g;->Jw:I

    iget v3, p1, Lcom/android/settings/dashboard/g;->Jw:I

    if-eq v1, v3, :cond_3

    :cond_2
    return v2

    :cond_3
    iget v1, p0, Lcom/android/settings/dashboard/g;->type:I

    sparse-switch v1, :sswitch_data_0

    :cond_4
    iget-object v1, p0, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    if-nez v1, :cond_7

    iget-object v1, p1, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    if-nez v1, :cond_5

    move v2, v0

    :cond_5
    :goto_0
    return v2

    :sswitch_0
    iget-object v0, p0, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    check-cast v0, Lcom/android/settingslib/drawer/DashboardCategory;

    iget-object v1, v0, Lcom/android/settingslib/drawer/DashboardCategory;->title:Ljava/lang/CharSequence;

    iget-object v0, p1, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    check-cast v0, Lcom/android/settingslib/drawer/DashboardCategory;

    iget-object v0, v0, Lcom/android/settingslib/drawer/DashboardCategory;->title:Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0

    :sswitch_1
    iget-object v0, p0, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v1, p1, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    check-cast v1, Lcom/android/settingslib/drawer/Tile;

    iget-object v3, v0, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    iget-object v4, v1, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v0, v0, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    iget-object v1, v1, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    :goto_1
    return v0

    :cond_6
    move v0, v2

    goto :goto_1

    :sswitch_2
    iget-boolean v1, p0, Lcom/android/settings/dashboard/g;->Ju:Z

    iget-boolean v3, p1, Lcom/android/settings/dashboard/g;->Ju:Z

    if-eq v1, v3, :cond_4

    return v2

    :cond_7
    iget-object v0, p0, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    iget-object v1, p1, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0d0062 -> :sswitch_2
        0x7f0d0086 -> :sswitch_0
        0x7f0d0088 -> :sswitch_1
    .end sparse-switch
.end method
