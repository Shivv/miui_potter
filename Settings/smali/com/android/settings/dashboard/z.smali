.class Lcom/android/settings/dashboard/z;
.super Lcom/android/settings/dashboard/y;
.source "SupportItemAdapter.java"


# instance fields
.field private KL:Z

.field private KM:Z

.field private KN:Ljava/lang/CharSequence;

.field private KO:Ljava/lang/CharSequence;

.field private KP:I

.field private KQ:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    const v0, 0x7f0d01f7

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/z;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/dashboard/y;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic EE(Lcom/android/settings/dashboard/z;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/dashboard/z;->KL:Z

    return v0
.end method

.method static synthetic EF(Lcom/android/settings/dashboard/z;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/dashboard/z;->KM:Z

    return v0
.end method

.method static synthetic EG(Lcom/android/settings/dashboard/z;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/z;->KN:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic EH(Lcom/android/settings/dashboard/z;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/z;->KO:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic EI(Lcom/android/settings/dashboard/z;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/z;->KP:I

    return v0
.end method

.method static synthetic EJ(Lcom/android/settings/dashboard/z;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/z;->KQ:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method EA(Ljava/lang/String;)Lcom/android/settings/dashboard/z;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/z;->KN:Ljava/lang/CharSequence;

    return-object p0
.end method

.method EB(Ljava/lang/String;)Lcom/android/settings/dashboard/z;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/z;->KO:Ljava/lang/CharSequence;

    return-object p0
.end method

.method EC(I)Lcom/android/settings/dashboard/z;
    .locals 0

    iput p1, p0, Lcom/android/settings/dashboard/z;->KP:I

    return-object p0
.end method

.method ED(I)Lcom/android/settings/dashboard/z;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/z;->KQ:Ljava/lang/CharSequence;

    return-object p0
.end method

.method Ey(Z)Lcom/android/settings/dashboard/z;
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/dashboard/z;->KL:Z

    return-object p0
.end method

.method Ez(Z)Lcom/android/settings/dashboard/z;
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/dashboard/z;->KM:Z

    return-object p0
.end method

.method build()Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;
    .locals 2

    new-instance v0, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;-><init>(Lcom/android/settings/dashboard/z;Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;)V

    return-object v0
.end method

.method bridge synthetic build()Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/z;->build()Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;

    move-result-object v0

    return-object v0
.end method
