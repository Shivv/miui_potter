.class public Lcom/android/settings/dashboard/k;
.super Ljava/lang/Object;
.source "DashboardFeatureProviderImpl.java"

# interfaces
.implements Lcom/android/settings/dashboard/n;


# instance fields
.field private final JA:Lcom/android/settingslib/drawer/e;

.field protected final mContext:Landroid/content/Context;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/k;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/dashboard/k;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/k;->Dc()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settingslib/drawer/e;->cdY(Landroid/content/Context;Ljava/lang/String;)Lcom/android/settingslib/drawer/e;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/k;->JA:Lcom/android/settingslib/drawer/e;

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/k;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/k;->mPackageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method private Df(Landroid/content/Intent;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/dashboard/k;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private Dg(Landroid/app/Activity;Lcom/android/settingslib/drawer/Tile;Landroid/content/Intent;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p3}, Lcom/android/settings/dashboard/k;->Df(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "DashboardFeatureImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot resolve intent, skipping. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/k;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/android/settingslib/drawer/ProfileSelectDialog;->ceH(Landroid/content/Context;Lcom/android/settingslib/drawer/Tile;)V

    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->czv:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/k;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v1, p0, Lcom/android/settings/dashboard/k;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p3, p4}, Lcom/android/settings/core/instrumentation/e;->ajR(Landroid/content/Context;Landroid/content/Intent;I)V

    invoke-virtual {p1, p3, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->czv:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/dashboard/k;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v1, p0, Lcom/android/settings/dashboard/k;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p3, p4}, Lcom/android/settings/core/instrumentation/e;->ajR(Landroid/content/Context;Landroid/content/Intent;I)V

    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->czv:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserHandle;

    invoke-virtual {p1, p3, v2, v0}, Landroid/app/Activity;->startActivityForResultAsUser(Landroid/content/Intent;ILandroid/os/UserHandle;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settingslib/drawer/ProfileSelectDialog;->ceI(Landroid/app/FragmentManager;Lcom/android/settingslib/drawer/Tile;)V

    goto :goto_0
.end method


# virtual methods
.method public CZ(Landroid/app/Activity;ILandroid/preference/Preference;Lcom/android/settingslib/drawer/Tile;Ljava/lang/String;I)V
    .locals 7

    const/4 v6, 0x0

    const/4 v0, 0x0

    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p3, p5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    if-eqz v1, :cond_5

    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->czu:Landroid/graphics/drawable/Icon;

    if-eqz v1, :cond_0

    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->czu:Landroid/graphics/drawable/Icon;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Icon;->loadDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v2, p4, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    if-eqz v2, :cond_a

    const-string/jumbo v0, "com.android.settings.FRAGMENT_CLASS"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "com.android.settings.intent.action"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {p3, v1}, Landroid/preference/Preference;->setFragment(Ljava/lang/String;)V

    :cond_1
    :goto_3
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget v1, p4, Lcom/android/settingslib/drawer/Tile;->czt:I

    if-eqz v1, :cond_3

    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_9

    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    :goto_4
    if-nez v0, :cond_2

    const v0, 0x7fffffff

    if-ne p6, v0, :cond_8

    :cond_2
    iget v0, p4, Lcom/android/settingslib/drawer/Tile;->czt:I

    neg-int v0, v0

    invoke-virtual {p3, v0}, Landroid/preference/Preference;->setOrder(I)V

    :cond_3
    :goto_5
    return-void

    :cond_4
    invoke-virtual {p0, p4}, Lcom/android/settings/dashboard/k;->Db(Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const v1, 0x7f1211e8

    invoke-virtual {p3, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_1

    :cond_6
    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_1

    new-instance v5, Landroid/content/Intent;

    iget-object v1, p4, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-direct {v5, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string/jumbo v1, ":settings:source_metrics"

    invoke-virtual {v5, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz v0, :cond_7

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :cond_7
    new-instance v0, Lcom/android/settings/dashboard/-$Lambda$SoaD6gR9iKBkQp9sNyh54Sf1d7o;

    move v1, p2

    move-object v2, p0

    move-object v3, p1

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/dashboard/-$Lambda$SoaD6gR9iKBkQp9sNyh54Sf1d7o;-><init>(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p3, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_3

    :cond_8
    iget v0, p4, Lcom/android/settingslib/drawer/Tile;->czt:I

    neg-int v0, v0

    add-int/2addr v0, p6

    invoke-virtual {p3, v0}, Landroid/preference/Preference;->setOrder(I)V

    goto :goto_5

    :cond_9
    move v0, v6

    goto :goto_4

    :cond_a
    move-object v1, v0

    goto :goto_2
.end method

.method public Da()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/k;->JA:Lcom/android/settingslib/drawer/e;

    iget-object v1, p0, Lcom/android/settings/dashboard/k;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/drawer/e;->cdX(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public Db(Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    if-nez v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p1, Lcom/android/settingslib/drawer/Tile;->key:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/android/settingslib/drawer/Tile;->key:Ljava/lang/String;

    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "dashboard_tile_pref_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public Dc()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public Dd(Landroid/content/Context;Lcom/android/settings/dashboard/MiuiDashboardFragment;Landroid/os/Bundle;)Lcom/android/settings/dashboard/s;
    .locals 2

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    const-string/jumbo v1, ":settings:fragment_args_key"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    new-instance v1, Lcom/android/settings/dashboard/s;

    invoke-direct {v1, p1, p2, v0}, Lcom/android/settings/dashboard/s;-><init>(Landroid/content/Context;Landroid/preference/PreferenceFragment;Z)V

    return-object v1
.end method

.method public De(Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/k;->JA:Lcom/android/settingslib/drawer/e;

    iget-object v1, p0, Lcom/android/settings/dashboard/k;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/android/settingslib/drawer/e;->cee(Landroid/content/Context;Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;

    move-result-object v0

    return-object v0
.end method

.method public Dh(Landroid/app/Activity;Lcom/android/settingslib/drawer/Tile;)V
    .locals 5

    const v4, 0x8000

    const/16 v3, 0x23

    if-nez p2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/k;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p2, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string/jumbo v1, ":settings:source_metrics"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "show_drawer_menu"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, v3}, Lcom/android/settings/dashboard/k;->Dg(Landroid/app/Activity;Lcom/android/settingslib/drawer/Tile;Landroid/content/Intent;I)V

    return-void
.end method

.method synthetic Di(Landroid/app/Activity;Lcom/android/settingslib/drawer/Tile;Landroid/content/Intent;ILandroid/preference/Preference;)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/dashboard/k;->Dg(Landroid/app/Activity;Lcom/android/settingslib/drawer/Tile;Landroid/content/Intent;I)V

    const/4 v0, 0x1

    return v0
.end method
