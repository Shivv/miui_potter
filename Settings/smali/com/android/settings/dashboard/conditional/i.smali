.class Lcom/android/settings/dashboard/conditional/i;
.super Landroid/os/AsyncTask;
.source "ConditionManager.java"


# instance fields
.field final synthetic Id:Lcom/android/settings/dashboard/conditional/h;


# direct methods
.method private constructor <init>(Lcom/android/settings/dashboard/conditional/h;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/dashboard/conditional/h;Lcom/android/settings/dashboard/conditional/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/conditional/i;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-void
.end method


# virtual methods
.method protected Bk(Ljava/util/ArrayList;)V
    .locals 2

    const-string/jumbo v0, "ConditionManager"

    const-string/jumbo v1, "conditions loaded from xml, refreshing conditions"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    invoke-static {v0}, Lcom/android/settings/dashboard/conditional/h;->Be(Lcom/android/settings/dashboard/conditional/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    invoke-static {v0}, Lcom/android/settings/dashboard/conditional/h;->Be(Lcom/android/settings/dashboard/conditional/h;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->AV()V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/conditional/i;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .locals 5

    const-string/jumbo v0, "ConditionManager"

    const-string/jumbo v1, "loading conditions from xml"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    invoke-static {v3}, Lcom/android/settings/dashboard/conditional/h;->Bf(Lcom/android/settings/dashboard/conditional/h;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, "condition_state.xml"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/android/settings/dashboard/conditional/h;->Bh(Lcom/android/settings/dashboard/conditional/h;Ljava/io/File;)Ljava/io/File;

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    invoke-static {v1}, Lcom/android/settings/dashboard/conditional/h;->Bg(Lcom/android/settings/dashboard/conditional/h;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    iget-object v2, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    invoke-static {v2}, Lcom/android/settings/dashboard/conditional/h;->Bg(Lcom/android/settings/dashboard/conditional/h;)Ljava/io/File;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/android/settings/dashboard/conditional/h;->Bj(Lcom/android/settings/dashboard/conditional/h;Ljava/io/File;Ljava/util/ArrayList;)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/i;->Id:Lcom/android/settings/dashboard/conditional/h;

    invoke-static {v1, v0}, Lcom/android/settings/dashboard/conditional/h;->Bi(Lcom/android/settings/dashboard/conditional/h;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/conditional/i;->Bk(Ljava/util/ArrayList;)V

    return-void
.end method
