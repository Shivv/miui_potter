.class public Lcom/android/settings/dashboard/conditional/k;
.super Lcom/android/settings/dashboard/conditional/e;
.source "BatterySaverCondition.java"


# direct methods
.method public constructor <init>(Lcom/android/settings/dashboard/conditional/h;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/conditional/e;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-void
.end method


# virtual methods
.method public AA()I
    .locals 1

    const/16 v0, 0x17b

    return v0
.end method

.method public AC(I)V
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/k;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/os/PowerManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager;->setPowerSaveMode(Z)Z

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/k;->AE()V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public AD()V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/k;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/android/settings/fuelgauge/BatterySaverSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    const v5, 0x7f120247

    const/16 v7, 0x23

    move-object v3, v2

    move-object v6, v2

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bra(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;IILjava/lang/CharSequence;I)V

    return-void
.end method

.method public AE()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/k;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/os/PowerManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/conditional/k;->AJ(Z)V

    return-void
.end method

.method public Az()[Ljava/lang/CharSequence;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/k;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f12044a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Icon;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/k;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080249

    invoke-static {v0, v1}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/k;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12043e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/k;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12043f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
