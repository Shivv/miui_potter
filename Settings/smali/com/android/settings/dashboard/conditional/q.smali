.class public Lcom/android/settings/dashboard/conditional/q;
.super Ljava/lang/Object;
.source "ConditionAdapterUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Bm(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    new-instance v0, Lcom/android/settings/dashboard/conditional/t;

    const/4 v1, 0x0

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2, p0}, Lcom/android/settings/dashboard/conditional/t;-><init>(IILandroid/support/v7/widget/RecyclerView;)V

    new-instance v1, Landroid/support/v7/widget/a/b;

    invoke-direct {v1, v0}, Landroid/support/v7/widget/a/b;-><init>(Landroid/support/v7/widget/a/c;)V

    invoke-virtual {v1, p0}, Landroid/support/v7/widget/a/b;->dnm(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public static Bn(Lcom/android/settings/dashboard/conditional/e;Lcom/android/settings/dashboard/q;ZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 7

    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    instance-of v0, p0, Lcom/android/settings/dashboard/conditional/c;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ConditionAdapterUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Airplane mode condition has been bound with isActive="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->AM()Z

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ". Airplane mode is currently "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/dashboard/conditional/e;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v4}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/settingslib/G;->csh(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p1, Lcom/android/settings/dashboard/q;->itemView:Landroid/view/View;

    const v1, 0x7f0a00fa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JQ:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->getIcon()Landroid/graphics/drawable/Icon;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageIcon(Landroid/graphics/drawable/Icon;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JS:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->itemView:Landroid/view/View;

    const v1, 0x7f0a00e7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->itemView:Landroid/view/View;

    const v1, 0x7f0a0186

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz p2, :cond_2

    const v1, 0x7f0801ca

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v5

    if-eqz p2, :cond_3

    const v1, 0x7f120444

    :goto_1
    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->itemView:Landroid/view/View;

    const v1, 0x7f0a0134

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->Az()[Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    if-eq p2, v0, :cond_1

    if-eqz p2, :cond_6

    array-length v0, v5

    if-lez v0, :cond_5

    move v0, v2

    :goto_3
    const v1, 0x7f0a0143

    invoke-static {v4, v1, v0}, Lcom/android/settings/dashboard/conditional/q;->Bo(Landroid/view/View;IZ)V

    const v1, 0x7f0a00b5

    invoke-static {v4, v1, v0}, Lcom/android/settings/dashboard/conditional/q;->Bo(Landroid/view/View;IZ)V

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_4
    if-eqz p2, :cond_9

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JR:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v3

    :goto_5
    const/4 v0, 0x2

    if-ge v1, v0, :cond_9

    if-nez v1, :cond_7

    const v0, 0x7f0a01a0

    :goto_6
    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    array-length v2, v5

    if-le v2, v1, :cond_8

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    aget-object v2, v5, v1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/android/settings/dashboard/conditional/u;

    invoke-direct {v2, p0, v1}, Lcom/android/settings/dashboard/conditional/u;-><init>(Lcom/android/settings/dashboard/conditional/e;I)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_2
    const v1, 0x7f0801cb

    goto :goto_0

    :cond_3
    const v1, 0x7f120445

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_7
    const v0, 0x7f0a03c9

    goto :goto_6

    :cond_8
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_7

    :cond_9
    return-void
.end method

.method private static Bo(Landroid/view/View;IZ)V
    .locals 2

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method
