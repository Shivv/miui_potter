.class public Lcom/android/settings/dashboard/conditional/n;
.super Lcom/android/settings/dashboard/conditional/e;
.source "CellularDataCondition.java"


# static fields
.field private static final If:Landroid/content/IntentFilter;


# instance fields
.field private final Ig:Lcom/android/settings/dashboard/conditional/o;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.ANY_DATA_STATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/settings/dashboard/conditional/n;->If:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/dashboard/conditional/h;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/conditional/e;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    new-instance v0, Lcom/android/settings/dashboard/conditional/o;

    invoke-direct {v0}, Lcom/android/settings/dashboard/conditional/o;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/conditional/n;->Ig:Lcom/android/settings/dashboard/conditional/o;

    return-void
.end method


# virtual methods
.method public AA()I
    .locals 1

    const/16 v0, 0x17c

    return v0
.end method

.method public AC(I)V
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    invoke-virtual {p0, v2}, Lcom/android/settings/dashboard/conditional/n;->AJ(Z)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public AD()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v2}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/android/settings/Settings$DataUsageSummaryActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public AE()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    :cond_0
    invoke-virtual {p0, v3}, Lcom/android/settings/dashboard/conditional/n;->AJ(Z)V

    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDataEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/conditional/n;->AJ(Z)V

    return-void
.end method

.method protected AH()Landroid/content/IntentFilter;
    .locals 1

    sget-object v0, Lcom/android/settings/dashboard/conditional/n;->If:Landroid/content/IntentFilter;

    return-object v0
.end method

.method protected AI()Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/n;->Ig:Lcom/android/settings/dashboard/conditional/o;

    return-object v0
.end method

.method public Az()[Ljava/lang/CharSequence;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f12044b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Icon;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0801b6

    invoke-static {v0, v1}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120442

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/n;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120443

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
