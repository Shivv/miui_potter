.class public Lcom/android/settings/dashboard/conditional/c;
.super Lcom/android/settings/dashboard/conditional/e;
.source "AirplaneModeCondition.java"


# static fields
.field private static final HO:Landroid/content/IntentFilter;

.field public static TAG:Ljava/lang/String;


# instance fields
.field private final HP:Lcom/android/settings/dashboard/conditional/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "APM_Condition"

    sput-object v0, Lcom/android/settings/dashboard/conditional/c;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/settings/dashboard/conditional/c;->HO:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/dashboard/conditional/h;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/conditional/e;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    new-instance v0, Lcom/android/settings/dashboard/conditional/d;

    invoke-direct {v0}, Lcom/android/settings/dashboard/conditional/d;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/conditional/c;->HP:Lcom/android/settings/dashboard/conditional/d;

    return-void
.end method


# virtual methods
.method public AA()I
    .locals 1

    const/16 v0, 0x179

    return v0
.end method

.method public AC(I)V
    .locals 3

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/c;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->setAirplaneMode(Z)V

    invoke-virtual {p0, v1}, Lcom/android/settings/dashboard/conditional/c;->AJ(Z)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public AD()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/c;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/dashboard/conditional/c;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v2}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/android/settings/Settings$NetworkDashboardActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public AE()V
    .locals 2

    sget-object v0, Lcom/android/settings/dashboard/conditional/c;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "APM condition refreshed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/c;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/G;->csh(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/conditional/c;->AJ(Z)V

    return-void
.end method

.method protected AH()Landroid/content/IntentFilter;
    .locals 1

    sget-object v0, Lcom/android/settings/dashboard/conditional/c;->HO:Landroid/content/IntentFilter;

    return-object v0
.end method

.method protected AI()Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/c;->HP:Lcom/android/settings/dashboard/conditional/d;

    return-object v0
.end method

.method protected AJ(Z)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/conditional/e;->AJ(Z)V

    sget-object v0, Lcom/android/settings/dashboard/conditional/c;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setActive was called with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public Az()[Ljava/lang/CharSequence;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/c;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f12044a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Icon;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/c;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08016e

    invoke-static {v0, v1}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/c;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12043c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/c;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12043d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
