.class public Lcom/android/settings/dashboard/o;
.super Landroid/support/v7/widget/b;
.source "DashboardAdapter.java"

# interfaces
.implements Lcom/android/settings/dashboard/E;
.implements Lcom/android/settings/dashboard/suggestions/g;


# instance fields
.field private final JI:Lcom/android/settings/dashboard/p;

.field private JJ:Landroid/view/View$OnClickListener;

.field private final JK:Lcom/android/settings/dashboard/n;

.field private JL:Z

.field private final JM:Lcom/android/settings/dashboard/suggestions/a;

.field private final JN:Ljava/util/ArrayList;

.field private JO:Landroid/view/View$OnClickListener;

.field private final mContext:Landroid/content/Context;

.field mDashboardData:Lcom/android/settings/dashboard/d;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/util/List;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/b;-><init>()V

    new-instance v2, Lcom/android/settings/dashboard/J;

    invoke-direct {v2, p0}, Lcom/android/settings/dashboard/J;-><init>(Lcom/android/settings/dashboard/o;)V

    iput-object v2, p0, Lcom/android/settings/dashboard/o;->JO:Landroid/view/View$OnClickListener;

    new-instance v2, Lcom/android/settings/dashboard/K;

    invoke-direct {v2, p0}, Lcom/android/settings/dashboard/K;-><init>(Lcom/android/settings/dashboard/o;)V

    iput-object v2, p0, Lcom/android/settings/dashboard/o;->JJ:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {v2, p1}, Lcom/android/settings/overlay/a;->aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/dashboard/o;->JK:Lcom/android/settings/dashboard/n;

    invoke-virtual {v2, p1}, Lcom/android/settings/overlay/a;->aIv(Landroid/content/Context;)Lcom/android/settings/dashboard/suggestions/a;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/dashboard/o;->JM:Lcom/android/settings/dashboard/suggestions/a;

    new-instance v2, Lcom/android/settings/dashboard/p;

    invoke-direct {v2, p1}, Lcom/android/settings/dashboard/p;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/settings/dashboard/o;->JI:Lcom/android/settings/dashboard/p;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/android/settings/dashboard/o;->setHasStableIds(Z)V

    if-eqz p2, :cond_0

    const-string/jumbo v1, "suggestion_list"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    const-string/jumbo v1, "category_list"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string/jumbo v3, "suggestion_mode"

    invoke-virtual {p2, v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v3, "suggestions_shown_logged"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    :goto_0
    new-instance v3, Lcom/android/settings/dashboard/e;

    invoke-direct {v3}, Lcom/android/settings/dashboard/e;-><init>()V

    invoke-virtual {v3, p3}, Lcom/android/settings/dashboard/e;->CP(Ljava/util/List;)Lcom/android/settings/dashboard/e;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/settings/dashboard/e;->CS(Ljava/util/List;)Lcom/android/settings/dashboard/e;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/settings/dashboard/e;->CO(Ljava/util/List;)Lcom/android/settings/dashboard/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/dashboard/e;->CR(I)Lcom/android/settings/dashboard/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/dashboard/e;->build()Lcom/android/settings/dashboard/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    return-void

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    move-object v2, v1

    goto :goto_0
.end method

.method static synthetic DA(Lcom/android/settings/dashboard/o;)Lcom/android/settings/core/instrumentation/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-object v0
.end method

.method static synthetic DB(Lcom/android/settings/dashboard/o;Lcom/android/settings/dashboard/conditional/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/o;->Du(Lcom/android/settings/dashboard/conditional/e;)V

    return-void
.end method

.method private Dp(Lcom/android/settings/dashboard/d;)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/dashboard/o;->JL:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/settings/dashboard/f;

    invoke-virtual {p1}, Lcom/android/settings/dashboard/d;->CC()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v2}, Lcom/android/settings/dashboard/d;->CC()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/dashboard/f;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v0}, Landroid/support/v7/c/a;->dIT(Landroid/support/v7/c/b;)Landroid/support/v7/c/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v7/c/e;->dJc(Landroid/support/v7/widget/b;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/dashboard/o;->JL:Z

    invoke-virtual {p0}, Lcom/android/settings/dashboard/o;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private Dq(Lcom/android/settings/dashboard/q;Lcom/android/settingslib/drawer/DashboardCategory;)V
    .locals 2

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JS:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settingslib/drawer/DashboardCategory;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private Dr(Lcom/android/settings/dashboard/q;Lcom/android/settingslib/drawer/Tile;)V
    .locals 3

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JQ:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->JI:Lcom/android/settings/dashboard/p;

    iget-object v2, p2, Lcom/android/settingslib/drawer/Tile;->czu:Landroid/graphics/drawable/Icon;

    invoke-virtual {v1, v2}, Lcom/android/settings/dashboard/p;->DC(Landroid/graphics/drawable/Icon;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JS:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JR:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JR:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JR:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private Du(Lcom/android/settings/dashboard/conditional/e;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    new-instance v1, Lcom/android/settings/dashboard/e;

    invoke-direct {v1, v0}, Lcom/android/settings/dashboard/e;-><init>(Lcom/android/settings/dashboard/d;)V

    invoke-virtual {v1, p1}, Lcom/android/settings/dashboard/e;->CQ(Lcom/android/settings/dashboard/conditional/e;)Lcom/android/settings/dashboard/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dashboard/e;->build()Lcom/android/settings/dashboard/d;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/o;->Dp(Lcom/android/settings/dashboard/d;)V

    return-void
.end method

.method static synthetic Dy(Lcom/android/settings/dashboard/o;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic Dz(Lcom/android/settings/dashboard/o;)Lcom/android/settings/dashboard/n;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->JK:Lcom/android/settings/dashboard/n;

    return-object v0
.end method


# virtual methods
.method public BP(I)Lcom/android/settingslib/drawer/Tile;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p1}, Lcom/android/settings/dashboard/d;->CA(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    return-object v0
.end method

.method public BQ(Lcom/android/settingslib/drawer/Tile;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/d;->CG()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    new-instance v2, Lcom/android/settings/dashboard/e;

    invoke-direct {v2, v1}, Lcom/android/settings/dashboard/e;-><init>(Lcom/android/settings/dashboard/d;)V

    invoke-virtual {v2, v0}, Lcom/android/settings/dashboard/e;->CS(Ljava/util/List;)Lcom/android/settings/dashboard/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/dashboard/e;->build()Lcom/android/settings/dashboard/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-direct {p0, v1}, Lcom/android/settings/dashboard/o;->Dp(Lcom/android/settings/dashboard/d;)V

    return-void
.end method

.method public BZ(Lcom/android/settingslib/drawer/Tile;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p1}, Lcom/android/settings/dashboard/d;->CE(Lcom/android/settingslib/drawer/Tile;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v1, v0}, Lcom/android/settings/dashboard/d;->CD(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/dashboard/o;->notifyItemChanged(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public Dl(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    const-string/jumbo v1, "DashboardAdapter"

    const-string/jumbo v2, "adapter setConditions called"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/settings/dashboard/e;

    invoke-direct {v1, v0}, Lcom/android/settings/dashboard/e;-><init>(Lcom/android/settings/dashboard/d;)V

    invoke-virtual {v1, p1}, Lcom/android/settings/dashboard/e;->CP(Ljava/util/List;)Lcom/android/settings/dashboard/e;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/settings/dashboard/e;->CQ(Lcom/android/settings/dashboard/conditional/e;)Lcom/android/settings/dashboard/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dashboard/e;->build()Lcom/android/settings/dashboard/d;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/o;->Dp(Lcom/android/settings/dashboard/d;)V

    return-void
.end method

.method public Dm(Ljava/util/List;Ljava/util/List;)V
    .locals 8

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v3, 0x1010429

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    const v3, 0x106000b

    invoke-virtual {v1, v3}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    move v3, v2

    :goto_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/DashboardCategory;

    iget-object v0, v0, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/DashboardCategory;

    iget-object v0, v0, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v6, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v0, v0, Lcom/android/settingslib/drawer/Tile;->czu:Landroid/graphics/drawable/Icon;

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Icon;->setTint(I)Landroid/graphics/drawable/Icon;

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    new-instance v1, Lcom/android/settings/dashboard/e;

    invoke-direct {v1, v0}, Lcom/android/settings/dashboard/e;-><init>(Lcom/android/settings/dashboard/d;)V

    invoke-virtual {v1, p2}, Lcom/android/settings/dashboard/e;->CS(Ljava/util/List;)Lcom/android/settings/dashboard/e;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/settings/dashboard/e;->CO(Ljava/util/List;)Lcom/android/settings/dashboard/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dashboard/e;->build()Lcom/android/settings/dashboard/d;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/o;->Dp(Lcom/android/settings/dashboard/d;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/d;->CF()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object p2, v4

    :goto_2
    :pswitch_1
    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->JM:Lcom/android/settings/dashboard/suggestions/a;

    iget-object v4, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-interface {v3, v4, v0}, Lcom/android/settings/dashboard/suggestions/a;->Bv(Landroid/content/Context;Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v4, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    new-array v5, v2, [Landroid/util/Pair;

    const/16 v6, 0x180

    invoke-virtual {v3, v4, v6, v0, v5}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :pswitch_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-interface {p2, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    goto :goto_2

    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public Dn(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    const-string/jumbo v1, "DashboardAdapter"

    const-string/jumbo v2, "adapter setCategory called"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/settings/dashboard/e;

    invoke-direct {v1, v0}, Lcom/android/settings/dashboard/e;-><init>(Lcom/android/settings/dashboard/d;)V

    invoke-virtual {v1, p1}, Lcom/android/settings/dashboard/e;->CO(Ljava/util/List;)Lcom/android/settings/dashboard/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dashboard/e;->build()Lcom/android/settings/dashboard/d;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/o;->Dp(Lcom/android/settings/dashboard/d;)V

    return-void
.end method

.method public Do(J)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/dashboard/d;->Cz(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public Ds(Lcom/android/settings/dashboard/q;I)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p2}, Lcom/android/settings/dashboard/d;->CD(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p2}, Lcom/android/settings/dashboard/d;->CA(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/DashboardCategory;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/o;->Dq(Lcom/android/settings/dashboard/q;Lcom/android/settingslib/drawer/DashboardCategory;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p2}, Lcom/android/settings/dashboard/d;->CA(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/o;->Dr(Lcom/android/settings/dashboard/q;Lcom/android/settingslib/drawer/Tile;)V

    iget-object v1, p1, Lcom/android/settings/dashboard/q;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->itemView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->JO:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p2}, Lcom/android/settings/dashboard/d;->CA(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/i;

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/dashboard/o;->onBindSuggestionHeader(Lcom/android/settings/dashboard/q;Lcom/android/settings/dashboard/i;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p2}, Lcom/android/settings/dashboard/d;->CA(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->JM:Lcom/android/settings/dashboard/suggestions/a;

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-interface {v2, v3, v0}, Lcom/android/settings/dashboard/suggestions/a;->Bv(Landroid/content/Context;Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v4, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    new-array v1, v1, [Landroid/util/Pair;

    const/16 v5, 0x180

    invoke-virtual {v3, v4, v5, v2, v1}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/o;->Dr(Lcom/android/settings/dashboard/q;Lcom/android/settingslib/drawer/Tile;)V

    iget-object v1, p1, Lcom/android/settings/dashboard/q;->itemView:Landroid/view/View;

    new-instance v3, Lcom/android/settings/dashboard/-$Lambda$umwiTQIR7duhwJEEgJt8o_U_-Yk$1;

    invoke-direct {v3, p0, v2, v0}, Lcom/android/settings/dashboard/-$Lambda$umwiTQIR7duhwJEEgJt8o_U_-Yk$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p2}, Lcom/android/settings/dashboard/d;->CA(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v2}, Lcom/android/settings/dashboard/d;->Cy()Lcom/android/settings/dashboard/conditional/e;

    move-result-object v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p2}, Lcom/android/settings/dashboard/d;->CA(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->JJ:Landroid/view/View$OnClickListener;

    new-instance v3, Lcom/android/settings/dashboard/-$Lambda$umwiTQIR7duhwJEEgJt8o_U_-Yk;

    invoke-direct {v3, p0}, Lcom/android/settings/dashboard/-$Lambda$umwiTQIR7duhwJEEgJt8o_U_-Yk;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, p1, v1, v2, v3}, Lcom/android/settings/dashboard/conditional/q;->Bn(Lcom/android/settings/dashboard/conditional/e;Lcom/android/settings/dashboard/q;ZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d0062 -> :sswitch_4
        0x7f0d0086 -> :sswitch_0
        0x7f0d0088 -> :sswitch_1
        0x7f0d01f3 -> :sswitch_2
        0x7f0d01f4 -> :sswitch_3
    .end sparse-switch
.end method

.method public Dt(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/d;->Cy()Lcom/android/settings/dashboard/conditional/e;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->AA()I

    move-result v0

    const/16 v3, 0x176

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/settings/core/instrumentation/e;->ajU(Landroid/content/Context;II)V

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/o;->Du(Lcom/android/settings/dashboard/conditional/e;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->AA()I

    move-result v3

    const/16 v4, 0x175

    invoke-virtual {v1, v2, v4, v3}, Lcom/android/settings/core/instrumentation/e;->ajU(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method synthetic Dv(Ljava/lang/String;Lcom/android/settingslib/drawer/Tile;Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/util/Pair;

    const/16 v3, 0x182

    invoke-virtual {v0, v1, v3, p1, v2}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/android/settings/bL;

    iget-object v1, p2, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/android/settings/bL;->bMI(Landroid/content/Intent;)V

    return-void
.end method

.method synthetic Dw(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/o;->Dt(Landroid/view/View;)V

    return-void
.end method

.method synthetic Dx(ZLandroid/view/View;)V
    .locals 7

    if-eqz p1, :cond_1

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/d;->CG()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->JM:Lcom/android/settings/dashboard/suggestions/a;

    iget-object v4, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-interface {v3, v4, v0}, Lcom/android/settings/dashboard/suggestions/a;->Bv(Landroid/content/Context;Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v4, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    new-array v5, v5, [Landroid/util/Pair;

    const/16 v6, 0x180

    invoke-virtual {v3, v4, v6, v0, v5}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    new-instance v2, Lcom/android/settings/dashboard/e;

    invoke-direct {v2, v1}, Lcom/android/settings/dashboard/e;-><init>(Lcom/android/settings/dashboard/d;)V

    invoke-virtual {v2, v0}, Lcom/android/settings/dashboard/e;->CR(I)Lcom/android/settings/dashboard/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/dashboard/e;->build()Lcom/android/settings/dashboard/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-direct {p0, v1}, Lcom/android/settings/dashboard/o;->Dp(Lcom/android/settings/dashboard/d;)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/d;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p1}, Lcom/android/settings/dashboard/d;->CB(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0, p1}, Lcom/android/settings/dashboard/d;->CD(I)I

    move-result v0

    return v0
.end method

.method onBindSuggestionHeader(Lcom/android/settings/dashboard/q;Lcom/android/settings/dashboard/i;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-boolean v1, p2, Lcom/android/settings/dashboard/i;->Jx:Z

    iget v2, p2, Lcom/android/settings/dashboard/i;->Jz:I

    iget-object v3, p1, Lcom/android/settings/dashboard/q;->JQ:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    const v0, 0x7f0801cb

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JS:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p2, Lcom/android/settings/dashboard/i;->Jy:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const v5, 0x7f1211e3

    invoke-virtual {v3, v5, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const v4, 0x7f100035

    invoke-virtual {v0, v4, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v3, p1, Lcom/android/settings/dashboard/q;->JR:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    if-nez v2, :cond_2

    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JR:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v0, p1, Lcom/android/settings/dashboard/q;->itemView:Landroid/view/View;

    new-instance v2, Lcom/android/settings/dashboard/-$Lambda$umwiTQIR7duhwJEEgJt8o_U_-Yk$2;

    invoke-direct {v2, v1, p0}, Lcom/android/settings/dashboard/-$Lambda$umwiTQIR7duhwJEEgJt8o_U_-Yk$2;-><init>(ZLjava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const v0, 0x7f0801ca

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    const v3, 0x7f120444

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lcom/android/settings/dashboard/q;->JR:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    const v2, 0x7f1211e2

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/p;I)V
    .locals 0

    check-cast p1, Lcom/android/settings/dashboard/q;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/dashboard/o;->Ds(Lcom/android/settings/dashboard/q;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/dashboard/o;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/dashboard/q;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/dashboard/q;
    .locals 3

    new-instance v0, Lcom/android/settings/dashboard/q;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/dashboard/q;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public onPause()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/d;->CG()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/d;->CG()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->JM:Lcom/android/settings/dashboard/suggestions/a;

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    invoke-interface {v2, v3, v0}, Lcom/android/settings/dashboard/suggestions/a;->Bv(Landroid/content/Context;Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/dashboard/o;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v3, p0, Lcom/android/settings/dashboard/o;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    new-array v4, v4, [Landroid/util/Pair;

    const/16 v5, 0x181

    invoke-virtual {v2, v3, v5, v0, v4}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/d;->CG()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/d;->Cw()Ljava/util/List;

    move-result-object v1

    if-eqz v0, :cond_0

    const-string/jumbo v2, "suggestion_list"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    if-eqz v1, :cond_1

    const-string/jumbo v0, "category_list"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_1
    const-string/jumbo v0, "suggestion_mode"

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->mDashboardData:Lcom/android/settings/dashboard/d;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/d;->CF()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "suggestions_shown_logged"

    iget-object v1, p0, Lcom/android/settings/dashboard/o;->JN:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method
