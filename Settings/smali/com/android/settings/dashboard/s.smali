.class public Lcom/android/settings/dashboard/s;
.super Ljava/lang/Object;
.source "ProgressiveDisclosureMixin.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/m;
.implements Lcom/android/settings/core/lifecycle/a/h;


# instance fields
.field private final JV:Ljava/util/List;

.field private JW:Lcom/android/settings/dashboard/ExpandPreference;

.field private final JX:Landroid/preference/PreferenceFragment;

.field private JY:I

.field private JZ:Z

.field private final mContext:Landroid/content/Context;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/preference/PreferenceFragment;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    const/16 v0, 0x12c

    iput v0, p0, Lcom/android/settings/dashboard/s;->JY:I

    iput-object p1, p0, Lcom/android/settings/dashboard/s;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/dashboard/s;->JX:Landroid/preference/PreferenceFragment;

    new-instance v0, Lcom/android/settings/dashboard/ExpandPreference;

    invoke-direct {v0, p1}, Lcom/android/settings/dashboard/ExpandPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/s;->JW:Lcom/android/settings/dashboard/ExpandPreference;

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JW:Lcom/android/settings/dashboard/ExpandPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/dashboard/ExpandPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/s;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iput-boolean p3, p0, Lcom/android/settings/dashboard/s;->JZ:Z

    return-void
.end method


# virtual methods
.method public DD(Landroid/preference/PreferenceScreen;Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1, p2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    :cond_1
    instance-of v2, v0, Landroid/preference/PreferenceGroup;

    if-eqz v2, :cond_2

    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, p2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_2

    return-object v0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "ProgressiveDisclosure"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot find preference with key "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v3
.end method

.method public DE(Landroid/preference/PreferenceScreen;)V
    .locals 3

    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/s;->DJ(Landroid/preference/PreferenceScreen;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "ProgressiveDisclosure"

    const-string/jumbo v2, "collapsed list should ALWAYS BE EMPTY before collapsing!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget v1, p0, Lcom/android/settings/dashboard/s;->JY:I

    if-lt v0, v1, :cond_2

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/dashboard/s;->addToCollapsedList(Landroid/preference/Preference;)V

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JW:Lcom/android/settings/dashboard/ExpandPreference;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method public DF(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/dashboard/s;->DI()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getOrder()I

    move-result v1

    invoke-virtual {p2}, Landroid/preference/Preference;->getOrder()I

    move-result v2

    if-le v1, v2, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p1, p2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/s;->addToCollapsedList(Landroid/preference/Preference;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/settings/dashboard/s;->addToCollapsedList(Landroid/preference/Preference;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p2}, Lcom/android/settings/dashboard/s;->addToCollapsedList(Landroid/preference/Preference;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/s;->DJ(Landroid/preference/PreferenceScreen;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, p2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/s;->DE(Landroid/preference/PreferenceScreen;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1, p2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public DG(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p1, p2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JW:Lcom/android/settings/dashboard/ExpandPreference;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/dashboard/s;->updateExpandButtonSummary()V

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-void
.end method

.method public DH(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/dashboard/s;->JY:I

    return-void
.end method

.method public DI()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public DJ(Landroid/preference/PreferenceScreen;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/settings/dashboard/s;->JZ:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    iget v2, p0, Lcom/android/settings/dashboard/s;->JY:I

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method addToCollapsedList(Landroid/preference/Preference;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    mul-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, -0x1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/s;->updateExpandButtonSummary()V

    return-void
.end method

.method getCollapsedPrefs()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    const-string/jumbo v0, "state_user_expanded"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/dashboard/s;->JZ:Z

    :cond_0
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    const/4 v1, 0x0

    instance-of v0, p1, Lcom/android/settings/dashboard/ExpandPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JX:Landroid/preference/PreferenceFragment;

    invoke-virtual {v0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/dashboard/s;->JZ:Z

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JX:Landroid/preference/PreferenceFragment;

    instance-of v0, v0, Lcom/android/settings/core/instrumentation/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JX:Landroid/preference/PreferenceFragment;

    check-cast v0, Lcom/android/settings/core/instrumentation/f;

    invoke-interface {v0}, Lcom/android/settings/core/instrumentation/f;->getMetricsCategory()I

    move-result v0

    :goto_1
    iget-object v2, p0, Lcom/android/settings/dashboard/s;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v3, p0, Lcom/android/settings/dashboard/s;->mContext:Landroid/content/Context;

    const/16 v4, 0x342

    invoke-virtual {v2, v3, v0, v4}, Lcom/android/settings/core/instrumentation/e;->ajZ(Landroid/content/Context;II)V

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "state_user_expanded"

    iget-boolean v1, p0, Lcom/android/settings/dashboard/s;->JZ:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method updateExpandButtonSummary()V
    .locals 8

    const/4 v3, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JW:Lcom/android/settings/dashboard/ExpandPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/ExpandPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    if-ne v4, v3, :cond_1

    iget-object v1, p0, Lcom/android/settings/dashboard/s;->JW:Lcom/android/settings/dashboard/ExpandPreference;

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/settings/dashboard/ExpandPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JV:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/android/settings/dashboard/s;->mContext:Landroid/content/Context;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v7

    aput-object v0, v6, v3

    const v0, 0x7f120886

    invoke-virtual {v5, v0, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/dashboard/s;->JW:Lcom/android/settings/dashboard/ExpandPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/ExpandPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
