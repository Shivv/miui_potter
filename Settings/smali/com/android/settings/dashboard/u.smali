.class final Lcom/android/settings/dashboard/u;
.super Ljava/lang/Object;
.source "SupportItemAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic Kl:Lcom/android/settings/dashboard/t;


# direct methods
.method private constructor <init>(Lcom/android/settings/dashboard/t;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/dashboard/t;Lcom/android/settings/dashboard/u;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/u;-><init>(Lcom/android/settings/dashboard/t;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Eb(Lcom/android/settings/dashboard/t;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Ee(Lcom/android/settings/dashboard/t;)Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v1

    new-array v2, v4, [Landroid/util/Pair;

    const/16 v3, 0x1e0

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Eh(Lcom/android/settings/dashboard/t;)Lcom/android/settings/overlay/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/settings/overlay/b;->aID()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Eh(Lcom/android/settings/dashboard/t;)Lcom/android/settings/overlay/b;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v2}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/settings/overlay/b;->aIK(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Ed(Lcom/android/settings/dashboard/t;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Ee(Lcom/android/settings/dashboard/t;)Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v1

    new-array v2, v4, [Landroid/util/Pair;

    const/16 v3, 0x1e1

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/settings/dashboard/t;->El(Lcom/android/settings/dashboard/t;I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Ee(Lcom/android/settings/dashboard/t;)Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v1

    new-array v2, v4, [Landroid/util/Pair;

    const/16 v3, 0x1e2

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/settings/dashboard/t;->El(Lcom/android/settings/dashboard/t;I)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Eh(Lcom/android/settings/dashboard/t;)Lcom/android/settings/overlay/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Eg(Lcom/android/settings/dashboard/t;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/android/settings/overlay/b;->aIL(Ljava/lang/String;Z)Lcom/android/settings/support/SupportPhone;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/support/SupportPhone;->aSW()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Ee(Lcom/android/settings/dashboard/t;)Lcom/android/settings/core/instrumentation/e;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v2}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v2

    new-array v3, v4, [Landroid/util/Pair;

    const/16 v4, 0x1e5

    invoke-virtual {v1, v2, v4, v3}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v0}, Lcom/android/settings/dashboard/t;->Eh(Lcom/android/settings/dashboard/t;)Lcom/android/settings/overlay/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Eg(Lcom/android/settings/dashboard/t;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/android/settings/overlay/b;->aIL(Ljava/lang/String;Z)Lcom/android/settings/support/SupportPhone;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/support/SupportPhoneDialogFragment;->aSX(Lcom/android/settings/support/SupportPhone;)Lcom/android/settings/support/SupportPhoneDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Ee(Lcom/android/settings/dashboard/t;)Lcom/android/settings/core/instrumentation/e;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v2}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v2

    new-array v3, v4, [Landroid/util/Pair;

    const/16 v4, 0x1e6

    invoke-virtual {v1, v2, v4, v3}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    iget-object v1, p0, Lcom/android/settings/dashboard/u;->Kl:Lcom/android/settings/dashboard/t;

    invoke-static {v1}, Lcom/android/settings/dashboard/t;->Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "SupportPhoneDialog"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/support/SupportPhoneDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1020014
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1020014
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1020014
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
