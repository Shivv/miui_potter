.class Lcom/android/settings/dashboard/G;
.super Landroid/os/Handler;
.source "SummaryLoader.java"


# instance fields
.field final synthetic Lo:Lcom/android/settings/dashboard/C;


# direct methods
.method public constructor <init>(Lcom/android/settings/dashboard/C;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/G;->Lo:Lcom/android/settings/dashboard/C;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    const/4 v0, 0x0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v1, p0, Lcom/android/settings/dashboard/G;->Lo:Lcom/android/settings/dashboard/C;

    invoke-static {v1, v0}, Lcom/android/settings/dashboard/C;->Fm(Lcom/android/settings/dashboard/C;Lcom/android/settingslib/drawer/Tile;)V

    goto :goto_0

    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dashboard/G;->Lo:Lcom/android/settings/dashboard/C;

    invoke-static {v1, v0}, Lcom/android/settings/dashboard/C;->Fn(Lcom/android/settings/dashboard/C;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
