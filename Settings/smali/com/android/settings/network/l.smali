.class Lcom/android/settings/network/l;
.super Landroid/database/ContentObserver;
.source "TetherPreferenceController.java"


# instance fields
.field public final aYF:Landroid/net/Uri;

.field final synthetic aYG:Lcom/android/settings/network/k;


# direct methods
.method public constructor <init>(Lcom/android/settings/network/k;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/network/l;->aYG:Lcom/android/settings/network/k;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "airplane_mode_on"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/l;->aYF:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3

    const/4 v0, 0x0

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v1, p0, Lcom/android/settings/network/l;->aYF:Landroid/net/Uri;

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/network/l;->aYG:Lcom/android/settings/network/k;

    invoke-static {v1}, Lcom/android/settings/network/k;->aMJ(Lcom/android/settings/network/k;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/network/l;->aYG:Lcom/android/settings/network/k;

    invoke-static {v0}, Lcom/android/settings/network/k;->aMK(Lcom/android/settings/network/k;)V

    :cond_1
    return-void
.end method
