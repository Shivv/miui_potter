.class public Lcom/android/settings/network/c;
.super Lcom/android/settings/core/e;
.source "NetworkScorerPickerPreferenceController.java"


# instance fields
.field private final aYd:Lcom/android/settings/network/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/network/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/network/c;->aYd:Lcom/android/settings/network/a;

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/network/c;->aYd:Lcom/android/settings/network/a;

    invoke-virtual {v0}, Lcom/android/settings/network/a;->aMo()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    if-nez v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/c;->aYd:Lcom/android/settings/network/a;

    invoke-virtual {v0}, Lcom/android/settings/network/a;->aMr()Landroid/net/NetworkScorerAppData;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/network/c;->mContext:Landroid/content/Context;

    const v1, 0x7f120b45

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkScorerAppData;->getRecommendationServiceLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "network_scorer_picker"

    return-object v0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
