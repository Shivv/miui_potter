.class final Lcom/android/settings/network/s;
.super Ljava/lang/Object;
.source "TetherPreferenceController.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# instance fields
.field final synthetic aYP:Lcom/android/settings/network/k;


# direct methods
.method constructor <init>(Lcom/android/settings/network/k;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/network/s;->aYP:Lcom/android/settings/network/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/s;->aYP:Lcom/android/settings/network/k;

    invoke-static {v0}, Lcom/android/settings/network/k;->aMI(Lcom/android/settings/network/k;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    check-cast p2, Landroid/bluetooth/BluetoothPan;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/network/s;->aYP:Lcom/android/settings/network/k;

    invoke-virtual {v0}, Lcom/android/settings/network/k;->updateSummary()V

    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/network/s;->aYP:Lcom/android/settings/network/k;

    invoke-static {v0}, Lcom/android/settings/network/k;->aMI(Lcom/android/settings/network/k;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method
