.class public Lcom/android/settings/network/n;
.super Lcom/android/settings/core/e;
.source "WifiCallingPreferenceController.java"


# instance fields
.field private aYL:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    const-string/jumbo v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/network/n;->aYL:Landroid/telephony/TelephonyManager;

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/network/n;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/network/n;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/network/n;->aYL:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/android/ims/ImsManager;->getWfcMode(Landroid/content/Context;Z)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/WifiCallingSettings;->bUC(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "wifi_calling_settings"

    return-object v0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
