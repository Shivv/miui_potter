.class public Lcom/android/settings/network/i;
.super Lcom/android/settings/core/e;
.source "NetworkResetPreferenceController.java"


# instance fields
.field private final aYw:Lcom/android/settings/network/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/network/b;

    invoke-direct {v0, p1}, Lcom/android/settings/network/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/network/i;->aYw:Lcom/android/settings/network/b;

    return-void
.end method


# virtual methods
.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "network_reset_pref"

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/i;->aYw:Lcom/android/settings/network/b;

    invoke-virtual {v0}, Lcom/android/settings/network/b;->aMt()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
