.class final Lcom/android/settings/et;
.super Landroid/os/AsyncTask;
.source "LockPatternChecker.java"


# instance fields
.field private ciP:I

.field final synthetic ciQ:Lcom/android/internal/widget/LockPatternUtils;

.field final synthetic ciR:Ljava/lang/String;

.field final synthetic ciS:J

.field final synthetic ciT:I

.field final synthetic ciU:Lcom/android/settings/O;


# direct methods
.method constructor <init>(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;JILcom/android/settings/O;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/et;->ciQ:Lcom/android/internal/widget/LockPatternUtils;

    iput-object p2, p0, Lcom/android/settings/et;->ciR:Ljava/lang/String;

    iput-wide p3, p0, Lcom/android/settings/et;->ciS:J

    iput p5, p0, Lcom/android/settings/et;->ciT:I

    iput-object p6, p0, Lcom/android/settings/et;->ciU:Lcom/android/settings/O;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bZS([B)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/et;->ciU:Lcom/android/settings/O;

    iget v1, p0, Lcom/android/settings/et;->ciP:I

    invoke-interface {v0, p1, v1}, Lcom/android/settings/O;->blZ([BI)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/et;->doInBackground([Ljava/lang/Void;)[B

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[B
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/et;->ciQ:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/et;->ciR:Ljava/lang/String;

    iget-wide v2, p0, Lcom/android/settings/et;->ciS:J

    iget v4, p0, Lcom/android/settings/et;->ciT:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/settings/bn;->bFr(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;JI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/et;->ciP:I

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/android/settings/et;->bZS([B)V

    return-void
.end method
