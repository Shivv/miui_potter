.class final enum Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;
.super Ljava/lang/Enum;
.source "KeySettingsPreviewPreference.java"


# static fields
.field public static final enum bLo:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum bLp:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum bLq:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum bLr:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum bLs:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field private static final synthetic bLt:[Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;


# instance fields
.field private mAnimArrayId:I

.field private mImgViewId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    const-string/jumbo v1, "POWER_CLICK"

    const v2, 0x7f0a0233

    const v3, 0x7f0300c3

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLr:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    const-string/jumbo v1, "LONG_PRESS"

    const v2, 0x7f0a0232

    const v3, 0x7f030092

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLq:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    const-string/jumbo v1, "CLICK_BOTTOM"

    const v2, 0x7f0a0231

    const v3, 0x7f030051

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLo:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    const-string/jumbo v1, "DOUBLE_CLICK_POWER"

    const v2, 0x7f0a0234

    const v3, 0x7f0300c4

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLp:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    const-string/jumbo v1, "THREE_DROP"

    const v2, 0x7f0a0239

    const v3, 0x7f0300fd

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLs:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget-object v1, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLr:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLq:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLo:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLp:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLs:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    aput-object v1, v0, v8

    sput-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLt:[Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->mImgViewId:I

    iput p4, p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->mAnimArrayId:I

    return-void
.end method

.method static synthetic bBT(Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->mAnimArrayId:I

    return v0
.end method

.method static synthetic bBU(Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->mImgViewId:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;
    .locals 1

    const-class v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    return-object v0
.end method

.method public static values()[Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;
    .locals 1

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLt:[Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    return-object v0
.end method
