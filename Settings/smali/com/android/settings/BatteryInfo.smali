.class public Lcom/android/settings/BatteryInfo;
.super Lmiui/app/Activity;
.source "BatteryInfo.java"


# instance fields
.field private byI:Lcom/android/internal/app/IBatteryStats;

.field private byJ:Landroid/os/Handler;

.field private byK:Landroid/widget/TextView;

.field private byL:Landroid/content/IntentFilter;

.field private byM:Landroid/content/BroadcastReceiver;

.field private byN:Landroid/widget/TextView;

.field private byO:Landroid/widget/TextView;

.field private byP:Landroid/widget/TextView;

.field private byQ:Landroid/os/IPowerManager;

.field private byR:Landroid/widget/TextView;

.field private byS:Landroid/widget/TextView;

.field private byT:Landroid/widget/TextView;

.field private byU:Landroid/widget/TextView;

.field private byV:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    new-instance v0, Lcom/android/settings/ew;

    invoke-direct {v0, p0}, Lcom/android/settings/ew;-><init>(Lcom/android/settings/BatteryInfo;)V

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byJ:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/ex;

    invoke-direct {v0, p0}, Lcom/android/settings/ex;-><init>(Lcom/android/settings/BatteryInfo;)V

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byM:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private final bmb(I)Ljava/lang/String;
    .locals 3

    div-int/lit8 v0, p1, 0xa

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-int/lit8 v0, v0, 0xa

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bmc()V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/settings/BatteryInfo;->byU:Landroid/widget/TextView;

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic bmd(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byK:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bme(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byN:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bmf(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byO:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bmg(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byP:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bmh(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byR:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bmi(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byS:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bmj(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byT:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bmk(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byV:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bml(Lcom/android/settings/BatteryInfo;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/BatteryInfo;->bmb(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bmm(Lcom/android/settings/BatteryInfo;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BatteryInfo;->bmc()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d004a

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->setContentView(I)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byL:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byL:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const v0, 0x7f120222

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->setTitle(I)V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lmiui/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byJ:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byM:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lmiui/app/Activity;->onResume()V

    const v0, 0x7f0a042b

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byR:Landroid/widget/TextView;

    const v0, 0x7f0a0326

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byO:Landroid/widget/TextView;

    const v0, 0x7f0a0262

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byN:Landroid/widget/TextView;

    const v0, 0x7f0a03a6

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byP:Landroid/widget/TextView;

    const v0, 0x7f0a01e1

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byK:Landroid/widget/TextView;

    const v0, 0x7f0a0481

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byS:Landroid/widget/TextView;

    const v0, 0x7f0a04fa

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byV:Landroid/widget/TextView;

    const v0, 0x7f0a0482

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byT:Landroid/widget/TextView;

    const v0, 0x7f0a04d1

    invoke-virtual {p0, v0}, Lcom/android/settings/BatteryInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byU:Landroid/widget/TextView;

    const-string/jumbo v0, "batterystats"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/app/IBatteryStats$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IBatteryStats;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byI:Lcom/android/internal/app/IBatteryStats;

    const-string/jumbo v0, "power"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/BatteryInfo;->byQ:Landroid/os/IPowerManager;

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byJ:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/android/settings/BatteryInfo;->byM:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/android/settings/BatteryInfo;->byL:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/BatteryInfo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
