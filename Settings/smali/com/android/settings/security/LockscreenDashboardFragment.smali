.class public Lcom/android/settings/security/LockscreenDashboardFragment;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "LockscreenDashboardFragment.java"


# static fields
.field static final KEY_LOCK_SCREEN_NOTIFICATON:Ljava/lang/String; = "security_setting_lock_screen_notif"

.field static final KEY_LOCK_SCREEN_NOTIFICATON_WORK_PROFILE:Ljava/lang/String; = "security_setting_lock_screen_notif_work"

.field static final KEY_LOCK_SCREEN_NOTIFICATON_WORK_PROFILE_HEADER:Ljava/lang/String; = "security_setting_lock_screen_notif_work_header"

.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private bfO:Lcom/android/settings/security/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/security/g;

    invoke-direct {v0}, Lcom/android/settings/security/g;-><init>()V

    sput-object v0, Lcom/android/settings/security/LockscreenDashboardFragment;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "LockscreenDashboardFragment"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f1500ba

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x372

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/security/LockscreenDashboardFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v1

    new-instance v2, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    const-string/jumbo v3, "security_setting_lock_screen_notif"

    const-string/jumbo v4, "security_setting_lock_screen_notif_work_header"

    const-string/jumbo v5, "security_setting_lock_screen_notif_work"

    invoke-direct {v2, p1, v3, v4, v5}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;

    invoke-direct {v2, p1}, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/security/c;

    invoke-direct {v2, p1, p0, v1}, Lcom/android/settings/security/c;-><init>(Landroid/content/Context;Landroid/app/Fragment;Lcom/android/settings/core/lifecycle/c;)V

    iput-object v2, p0, Lcom/android/settings/security/LockscreenDashboardFragment;->bfO:Lcom/android/settings/security/c;

    iget-object v1, p0, Lcom/android/settings/security/LockscreenDashboardFragment;->bfO:Lcom/android/settings/security/c;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
