.class public Lcom/android/settings/security/c;
.super Lcom/android/settings/core/e;
.source "OwnerInfoPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;


# static fields
.field private static final bfP:I


# instance fields
.field private bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

.field private final bfR:Landroid/app/Fragment;

.field private final mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    sput v0, Lcom/android/settings/security/c;->bfP:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/Fragment;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/security/c;->bfR:Landroid/app/Fragment;

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/security/c;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    if-eqz p3, :cond_0

    invoke-virtual {p3, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method

.method static synthetic aUb(Lcom/android/settings/security/c;)Landroid/app/Fragment;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/security/c;->bfR:Landroid/app/Fragment;

    return-object v0
.end method


# virtual methods
.method public aUa()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/security/c;->isDeviceOwnerInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/security/c;->getDeviceOwner()Lcom/android/settingslib/n;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    iget-object v1, p0, Lcom/android/settings/security/c;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v2, Lcom/android/settings/security/c;->bfP:I

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->isLockScreenDisabled(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    new-instance v1, Lcom/android/settings/security/h;

    invoke-direct {v1, p0}, Lcom/android/settings/security/h;-><init>(Lcom/android/settings/security/c;)V

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method getDeviceOwner()Lcom/android/settingslib/n;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/security/c;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/w;->cqN(Landroid/content/Context;)Lcom/android/settingslib/n;

    move-result-object v0

    return-object v0
.end method

.method getDeviceOwnerInfo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/security/c;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->getDeviceOwnerInfo()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getOwnerInfo()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/security/c;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v1, Lcom/android/settings/security/c;->bfP:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getOwnerInfo(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    const-string/jumbo v0, "owner_info_settings"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedPreference;

    iput-object v0, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    return-void
.end method

.method isDeviceOwnerInfoEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/security/c;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isDeviceOwnerInfoEnabled()Z

    move-result v0

    return v0
.end method

.method isOwnerInfoEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/security/c;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v1, Lcom/android/settings/security/c;->bfP:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isOwnerInfoEnabled(I)Z

    move-result v0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "owner_info_settings"

    return-object v0
.end method

.method public onResume()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/security/c;->aUa()V

    invoke-virtual {p0}, Lcom/android/settings/security/c;->updateSummary()V

    return-void
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public updateSummary()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/security/c;->isDeviceOwnerInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {p0}, Lcom/android/settings/security/c;->getDeviceOwnerInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/security/c;->bfQ:Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {p0}, Lcom/android/settings/security/c;->isOwnerInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/security/c;->getOwnerInfo()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/security/c;->mContext:Landroid/content/Context;

    const v2, 0x7f120c35

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
