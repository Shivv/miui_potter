.class public Lcom/android/settings/widget/ChartSweepView;
.super Landroid/view/View;
.source "ChartSweepView.java"


# instance fields
.field private aQH:Lcom/android/settings/widget/k;

.field private aQI:Landroid/view/View$OnClickListener;

.field private aQJ:Landroid/graphics/Rect;

.field private aQK:J

.field private aQL:I

.field private aQM:I

.field private aQN:Landroid/text/DynamicLayout;

.field private aQO:I

.field private aQP:F

.field private aQQ:F

.field private aQR:Landroid/text/SpannableStringBuilder;

.field private aQS:I

.field private aQT:J

.field private aQU:Lcom/android/settings/widget/E;

.field private aQV:Landroid/graphics/Rect;

.field private aQW:F

.field private aQX:[Lcom/android/settings/widget/ChartSweepView;

.field private aQY:Landroid/graphics/Paint;

.field private aQZ:I

.field private aRa:Landroid/graphics/drawable/Drawable;

.field private aRb:Landroid/graphics/Point;

.field private aRc:Landroid/graphics/Rect;

.field private aRd:I

.field private aRe:Landroid/view/MotionEvent;

.field private aRf:F

.field private aRg:J

.field private aRh:Lcom/android/settings/widget/ChartSweepView;

.field private aRi:J

.field private aRj:Lcom/android/settings/widget/ChartSweepView;

.field private aRk:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/widget/ChartSweepView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/widget/ChartSweepView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQY:Landroid/graphics/Paint;

    iput v3, p0, Lcom/android/settings/widget/ChartSweepView;->aRd:I

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQK:J

    new-array v0, v3, [Lcom/android/settings/widget/ChartSweepView;

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQX:[Lcom/android/settings/widget/ChartSweepView;

    new-instance v0, Lcom/android/settings/widget/aj;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/aj;-><init>(Lcom/android/settings/widget/ChartSweepView;)V

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQI:Landroid/view/View$OnClickListener;

    sget-object v0, Lcom/android/settings/cw;->bYO:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const v1, -0xffff01

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/widget/ChartSweepView;->setSweepDrawable(Landroid/graphics/drawable/Drawable;I)V

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/ChartSweepView;->setFollowAxis(I)V

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/ChartSweepView;->setNeighborMargin(F)V

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/ChartSweepView;->setSafeRegion(I)V

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/ChartSweepView;->setLabelMinSize(I)V

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/ChartSweepView;->setLabelTemplate(I)V

    invoke-virtual {p0, v1}, Lcom/android/settings/widget/ChartSweepView;->setLabelColor(I)V

    const v1, 0x7f0800fc

    invoke-virtual {p0, v1}, Lcom/android/settings/widget/ChartSweepView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQY:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQY:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQY:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v4}, Lcom/android/settings/widget/ChartSweepView;->setClickable(Z)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQI:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartSweepView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v3}, Lcom/android/settings/widget/ChartSweepView;->setWillNotDraw(Z)V

    return-void
.end method

.method private aCf(Landroid/graphics/Rect;JJF)Landroid/graphics/Rect;
    .locals 10

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    instance-of v0, v0, Lcom/android/settings/widget/v;

    if-eqz v0, :cond_6

    :goto_0
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p4, v0

    if-eqz v0, :cond_2

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p4, v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, p2, v2

    if-eqz v0, :cond_3

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, p2, v2

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    invoke-interface {v2, p4, p5}, Lcom/android/settings/widget/k;->aAs(J)F

    move-result v2

    add-float v2, v2, p6

    iget-object v3, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    invoke-interface {v3, p2, p3}, Lcom/android/settings/widget/k;->aAs(J)F

    move-result v3

    sub-float v3, v3, p6

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget v5, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    if-eqz v0, :cond_0

    iget v0, v4, Landroid/graphics/Rect;->top:I

    float-to-int v3, v3

    add-int/2addr v0, v3

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    :cond_0
    if-eqz v1, :cond_1

    iget v0, v4, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v4, Landroid/graphics/Rect;->top:I

    :cond_1
    :goto_3
    return-object v4

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    if-eqz v0, :cond_5

    iget v0, v4, Landroid/graphics/Rect;->left:I

    float-to-int v3, v3

    add-int/2addr v0, v3

    iput v0, v4, Landroid/graphics/Rect;->right:I

    :cond_5
    if-eqz v1, :cond_1

    iget v0, v4, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v4, Landroid/graphics/Rect;->left:I

    goto :goto_3

    :cond_6
    move-wide v7, p4

    move-wide p4, p2

    move-wide p2, v7

    goto :goto_0
.end method

.method private aCg(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 8

    iget-wide v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRg:J

    iget-wide v4, p0, Lcom/android/settings/widget/ChartSweepView;->aRi:J

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/widget/ChartSweepView;->aCf(Landroid/graphics/Rect;JJF)Landroid/graphics/Rect;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->getValidAfterDynamic()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->getValidBeforeDynamic()J

    move-result-wide v4

    iget v6, p0, Lcom/android/settings/widget/ChartSweepView;->aQW:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/widget/ChartSweepView;->aCf(Landroid/graphics/Rect;JJF)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v7}, Landroid/graphics/Rect;->setEmpty()V

    :cond_0
    return-object v7
.end method

.method private aCh(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQU:Lcom/android/settings/widget/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQU:Lcom/android/settings/widget/E;

    invoke-interface {v0, p0, p1}, Lcom/android/settings/widget/E;->aCs(Lcom/android/settings/widget/ChartSweepView;Z)V

    :cond_0
    return-void
.end method

.method private aCi()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQU:Lcom/android/settings/widget/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQU:Lcom/android/settings/widget/E;

    invoke-interface {v0, p0}, Lcom/android/settings/widget/E;->aCt(Lcom/android/settings/widget/ChartSweepView;)V

    :cond_0
    return-void
.end method

.method public static aCj(Lcom/android/settings/widget/ChartSweepView;)F
    .locals 2

    invoke-static {p0}, Lcom/android/settings/widget/ChartSweepView;->aCk(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    invoke-virtual {v1}, Landroid/text/DynamicLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method

.method public static aCk(Lcom/android/settings/widget/ChartSweepView;)F
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method

.method public static aCl(Lcom/android/settings/widget/ChartSweepView;)F
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    invoke-virtual {v0}, Landroid/text/DynamicLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    invoke-virtual {v1}, Landroid/text/DynamicLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    return v0
.end method

.method private aCm(Landroid/view/MotionEvent;)F
    .locals 3

    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getX()F

    move-result v1

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->getTargetInset()F

    move-result v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getY()F

    move-result v1

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->getTargetInset()F

    move-result v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method private aCn()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQR:Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aQR:Landroid/text/SpannableStringBuilder;

    iget-wide v4, p0, Lcom/android/settings/widget/ChartSweepView;->aRk:J

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/android/settings/widget/k;->aAt(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQT:J

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQR:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartSweepView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->aCo()V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    iget-wide v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRk:J

    iput-wide v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQT:J

    goto :goto_0
.end method

.method private aCp()V
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x0

    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQS:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQS:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v2, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(I)V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, v2, Landroid/text/TextPaint;->density:F

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setCompatibilityScaling(F)V

    iget v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQM:I

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setColor(I)V

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQR:Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/DynamicLayout;

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQR:Landroid/text/SpannableStringBuilder;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    const/16 v3, 0x400

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->aCn()V

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->invalidate()V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->requestLayout()V

    return-void

    :cond_0
    iput-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQR:Landroid/text/SpannableStringBuilder;

    iput-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    goto :goto_0
.end method

.method static synthetic aCr(Lcom/android/settings/widget/ChartSweepView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->aCi()V

    return-void
.end method

.method private getParentContentRect()Landroid/graphics/Rect;
    .locals 6

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    sub-int v0, v5, v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method

.method private getTargetInset()F
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method

.method private getValidAfterDynamic()J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRh:Lcom/android/settings/widget/ChartSweepView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartSweepView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartSweepView;->getValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method private getValidBeforeDynamic()J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRj:Lcom/android/settings/widget/ChartSweepView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartSweepView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartSweepView;->getValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method


# virtual methods
.method public aCb(Lcom/android/settings/widget/E;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQU:Lcom/android/settings/widget/E;

    return-void
.end method

.method aCc(Lcom/android/settings/widget/k;)V
    .locals 1

    const-string/jumbo v0, "missing axis"

    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/k;

    iput-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    return-void
.end method

.method public aCd()I
    .locals 4

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/android/settings/widget/k;->aAv(J)I

    move-result v0

    return v0
.end method

.method public aCe()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->getParentContentRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float v0, v1, v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    invoke-interface {v1, v0}, Lcom/android/settings/widget/k;->aAr(F)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/widget/ChartSweepView;->setValue(J)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float v0, v1, v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    invoke-interface {v1, v0}, Lcom/android/settings/widget/k;->aAr(F)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/widget/ChartSweepView;->setValue(J)V

    goto :goto_0
.end method

.method public aCo()V
    .locals 4

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRh:Lcom/android/settings/widget/ChartSweepView;

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/android/settings/widget/ChartSweepView;->aCl(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRh:Lcom/android/settings/widget/ChartSweepView;

    invoke-static {v2}, Lcom/android/settings/widget/ChartSweepView;->aCl(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQQ:F

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRh:Lcom/android/settings/widget/ChartSweepView;

    invoke-static {v1}, Lcom/android/settings/widget/ChartSweepView;->aCk(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v1

    invoke-static {p0}, Lcom/android/settings/widget/ChartSweepView;->aCj(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v2

    sub-float/2addr v1, v2

    cmpg-float v2, v1, v0

    if-gez v2, :cond_0

    div-float v0, v1, v3

    :cond_0
    :goto_0
    iget v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQQ:F

    iget v2, p0, Lcom/android/settings/widget/ChartSweepView;->aQO:I

    int-to-float v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQQ:F

    iget v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQP:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    iput v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQP:F

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->invalidate()V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRh:Lcom/android/settings/widget/ChartSweepView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRh:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartSweepView;->aCo()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRj:Lcom/android/settings/widget/ChartSweepView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartSweepView;->aCo()V

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRj:Lcom/android/settings/widget/ChartSweepView;

    if-eqz v1, :cond_4

    invoke-static {p0}, Lcom/android/settings/widget/ChartSweepView;->aCl(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRj:Lcom/android/settings/widget/ChartSweepView;

    invoke-static {v2}, Lcom/android/settings/widget/ChartSweepView;->aCl(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQQ:F

    invoke-static {p0}, Lcom/android/settings/widget/ChartSweepView;->aCk(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRj:Lcom/android/settings/widget/ChartSweepView;

    invoke-static {v2}, Lcom/android/settings/widget/ChartSweepView;->aCj(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v2

    sub-float/2addr v1, v2

    cmpg-float v2, v1, v0

    if-gez v2, :cond_0

    neg-float v0, v1

    div-float/2addr v0, v3

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lcom/android/settings/widget/ChartSweepView;->aCl(Lcom/android/settings/widget/ChartSweepView;)F

    move-result v1

    iput v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQQ:F

    goto :goto_0
.end method

.method public aCq(Landroid/view/MotionEvent;Lcom/android/settings/widget/ChartSweepView;)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/widget/ChartSweepView;->aCm(Landroid/view/MotionEvent;)F

    move-result v0

    invoke-direct {p2, p1}, Lcom/android/settings/widget/ChartSweepView;->aCm(Landroid/view/MotionEvent;)F

    move-result v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
    .locals 0

    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_0
    return-void
.end method

.method public getAxis()Lcom/android/settings/widget/k;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    return-object v0
.end method

.method public getFollowAxis()I
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    return v0
.end method

.method public getLabelValue()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQT:J

    return-wide v0
.end method

.method public getMargins()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPoint()F
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    iget-wide v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRk:J

    invoke-interface {v0, v2, v3}, Lcom/android/settings/widget/k;->aAs(J)F

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getValue()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRk:J

    return-wide v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    iget v3, p0, Lcom/android/settings/widget/ChartSweepView;->aQQ:F

    const/high16 v4, 0x44800000    # 1024.0f

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v5, p0, Lcom/android/settings/widget/ChartSweepView;->aQP:F

    add-float/2addr v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    invoke-virtual {v3, p1}, Landroid/text/DynamicLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQQ:F

    float-to-int v0, v0

    iget v3, p0, Lcom/android/settings/widget/ChartSweepView;->aQZ:I

    add-int/2addr v0, v3

    :goto_0
    iget v3, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v4

    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v5

    invoke-virtual {v1, v3, v0, v4, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->aCo()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    invoke-virtual {v1}, Landroid/text/DynamicLayout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iput v5, v2, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iput v5, v2, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    div-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->getTargetInset()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/android/settings/widget/ChartSweepView;->setMeasuredDimension(II)V

    :goto_0
    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    neg-int v0, v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    :goto_1
    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x3

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/widget/ChartSweepView;->setMeasuredDimension(II)V

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, v1, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    :goto_2
    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->offset(II)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    neg-int v1, v1

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iput v5, v0, Landroid/graphics/Point;->x:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRb:Landroid/graphics/Point;

    iput v5, v0, Landroid/graphics/Point;->y:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/widget/ChartSweepView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    neg-int v0, v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iput v5, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_1

    :cond_2
    mul-int/lit8 v2, v1, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/widget/ChartSweepView;->setMeasuredDimension(II)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v5, v1}, Landroid/graphics/Rect;->offset(II)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQJ:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    goto/16 :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    const/4 v5, 0x0

    const/4 v10, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return v3

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    return v3

    :pswitch_0
    iget v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    if-ne v1, v2, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v5, v5, 0x8

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v1, v1, v4

    if-lez v1, :cond_1

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    invoke-virtual {v5}, Landroid/text/DynamicLayout;->getWidth()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    move v4, v1

    move v1, v2

    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getLeft()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getTop()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v6, v5, v7}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v7, p0, Lcom/android/settings/widget/ChartSweepView;->aQX:[Lcom/android/settings/widget/ChartSweepView;

    array-length v8, v7

    move v5, v3

    :goto_2
    if-ge v5, v8, :cond_9

    aget-object v9, v7, v5

    invoke-virtual {p0, v6, v9}, Lcom/android/settings/widget/ChartSweepView;->aCq(Landroid/view/MotionEvent;Lcom/android/settings/widget/ChartSweepView;)Z

    move-result v9

    if-eqz v9, :cond_8

    return v3

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v4, v1

    move v1, v3

    goto :goto_1

    :cond_3
    move v4, v1

    move v1, v3

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    mul-int/lit8 v5, v5, 0x8

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v1, v1, v4

    if-lez v1, :cond_5

    move v1, v2

    :goto_3
    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    if-eqz v4, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aQN:Landroid/text/DynamicLayout;

    invoke-virtual {v5}, Landroid/text/DynamicLayout;->getHeight()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_6

    move v4, v1

    move v1, v2

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_3

    :cond_6
    move v4, v1

    move v1, v3

    goto :goto_1

    :cond_7
    move v4, v1

    move v1, v3

    goto :goto_1

    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_9
    if-eqz v4, :cond_c

    iget v1, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    if-ne v1, v2, :cond_b

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getTop()I

    move-result v1

    iget-object v3, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iput v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRf:F

    :goto_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRe:Landroid/view/MotionEvent;

    iput v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRd:I

    invoke-virtual {v0}, Landroid/view/View;->isActivated()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v0, v2}, Landroid/view/View;->setActivated(Z)V

    :cond_a
    return v2

    :cond_b
    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getLeft()I

    move-result v1

    iget-object v3, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iput v1, p0, Lcom/android/settings/widget/ChartSweepView;->aRf:F

    goto :goto_4

    :cond_c
    if-eqz v1, :cond_d

    iput v10, p0, Lcom/android/settings/widget/ChartSweepView;->aRd:I

    return v2

    :cond_d
    iput v3, p0, Lcom/android/settings/widget/ChartSweepView;->aRd:I

    return v3

    :pswitch_1
    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRd:I

    if-ne v0, v10, :cond_e

    return v2

    :cond_e
    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->getParentContentRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/widget/ChartSweepView;->aCg(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_f

    return v2

    :cond_f
    iget v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    if-ne v4, v2, :cond_10

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getTop()I

    move-result v4

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, p0, Lcom/android/settings/widget/ChartSweepView;->aRf:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    iget-object v7, p0, Lcom/android/settings/widget/ChartSweepView;->aRe:Landroid/view/MotionEvent;

    invoke-virtual {v7}, Landroid/view/MotionEvent;->getRawY()F

    move-result v7

    sub-float/2addr v6, v7

    add-float/2addr v5, v6

    iget v6, v1, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    invoke-static {v5, v6, v1}, Landroid/util/MathUtils;->constrain(FFF)F

    move-result v1

    sub-float v4, v1, v4

    invoke-virtual {p0, v4}, Lcom/android/settings/widget/ChartSweepView;->setTranslationY(F)V

    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float v0, v1, v0

    invoke-interface {v4, v0}, Lcom/android/settings/widget/k;->aAr(F)J

    move-result-wide v0

    :goto_5
    iget-wide v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQK:J

    rem-long v4, v0, v4

    sub-long/2addr v0, v4

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/widget/ChartSweepView;->setValue(J)V

    invoke-direct {p0, v3}, Lcom/android/settings/widget/ChartSweepView;->aCh(Z)V

    return v2

    :cond_10
    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getLeft()I

    move-result v4

    iget-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aQV:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, p0, Lcom/android/settings/widget/ChartSweepView;->aRf:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    iget-object v7, p0, Lcom/android/settings/widget/ChartSweepView;->aRe:Landroid/view/MotionEvent;

    invoke-virtual {v7}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    sub-float/2addr v6, v7

    add-float/2addr v5, v6

    iget v6, v1, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    invoke-static {v5, v6, v1}, Landroid/util/MathUtils;->constrain(FFF)F

    move-result v1

    sub-float v4, v1, v4

    invoke-virtual {p0, v4}, Lcom/android/settings/widget/ChartSweepView;->setTranslationX(F)V

    iget-object v4, p0, Lcom/android/settings/widget/ChartSweepView;->aQH:Lcom/android/settings/widget/k;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float v0, v1, v0

    invoke-interface {v4, v0}, Lcom/android/settings/widget/k;->aAr(F)J

    move-result-wide v0

    goto :goto_5

    :pswitch_2
    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRd:I

    if-ne v0, v10, :cond_12

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->performClick()Z

    :cond_11
    :goto_6
    iput v3, p0, Lcom/android/settings/widget/ChartSweepView;->aRd:I

    return v2

    :cond_12
    iget v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRd:I

    if-ne v0, v2, :cond_11

    iput v4, p0, Lcom/android/settings/widget/ChartSweepView;->aRf:F

    iput-object v5, p0, Lcom/android/settings/widget/ChartSweepView;->aRe:Landroid/view/MotionEvent;

    iget-wide v0, p0, Lcom/android/settings/widget/ChartSweepView;->aQT:J

    iput-wide v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRk:J

    invoke-direct {p0, v2}, Lcom/android/settings/widget/ChartSweepView;->aCh(Z)V

    invoke-virtual {p0, v4}, Lcom/android/settings/widget/ChartSweepView;->setTranslationX(F)V

    invoke-virtual {p0, v4}, Lcom/android/settings/widget/ChartSweepView;->setTranslationY(F)V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->requestLayout()V

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
    .locals 0

    return-void
.end method

.method public setDragInterval(J)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQK:J

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/ChartSweepView;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->requestLayout()V

    return-void
.end method

.method public setFollowAxis(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQL:I

    return-void
.end method

.method public setLabelColor(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQM:I

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->aCp()V

    return-void
.end method

.method public setLabelMinSize(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQO:I

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->aCp()V

    return-void
.end method

.method public setLabelTemplate(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQS:I

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->aCp()V

    return-void
.end method

.method public setNeighborMargin(F)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQW:F

    return-void
.end method

.method public varargs setNeighbors([Lcom/android/settings/widget/ChartSweepView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQX:[Lcom/android/settings/widget/ChartSweepView;

    return-void
.end method

.method public setSafeRegion(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/ChartSweepView;->aQZ:I

    return-void
.end method

.method public setSweepDrawable(Landroid/graphics/drawable/Drawable;I)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartSweepView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p1, :cond_3

    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    iput-object p1, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRc:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/widget/ChartSweepView;->invalidate()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iput-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method public setValidRange(JJ)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/widget/ChartSweepView;->aRg:J

    iput-wide p3, p0, Lcom/android/settings/widget/ChartSweepView;->aRi:J

    return-void
.end method

.method public setValidRangeDynamic(Lcom/android/settings/widget/ChartSweepView;Lcom/android/settings/widget/ChartSweepView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/ChartSweepView;->aRh:Lcom/android/settings/widget/ChartSweepView;

    iput-object p2, p0, Lcom/android/settings/widget/ChartSweepView;->aRj:Lcom/android/settings/widget/ChartSweepView;

    return-void
.end method

.method public setValue(J)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/widget/ChartSweepView;->aRk:J

    invoke-direct {p0}, Lcom/android/settings/widget/ChartSweepView;->aCn()V

    return-void
.end method

.method public setVisibility(I)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ChartSweepView;->aRa:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
