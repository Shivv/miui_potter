.class final Lcom/android/settings/widget/C;
.super Ljava/lang/Object;
.source "SlidingTabLayout.java"

# interfaces
.implements Landroid/support/v4/view/j;


# instance fields
.field private aQE:I

.field final synthetic aQF:Lcom/android/settings/widget/SlidingTabLayout;


# direct methods
.method private constructor <init>(Lcom/android/settings/widget/SlidingTabLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/C;->aQF:Lcom/android/settings/widget/SlidingTabLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/widget/SlidingTabLayout;Lcom/android/settings/widget/C;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/widget/C;-><init>(Lcom/android/settings/widget/SlidingTabLayout;)V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/C;->aQE:I

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/C;->aQF:Lcom/android/settings/widget/SlidingTabLayout;

    invoke-static {v0}, Lcom/android/settings/widget/SlidingTabLayout;->aBX(Lcom/android/settings/widget/SlidingTabLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/C;->aQF:Lcom/android/settings/widget/SlidingTabLayout;

    invoke-static {v0, p1, p2}, Lcom/android/settings/widget/SlidingTabLayout;->aBZ(Lcom/android/settings/widget/SlidingTabLayout;IF)V

    return-void
.end method

.method public onPageSelected(I)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/C;->aQF:Lcom/android/settings/widget/SlidingTabLayout;

    invoke-static {v0}, Lcom/android/settings/widget/SlidingTabLayout;->aBY(Lcom/android/settings/widget/SlidingTabLayout;)Lcom/android/settings/widget/RtlCompatibleViewPager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/RtlCompatibleViewPager;->aAw(I)I

    move-result v3

    iget v0, p0, Lcom/android/settings/widget/C;->aQE:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/C;->aQF:Lcom/android/settings/widget/SlidingTabLayout;

    const/4 v2, 0x0

    invoke-static {v0, v3, v2}, Lcom/android/settings/widget/SlidingTabLayout;->aBZ(Lcom/android/settings/widget/SlidingTabLayout;IF)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/widget/C;->aQF:Lcom/android/settings/widget/SlidingTabLayout;

    invoke-static {v0}, Lcom/android/settings/widget/SlidingTabLayout;->aBX(Lcom/android/settings/widget/SlidingTabLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    iget-object v0, p0, Lcom/android/settings/widget/C;->aQF:Lcom/android/settings/widget/SlidingTabLayout;

    invoke-static {v0}, Lcom/android/settings/widget/SlidingTabLayout;->aBX(Lcom/android/settings/widget/SlidingTabLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-ne v3, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    return-void
.end method
