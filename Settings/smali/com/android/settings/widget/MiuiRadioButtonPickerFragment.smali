.class public abstract Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiRadioButtonPickerFragment.java"

# interfaces
.implements Lcom/android/settings/widget/D;
.implements Lcom/android/settings/core/instrumentation/f;


# static fields
.field static final EXTRA_FOR_WORK:Ljava/lang/String; = "for_work"


# instance fields
.field private final aOI:Ljava/util/Map;

.field protected mUserId:I

.field protected mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->aOI:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method protected aAB(Ljava/lang/String;)Lcom/android/settings/widget/r;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->aOI:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/r;

    return-object v0
.end method

.method protected aAC(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->mG(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->updateCheckedState(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->aAD(Z)V

    return-void
.end method

.method protected aAD(Z)V
    .locals 0

    return-void
.end method

.method public bindPreference(Lcom/android/settings/widget/MiuiRadioButtonPreference;Ljava/lang/String;Lcom/android/settings/widget/r;Ljava/lang/String;)Lcom/android/settings/widget/MiuiRadioButtonPreference;
    .locals 1

    invoke-virtual {p3}, Lcom/android/settings/widget/r;->mS()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p3}, Lcom/android/settings/widget/r;->ne()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, p2}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setKey(Ljava/lang/String;)V

    invoke-static {p4, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    :cond_0
    iget-boolean v0, p3, Lcom/android/settings/widget/r;->enabled:Z

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setEnabled(Z)V

    invoke-virtual {p1, p0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->aCa(Lcom/android/settings/widget/D;)V

    return-object p1
.end method

.method public bindPreferenceExtra(Lcom/android/settings/widget/MiuiRadioButtonPreference;Ljava/lang/String;Lcom/android/settings/widget/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected abstract mB()Ljava/lang/String;
.end method

.method protected abstract mG(Ljava/lang/String;)Z
.end method

.method protected mH()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public mayCheckOnlyRadioButton()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    instance-of v1, v0, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method protected abstract my()Ljava/util/List;
.end method

.method protected nN()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public ns(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V
    .locals 1

    invoke-virtual {p1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->aAC(Ljava/lang/String;)V

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onAttach(Landroid/content/Context;)V

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    const-string/jumbo v0, "for_work"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->mUserManager:Landroid/os/UserManager;

    invoke-static {v1}, Lcom/android/settings/aq;->bqQ(Landroid/os/UserManager;)Landroid/os/UserHandle;

    move-result-object v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->mUserId:I

    return-void

    :cond_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f1500a1

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->updateCandidates()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->setHasOptionsMenu(Z)V

    return-object v0
.end method

.method public updateCandidates()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->aOI:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->my()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/r;

    iget-object v2, p0, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->aOI:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/android/settings/widget/r;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->mB()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->nN()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-virtual {v6}, Landroid/preference/PreferenceScreen;->removeAll()V

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->mH()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080233

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setIcon(I)V

    const v1, 0x7f120138

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setTitle(I)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->aCa(Lcom/android/settings/widget/D;)V

    invoke-virtual {v6, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->aOI:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/settings/widget/MiuiRadioButtonPreference;-><init>(Landroid/content/Context;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/widget/r;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->bindPreference(Lcom/android/settings/widget/MiuiRadioButtonPreference;Ljava/lang/String;Lcom/android/settings/widget/r;Ljava/lang/String;)Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/widget/r;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->bindPreferenceExtra(Lcom/android/settings/widget/MiuiRadioButtonPreference;Ljava/lang/String;Lcom/android/settings/widget/r;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->mayCheckOnlyRadioButton()V

    return-void
.end method

.method public updateCheckedState(Ljava/lang/String;)V
    .locals 7

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    instance-of v0, v1, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    invoke-virtual {v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->isChecked()Z

    move-result v6

    if-eq v6, v5, :cond_0

    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method
