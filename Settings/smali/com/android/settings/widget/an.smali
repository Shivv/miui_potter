.class final Lcom/android/settings/widget/an;
.super Landroid/os/AsyncTask;
.source "SettingsAppWidgetProvider.java"


# instance fields
.field final synthetic aSo:Lcom/android/settings/widget/J;

.field final synthetic aSp:Z

.field final synthetic aSq:Z

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settings/widget/J;ZZLandroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/an;->aSo:Lcom/android/settings/widget/J;

    iput-boolean p2, p0, Lcom/android/settings/widget/an;->aSp:Z

    iput-boolean p3, p0, Lcom/android/settings/widget/an;->aSq:Z

    iput-object p4, p0, Lcom/android/settings/widget/an;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected aDb(Ljava/lang/Boolean;)V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/widget/an;->aSo:Lcom/android/settings/widget/J;

    iget-object v2, p0, Lcom/android/settings/widget/an;->val$context:Landroid/content/Context;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/android/settings/widget/J;->aCQ(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/settings/widget/an;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCB(Landroid/content/Context;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/settings/widget/an;->aSp:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/widget/an;->aSq:Z

    if-nez v0, :cond_0

    invoke-static {v2}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    :cond_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/widget/an;->aSq:Z

    if-eqz v0, :cond_2

    invoke-static {v1}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    :cond_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/an;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/an;->aDb(Ljava/lang/Boolean;)V

    return-void
.end method
