.class public Lcom/android/settings/widget/v;
.super Ljava/lang/Object;
.source "InvertedChartAxis.java"

# interfaces
.implements Lcom/android/settings/widget/k;


# instance fields
.field private aQb:F

.field private final aQc:Lcom/android/settings/widget/k;


# direct methods
.method public constructor <init>(Lcom/android/settings/widget/k;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/widget/v;->aQc:Lcom/android/settings/widget/k;

    return-void
.end method


# virtual methods
.method public aAq(F)Z
    .locals 1

    iput p1, p0, Lcom/android/settings/widget/v;->aQb:F

    iget-object v0, p0, Lcom/android/settings/widget/v;->aQc:Lcom/android/settings/widget/k;

    invoke-interface {v0, p1}, Lcom/android/settings/widget/k;->aAq(F)Z

    move-result v0

    return v0
.end method

.method public aAr(F)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/v;->aQc:Lcom/android/settings/widget/k;

    iget v1, p0, Lcom/android/settings/widget/v;->aQb:F

    sub-float/2addr v1, p1

    invoke-interface {v0, v1}, Lcom/android/settings/widget/k;->aAr(F)J

    move-result-wide v0

    return-wide v0
.end method

.method public aAs(J)F
    .locals 3

    iget v0, p0, Lcom/android/settings/widget/v;->aQb:F

    iget-object v1, p0, Lcom/android/settings/widget/v;->aQc:Lcom/android/settings/widget/k;

    invoke-interface {v1, p1, p2}, Lcom/android/settings/widget/k;->aAs(J)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public aAt(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;J)J
    .locals 3

    iget-object v0, p0, Lcom/android/settings/widget/v;->aQc:Lcom/android/settings/widget/k;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/settings/widget/k;->aAt(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public aAu()[F
    .locals 4

    iget-object v0, p0, Lcom/android/settings/widget/v;->aQc:Lcom/android/settings/widget/k;

    invoke-interface {v0}, Lcom/android/settings/widget/k;->aAu()[F

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    iget v2, p0, Lcom/android/settings/widget/v;->aQb:F

    aget v3, v1, v0

    sub-float/2addr v2, v3

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public aAv(J)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/v;->aQc:Lcom/android/settings/widget/k;

    invoke-interface {v0, p1, p2}, Lcom/android/settings/widget/k;->aAv(J)I

    move-result v0

    return v0
.end method

.method public setBounds(JJ)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/v;->aQc:Lcom/android/settings/widget/k;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/settings/widget/k;->setBounds(JJ)Z

    move-result v0

    return v0
.end method
