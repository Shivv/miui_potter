.class final Lcom/android/settings/widget/ac;
.super Ljava/lang/Object;
.source "ChartDataUsageView.java"

# interfaces
.implements Lcom/android/settings/widget/E;


# instance fields
.field final synthetic aRW:Lcom/android/settings/widget/ChartDataUsageView;


# direct methods
.method constructor <init>(Lcom/android/settings/widget/ChartDataUsageView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aCs(Lcom/android/settings/widget/ChartSweepView;Z)V
    .locals 2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0, p1}, Lcom/android/settings/widget/ChartDataUsageView;->aBe(Lcom/android/settings/widget/ChartDataUsageView;Lcom/android/settings/widget/ChartSweepView;)V

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBh(Lcom/android/settings/widget/ChartDataUsageView;)V

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBc(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/ChartSweepView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/widget/n;->aBk()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBb(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/ChartSweepView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/widget/n;->aBj()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/android/settings/widget/ChartDataUsageView;->aBf(Lcom/android/settings/widget/ChartDataUsageView;Lcom/android/settings/widget/ChartSweepView;Z)V

    goto :goto_0
.end method

.method public aCt(Lcom/android/settings/widget/ChartSweepView;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBc(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/ChartSweepView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/widget/n;->aBm()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBb(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/ChartSweepView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ac;->aRW:Lcom/android/settings/widget/ChartDataUsageView;

    invoke-static {v0}, Lcom/android/settings/widget/ChartDataUsageView;->aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/widget/n;->aBl()V

    goto :goto_0
.end method
