.class final Lcom/android/settings/widget/H;
.super Lcom/android/settings/widget/F;
.source "SettingsAppWidgetProvider.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/widget/F;-><init>(Lcom/android/settings/widget/F;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/widget/H;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/H;-><init>()V

    return-void
.end method

.method private static aCU(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x4

    return v0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const/4 v0, 0x1

    return v0

    :pswitch_2
    const/4 v0, 0x2

    return v0

    :pswitch_3
    const/4 v0, 0x3

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public aCG(Landroid/content/Context;)I
    .locals 2

    const/4 v1, 0x4

    invoke-static {}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCE()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/android/settings/bluetooth/Utils;->arq(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/q;

    move-result-object v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckl()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCF(Lcom/android/settingslib/bluetooth/d;)Lcom/android/settingslib/bluetooth/d;

    invoke-static {}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCE()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-static {}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCE()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    invoke-static {v0}, Lcom/android/settings/widget/H;->aCU(I)I

    move-result v0

    return v0
.end method

.method public aCH()I
    .locals 1

    const v0, 0x7f1207ab

    return v0
.end method

.method public aCI()I
    .locals 1

    const v0, 0x7f0a01fd

    return v0
.end method

.method public aCJ(Z)I
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f080178

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f080177

    goto :goto_0
.end method

.method public aCL()I
    .locals 1

    const v0, 0x7f0a0203

    return v0
.end method

.method public aCO(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string/jumbo v0, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string/jumbo v0, "android.bluetooth.adapter.extra.STATE"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/android/settings/widget/H;->aCU(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/widget/H;->aCQ(Landroid/content/Context;I)V

    return-void
.end method

.method protected aCP(Landroid/content/Context;Z)V
    .locals 2

    invoke-static {}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCE()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SettingsAppWidgetProvider"

    const-string/jumbo v1, "No LocalBluetoothManager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/widget/al;

    invoke-direct {v0, p0, p2}, Lcom/android/settings/widget/al;-><init>(Lcom/android/settings/widget/H;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/al;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public getContainerId()I
    .locals 1

    const v0, 0x7f0a00a6

    return v0
.end method
