.class final Lcom/android/settings/widget/J;
.super Lcom/android/settings/widget/F;
.source "SettingsAppWidgetProvider.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/widget/F;-><init>(Lcom/android/settings/widget/F;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/widget/J;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/J;-><init>()V

    return-void
.end method


# virtual methods
.method public aCG(Landroid/content/Context;)I
    .locals 1

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aCH()I
    .locals 1

    const v0, 0x7f1207b8

    return v0
.end method

.method public aCI()I
    .locals 1

    const v0, 0x7f0a0201

    return v0
.end method

.method public aCJ(Z)I
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f080183

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f080182

    goto :goto_0
.end method

.method public aCL()I
    .locals 1

    const v0, 0x7f0a0206

    return v0
.end method

.method public aCO(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/J;->aCG(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/widget/J;->aCQ(Landroid/content/Context;I)V

    return-void
.end method

.method public aCP(Landroid/content/Context;Z)V
    .locals 2

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    new-instance v1, Lcom/android/settings/widget/an;

    invoke-direct {v1, p0, p2, v0, p1}, Lcom/android/settings/widget/an;-><init>(Lcom/android/settings/widget/J;ZZLandroid/content/Context;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/an;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public getContainerId()I
    .locals 1

    const v0, 0x7f0a00ad

    return v0
.end method
