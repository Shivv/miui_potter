.class public Lcom/android/settings/widget/DonutView;
.super Landroid/view/View;
.source "DonutView.java"


# instance fields
.field private aQp:Landroid/graphics/Paint;

.field private aQq:Landroid/text/TextPaint;

.field private aQr:F

.field private aQs:Landroid/graphics/Paint;

.field private aQt:Ljava/lang/String;

.field private aQu:I

.field private aQv:Ljava/lang/String;

.field private aQw:F

.field private aQx:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/android/settings/widget/DonutView;->aQr:F

    iget v0, p0, Lcom/android/settings/widget/DonutView;->aQr:F

    const/high16 v1, 0x40c00000    # 6.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    const v1, 0x1010435

    invoke-static {p1, v1}, Lcom/android/settings/aq;->cqJ(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/settings/widget/DonutView;->aQp:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQp:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQp:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQp:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQp:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQp:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQp:Landroid/graphics/Paint;

    const v2, 0x7f0600a8

    invoke-virtual {p1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/settings/widget/DonutView;->aQs:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQs:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQs:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQs:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQs:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQs:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/settings/widget/DonutView;->mContext:Landroid/content/Context;

    const v3, 0x7f0600a9

    invoke-static {v2, v3}, Lcom/android/settings/aq;->cqy(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQs:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/aq;->cqI(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    const v2, 0x7f070229

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, Lcom/android/settings/widget/DonutView;->aQq:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQq:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/aq;->cqI(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQq:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/settings/widget/DonutView;->aQq:Landroid/text/TextPaint;

    const v2, 0x7f07022a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/settings/widget/DonutView;->aQq:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    return-void
.end method

.method private aBR(Landroid/graphics/Canvas;)V
    .locals 10

    const/4 v7, 0x0

    const/high16 v5, -0x3d4c0000    # -90.0f

    const/4 v9, 0x0

    iget v0, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    add-float v1, v9, v0

    iget v0, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    add-float v2, v9, v0

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    sub-float v3, v0, v3

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v4, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    sub-float v4, v0, v4

    const/high16 v6, 0x43b40000    # 360.0f

    iget-object v8, p0, Lcom/android/settings/widget/DonutView;->aQp:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v8}, Landroid/graphics/Canvas;->drawArc(FFFFFFZLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    add-float v1, v9, v0

    iget v0, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    add-float v2, v9, v0

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    sub-float v3, v0, v3

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v4, p0, Lcom/android/settings/widget/DonutView;->aQw:F

    sub-float v4, v0, v4

    iget v0, p0, Lcom/android/settings/widget/DonutView;->aQu:I

    mul-int/lit16 v0, v0, 0x168

    div-int/lit8 v0, v0, 0x64

    int-to-float v6, v0

    iget-object v8, p0, Lcom/android/settings/widget/DonutView;->aQs:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v8}, Landroid/graphics/Canvas;->drawArc(FFFFFFZLandroid/graphics/Paint;)V

    return-void
.end method

.method private aBS(Landroid/graphics/Canvas;)V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    invoke-direct {p0, v2}, Lcom/android/settings/widget/DonutView;->aBT(Landroid/text/TextPaint;)F

    move-result v2

    iget-object v3, p0, Lcom/android/settings/widget/DonutView;->aQq:Landroid/text/TextPaint;

    invoke-direct {p0, v3}, Lcom/android/settings/widget/DonutView;->aBT(Landroid/text/TextPaint;)F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/android/settings/widget/DonutView;->aQv:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    invoke-direct {p0, v3}, Lcom/android/settings/widget/DonutView;->aBT(Landroid/text/TextPaint;)F

    move-result v3

    sub-float v3, v1, v3

    iget-object v4, p0, Lcom/android/settings/widget/DonutView;->aQq:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->descent()F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/settings/widget/DonutView;->aQq:Landroid/text/TextPaint;

    invoke-virtual {p1, v2, v0, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/android/settings/widget/DonutView;->aQt:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private aBT(Landroid/text/TextPaint;)F
    .locals 2

    invoke-virtual {p1}, Landroid/text/TextPaint;->descent()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/settings/widget/DonutView;->aBR(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/settings/widget/DonutView;->aBS(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public setPercentage(I)V
    .locals 3

    iput p1, p0, Lcom/android/settings/widget/DonutView;->aQu:I

    iget v0, p0, Lcom/android/settings/widget/DonutView;->aQu:I

    invoke-static {v0}, Lcom/android/settings/aq;->cqH(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/DonutView;->aQv:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12119f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/DonutView;->aQt:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/widget/DonutView;->aQt:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DonutView;->aQx:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07022b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/widget/DonutView;->invalidate()V

    return-void
.end method
