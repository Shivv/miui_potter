.class public Lcom/android/settings/widget/ChartDataUsageView;
.super Lcom/android/settings/widget/ChartView;
.source "ChartDataUsageView.java"


# instance fields
.field private aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

.field private aPc:Lcom/android/settings/widget/ChartGridView;

.field private aPd:Landroid/os/Handler;

.field private aPe:J

.field private aPf:J

.field private aPg:Lcom/android/settings/widget/n;

.field private aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

.field private aPi:Lcom/android/settings/widget/ChartSweepView;

.field private aPj:Lcom/android/settings/widget/ChartSweepView;

.field private aPk:Lcom/android/settings/widget/E;

.field private aPl:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/widget/ChartDataUsageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/widget/ChartDataUsageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/widget/ChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/settings/widget/ac;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/ac;-><init>(Lcom/android/settings/widget/ChartDataUsageView;)V

    iput-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPk:Lcom/android/settings/widget/E;

    new-instance v0, Lcom/android/settings/widget/o;

    invoke-direct {v0}, Lcom/android/settings/widget/o;-><init>()V

    new-instance v1, Lcom/android/settings/widget/v;

    new-instance v2, Lcom/android/settings/widget/p;

    invoke-direct {v2}, Lcom/android/settings/widget/p;-><init>()V

    invoke-direct {v1, v2}, Lcom/android/settings/widget/v;-><init>(Lcom/android/settings/widget/k;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/widget/ChartDataUsageView;->ayL(Lcom/android/settings/widget/k;Lcom/android/settings/widget/k;)V

    new-instance v0, Lcom/android/settings/widget/ad;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/ad;-><init>(Lcom/android/settings/widget/ChartDataUsageView;)V

    iput-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPd:Landroid/os/Handler;

    return-void
.end method

.method private aAU(Lcom/android/settings/widget/ChartSweepView;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPd:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    return-void
.end method

.method private static aAV(J)J
    .locals 6

    const-wide/16 v4, 0x1

    sub-long v0, p0, v4

    const/4 v2, 0x1

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    const/4 v2, 0x2

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    const/4 v2, 0x4

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    const/16 v2, 0x8

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    const/16 v2, 0x10

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    add-long/2addr v0, v4

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method private aAW(Lcom/android/settings/widget/ChartSweepView;Z)V
    .locals 4

    const/16 v2, 0x64

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPd:Landroid/os/Handler;

    invoke-virtual {v0, v2, p1}, Landroid/os/Handler;->hasMessages(ILjava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPd:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPd:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    return-void
.end method

.method private aAX()V
    .locals 8

    const-wide v0, 0x7fffffffffffffffL

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v2}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getMaxEstimate()J

    move-result-wide v4

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v2}, Lcom/android/settings/widget/ChartSweepView;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v2}, Lcom/android/settings/widget/ChartSweepView;->getValue()J

    move-result-wide v2

    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-gez v6, :cond_2

    :goto_1
    const-wide/16 v2, 0x7

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xa

    div-long/2addr v0, v2

    cmp-long v0, v4, v0

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setEstimateVisible(Z)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v2}, Lcom/android/settings/widget/ChartSweepView;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v2}, Lcom/android/settings/widget/ChartSweepView;->getValue()J

    move-result-wide v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move-wide v0, v2

    goto :goto_1

    :cond_3
    move-wide v2, v0

    goto :goto_0
.end method

.method private aAY()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setSecondary(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setSecondary(Z)V

    goto :goto_0
.end method

.method private aAZ(Lcom/android/settings/widget/ChartSweepView;)V
    .locals 12

    const-wide/16 v10, 0xa

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPl:J

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/android/settings/widget/ChartSweepView;->aCd()I

    move-result v4

    if-lez v4, :cond_5

    const-wide/16 v4, 0xb

    mul-long/2addr v0, v4

    div-long/2addr v0, v10

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v4}, Lcom/android/settings/widget/ChartSweepView;->getValue()J

    move-result-wide v4

    iget-object v6, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v6}, Lcom/android/settings/widget/ChartSweepView;->getValue()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iget-object v6, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v6}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getMaxVisible()J

    move-result-wide v6

    iget-object v8, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v8}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getMaxVisible()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    const-wide/16 v6, 0xc

    mul-long/2addr v4, v6

    div-long/2addr v4, v10

    const-wide/32 v6, 0x3200000

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-wide v4, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPl:J

    cmp-long v4, v0, v4

    if-eqz v4, :cond_4

    iput-wide v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPl:J

    iget-object v4, p0, Lcom/android/settings/widget/ChartDataUsageView;->aMO:Lcom/android/settings/widget/k;

    invoke-interface {v4, v2, v3, v0, v1}, Lcom/android/settings/widget/k;->setBounds(JJ)Z

    move-result v4

    iget-object v5, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v5, v2, v3, v0, v1}, Lcom/android/settings/widget/ChartSweepView;->setValidRange(JJ)V

    iget-object v5, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v5, v2, v3, v0, v1}, Lcom/android/settings/widget/ChartSweepView;->setValidRange(JJ)V

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->azM()V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->azM()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPc:Lcom/android/settings/widget/ChartGridView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartGridView;->invalidate()V

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/widget/ChartSweepView;->aCe()V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    if-eq v0, p1, :cond_3

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartDataUsageView;->ayM(Lcom/android/settings/widget/ChartSweepView;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    if-eq v0, p1, :cond_4

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartDataUsageView;->ayM(Lcom/android/settings/widget/ChartSweepView;)V

    :cond_4
    return-void

    :cond_5
    if-gez v4, :cond_0

    const-wide/16 v4, 0x9

    mul-long/2addr v0, v4

    div-long/2addr v0, v10

    goto :goto_0

    :cond_6
    move-wide v0, v2

    goto :goto_0
.end method

.method static synthetic aBa(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/n;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPg:Lcom/android/settings/widget/n;

    return-object v0
.end method

.method static synthetic aBb(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/ChartSweepView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    return-object v0
.end method

.method static synthetic aBc(Lcom/android/settings/widget/ChartDataUsageView;)Lcom/android/settings/widget/ChartSweepView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    return-object v0
.end method

.method static synthetic aBd(J)J
    .locals 2

    invoke-static {p0, p1}, Lcom/android/settings/widget/ChartDataUsageView;->aAV(J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic aBe(Lcom/android/settings/widget/ChartDataUsageView;Lcom/android/settings/widget/ChartSweepView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/widget/ChartDataUsageView;->aAU(Lcom/android/settings/widget/ChartSweepView;)V

    return-void
.end method

.method static synthetic aBf(Lcom/android/settings/widget/ChartDataUsageView;Lcom/android/settings/widget/ChartSweepView;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/ChartDataUsageView;->aAW(Lcom/android/settings/widget/ChartSweepView;Z)V

    return-void
.end method

.method static synthetic aBg(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/android/settings/widget/ChartDataUsageView;->setText(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic aBh(Lcom/android/settings/widget/ChartDataUsageView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/ChartDataUsageView;->aAX()V

    return-void
.end method

.method static synthetic aBi(Lcom/android/settings/widget/ChartDataUsageView;Lcom/android/settings/widget/ChartSweepView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/widget/ChartDataUsageView;->aAZ(Lcom/android/settings/widget/ChartSweepView;)V

    return-void
.end method

.method private static setText(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-static {p0, p3}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    const/16 v2, 0x12

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    invoke-virtual {p0, v1, v0, p2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    return-void
.end method


# virtual methods
.method public getInspectEnd()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPe:J

    return-wide v0
.end method

.method public getInspectStart()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPf:J

    return-wide v0
.end method

.method public getLimitBytes()J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartSweepView;->getLabelValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getWarningBytes()J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartSweepView;->getLabelValue()J

    move-result-wide v0

    return-wide v0
.end method

.method protected onFinishInflate()V
    .locals 8

    const-wide/32 v6, 0x500000

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/android/settings/widget/ChartView;->onFinishInflate()V

    const v0, 0x7f0a01cc

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartDataUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/ChartGridView;

    iput-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPc:Lcom/android/settings/widget/ChartGridView;

    const v0, 0x7f0a03db

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartDataUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/ChartNetworkSeriesView;

    iput-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    const v0, 0x7f0a0135

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartDataUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/ChartNetworkSeriesView;

    iput-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setVisibility(I)V

    const v0, 0x7f0a0469

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartDataUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/ChartSweepView;

    iput-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    const v0, 0x7f0a046a

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/ChartDataUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/ChartSweepView;

    iput-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v0, v2, v1}, Lcom/android/settings/widget/ChartSweepView;->setValidRangeDynamic(Lcom/android/settings/widget/ChartSweepView;Lcom/android/settings/widget/ChartSweepView;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/widget/ChartSweepView;->setValidRangeDynamic(Lcom/android/settings/widget/ChartSweepView;Lcom/android/settings/widget/ChartSweepView;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    new-array v1, v4, [Lcom/android/settings/widget/ChartSweepView;

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartSweepView;->setNeighbors([Lcom/android/settings/widget/ChartSweepView;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    new-array v1, v4, [Lcom/android/settings/widget/ChartSweepView;

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartSweepView;->setNeighbors([Lcom/android/settings/widget/ChartSweepView;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPk:Lcom/android/settings/widget/E;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartSweepView;->aCb(Lcom/android/settings/widget/E;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPk:Lcom/android/settings/widget/E;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartSweepView;->aCb(Lcom/android/settings/widget/E;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v0, v6, v7}, Lcom/android/settings/widget/ChartSweepView;->setDragInterval(J)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    invoke-virtual {v0, v6, v7}, Lcom/android/settings/widget/ChartSweepView;->setDragInterval(J)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPc:Lcom/android/settings/widget/ChartGridView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aML:Lcom/android/settings/widget/k;

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aMO:Lcom/android/settings/widget/k;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/widget/ChartGridView;->aBD(Lcom/android/settings/widget/k;Lcom/android/settings/widget/k;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aML:Lcom/android/settings/widget/k;

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aMO:Lcom/android/settings/widget/k;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/widget/ChartNetworkSeriesView;->azL(Lcom/android/settings/widget/k;Lcom/android/settings/widget/k;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aML:Lcom/android/settings/widget/k;

    iget-object v2, p0, Lcom/android/settings/widget/ChartDataUsageView;->aMO:Lcom/android/settings/widget/k;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/widget/ChartNetworkSeriesView;->azL(Lcom/android/settings/widget/k;Lcom/android/settings/widget/k;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPj:Lcom/android/settings/widget/ChartSweepView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aMO:Lcom/android/settings/widget/k;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartSweepView;->aCc(Lcom/android/settings/widget/k;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPi:Lcom/android/settings/widget/ChartSweepView;

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aMO:Lcom/android/settings/widget/k;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/ChartSweepView;->aCc(Lcom/android/settings/widget/k;)V

    invoke-virtual {p0, v3}, Lcom/android/settings/widget/ChartDataUsageView;->setActivated(Z)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartDataUsageView;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    return v2

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return v2

    :pswitch_0
    return v1

    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/android/settings/widget/ChartDataUsageView;->setActivated(Z)V

    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setListener(Lcom/android/settings/widget/n;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPg:Lcom/android/settings/widget/n;

    return-void
.end method

.method public setVisibleRange(JJ)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aML:Lcom/android/settings/widget/k;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/settings/widget/k;->setBounds(JJ)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPc:Lcom/android/settings/widget/ChartGridView;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/settings/widget/ChartGridView;->setBounds(JJ)V

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setBounds(JJ)V

    iget-object v1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setBounds(JJ)V

    iput-wide p1, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPf:J

    iput-wide p3, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPe:J

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartDataUsageView;->requestLayout()V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPh:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->azM()V

    iget-object v0, p0, Lcom/android/settings/widget/ChartDataUsageView;->aPb:Lcom/android/settings/widget/ChartNetworkSeriesView;

    invoke-virtual {v0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->azM()V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/widget/ChartDataUsageView;->aAZ(Lcom/android/settings/widget/ChartSweepView;)V

    invoke-direct {p0}, Lcom/android/settings/widget/ChartDataUsageView;->aAX()V

    invoke-direct {p0}, Lcom/android/settings/widget/ChartDataUsageView;->aAY()V

    return-void
.end method
