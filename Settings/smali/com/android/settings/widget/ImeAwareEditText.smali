.class public Lcom/android/settings/widget/ImeAwareEditText;
.super Landroid/widget/EditText;
.source "ImeAwareEditText.java"


# instance fields
.field private aPZ:Z

.field final aQa:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/settings/widget/-$Lambda$dHG5DGYOImHBpTaqZZopKeGJGmI;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/android/settings/widget/-$Lambda$dHG5DGYOImHBpTaqZZopKeGJGmI;-><init>(BLjava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/widget/ImeAwareEditText;->aQa:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/settings/widget/-$Lambda$dHG5DGYOImHBpTaqZZopKeGJGmI;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/android/settings/widget/-$Lambda$dHG5DGYOImHBpTaqZZopKeGJGmI;-><init>(BLjava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/widget/ImeAwareEditText;->aQa:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/settings/widget/-$Lambda$dHG5DGYOImHBpTaqZZopKeGJGmI;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lcom/android/settings/widget/-$Lambda$dHG5DGYOImHBpTaqZZopKeGJGmI;-><init>(BLjava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/widget/ImeAwareEditText;->aQa:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Lcom/android/settings/widget/-$Lambda$dHG5DGYOImHBpTaqZZopKeGJGmI;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0}, Lcom/android/settings/widget/-$Lambda$dHG5DGYOImHBpTaqZZopKeGJGmI;-><init>(BLjava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/widget/ImeAwareEditText;->aQa:Ljava/lang/Runnable;

    return-void
.end method

.method private aBI()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settings/widget/ImeAwareEditText;->aPZ:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/widget/ImeAwareEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, p0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    iput-boolean v2, p0, Lcom/android/settings/widget/ImeAwareEditText;->aPZ:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public aBH()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/widget/ImeAwareEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/android/settings/widget/ImeAwareEditText;->aPZ:Z

    iget-object v1, p0, Lcom/android/settings/widget/ImeAwareEditText;->aQa:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/android/settings/widget/ImeAwareEditText;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-virtual {v0, p0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/ImeAwareEditText;->aPZ:Z

    return-void
.end method

.method synthetic aBJ()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/ImeAwareEditText;->aBI()V

    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/widget/ImeAwareEditText;->aPZ:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/ImeAwareEditText;->aQa:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/android/settings/widget/ImeAwareEditText;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/android/settings/widget/ImeAwareEditText;->aQa:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/android/settings/widget/ImeAwareEditText;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-object v0
.end method
