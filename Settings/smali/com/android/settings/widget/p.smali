.class public Lcom/android/settings/widget/p;
.super Ljava/lang/Object;
.source "ChartDataUsageView.java"

# interfaces
.implements Lcom/android/settings/widget/k;


# static fields
.field private static final aPt:Ljava/lang/Object;

.field private static final aPu:Ljava/lang/Object;


# instance fields
.field private aPq:J

.field private aPr:J

.field private aPs:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/settings/widget/p;->aPt:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/settings/widget/p;->aPu:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aAq(F)Z
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/p;->aPs:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/android/settings/widget/p;->aPs:F

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public aAr(F)J
    .locals 6

    iget-wide v0, p0, Lcom/android/settings/widget/p;->aPr:J

    long-to-float v0, v0

    iget-wide v2, p0, Lcom/android/settings/widget/p;->aPq:J

    iget-wide v4, p0, Lcom/android/settings/widget/p;->aPr:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/android/settings/widget/p;->aPs:F

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-long v0, v0

    return-wide v0
.end method

.method public aAs(J)F
    .locals 7

    iget v0, p0, Lcom/android/settings/widget/p;->aPs:F

    iget-wide v2, p0, Lcom/android/settings/widget/p;->aPr:J

    sub-long v2, p1, v2

    long-to-float v1, v2

    mul-float/2addr v0, v1

    iget-wide v2, p0, Lcom/android/settings/widget/p;->aPq:J

    iget-wide v4, p0, Lcom/android/settings/widget/p;->aPr:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public aAt(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;J)J
    .locals 7

    const-wide/16 v2, 0x0

    const-wide v4, 0x10000000000L

    move-wide v0, p3

    invoke-static/range {v0 .. v5}, Landroid/util/MathUtils;->constrain(JJJ)J

    move-result-wide v0

    const/4 v2, 0x3

    invoke-static {p1, v0, v1, v2}, Landroid/text/format/Formatter;->formatBytes(Landroid/content/res/Resources;JI)Landroid/text/format/Formatter$BytesResult;

    move-result-object v0

    sget-object v1, Lcom/android/settings/widget/p;->aPt:Ljava/lang/Object;

    iget-object v2, v0, Landroid/text/format/Formatter$BytesResult;->value:Ljava/lang/String;

    const-string/jumbo v3, "^1"

    invoke-static {p2, v1, v2, v3}, Lcom/android/settings/widget/ChartDataUsageView;->aBg(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/String;)V

    sget-object v1, Lcom/android/settings/widget/p;->aPu:Ljava/lang/Object;

    iget-object v2, v0, Landroid/text/format/Formatter$BytesResult;->units:Ljava/lang/String;

    const-string/jumbo v3, "^2"

    invoke-static {p2, v1, v2, v3}, Lcom/android/settings/widget/ChartDataUsageView;->aBg(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/String;)V

    iget-wide v0, v0, Landroid/text/format/Formatter$BytesResult;->roundedBytes:J

    return-wide v0
.end method

.method public aAu()[F
    .locals 7

    iget-wide v0, p0, Lcom/android/settings/widget/p;->aPq:J

    iget-wide v2, p0, Lcom/android/settings/widget/p;->aPr:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x10

    div-long v2, v0, v2

    invoke-static {v2, v3}, Lcom/android/settings/widget/ChartDataUsageView;->aBd(J)J

    move-result-wide v4

    div-long/2addr v0, v4

    long-to-int v0, v0

    new-array v1, v0, [F

    iget-wide v2, p0, Lcom/android/settings/widget/p;->aPr:J

    const/4 v0, 0x0

    :goto_0
    array-length v6, v1

    if-ge v0, v6, :cond_0

    invoke-virtual {p0, v2, v3}, Lcom/android/settings/widget/p;->aAs(J)F

    move-result v6

    aput v6, v1, v0

    add-long/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public aAv(J)I
    .locals 9

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/widget/p;->aAs(J)F

    move-result v0

    float-to-double v2, v0

    iget v1, p0, Lcom/android/settings/widget/p;->aPs:F

    float-to-double v4, v1

    const-wide v6, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v4, v6

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    float-to-double v0, v0

    iget v2, p0, Lcom/android/settings/widget/p;->aPs:F

    float-to-double v2, v2

    const-wide v4, 0x3feb333333333333L    # 0.85

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/android/settings/widget/p;->aPr:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-wide v2, p0, Lcom/android/settings/widget/p;->aPq:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget v1, p0, Lcom/android/settings/widget/p;->aPs:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public setBounds(JJ)Z
    .locals 3

    iget-wide v0, p0, Lcom/android/settings/widget/p;->aPr:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/settings/widget/p;->aPq:J

    cmp-long v0, v0, p3

    if-eqz v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lcom/android/settings/widget/p;->aPr:J

    iput-wide p3, p0, Lcom/android/settings/widget/p;->aPq:J

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
