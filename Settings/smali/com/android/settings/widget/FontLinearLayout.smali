.class public Lcom/android/settings/widget/FontLinearLayout;
.super Landroid/widget/LinearLayout;
.source "FontLinearLayout.java"


# instance fields
.field private aQf:Z

.field private aQg:Lcom/android/settings/widget/y;

.field private aQh:F

.field private aQi:I

.field private aQj:F

.field private aQk:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQi:I

    return-void
.end method

.method private aBN(Landroid/view/MotionEvent;)Z
    .locals 4

    iget v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQk:F

    iget v1, p0, Lcom/android/settings/widget/FontLinearLayout;->aQj:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/android/settings/widget/FontLinearLayout;->aQi:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/widget/FontLinearLayout;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/android/settings/widget/FontLinearLayout;->mPaddingLeft:I

    sub-int v1, v0, v1

    iget v2, p0, Lcom/android/settings/widget/FontLinearLayout;->mPaddingRight:I

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iget v3, p0, Lcom/android/settings/widget/FontLinearLayout;->mPaddingLeft:I

    if-ge v2, v3, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQh:F

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iget v3, p0, Lcom/android/settings/widget/FontLinearLayout;->mPaddingRight:I

    sub-int/2addr v0, v3

    if-le v2, v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQh:F

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/settings/widget/FontLinearLayout;->mPaddingLeft:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQh:F

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method aBL()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQf:Z

    return-void
.end method

.method aBM()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQf:Z

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQj:F

    invoke-virtual {p0}, Lcom/android/settings/widget/FontLinearLayout;->aBL()V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQf:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQk:F

    invoke-virtual {p0}, Lcom/android/settings/widget/FontLinearLayout;->aBM()V

    invoke-direct {p0, p1}, Lcom/android/settings/widget/FontLinearLayout;->aBN(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQg:Lcom/android/settings/widget/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/FontLinearLayout;->aQg:Lcom/android/settings/widget/y;

    iget v1, p0, Lcom/android/settings/widget/FontLinearLayout;->aQh:F

    invoke-interface {v0, p0, v1}, Lcom/android/settings/widget/y;->Ol(Lcom/android/settings/widget/FontLinearLayout;F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnFontLinearLayoutClickListener(Lcom/android/settings/widget/y;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/FontLinearLayout;->aQg:Lcom/android/settings/widget/y;

    return-void
.end method
