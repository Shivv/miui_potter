.class public Lcom/android/settings/widget/MiuiMasterSwitchPreference;
.super Lcom/android/settingslib/MiuiTwoTargetPreference;
.source "MiuiMasterSwitchPreference.java"


# instance fields
.field private aMQ:Z

.field private aMR:Z

.field private aMS:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settingslib/MiuiTwoTargetPreference;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMR:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/MiuiTwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMR:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/MiuiTwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMR:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/MiuiTwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMR:Z

    return-void
.end method

.method static synthetic ayQ(Lcom/android/settings/widget/MiuiMasterSwitchPreference;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMQ:Z

    return v0
.end method

.method static synthetic ayR(Lcom/android/settings/widget/MiuiMasterSwitchPreference;)Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic ayS(Lcom/android/settings/widget/MiuiMasterSwitchPreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic ayT(Lcom/android/settings/widget/MiuiMasterSwitchPreference;Z)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->persistBoolean(Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public ayO(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMR:Z

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected ayP()I
    .locals 1

    const v0, 0x7f0d0159

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiTwoTargetPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x1020018

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/widget/N;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/N;-><init>(Lcom/android/settings/widget/MiuiMasterSwitchPreference;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f0a046c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMQ:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMR:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMQ:Z

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->aMS:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_0
    return-void
.end method
