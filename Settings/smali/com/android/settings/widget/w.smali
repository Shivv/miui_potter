.class public abstract Lcom/android/settings/widget/w;
.super Ljava/lang/Object;
.source "SummaryUpdater.java"


# instance fields
.field private final aQd:Lcom/android/settings/widget/x;

.field private aQe:Ljava/lang/String;

.field protected final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/widget/x;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/widget/w;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/widget/w;->aQd:Lcom/android/settings/widget/x;

    return-void
.end method


# virtual methods
.method protected aBK()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/widget/w;->getSummary()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/widget/w;->aQe:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/android/settings/widget/w;->aQe:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/widget/w;->aQd:Lcom/android/settings/widget/x;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/w;->aQd:Lcom/android/settings/widget/x;

    invoke-interface {v1, v0}, Lcom/android/settings/widget/x;->ahr(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected abstract getSummary()Ljava/lang/String;
.end method
