.class public Lcom/android/settings/widget/GearPreference;
.super Lcom/android/settingslib/RestrictedPreference;
.source "GearPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private aMT:Lcom/android/settings/widget/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settingslib/RestrictedPreference;->al(Landroid/support/v7/preference/l;)V

    const v0, 0x7f0a03df

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/widget/GearPreference;->aMT:Lcom/android/settings/widget/a;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected ayU()I
    .locals 1

    const v0, 0x7f0d0158

    return v0
.end method

.method protected ayV()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/GearPreference;->aMT:Lcom/android/settings/widget/a;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a03df

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/GearPreference;->aMT:Lcom/android/settings/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/GearPreference;->aMT:Lcom/android/settings/widget/a;

    invoke-interface {v0, p0}, Lcom/android/settings/widget/a;->ayW(Lcom/android/settings/widget/GearPreference;)V

    :cond_0
    return-void
.end method
