.class final Lcom/android/settings/widget/R;
.super Ljava/lang/Object;
.source "DotsPageIndicator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic aRJ:Lcom/android/settings/widget/d;


# direct methods
.method constructor <init>(Lcom/android/settings/widget/d;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/R;->aRJ:Lcom/android/settings/widget/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/widget/R;->aRJ:Lcom/android/settings/widget/d;

    iget-object v1, v0, Lcom/android/settings/widget/d;->aNR:Lcom/android/settings/widget/DotsPageIndicator;

    iget-object v0, p0, Lcom/android/settings/widget/R;->aRJ:Lcom/android/settings/widget/d;

    invoke-static {v0}, Lcom/android/settings/widget/d;->azH(Lcom/android/settings/widget/d;)I

    move-result v2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v1, v2, v0}, Lcom/android/settings/widget/DotsPageIndicator;->azD(Lcom/android/settings/widget/DotsPageIndicator;IF)V

    return-void
.end method
