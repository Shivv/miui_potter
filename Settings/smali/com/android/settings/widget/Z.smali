.class final Lcom/android/settings/widget/Z;
.super Ljava/lang/Object;
.source "VideoPreference.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field final synthetic aRS:Lcom/android/settings/widget/VideoPreference;

.field final synthetic aRT:Landroid/widget/ImageView;

.field final synthetic aRU:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/android/settings/widget/VideoPreference;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/Z;->aRS:Lcom/android/settings/widget/VideoPreference;

    iput-object p2, p0, Lcom/android/settings/widget/Z;->aRT:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/android/settings/widget/Z;->aRU:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRS:Lcom/android/settings/widget/VideoPreference;

    invoke-static {v0}, Lcom/android/settings/widget/VideoPreference;->aAg(Lcom/android/settings/widget/VideoPreference;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRS:Lcom/android/settings/widget/VideoPreference;

    invoke-static {v0}, Lcom/android/settings/widget/VideoPreference;->aAg(Lcom/android/settings/widget/VideoPreference;)Landroid/media/MediaPlayer;

    move-result-object v0

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRS:Lcom/android/settings/widget/VideoPreference;

    invoke-static {v0, v2}, Lcom/android/settings/widget/VideoPreference;->aAi(Lcom/android/settings/widget/VideoPreference;Z)Z

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRS:Lcom/android/settings/widget/VideoPreference;

    invoke-static {v0}, Lcom/android/settings/widget/VideoPreference;->aAg(Lcom/android/settings/widget/VideoPreference;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRT:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return v1
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRS:Lcom/android/settings/widget/VideoPreference;

    invoke-static {v0}, Lcom/android/settings/widget/VideoPreference;->aAh(Lcom/android/settings/widget/VideoPreference;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRT:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRT:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRS:Lcom/android/settings/widget/VideoPreference;

    invoke-static {v0}, Lcom/android/settings/widget/VideoPreference;->aAg(Lcom/android/settings/widget/VideoPreference;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRS:Lcom/android/settings/widget/VideoPreference;

    invoke-static {v0}, Lcom/android/settings/widget/VideoPreference;->aAg(Lcom/android/settings/widget/VideoPreference;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRU:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/Z;->aRU:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    return-void
.end method
