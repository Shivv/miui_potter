.class final Lcom/android/settings/kd;
.super Ljava/lang/Object;
.source "MiuiFingerprintDetailFragment.java"

# interfaces
.implements Lcom/android/settings/f;


# instance fields
.field final synthetic cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiFingerprintDetailFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bfd()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f120735

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/MiuiFingerprintDetailFragment;->bRl(Lcom/android/settings/MiuiFingerprintDetailFragment;Z)Z

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->finish()V

    :cond_0
    return-void
.end method

.method public bfe()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-static {v1}, Lcom/android/settings/MiuiFingerprintDetailFragment;->bRk(Lcom/android/settings/MiuiFingerprintDetailFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/bd;->bAe(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/MiuiFingerprintDetailFragment;->bRl(Lcom/android/settings/MiuiFingerprintDetailFragment;Z)Z

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-static {v1}, Lcom/android/settings/MiuiFingerprintDetailFragment;->bRk(Lcom/android/settings/MiuiFingerprintDetailFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/security/FingerprintIdUtils;->deleteFingerprintById(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    iget-object v0, p0, Lcom/android/settings/kd;->cqG:Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->finish()V

    :cond_0
    return-void
.end method
