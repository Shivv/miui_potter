.class final Lcom/android/settings/ad/a;
.super Ljava/lang/Object;
.source "AdServiceSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic bpZ:Lcom/android/settings/ad/AdServiceSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/ad/AdServiceSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ad/a;->bpZ:Lcom/android/settings/ad/AdServiceSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/ad/a;->bpZ:Lcom/android/settings/ad/AdServiceSettings;

    invoke-static {v2}, Lcom/android/settings/ad/AdServiceSettings;->bdm(Lcom/android/settings/ad/AdServiceSettings;)Landroid/content/ContentResolver;

    move-result-object v3

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v3, v2}, Landroid/provider/MiuiSettings$Ad;->setPersonalizedAdEnable(Landroid/content/ContentResolver;Z)V

    iget-object v2, p0, Lcom/android/settings/ad/a;->bpZ:Lcom/android/settings/ad/AdServiceSettings;

    invoke-static {v2}, Lcom/android/settings/ad/AdServiceSettings;->bdm(Lcom/android/settings/ad/AdServiceSettings;)Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/provider/MiuiSettings$Ad;->setPersonalizedAdEnableTime(Landroid/content/ContentResolver;J)V

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "AdServiceSettings"

    const-string/jumbo v3, "click_use_personalized_open"

    invoke-static {v2, v3}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 v2, 0x1

    return v2

    :cond_0
    const-string/jumbo v2, "AdServiceSettings"

    const-string/jumbo v3, "click_use_personalized_close"

    invoke-static {v2, v3}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
