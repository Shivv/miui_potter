.class public Lcom/android/settings/ad/AdServiceSettings;
.super Lmiui/preference/PreferenceActivity;
.source "AdServiceSettings.java"


# instance fields
.field private bpV:Landroid/app/Activity;

.field private bpW:Landroid/view/View;

.field private bpX:Landroid/widget/TextView;

.field bpY:Landroid/preference/CheckBoxPreference;

.field private mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic bdm(Lcom/android/settings/ad/AdServiceSettings;)Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    iput-object p0, p0, Lcom/android/settings/ad/AdServiceSettings;->bpV:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/android/settings/ad/AdServiceSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->mContentResolver:Landroid/content/ContentResolver;

    const v0, 0x7f15000e

    invoke-virtual {p0, v0}, Lcom/android/settings/ad/AdServiceSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "personalized_ad_switch"

    invoke-virtual {p0, v0}, Lcom/android/settings/ad/AdServiceSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->bpY:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->bpY:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/ad/a;

    invoke-direct {v1, p0}, Lcom/android/settings/ad/a;-><init>(Lcom/android/settings/ad/AdServiceSettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->bpY:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/ad/AdServiceSettings;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v1}, Landroid/provider/MiuiSettings$Ad;->isPersonalizedAdEnabled(Landroid/content/ContentResolver;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0023

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->bpW:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->bpW:Landroid/view/View;

    const v1, 0x7f0a0033

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->bpX:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/ad/AdServiceSettings;->bpX:Landroid/widget/TextView;

    new-instance v1, Lcom/android/settings/ad/b;

    invoke-direct {v1, p0}, Lcom/android/settings/ad/b;-><init>(Lcom/android/settings/ad/AdServiceSettings;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/ad/AdServiceSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/ad/AdServiceSettings;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/ad/AdServiceSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/android/settings/ad/AdServiceSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/ad/AdServiceSettings;->bpW:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/settings/ad/AdServiceSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AdServiceSettings"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
