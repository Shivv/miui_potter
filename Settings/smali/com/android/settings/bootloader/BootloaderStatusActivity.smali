.class public Lcom/android/settings/bootloader/BootloaderStatusActivity;
.super Lmiui/app/Activity;
.source "BootloaderStatusActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private bmJ:Z

.field private bmK:Landroid/widget/Button;

.field private bmL:Landroid/widget/Toast;


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/bootloader/BootloaderStatusActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmJ:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmL:Landroid/widget/Toast;

    return-void
.end method

.method private baq()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/android/settings/bootloader/b;->bay(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmJ:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmJ:Z

    invoke-static {p0, p0}, Lcom/android/settings/bootloader/b;->baz(Landroid/content/Context;Landroid/accounts/AccountManagerCallback;)V

    return-void

    :cond_0
    invoke-direct {p0, v4, v3}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bar(ILjava/lang/String;)V

    return-void

    :cond_1
    invoke-static {p0}, Lcom/android/settings/bootloader/b;->baA(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x2

    invoke-direct {p0, v0, v3}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bar(ILjava/lang/String;)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmK:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmK:Landroid/widget/Button;

    const v1, 0x33708090

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    const/4 v0, 0x6

    invoke-direct {p0, v0, v3}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bar(ILjava/lang/String;)V

    new-instance v0, Lcom/android/settings/bootloader/a;

    invoke-direct {v0, p0}, Lcom/android/settings/bootloader/a;-><init>(Lcom/android/settings/bootloader/BootloaderStatusActivity;)V

    new-array v1, v4, [Landroid/content/Context;

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/settings/bootloader/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private bar(ILjava/lang/String;)V
    .locals 4

    const v0, 0x7f120372

    const/4 v3, 0x1

    iget-boolean v1, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmJ:Z

    if-nez v1, :cond_0

    iput-boolean v3, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmJ:Z

    :cond_0
    iget-object v1, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmL:Landroid/widget/Toast;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmL:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    :cond_1
    const v1, 0x7f120371

    if-ne p1, v3, :cond_3

    const v0, 0x7f120374

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmL:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmL:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_3
    const/4 v2, 0x2

    if-ne p1, v2, :cond_4

    const v0, 0x7f120375

    goto :goto_0

    :cond_4
    const/4 v2, 0x3

    if-eq p1, v2, :cond_2

    const/4 v2, 0x5

    if-eq p1, v2, :cond_2

    const/4 v0, 0x6

    if-ne p1, v0, :cond_5

    const v0, 0x7f120373

    goto :goto_0

    :cond_5
    const/4 v0, 0x4

    if-ne p1, v0, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmL:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmL:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method private bas(Landroid/content/Context;)Z
    .locals 4

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/provider/MiuiSettings$Privacy;->isEnabled(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/settings/bootloader/BootloaderStatusActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "get privacy status error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic bat(Lcom/android/settings/bootloader/BootloaderStatusActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmK:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic bau(Lcom/android/settings/bootloader/BootloaderStatusActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->baq()V

    return-void
.end method

.method static synthetic bav(Lcom/android/settings/bootloader/BootloaderStatusActivity;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bar(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->finish()V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lmiui/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "ro.secureboot.lockstate"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unlocked"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    const v0, 0x7f0d0052

    invoke-virtual {p0, v0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->setContentView(I)V

    return-void

    :cond_1
    const v0, 0x7f0d0051

    invoke-virtual {p0, v0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->setContentView(I)V

    const v0, 0x7f0a00b4

    invoke-virtual {p0, v0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmK:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bmK:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/bootloader/e;

    invoke-direct {v1, p0}, Lcom/android/settings/bootloader/e;-><init>(Lcom/android/settings/bootloader/BootloaderStatusActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, p0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bas(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.PRIVACY_AUTHORIZATION_DIALOG"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "key"

    invoke-virtual {p0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_2
    return-void
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const-string/jumbo v2, "booleanResult"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0, v3}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bar(ILjava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->baq()V

    goto :goto_1
.end method
