.class Lcom/android/settings/bootloader/a;
.super Landroid/os/AsyncTask;
.source "BootloaderStatusActivity.java"


# instance fields
.field final synthetic bmM:Lcom/android/settings/bootloader/BootloaderStatusActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/bootloader/BootloaderStatusActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bootloader/a;->bmM:Lcom/android/settings/bootloader/BootloaderStatusActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs baw([Landroid/content/Context;)Lcom/android/settings/bootloader/c;
    .locals 6

    const/4 v5, 0x3

    const/4 v0, 0x0

    aget-object v1, p1, v0

    new-instance v0, Lcom/android/settings/bootloader/c;

    invoke-direct {v0}, Lcom/android/settings/bootloader/c;-><init>()V

    :try_start_0
    invoke-static {v1}, Lcom/android/settings/bootloader/d;->baJ(Landroid/content/Context;)Lcom/android/settings/bootloader/c;
    :try_end_0
    .catch Lcom/android/settings/bootloader/Utils$AccountExcepiton; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/android/settings/bootloader/Utils$ParameterException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    iput v5, v0, Lcom/android/settings/bootloader/c;->bmN:I

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-static {}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->-get0()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Parameter error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/android/settings/bootloader/Utils$ParameterException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v5, v0, Lcom/android/settings/bootloader/c;->bmN:I

    goto :goto_0

    :catch_2
    move-exception v1

    const/4 v1, 0x1

    iput v1, v0, Lcom/android/settings/bootloader/c;->bmN:I

    goto :goto_0
.end method

.method protected bax(Lcom/android/settings/bootloader/c;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bootloader/a;->bmM:Lcom/android/settings/bootloader/BootloaderStatusActivity;

    invoke-static {v0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bat(Lcom/android/settings/bootloader/BootloaderStatusActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/bootloader/a;->bmM:Lcom/android/settings/bootloader/BootloaderStatusActivity;

    invoke-static {v0}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bat(Lcom/android/settings/bootloader/BootloaderStatusActivity;)Landroid/widget/Button;

    move-result-object v0

    const/high16 v1, -0x34000000    # -3.3554432E7f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/bootloader/a;->bmM:Lcom/android/settings/bootloader/BootloaderStatusActivity;

    iget v1, p1, Lcom/android/settings/bootloader/c;->bmN:I

    iget-object v2, p1, Lcom/android/settings/bootloader/c;->bmO:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/bootloader/BootloaderStatusActivity;->bav(Lcom/android/settings/bootloader/BootloaderStatusActivity;ILjava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/android/settings/bootloader/a;->baw([Landroid/content/Context;)Lcom/android/settings/bootloader/c;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/settings/bootloader/c;

    invoke-virtual {p0, p1}, Lcom/android/settings/bootloader/a;->bax(Lcom/android/settings/bootloader/c;)V

    return-void
.end method
