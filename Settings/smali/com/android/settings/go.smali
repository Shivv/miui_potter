.class final Lcom/android/settings/go;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Lmiui/view/SearchActionMode$Callback;


# instance fields
.field final synthetic clH:Lcom/android/settings/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    check-cast p1, Lmiui/view/SearchActionMode;

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxu(Lcom/android/settings/SettingsFragment;)Landroid/view/View;

    move-result-object v0

    invoke-interface {p1, v0}, Lmiui/view/SearchActionMode;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxz(Lcom/android/settings/SettingsFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-interface {p1, v0}, Lmiui/view/SearchActionMode;->setAnimateView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxI(Lcom/android/settings/SettingsFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-interface {p1, v0}, Lmiui/view/SearchActionMode;->setResultView(Landroid/view/View;)V

    invoke-static {}, Lcom/android/settings/SettingsFragment;->bxL()Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    iget-object v0, v0, Lcom/android/settings/SettingsFragment;->caL:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxU(Lcom/android/settings/SettingsFragment;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-interface {p1}, Lmiui/view/SearchActionMode;->getSearchInput()Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/SettingsFragment;->bxO(Lcom/android/settings/SettingsFragment;Landroid/widget/EditText;)Landroid/widget/EditText;

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxE(Lcom/android/settings/SettingsFragment;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v1}, Lcom/android/settings/SettingsFragment;->bxw(Lcom/android/settings/SettingsFragment;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxE(Lcom/android/settings/SettingsFragment;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v1}, Lcom/android/settings/SettingsFragment;->bxK(Lcom/android/settings/SettingsFragment;)Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxE(Lcom/android/settings/SettingsFragment;)Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/android/settings/gp;

    invoke-direct {v1, p0}, Lcom/android/settings/gp;-><init>(Lcom/android/settings/go;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0, v2}, Lcom/android/settings/SettingsFragment;->bxM(Lcom/android/settings/SettingsFragment;Z)Z

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0, v1}, Lcom/android/settings/SettingsFragment;->bxO(Lcom/android/settings/SettingsFragment;Landroid/widget/EditText;)Landroid/widget/EditText;

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxI(Lcom/android/settings/SettingsFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxF(Lcom/android/settings/SettingsFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxz(Lcom/android/settings/SettingsFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0, v1}, Lcom/android/settings/SettingsFragment;->bxQ(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxH(Lcom/android/settings/SettingsFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxH(Lcom/android/settings/SettingsFragment;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/android/settings/search/SearchResultItem;->EMPTY:Lcom/android/settings/search/SearchResultItem;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxB(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/aS;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v1}, Lcom/android/settings/SettingsFragment;->bxH(Lcom/android/settings/SettingsFragment;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/aS;->bxZ(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxD(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/aQ;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxD(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/aQ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/aQ;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/go;->clH:Lcom/android/settings/SettingsFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/SettingsFragment;->bxM(Lcom/android/settings/SettingsFragment;Z)Z

    const/4 v0, 0x0

    return v0
.end method
