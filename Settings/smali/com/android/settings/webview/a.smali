.class public Lcom/android/settings/webview/a;
.super Lcom/android/settings/applications/defaultapps/c;
.source "WebViewAppPreferenceController.java"


# instance fields
.field private boG:Landroid/preference/Preference;

.field private final boH:Lcom/android/settings/webview/c;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/android/settings/webview/c;

    invoke-direct {v0}, Lcom/android/settings/webview/c;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/android/settings/webview/a;-><init>(Landroid/content/Context;Lcom/android/settings/webview/c;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/webview/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/applications/defaultapps/c;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/webview/a;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/webview/a;->boH:Lcom/android/settings/webview/c;

    return-void
.end method


# virtual methods
.method public bcj(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/webview/a;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/webview/a;->boG:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/applications/defaultapps/c;->i(Landroid/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/webview/a;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "select_webview_provider"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/webview/a;->boG:Landroid/preference/Preference;

    :cond_0
    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "select_webview_provider"

    return-object v0
.end method

.method public mr()Lcom/android/settings/applications/defaultapps/a;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/webview/a;->boH:Lcom/android/settings/webview/c;

    invoke-virtual {v1}, Lcom/android/settings/webview/c;->bcm()Landroid/content/pm/PackageInfo;

    move-result-object v1

    new-instance v2, Lcom/android/settings/applications/defaultapps/a;

    iget-object v3, p0, Lcom/android/settings/webview/a;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    if-nez v1, :cond_0

    :goto_0
    invoke-direct {v2, v3, v0}, Lcom/android/settings/applications/defaultapps/a;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/PackageItemInfo;)V

    return-object v2

    :cond_0
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
