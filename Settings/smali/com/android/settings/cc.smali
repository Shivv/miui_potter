.class Lcom/android/settings/cc;
.super Ljava/lang/Object;
.source "EqualizerView.java"


# instance fields
.field final bWo:F

.field final bWp:F


# direct methods
.method protected constructor <init>(FF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/cc;->bWp:F

    iput p2, p0, Lcom/android/settings/cc;->bWo:F

    return-void
.end method


# virtual methods
.method protected bQf(Lcom/android/settings/cc;)Lcom/android/settings/cc;
    .locals 4

    new-instance v0, Lcom/android/settings/cc;

    iget v1, p0, Lcom/android/settings/cc;->bWp:F

    iget v2, p1, Lcom/android/settings/cc;->bWp:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/android/settings/cc;->bWo:F

    iget v3, p1, Lcom/android/settings/cc;->bWo:F

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/android/settings/cc;-><init>(FF)V

    return-object v0
.end method

.method protected bQg()Lcom/android/settings/cc;
    .locals 3

    new-instance v0, Lcom/android/settings/cc;

    iget v1, p0, Lcom/android/settings/cc;->bWp:F

    iget v2, p0, Lcom/android/settings/cc;->bWo:F

    neg-float v2, v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/cc;-><init>(FF)V

    return-object v0
.end method

.method protected bQh(Lcom/android/settings/cc;)Lcom/android/settings/cc;
    .locals 3

    iget v0, p1, Lcom/android/settings/cc;->bWp:F

    iget v1, p1, Lcom/android/settings/cc;->bWp:F

    mul-float/2addr v0, v1

    iget v1, p1, Lcom/android/settings/cc;->bWo:F

    iget v2, p1, Lcom/android/settings/cc;->bWo:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p1}, Lcom/android/settings/cc;->bQg()Lcom/android/settings/cc;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/cc;->bQj(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/cc;->bQi(F)Lcom/android/settings/cc;

    move-result-object v0

    return-object v0
.end method

.method protected bQi(F)Lcom/android/settings/cc;
    .locals 3

    new-instance v0, Lcom/android/settings/cc;

    iget v1, p0, Lcom/android/settings/cc;->bWp:F

    div-float/2addr v1, p1

    iget v2, p0, Lcom/android/settings/cc;->bWo:F

    div-float/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/settings/cc;-><init>(FF)V

    return-object v0
.end method

.method protected bQj(Lcom/android/settings/cc;)Lcom/android/settings/cc;
    .locals 5

    new-instance v0, Lcom/android/settings/cc;

    iget v1, p0, Lcom/android/settings/cc;->bWp:F

    iget v2, p1, Lcom/android/settings/cc;->bWp:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/settings/cc;->bWo:F

    iget v3, p1, Lcom/android/settings/cc;->bWo:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/android/settings/cc;->bWp:F

    iget v3, p1, Lcom/android/settings/cc;->bWo:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/android/settings/cc;->bWo:F

    iget v4, p1, Lcom/android/settings/cc;->bWp:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/android/settings/cc;-><init>(FF)V

    return-object v0
.end method

.method protected bQk(F)Lcom/android/settings/cc;
    .locals 3

    new-instance v0, Lcom/android/settings/cc;

    iget v1, p0, Lcom/android/settings/cc;->bWp:F

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/android/settings/cc;->bWo:F

    mul-float/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/settings/cc;-><init>(FF)V

    return-object v0
.end method

.method protected bQl()F
    .locals 3

    iget v0, p0, Lcom/android/settings/cc;->bWp:F

    iget v1, p0, Lcom/android/settings/cc;->bWp:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/android/settings/cc;->bWo:F

    iget v2, p0, Lcom/android/settings/cc;->bWo:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method
