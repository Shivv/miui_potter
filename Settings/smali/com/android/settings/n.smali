.class Lcom/android/settings/n;
.super Ljava/lang/Object;
.source "TrustedCredentialsSettings.java"


# instance fields
.field private final buU:Lcom/android/settings/k;

.field private final buV:Landroid/util/SparseArray;

.field private final buW:Lcom/android/settings/TrustedCredentialsSettings$Tab;

.field final synthetic buX:Lcom/android/settings/TrustedCredentialsSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/TrustedCredentialsSettings$Tab;Lcom/android/settings/k;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/n;->buV:Landroid/util/SparseArray;

    iput-object p3, p0, Lcom/android/settings/n;->buU:Lcom/android/settings/k;

    iput-object p2, p0, Lcom/android/settings/n;->buW:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/TrustedCredentialsSettings$Tab;Lcom/android/settings/k;Lcom/android/settings/n;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/n;-><init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/TrustedCredentialsSettings$Tab;Lcom/android/settings/k;)V

    return-void
.end method

.method static synthetic bhQ(Lcom/android/settings/n;)Lcom/android/settings/k;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/n;->buU:Lcom/android/settings/k;

    return-object v0
.end method

.method static synthetic bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/n;->buV:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic bhS(Lcom/android/settings/n;)Lcom/android/settings/TrustedCredentialsSettings$Tab;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/n;->buW:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    return-object v0
.end method


# virtual methods
.method public bhP(Lcom/android/settings/p;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/n;->buV:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/n;->buV:Landroid/util/SparseArray;

    iget v1, p1, Lcom/android/settings/p;->bve:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
