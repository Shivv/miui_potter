.class Lcom/android/settings/credentials/c;
.super Landroid/os/AsyncTask;
.source "MiuiCredentialsUpdater.java"


# instance fields
.field private bqf:Lcom/android/settings/credentials/d;

.field final synthetic bqg:Lcom/android/settings/credentials/a;


# direct methods
.method public constructor <init>(Lcom/android/settings/credentials/a;Lcom/android/settings/credentials/d;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/credentials/c;->bqg:Lcom/android/settings/credentials/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/android/settings/credentials/c;->bqf:Lcom/android/settings/credentials/d;

    return-void
.end method


# virtual methods
.method protected varargs bdE([Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string/jumbo v0, "do_get"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/credentials/c;->bqg:Lcom/android/settings/credentials/a;

    aget-object v1, p1, v3

    invoke-static {v0, v1}, Lcom/android/settings/credentials/a;->bdA(Lcom/android/settings/credentials/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "do_get_pic"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/credentials/c;->bqg:Lcom/android/settings/credentials/a;

    aget-object v1, p1, v3

    invoke-static {v0, v1}, Lcom/android/settings/credentials/a;->bdz(Lcom/android/settings/credentials/a;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/settings/credentials/c;->bdE([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/credentials/c;->bqf:Lcom/android/settings/credentials/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/credentials/c;->bqf:Lcom/android/settings/credentials/d;

    invoke-interface {v0, p1}, Lcom/android/settings/credentials/d;->bdF(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
