.class public Lcom/android/setupwizardlib/view/NavigationBarButton;
.super Landroid/widget/Button;
.source "NavigationBarButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->caV()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->caV()V

    return-void
.end method

.method private caV()V
    .locals 5

    const/4 v1, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_2

    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-object v3, v2, v0

    if-eqz v3, :cond_0

    aget-object v3, v2, v0

    invoke-static {v3}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object v3

    aput-object v3, v2, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    aget-object v0, v2, v1

    const/4 v1, 0x1

    aget-object v1, v2, v1

    const/4 v3, 0x2

    aget-object v3, v2, v3

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {p0, v0, v1, v3, v2}, Lcom/android/setupwizardlib/view/NavigationBarButton;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_2
    return-void
.end method

.method private caW()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->getAllCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v0, 0x0

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    instance-of v5, v0, Lcom/android/setupwizardlib/view/g;

    if-eqz v5, :cond_0

    check-cast v0, Lcom/android/setupwizardlib/view/g;

    invoke-virtual {v0, v2}, Lcom/android/setupwizardlib/view/g;->cbq(Landroid/content/res/ColorStateList;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->invalidate()V

    :cond_2
    return-void
.end method

.method private getAllCompoundDrawables()[Landroid/graphics/drawable/Drawable;
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v0, 0x6

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v2, v1, v3

    aput-object v2, v0, v3

    aget-object v2, v1, v5

    aput-object v2, v0, v5

    aget-object v2, v1, v4

    aput-object v2, v0, v4

    aget-object v1, v1, v6

    aput-object v1, v0, v6

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v2, v1, v3

    const/4 v3, 0x4

    aput-object v2, v0, v3

    aget-object v1, v1, v4

    const/4 v2, 0x5

    aput-object v1, v0, v2

    :cond_0
    return-object v0
.end method


# virtual methods
.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object p1

    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p2}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object p2

    :cond_1
    if-eqz p3, :cond_2

    invoke-static {p3}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object p3

    :cond_2
    if-eqz p4, :cond_3

    invoke-static {p4}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object p4

    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/Button;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->caW()V

    return-void
.end method

.method public setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object p1

    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p2}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object p2

    :cond_1
    if-eqz p3, :cond_2

    invoke-static {p3}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object p3

    :cond_2
    if-eqz p4, :cond_3

    invoke-static {p4}, Lcom/android/setupwizardlib/view/g;->cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;

    move-result-object p4

    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/Button;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->caW()V

    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/NavigationBarButton;->caW()V

    return-void
.end method
