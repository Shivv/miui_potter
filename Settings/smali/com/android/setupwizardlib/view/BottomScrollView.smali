.class public Lcom/android/setupwizardlib/view/BottomScrollView;
.super Landroid/widget/ScrollView;
.source "BottomScrollView.java"


# instance fields
.field private cto:I

.field private ctp:Lcom/android/setupwizardlib/view/e;

.field private final ctq:Ljava/lang/Runnable;

.field private ctr:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctr:Z

    new-instance v0, Lcom/android/setupwizardlib/view/f;

    invoke-direct {v0, p0}, Lcom/android/setupwizardlib/view/f;-><init>(Lcom/android/setupwizardlib/view/BottomScrollView;)V

    iput-object v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctq:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctr:Z

    new-instance v0, Lcom/android/setupwizardlib/view/f;

    invoke-direct {v0, p0}, Lcom/android/setupwizardlib/view/f;-><init>(Lcom/android/setupwizardlib/view/BottomScrollView;)V

    iput-object v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctq:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctr:Z

    new-instance v0, Lcom/android/setupwizardlib/view/f;

    invoke-direct {v0, p0}, Lcom/android/setupwizardlib/view/f;-><init>(Lcom/android/setupwizardlib/view/BottomScrollView;)V

    iput-object v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctq:Ljava/lang/Runnable;

    return-void
.end method

.method private cbw()V
    .locals 2

    iget-object v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctp:Lcom/android/setupwizardlib/view/e;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/BottomScrollView;->getScrollY()I

    move-result v0

    iget v1, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->cto:I

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctp:Lcom/android/setupwizardlib/view/e;

    invoke-interface {v0}, Lcom/android/setupwizardlib/view/e;->cbi()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctr:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctr:Z

    iget-object v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctp:Lcom/android/setupwizardlib/view/e;

    invoke-interface {v0}, Lcom/android/setupwizardlib/view/e;->cbh()V

    goto :goto_0
.end method

.method static synthetic cbx(Lcom/android/setupwizardlib/view/BottomScrollView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/BottomScrollView;->cbw()V

    return-void
.end method


# virtual methods
.method public getScrollThreshold()I
    .locals 1

    iget v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->cto:I

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    const/4 v2, 0x0

    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    invoke-virtual {p0, v2}, Lcom/android/setupwizardlib/view/BottomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, p5

    add-int/2addr v0, p3

    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/BottomScrollView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->cto:I

    :cond_0
    sub-int v0, p5, p3

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctq:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/view/BottomScrollView;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    if-eq p4, p2, :cond_0

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/BottomScrollView;->cbw()V

    :cond_0
    return-void
.end method

.method public setBottomScrollListener(Lcom/android/setupwizardlib/view/e;)V
    .locals 0

    iput-object p1, p0, Lcom/android/setupwizardlib/view/BottomScrollView;->ctp:Lcom/android/setupwizardlib/view/e;

    return-void
.end method
