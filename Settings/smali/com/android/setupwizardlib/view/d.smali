.class final Lcom/android/setupwizardlib/view/d;
.super Landroid/support/v7/widget/c;
.source "HeaderRecyclerView.java"


# instance fields
.field final synthetic csV:Lcom/android/setupwizardlib/view/b;


# direct methods
.method constructor <init>(Lcom/android/setupwizardlib/view/b;)V
    .locals 0

    iput-object p1, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-direct {p0}, Landroid/support/v7/widget/c;-><init>()V

    return-void
.end method


# virtual methods
.method public bZY(II)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-static {v0}, Lcom/android/setupwizardlib/view/b;->caT(Lcom/android/setupwizardlib/view/b;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    iget-object v0, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-virtual {v0, p1, p2}, Lcom/android/setupwizardlib/view/b;->notifyItemRangeChanged(II)V

    return-void
.end method

.method public caa(II)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-static {v0}, Lcom/android/setupwizardlib/view/b;->caT(Lcom/android/setupwizardlib/view/b;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    iget-object v0, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-virtual {v0, p1, p2}, Lcom/android/setupwizardlib/view/b;->notifyItemRangeInserted(II)V

    return-void
.end method

.method public cab(III)V
    .locals 4

    iget-object v0, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-static {v0}, Lcom/android/setupwizardlib/view/b;->caT(Lcom/android/setupwizardlib/view/b;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, p2, 0x1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_1

    iget-object v1, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    add-int v2, p1, v0

    add-int v3, p2, v0

    invoke-virtual {v1, v2, v3}, Lcom/android/setupwizardlib/view/b;->notifyItemMoved(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public cac(II)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-static {v0}, Lcom/android/setupwizardlib/view/b;->caT(Lcom/android/setupwizardlib/view/b;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    iget-object v0, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-virtual {v0, p1, p2}, Lcom/android/setupwizardlib/view/b;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method public onChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/view/d;->csV:Lcom/android/setupwizardlib/view/b;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/view/b;->notifyDataSetChanged()V

    return-void
.end method
