.class public Lcom/android/setupwizardlib/GlifLayout;
.super Lcom/android/setupwizardlib/TemplateLayout;
.source "GlifLayout.java"


# instance fields
.field private cyJ:Landroid/content/res/ColorStateList;

.field private cyK:Z

.field private cyL:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lcom/android/setupwizardlib/GlifLayout;-><init>(Landroid/content/Context;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/setupwizardlib/GlifLayout;-><init>(Landroid/content/Context;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/setupwizardlib/TemplateLayout;-><init>(Landroid/content/Context;II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/setupwizardlib/GlifLayout;->cyK:Z

    sget v0, Lcom/android/setupwizardlib/a;->cuM:I

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdm(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/setupwizardlib/TemplateLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/setupwizardlib/GlifLayout;->cyK:Z

    sget v0, Lcom/android/setupwizardlib/a;->cuM:I

    invoke-direct {p0, p2, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdm(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/setupwizardlib/TemplateLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/setupwizardlib/GlifLayout;->cyK:Z

    invoke-direct {p0, p2, p3}, Lcom/android/setupwizardlib/GlifLayout;->cdm(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private cdj()V
    .locals 3

    sget v0, Lcom/android/setupwizardlib/d;->cvk:I

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdq(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/setupwizardlib/GlifLayout;->cyJ:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/android/setupwizardlib/GlifLayout;->cyJ:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/android/setupwizardlib/GlifLayout;->cyK:Z

    if-eqz v2, :cond_4

    new-instance v2, Lcom/android/setupwizardlib/m;

    invoke-direct {v2, v1}, Lcom/android/setupwizardlib/m;-><init>(I)V

    move-object v1, v2

    :goto_1
    instance-of v2, v0, Lcom/android/setupwizardlib/view/StatusBarBackgroundLayout;

    if-eqz v2, :cond_5

    check-cast v0, Lcom/android/setupwizardlib/view/StatusBarBackgroundLayout;

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/view/StatusBarBackgroundLayout;->setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    const/16 v0, 0x400

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->setSystemUiVisibility(I)V

    :cond_2
    return-void

    :cond_3
    iget-object v2, p0, Lcom/android/setupwizardlib/GlifLayout;->cyL:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/android/setupwizardlib/GlifLayout;->cyL:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    goto :goto_0

    :cond_4
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move-object v1, v2

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method private cdm(Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    const-class v0, Lcom/android/setupwizardlib/b/e;

    new-instance v1, Lcom/android/setupwizardlib/b/k;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/setupwizardlib/b/k;-><init>(Lcom/android/setupwizardlib/TemplateLayout;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, v0, v1}, Lcom/android/setupwizardlib/GlifLayout;->cdx(Ljava/lang/Class;Lcom/android/setupwizardlib/b/b;)V

    const-class v0, Lcom/android/setupwizardlib/b/p;

    new-instance v1, Lcom/android/setupwizardlib/b/p;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/setupwizardlib/b/p;-><init>(Lcom/android/setupwizardlib/TemplateLayout;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, v0, v1}, Lcom/android/setupwizardlib/GlifLayout;->cdx(Ljava/lang/Class;Lcom/android/setupwizardlib/b/b;)V

    const-class v0, Lcom/android/setupwizardlib/b/n;

    new-instance v1, Lcom/android/setupwizardlib/b/n;

    invoke-direct {v1, p0}, Lcom/android/setupwizardlib/b/n;-><init>(Lcom/android/setupwizardlib/TemplateLayout;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/setupwizardlib/GlifLayout;->cdx(Ljava/lang/Class;Lcom/android/setupwizardlib/b/b;)V

    const-class v0, Lcom/android/setupwizardlib/b/j;

    new-instance v1, Lcom/android/setupwizardlib/b/j;

    invoke-direct {v1, p0}, Lcom/android/setupwizardlib/b/j;-><init>(Lcom/android/setupwizardlib/TemplateLayout;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/setupwizardlib/GlifLayout;->cdx(Ljava/lang/Class;Lcom/android/setupwizardlib/b/b;)V

    new-instance v0, Lcom/android/setupwizardlib/b/g;

    invoke-direct {v0, p0}, Lcom/android/setupwizardlib/b/g;-><init>(Lcom/android/setupwizardlib/TemplateLayout;)V

    const-class v1, Lcom/android/setupwizardlib/b/g;

    invoke-virtual {p0, v1, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdx(Ljava/lang/Class;Lcom/android/setupwizardlib/b/b;)V

    invoke-virtual {p0}, Lcom/android/setupwizardlib/GlifLayout;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/android/setupwizardlib/b/h;

    invoke-direct {v2, v0, v1}, Lcom/android/setupwizardlib/b/h;-><init>(Lcom/android/setupwizardlib/b/g;Landroid/widget/ScrollView;)V

    invoke-virtual {v0, v2}, Lcom/android/setupwizardlib/b/g;->cbJ(Lcom/android/setupwizardlib/b/l;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/setupwizardlib/GlifLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/android/setupwizardlib/g;->cxl:[I

    invoke-virtual {v0, p1, v1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/android/setupwizardlib/g;->cxo:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/setupwizardlib/GlifLayout;->setPrimaryColor(Landroid/content/res/ColorStateList;)V

    :cond_1
    sget v1, Lcom/android/setupwizardlib/g;->cxm:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/setupwizardlib/GlifLayout;->setBackgroundBaseColor(Landroid/content/res/ColorStateList;)V

    sget v1, Lcom/android/setupwizardlib/g;->cxn:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/setupwizardlib/GlifLayout;->setBackgroundPatterned(Z)V

    sget v1, Lcom/android/setupwizardlib/g;->cxp:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v1}, Lcom/android/setupwizardlib/GlifLayout;->cdl(I)Landroid/view/View;

    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected cdk(I)Landroid/view/ViewGroup;
    .locals 1

    if-nez p1, :cond_0

    sget p1, Lcom/android/setupwizardlib/d;->cuY:I

    :cond_0
    invoke-super {p0, p1}, Lcom/android/setupwizardlib/TemplateLayout;->cdk(I)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public cdl(I)Landroid/view/View;
    .locals 1

    sget v0, Lcom/android/setupwizardlib/d;->cva:I

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdq(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected cdn(Landroid/view/LayoutInflater;I)Landroid/view/View;
    .locals 1

    if-nez p2, :cond_0

    sget p2, Lcom/android/setupwizardlib/e;->cvs:I

    :cond_0
    sget v0, Lcom/android/setupwizardlib/f;->cvG:I

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/setupwizardlib/GlifLayout;->cdo(Landroid/view/LayoutInflater;II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundBaseColor()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifLayout;->cyJ:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public getHeaderColor()Landroid/content/res/ColorStateList;
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/k;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/k;->cbN()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderText()Ljava/lang/CharSequence;
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/e;->cbB()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderTextView()Landroid/widget/TextView;
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/e;->cbD()Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/p;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/p;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/p;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryColor()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifLayout;->cyL:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public getScrollView()Landroid/widget/ScrollView;
    .locals 2

    sget v0, Lcom/android/setupwizardlib/d;->cvm:I

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdq(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/ScrollView;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/ScrollView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBackgroundBaseColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/setupwizardlib/GlifLayout;->cyJ:Landroid/content/res/ColorStateList;

    invoke-direct {p0}, Lcom/android/setupwizardlib/GlifLayout;->cdj()V

    return-void
.end method

.method public setBackgroundPatterned(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/setupwizardlib/GlifLayout;->cyK:Z

    invoke-direct {p0}, Lcom/android/setupwizardlib/GlifLayout;->cdj()V

    return-void
.end method

.method public setHeaderColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/k;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/k;->cbM(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setHeaderText(I)V
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/e;->cbC(I)V

    return-void
.end method

.method public setHeaderText(Ljava/lang/CharSequence;)V
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/e;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/e;->cbE(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/p;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/p;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/p;->setIcon(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setPrimaryColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    iput-object p1, p0, Lcom/android/setupwizardlib/GlifLayout;->cyL:Landroid/content/res/ColorStateList;

    invoke-direct {p0}, Lcom/android/setupwizardlib/GlifLayout;->cdj()V

    const-class v0, Lcom/android/setupwizardlib/b/n;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/n;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/n;->cbQ(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setProgressBarShown(Z)V
    .locals 1

    const-class v0, Lcom/android/setupwizardlib/b/n;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/n;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/n;->cbO(Z)V

    return-void
.end method
