.class public Lcom/android/setupwizardlib/b/n;
.super Ljava/lang/Object;
.source "ProgressBarMixin.java"

# interfaces
.implements Lcom/android/setupwizardlib/b/b;


# instance fields
.field private ctX:Landroid/content/res/ColorStateList;

.field private ctY:Lcom/android/setupwizardlib/TemplateLayout;


# direct methods
.method public constructor <init>(Lcom/android/setupwizardlib/TemplateLayout;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/setupwizardlib/b/n;->ctY:Lcom/android/setupwizardlib/TemplateLayout;

    return-void
.end method

.method private cbR()Landroid/widget/ProgressBar;
    .locals 2

    invoke-virtual {p0}, Lcom/android/setupwizardlib/b/n;->cbT()Landroid/widget/ProgressBar;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/n;->ctY:Lcom/android/setupwizardlib/TemplateLayout;

    sget v1, Lcom/android/setupwizardlib/d;->cve:I

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/TemplateLayout;->cdq(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/android/setupwizardlib/b/n;->ctX:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/b/n;->cbQ(Landroid/content/res/ColorStateList;)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/setupwizardlib/b/n;->cbT()Landroid/widget/ProgressBar;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cbO(Z)V
    .locals 2

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/n;->cbR()Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/setupwizardlib/b/n;->cbT()Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public cbP()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/setupwizardlib/b/n;->ctY:Lcom/android/setupwizardlib/TemplateLayout;

    sget v2, Lcom/android/setupwizardlib/d;->cvd:I

    invoke-virtual {v1, v2}, Lcom/android/setupwizardlib/TemplateLayout;->cdq(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public cbQ(Landroid/content/res/ColorStateList;)V
    .locals 3

    iput-object p1, p0, Lcom/android/setupwizardlib/b/n;->ctX:Landroid/content/res/ColorStateList;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/setupwizardlib/b/n;->cbT()Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminateTintList(Landroid/content/res/ColorStateList;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v1, v2, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgressBackgroundTintList(Landroid/content/res/ColorStateList;)V

    :cond_1
    return-void
.end method

.method public cbS()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/n;->ctX:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public cbT()Landroid/widget/ProgressBar;
    .locals 2

    iget-object v0, p0, Lcom/android/setupwizardlib/b/n;->ctY:Lcom/android/setupwizardlib/TemplateLayout;

    sget v1, Lcom/android/setupwizardlib/d;->cvd:I

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/TemplateLayout;->cdq(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    return-object v0
.end method
