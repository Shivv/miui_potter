.class public Lcom/android/setupwizardlib/b/h;
.super Ljava/lang/Object;
.source "ScrollViewScrollHandlingDelegate.java"

# interfaces
.implements Lcom/android/setupwizardlib/b/l;
.implements Lcom/android/setupwizardlib/view/e;


# instance fields
.field private final ctR:Lcom/android/setupwizardlib/b/g;

.field private final ctS:Lcom/android/setupwizardlib/view/BottomScrollView;


# direct methods
.method public constructor <init>(Lcom/android/setupwizardlib/b/g;Landroid/widget/ScrollView;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/setupwizardlib/b/h;->ctR:Lcom/android/setupwizardlib/b/g;

    instance-of v0, p2, Lcom/android/setupwizardlib/view/BottomScrollView;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/android/setupwizardlib/view/BottomScrollView;

    iput-object p2, p0, Lcom/android/setupwizardlib/b/h;->ctS:Lcom/android/setupwizardlib/view/BottomScrollView;

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "ScrollViewDelegate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot set non-BottomScrollView. Found="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/setupwizardlib/b/h;->ctS:Lcom/android/setupwizardlib/view/BottomScrollView;

    goto :goto_0
.end method


# virtual methods
.method public cbh()V
    .locals 2

    iget-object v0, p0, Lcom/android/setupwizardlib/b/h;->ctR:Lcom/android/setupwizardlib/b/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/b/g;->cbK(Z)V

    return-void
.end method

.method public cbi()V
    .locals 2

    iget-object v0, p0, Lcom/android/setupwizardlib/b/h;->ctR:Lcom/android/setupwizardlib/b/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/b/g;->cbK(Z)V

    return-void
.end method
