.class public Lcom/android/setupwizardlib/items/j;
.super Lcom/android/setupwizardlib/items/k;
.source "ItemInflater.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/setupwizardlib/items/k;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/android/setupwizardlib/items/Item;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/items/j;->ccY(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic ccG(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/setupwizardlib/items/f;

    check-cast p2, Lcom/android/setupwizardlib/items/f;

    invoke-virtual {p0, p1, p2}, Lcom/android/setupwizardlib/items/j;->ccU(Lcom/android/setupwizardlib/items/f;Lcom/android/setupwizardlib/items/f;)V

    return-void
.end method

.method protected ccU(Lcom/android/setupwizardlib/items/f;Lcom/android/setupwizardlib/items/f;)V
    .locals 3

    instance-of v0, p1, Lcom/android/setupwizardlib/items/m;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/setupwizardlib/items/m;

    invoke-interface {p1, p2}, Lcom/android/setupwizardlib/items/m;->ccN(Lcom/android/setupwizardlib/items/f;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot add child item to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
