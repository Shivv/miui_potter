.class Lcom/android/setupwizardlib/items/a;
.super Landroid/support/v7/widget/p;
.source "ItemViewHolder.java"

# interfaces
.implements Lcom/android/setupwizardlib/i;


# instance fields
.field private cue:Z

.field private cuf:Lcom/android/setupwizardlib/items/i;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/p;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public cbc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/setupwizardlib/items/a;->cue:Z

    return v0
.end method

.method public cbd()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/setupwizardlib/items/a;->cue:Z

    return v0
.end method

.method public cco(Lcom/android/setupwizardlib/items/i;)V
    .locals 0

    iput-object p1, p0, Lcom/android/setupwizardlib/items/a;->cuf:Lcom/android/setupwizardlib/items/i;

    return-void
.end method

.method public getItem()Lcom/android/setupwizardlib/items/i;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/items/a;->cuf:Lcom/android/setupwizardlib/items/i;

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/setupwizardlib/items/a;->cue:Z

    iget-object v0, p0, Lcom/android/setupwizardlib/items/a;->itemView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/android/setupwizardlib/items/a;->itemView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/setupwizardlib/items/a;->itemView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setFocusable(Z)V

    return-void
.end method
