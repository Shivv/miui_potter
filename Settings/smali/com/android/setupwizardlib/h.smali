.class public Lcom/android/setupwizardlib/h;
.super Landroid/support/v7/widget/d;
.source "DividerItemDecoration.java"


# instance fields
.field private cyF:Landroid/graphics/drawable/Drawable;

.field private cyG:I

.field private cyH:I

.field private cyI:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/widget/d;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/d;-><init>()V

    sget-object v0, Lcom/android/setupwizardlib/g;->cxb:[I

    invoke-virtual {p1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/android/setupwizardlib/g;->cxd:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Lcom/android/setupwizardlib/g;->cxc:I

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    sget v3, Lcom/android/setupwizardlib/g;->cxe:I

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v1}, Lcom/android/setupwizardlib/h;->cdf(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v2}, Lcom/android/setupwizardlib/h;->cdh(I)V

    invoke-virtual {p0, v3}, Lcom/android/setupwizardlib/h;->cdg(I)V

    return-void
.end method

.method private cdi(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2, p1}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v1

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/b;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/h;->ayt(Landroid/support/v7/widget/p;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/setupwizardlib/h;->cyH:I

    if-nez v0, :cond_2

    return v4

    :cond_0
    iget v0, p0, Lcom/android/setupwizardlib/h;->cyH:I

    if-eq v0, v4, :cond_1

    if-ne v1, v2, :cond_2

    :cond_1
    return v3

    :cond_2
    if-ge v1, v2, :cond_3

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->doW(I)Landroid/support/v7/widget/p;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/h;->ays(Landroid/support/v7/widget/p;)Z

    move-result v0

    if-nez v0, :cond_3

    return v3

    :cond_3
    return v4
.end method


# virtual methods
.method protected ays(Landroid/support/v7/widget/p;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/setupwizardlib/i;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/setupwizardlib/i;

    invoke-interface {p1}, Lcom/android/setupwizardlib/i;->cbc()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected ayt(Landroid/support/v7/widget/p;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/setupwizardlib/i;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/setupwizardlib/i;

    invoke-interface {p1}, Lcom/android/setupwizardlib/i;->cbd()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cdd(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 1

    invoke-direct {p0, p2, p3}, Lcom/android/setupwizardlib/h;->cdi(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/setupwizardlib/h;->cyI:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/setupwizardlib/h;->cyI:I

    :goto_0
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/setupwizardlib/h;->cyG:I

    goto :goto_0
.end method

.method public cde(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/setupwizardlib/h;->cyF:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v3

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    iget v0, p0, Lcom/android/setupwizardlib/h;->cyI:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/setupwizardlib/h;->cyI:I

    :goto_0
    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p2, v1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5, p2}, Lcom/android/setupwizardlib/h;->cdi(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v5}, Landroid/support/v4/view/z;->dPy(Landroid/view/View;)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/android/setupwizardlib/h;->cyF:Landroid/graphics/drawable/Drawable;

    add-int v7, v5, v0

    invoke-virtual {v6, v2, v5, v4, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v5, p0, Lcom/android/setupwizardlib/h;->cyF:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/android/setupwizardlib/h;->cyG:I

    goto :goto_0

    :cond_3
    return-void
.end method

.method public cdf(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/android/setupwizardlib/h;->cyG:I

    :goto_0
    iput-object p1, p0, Lcom/android/setupwizardlib/h;->cyF:Landroid/graphics/drawable/Drawable;

    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/setupwizardlib/h;->cyG:I

    goto :goto_0
.end method

.method public cdg(I)V
    .locals 0

    iput p1, p0, Lcom/android/setupwizardlib/h;->cyH:I

    return-void
.end method

.method public cdh(I)V
    .locals 0

    iput p1, p0, Lcom/android/setupwizardlib/h;->cyI:I

    return-void
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/h;->cyF:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method
