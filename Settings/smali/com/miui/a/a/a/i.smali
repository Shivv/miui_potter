.class Lcom/miui/a/a/a/i;
.super Ljava/lang/Object;
.source "DiracUtils.java"


# static fields
.field public static cRA:Z

.field public static cRB:Z

.field public static cRC:Z

.field public static cRD:Z

.field public static cRE:Z

.field public static cRF:Z

.field public static cRG:Z

.field public static cRw:Z

.field public static cRx:Z

.field public static cRy:Z

.field public static cRz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRC:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRy:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRA:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRB:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRz:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRF:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRw:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRx:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRG:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRD:Z

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRE:Z

    :try_start_0
    const-string/jumbo v0, "miui.os.Build"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v1, "IS_MI2A"

    sget-boolean v2, Lcom/miui/a/a/a/i;->cRC:Z

    invoke-static {v0, v1, v2}, Lcom/miui/a/a/a/i;->cts(Ljava/lang/Class;Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/miui/a/a/a/i;->cRC:Z

    const-string/jumbo v1, "IS_HONGMI_TWO"

    sget-boolean v2, Lcom/miui/a/a/a/i;->cRy:Z

    invoke-static {v0, v1, v2}, Lcom/miui/a/a/a/i;->cts(Ljava/lang/Class;Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/miui/a/a/a/i;->cRy:Z

    const-string/jumbo v1, "IS_HONGMI_TWO_A"

    sget-boolean v2, Lcom/miui/a/a/a/i;->cRA:Z

    invoke-static {v0, v1, v2}, Lcom/miui/a/a/a/i;->cts(Ljava/lang/Class;Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/miui/a/a/a/i;->cRA:Z

    const-string/jumbo v1, "IS_HONGMI_TWO_S"

    sget-boolean v2, Lcom/miui/a/a/a/i;->cRB:Z

    invoke-static {v0, v1, v2}, Lcom/miui/a/a/a/i;->cts(Ljava/lang/Class;Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/miui/a/a/a/i;->cRB:Z

    const-string/jumbo v1, "IS_HONGMI_TWOS_LTE_MTK"

    sget-boolean v2, Lcom/miui/a/a/a/i;->cRz:Z

    invoke-static {v0, v1, v2}, Lcom/miui/a/a/a/i;->cts(Ljava/lang/Class;Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRz:Z

    const-string/jumbo v0, "meri"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRF:Z

    const-string/jumbo v0, "ro.product.mod_device"

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Lcom/miui/a/a/a/i;->ctt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "alpha"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRw:Z

    const-string/jumbo v0, "gemini"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRx:Z

    const-string/jumbo v0, "vince"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRG:Z

    const-string/jumbo v0, "rosy"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRD:Z

    const-string/jumbo v0, "sakura"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/a/a/a/i;->cRE:Z

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto/16 :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cts(Ljava/lang/Class;Ljava/lang/String;Z)Z
    .locals 3

    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "DiracUtils"

    const-string/jumbo v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return p2
.end method

.method private static ctt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    :try_start_0
    const-string/jumbo v0, "miui.os.SystemProperties"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "get"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "DiracUtils"

    const-string/jumbo v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, p1

    goto :goto_0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method static synthetic ctu(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0, p1}, Lcom/miui/a/a/a/i;->ctt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
