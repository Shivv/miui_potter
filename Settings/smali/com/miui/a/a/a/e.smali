.class public Lcom/miui/a/a/a/e;
.super Lcom/miui/a/a/a/h;
.source "SongDiracUtils.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private cRi:Lse/dirac/acs/api/AudioControlService;

.field private cRj:Z

.field private cRk:Ljava/lang/Object;

.field private cRl:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/miui/a/a/a/h;-><init>()V

    const-string/jumbo v0, "SongDiracUtils"

    iput-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/a/a/a/e;->cRk:Ljava/lang/Object;

    return-void
.end method

.method private static csK()Landroid/app/Application;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    const-string/jumbo v0, "android.app.ActivityThread"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v2, "currentApplication"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0
.end method

.method private csL(Landroid/content/Context;)Lse/dirac/acs/api/AudioControlService;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/miui/a/a/a/e;->cRi:Lse/dirac/acs/api/AudioControlService;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "initialize+"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v2, p0, Lcom/miui/a/a/a/e;->cRk:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v3

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/miui/a/a/a/e;->cRj:Z

    iget v0, p0, Lcom/miui/a/a/a/e;->cRl:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/miui/a/a/a/e;->cRl:I

    new-instance v0, Lcom/miui/a/a/a/f;

    invoke-direct {v0, p0}, Lcom/miui/a/a/a/f;-><init>(Lcom/miui/a/a/a/e;)V

    invoke-static {p1, v0}, Lse/dirac/acs/api/AudioControlService;->esU(Landroid/content/Context;Lse/dirac/acs/api/AudioControlService$Connection;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "could not bind against the control service"

    invoke-direct {v0, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_0
    move-exception v0

    iget v0, p0, Lcom/miui/a/a/a/e;->cRl:I

    :goto_1
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/miui/a/a/a/e;->cRl:I

    :goto_2
    iget-object v0, p0, Lcom/miui/a/a/a/e;->cRi:Lse/dirac/acs/api/AudioControlService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/a/a/a/e;->cRi:Lse/dirac/acs/api/AudioControlService;

    sget-object v2, Lse/dirac/acs/api/Output;->eYx:Lse/dirac/acs/api/Output;

    invoke-virtual {v0, v2}, Lse/dirac/acs/api/AudioControlService;->esS(Lse/dirac/acs/api/Output;)Lse/dirac/acs/api/OutputSettings;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "disable dirac by default"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/a/a/a/e;->cRi:Lse/dirac/acs/api/AudioControlService;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3, v1}, Lcom/miui/a/a/a/e;->csM(Lse/dirac/acs/api/AudioControlService;JZ)V

    :cond_0
    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "initialize-"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/miui/a/a/a/e;->cRi:Lse/dirac/acs/api/AudioControlService;

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "wait for connection"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/miui/a/a/a/e;->cRj:Z

    if-eqz v0, :cond_4

    invoke-static {}, Landroid/os/Looper;->loop()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_3
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget v0, p0, Lcom/miui/a/a/a/e;->cRl:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/miui/a/a/a/e;->cRl:I

    goto :goto_2

    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/miui/a/a/a/e;->cRk:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    :catch_1
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    iget v0, p0, Lcom/miui/a/a/a/e;->cRl:I

    goto :goto_1

    :catchall_1
    move-exception v0

    iget v1, p0, Lcom/miui/a/a/a/e;->cRl:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/miui/a/a/a/e;->cRl:I

    throw v0
.end method

.method private static csM(Lse/dirac/acs/api/AudioControlService;JZ)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, p1, p2}, Lse/dirac/acs/api/AudioControlService;->esT(J)Lse/dirac/acs/api/Device;

    move-result-object v1

    iget-object v0, v1, Lse/dirac/acs/api/Device;->eYD:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/dirac/acs/api/Filter;

    new-instance v2, Lse/dirac/acs/api/OutputSettings;

    invoke-direct {v2, v1, v0}, Lse/dirac/acs/api/OutputSettings;-><init>(Lse/dirac/acs/api/Device;Lse/dirac/acs/api/Filter;)V

    invoke-virtual {v2, p3}, Lse/dirac/acs/api/OutputSettings;->etL(Z)Lse/dirac/acs/api/OutputSettings;

    invoke-virtual {v2, v4}, Lse/dirac/acs/api/OutputSettings;->etM(Z)Lse/dirac/acs/api/OutputSettings;

    invoke-virtual {v2, v4}, Lse/dirac/acs/api/OutputSettings;->etJ(Z)Lse/dirac/acs/api/OutputSettings;

    invoke-virtual {v2, v3}, Lse/dirac/acs/api/OutputSettings;->etH(Z)Lse/dirac/acs/api/OutputSettings;

    invoke-virtual {p0, v2}, Lse/dirac/acs/api/AudioControlService;->esP(Lse/dirac/acs/api/OutputSettings;)Z

    return-void
.end method

.method static synthetic csN(Lcom/miui/a/a/a/e;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic csO(Lcom/miui/a/a/a/e;)Lse/dirac/acs/api/AudioControlService;
    .locals 1

    iget-object v0, p0, Lcom/miui/a/a/a/e;->cRi:Lse/dirac/acs/api/AudioControlService;

    return-object v0
.end method

.method static synthetic csP(Lcom/miui/a/a/a/e;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/a/a/a/e;->cRj:Z

    return v0
.end method

.method static synthetic csQ(Lcom/miui/a/a/a/e;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/miui/a/a/a/e;->cRk:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic csR(Lcom/miui/a/a/a/e;)I
    .locals 1

    iget v0, p0, Lcom/miui/a/a/a/e;->cRl:I

    return v0
.end method

.method static synthetic csS(Lcom/miui/a/a/a/e;Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService;
    .locals 0

    iput-object p1, p0, Lcom/miui/a/a/a/e;->cRi:Lse/dirac/acs/api/AudioControlService;

    return-object p1
.end method


# virtual methods
.method public csB(Landroid/content/Context;IF)V
    .locals 3

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setLevel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/miui/a/a/a/e;->csL(Landroid/content/Context;)Lse/dirac/acs/api/AudioControlService;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lse/dirac/acs/api/Output;->eYx:Lse/dirac/acs/api/Output;

    invoke-virtual {v0, v1}, Lse/dirac/acs/api/AudioControlService;->esS(Lse/dirac/acs/api/Output;)Lse/dirac/acs/api/OutputSettings;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lse/dirac/acs/api/OutputSettings;->etK(IF)Lse/dirac/acs/api/OutputSettings;

    invoke-virtual {v0, v1}, Lse/dirac/acs/api/AudioControlService;->esP(Lse/dirac/acs/api/OutputSettings;)Z

    :cond_0
    return-void
.end method

.method public csu()Ljava/util/List;
    .locals 8

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getHeadseIdsAndTypes"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/miui/a/a/a/e;->csK()Landroid/app/Application;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/a/a/a/e;->csL(Landroid/content/Context;)Lse/dirac/acs/api/AudioControlService;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    sget-object v2, Lse/dirac/acs/api/Output;->eYx:Lse/dirac/acs/api/Output;

    invoke-virtual {v0, v2}, Lse/dirac/acs/api/AudioControlService;->esL(Lse/dirac/acs/api/Output;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/dirac/acs/api/Device;

    const-string/jumbo v3, "MEP 100"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string/jumbo v3, "MEP 200"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string/jumbo v3, "Piston 100"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const-string/jumbo v3, "General Earbud"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    const-string/jumbo v3, "General InEar"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v3, "MK 101"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v3, "MK 301"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v3, "MK 303"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_8
    const-string/jumbo v3, "MO 701"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v3, "MR 102"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_a
    const-string/jumbo v3, "EM 303"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0xd

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_b
    const-string/jumbo v3, "EM 304"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_c
    const-string/jumbo v3, "EM 001"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_d
    const-string/jumbo v3, "EM 007"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_e
    const-string/jumbo v3, "HM 004"

    iget-object v4, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    new-instance v3, Landroid/util/Pair;

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_f
    iget-object v3, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "unknown device: name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lse/dirac/acs/api/Device;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v0, Lse/dirac/acs/api/Device;->eYA:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_10
    return-object v1
.end method

.method public csv(Landroid/content/Context;)I
    .locals 2

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getHeadsetType"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/miui/a/a/a/e;->csL(Landroid/content/Context;)Lse/dirac/acs/api/AudioControlService;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lse/dirac/acs/api/Output;->eYx:Lse/dirac/acs/api/Output;

    invoke-virtual {v0, v1}, Lse/dirac/acs/api/AudioControlService;->esS(Lse/dirac/acs/api/Output;)Lse/dirac/acs/api/OutputSettings;

    move-result-object v0

    iget-object v0, v0, Lse/dirac/acs/api/OutputSettings;->eYH:Lse/dirac/acs/api/Device;

    iget-wide v0, v0, Lse/dirac/acs/api/Device;->eYA:J

    long-to-int v0, v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public csy(Landroid/content/Context;Z)V
    .locals 3

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setEnabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/miui/a/a/a/e;->csL(Landroid/content/Context;)Lse/dirac/acs/api/AudioControlService;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lse/dirac/acs/api/Output;->eYx:Lse/dirac/acs/api/Output;

    invoke-virtual {v0, v1}, Lse/dirac/acs/api/AudioControlService;->esS(Lse/dirac/acs/api/Output;)Lse/dirac/acs/api/OutputSettings;

    move-result-object v1

    invoke-virtual {v1, p2}, Lse/dirac/acs/api/OutputSettings;->etL(Z)Lse/dirac/acs/api/OutputSettings;

    invoke-virtual {v0, v1}, Lse/dirac/acs/api/AudioControlService;->esP(Lse/dirac/acs/api/OutputSettings;)Z

    :cond_0
    return-void
.end method

.method public csz(Landroid/content/Context;I)V
    .locals 4

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setHeadsetType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/miui/a/a/a/e;->csL(Landroid/content/Context;)Lse/dirac/acs/api/AudioControlService;

    move-result-object v0

    if-eqz v0, :cond_0

    int-to-long v2, p2

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/miui/a/a/a/e;->csM(Lse/dirac/acs/api/AudioControlService;JZ)V

    :cond_0
    return-void
.end method

.method public isEnabled(Landroid/content/Context;)Z
    .locals 2

    iget-object v0, p0, Lcom/miui/a/a/a/e;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "isEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/miui/a/a/a/e;->csL(Landroid/content/Context;)Lse/dirac/acs/api/AudioControlService;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lse/dirac/acs/api/Output;->eYx:Lse/dirac/acs/api/Output;

    invoke-virtual {v0, v1}, Lse/dirac/acs/api/AudioControlService;->esS(Lse/dirac/acs/api/Output;)Lse/dirac/acs/api/OutputSettings;

    move-result-object v0

    invoke-virtual {v0}, Lse/dirac/acs/api/OutputSettings;->etI()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
