.class public Lcom/miui/a/a/a/j;
.super Lcom/miui/a/a/a/h;
.source "TaurusDiracUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/a/a/a/h;-><init>()V

    return-void
.end method


# virtual methods
.method public csB(Landroid/content/Context;IF)V
    .locals 5

    const/4 v4, 0x0

    const-string/jumbo v0, "diracband=%d;value=%f"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/miui/a/a/a/j;->ctd(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "TaurusDiracUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "set EQ Level: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_1

    invoke-super {p0, p1, v4, p3}, Lcom/miui/a/a/a/h;->csB(Landroid/content/Context;IF)V

    :cond_0
    :goto_0
    add-int/lit8 v0, p2, 0x1

    invoke-super {p0, p1, v0, p3}, Lcom/miui/a/a/a/h;->csB(Landroid/content/Context;IF)V

    return-void

    :cond_1
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    const/16 v0, 0x8

    invoke-super {p0, p1, v0, p3}, Lcom/miui/a/a/a/h;->csB(Landroid/content/Context;IF)V

    goto :goto_0
.end method
