.class public Lcom/xiaomi/g/c;
.super Ljava/lang/Object;
.source "TinyDataManager.java"


# static fields
.field private static dBH:Lcom/xiaomi/g/c;


# instance fields
.field private dBG:Ljava/util/Map;

.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/g/c;->dBG:Ljava/util/Map;

    iput-object p1, p0, Lcom/xiaomi/g/c;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/g/c;
    .locals 2

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    sget-object v0, Lcom/xiaomi/g/c;->dBH:Lcom/xiaomi/g/c;

    if-eqz v0, :cond_1

    :goto_0
    sget-object v0, Lcom/xiaomi/g/c;->dBH:Lcom/xiaomi/g/c;

    return-object v0

    :cond_0
    const-string/jumbo v0, "[TinyDataManager]:mContext is null, TinyDataManager.getInstance(Context) failed."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v1

    :cond_1
    const-class v1, Lcom/xiaomi/g/c;

    const-class v0, Lcom/xiaomi/g/c;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/g/c;->dBH:Lcom/xiaomi/g/c;

    if-eqz v0, :cond_2

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    new-instance v0, Lcom/xiaomi/g/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/g/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/g/c;->dBH:Lcom/xiaomi/g/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public dgQ(Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;Ljava/lang/String;)Z
    .locals 2

    const/4 v1, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, v1}, Lcom/xiaomi/push/service/y;->cNP(Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;Z)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_0
    invoke-virtual {p1, p2}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->cWB(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;

    iget-object v0, p0, Lcom/xiaomi/g/c;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/xiaomi/push/service/H;->cOb(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string/jumbo v0, "pkgName is null or empty, upload ClientUploadDataItem failed."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return v1

    :cond_1
    return v1

    :cond_2
    invoke-static {}, Lcom/xiaomi/push/service/y;->cNQ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->cWf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;

    goto :goto_0
.end method

.method dgR()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/g/c;->dBG:Ljava/util/Map;

    return-object v0
.end method

.method public dgS(Lcom/xiaomi/g/b;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/g/c;->dgR()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    const-string/jumbo v0, "[TinyDataManager]: please do not add null mUploader to TinyDataManager."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string/jumbo v0, "[TinyDataManager]: can not add a provider from unkown resource."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-void
.end method

.method dgT()Lcom/xiaomi/g/b;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/xiaomi/g/c;->dBG:Ljava/util/Map;

    const-string/jumbo v1, "UPLOADER_PUSH_CHANNEL"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/g/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/g/c;->dBG:Ljava/util/Map;

    const-string/jumbo v1, "UPLOADER_HTTP"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/g/b;

    if-nez v0, :cond_1

    return-object v2

    :cond_0
    return-object v0

    :cond_1
    return-object v0
.end method
