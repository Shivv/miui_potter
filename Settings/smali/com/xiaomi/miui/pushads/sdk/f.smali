.class public Lcom/xiaomi/miui/pushads/sdk/f;
.super Lcom/xiaomi/miui/pushads/sdk/b/a;
.source "NotifyAdsCell.java"


# instance fields
.field public cSu:Ljava/lang/String;

.field public cSv:Ljava/lang/String;

.field public cSw:Ljava/lang/String;

.field public cSx:Ljava/lang/String;

.field public cSy:Ljava/lang/String;

.field public cSz:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/miui/pushads/sdk/b/a;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/xiaomi/miui/pushads/sdk/f;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/miui/pushads/sdk/b/a;-><init>(Lcom/xiaomi/miui/pushads/sdk/b/a;)V

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/f;->cSx:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSx:Ljava/lang/String;

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/f;->cSy:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSy:Ljava/lang/String;

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/f;->cSz:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSz:Ljava/lang/String;

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/f;->cSv:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSv:Ljava/lang/String;

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/f;->cSu:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSu:Ljava/lang/String;

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/f;->type:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->type:Ljava/lang/String;

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/f;->cSw:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSw:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public ctP()Landroid/os/Bundle;
    .locals 3

    invoke-super {p0}, Lcom/xiaomi/miui/pushads/sdk/b/a;->ctP()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "actionUrl"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSx:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "imgUrl"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSy:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "titText"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSz:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "priText"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSv:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "secText"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSu:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "type"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "actionText"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSw:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public ctQ(Lorg/json/JSONObject;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/xiaomi/miui/pushads/sdk/b/a;->ctQ(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "actionUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSx:Ljava/lang/String;

    const-string/jumbo v0, "imgUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSy:Ljava/lang/String;

    const-string/jumbo v0, "titText"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSz:Ljava/lang/String;

    const-string/jumbo v0, "priText"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSv:Ljava/lang/String;

    const-string/jumbo v0, "secText"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSu:Ljava/lang/String;

    const-string/jumbo v0, "type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->type:Ljava/lang/String;

    const-string/jumbo v0, "actionText"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSw:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string/jumbo v1, "showType"

    iget v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSc:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "lastShowTime"

    iget-wide v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSd:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "actionUrl"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSx:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "type"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "imgUrl"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSy:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "receiveUpperBound"

    iget v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSg:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "downloadedPath"

    invoke-virtual {p0}, Lcom/xiaomi/miui/pushads/sdk/f;->ctR()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "titText"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSz:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "priText"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSv:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "secText"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSu:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "actionText"

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/f;->cSw:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0
.end method
