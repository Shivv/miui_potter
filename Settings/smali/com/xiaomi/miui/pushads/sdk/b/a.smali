.class public Lcom/xiaomi/miui/pushads/sdk/b/a;
.super Ljava/lang/Object;
.source "MiuiAdsCell.java"


# instance fields
.field private cSa:Ljava/lang/String;

.field public cSb:I

.field public cSc:I

.field public cSd:J

.field public cSe:I

.field public cSf:J

.field public cSg:I

.field public cSh:I

.field public cSi:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/xiaomi/miui/pushads/sdk/b/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-wide v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSf:J

    iput-wide v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSf:J

    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSi:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSi:Ljava/lang/String;

    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSe:I

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSe:I

    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSg:I

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSg:I

    iget-wide v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSd:J

    iput-wide v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSd:J

    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSh:I

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSh:I

    iget-object v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSa:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSa:Ljava/lang/String;

    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSb:I

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSb:I

    return-void
.end method


# virtual methods
.method public ctP()Landroid/os/Bundle;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "id"

    iget-wide v2, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSf:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v1, "showType"

    iget v2, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "nonsense"

    iget v2, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSe:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "receiveUpperBound"

    iget v2, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSg:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "lastShowTime"

    iget-wide v2, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSd:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v1, "multi"

    iget v2, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSb:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method public ctQ(Lorg/json/JSONObject;)V
    .locals 2

    const-string/jumbo v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSf:J

    const-string/jumbo v0, "showType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    const-string/jumbo v0, "nonsense"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSe:I

    const-string/jumbo v0, "receiveUpperBound"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSg:I

    const-string/jumbo v0, "lastShowTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSd:J

    const-string/jumbo v0, "multi"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSb:I

    return-void
.end method

.method public ctR()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSa:Ljava/lang/String;

    return-object v0
.end method

.method public ctS(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSa:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, ""

    return-object v0
.end method
