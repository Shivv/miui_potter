.class public final enum Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;
.super Ljava/lang/Enum;
.source "NotifyAdsManager.java"


# static fields
.field public static final enum cSo:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

.field public static final enum cSp:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

.field public static final enum cSq:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

.field public static final enum cSr:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

.field private static final synthetic cSs:[Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

.field public static final enum cSt:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSp:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    const-string/jumbo v1, "Wifi"

    invoke-direct {v0, v1, v3}, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSt:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    const-string/jumbo v1, "MN2G"

    invoke-direct {v0, v1, v4}, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSq:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    const-string/jumbo v1, "MN3G"

    invoke-direct {v0, v1, v5}, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSo:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    const-string/jumbo v1, "MN4G"

    invoke-direct {v0, v1, v6}, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSr:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    sget-object v1, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSp:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSt:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSq:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSo:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSr:Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSs:[Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;
    .locals 1

    const-class v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;
    .locals 1

    sget-object v0, Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;->cSs:[Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    return-object v0
.end method
