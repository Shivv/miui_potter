.class Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;
.super Landroid/widget/RemoteViews;
.source "NotificationBaseRemoteView.java"


# instance fields
.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "notification_base_layout"

    const-string/jumbo v3, "layout"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public ctV(Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "action_button"

    const-string/jumbo v2, "id"

    iget-object v3, p0, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, v0, p1}, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->setTextViewText(ILjava/lang/CharSequence;)V

    if-eqz p2, :cond_1

    invoke-virtual {p0, v0, p2}, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->setViewVisibility(II)V

    :goto_0
    return-void

    :cond_2
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->setViewVisibility(II)V

    goto :goto_0
.end method

.method public ctW(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v5, 0x0

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v0, ""

    move-object v6, v0

    move-object v0, p2

    move-object p2, v6

    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "sub_title"

    const-string/jumbo v3, "id"

    iget-object v4, p0, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1, p2}, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p0, v1, v5}, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->setViewVisibility(II)V

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "title"

    const-string/jumbo v3, "id"

    iget-object v4, p0, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->setTextViewText(ILjava/lang/CharSequence;)V

    return-void

    :cond_4
    move-object v0, p1

    goto :goto_0
.end method

.method public setIcon(I)V
    .locals 1

    const v0, 0x1020006

    invoke-virtual {p0, v0, p1}, Lcom/xiaomi/miui/pushads/sdk/NotificationBaseRemoteView;->setImageViewResource(II)V

    return-void
.end method
