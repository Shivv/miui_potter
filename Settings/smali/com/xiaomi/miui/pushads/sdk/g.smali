.class public Lcom/xiaomi/miui/pushads/sdk/g;
.super Lcom/xiaomi/mipush/sdk/q;
.source "NotifyAdsManager.java"

# interfaces
.implements Lcom/xiaomi/miui/pushads/sdk/a;


# static fields
.field private static cSD:Lcom/xiaomi/miui/pushads/sdk/g;


# instance fields
.field private cSA:Landroid/os/Handler;

.field private cSB:Lcom/xiaomi/miui/pushads/sdk/b/d;

.field private cSC:Landroid/content/SharedPreferences;

.field private cSE:I

.field private cSF:Lcom/xiaomi/miui/pushads/sdk/c;

.field private cSG:Ljava/lang/String;

.field private cSH:Ljava/lang/String;

.field private cSI:I

.field private mContext:Landroid/content/Context;


# direct methods
.method private cum(Ljava/lang/String;ILjava/lang/String;)V
    .locals 7

    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/j;

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/g;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/xiaomi/miui/pushads/sdk/j;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;ILjava/lang/String;Lcom/xiaomi/miui/pushads/sdk/a;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/miui/pushads/sdk/j;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public static cun(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "ads-notify-fd5dfce4"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private cuo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/xiaomi/miui/pushads/sdk/g;->cum(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private cup(Lcom/xiaomi/miui/pushads/sdk/b/a;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSg:I

    if-gtz v0, :cond_0

    const-string/jumbo v0, "\u767d\u540d\u5355\u7528\u6237"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    return v4

    :cond_0
    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    packed-switch v0, :pswitch_data_0

    move v0, v1

    move v2, v1

    :goto_0
    if-gt v2, v0, :cond_1

    return v4

    :pswitch_0
    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSg:I

    mul-int/lit8 v0, v0, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\u5192\u6ce1\u4e0a\u9650: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "bubblecount"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_1
    iget v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSg:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\u901a\u77e5\u4e0a\u9650: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "notifycount"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\u5e7f\u544a\u6b21\u6570\u8d85\u8fc7\u4e0a\u9650---\u5df2\u7ecf\u83b7\u5f97\u6b21\u6570\uff1a "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " \u4e0a\u9650: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private cuu(Ljava/lang/String;JI)V
    .locals 2

    iget v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSI:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSI:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u5b58\u5165cache \u7684\u6570\u91cf: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSI:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSF:Lcom/xiaomi/miui/pushads/sdk/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/miui/pushads/sdk/c;->cub(Ljava/lang/String;JI)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSF:Lcom/xiaomi/miui/pushads/sdk/c;

    invoke-virtual {v0}, Lcom/xiaomi/miui/pushads/sdk/c;->cua()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/xiaomi/miui/pushads/sdk/g;
    .locals 2

    const-class v0, Lcom/xiaomi/miui/pushads/sdk/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/miui/pushads/sdk/g;->cSD:Lcom/xiaomi/miui/pushads/sdk/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public ctX(ILcom/xiaomi/miui/pushads/sdk/b/a;Lcom/xiaomi/miui/pushads/sdk/j;)V
    .locals 4

    if-nez p2, :cond_0

    const-string/jumbo v0, "\u8fd4\u56de\u5e7f\u544a\u4e3anull"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u5e7f\u544a\u4e0b\u8f7d\u5931\u8d25: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSf:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    iget v0, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSh:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSh:I

    iget v0, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSh:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u4e0b\u8f7d\u5931\u8d25\u5199\u5165\u7f13\u5b58 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSd:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSh:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    iget-object v0, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSi:Ljava/lang/String;

    iget-wide v2, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSd:J

    iget v1, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSh:I

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/xiaomi/miui/pushads/sdk/g;->cuu(Ljava/lang/String;JI)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSB:Lcom/xiaomi/miui/pushads/sdk/b/d;

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    invoke-direct {p0, p2}, Lcom/xiaomi/miui/pushads/sdk/g;->cup(Lcom/xiaomi/miui/pushads/sdk/b/a;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "===========\u7ed9APP \u53d1\u9001\u5e7f\u544a\u4fe1\u606f"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSB:Lcom/xiaomi/miui/pushads/sdk/b/d;

    invoke-interface {v0, p2}, Lcom/xiaomi/miui/pushads/sdk/b/d;->ctU(Lcom/xiaomi/miui/pushads/sdk/b/a;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string/jumbo v0, "\u4e0b\u8f7d\u5931\u8d25\u6b21\u6570\u8d85\u8fc7 10 \u4e0d\u5199\u5165\u7f13\u5b58"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-nez p1, :cond_5

    iget v0, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSg:I

    if-lez v0, :cond_4

    iget v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSE:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSE:I

    invoke-static {}, Lcom/xiaomi/miui/pushads/sdk/g;->getInstance()Lcom/xiaomi/miui/pushads/sdk/g;

    move-result-object v0

    iget v1, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/miui/pushads/sdk/g;->cur(I)V

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u5e7f\u544a\u4e0b\u8f7d\u6210\u529f: id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSf:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \u7c7b\u578b: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \u6210\u529f\u6b21\u6570: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/miui/pushads/sdk/g;->getInstance()Lcom/xiaomi/miui/pushads/sdk/g;

    move-result-object v1

    iget v2, p2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    invoke-virtual {v1, v2}, Lcom/xiaomi/miui/pushads/sdk/g;->cux(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string/jumbo v0, "com.miui.ads"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\u5e7f\u544a\u65e0\u6548\u6216\u8005\u8d85\u8fc7\u9650\u5236 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "\u5e7f\u544a\u65e0\u6548\u6216\u8005\u8d85\u8fc7\u9650\u5236"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v0, "\u5e7f\u544a\u6570\u91cf\u8d85\u8fc7\u9650\u5236\uff0c\u4e0d\u8fd4\u56de\u7ed9APP"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public cuq(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u63a5\u53d7\u5230\u6d88\u606f "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSG:Ljava/lang/String;

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/h;->cuz(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u6ca1\u6709\u6709\u6548alias\uff0c\u5ffd\u7565\u6d88\u606f "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p2}, Lcom/xiaomi/miui/pushads/sdk/h;->cuz(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSG:Ljava/lang/String;

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/h;->cuz(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSG:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u63a5\u53d7\u5230\u4e0d\u540calias \u7684\u6d88\u606f\uff0c\u6ce8\u9500\u65e7\u7684 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/xiaomi/miui/pushads/sdk/g;->cEq()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p2, v1}, Lcom/xiaomi/mipush/sdk/C;->cGJ(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSH:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cuo(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized cur(I)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "notifycount"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "notifycount"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "bubblecount"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "bubblecount"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cus(JLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public cut(JLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSB:Lcom/xiaomi/miui/pushads/sdk/b/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSA:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    long-to-int v1, p1

    iput v1, v0, Landroid/os/Message;->arg1:I

    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSA:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    const-string/jumbo v0, "\u901a\u9053\u8fdb\u884c\u521d\u59cb\u5316OK"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSA:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSA:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "\u901a\u9053\u521d\u59cb\u5316\u5931\u8d25\uff0c \u5df2\u7ecf\u901a\u77e5\u4e86app\uff0c\u9700\u8981\u91cd\u65b0 open \u901a\u9053"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cuv(JLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public cuw(Ljava/lang/String;JLjava/lang/String;Ljava/util/List;)V
    .locals 4

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u547d\u4ee4\u5931\u8d25: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    move v1, v2

    :goto_0
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "param: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "set-alias"

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v2

    :goto_1
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-interface {p5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    const-string/jumbo v1, "\u8bbe\u7f6e\u522b\u540d\u6210\u529f: "

    invoke-static {v1}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    :goto_2
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    if-nez v1, :cond_2

    const-string/jumbo v0, "\u8bbe\u7f6e\u522b\u540d\u5931\u8d25\uff0c\u91cd\u65b0\u8bbe\u7f6e: "

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSA:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public declared-synchronized cux(I)I
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "notifycount"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/g;->cSC:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "bubblecount"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
