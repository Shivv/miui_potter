.class public final Lcom/xiaomi/c/f;
.super Ljava/lang/Object;
.source "Host.java"


# instance fields
.field private dln:I

.field private dlo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/c/f;->dlo:Ljava/lang/String;

    iput p2, p0, Lcom/xiaomi/c/f;->dln:I

    return-void
.end method

.method public static cUK(Ljava/lang/String;I)Ljava/net/InetSocketAddress;
    .locals 3

    invoke-static {p0, p1}, Lcom/xiaomi/c/f;->cUL(Ljava/lang/String;I)Lcom/xiaomi/c/f;

    move-result-object v0

    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Lcom/xiaomi/c/f;->cUJ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/c/f;->cUM()I

    move-result v0

    invoke-direct {v1, v2, v0}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    return-object v1
.end method

.method public static cUL(Ljava/lang/String;I)Lcom/xiaomi/c/f;
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :goto_0
    new-instance v0, Lcom/xiaomi/c/f;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/c/f;-><init>(Ljava/lang/String;I)V

    return-object v0

    :cond_0
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-lez v0, :cond_1

    move p1, v0

    :cond_1
    move-object p0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object p0, v1

    goto :goto_0
.end method


# virtual methods
.method public cUJ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/c/f;->dlo:Ljava/lang/String;

    return-object v0
.end method

.method public cUM()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/c/f;->dln:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/xiaomi/c/f;->dln:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/c/f;->dlo:Ljava/lang/String;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/c/f;->dlo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/c/f;->dln:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
