.class Lcom/xiaomi/c/m;
.super Lcom/xiaomi/c/e;
.source "HostManager.java"


# instance fields
.field final synthetic dlM:Lcom/xiaomi/c/e;

.field dlN:Lcom/xiaomi/c/e;

.field final synthetic dlO:Lcom/xiaomi/c/g;


# direct methods
.method constructor <init>(Lcom/xiaomi/c/g;Ljava/lang/String;Lcom/xiaomi/c/e;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/c/m;->dlO:Lcom/xiaomi/c/g;

    iput-object p3, p0, Lcom/xiaomi/c/m;->dlM:Lcom/xiaomi/c/e;

    invoke-direct {p0, p2}, Lcom/xiaomi/c/e;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/c/m;->dlM:Lcom/xiaomi/c/e;

    iput-object v0, p0, Lcom/xiaomi/c/m;->dlN:Lcom/xiaomi/c/e;

    iget-object v0, p0, Lcom/xiaomi/c/m;->host:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/c/m;->host:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/c/m;->dlM:Lcom/xiaomi/c/e;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/c/m;->dlM:Lcom/xiaomi/c/e;

    iget-object v0, v0, Lcom/xiaomi/c/e;->dlf:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/c/m;->dlf:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized cUE(Z)Ljava/util/ArrayList;
    .locals 6

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/c/m;->dlN:Lcom/xiaomi/c/e;

    if-nez v0, :cond_0

    :goto_0
    sget-object v2, Lcom/xiaomi/c/g;->dlt:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, Lcom/xiaomi/c/g;->dlt:Ljava/util/Map;

    iget-object v3, p0, Lcom/xiaomi/c/m;->host:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/c/e;

    if-nez v0, :cond_1

    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/xiaomi/c/m;->dlN:Lcom/xiaomi/c/e;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/xiaomi/c/e;->cUE(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v3, 0x1

    :try_start_3
    invoke-virtual {v0, v3}, Lcom/xiaomi/c/e;->cUE(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/c/m;->host:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/c/m;->host:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    :try_start_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2
.end method

.method public cUo()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized cUp(Ljava/lang/String;Lcom/xiaomi/c/j;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/c/m;->dlN:Lcom/xiaomi/c/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/c/m;->dlN:Lcom/xiaomi/c/e;

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/c/e;->cUp(Ljava/lang/String;Lcom/xiaomi/c/j;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
