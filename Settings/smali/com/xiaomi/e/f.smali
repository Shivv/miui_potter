.class Lcom/xiaomi/e/f;
.super Ljava/lang/Object;
.source "BindTracker.java"

# interfaces
.implements Lcom/xiaomi/push/service/ae;


# instance fields
.field private dBm:Lcom/xiaomi/push/service/XMPushService;

.field private dBn:Z

.field private dBo:Lcom/xiaomi/push/service/as;

.field private dBp:I

.field private dBq:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

.field private dBr:Lcom/xiaomi/smack/e;


# direct methods
.method constructor <init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/e/f;->dBn:Z

    iput-object p1, p0, Lcom/xiaomi/e/f;->dBm:Lcom/xiaomi/push/service/XMPushService;

    sget-object v0, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfV:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput-object v0, p0, Lcom/xiaomi/e/f;->dBq:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput-object p2, p0, Lcom/xiaomi/e/f;->dBo:Lcom/xiaomi/push/service/as;

    return-void
.end method

.method private dgB()V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/xiaomi/e/f;->dgz()V

    iget-boolean v0, p0, Lcom/xiaomi/e/f;->dBn:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/xiaomi/e/f;->dBp:I

    const/16 v2, 0xb

    if-eq v0, v2, :cond_1

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/e/e;->dgs()Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/e/g;->dBs:[I

    iget-object v3, p0, Lcom/xiaomi/e/f;->dBq:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    invoke-virtual {v3}, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-nez v0, :cond_4

    :goto_1
    return-void

    :cond_0
    return-void

    :cond_1
    return-void

    :pswitch_1
    iget v2, p0, Lcom/xiaomi/e/f;->dBp:I

    const/16 v3, 0x11

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/xiaomi/e/f;->dBp:I

    const/16 v3, 0x15

    if-eq v2, v3, :cond_3

    :try_start_0
    invoke-static {}, Lcom/xiaomi/e/e;->getContext()Lcom/xiaomi/e/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/e/b;->dgc()Ljava/lang/Exception;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/e/k;->dgG(Ljava/lang/Exception;)Lcom/xiaomi/e/c;

    move-result-object v2

    iget-object v3, v2, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v3

    iput v3, v0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    iget-object v2, v2, Lcom/xiaomi/e/c;->annotation:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/thrift/StatsEvent;->cTv(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dki:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v1}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v1

    iput v1, v0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djL:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v1}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v1

    iput v1, v0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    goto :goto_0

    :pswitch_2
    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djM:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v1}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v1

    iput v1, v0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/xiaomi/e/f;->dBr:Lcom/xiaomi/smack/e;

    invoke-virtual {v1}, Lcom/xiaomi/smack/e;->cxk()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTw(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-object v1, p0, Lcom/xiaomi/e/f;->dBo:Lcom/xiaomi/push/service/as;

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTW(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    const/4 v1, 0x1

    iput v1, v0, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/e/f;->dBo:Lcom/xiaomi/push/service/as;

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTP(B)Lcom/xiaomi/push/thrift/StatsEvent;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic dgy(Lcom/xiaomi/e/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/e/f;->dgB()V

    return-void
.end method

.method private dgz()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/e/f;->dBo:Lcom/xiaomi/push/service/as;

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/service/as;->cRk(Lcom/xiaomi/push/service/ae;)V

    return-void
.end method


# virtual methods
.method public cOX(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;I)V
    .locals 3

    iget-boolean v0, p0, Lcom/xiaomi/e/f;->dBn:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/e/f;->dBm:Lcom/xiaomi/push/service/XMPushService;

    new-instance v1, Lcom/xiaomi/e/a;

    const/4 v2, 0x4

    invoke-direct {v1, p0, v2}, Lcom/xiaomi/e/a;-><init>(Lcom/xiaomi/e/f;I)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    return-void

    :cond_1
    sget-object v0, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfV:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-ne p1, v0, :cond_0

    iput-object p2, p0, Lcom/xiaomi/e/f;->dBq:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput p3, p0, Lcom/xiaomi/e/f;->dBp:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/e/f;->dBn:Z

    goto :goto_0
.end method

.method dgA()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/e/f;->dBo:Lcom/xiaomi/push/service/as;

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/service/as;->cRq(Lcom/xiaomi/push/service/ae;)V

    iget-object v0, p0, Lcom/xiaomi/e/f;->dBm:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/XMPushService;->cQs()Lcom/xiaomi/smack/e;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/e/f;->dBr:Lcom/xiaomi/smack/e;

    return-void
.end method
