.class public Lcom/xiaomi/e/b;
.super Ljava/lang/Object;
.source "StatsContext.java"

# interfaces
.implements Lcom/xiaomi/smack/a;


# instance fields
.field dAU:Lcom/xiaomi/push/service/XMPushService;

.field private dAV:J

.field private dAW:J

.field private dAX:Ljava/lang/Exception;

.field private dAY:J

.field dAZ:Lcom/xiaomi/smack/e;

.field private dBa:Ljava/lang/String;

.field private dBb:J

.field private dBc:I

.field private dBd:J

.field private dBe:J


# direct methods
.method constructor <init>(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 4

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBe:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dAY:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dAW:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBd:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBb:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dAV:J

    iput-object p1, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/e/b;->dBa:Ljava/lang/String;

    invoke-direct {p0}, Lcom/xiaomi/e/b;->dgb()V

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {v0}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/e/b;->dAV:J

    invoke-static {v0}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBb:J

    return-void
.end method

.method private declared-synchronized dga()V
    .locals 6

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stat connpt = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/e/b;->dBa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " netDuration = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/xiaomi/e/b;->dAY:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ChannelDuration = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/xiaomi/e/b;->dBd:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " channelConnectedTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/xiaomi/e/b;->dAW:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-direct {v0}, Lcom/xiaomi/push/thrift/StatsEvent;-><init>()V

    const/4 v1, 0x0

    iput-byte v1, v0, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djS:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v1}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-object v1, p0, Lcom/xiaomi/e/b;->dBa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTQ(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTL(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-wide v2, p0, Lcom/xiaomi/e/b;->dAY:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTU(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-wide v2, p0, Lcom/xiaomi/e/b;->dBd:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTD(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V

    invoke-direct {p0}, Lcom/xiaomi/e/b;->dgb()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private dgb()V
    .locals 3

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dAY:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBd:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBe:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dAW:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v2}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    return-void

    :cond_0
    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBe:J

    goto :goto_0

    :cond_1
    iput-wide v0, p0, Lcom/xiaomi/e/b;->dAW:J

    goto :goto_1
.end method


# virtual methods
.method public cwY(Lcom/xiaomi/smack/e;)V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Lcom/xiaomi/e/b;->dBc:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/e/b;->dAX:Ljava/lang/Exception;

    iput-object p1, p0, Lcom/xiaomi/e/b;->dAZ:Lcom/xiaomi/smack/e;

    iget-object v0, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/e/b;->dBa:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djN:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v0}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/xiaomi/e/d;->dgn(II)V

    return-void
.end method

.method public cwZ(Lcom/xiaomi/smack/e;)V
    .locals 4

    invoke-virtual {p0}, Lcom/xiaomi/e/b;->dfZ()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dAW:J

    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djN:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v0}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/smack/e;->cxk()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/smack/e;->cxo()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v3, v0, v1, v2}, Lcom/xiaomi/e/d;->dgh(IILjava/lang/String;I)V

    return-void
.end method

.method public cxa(Lcom/xiaomi/smack/e;ILjava/lang/Exception;)V
    .locals 8

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    iget v0, p0, Lcom/xiaomi/e/b;->dBc:I

    if-eqz v0, :cond_2

    :cond_0
    :goto_0
    const/16 v0, 0x16

    if-eq p2, v0, :cond_3

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/e/b;->dfZ()V

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {v0}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v2

    invoke-static {v0}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Stats rx="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/xiaomi/e/b;->dAV:J

    sub-long v6, v2, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", tx="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/xiaomi/e/b;->dBb:J

    sub-long v6, v0, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iput-wide v2, p0, Lcom/xiaomi/e/b;->dAV:J

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBb:J

    return-void

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/e/b;->dAX:Ljava/lang/Exception;

    if-nez v0, :cond_0

    iput p2, p0, Lcom/xiaomi/e/b;->dBc:I

    iput-object p3, p0, Lcom/xiaomi/e/b;->dAX:Ljava/lang/Exception;

    invoke-virtual {p1}, Lcom/xiaomi/smack/e;->cxk()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/xiaomi/e/d;->dgg(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    iget-wide v0, p0, Lcom/xiaomi/e/b;->dAW:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/smack/e;->cxK()J

    move-result-wide v0

    iget-wide v6, p0, Lcom/xiaomi/e/b;->dAW:J

    sub-long/2addr v0, v6

    cmp-long v5, v0, v2

    if-ltz v5, :cond_4

    const/4 v4, 0x1

    :cond_4
    if-nez v4, :cond_5

    move-wide v0, v2

    :cond_5
    invoke-static {}, Lcom/xiaomi/smack/f;->cxQ()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-long v4, v4

    add-long/2addr v0, v4

    iget-wide v4, p0, Lcom/xiaomi/e/b;->dBd:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/xiaomi/e/b;->dBd:J

    iput-wide v2, p0, Lcom/xiaomi/e/b;->dAW:J

    goto :goto_1
.end method

.method public cxb(Lcom/xiaomi/smack/e;Ljava/lang/Exception;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sget-object v3, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkw:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v3

    invoke-virtual {p1}, Lcom/xiaomi/smack/e;->cxk()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v2, v4, v0}, Lcom/xiaomi/e/d;->dgi(IIILjava/lang/String;I)V

    invoke-virtual {p0}, Lcom/xiaomi/e/b;->dfZ()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public declared-synchronized dfZ()V
    .locals 14

    const/4 v0, 0x1

    const-wide/16 v12, 0x0

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/xiaomi/e/b;->dBe:J

    cmp-long v2, v8, v12

    if-gtz v2, :cond_4

    move v2, v0

    :goto_0
    if-nez v2, :cond_0

    iget-wide v8, p0, Lcom/xiaomi/e/b;->dAY:J

    iget-wide v10, p0, Lcom/xiaomi/e/b;->dBe:J

    sub-long v10, v6, v10

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/xiaomi/e/b;->dAY:J

    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/xiaomi/e/b;->dBe:J

    :cond_0
    iget-wide v8, p0, Lcom/xiaomi/e/b;->dAW:J

    cmp-long v2, v8, v12

    if-eqz v2, :cond_1

    iget-wide v8, p0, Lcom/xiaomi/e/b;->dBd:J

    iget-wide v10, p0, Lcom/xiaomi/e/b;->dAW:J

    sub-long v10, v6, v10

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/xiaomi/e/b;->dBd:J

    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/xiaomi/e/b;->dAW:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    if-nez v4, :cond_5

    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    :cond_3
    monitor-exit p0

    return-void

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    :try_start_1
    iget-object v2, p0, Lcom/xiaomi/e/b;->dBa:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    :goto_2
    iget-wide v4, p0, Lcom/xiaomi/e/b;->dAY:J

    const-wide/32 v8, 0x5265c0

    cmp-long v2, v4, v8

    if-gtz v2, :cond_b

    :goto_3
    if-nez v0, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/xiaomi/e/b;->dga()V

    :cond_7
    iput-object v3, p0, Lcom/xiaomi/e/b;->dBa:Ljava/lang/String;

    iget-wide v0, p0, Lcom/xiaomi/e/b;->dBe:J

    cmp-long v0, v0, v12

    if-nez v0, :cond_8

    iput-wide v6, p0, Lcom/xiaomi/e/b;->dBe:J

    :cond_8
    iget-object v0, p0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-wide v6, p0, Lcom/xiaomi/e/b;->dAW:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_9
    :try_start_2
    iget-wide v4, p0, Lcom/xiaomi/e/b;->dAY:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-wide/16 v8, 0x7530

    cmp-long v2, v4, v8

    if-lez v2, :cond_a

    move v2, v0

    :goto_4
    if-nez v2, :cond_6

    goto :goto_2

    :cond_a
    move v2, v1

    goto :goto_4

    :cond_b
    move v0, v1

    goto :goto_3
.end method

.method dgc()Ljava/lang/Exception;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/e/b;->dAX:Ljava/lang/Exception;

    return-object v0
.end method
