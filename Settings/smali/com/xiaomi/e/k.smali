.class final Lcom/xiaomi/e/k;
.super Ljava/lang/Object;
.source "StatsAnalyser.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static dgC(Ljava/lang/Exception;)Lcom/xiaomi/e/c;
    .locals 5

    invoke-static {p0}, Lcom/xiaomi/e/k;->dgD(Ljava/lang/Exception;)V

    instance-of v0, p0, Lcom/xiaomi/smack/XMPPException;

    if-nez v0, :cond_3

    :cond_0
    :goto_0
    new-instance v0, Lcom/xiaomi/e/c;

    invoke-direct {v0}, Lcom/xiaomi/e/c;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/xiaomi/smack/l;->cyt(Ljava/lang/Throwable;)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sparse-switch v2, :sswitch_data_0

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djZ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    :cond_1
    :goto_1
    iget-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-ne v1, v2, :cond_4

    :cond_2
    iput-object v3, v0, Lcom/xiaomi/e/c;->annotation:Ljava/lang/String;

    :goto_2
    return-object v0

    :cond_3
    move-object v0, p0

    check-cast v0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {v0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast p0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {p0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    :sswitch_0
    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkb:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_1

    :sswitch_1
    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkd:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_1

    :sswitch_2
    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dks:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_1

    :sswitch_3
    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_1

    :sswitch_4
    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkA:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v2, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v2, "Terminal binding condition encountered: item-not-found"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djY:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_1

    :cond_4
    iget-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->djZ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-eq v1, v2, :cond_2

    iget-object v1, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkA:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-eq v1, v2, :cond_2

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_0
        0x6d -> :sswitch_1
        0x6e -> :sswitch_2
        0xc7 -> :sswitch_3
        0x1f3 -> :sswitch_4
    .end sparse-switch
.end method

.method private static dgD(Ljava/lang/Exception;)V
    .locals 1

    if-eqz p0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
.end method

.method static dgE(Ljava/lang/Exception;)Lcom/xiaomi/e/c;
    .locals 4

    invoke-static {p0}, Lcom/xiaomi/e/k;->dgD(Ljava/lang/Exception;)V

    instance-of v0, p0, Lcom/xiaomi/smack/XMPPException;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    new-instance v1, Lcom/xiaomi/e/c;

    invoke-direct {v1}, Lcom/xiaomi/e/c;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/smack/l;->cyt(Ljava/lang/Throwable;)I

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-eqz v2, :cond_4

    :goto_3
    iget-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v3, Lcom/xiaomi/push/thrift/ChannelStatsType;->dko:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-eq v2, v3, :cond_5

    :goto_4
    return-object v1

    :cond_1
    move-object v0, p0

    check-cast v0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {v0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast p0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {p0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkg:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2}, Lcom/xiaomi/push/thrift/ChannelStatsType;->cTZ(I)Lcom/xiaomi/push/thrift/ChannelStatsType;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_2

    :cond_4
    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->dko:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_3

    :cond_5
    iput-object v0, v1, Lcom/xiaomi/e/c;->annotation:Ljava/lang/String;

    goto :goto_4
.end method

.method static dgF(Ljava/lang/Exception;)Lcom/xiaomi/e/c;
    .locals 5

    invoke-static {p0}, Lcom/xiaomi/e/k;->dgD(Ljava/lang/Exception;)V

    instance-of v0, p0, Lcom/xiaomi/smack/XMPPException;

    if-nez v0, :cond_3

    :cond_0
    :goto_0
    new-instance v1, Lcom/xiaomi/e/c;

    invoke-direct {v1}, Lcom/xiaomi/e/c;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-nez v2, :cond_4

    :goto_1
    invoke-static {p0}, Lcom/xiaomi/smack/l;->cyt(Ljava/lang/Throwable;)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v2, :cond_5

    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->djI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    :cond_1
    :goto_2
    iget-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v3, Lcom/xiaomi/push/thrift/ChannelStatsType;->djH:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-ne v2, v3, :cond_6

    :cond_2
    iput-object v0, v1, Lcom/xiaomi/e/c;->annotation:Ljava/lang/String;

    :goto_3
    return-object v1

    :cond_3
    move-object v0, p0

    check-cast v0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {v0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast p0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {p0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    sget-object v3, Lcom/xiaomi/push/thrift/ChannelStatsType;->djN:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2}, Lcom/xiaomi/push/thrift/ChannelStatsType;->cTZ(I)Lcom/xiaomi/push/thrift/ChannelStatsType;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iget-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v3, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkx:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_1

    instance-of v2, v2, Ljava/net/UnknownHostException;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_2

    :cond_6
    iget-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v3, Lcom/xiaomi/push/thrift/ChannelStatsType;->djI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v3, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkx:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-eq v2, v3, :cond_2

    goto :goto_3
.end method

.method static dgG(Ljava/lang/Exception;)Lcom/xiaomi/e/c;
    .locals 5

    invoke-static {p0}, Lcom/xiaomi/e/k;->dgD(Ljava/lang/Exception;)V

    instance-of v0, p0, Lcom/xiaomi/smack/XMPPException;

    if-nez v0, :cond_3

    :cond_0
    :goto_0
    new-instance v1, Lcom/xiaomi/e/c;

    invoke-direct {v1}, Lcom/xiaomi/e/c;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-nez v2, :cond_4

    :goto_1
    invoke-static {p0}, Lcom/xiaomi/smack/l;->cyt(Ljava/lang/Throwable;)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sparse-switch v2, :sswitch_data_0

    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    :cond_1
    :goto_2
    iget-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-ne v0, v2, :cond_5

    :cond_2
    iput-object v3, v1, Lcom/xiaomi/e/c;->annotation:Ljava/lang/String;

    :goto_3
    return-object v1

    :cond_3
    move-object v0, p0

    check-cast v0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {v0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast p0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {p0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :sswitch_0
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dki:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_2

    :sswitch_1
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkG:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_2

    :sswitch_2
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkh:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_2

    :sswitch_3
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_2

    :sswitch_4
    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->dky:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v2, "Terminal binding condition encountered: item-not-found"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dku:Lcom/xiaomi/push/thrift/ChannelStatsType;

    iput-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    goto :goto_2

    :cond_5
    iget-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->djJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-eq v0, v2, :cond_2

    iget-object v0, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v2, Lcom/xiaomi/push/thrift/ChannelStatsType;->dky:Lcom/xiaomi/push/thrift/ChannelStatsType;

    if-eq v0, v2, :cond_2

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_0
        0x6d -> :sswitch_1
        0x6e -> :sswitch_2
        0xc7 -> :sswitch_3
        0x1f3 -> :sswitch_4
    .end sparse-switch
.end method
