.class public Lcom/xiaomi/b/d/e;
.super Ljava/lang/Object;
.source "GeoFencingServiceWrapper.java"


# instance fields
.field private dae:Lcom/xiaomi/b/d/a;

.field private daf:Ljava/util/List;

.field private dag:Landroid/os/Handler;

.field private dah:Z

.field private dai:Ljava/util/List;

.field private daj:I

.field private final dak:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/xiaomi/b/d/e;->dah:Z

    iput v1, p0, Lcom/xiaomi/b/d/e;->daj:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/b/d/e;->daf:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/b/d/e;->dai:Ljava/util/List;

    new-instance v0, Lcom/xiaomi/b/d/d;

    invoke-direct {v0, p0}, Lcom/xiaomi/b/d/d;-><init>(Lcom/xiaomi/b/d/e;)V

    iput-object v0, p0, Lcom/xiaomi/b/d/e;->dak:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/xiaomi/b/d/e;->mContext:Landroid/content/Context;

    iput-boolean v1, p0, Lcom/xiaomi/b/d/e;->dah:Z

    invoke-virtual {p0, p1}, Lcom/xiaomi/b/d/e;->cHV(Landroid/content/Context;)V

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "GeoFencingServiceWrapper"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lcom/xiaomi/b/d/g;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/xiaomi/b/d/g;-><init>(Lcom/xiaomi/b/d/e;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/xiaomi/b/d/e;->dag:Landroid/os/Handler;

    iget-boolean v0, p0, Lcom/xiaomi/b/d/e;->dah:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dag:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method static synthetic cHU(Lcom/xiaomi/b/d/e;)I
    .locals 2

    iget v0, p0, Lcom/xiaomi/b/d/e;->daj:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/xiaomi/b/d/e;->daj:I

    return v0
.end method

.method static synthetic cHW(Lcom/xiaomi/b/d/e;Lcom/xiaomi/b/d/a;)Lcom/xiaomi/b/d/a;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    return-object p1
.end method

.method static synthetic cHX(Lcom/xiaomi/b/d/e;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/b/d/e;->dah:Z

    return v0
.end method

.method static synthetic cHY(Lcom/xiaomi/b/d/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/b/d/e;->cHZ()V

    return-void
.end method

.method private cHZ()V
    .locals 13

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/b/d/e;->daf:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->daf:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "try registerPendingFence size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "GeoFencingServiceWrapper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->daf:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->daf:Ljava/util/List;

    if-nez v0, :cond_3

    :goto_1
    return-void

    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/b/d/f;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    iget-wide v2, v0, Lcom/xiaomi/b/d/f;->dam:D

    iget-wide v4, v0, Lcom/xiaomi/b/d/f;->dao:D

    iget v6, v0, Lcom/xiaomi/b/d/f;->daq:F

    iget-wide v7, v0, Lcom/xiaomi/b/d/f;->dar:J

    iget-object v9, v0, Lcom/xiaomi/b/d/f;->mPackageName:Ljava/lang/String;

    iget-object v10, v0, Lcom/xiaomi/b/d/f;->dan:Ljava/lang/String;

    iget-object v11, v0, Lcom/xiaomi/b/d/f;->dal:Ljava/lang/String;

    invoke-interface/range {v1 .. v11}, Lcom/xiaomi/b/d/a;->cHN(DDFJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "registerPendingFence:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "GeoFencingServiceWrapper"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/b/d/e;->daf:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1
.end method

.method private cIb()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/b/d/e;->dai:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dai:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "try unregisterPendingFence size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "GeoFencingServiceWrapper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dai:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dai:Ljava/util/List;

    if-nez v0, :cond_3

    :goto_1
    return-void

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/b/d/f;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    iget-object v3, v0, Lcom/xiaomi/b/d/f;->mPackageName:Ljava/lang/String;

    iget-object v0, v0, Lcom/xiaomi/b/d/f;->dan:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/xiaomi/b/d/a;->cHQ(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unregisterPendingFence:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "GeoFencingServiceWrapper"

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dai:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1
.end method

.method static synthetic cId(Lcom/xiaomi/b/d/e;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic cIe(Lcom/xiaomi/b/d/e;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dag:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic cIf(Lcom/xiaomi/b/d/e;)I
    .locals 1

    iget v0, p0, Lcom/xiaomi/b/d/e;->daj:I

    return v0
.end method

.method static synthetic cIg(Lcom/xiaomi/b/d/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/b/d/e;->cIb()V

    return-void
.end method


# virtual methods
.method public cHV(Landroid/content/Context;)V
    .locals 3

    iget-boolean v0, p0, Lcom/xiaomi/b/d/e;->dah:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "GeoFencingService already started"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.xiaomi.metoknlp.GeoFencingService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.xiaomi.metoknlp"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/b/d/e;->dak:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "GeoFencingService started"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/b/d/e;->dah:Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SecurityException:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "GeoFencingServiceWrapper"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    :try_start_1
    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "Can\'t bind GeoFencingService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/b/d/e;->dah:Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public cIa(Landroid/content/Context;DDFJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    invoke-virtual {p0, p1}, Lcom/xiaomi/b/d/e;->cHV(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    if-nez v0, :cond_0

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "registerFenceListener service not ready, add to pending list."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/xiaomi/b/d/f;

    move-object v1, p0

    move-wide v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move-wide/from16 v7, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v11}, Lcom/xiaomi/b/d/f;-><init>(Lcom/xiaomi/b/d/e;DDFJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/b/d/e;->daf:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    move-wide v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move-wide/from16 v7, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-interface/range {v1 .. v11}, Lcom/xiaomi/b/d/a;->cHN(DDFJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "calling registerFenceListener success"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "GeoFencingService has died"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public cIc(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/xiaomi/b/d/e;->cHV(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    if-nez v0, :cond_0

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "unregisterFenceListener service not ready, add to pending list."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/xiaomi/b/d/f;

    const-string/jumbo v11, ""

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    move-object v1, p0

    move-wide v4, v2

    move-object v9, p2

    move-object v10, p3

    invoke-direct/range {v0 .. v11}, Lcom/xiaomi/b/d/f;-><init>(Lcom/xiaomi/b/d/e;DDFJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/b/d/e;->dai:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/b/d/e;->dae:Lcom/xiaomi/b/d/a;

    invoke-interface {v0, p2, p3}, Lcom/xiaomi/b/d/a;->cHQ(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "calling unregisterFenceListener success"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "GeoFencingService has died"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
