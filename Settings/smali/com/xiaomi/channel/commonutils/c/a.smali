.class public Lcom/xiaomi/channel/commonutils/c/a;
.super Ljava/lang/Object;
.source "Base64Coder.java"


# static fields
.field private static cWj:[B

.field private static cWk:[C

.field private static final cWl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/16 v6, 0x40

    const/4 v1, 0x0

    const-string/jumbo v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/channel/commonutils/c/a;->cWl:Ljava/lang/String;

    new-array v0, v6, [C

    sput-object v0, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    const/16 v0, 0x41

    move v2, v1

    :goto_0
    const/16 v3, 0x5a

    if-le v0, v3, :cond_0

    const/16 v0, 0x61

    :goto_1
    const/16 v3, 0x7a

    if-le v0, v3, :cond_1

    const/16 v0, 0x30

    :goto_2
    const/16 v3, 0x39

    if-le v0, v3, :cond_2

    sget-object v0, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    add-int/lit8 v3, v2, 0x1

    const/16 v4, 0x2b

    aput-char v4, v0, v2

    sget-object v0, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    const/16 v2, 0x2f

    aput-char v2, v0, v3

    const/16 v0, 0x80

    new-array v0, v0, [B

    sput-object v0, Lcom/xiaomi/channel/commonutils/c/a;->cWj:[B

    move v0, v1

    :goto_3
    sget-object v2, Lcom/xiaomi/channel/commonutils/c/a;->cWj:[B

    array-length v2, v2

    if-lt v0, v2, :cond_3

    :goto_4
    if-lt v1, v6, :cond_4

    return-void

    :cond_0
    sget-object v4, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    add-int/lit8 v3, v2, 0x1

    int-to-char v5, v0

    aput-char v5, v4, v2

    add-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    move v2, v3

    goto :goto_0

    :cond_1
    sget-object v4, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    add-int/lit8 v3, v2, 0x1

    int-to-char v5, v0

    aput-char v5, v4, v2

    add-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    move v2, v3

    goto :goto_1

    :cond_2
    sget-object v4, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    add-int/lit8 v3, v2, 0x1

    int-to-char v5, v0

    aput-char v5, v4, v2

    add-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    move v2, v3

    goto :goto_2

    :cond_3
    sget-object v2, Lcom/xiaomi/channel/commonutils/c/a;->cWj:[B

    const/4 v3, -0x1

    aput-byte v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    sget-object v0, Lcom/xiaomi/channel/commonutils/c/a;->cWj:[B

    sget-object v2, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    aget-char v2, v2, v1

    int-to-byte v3, v1

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cyV(Ljava/lang/String;)[B
    .locals 1

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/c/a;->cyZ([C)[B

    move-result-object v0

    return-object v0
.end method

.method public static cyW([B)[C
    .locals 2

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/xiaomi/channel/commonutils/c/a;->cyY([BII)[C

    move-result-object v0

    return-object v0
.end method

.method public static cyX([CII)[B
    .locals 13

    const/16 v2, 0x41

    const/16 v11, 0x7f

    const/4 v1, 0x0

    rem-int/lit8 v0, p2, 0x4

    if-nez v0, :cond_1

    :goto_0
    if-gtz p2, :cond_2

    :cond_0
    mul-int/lit8 v0, p2, 0x3

    div-int/lit8 v5, v0, 0x4

    new-array v6, v5, [B

    add-int v7, p1, p2

    move v4, v1

    :goto_1
    if-lt p1, v7, :cond_3

    return-object v6

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Length of Base64 encoded input string is not a multiple of 4."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    aget-char v0, p0, v0

    const/16 v3, 0x3d

    if-ne v0, v3, :cond_0

    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, p1, 0x1

    aget-char v8, p0, p1

    add-int/lit8 v0, v1, 0x1

    aget-char v9, p0, v1

    if-lt v0, v7, :cond_5

    move v1, v2

    :goto_2
    if-lt v0, v7, :cond_6

    move v3, v2

    :goto_3
    if-le v8, v11, :cond_7

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal character in Base64 encoded data."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    add-int/lit8 v1, v0, 0x1

    aget-char v0, p0, v0

    move v12, v1

    move v1, v0

    move v0, v12

    goto :goto_2

    :cond_6
    add-int/lit8 v3, v0, 0x1

    aget-char v0, p0, v0

    move v12, v3

    move v3, v0

    move v0, v12

    goto :goto_3

    :cond_7
    if-gt v9, v11, :cond_4

    if-gt v1, v11, :cond_4

    if-gt v3, v11, :cond_4

    sget-object v10, Lcom/xiaomi/channel/commonutils/c/a;->cWj:[B

    aget-byte v8, v10, v8

    sget-object v10, Lcom/xiaomi/channel/commonutils/c/a;->cWj:[B

    aget-byte v9, v10, v9

    sget-object v10, Lcom/xiaomi/channel/commonutils/c/a;->cWj:[B

    aget-byte v1, v10, v1

    sget-object v10, Lcom/xiaomi/channel/commonutils/c/a;->cWj:[B

    aget-byte v3, v10, v3

    if-gez v8, :cond_9

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal character in Base64 encoded data."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    if-ltz v9, :cond_8

    if-ltz v1, :cond_8

    if-ltz v3, :cond_8

    shl-int/lit8 v8, v8, 0x2

    ushr-int/lit8 v10, v9, 0x4

    or-int/2addr v8, v10

    and-int/lit8 v9, v9, 0xf

    shl-int/lit8 v9, v9, 0x4

    ushr-int/lit8 v10, v1, 0x2

    or-int/2addr v9, v10

    and-int/lit8 v1, v1, 0x3

    shl-int/lit8 v1, v1, 0x6

    or-int v10, v1, v3

    add-int/lit8 v1, v4, 0x1

    int-to-byte v3, v8

    int-to-byte v3, v3

    aput-byte v3, v6, v4

    if-lt v1, v5, :cond_a

    :goto_4
    if-lt v1, v5, :cond_b

    :goto_5
    move v4, v1

    move p1, v0

    goto/16 :goto_1

    :cond_a
    add-int/lit8 v3, v1, 0x1

    int-to-byte v4, v9

    int-to-byte v4, v4

    aput-byte v4, v6, v1

    move v1, v3

    goto :goto_4

    :cond_b
    add-int/lit8 v3, v1, 0x1

    int-to-byte v4, v10

    int-to-byte v4, v4

    aput-byte v4, v6, v1

    move v1, v3

    goto :goto_5
.end method

.method public static cyY([BII)[C
    .locals 12

    mul-int/lit8 v0, p2, 0x4

    add-int/lit8 v0, v0, 0x2

    div-int/lit8 v4, v0, 0x3

    add-int/lit8 v0, p2, 0x2

    div-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x4

    new-array v5, v0, [C

    add-int v6, p1, p2

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-lt p1, v6, :cond_0

    return-object v5

    :cond_0
    add-int/lit8 v0, p1, 0x1

    aget-byte v1, p0, p1

    and-int/lit16 v7, v1, 0xff

    if-lt v0, v6, :cond_1

    const/4 v1, 0x0

    :goto_1
    if-lt v0, v6, :cond_2

    const/4 v2, 0x0

    :goto_2
    ushr-int/lit8 v8, v7, 0x2

    and-int/lit8 v7, v7, 0x3

    shl-int/lit8 v7, v7, 0x4

    ushr-int/lit8 v9, v1, 0x4

    or-int/2addr v7, v9

    and-int/lit8 v1, v1, 0xf

    shl-int/lit8 v1, v1, 0x2

    ushr-int/lit8 v9, v2, 0x6

    or-int/2addr v1, v9

    and-int/lit8 v2, v2, 0x3f

    add-int/lit8 v9, v3, 0x1

    sget-object v10, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    aget-char v8, v10, v8

    int-to-char v8, v8

    aput-char v8, v5, v3

    add-int/lit8 v3, v9, 0x1

    sget-object v8, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    aget-char v7, v8, v7

    int-to-char v7, v7

    aput-char v7, v5, v9

    if-lt v3, v4, :cond_3

    const/16 v1, 0x3d

    :goto_3
    int-to-char v1, v1

    aput-char v1, v5, v3

    add-int/lit8 v3, v3, 0x1

    if-lt v3, v4, :cond_4

    const/16 v1, 0x3d

    :goto_4
    int-to-char v1, v1

    aput-char v1, v5, v3

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move p1, v0

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    move v11, v1

    move v1, v0

    move v0, v11

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    move v11, v2

    move v2, v0

    move v0, v11

    goto :goto_2

    :cond_3
    sget-object v7, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    aget-char v1, v7, v1

    goto :goto_3

    :cond_4
    sget-object v1, Lcom/xiaomi/channel/commonutils/c/a;->cWk:[C

    aget-char v1, v1, v2

    goto :goto_4
.end method

.method public static cyZ([C)[B
    .locals 2

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/xiaomi/channel/commonutils/c/a;->cyX([CII)[B

    move-result-object v0

    return-object v0
.end method
