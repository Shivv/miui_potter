.class public Lcom/xiaomi/channel/commonutils/android/e;
.super Ljava/lang/Object;
.source "MIUIUtils.java"


# static fields
.field private static cWE:Ljava/util/Map;

.field private static cWF:I

.field private static cWG:I

.field private static cWH:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    sput v0, Lcom/xiaomi/channel/commonutils/android/e;->cWH:I

    sput v1, Lcom/xiaomi/channel/commonutils/android/e;->cWF:I

    sput v1, Lcom/xiaomi/channel/commonutils/android/e;->cWG:I

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cAf()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget v2, Lcom/xiaomi/channel/commonutils/android/e;->cWG:I

    if-ltz v2, :cond_0

    :goto_0
    sget v2, Lcom/xiaomi/channel/commonutils/android/e;->cWG:I

    if-gtz v2, :cond_3

    :goto_1
    return v0

    :cond_0
    sget-object v2, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    invoke-virtual {v2}, Lcom/xiaomi/channel/commonutils/android/Region;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/e;->cAm()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/android/e;->cAi(Ljava/lang/String;)Lcom/xiaomi/channel/commonutils/android/Region;

    move-result-object v3

    invoke-virtual {v3}, Lcom/xiaomi/channel/commonutils/android/Region;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    sput v0, Lcom/xiaomi/channel/commonutils/android/e;->cWG:I

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/e;->cAg()Z

    move-result v2

    if-eqz v2, :cond_1

    sput v1, Lcom/xiaomi/channel/commonutils/android/e;->cWG:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static declared-synchronized cAg()Z
    .locals 3

    const/4 v0, 0x1

    const-class v1, Lcom/xiaomi/channel/commonutils/android/e;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/e;->cAl()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eq v2, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static cAh()V
    .locals 3

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWM:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "CN"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "FI"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "SE"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "NO"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "FO"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "EE"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "LV"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "LT"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "BY"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "MD"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "UA"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "PL"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "CZ"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "SK"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "HU"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "DE"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "AT"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "CH"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "LI"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "GB"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "IE"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "NL"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "BE"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "LU"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "FR"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "RO"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "BG"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "RS"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "MK"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "AL"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "GR"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "SI"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "HR"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "IT"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "SM"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "MT"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "ES"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "PT"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "AD"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "CY"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "DK"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    sget-object v1, Lcom/xiaomi/channel/commonutils/android/Region;->cWL:Lcom/xiaomi/channel/commonutils/android/Region;

    const-string/jumbo v2, "RU"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    return-void
.end method

.method public static cAi(Ljava/lang/String;)Lcom/xiaomi/channel/commonutils/android/Region;
    .locals 1

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/android/e;->cAk(Ljava/lang/String;)Lcom/xiaomi/channel/commonutils/android/Region;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    sget-object v0, Lcom/xiaomi/channel/commonutils/android/Region;->cWI:Lcom/xiaomi/channel/commonutils/android/Region;

    return-object v0
.end method

.method public static cAj(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v0, v2

    const-string/jumbo v2, ""

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const-string/jumbo v2, "android.os.SystemProperties"

    const-string/jumbo v3, "get"

    invoke-static {v2, v3, v0}, Lcom/xiaomi/channel/commonutils/a/b;->cyC(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1

    :catchall_0
    move-exception v0

    return-object v1
.end method

.method private static cAk(Ljava/lang/String;)Lcom/xiaomi/channel/commonutils/android/Region;
    .locals 2

    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/e;->cAh()V

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/e;->cWE:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/channel/commonutils/android/Region;

    return-object v0
.end method

.method public static declared-synchronized cAl()I
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-class v2, Lcom/xiaomi/channel/commonutils/android/e;

    monitor-enter v2

    :try_start_0
    sget v3, Lcom/xiaomi/channel/commonutils/android/e;->cWH:I

    if-eqz v3, :cond_0

    :goto_0
    sget v0, Lcom/xiaomi/channel/commonutils/android/e;->cWH:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return v0

    :cond_0
    :try_start_1
    const-string/jumbo v3, "ro.miui.ui.version.code"

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/android/e;->cAj(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_1
    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    const/4 v1, 0x2

    :cond_2
    sput v1, Lcom/xiaomi/channel/commonutils/android/e;->cWH:I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "isMIUI\'s value is: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/xiaomi/channel/commonutils/android/e;->cWH:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_3
    :try_start_3
    const-string/jumbo v3, "ro.miui.ui.version.name"

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/android/e;->cAj(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_4
    const-string/jumbo v1, "get isMIUI failed"

    invoke-static {v1, v0}, Lcom/xiaomi/channel/commonutils/e/a;->czv(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    sput v0, Lcom/xiaomi/channel/commonutils/android/e;->cWH:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public static cAm()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "ro.miui.region"

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Lcom/xiaomi/channel/commonutils/android/h;->cAv(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    return-object v0

    :cond_0
    const-string/jumbo v0, "ro.product.locale.region"

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Lcom/xiaomi/channel/commonutils/android/h;->cAv(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "persist.sys.country"

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Lcom/xiaomi/channel/commonutils/android/h;->cAv(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
