.class public Lcom/xiaomi/channel/commonutils/h/n;
.super Ljava/lang/Object;
.source "SerializedAsyncTaskProcessor.java"


# instance fields
.field private volatile cXt:Z

.field private final cXu:Z

.field private cXv:Lcom/xiaomi/channel/commonutils/h/f;

.field private volatile cXw:Lcom/xiaomi/channel/commonutils/h/b;

.field private cXx:I

.field private cXy:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/xiaomi/channel/commonutils/h/n;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/channel/commonutils/h/n;-><init>(ZI)V

    return-void
.end method

.method public constructor <init>(ZI)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXy:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXt:Z

    iput v1, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXx:I

    new-instance v0, Lcom/xiaomi/channel/commonutils/h/g;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/channel/commonutils/h/g;-><init>(Lcom/xiaomi/channel/commonutils/h/n;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXy:Landroid/os/Handler;

    iput-boolean p1, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXu:Z

    iput p2, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXx:I

    return-void
.end method

.method private declared-synchronized cBr()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXv:Lcom/xiaomi/channel/commonutils/h/f;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXt:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic cBs(Lcom/xiaomi/channel/commonutils/h/n;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXt:Z

    return v0
.end method

.method static synthetic cBu(Lcom/xiaomi/channel/commonutils/h/n;)I
    .locals 1

    iget v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXx:I

    return v0
.end method

.method static synthetic cBw(Lcom/xiaomi/channel/commonutils/h/n;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/channel/commonutils/h/n;->cBr()V

    return-void
.end method

.method static synthetic cBx(Lcom/xiaomi/channel/commonutils/h/n;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXy:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic cBy(Lcom/xiaomi/channel/commonutils/h/n;Lcom/xiaomi/channel/commonutils/h/b;)Lcom/xiaomi/channel/commonutils/h/b;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXw:Lcom/xiaomi/channel/commonutils/h/b;

    return-object p1
.end method

.method static synthetic cBz(Lcom/xiaomi/channel/commonutils/h/n;)Lcom/xiaomi/channel/commonutils/h/b;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXw:Lcom/xiaomi/channel/commonutils/h/b;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized cBt(Lcom/xiaomi/channel/commonutils/h/b;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXv:Lcom/xiaomi/channel/commonutils/h/f;

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXv:Lcom/xiaomi/channel/commonutils/h/f;

    invoke-virtual {v0, p1}, Lcom/xiaomi/channel/commonutils/h/f;->cAW(Lcom/xiaomi/channel/commonutils/h/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/xiaomi/channel/commonutils/h/f;

    invoke-direct {v0, p0}, Lcom/xiaomi/channel/commonutils/h/f;-><init>(Lcom/xiaomi/channel/commonutils/h/n;)V

    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXv:Lcom/xiaomi/channel/commonutils/h/f;

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXv:Lcom/xiaomi/channel/commonutils/h/f;

    iget-boolean v1, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXu:Z

    invoke-virtual {v0, v1}, Lcom/xiaomi/channel/commonutils/h/f;->setDaemon(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXt:Z

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXv:Lcom/xiaomi/channel/commonutils/h/f;

    invoke-virtual {v0}, Lcom/xiaomi/channel/commonutils/h/f;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cBv(Lcom/xiaomi/channel/commonutils/h/b;J)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/n;->cXy:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/channel/commonutils/h/m;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/channel/commonutils/h/m;-><init>(Lcom/xiaomi/channel/commonutils/h/n;Lcom/xiaomi/channel/commonutils/h/b;)V

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
