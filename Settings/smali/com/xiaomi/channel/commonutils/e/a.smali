.class public abstract Lcom/xiaomi/channel/commonutils/e/a;
.super Ljava/lang/Object;
.source "MyLog.java"


# static fields
.field private static cWp:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final cWq:Ljava/util/HashMap;

.field private static cWr:I

.field private static cWs:Lcom/xiaomi/channel/commonutils/e/c;

.field private static final cWt:Ljava/util/HashMap;

.field private static final cWu:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x2

    sput v0, Lcom/xiaomi/channel/commonutils/e/a;->cWr:I

    new-instance v0, Lcom/xiaomi/channel/commonutils/e/b;

    invoke-direct {v0}, Lcom/xiaomi/channel/commonutils/e/b;-><init>()V

    sput-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWs:Lcom/xiaomi/channel/commonutils/e/c;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWt:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWq:Ljava/util/HashMap;

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWu:Ljava/lang/Integer;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWp:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static czA(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    sget v0, Lcom/xiaomi/channel/commonutils/e/a;->cWr:I

    if-ge p0, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWs:Lcom/xiaomi/channel/commonutils/e/c;

    invoke-interface {v0, p1, p2}, Lcom/xiaomi/channel/commonutils/e/c;->czG(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static czB(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[Thread:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "] "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/channel/commonutils/e/a;->czy(ILjava/lang/String;)V

    return-void
.end method

.method public static czC(ILjava/lang/Throwable;)V
    .locals 2

    sget v0, Lcom/xiaomi/channel/commonutils/e/a;->cWr:I

    if-ge p0, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWs:Lcom/xiaomi/channel/commonutils/e/c;

    const-string/jumbo v1, ""

    invoke-interface {v0, v1, p1}, Lcom/xiaomi/channel/commonutils/e/c;->czG(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static czD(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/xiaomi/channel/commonutils/e/a;->czy(ILjava/lang/String;)V

    return-void
.end method

.method public static czE(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[Thread:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "] "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/channel/commonutils/e/a;->czy(ILjava/lang/String;)V

    return-void
.end method

.method public static czu(Ljava/lang/Throwable;)V
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0, p0}, Lcom/xiaomi/channel/commonutils/e/a;->czC(ILjava/lang/Throwable;)V

    return-void
.end method

.method public static czv(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0, p0, p1}, Lcom/xiaomi/channel/commonutils/e/a;->czA(ILjava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static czw(Ljava/lang/Integer;)V
    .locals 6

    sget v0, Lcom/xiaomi/channel/commonutils/e/a;->cWr:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWt:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWt:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWq:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    sget-object v1, Lcom/xiaomi/channel/commonutils/e/a;->cWs:Lcom/xiaomi/channel/commonutils/e/c;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " ends in "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " ms"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/xiaomi/channel/commonutils/e/c;->czF(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static czx(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4

    sget v0, Lcom/xiaomi/channel/commonutils/e/a;->cWr:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    sget-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWu:Ljava/lang/Integer;

    return-object v0

    :cond_0
    sget-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWp:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/channel/commonutils/e/a;->cWt:Ljava/util/HashMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/channel/commonutils/e/a;->cWq:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/channel/commonutils/e/a;->cWs:Lcom/xiaomi/channel/commonutils/e/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " starts"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/xiaomi/channel/commonutils/e/c;->czF(Ljava/lang/String;)V

    return-object v0
.end method

.method public static czy(ILjava/lang/String;)V
    .locals 1

    sget v0, Lcom/xiaomi/channel/commonutils/e/a;->cWr:I

    if-ge p0, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/channel/commonutils/e/a;->cWs:Lcom/xiaomi/channel/commonutils/e/c;

    invoke-interface {v0, p1}, Lcom/xiaomi/channel/commonutils/e/c;->czF(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static czz()I
    .locals 1

    sget v0, Lcom/xiaomi/channel/commonutils/e/a;->cWr:I

    return v0
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0, p0}, Lcom/xiaomi/channel/commonutils/e/a;->czy(ILjava/lang/String;)V

    return-void
.end method
