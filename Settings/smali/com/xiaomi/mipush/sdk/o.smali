.class public Lcom/xiaomi/mipush/sdk/o;
.super Ljava/lang/Object;
.source "AppInfoHolder.java"


# instance fields
.field public cYA:Z

.field public cYB:I

.field public cYC:Ljava/lang/String;

.field public cYD:Ljava/lang/String;

.field public cYE:Ljava/lang/String;

.field public cYw:Ljava/lang/String;

.field public cYx:Ljava/lang/String;

.field public cYy:Z

.field public cYz:Ljava/lang/String;

.field public deviceId:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field public regSecret:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/mipush/sdk/o;->cYA:Z

    iput v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYB:I

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    return-void
.end method

.method private cEf()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/channel/commonutils/android/b;->czK(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static cEi(Lcom/xiaomi/mipush/sdk/o;)Ljava/lang/String;
    .locals 3

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYE:Ljava/lang/String;

    const-string/jumbo v2, "appId"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYz:Ljava/lang/String;

    const-string/jumbo v2, "appToken"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYw:Ljava/lang/String;

    const-string/jumbo v2, "regId"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->regSecret:Ljava/lang/String;

    const-string/jumbo v2, "regSec"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    const-string/jumbo v2, "devId"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYC:Ljava/lang/String;

    const-string/jumbo v2, "vName"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-boolean v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    const-string/jumbo v2, "valid"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    iget-boolean v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYA:Z

    const-string/jumbo v2, "paused"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    iget v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYB:I

    const-string/jumbo v2, "envType"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYx:Ljava/lang/String;

    const-string/jumbo v2, "regResource"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public cEe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/o;->cYE:Ljava/lang/String;

    iput-object p2, p0, Lcom/xiaomi/mipush/sdk/o;->cYz:Ljava/lang/String;

    iput-object p3, p0, Lcom/xiaomi/mipush/sdk/o;->cYx:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYE:Ljava/lang/String;

    const-string/jumbo v2, "appId"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "appToken"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "regResource"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public cEg(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYE:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYz:Ljava/lang/String;

    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYw:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->regSecret:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/android/c;->czU(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cEh()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->cYE:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYz:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/mipush/sdk/o;->cEg(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public cEj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/o;->cYw:Ljava/lang/String;

    iput-object p2, p0, Lcom/xiaomi/mipush/sdk/o;->regSecret:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/c;->czU(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/o;->cEf()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->cYC:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    iput-object p3, p0, Lcom/xiaomi/mipush/sdk/o;->cYD:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "regId"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "regSec"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    const-string/jumbo v2, "devId"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/o;->cEf()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "vName"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "valid"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "appRegion"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public cEk(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/mipush/sdk/o;->cYA:Z

    return-void
.end method

.method public cEl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/o;->cYw:Ljava/lang/String;

    iput-object p2, p0, Lcom/xiaomi/mipush/sdk/o;->regSecret:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/c;->czU(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/o;->cEf()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->cYC:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    return-void
.end method

.method public cEm()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-boolean v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    const-string/jumbo v2, "valid"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public clear()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/o;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYE:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYz:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYw:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->regSecret:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYC:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    iput-boolean v2, p0, Lcom/xiaomi/mipush/sdk/o;->cYA:Z

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/o;->cYD:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/xiaomi/mipush/sdk/o;->cYB:I

    return-void
.end method
