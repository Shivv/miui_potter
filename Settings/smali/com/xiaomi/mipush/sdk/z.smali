.class public Lcom/xiaomi/mipush/sdk/z;
.super Ljava/lang/Object;
.source "PushServiceClient.java"


# static fields
.field private static cZe:Lcom/xiaomi/mipush/sdk/z;

.field private static cZf:Z

.field private static final cZh:Ljava/util/ArrayList;


# instance fields
.field private cYX:Ljava/lang/String;

.field private cYY:Ljava/util/List;

.field private cYZ:Landroid/os/Handler;

.field private cZa:Z

.field private cZb:Z

.field private cZc:Landroid/content/Intent;

.field private cZd:Landroid/os/Messenger;

.field private cZg:Ljava/lang/Integer;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/xiaomi/mipush/sdk/z;->cZf:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/mipush/sdk/z;->cZh:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/xiaomi/mipush/sdk/z;->cZa:Z

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cYZ:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cYY:Ljava/util/List;

    iput-boolean v2, p0, Lcom/xiaomi/mipush/sdk/z;->cZb:Z

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cZc:Landroid/content/Intent;

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cZg:Ljava/lang/Integer;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cYX:Ljava/lang/String;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFa()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZa:Z

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFQ()Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/mipush/sdk/z;->cZf:Z

    new-instance v0, Lcom/xiaomi/mipush/sdk/S;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/mipush/sdk/S;-><init>(Lcom/xiaomi/mipush/sdk/z;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cYZ:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFp()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFE(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private cEW()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "miui"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    return v1

    :cond_1
    const-string/jumbo v3, "xiaomi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private declared-synchronized cEX()I
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "mipush_extra"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "service_boot_mode"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic cEZ(Lcom/xiaomi/mipush/sdk/z;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/mipush/sdk/z;->cZb:Z

    return p1
.end method

.method private cFA(Landroid/content/Intent;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/push/service/aU;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aU;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ConfigKey;->dpj:Lcom/xiaomi/xmpush/thrift/ConfigKey;

    invoke-virtual {v2}, Lcom/xiaomi/xmpush/thrift/ConfigKey;->getValue()I

    move-result v2

    sget-object v3, Lcom/xiaomi/xmpush/thrift/BootModeType;->drU:Lcom/xiaomi/xmpush/thrift/BootModeType;

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/BootModeType;->getValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/push/service/aU;->cSW(II)I

    move-result v1

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cEX()I

    move-result v2

    sget-object v3, Lcom/xiaomi/xmpush/thrift/BootModeType;->drV:Lcom/xiaomi/xmpush/thrift/BootModeType;

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/BootModeType;->getValue()I

    move-result v3

    if-eq v1, v3, :cond_1

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/BootModeType;->drU:Lcom/xiaomi/xmpush/thrift/BootModeType;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/BootModeType;->getValue()I

    move-result v1

    :goto_1
    if-ne v1, v2, :cond_3

    :goto_2
    if-nez v0, :cond_4

    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/z;->cFE(Landroid/content/Intent;)V

    :goto_3
    return-void

    :cond_1
    sget-boolean v1, Lcom/xiaomi/mipush/sdk/z;->cZf:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/xiaomi/xmpush/thrift/BootModeType;->drV:Lcom/xiaomi/xmpush/thrift/BootModeType;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/BootModeType;->getValue()I

    move-result v1

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v1}, Lcom/xiaomi/mipush/sdk/z;->cFm(I)Z

    goto :goto_2

    :cond_4
    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/z;->cFF(Landroid/content/Intent;)V

    goto :goto_3
.end method

.method private cFD()Landroid/content/Intent;
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/mipush/sdk/z;->cFh()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "pushChannel app start  own channel"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFL()Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "pushChannel app start miui china channel"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFz()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private cFE(Landroid/content/Intent;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private declared-synchronized cFF(Landroid/content/Intent;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZb:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZd:Landroid/os/Messenger;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/z;->cFi(Landroid/content/Intent;)Landroid/os/Message;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cZd:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/z;->cFi(Landroid/content/Intent;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cYY:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x32

    if-ge v1, v2, :cond_1

    :goto_1
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cYY:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_3
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cYY:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/xiaomi/mipush/sdk/g;

    invoke-direct {v1, p0}, Lcom/xiaomi/mipush/sdk/g;-><init>(Lcom/xiaomi/mipush/sdk/z;)V

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZb:Z

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cYY:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/z;->cFi(Landroid/content/Intent;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cYY:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method static synthetic cFG(Lcom/xiaomi/mipush/sdk/z;Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/xiaomi/mipush/sdk/z;->cFP(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V

    return-void
.end method

.method private cFH()Landroid/content/Intent;
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/mipush/sdk/z;->cFh()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFL()Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.xmsf"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFz()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic cFJ(Lcom/xiaomi/mipush/sdk/z;)Landroid/os/Messenger;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZd:Landroid/os/Messenger;

    return-object v0
.end method

.method private cFL()Landroid/content/Intent;
    .locals 5

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFo()V

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "com.xiaomi.push.service.XMPushService"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string/jumbo v2, "mipush_app_package"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic cFO(Lcom/xiaomi/mipush/sdk/z;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/z;->cZg:Ljava/lang/Integer;

    return-object p1
.end method

.method private cFP(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDJ()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v4, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-direct {v4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>()V

    invoke-virtual {v4, v7}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daD(Z)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v4, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dah(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    if-nez p3, :cond_6

    move-object v1, v2

    :goto_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/O;->cZQ:[I

    invoke-virtual {p2}, Lcom/xiaomi/mipush/sdk/RetryType;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dau(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {p0, v4, v0, v3, v2}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    if-nez p3, :cond_9

    :cond_3
    :goto_2
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x13

    iput v1, v0, Landroid/os/Message;->what:I

    invoke-virtual {p2}, Lcom/xiaomi/mipush/sdk/RetryType;->ordinal()I

    move-result v1

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cYZ:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void

    :cond_4
    invoke-static {}, Lcom/xiaomi/mipush/sdk/C;->cGf()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dah(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    if-nez p3, :cond_5

    move-object v0, v2

    :goto_3
    const-class v1, Lcom/xiaomi/mipush/sdk/m;

    const-class v5, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v5

    :try_start_0
    iget-object v5, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/xiaomi/mipush/sdk/m;->cDX(Ljava/lang/String;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v0

    goto :goto_0

    :cond_5
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-direct {v0, p1, v7}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>(Ljava/lang/String;Z)V

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-direct {v1, p1, v7}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzb:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzb:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    if-nez p4, :cond_7

    :goto_4
    const-string/jumbo v0, "com.xiaomi.mipush.DISABLE_PUSH_MESSAGE"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_7
    invoke-virtual {v4, p4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dar(Ljava/util/Map;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-virtual {v1, p4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dar(Ljava/util/Map;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    goto :goto_4

    :pswitch_1
    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzz:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzz:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    if-nez p4, :cond_8

    :goto_5
    const-string/jumbo v0, "com.xiaomi.mipush.ENABLE_PUSH_MESSAGE"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v4, p4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dar(Ljava/util/Map;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-virtual {v1, p4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dar(Ljava/util/Map;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    goto :goto_5

    :pswitch_2
    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzd:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    if-eqz p4, :cond_2

    invoke-virtual {v4, p4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dar(Ljava/util/Map;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dau(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v5

    invoke-virtual {v5}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/R;->cHx(Landroid/content/Context;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    const-string/jumbo v1, "mipush_payload"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string/jumbo v0, "com.xiaomi.mipush.MESSAGE_CACHE"

    invoke-virtual {v6, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mipush_app_id"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDV()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mipush_app_token"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v6}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private cFQ()Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/mipush/sdk/z;->cFh()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v4

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.xmsf"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x6c

    if-ge v0, v1, :cond_1

    return v3

    :cond_1
    return v4

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private cFa()Z
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    const-string/jumbo v1, "com.xiaomi.xmsf"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x69

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_0
    return v3

    :cond_1
    return v3

    :catch_0
    move-exception v0

    return v3
.end method

.method static synthetic cFb(Lcom/xiaomi/mipush/sdk/z;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZg:Ljava/lang/Integer;

    return-object v0
.end method

.method private cFi(Landroid/content/Intent;)Landroid/os/Message;
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x11

    iput v1, v0, Landroid/os/Message;->what:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic cFj(Lcom/xiaomi/mipush/sdk/z;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cYY:Ljava/util/List;

    return-object v0
.end method

.method static synthetic cFl(Lcom/xiaomi/mipush/sdk/z;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/z;->cZd:Landroid/os/Messenger;

    return-object p1
.end method

.method private cFo()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.xiaomi.push.service.XMPushService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private cFp()Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.xmsf"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "pushChannel xmsf create own channel"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFL()Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFD()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private cFr()Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.xmsf"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x6a

    if-ge v0, v1, :cond_0

    :goto_0
    const-string/jumbo v0, "com.xiaomi.xmsf.push.service.XMPushService"

    return-object v0

    :cond_0
    :try_start_1
    const-string/jumbo v0, "com.xiaomi.push.service.XMPushService"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private cFs()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.xiaomi.push.service.XMPushService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private declared-synchronized cFy(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "mipush_extra"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "service_boot_mode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private cFz()Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.xiaomi.xmsf"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFr()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.xiaomi.xmsf"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "mipush_app_package"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFs()V

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;
    .locals 2

    const-class v1, Lcom/xiaomi/mipush/sdk/z;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/z;->cZe:Lcom/xiaomi/mipush/sdk/z;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/z;->cZe:Lcom/xiaomi/mipush/sdk/z;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/xiaomi/mipush/sdk/z;

    invoke-direct {v0, p0}, Lcom/xiaomi/mipush/sdk/z;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/z;->cZe:Lcom/xiaomi/mipush/sdk/z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final cEY(Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "com.xiaomi.mipush.SEND_TINYDATA"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "mipush_payload"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFE(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string/jumbo v0, "send TinyData failed, because tinyDataBytes is null."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void
.end method

.method public cFB()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZc:Landroid/content/Intent;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZc:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    iput-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cZc:Landroid/content/Intent;

    goto :goto_0
.end method

.method public final cFC(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;Lcom/xiaomi/mipush/sdk/AssemblePush;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    const-string/jumbo v1, "syncing"

    invoke-virtual {v0, p2, v1}, Lcom/xiaomi/mipush/sdk/m;->cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/xiaomi/mipush/sdk/h;->cDk(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Ljava/util/HashMap;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/xiaomi/mipush/sdk/z;->cFP(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V

    return-void
.end method

.method public cFI()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/mipush/sdk/z;->cFh()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    return v1

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cEW()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->cZg:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->cZg:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_4

    :goto_1
    return v0

    :cond_3
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/push/service/R;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/R;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/push/service/R;->cOt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->cZg:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->cZg:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Lcom/xiaomi/mipush/sdk/K;

    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v2, p0, v3}, Lcom/xiaomi/mipush/sdk/K;-><init>(Lcom/xiaomi/mipush/sdk/z;Landroid/os/Handler;)V

    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/xiaomi/push/service/R;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/R;

    move-result-object v4

    invoke-virtual {v4}, Lcom/xiaomi/push/service/R;->cOu()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public cFK()V
    .locals 9

    sget-object v7, Lcom/xiaomi/mipush/sdk/z;->cZh:Ljava/util/ArrayList;

    monitor-enter v7

    :try_start_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/z;->cZh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/xiaomi/mipush/sdk/z;->cZh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v7

    return-void

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/T;

    iget-object v1, v0, Lcom/xiaomi/mipush/sdk/T;->cZZ:Lorg/apache/thrift/TBase;

    iget-object v2, v0, Lcom/xiaomi/mipush/sdk/T;->daa:Lcom/xiaomi/xmpush/thrift/ActionType;

    iget-boolean v3, v0, Lcom/xiaomi/mipush/sdk/T;->dab:Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/xiaomi/mipush/sdk/z;->cFd(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZZLcom/xiaomi/xmpush/thrift/PushMetaInfo;Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final cFM(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZZLcom/xiaomi/xmpush/thrift/PushMetaInfo;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDL()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p7

    move-object v5, p8

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/R;->cHx(Landroid/content/Context;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    if-nez p5, :cond_2

    :goto_0
    invoke-static {v0}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "com.xiaomi.mipush.SEND_MESSAGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "mipush_payload"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string/jumbo v0, "com.xiaomi.mipush.MESSAGE_CACHE"

    invoke-virtual {v1, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-direct {p0, v1}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    :goto_1
    return-void

    :cond_0
    if-nez p4, :cond_1

    const-string/jumbo v0, "drop the message before initialization."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/mipush/sdk/z;->cFx(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;Z)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0, p5}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->deb(Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "send message fail, because msgBytes is null."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void
.end method

.method public final cFN()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.DISABLE_PUSH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    return-void
.end method

.method public final cFc(Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration;Z)V
    .locals 4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZc:Landroid/content/Intent;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/mipush/sdk/l;->cYr:Ljava/lang/String;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ActionType;->dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-static {v1, p1, v2}, Lcom/xiaomi/mipush/sdk/R;->cHz(Landroid/content/Context;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "com.xiaomi.mipush.REGISTER_APP"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v2

    const-string/jumbo v3, "mipush_app_id"

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "mipush_payload"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->cYX:Ljava/lang/String;

    const-string/jumbo v2, "mipush_session"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "mipush_env_chanage"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    const-string/jumbo v2, "mipush_env_type"

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/l;->cDU()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->cZc:Landroid/content/Intent;

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "register fail, because msgBytes is null."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/mipush/sdk/z;->cFI()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final cFd(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZZLcom/xiaomi/xmpush/thrift/PushMetaInfo;Z)V
    .locals 9

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v8}, Lcom/xiaomi/mipush/sdk/z;->cFM(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZZLcom/xiaomi/xmpush/thrift/PushMetaInfo;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public cFe(I)V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.CLEAR_NOTIFICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/xiaomi/push/service/b;->ddN:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    return-void
.end method

.method public final cFf(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)V
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {p2, v1}, Lcom/xiaomi/xmpush/thrift/ActionType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cFg()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFE(Landroid/content/Intent;)V

    return-void
.end method

.method public cFh()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/xiaomi/mipush/sdk/z;->cZa:Z

    if-nez v2, :cond_1

    :goto_0
    move v0, v1

    :cond_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDU()I

    move-result v2

    if-eq v0, v2, :cond_0

    goto :goto_0
.end method

.method public final cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V
    .locals 7

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/xiaomi/mipush/sdk/z;->cFd(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZZLcom/xiaomi/xmpush/thrift/PushMetaInfo;Z)V

    return-void
.end method

.method public cFm(I)Z
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDJ()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/z;->cFy(I)V

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>()V

    invoke-static {}, Lcom/xiaomi/mipush/sdk/C;->cGf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dah(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dau(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyZ:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "boot_mode"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v4, v3}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    return v4
.end method

.method public final cFn(Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistration;)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAO:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-static {v0, p1, v1}, Lcom/xiaomi/mipush/sdk/R;->cHz(Landroid/content/Context;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "com.xiaomi.mipush.UNREGISTER_APP"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v2

    const-string/jumbo v3, "mipush_app_id"

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "mipush_payload"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-direct {p0, v1}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string/jumbo v0, "unregister fail, because msgBytes is null."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void
.end method

.method public final cFq(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/mipush/sdk/z;->cFt(ZLjava/lang/String;)V

    return-void
.end method

.method public final cFt(ZLjava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    const-string/jumbo v2, "syncing"

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/mipush/sdk/m;->cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/mipush/sdk/m;->cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V

    sget-object v0, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/xiaomi/mipush/sdk/z;->cFP(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    const-string/jumbo v2, "syncing"

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/mipush/sdk/m;->cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/mipush/sdk/m;->cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V

    sget-object v0, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-direct {p0, p2, v0, v3, v4}, Lcom/xiaomi/mipush/sdk/z;->cFP(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V

    goto :goto_0
.end method

.method public cFu(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.CLEAR_NOTIFICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/xiaomi/push/service/b;->des:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/xiaomi/push/service/b;->deg:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    return-void
.end method

.method public cFv()V
    .locals 3

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/z;->cFH()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.SET_NOTIFICATION_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/xiaomi/push/service/b;->ddL:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/z;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/c/c;->czl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/z;->cFA(Landroid/content/Intent;)V

    return-void
.end method

.method public cFx(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;Z)V
    .locals 3

    new-instance v0, Lcom/xiaomi/mipush/sdk/T;

    invoke-direct {v0}, Lcom/xiaomi/mipush/sdk/T;-><init>()V

    iput-object p1, v0, Lcom/xiaomi/mipush/sdk/T;->cZZ:Lorg/apache/thrift/TBase;

    iput-object p2, v0, Lcom/xiaomi/mipush/sdk/T;->daa:Lcom/xiaomi/xmpush/thrift/ActionType;

    iput-boolean p3, v0, Lcom/xiaomi/mipush/sdk/T;->dab:Z

    sget-object v1, Lcom/xiaomi/mipush/sdk/z;->cZh:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/xiaomi/mipush/sdk/z;->cZh:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/xiaomi/mipush/sdk/z;->cZh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0xa

    if-gt v0, v2, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/z;->cZh:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
