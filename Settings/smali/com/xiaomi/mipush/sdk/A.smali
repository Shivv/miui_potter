.class public Lcom/xiaomi/mipush/sdk/A;
.super Ljava/lang/Object;
.source "AssemblePushUtils.java"


# static fields
.field private static cZi:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/xiaomi/mipush/sdk/A;->cZi:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cFR()Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const-string/jumbo v1, "ro.build.hw_emui_api_level"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, ""

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "android.os.SystemProperties"

    const-string/jumbo v2, "get"

    invoke-static {v1, v2, v0}, Lcom/xiaomi/channel/commonutils/a/b;->cyC(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    return v4

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static cFS(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/PhoneBrand;
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.huawei.hwid"

    const-string/jumbo v3, "com.huawei.hms.core.service.HMSCoreService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZn:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    return-object v0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/xiaomi/mipush/sdk/A;->cFR()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZo:Lcom/xiaomi/mipush/sdk/PhoneBrand;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZn:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    return-object v0
.end method

.method public static cFT(Landroid/content/Context;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/Object;

    const-string/jumbo v3, "com.google.android.gms.common.GoogleApiAvailability"

    const-string/jumbo v4, "getInstance"

    invoke-static {v3, v4, v0}, Lcom/xiaomi/channel/commonutils/a/b;->cyC(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/Object;

    aput-object p0, v3, v1

    const-string/jumbo v4, "isGooglePlayServicesAvailable"

    invoke-static {v0, v4, v3}, Lcom/xiaomi/channel/commonutils/a/b;->cyz(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const-string/jumbo v0, "com.google.android.gms.common.ConnectionResult"

    const-string/jumbo v4, "SUCCESS"

    invoke-static {v0, v4}, Lcom/xiaomi/channel/commonutils/a/b;->cyE(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, "google service is not avaliable"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    sput v1, Lcom/xiaomi/mipush/sdk/A;->cZi:I

    return v1

    :cond_1
    instance-of v4, v0, Ljava/lang/Integer;

    if-eqz v4, :cond_0

    const-class v4, Ljava/lang/Integer;

    invoke-virtual {v4, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v3, :cond_2

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "is google service can be used"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v0, Lcom/xiaomi/mipush/sdk/A;->cZi:I

    if-gtz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    sget v0, Lcom/xiaomi/mipush/sdk/A;->cZi:I

    if-gtz v0, :cond_6

    :goto_2
    return v1

    :cond_2
    instance-of v0, v3, Ljava/lang/Integer;

    if-nez v0, :cond_3

    sput v1, Lcom/xiaomi/mipush/sdk/A;->cZi:I

    const-string/jumbo v0, "google service is not avaliable"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v4, :cond_4

    move v0, v1

    :goto_3
    sput v0, Lcom/xiaomi/mipush/sdk/A;->cZi:I

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public static cFU(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v0

    const-string/jumbo v2, "com.xiaomi.assemble.control.COSPushManager"

    const-string/jumbo v3, "isSupportPush"

    invoke-static {v2, v3, v1}, Lcom/xiaomi/channel/commonutils/a/b;->cyC(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "color os push  is avaliable ? :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return v0

    :cond_1
    instance-of v2, v1, Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
