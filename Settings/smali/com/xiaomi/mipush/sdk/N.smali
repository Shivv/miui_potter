.class public Lcom/xiaomi/mipush/sdk/N;
.super Ljava/lang/Object;
.source "COSPushHelper.java"


# static fields
.field private static volatile cZK:Z

.field private static cZL:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput-boolean v0, Lcom/xiaomi/mipush/sdk/N;->cZK:Z

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/xiaomi/mipush/sdk/N;->cZL:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cHn(Landroid/content/Context;)V
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {}, Lcom/xiaomi/mipush/sdk/N;->cHp()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-wide v2, Lcom/xiaomi/mipush/sdk/N;->cZL:J

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-gtz v2, :cond_3

    move v2, v0

    :goto_1
    if-nez v2, :cond_2

    sget-wide v2, Lcom/xiaomi/mipush/sdk/N;->cZL:J

    const-wide/32 v6, 0x493e0

    add-long/2addr v2, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    :goto_2
    if-nez v0, :cond_0

    :cond_2
    sput-wide v4, Lcom/xiaomi/mipush/sdk/N;->cZL:J

    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/N;->cHo(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public static cHo(Landroid/content/Context;)V
    .locals 2

    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/e;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/e;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/e;->cCG(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "register cos when network change!"

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/xiaomi/mipush/sdk/t;->cCD()V

    goto :goto_0
.end method

.method public static cHp()Z
    .locals 1

    sget-boolean v0, Lcom/xiaomi/mipush/sdk/N;->cZK:Z

    return v0
.end method
