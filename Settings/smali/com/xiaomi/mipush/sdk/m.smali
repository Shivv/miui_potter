.class public Lcom/xiaomi/mipush/sdk/m;
.super Ljava/lang/Object;
.source "OperatePushHelper.java"


# static fields
.field private static volatile cYt:Lcom/xiaomi/mipush/sdk/m;


# instance fields
.field private cYu:Landroid/content/Context;

.field private cYv:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/mipush/sdk/m;->cYt:Lcom/xiaomi/mipush/sdk/m;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYu:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYu:Landroid/content/Context;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/m;->cYu:Landroid/content/Context;

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;
    .locals 2

    sget-object v0, Lcom/xiaomi/mipush/sdk/m;->cYt:Lcom/xiaomi/mipush/sdk/m;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/m;->cYt:Lcom/xiaomi/mipush/sdk/m;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/mipush/sdk/m;

    const-class v0, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/m;->cYt:Lcom/xiaomi/mipush/sdk/m;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/mipush/sdk/m;

    invoke-direct {v0, p0}, Lcom/xiaomi/mipush/sdk/m;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/m;->cYt:Lcom/xiaomi/mipush/sdk/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public cDW(Ljava/lang/String;)Z
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/xiaomi/mipush/sdk/P;

    invoke-direct {v0}, Lcom/xiaomi/mipush/sdk/P;-><init>()V

    iput-object p1, v0, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v1

    return v3

    :cond_0
    monitor-exit v1

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cDX(Ljava/lang/String;)V
    .locals 3

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/xiaomi/mipush/sdk/P;

    invoke-direct {v0}, Lcom/xiaomi/mipush/sdk/P;-><init>()V

    const/4 v2, 0x0

    iput v2, v0, Lcom/xiaomi/mipush/sdk/P;->cZR:I

    iput-object p1, v0, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cDY(Ljava/lang/String;)I
    .locals 6

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    new-instance v2, Lcom/xiaomi/mipush/sdk/P;

    invoke-direct {v2}, Lcom/xiaomi/mipush/sdk/P;-><init>()V

    iput-object p1, v2, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit v1

    return v5

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/P;

    invoke-virtual {v0, v2}, Lcom/xiaomi/mipush/sdk/P;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget v0, v0, Lcom/xiaomi/mipush/sdk/P;->cZR:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cDZ(Ljava/lang/String;)V
    .locals 5

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    new-instance v1, Lcom/xiaomi/mipush/sdk/P;

    invoke-direct {v1}, Lcom/xiaomi/mipush/sdk/P;-><init>()V

    iput-object p1, v1, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    iget v1, v0, Lcom/xiaomi/mipush/sdk/P;->cZR:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/xiaomi/mipush/sdk/P;->cZR:I

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v2

    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/P;

    invoke-virtual {v1, v0}, Lcom/xiaomi/mipush/sdk/P;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYu:Landroid/content/Context;

    const-string/jumbo v1, "mipush_extra"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/mipush/sdk/RetryType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/m;->cYu:Landroid/content/Context;

    const-string/jumbo v1, "mipush_extra"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/mipush/sdk/RetryType;->name()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cEc(Ljava/lang/String;)V
    .locals 3

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/xiaomi/mipush/sdk/P;

    invoke-direct {v0}, Lcom/xiaomi/mipush/sdk/P;-><init>()V

    iput-object p1, v0, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/m;->cYv:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
