.class public final enum Lcom/xiaomi/mipush/sdk/PhoneBrand;
.super Ljava/lang/Enum;
.source "PhoneBrand.java"


# static fields
.field public static final enum cZk:Lcom/xiaomi/mipush/sdk/PhoneBrand;

.field private static final synthetic cZl:[Lcom/xiaomi/mipush/sdk/PhoneBrand;

.field public static final enum cZm:Lcom/xiaomi/mipush/sdk/PhoneBrand;

.field public static final enum cZn:Lcom/xiaomi/mipush/sdk/PhoneBrand;

.field public static final enum cZo:Lcom/xiaomi/mipush/sdk/PhoneBrand;

.field public static final enum cZp:Lcom/xiaomi/mipush/sdk/PhoneBrand;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;

    const-string/jumbo v1, "HUAWEI"

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/mipush/sdk/PhoneBrand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZo:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    new-instance v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;

    const-string/jumbo v1, "MEIZU"

    invoke-direct {v0, v1, v3}, Lcom/xiaomi/mipush/sdk/PhoneBrand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZp:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    new-instance v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;

    const-string/jumbo v1, "FCM"

    invoke-direct {v0, v1, v4}, Lcom/xiaomi/mipush/sdk/PhoneBrand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZm:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    new-instance v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;

    const-string/jumbo v1, "OPPO"

    invoke-direct {v0, v1, v5}, Lcom/xiaomi/mipush/sdk/PhoneBrand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZk:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    new-instance v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;

    const-string/jumbo v1, "OTHER"

    invoke-direct {v0, v1, v6}, Lcom/xiaomi/mipush/sdk/PhoneBrand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZn:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/xiaomi/mipush/sdk/PhoneBrand;

    sget-object v1, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZo:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZp:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZm:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZk:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZn:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    aput-object v1, v0, v6

    sput-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZl:[Lcom/xiaomi/mipush/sdk/PhoneBrand;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/mipush/sdk/PhoneBrand;
    .locals 1

    const-class v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/mipush/sdk/PhoneBrand;
    .locals 1

    sget-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZl:[Lcom/xiaomi/mipush/sdk/PhoneBrand;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/mipush/sdk/PhoneBrand;

    return-object v0
.end method
