.class public Lcom/xiaomi/mipush/sdk/H;
.super Ljava/lang/Object;
.source "MiPushClient4Hybrid.java"


# static fields
.field private static cZC:Ljava/util/Map;

.field private static cZD:Ljava/util/Map;

.field private static cZE:Lcom/xiaomi/mipush/sdk/J;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/mipush/sdk/H;->cZC:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/mipush/sdk/H;->cZD:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cHd(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;)V
    .locals 7

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->cYs()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->cYv()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    sget-object v0, Lcom/xiaomi/mipush/sdk/H;->cZC:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/o;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->regId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v1, v5

    :goto_1
    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->errorCode:J

    iget-object v4, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->reason:Ljava/lang/String;

    const-string/jumbo v0, "register"

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/k;->cDo(Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/H;->cZE:Lcom/xiaomi/mipush/sdk/J;

    if-nez v1, :cond_3

    :goto_2
    return-void

    :cond_1
    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->regId:Ljava/lang/String;

    iget-object v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->regSecret:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/mipush/sdk/o;->cEl(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    invoke-virtual {v1, v6, v0}, Lcom/xiaomi/mipush/sdk/l;->cDP(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/o;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->regId:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/xiaomi/mipush/sdk/H;->cZE:Lcom/xiaomi/mipush/sdk/J;

    invoke-virtual {v1, v6, v0}, Lcom/xiaomi/mipush/sdk/J;->cHm(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;)V

    goto :goto_2
.end method

.method public static cHe(Landroid/content/Context;Ljava/lang/String;Lcom/xiaomi/mipush/sdk/MiPushMessage;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/xiaomi/mipush/sdk/H;->cZE:Lcom/xiaomi/mipush/sdk/J;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lcom/xiaomi/mipush/sdk/H;->cZE:Lcom/xiaomi/mipush/sdk/J;

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/mipush/sdk/J;->cHl(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/MiPushMessage;)V

    goto :goto_0
.end method

.method public static cHf(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;)V
    .locals 6

    const/4 v1, 0x0

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;->errorCode:J

    iget-object v4, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;->reason:Ljava/lang/String;

    const-string/jumbo v0, "unregister"

    move-object v5, v1

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/k;->cDo(Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;->cVE()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/mipush/sdk/H;->cZE:Lcom/xiaomi/mipush/sdk/J;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/xiaomi/mipush/sdk/H;->cZE:Lcom/xiaomi/mipush/sdk/J;

    invoke-virtual {v2, v1, v0}, Lcom/xiaomi/mipush/sdk/J;->cHk(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;)V

    goto :goto_0
.end method
