.class public Lcom/xiaomi/mipush/sdk/s;
.super Landroid/app/IntentService;
.source "MessageHandleService.java"


# static fields
.field private static cYL:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/xiaomi/mipush/sdk/s;->cYL:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string/jumbo v0, "MessageHandleThread"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static cEt(Lcom/xiaomi/mipush/sdk/r;)V
    .locals 1

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/s;->cYL:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected static cEu(Landroid/app/Service;Landroid/content/Intent;)V
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_1

    :try_start_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/s;->cYL:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/r;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/r;->cEs()Lcom/xiaomi/mipush/sdk/y;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/r;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "message_type"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    return-void

    :cond_2
    return-void

    :pswitch_1
    return-void

    :pswitch_2
    const-string/jumbo v2, "key_command"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/mipush/sdk/y;->cEU(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;)V

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;->cDw()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "register"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/mipush/sdk/y;->cEQ(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;)V

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;->cDB()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/h;->cDl(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0

    :pswitch_3
    :try_start_1
    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/v;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/v;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/xiaomi/mipush/sdk/v;->cEE(Landroid/content/Intent;)Lcom/xiaomi/mipush/sdk/PushMessageHandler$PushMessageInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/xiaomi/mipush/sdk/MiPushMessage;

    if-nez v2, :cond_3

    instance-of v2, v0, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/mipush/sdk/y;->cEU(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;)V

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;->cDw()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "register"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/mipush/sdk/y;->cEQ(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;)V

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;->cDB()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/h;->cDl(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    check-cast v0, Lcom/xiaomi/mipush/sdk/MiPushMessage;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cCV()Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_1
    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cDg()I

    move-result v2

    if-eq v2, v4, :cond_5

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cCH()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/mipush/sdk/y;->cER(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/MiPushMessage;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/mipush/sdk/y;->cEV(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/MiPushMessage;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/mipush/sdk/y;->cES(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/MiPushMessage;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v1, p0, v0}, Lcom/xiaomi/mipush/sdk/y;->cET(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/MiPushMessage;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/xiaomi/mipush/sdk/s;->cEu(Landroid/app/Service;Landroid/content/Intent;)V

    return-void
.end method
