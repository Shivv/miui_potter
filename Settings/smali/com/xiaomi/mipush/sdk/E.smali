.class public Lcom/xiaomi/mipush/sdk/E;
.super Ljava/lang/Object;
.source "GeoFenceRegMessageProcessor.java"


# static fields
.field private static volatile cZu:Lcom/xiaomi/mipush/sdk/E;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "GeoFenceRegMessageProcessor."

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    return-void
.end method

.method private cGS(Lcom/xiaomi/xmpush/thrift/GeoFencing;ZZ)V
    .locals 5

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v1

    new-instance v2, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-static {}, Lcom/xiaomi/mipush/sdk/C;->cGf()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>(Ljava/lang/String;Z)V

    if-nez p2, :cond_0

    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyT:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-virtual {v2, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dag([B)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    if-nez p3, :cond_1

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "GeoFenceRegMessageProcessor. report geo_fencing id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p2, :cond_2

    const-string/jumbo v0, "geo_unreg"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  isUnauthorized:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzn:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/xiaomi/push/service/aA;->dhV:Ljava/lang/String;

    const-string/jumbo v1, "permission_to_location"

    invoke-virtual {v2, v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dao(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "geo_reg"

    goto :goto_2
.end method

.method private cGT(Z)Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;
    .locals 4

    new-instance v1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;-><init>()V

    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    if-nez p1, :cond_1

    :cond_0
    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deV(Ljava/util/Set;)Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;

    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/az;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/az;->cRY()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static cGV(Ljava/util/Map;)Z
    .locals 2

    if-eqz p0, :cond_0

    const-string/jumbo v1, "1"

    const-string/jumbo v0, "__geo_local_cache"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private cGX(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;Z)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 3

    const/4 v2, 0x0

    if-nez p2, :cond_2

    :cond_0
    if-nez p2, :cond_3

    :cond_1
    :try_start_0
    new-instance v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;-><init>()V

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dan()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V
    :try_end_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/aJ;->cSn(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object v2

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/aJ;->cSk(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/apache/thrift/TException;->printStackTrace()V

    return-object v2
.end method

.method private cGZ(Lcom/xiaomi/xmpush/thrift/GeoFencing;)V
    .locals 5

    invoke-static {p1}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-static {}, Lcom/xiaomi/mipush/sdk/C;->cGf()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>(Ljava/lang/String;Z)V

    sget-object v2, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyQ:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v2, v2, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dag([B)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "GeoFenceRegMessageProcessor. report package not exist geo_fencing id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-void
.end method

.method private cHa(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/E;->cGV(Ljava/util/Map;)Z

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/push/service/aJ;->cSo(Landroid/content/Context;)Z

    move-result v2

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/E;
    .locals 2

    sget-object v0, Lcom/xiaomi/mipush/sdk/E;->cZu:Lcom/xiaomi/mipush/sdk/E;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/E;->cZu:Lcom/xiaomi/mipush/sdk/E;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/mipush/sdk/E;

    const-class v0, Lcom/xiaomi/mipush/sdk/E;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/E;->cZu:Lcom/xiaomi/mipush/sdk/E;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/mipush/sdk/E;

    invoke-direct {v0, p0}, Lcom/xiaomi/mipush/sdk/E;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/E;->cZu:Lcom/xiaomi/mipush/sdk/E;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public cGU(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)V
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/aJ;->cSm(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/E;->cHa(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    if-nez v0, :cond_4

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->packageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/xiaomi/channel/commonutils/android/b;->czP(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/E;->cGT(Z)Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v1

    new-instance v2, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    const-string/jumbo v3, "-1"

    invoke-direct {v2, v3, v4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>(Ljava/lang/String;Z)V

    sget-object v3, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzr:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v3, v3, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-virtual {v2, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dag([B)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v1

    sget-object v3, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "GeoFenceRegMessageProcessor. sync_geo_data. geos size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deX()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-void

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/push/service/aJ;->cSn(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_4
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/push/service/aJ;->cSk(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_5
    return-void
.end method

.method public cGW(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/E;->cHa(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/mipush/sdk/E;->cGX(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;Z)Lcom/xiaomi/xmpush/thrift/GeoFencing;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/push/service/aJ;->cSm(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcs()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/xiaomi/channel/commonutils/android/b;->czP(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/az;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/az;

    move-result-object v0

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/az;->cRP(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/O;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/O;

    move-result-object v0

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/O;->cOq(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    :goto_1
    new-instance v0, Lcom/xiaomi/mipush/sdk/b;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/xiaomi/mipush/sdk/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/xiaomi/mipush/sdk/b;->cCq(Ljava/lang/String;)V

    invoke-direct {p0, v1, v4, v4}, Lcom/xiaomi/mipush/sdk/E;->cGS(Lcom/xiaomi/xmpush/thrift/GeoFencing;ZZ)V

    const-string/jumbo v0, "GeoFenceRegMessageProcessor. receive geo unreg notification"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "GeoFenceRegMessageProcessor. unregistration convert geofence object failed notification_id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v1, v4, v0}, Lcom/xiaomi/mipush/sdk/E;->cGS(Lcom/xiaomi/xmpush/thrift/GeoFencing;ZZ)V

    return-void

    :cond_2
    if-nez v0, :cond_3

    :goto_2
    return-void

    :cond_3
    invoke-direct {p0, v1}, Lcom/xiaomi/mipush/sdk/E;->cGZ(Lcom/xiaomi/xmpush/thrift/GeoFencing;)V

    goto :goto_2

    :cond_4
    invoke-direct {p0, v1, v4, v4}, Lcom/xiaomi/mipush/sdk/E;->cGS(Lcom/xiaomi/xmpush/thrift/GeoFencing;ZZ)V

    return-void

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "GeoFenceRegMessageProcessor. delete a geofence about geo_id:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " falied"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "GeoFenceRegMessageProcessor. delete all geofence messages about geo_id:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " failed"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public cGY(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/E;->cHa(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/mipush/sdk/E;->cGX(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;Z)Lcom/xiaomi/xmpush/thrift/GeoFencing;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/push/service/aJ;->cSm(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcs()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/xiaomi/channel/commonutils/android/b;->czP(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/az;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/az;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/az;->cRS(Lcom/xiaomi/xmpush/thrift/GeoFencing;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "GeoFenceRegMessageProcessor. insert a new geofence failed about geo_id:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/xiaomi/mipush/sdk/b;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/E;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/xiaomi/mipush/sdk/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/b;->cCp(Lcom/xiaomi/xmpush/thrift/GeoFencing;)Z

    invoke-direct {p0, v1, v7, v6}, Lcom/xiaomi/mipush/sdk/E;->cGS(Lcom/xiaomi/xmpush/thrift/GeoFencing;ZZ)V

    const-string/jumbo v0, "GeoFenceRegMessageProcessor. receive geo reg notification"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "GeoFenceRegMessageProcessor. registration convert geofence object failed notification_id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-direct {p0, v1, v7, v7}, Lcom/xiaomi/mipush/sdk/E;->cGS(Lcom/xiaomi/xmpush/thrift/GeoFencing;ZZ)V

    return-void

    :cond_3
    if-nez v0, :cond_4

    :goto_0
    return-void

    :cond_4
    invoke-direct {p0, v1}, Lcom/xiaomi/mipush/sdk/E;->cGZ(Lcom/xiaomi/xmpush/thrift/GeoFencing;)V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v1, v7, v6}, Lcom/xiaomi/mipush/sdk/E;->cGS(Lcom/xiaomi/xmpush/thrift/GeoFencing;ZZ)V

    return-void
.end method
