.class public Lcom/xiaomi/mipush/sdk/p;
.super Ljava/lang/Object;
.source "PushConfiguration.java"


# instance fields
.field private cYF:Z

.field private cYG:Z

.field private cYH:Lcom/xiaomi/push/service/module/PushChannelRegion;

.field private cYI:Z

.field private cYJ:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/xiaomi/push/service/module/PushChannelRegion;->ddy:Lcom/xiaomi/push/service/module/PushChannelRegion;

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/p;->cYH:Lcom/xiaomi/push/service/module/PushChannelRegion;

    iput-boolean v1, p0, Lcom/xiaomi/mipush/sdk/p;->cYI:Z

    iput-boolean v1, p0, Lcom/xiaomi/mipush/sdk/p;->cYF:Z

    iput-boolean v1, p0, Lcom/xiaomi/mipush/sdk/p;->cYJ:Z

    iput-boolean v1, p0, Lcom/xiaomi/mipush/sdk/p;->cYG:Z

    return-void
.end method


# virtual methods
.method public cEn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/mipush/sdk/p;->cYF:Z

    return v0
.end method

.method public cEo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/mipush/sdk/p;->cYJ:Z

    return v0
.end method

.method public cEp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/mipush/sdk/p;->cYG:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v1, "PushConfiguration{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "Region:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/p;->cYH:Lcom/xiaomi/push/service/module/PushChannelRegion;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/p;->cYH:Lcom/xiaomi/push/service/module/PushChannelRegion;

    invoke-virtual {v1}, Lcom/xiaomi/push/service/module/PushChannelRegion;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
