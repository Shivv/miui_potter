.class public Lcom/xiaomi/mipush/sdk/v;
.super Ljava/lang/Object;
.source "PushMessageProcessor.java"


# static fields
.field private static cYN:Ljava/util/Queue;

.field private static cYO:Lcom/xiaomi/mipush/sdk/v;

.field private static cYQ:Ljava/lang/Object;


# instance fields
.field private cYP:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/mipush/sdk/v;->cYO:Lcom/xiaomi/mipush/sdk/v;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/mipush/sdk/v;->cYQ:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    goto :goto_0
.end method

.method private cEA(Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;)V
    .locals 4

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->deG()Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "receiver hw token sync ack"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->errorCode:J

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/xiaomi/mipush/sdk/v;->cEF(Ljava/lang/String;JLcom/xiaomi/mipush/sdk/AssemblePush;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v2, "RegInfo"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "brand:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZm:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    invoke-virtual {v3}, Lcom/xiaomi/mipush/sdk/PhoneBrand;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "brand:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZo:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    invoke-virtual {v3}, Lcom/xiaomi/mipush/sdk/PhoneBrand;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "brand:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZk:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    invoke-virtual {v3}, Lcom/xiaomi/mipush/sdk/PhoneBrand;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "receiver COS token sync ack"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->errorCode:J

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/xiaomi/mipush/sdk/v;->cEF(Ljava/lang/String;JLcom/xiaomi/mipush/sdk/AssemblePush;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "receiver fcm token sync ack"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->errorCode:J

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/xiaomi/mipush/sdk/v;->cEF(Ljava/lang/String;JLcom/xiaomi/mipush/sdk/AssemblePush;)V

    goto/16 :goto_0

    :cond_3
    const-string/jumbo v0, "receiver hw token sync ack"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->errorCode:J

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/xiaomi/mipush/sdk/v;->cEF(Ljava/lang/String;JLcom/xiaomi/mipush/sdk/AssemblePush;)V

    goto/16 :goto_0
.end method

.method public static cEB(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;
    .locals 6

    const/4 v2, 0x0

    if-nez p2, :cond_1

    :cond_0
    return-object v2

    :cond_1
    const-string/jumbo v0, "notify_effect"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "notify_effect"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/xiaomi/push/service/b;->dep:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/xiaomi/push/service/b;->ddR:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/xiaomi/push/service/b;->ded:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    move-object v1, v2

    :cond_2
    :goto_0
    if-nez v1, :cond_f

    :cond_3
    :goto_1
    return-object v2

    :cond_4
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cause: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_0

    :cond_5
    const-string/jumbo v0, "intent_uri"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "class_name"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    move-object v1, v2

    goto :goto_0

    :cond_6
    const-string/jumbo v0, "intent_uri"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_7

    move-object v1, v2

    goto :goto_0

    :cond_7
    const/4 v1, 0x1

    :try_start_1
    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v1

    :try_start_2
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Cause: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    const-string/jumbo v0, "class_name"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, p1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :try_start_3
    const-string/jumbo v0, "intent_flag"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "intent_flag"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Cause by intent_flag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v0, "web_uri"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_a

    move-object v1, v2

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_b
    move-object v1, v0

    :goto_3
    :try_start_4
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "http"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    :cond_c
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_3

    :try_start_5
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
    :try_end_5
    .catch Ljava/net/MalformedURLException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_4
    move-object v1, v0

    goto/16 :goto_0

    :cond_d
    const-string/jumbo v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "http://"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    :cond_e
    :try_start_6
    const-string/jumbo v3, "https"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/net/MalformedURLException; {:try_start_6 .. :try_end_6} :catch_3

    move-result v0

    if-nez v0, :cond_c

    move-object v0, v2

    goto :goto_4

    :catch_3
    move-exception v0

    move-object v1, v2

    :goto_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Cause: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_7
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v3, 0x10000

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    move-result-object v0

    if-eqz v0, :cond_3

    return-object v1

    :catch_4
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cause: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_5
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_5

    :catch_6
    move-exception v0

    move-object v1, v2

    goto/16 :goto_2
.end method

.method private cEC()V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    const-string/jumbo v2, "mipush_extra"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string/jumbo v4, "last_reinitialize"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0x1b7740

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    sget-object v4, Lcom/xiaomi/xmpush/thrift/RegistrationReason;->dsU:Lcom/xiaomi/xmpush/thrift/RegistrationReason;

    invoke-static {v0, v4}, Lcom/xiaomi/mipush/sdk/C;->cGj(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/RegistrationReason;)V

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_reinitialize"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    return-void
.end method

.method private cED(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;[B)Lcom/xiaomi/mipush/sdk/PushMessageHandler$PushMessageInterface;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/xiaomi/mipush/sdk/R;->cHA(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Lorg/apache/thrift/TBase;
    :try_end_0
    .catch Lcom/xiaomi/mipush/sdk/DecryptException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddQ()Lcom/xiaomi/xmpush/thrift/ActionType;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "message arrived: processing an arrived message, action="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    sget-object v3, Lcom/xiaomi/mipush/sdk/i;->cYm:[I

    invoke-virtual {v2}, Lcom/xiaomi/xmpush/thrift/ActionType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    return-object v1

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "message arrived: receiving an un-recognized message. "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->action:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/xiaomi/mipush/sdk/DecryptException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/thrift/TException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    const-string/jumbo v0, "message arrived: receive a message but decrypt failed. report when click."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    const-string/jumbo v0, "message arrived: receive a message which action string is not valid. is the reg expired?"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v1

    :pswitch_0
    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->getMessage()Lcom/xiaomi/xmpush/thrift/PushMessage;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    if-nez v3, :cond_3

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/xiaomi/mipush/sdk/k;->cDt(Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;Lcom/xiaomi/xmpush/thrift/PushMetaInfo;Z)Lcom/xiaomi/mipush/sdk/MiPushMessage;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cCS(Z)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "message arrived: receive a message, msgid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/xiaomi/xmpush/thrift/PushMessage;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", jobkey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v0

    :cond_2
    const-string/jumbo v0, "message arrived: receive an empty message without push content, drop it"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v1

    :cond_3
    iget-object v3, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZO()Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    const-string/jumbo v3, "jobkey"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private cEF(Ljava/lang/String;JLcom/xiaomi/mipush/sdk/AssemblePush;)V
    .locals 4

    invoke-static {p4}, Lcom/xiaomi/mipush/sdk/D;->cGO(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/RetryType;

    move-result-object v0

    if-eqz v0, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-nez v1, :cond_3

    const-class v1, Lcom/xiaomi/mipush/sdk/m;

    const-class v2, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v2

    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/xiaomi/mipush/sdk/m;->cDW(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    :goto_0
    monitor-exit v1

    :goto_1
    return-void

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "syncing"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v2

    const-string/jumbo v3, "synced"

    invoke-virtual {v2, v0, v3}, Lcom/xiaomi/mipush/sdk/m;->cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "syncing"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-class v1, Lcom/xiaomi/mipush/sdk/m;

    const-class v2, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v2

    :try_start_1
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/xiaomi/mipush/sdk/m;->cDW(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    :goto_2
    monitor-exit v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :cond_5
    :try_start_2
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/xiaomi/mipush/sdk/m;->cDY(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0xa

    if-lt v2, v3, :cond_6

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/xiaomi/mipush/sdk/m;->cDZ(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v2

    invoke-virtual {v2, p1, v0, p4}, Lcom/xiaomi/mipush/sdk/z;->cFC(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;Lcom/xiaomi/mipush/sdk/AssemblePush;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2
.end method

.method private cEG(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V
    .locals 5

    const/4 v4, 0x0

    const-string/jumbo v0, "receive a message but decrypt failed. report now."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    invoke-direct {v0, v1, v4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>(Ljava/lang/String;Z)V

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzg:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dau(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/C;->cGz(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "regid"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v4, v3}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    return-void
.end method

.method private cEH(Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V
    .locals 4

    invoke-virtual {p2}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;-><init>()V

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfQ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWT(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->getMessage()Lcom/xiaomi/xmpush/thrift/PushMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/xmpush/thrift/PushMessage;->del()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXj(J)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfS()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2, p2}, Lcom/xiaomi/xmpush/thrift/a;->daJ(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)S

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXi(S)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/xmpush/thrift/ActionType;->dAP:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {v2, v1, v3, v0}, Lcom/xiaomi/mipush/sdk/z;->cFf(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfS()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWF(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfF()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWY(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    goto :goto_1
.end method

.method private static cEv(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8

    const/4 v1, 0x0

    sget-object v2, Lcom/xiaomi/mipush/sdk/v;->cYQ:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    sget-object v0, Lcom/xiaomi/mipush/sdk/v;->cYN:Ljava/util/Queue;

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/v;->cYN:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/xiaomi/mipush/sdk/v;->cYN:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/xiaomi/mipush/sdk/v;->cYN:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/16 v4, 0x19

    if-gt v0, v4, :cond_3

    :goto_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/v;->cYN:Ljava/util/Queue;

    const-string/jumbo v4, ","

    invoke-static {v0, v4}, Lcom/xiaomi/channel/commonutils/c/b;->czi(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string/jumbo v4, "pref_msg_ids"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/n;->cEd(Landroid/content/SharedPreferences$Editor;)V

    monitor-exit v2

    return v1

    :cond_1
    const-string/jumbo v0, "pref_msg_ids"

    const-string/jumbo v4, ""

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/xiaomi/mipush/sdk/v;->cYN:Ljava/util/Queue;

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    sget-object v7, Lcom/xiaomi/mipush/sdk/v;->cYN:Ljava/util/Queue;

    invoke-interface {v7, v6}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    monitor-exit v2

    const/4 v0, 0x1

    return v0

    :cond_3
    sget-object v0, Lcom/xiaomi/mipush/sdk/v;->cYN:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private cEw(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddP()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "com.miui.hybrid"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZO()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v2, "1"

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZO()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v3, "hybrid_pt"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private cEx(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;-><init>()V

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddE()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWT(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZq()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXj(J)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZN()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/xiaomi/xmpush/thrift/a;->daJ(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)S

    move-result v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXi(S)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ActionType;->dAP:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZN()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWF(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    goto :goto_0
.end method

.method private cEz(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;Z[B)Lcom/xiaomi/mipush/sdk/PushMessageHandler$PushMessageInterface;
    .locals 12

    const/4 v2, -0x2

    const/4 v4, 0x1

    const-wide/16 v10, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/xiaomi/mipush/sdk/R;->cHA(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Lorg/apache/thrift/TBase;
    :try_end_0
    .catch Lcom/xiaomi/mipush/sdk/DecryptException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddQ()Lcom/xiaomi/xmpush/thrift/ActionType;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "processing a message, action="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    sget-object v3, Lcom/xiaomi/mipush/sdk/i;->cYm:[I

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/ActionType;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "receiving an un-recognized message. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->action:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/xiaomi/mipush/sdk/DecryptException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/thrift/TException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v5

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/mipush/sdk/v;->cEG(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V

    return-object v5

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    const-string/jumbo v0, "receive a message which action string is not valid. is the reg expired?"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v5

    :pswitch_0
    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    iget-object v1, v1, Lcom/xiaomi/mipush/sdk/l;->cYr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string/jumbo v0, "bad Registration result:"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v5

    :cond_3
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    iput-object v5, v1, Lcom/xiaomi/mipush/sdk/l;->cYr:Ljava/lang/String;

    iget-wide v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->errorCode:J

    cmp-long v1, v2, v10

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    iget-object v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->regId:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->regSecret:Ljava/lang/String;

    iget-object v4, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->region:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/xiaomi/mipush/sdk/l;->cDI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->regId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object v1, v5

    :goto_1
    iget-wide v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->errorCode:J

    iget-object v4, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->reason:Ljava/lang/String;

    const-string/jumbo v0, "register"

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/k;->cDo(Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/z;->cFK()V

    return-object v0

    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;->regId:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;

    iget-wide v0, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;->errorCode:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->clear()V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/C;->cGr(Landroid/content/Context;)V

    :cond_6
    invoke-static {}, Lcom/xiaomi/mipush/sdk/PushMessageHandler;->cHH()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/l;->cDO()Z

    move-result v1

    if-nez v1, :cond_e

    :cond_7
    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->getMessage()Lcom/xiaomi/xmpush/thrift/PushMessage;

    move-result-object v3

    if-eqz v3, :cond_f

    if-nez p2, :cond_10

    :goto_2
    if-eqz p2, :cond_12

    :cond_8
    :goto_3
    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    if-nez v1, :cond_17

    :cond_9
    move-object v1, v5

    :goto_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_18

    move-object v2, v1

    :goto_5
    if-eqz p2, :cond_19

    :cond_a
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/xiaomi/mipush/sdk/k;->cDt(Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;Lcom/xiaomi/xmpush/thrift/PushMetaInfo;Z)Lcom/xiaomi/mipush/sdk/MiPushMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cDg()I

    move-result v4

    if-eqz v4, :cond_1a

    :cond_b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "receive a message, msgid="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/PushMessage;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, ", jobkey="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    if-nez p2, :cond_1b

    :cond_c
    move-object v5, v1

    :goto_6
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v1

    if-eqz v1, :cond_22

    :cond_d
    :goto_7
    return-object v5

    :cond_e
    if-nez p2, :cond_7

    const-string/jumbo v0, "receive a message in pause state. drop it"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v5

    :cond_f
    const-string/jumbo v0, "receive an empty message without push content, drop it"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v5

    :cond_10
    invoke-static {p1}, Lcom/xiaomi/push/service/h;->cMS(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Z

    move-result v1

    if-nez v1, :cond_11

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/PushMessage;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v7

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/PushMessage;->dem()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v2, v7, v8}, Lcom/xiaomi/mipush/sdk/C;->cGe(Landroid/content/Context;Ljava/lang/String;Lcom/xiaomi/xmpush/thrift/PushMetaInfo;Ljava/lang/String;)V

    goto :goto_2

    :cond_11
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/PushMessage;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v7

    iget-object v8, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->packageName:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/PushMessage;->dem()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v2, v7, v8, v9}, Lcom/xiaomi/mipush/sdk/C;->cGH(Landroid/content/Context;Ljava/lang/String;Lcom/xiaomi/xmpush/thrift/PushMetaInfo;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_12
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfF()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    :cond_13
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfS()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfS()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/C;->cGn(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v1, v8, v10

    if-ltz v1, :cond_16

    move v1, v4

    :goto_8
    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfS()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/C;->cGo(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_14
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/C;->cGD(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v1, v8, v10

    if-ltz v1, :cond_15

    move v1, v4

    :goto_9
    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/C;->cFY(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_15
    move v1, v6

    goto :goto_9

    :cond_16
    move v1, v6

    goto :goto_8

    :cond_17
    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZO()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    const-string/jumbo v2, "jobkey"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto/16 :goto_4

    :cond_18
    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/PushMessage;->getId()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_5

    :cond_19
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/v;->cEv(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "drop a duplicate message, key="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_1a
    if-nez p2, :cond_b

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cCX()Ljava/util/Map;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/push/service/h;->cNn(Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0, p1, p3}, Lcom/xiaomi/push/service/h;->cNb(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;[B)Lcom/xiaomi/push/service/z;

    return-object v5

    :cond_1b
    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cCX()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cCX()Ljava/util/Map;

    move-result-object v2

    const-string/jumbo v4, "notify_effect"

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    move-object v5, v1

    goto/16 :goto_6

    :cond_1c
    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/MiPushMessage;->cCX()Ljava/util/Map;

    move-result-object v2

    const-string/jumbo v0, "notify_effect"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1}, Lcom/xiaomi/push/service/h;->cMS(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Z

    move-result v4

    if-nez v4, :cond_1d

    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/xiaomi/mipush/sdk/v;->cEB(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_20

    :goto_a
    return-object v5

    :cond_1d
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->packageName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/xiaomi/mipush/sdk/v;->cEB(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1e

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/PushMessage;->dej()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1f

    :goto_b
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_a

    :cond_1e
    const-string/jumbo v0, "Getting Intent fail from ignore reg message. "

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v5

    :cond_1f
    const-string/jumbo v2, "payload"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_b

    :cond_20
    sget-object v3, Lcom/xiaomi/push/service/b;->ded:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    :goto_c
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_a

    :cond_21
    const-string/jumbo v0, "key_message"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_c

    :cond_22
    if-nez p2, :cond_d

    invoke-direct {p0, v0, p1}, Lcom/xiaomi/mipush/sdk/v;->cEH(Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V

    goto/16 :goto_7

    :pswitch_3
    move-object v6, v0

    check-cast v6, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;

    iget-wide v0, v6, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;->errorCode:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_23

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;->cYa()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/C;->cGo(Landroid/content/Context;Ljava/lang/String;)V

    :cond_23
    invoke-virtual {v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;->cYa()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_24

    move-object v1, v5

    :goto_d
    iget-wide v2, v6, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;->errorCode:J

    iget-object v4, v6, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;->reason:Ljava/lang/String;

    const-string/jumbo v0, "subscribe-topic"

    invoke-virtual {v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;->cXO()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/k;->cDo(Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    move-result-object v0

    return-object v0

    :cond_24
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;->cYa()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :pswitch_4
    move-object v6, v0

    check-cast v6, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;

    iget-wide v0, v6, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;->errorCode:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_25

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-virtual {v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;->daZ()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/C;->cGi(Landroid/content/Context;Ljava/lang/String;)V

    :cond_25
    invoke-virtual {v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;->daZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_26

    move-object v1, v5

    :goto_e
    iget-wide v2, v6, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;->errorCode:J

    iget-object v4, v6, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;->reason:Ljava/lang/String;

    const-string/jumbo v0, "unsubscibe-topic"

    invoke-virtual {v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;->daM()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/k;->cDo(Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    move-result-object v0

    return-object v0

    :cond_26
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;->daZ()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    :pswitch_5
    move-object v5, v0

    check-cast v5, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;

    invoke-virtual {v5}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;->cYQ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;->cYI()Ljava/util/List;

    move-result-object v3

    iget-wide v8, v5, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;->errorCode:J

    cmp-long v1, v8, v10

    if-nez v1, :cond_2c

    const-string/jumbo v1, "accept-time"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2d

    :cond_27
    const-string/jumbo v1, "set-alias"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_30

    :cond_28
    const-string/jumbo v1, "unset-alias"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_31

    :cond_29
    const-string/jumbo v1, "set-account"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_32

    :cond_2a
    const-string/jumbo v1, "unset-account"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_33

    :cond_2b
    move-object v1, v3

    :goto_f
    iget-wide v2, v5, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;->errorCode:J

    iget-object v4, v5, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;->reason:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;->cYH()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/k;->cDo(Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    move-result-object v0

    return-object v0

    :cond_2c
    move-object v1, v3

    goto :goto_f

    :cond_2d
    if-eqz v3, :cond_27

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v4, :cond_27

    iget-object v7, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v7, v1, v2}, Lcom/xiaomi/mipush/sdk/C;->cGq(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    const-string/jumbo v2, "00:00"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2f

    :cond_2e
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/xiaomi/mipush/sdk/l;->cDR(Z)V

    :goto_10
    const-string/jumbo v1, "GMT+08"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/xiaomi/mipush/sdk/v;->cEy(Ljava/util/TimeZone;Ljava/util/TimeZone;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    goto :goto_f

    :cond_2f
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    const-string/jumbo v2, "00:00"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/xiaomi/mipush/sdk/l;->cDR(Z)V

    goto :goto_10

    :cond_30
    if-eqz v3, :cond_28

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_28

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/xiaomi/mipush/sdk/C;->cFY(Landroid/content/Context;Ljava/lang/String;)V

    move-object v1, v3

    goto :goto_f

    :cond_31
    if-eqz v3, :cond_29

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_29

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/xiaomi/mipush/sdk/C;->cFV(Landroid/content/Context;Ljava/lang/String;)V

    move-object v1, v3

    goto/16 :goto_f

    :cond_32
    if-eqz v3, :cond_2a

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2a

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/xiaomi/mipush/sdk/C;->cGC(Landroid/content/Context;Ljava/lang/String;)V

    move-object v1, v3

    goto/16 :goto_f

    :cond_33
    if-eqz v3, :cond_2b

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_34

    move-object v1, v3

    goto/16 :goto_f

    :cond_34
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/xiaomi/mipush/sdk/C;->cGA(Landroid/content/Context;Ljava/lang/String;)V

    move-object v1, v3

    goto/16 :goto_f

    :pswitch_6
    instance-of v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;

    if-nez v1, :cond_35

    instance-of v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    const-string/jumbo v3, "registration id expired"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_44

    iget-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    const-string/jumbo v3, "client_info_update_ok"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_48

    iget-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    const-string/jumbo v3, "awake_app"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_49

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyR:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4a

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyV:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4b

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzH:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4c

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzu:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4d

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyK:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4e

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dza:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4f

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyU:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_50

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzh:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_51

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzv:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_57

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyN:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_2
    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;-><init>()V

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dan()[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/H;->cHf(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;)V
    :try_end_2
    .catch Lorg/apache/thrift/TException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_35
    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->getId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzb:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v2, v2, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_36

    sget-object v2, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzz:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v2, v2, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3d

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzd:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v1, v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    iget-object v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/xiaomi/mipush/sdk/v;->cEA(Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;)V

    goto/16 :goto_0

    :cond_36
    iget-wide v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->errorCode:J

    cmp-long v0, v2, v10

    if-nez v0, :cond_39

    const-class v2, Lcom/xiaomi/mipush/sdk/m;

    const-class v0, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v0

    :try_start_3
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cDW(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_38

    :cond_37
    :goto_11
    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_38
    :try_start_4
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "syncing"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    const-string/jumbo v3, "synced"

    invoke-virtual {v0, v1, v3}, Lcom/xiaomi/mipush/sdk/m;->cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/C;->cGK(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/C;->cGl(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/mipush/sdk/PushMessageHandler;->cHH()V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/z;->cFN()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_11

    :cond_39
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v2}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "syncing"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3a

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3a
    const-class v2, Lcom/xiaomi/mipush/sdk/m;

    const-class v0, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v0

    :try_start_5
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cDW(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3b

    :goto_12
    monitor-exit v2

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :cond_3b
    :try_start_6
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cDY(Ljava/lang/String;)I

    move-result v0

    const/16 v3, 0xa

    if-lt v0, v3, :cond_3c

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    goto :goto_12

    :cond_3c
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cDZ(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/xiaomi/mipush/sdk/z;->cFt(ZLjava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_12

    :cond_3d
    iget-wide v2, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->errorCode:J

    cmp-long v0, v2, v10

    if-nez v0, :cond_40

    const-class v2, Lcom/xiaomi/mipush/sdk/m;

    const-class v0, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v0

    :try_start_7
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cDW(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3f

    :cond_3e
    :goto_13
    monitor-exit v2

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    :cond_3f
    :try_start_8
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "syncing"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    const-string/jumbo v3, "synced"

    invoke-virtual {v0, v1, v3}, Lcom/xiaomi/mipush/sdk/m;->cEa(Lcom/xiaomi/mipush/sdk/RetryType;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_13

    :cond_40
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v2}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "syncing"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_41

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_41
    const-class v2, Lcom/xiaomi/mipush/sdk/m;

    const-class v0, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v0

    :try_start_9
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cDW(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_42

    :goto_14
    monitor-exit v2

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    :cond_42
    :try_start_a
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cDY(Ljava/lang/String;)I

    move-result v0

    const/16 v3, 0xa

    if-lt v0, v3, :cond_43

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    goto :goto_14

    :cond_43
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cDZ(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/xiaomi/mipush/sdk/z;->cFt(ZLjava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto :goto_14

    :cond_44
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/C;->cGg(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/C;->cGp(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/C;->cGM(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/C;->cGt(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    sget-object v8, Lcom/xiaomi/xmpush/thrift/RegistrationReason;->dsT:Lcom/xiaomi/xmpush/thrift/RegistrationReason;

    invoke-static {v7, v8}, Lcom/xiaomi/mipush/sdk/C;->cGj(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/RegistrationReason;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_15
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_45

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_46

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_47

    const-string/jumbo v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    aget-object v2, v0, v6

    aget-object v0, v0, v4

    invoke-static {v1, v2, v0}, Lcom/xiaomi/mipush/sdk/C;->cGq(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_45
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v8, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v8, v0, v5}, Lcom/xiaomi/mipush/sdk/C;->cFZ(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_15

    :cond_46
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v7, v0, v5}, Lcom/xiaomi/mipush/sdk/C;->cGb(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_16

    :cond_47
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2, v0, v5}, Lcom/xiaomi/mipush/sdk/C;->cGx(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_17

    :cond_48
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "app_version"

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "app_version"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/mipush/sdk/l;->cDT(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_49
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "packages"

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "packages"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/xiaomi/mipush/sdk/C;->cGB(Landroid/content/Context;[Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4a
    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig;-><init>()V

    :try_start_b
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dan()[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/aU;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aU;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/xiaomi/push/service/au;->cRx(Lcom/xiaomi/push/service/aU;Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/h;->cDl(Landroid/content/Context;)V
    :try_end_b
    .catch Lorg/apache/thrift/TException; {:try_start_b .. :try_end_b} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_4b
    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCustomConfig;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCustomConfig;-><init>()V

    :try_start_c
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dan()[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/aU;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aU;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/xiaomi/push/service/au;->cRy(Lcom/xiaomi/push/service/aU;Lcom/xiaomi/xmpush/thrift/XmPushActionCustomConfig;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/h;->cDl(Landroid/content/Context;)V
    :try_end_c
    .catch Lorg/apache/thrift/TException; {:try_start_c .. :try_end_c} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_4c
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/xiaomi/mipush/sdk/d;->cCu(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)V

    goto/16 :goto_0

    :cond_4d
    const-string/jumbo v0, "receive force sync notification"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/xiaomi/mipush/sdk/d;->cCv(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_4e
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/E;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/E;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/mipush/sdk/E;->cGY(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)V

    goto/16 :goto_0

    :cond_4f
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/E;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/E;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/mipush/sdk/E;->cGW(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)V

    goto/16 :goto_0

    :cond_50
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/E;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/E;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/mipush/sdk/E;->cGU(Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;)V

    goto/16 :goto_0

    :cond_51
    const-string/jumbo v1, ""

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    sget-object v3, Lcom/xiaomi/push/service/b;->dec:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_52

    move v1, v2

    :goto_18
    const/4 v2, -0x1

    if-ge v1, v2, :cond_54

    const-string/jumbo v1, ""

    const-string/jumbo v2, ""

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lcom/xiaomi/push/service/b;->ddU:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_55

    :goto_19
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lcom/xiaomi/push/service/b;->deq:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_56

    move-object v0, v2

    :goto_1a
    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2, v1, v0}, Lcom/xiaomi/mipush/sdk/C;->cGm(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_52
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    sget-object v3, Lcom/xiaomi/push/service/b;->dec:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_53

    move v1, v2

    goto :goto_18

    :cond_53
    :try_start_d
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_d} :catch_5

    move-result v1

    goto :goto_18

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    move v1, v2

    goto :goto_18

    :cond_54
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/C;->cGE(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_55
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v1

    sget-object v3, Lcom/xiaomi/push/service/b;->ddU:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_19

    :cond_56
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daz()Ljava/util/Map;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/push/service/b;->deq:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1a

    :cond_57
    :try_start_e
    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;-><init>()V

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dan()[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/H;->cHd(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;)V
    :try_end_e
    .catch Lorg/apache/thrift/TException; {:try_start_e .. :try_end_e} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/v;
    .locals 1

    sget-object v0, Lcom/xiaomi/mipush/sdk/v;->cYO:Lcom/xiaomi/mipush/sdk/v;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/v;->cYO:Lcom/xiaomi/mipush/sdk/v;

    return-object v0

    :cond_0
    new-instance v0, Lcom/xiaomi/mipush/sdk/v;

    invoke-direct {v0, p0}, Lcom/xiaomi/mipush/sdk/v;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/v;->cYO:Lcom/xiaomi/mipush/sdk/v;

    goto :goto_0
.end method


# virtual methods
.method public cEE(Landroid/content/Intent;)Lcom/xiaomi/mipush/sdk/PushMessageHandler$PushMessageInterface;
    .locals 9

    const/4 v8, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "receive an intent from server, action="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    const-string/jumbo v0, "mrt"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    const-string/jumbo v2, "com.xiaomi.mipush.RECEIVE_MESSAGE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v0, "com.xiaomi.mipush.ERROR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string/jumbo v0, "com.xiaomi.mipush.MESSAGE_ARRIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    :goto_1
    return-object v8

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "mipush_payload"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    const-string/jumbo v2, "mipush_notified"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v1, :cond_6

    new-instance v3, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-direct {v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;-><init>()V

    :try_start_0
    invoke-static {v3, v1}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v4}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v4

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v5

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddQ()Lcom/xiaomi/xmpush/thrift/ActionType;

    move-result-object v6

    sget-object v7, Lcom/xiaomi/xmpush/thrift/ActionType;->dAI:Lcom/xiaomi/xmpush/thrift/ActionType;

    if-eq v6, v7, :cond_7

    :cond_2
    :goto_2
    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddQ()Lcom/xiaomi/xmpush/thrift/ActionType;

    move-result-object v0

    sget-object v6, Lcom/xiaomi/xmpush/thrift/ActionType;->dAI:Lcom/xiaomi/xmpush/thrift/ActionType;

    if-eq v0, v6, :cond_9

    :cond_3
    invoke-virtual {v4}, Lcom/xiaomi/mipush/sdk/l;->cDL()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_4
    invoke-virtual {v4}, Lcom/xiaomi/mipush/sdk/l;->cDL()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_5
    invoke-direct {p0, v3, v2, v1}, Lcom/xiaomi/mipush/sdk/v;->cEz(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;Z[B)Lcom/xiaomi/mipush/sdk/PushMessageHandler$PushMessageInterface;
    :try_end_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :cond_6
    const-string/jumbo v0, "receiving an empty message, drop"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v8

    :cond_7
    if-eqz v5, :cond_2

    :try_start_1
    invoke-virtual {v4}, Lcom/xiaomi/mipush/sdk/l;->cDO()Z

    move-result v6

    if-nez v6, :cond_2

    if-nez v2, :cond_2

    const-string/jumbo v6, "mrt"

    invoke-virtual {v5, v6, v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, "mat"

    invoke-virtual {v5, v6, v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/xiaomi/mipush/sdk/v;->cEw(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "this is a mina\'s pass through message, ack later"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V

    const-string/jumbo v0, "__mina_message_ts"

    invoke-virtual {v5}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZq()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    const-string/jumbo v6, "__mina_device_status"

    invoke-static {v0, v3}, Lcom/xiaomi/xmpush/thrift/a;->daJ(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)S

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZv(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddE()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, "__mina_appid"

    invoke-virtual {v5, v6, v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZv(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/apache/thrift/TException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_8
    :try_start_2
    invoke-direct {p0, v3}, Lcom/xiaomi/mipush/sdk/v;->cEx(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V
    :try_end_2
    .catch Lorg/apache/thrift/TException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_9
    :try_start_3
    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddC()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v3}, Lcom/xiaomi/push/service/h;->cMS(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Z

    move-result v0

    if-eqz v0, :cond_b

    if-nez v2, :cond_d

    :cond_a
    :goto_3
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string/jumbo v1, "drop an un-encrypted messages. %1$s, %2$s"

    const/4 v2, 0x0

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddP()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    invoke-virtual {v5}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->getId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v8

    :cond_b
    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const-string/jumbo v2, "drop an un-encrypted messages. %1$s, %2$s"

    const/4 v0, 0x0

    invoke-virtual {v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddP()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    const/4 v3, 0x1

    if-nez v5, :cond_c

    const-string/jumbo v0, ""

    :goto_4
    aput-object v0, v1, v3

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v8

    :cond_c
    invoke-virtual {v5}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_d
    invoke-virtual {v5}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZO()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v5}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZO()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v6, "notify_effect"

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_3

    :cond_e
    iget-object v0, v3, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->action:Lcom/xiaomi/xmpush/thrift/ActionType;

    sget-object v5, Lcom/xiaomi/xmpush/thrift/ActionType;->dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

    if-eq v0, v5, :cond_4

    invoke-static {v3}, Lcom/xiaomi/push/service/h;->cMS(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Z

    move-result v0

    if-nez v0, :cond_f

    const-string/jumbo v0, "receive message without registration. need re-register!"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/v;->cEC()V

    goto/16 :goto_1

    :cond_f
    invoke-direct {p0, v3, v2, v1}, Lcom/xiaomi/mipush/sdk/v;->cEz(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;Z[B)Lcom/xiaomi/mipush/sdk/PushMessageHandler$PushMessageInterface;

    move-result-object v0

    return-object v0

    :cond_10
    invoke-virtual {v4}, Lcom/xiaomi/mipush/sdk/l;->cDN()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->action:Lcom/xiaomi/xmpush/thrift/ActionType;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAO:Lcom/xiaomi/xmpush/thrift/ActionType;

    if-eq v0, v1, :cond_11

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/C;->cGa(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_11
    invoke-virtual {v4}, Lcom/xiaomi/mipush/sdk/l;->clear()V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/C;->cGr(Landroid/content/Context;)V

    invoke-static {}, Lcom/xiaomi/mipush/sdk/PushMessageHandler;->cHH()V
    :try_end_3
    .catch Lorg/apache/thrift/TException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    :cond_12
    new-instance v0, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;

    invoke-direct {v0}, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;-><init>()V

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;-><init>()V

    :try_start_4
    const-string/jumbo v2, "mipush_payload"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B
    :try_end_4
    .catch Lorg/apache/thrift/TException; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v2

    if-nez v2, :cond_13

    :goto_5
    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddQ()Lcom/xiaomi/xmpush/thrift/ActionType;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;->cDA(Ljava/lang/String;)V

    const-string/jumbo v1, "mipush_error_code"

    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;->cDy(J)V

    const-string/jumbo v1, "mipush_error_msg"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/MiPushCommandMessage;->cDC(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "receive a error message. code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mipush_error_code"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", msg= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mipush_error_msg"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v0

    :cond_13
    :try_start_5
    invoke-static {v1, v2}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V
    :try_end_5
    .catch Lorg/apache/thrift/TException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_5

    :catch_2
    move-exception v2

    goto :goto_5

    :cond_14
    const-string/jumbo v0, "mipush_payload"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_16

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;-><init>()V

    :try_start_6
    invoke-static {v1, v0}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/v;->cYP:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v2

    invoke-static {v1}, Lcom/xiaomi/push/service/h;->cMS(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Z

    move-result v3

    if-nez v3, :cond_17

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDL()Z

    move-result v3

    if-eqz v3, :cond_18

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDL()Z

    move-result v3

    if-nez v3, :cond_19

    :cond_15
    invoke-direct {p0, v1, v0}, Lcom/xiaomi/mipush/sdk/v;->cED(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;[B)Lcom/xiaomi/mipush/sdk/PushMessageHandler$PushMessageInterface;
    :try_end_6
    .catch Lorg/apache/thrift/TException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    move-result-object v0

    return-object v0

    :cond_16
    const-string/jumbo v0, "message arrived: receiving an empty message, drop"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v8

    :cond_17
    :try_start_7
    const-string/jumbo v0, "message arrived: receive ignore reg message, ignore!"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V
    :try_end_7
    .catch Lorg/apache/thrift/TException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_1

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_18
    :try_start_8
    const-string/jumbo v0, "message arrived: receive message without registration. need unregister or re-register!"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V
    :try_end_8
    .catch Lorg/apache/thrift/TException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_19
    :try_start_9
    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDN()Z

    move-result v2

    if-eqz v2, :cond_15

    const-string/jumbo v0, "message arrived: app info is invalidated"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V
    :try_end_9
    .catch Lorg/apache/thrift/TException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_1
.end method

.method public cEy(Ljava/util/TimeZone;Ljava/util/TimeZone;Ljava/util/List;)Ljava/util/List;
    .locals 12

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    invoke-virtual {p2}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x3e8

    div-int/lit8 v0, v0, 0x3c

    int-to-long v2, v0

    const/4 v0, 0x0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v0, 0x0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v0, 0x1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const/4 v0, 0x1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v10, 0x3c

    mul-long/2addr v4, v10

    add-long/2addr v4, v6

    sub-long/2addr v4, v2

    const-wide/16 v6, 0x5a0

    add-long/2addr v4, v6

    const-wide/16 v6, 0x5a0

    rem-long/2addr v4, v6

    const-wide/16 v6, 0x3c

    mul-long/2addr v6, v8

    add-long/2addr v0, v6

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x5a0

    add-long/2addr v0, v2

    const-wide/16 v2, 0x5a0

    rem-long/2addr v0, v2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-wide/16 v6, 0x3c

    div-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v3, v7

    const-wide/16 v6, 0x3c

    rem-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    const-string/jumbo v4, "%1$02d:%2$02d"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-wide/16 v4, 0x3c

    div-long v4, v0, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-wide/16 v4, 0x3c

    rem-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v3, v1

    const-string/jumbo v0, "%1$02d:%2$02d"

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v2

    :cond_0
    return-object p3
.end method
