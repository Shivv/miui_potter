.class Lcom/xiaomi/mipush/sdk/S;
.super Landroid/os/Handler;
.source "PushServiceClient.java"


# instance fields
.field final synthetic cZY:Lcom/xiaomi/mipush/sdk/z;


# direct methods
.method constructor <init>(Lcom/xiaomi/mipush/sdk/z;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 6

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v1, p1, Landroid/os/Message;->arg1:I

    const-class v2, Lcom/xiaomi/mipush/sdk/m;

    const-class v3, Lcom/xiaomi/mipush/sdk/m;

    monitor-enter v3

    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/xiaomi/mipush/sdk/m;->cDW(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/xiaomi/mipush/sdk/m;->cDY(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0xa

    if-lt v3, v4, :cond_1

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/mipush/sdk/m;->cEc(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3}, Lcom/xiaomi/mipush/sdk/RetryType;->ordinal()I

    move-result v3

    if-eq v3, v1, :cond_7

    :cond_2
    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3}, Lcom/xiaomi/mipush/sdk/RetryType;->ordinal()I

    move-result v3

    if-eq v3, v1, :cond_8

    :cond_3
    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZB:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3}, Lcom/xiaomi/mipush/sdk/RetryType;->ordinal()I

    move-result v3

    if-eq v3, v1, :cond_9

    :cond_4
    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZy:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3}, Lcom/xiaomi/mipush/sdk/RetryType;->ordinal()I

    move-result v3

    if-eq v3, v1, :cond_a

    :cond_5
    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZA:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3}, Lcom/xiaomi/mipush/sdk/RetryType;->ordinal()I

    move-result v3

    if-eq v3, v1, :cond_b

    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/mipush/sdk/m;->cDZ(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    const-string/jumbo v4, "syncing"

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v3

    sget-object v5, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3, v5}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v1, v0, v3, v4, v5}, Lcom/xiaomi/mipush/sdk/z;->cFG(Lcom/xiaomi/mipush/sdk/z;Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V

    goto :goto_2

    :cond_8
    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    const-string/jumbo v4, "syncing"

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v3

    sget-object v5, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3, v5}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v1, v0, v3, v4, v5}, Lcom/xiaomi/mipush/sdk/z;->cFG(Lcom/xiaomi/mipush/sdk/z;Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V

    goto :goto_2

    :cond_9
    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    const-string/jumbo v4, "syncing"

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v3

    sget-object v5, Lcom/xiaomi/mipush/sdk/RetryType;->cZB:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3, v5}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZB:Lcom/xiaomi/mipush/sdk/RetryType;

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v4}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v4, v5}, Lcom/xiaomi/mipush/sdk/h;->cDk(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Ljava/util/HashMap;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v1, v0, v3, v5, v4}, Lcom/xiaomi/mipush/sdk/z;->cFG(Lcom/xiaomi/mipush/sdk/z;Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V

    goto/16 :goto_2

    :cond_a
    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    const-string/jumbo v4, "syncing"

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v3

    sget-object v5, Lcom/xiaomi/mipush/sdk/RetryType;->cZy:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v3, v5}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZy:Lcom/xiaomi/mipush/sdk/RetryType;

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v4}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v4, v5}, Lcom/xiaomi/mipush/sdk/h;->cDk(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Ljava/util/HashMap;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v1, v0, v3, v5, v4}, Lcom/xiaomi/mipush/sdk/z;->cFG(Lcom/xiaomi/mipush/sdk/z;Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V

    goto/16 :goto_2

    :cond_b
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    const-string/jumbo v3, "syncing"

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v1

    sget-object v4, Lcom/xiaomi/mipush/sdk/RetryType;->cZA:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v1, v4}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    sget-object v3, Lcom/xiaomi/mipush/sdk/RetryType;->cZA:Lcom/xiaomi/mipush/sdk/RetryType;

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/S;->cZY:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v4}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v4, v5}, Lcom/xiaomi/mipush/sdk/h;->cDk(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Ljava/util/HashMap;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v1, v0, v3, v5, v4}, Lcom/xiaomi/mipush/sdk/z;->cFG(Lcom/xiaomi/mipush/sdk/z;Ljava/lang/String;Lcom/xiaomi/mipush/sdk/RetryType;ZLjava/util/HashMap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
    .end packed-switch
.end method
