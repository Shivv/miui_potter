.class public Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;
.super Ljava/lang/Object;
.source "XmPushActionAckMessage.java"

# interfaces
.implements Lorg/apache/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final dnA:Ljava/util/Map;

.field private static final dnB:Lorg/apache/thrift/protocol/i;

.field private static final dnC:Lorg/apache/thrift/protocol/i;

.field private static final dnD:Lorg/apache/thrift/protocol/i;

.field private static final dnE:Lorg/apache/thrift/protocol/i;

.field private static final dnF:Lorg/apache/thrift/protocol/i;

.field private static final dnG:Lorg/apache/thrift/protocol/i;

.field private static final dnH:Lorg/apache/thrift/protocol/i;

.field private static final dnI:Lorg/apache/thrift/protocol/i;

.field private static final dnJ:Lorg/apache/thrift/protocol/i;

.field private static final dnK:Lorg/apache/thrift/protocol/i;

.field private static final dnL:Lorg/apache/thrift/protocol/i;

.field private static final dnM:Lorg/apache/thrift/protocol/i;

.field private static final dnN:Lorg/apache/thrift/protocol/i;

.field private static final dns:Lorg/apache/thrift/protocol/i;

.field private static final dnt:Lorg/apache/thrift/protocol/c;

.field private static final dnu:Lorg/apache/thrift/protocol/i;

.field private static final dnv:Lorg/apache/thrift/protocol/i;

.field private static final dnw:Lorg/apache/thrift/protocol/i;

.field private static final dnx:Lorg/apache/thrift/protocol/i;

.field private static final dny:Lorg/apache/thrift/protocol/i;

.field private static final dnz:Lorg/apache/thrift/protocol/i;


# instance fields
.field private __isset_bit_vector:Ljava/util/BitSet;

.field public aliasName:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public callbackUrl:Ljava/lang/String;

.field public category:Ljava/lang/String;

.field public debug:Ljava/lang/String;

.field public deviceId:Ljava/lang/String;

.field public deviceStatus:S

.field public extra:Ljava/util/Map;

.field public geoMsgStatus:S

.field public id:Ljava/lang/String;

.field public imeiMd5:Ljava/lang/String;

.field public isOnline:Z

.field public messageTs:J

.field public packageName:Ljava/lang/String;

.field public passThrough:I

.field public regId:Ljava/lang/String;

.field public request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

.field public target:Lcom/xiaomi/xmpush/thrift/Target;

.field public topic:Ljava/lang/String;

.field public userAccount:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x1

    const/16 v8, 0xc

    const/4 v5, 0x6

    const/4 v7, 0x2

    const/16 v6, 0xb

    new-instance v0, Lorg/apache/thrift/protocol/c;

    const-string/jumbo v1, "XmPushActionAckMessage"

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnt:Lorg/apache/thrift/protocol/c;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "debug"

    invoke-direct {v0, v1, v6, v9}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnz:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "target"

    invoke-direct {v0, v1, v8, v7}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnC:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "id"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dns:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "appId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnx:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "messageTs"

    const/16 v2, 0xa

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnD:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "topic"

    invoke-direct {v0, v1, v6, v5}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnH:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "aliasName"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnL:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "request"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnJ:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "packageName"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnE:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "category"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnK:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "isOnline"

    invoke-direct {v0, v1, v7, v6}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnF:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "regId"

    invoke-direct {v0, v1, v6, v8}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnv:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "callbackUrl"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnu:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "userAccount"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnB:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "deviceStatus"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnG:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "geoMsgStatus"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnw:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "imeiMd5"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnM:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "deviceId"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnI:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "passThrough"

    const/16 v2, 0x8

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnN:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "extra"

    const/16 v2, 0xd

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dny:Lorg/apache/thrift/protocol/i;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyG:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "debug"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyF:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/StructMetaData;

    const-class v4, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-direct {v3, v8, v4}, Lorg/apache/thrift/meta_data/StructMetaData;-><init>(BLjava/lang/Class;)V

    const-string/jumbo v4, "target"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyu:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "id"

    invoke-direct {v2, v4, v9, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyA:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "appId"

    invoke-direct {v2, v4, v9, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyI:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "messageTs"

    invoke-direct {v2, v4, v9, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dys:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "topic"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyD:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "aliasName"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyw:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/StructMetaData;

    const-class v4, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-direct {v3, v8, v4}, Lorg/apache/thrift/meta_data/StructMetaData;-><init>(BLjava/lang/Class;)V

    const-string/jumbo v4, "request"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyv:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "packageName"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyt:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "category"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyB:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v7}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "isOnline"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyp:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "regId"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyx:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "callbackUrl"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyC:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "userAccount"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyJ:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "deviceStatus"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyy:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "geoMsgStatus"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyz:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "imeiMd5"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyo:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "deviceId"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyH:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    const/16 v4, 0x8

    invoke-direct {v3, v4}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "passThrough"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyE:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/MapMetaData;

    new-instance v4, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v4, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    new-instance v5, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v5, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const/16 v6, 0xd

    invoke-direct {v3, v6, v4, v5}, Lorg/apache/thrift/meta_data/MapMetaData;-><init>(BLorg/apache/thrift/meta_data/FieldValueMetaData;Lorg/apache/thrift/meta_data/FieldValueMetaData;)V

    const-string/jumbo v4, "extra"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnA:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnA:Ljava/util/Map;

    const-class v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-static {v1, v0}, Lorg/apache/thrift/meta_data/FieldMetaData;->esm(Ljava/lang/Class;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->isOnline:Z

    return-void
.end method


# virtual methods
.method public cTq(Lorg/apache/thrift/protocol/a;)V
    .locals 10

    const/4 v9, 0x6

    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v7, 0x1

    const/16 v6, 0xb

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v2, :cond_15

    iget-short v2, v0, Lorg/apache/thrift/protocol/i;->eXX:S

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    :goto_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_0

    :pswitch_1
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xc

    if-eq v2, v3, :cond_1

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/Target;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/Target;->cTq(Lorg/apache/thrift/protocol/a;)V

    goto :goto_1

    :pswitch_3
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_2

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_3

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    goto :goto_1

    :pswitch_5
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xa

    if-eq v2, v3, :cond_4

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erw()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->messageTs:J

    invoke-virtual {p0, v7}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWU(Z)V

    goto :goto_1

    :pswitch_6
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_5

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    goto :goto_1

    :pswitch_7
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_6

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_8
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xc

    if-eq v2, v3, :cond_7

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_7
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->cTq(Lorg/apache/thrift/protocol/a;)V

    goto/16 :goto_1

    :pswitch_9
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_8

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_a
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_9

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_b
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v8, :cond_a

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erM()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->isOnline:Z

    invoke-virtual {p0, v7}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXh(Z)V

    goto/16 :goto_1

    :pswitch_c
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_b

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_b
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_d
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_c

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_c
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_e
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_d

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_d
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_f
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v9, :cond_e

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_e
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erz()S

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceStatus:S

    invoke-virtual {p0, v7}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWV(Z)V

    goto/16 :goto_1

    :pswitch_10
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v9, :cond_f

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_f
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erz()S

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->geoMsgStatus:S

    invoke-virtual {p0, v7}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXl(Z)V

    goto/16 :goto_1

    :pswitch_11
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_10

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_10
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_12
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_11

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_11
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_13
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0x8

    if-eq v2, v3, :cond_12

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_12
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->passThrough:I

    invoke-virtual {p0, v7}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWW(Z)V

    goto/16 :goto_1

    :pswitch_14
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xd

    if-eq v2, v3, :cond_13

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_13
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erV()Lorg/apache/thrift/protocol/d;

    move-result-object v2

    new-instance v0, Ljava/util/HashMap;

    iget v3, v2, Lorg/apache/thrift/protocol/d;->eXG:I

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    move v0, v1

    :goto_2
    iget v3, v2, Lorg/apache/thrift/protocol/d;->eXG:I

    if-lt v0, v3, :cond_14

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erP()V

    goto/16 :goto_1

    :cond_14
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_15
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erU()V

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXg()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXd()V

    return-void

    :cond_16
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'messageTs\' was not found in serialized data! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method public cTu(Lorg/apache/thrift/protocol/a;)V
    .locals 3

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXd()V

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnt:Lorg/apache/thrift/protocol/c;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erx(Lorg/apache/thrift/protocol/c;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    if-nez v0, :cond_d

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    if-nez v0, :cond_e

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    if-nez v0, :cond_f

    :goto_2
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    if-nez v0, :cond_10

    :goto_3
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnD:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->messageTs:J

    invoke-virtual {p1, v0, v1}, Lorg/apache/thrift/protocol/a;->erF(J)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    if-nez v0, :cond_11

    :cond_2
    :goto_4
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    if-nez v0, :cond_12

    :cond_3
    :goto_5
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    if-nez v0, :cond_13

    :cond_4
    :goto_6
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    if-nez v0, :cond_14

    :cond_5
    :goto_7
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    if-nez v0, :cond_15

    :cond_6
    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWZ()Z

    move-result v0

    if-nez v0, :cond_16

    :goto_9
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    if-nez v0, :cond_17

    :cond_7
    :goto_a
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    if-nez v0, :cond_18

    :cond_8
    :goto_b
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    if-nez v0, :cond_19

    :cond_9
    :goto_c
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWS()Z

    move-result v0

    if-nez v0, :cond_1a

    :goto_d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXk()Z

    move-result v0

    if-nez v0, :cond_1b

    :goto_e
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    if-nez v0, :cond_1c

    :cond_a
    :goto_f
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    if-nez v0, :cond_1d

    :cond_b
    :goto_10
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWE()Z

    move-result v0

    if-nez v0, :cond_1e

    :goto_11
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    if-nez v0, :cond_1f

    :cond_c
    :goto_12
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erQ()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erL()V

    return-void

    :cond_d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWH()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnz:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_0

    :cond_e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWG()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnC:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/Target;->cTu(Lorg/apache/thrift/protocol/a;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_1

    :cond_f
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dns:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_2

    :cond_10
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnx:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_3

    :cond_11
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWI()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnH:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_4

    :cond_12
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWQ()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnL:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_5

    :cond_13
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXb()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnJ:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->cTu(Lorg/apache/thrift/protocol/a;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_6

    :cond_14
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWK()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnE:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_7

    :cond_15
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWP()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnK:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_8

    :cond_16
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnF:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->isOnline:Z

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->eru(Z)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_9

    :cond_17
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXa()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnv:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_a

    :cond_18
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWO()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnu:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_b

    :cond_19
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWJ()Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnB:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_c

    :cond_1a
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnG:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceStatus:S

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erE(S)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_d

    :cond_1b
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnw:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->geoMsgStatus:S

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erE(S)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_e

    :cond_1c
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWM()Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnM:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_f

    :cond_1d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXe()Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnI:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_10

    :cond_1e
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dnN:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->passThrough:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_11

    :cond_1f
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWR()Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->dny:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    new-instance v0, Lorg/apache/thrift/protocol/d;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v2, v2, v1}, Lorg/apache/thrift/protocol/d;-><init>(BBI)V

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->esc(Lorg/apache/thrift/protocol/d;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_20

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erD()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_12

    :cond_20
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    goto :goto_13
.end method

.method public cWD(S)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;
    .locals 1

    int-to-short v0, p1

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->geoMsgStatus:S

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXl(Z)V

    return-object p0
.end method

.method public cWE()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cWF(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    return-object p0
.end method

.method public cWG()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWH()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWI()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWJ()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWK()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWL(Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;)I
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWH()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWH()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWH()Z

    move-result v0

    if-nez v0, :cond_16

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWG()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWG()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWG()Z

    move-result v0

    if-nez v0, :cond_18

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXc()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXc()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_19

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXc()Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWN()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWN()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1b

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWN()Z

    move-result v0

    if-nez v0, :cond_1c

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXg()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXg()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1d

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXg()Z

    move-result v0

    if-nez v0, :cond_1e

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWI()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWI()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1f

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWI()Z

    move-result v0

    if-nez v0, :cond_20

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWQ()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWQ()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_21

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWQ()Z

    move-result v0

    if-nez v0, :cond_22

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXb()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXb()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_23

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXb()Z

    move-result v0

    if-nez v0, :cond_24

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWK()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWK()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_25

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWK()Z

    move-result v0

    if-nez v0, :cond_26

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWP()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWP()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_27

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWP()Z

    move-result v0

    if-nez v0, :cond_28

    :cond_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWZ()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWZ()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_29

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWZ()Z

    move-result v0

    if-nez v0, :cond_2a

    :cond_a
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXa()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXa()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_2b

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXa()Z

    move-result v0

    if-nez v0, :cond_2c

    :cond_b
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWO()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWO()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_2d

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWO()Z

    move-result v0

    if-nez v0, :cond_2e

    :cond_c
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWJ()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWJ()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_2f

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWJ()Z

    move-result v0

    if-nez v0, :cond_30

    :cond_d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWS()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWS()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_31

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWS()Z

    move-result v0

    if-nez v0, :cond_32

    :cond_e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXk()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXk()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_33

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXk()Z

    move-result v0

    if-nez v0, :cond_34

    :cond_f
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWM()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWM()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_35

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWM()Z

    move-result v0

    if-nez v0, :cond_36

    :cond_10
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXe()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXe()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_37

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXe()Z

    move-result v0

    if-nez v0, :cond_38

    :cond_11
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWE()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWE()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_39

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWE()Z

    move-result v0

    if-nez v0, :cond_3a

    :cond_12
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWR()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWR()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_3b

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWR()Z

    move-result v0

    if-nez v0, :cond_3c

    :cond_13
    return v4

    :cond_14
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_15
    return v0

    :cond_16
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_17
    return v0

    :cond_18
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esE(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_19
    return v0

    :cond_1a
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    :cond_1b
    return v0

    :cond_1c
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    :cond_1d
    return v0

    :cond_1e
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->messageTs:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->messageTs:J

    invoke-static {v0, v1, v2, v3}, Lorg/apache/thrift/b;->esF(JJ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    :cond_1f
    return v0

    :cond_20
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    :cond_21
    return v0

    :cond_22
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    :cond_23
    return v0

    :cond_24
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esE(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    :cond_25
    return v0

    :cond_26
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    :cond_27
    return v0

    :cond_28
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    :cond_29
    return v0

    :cond_2a
    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->isOnline:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->isOnline:Z

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esy(ZZ)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    :cond_2b
    return v0

    :cond_2c
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    :cond_2d
    return v0

    :cond_2e
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    :cond_2f
    return v0

    :cond_30
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    :cond_31
    return v0

    :cond_32
    iget-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceStatus:S

    iget-short v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceStatus:S

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esw(SS)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    :cond_33
    return v0

    :cond_34
    iget-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->geoMsgStatus:S

    iget-short v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->geoMsgStatus:S

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esw(SS)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    :cond_35
    return v0

    :cond_36
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    :cond_37
    return v0

    :cond_38
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    :cond_39
    return v0

    :cond_3a
    iget v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->passThrough:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->passThrough:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    :cond_3b
    return v0

    :cond_3c
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esx(Ljava/util/Map;Ljava/util/Map;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0
.end method

.method public cWM()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWN()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWO()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWP()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWQ()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWR()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cWS()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cWT(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    return-object p0
.end method

.method public cWU(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cWV(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cWW(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cWX(Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWH()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWH()Z

    move-result v1

    if-eqz v0, :cond_3

    :cond_0
    if-nez v0, :cond_7

    :cond_1
    return v4

    :cond_2
    return v4

    :cond_3
    if-nez v1, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWG()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWG()Z

    move-result v1

    if-eqz v0, :cond_8

    :cond_5
    if-nez v0, :cond_c

    :cond_6
    return v4

    :cond_7
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    return v4

    :cond_8
    if-nez v1, :cond_5

    :cond_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXc()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXc()Z

    move-result v1

    if-eqz v0, :cond_d

    :cond_a
    if-nez v0, :cond_11

    :cond_b
    return v4

    :cond_c
    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/Target;->dae(Lcom/xiaomi/xmpush/thrift/Target;)Z

    move-result v0

    if-nez v0, :cond_9

    return v4

    :cond_d
    if-nez v1, :cond_a

    :cond_e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWN()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWN()Z

    move-result v1

    if-eqz v0, :cond_12

    :cond_f
    if-nez v0, :cond_14

    :cond_10
    return v4

    :cond_11
    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    return v4

    :cond_12
    if-nez v1, :cond_f

    :cond_13
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->messageTs:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->messageTs:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_15

    return v4

    :cond_14
    if-eqz v1, :cond_10

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    return v4

    :cond_15
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWI()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWI()Z

    move-result v1

    if-eqz v0, :cond_18

    :cond_16
    if-nez v0, :cond_1c

    :cond_17
    return v4

    :cond_18
    if-nez v1, :cond_16

    :cond_19
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWQ()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWQ()Z

    move-result v1

    if-eqz v0, :cond_1d

    :cond_1a
    if-nez v0, :cond_21

    :cond_1b
    return v4

    :cond_1c
    if-eqz v1, :cond_17

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    return v4

    :cond_1d
    if-nez v1, :cond_1a

    :cond_1e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXb()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXb()Z

    move-result v1

    if-eqz v0, :cond_22

    :cond_1f
    if-nez v0, :cond_26

    :cond_20
    return v4

    :cond_21
    if-eqz v1, :cond_1b

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    return v4

    :cond_22
    if-nez v1, :cond_1f

    :cond_23
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWK()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWK()Z

    move-result v1

    if-eqz v0, :cond_27

    :cond_24
    if-nez v0, :cond_2b

    :cond_25
    return v4

    :cond_26
    if-eqz v1, :cond_20

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;->dfJ(Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;)Z

    move-result v0

    if-nez v0, :cond_23

    return v4

    :cond_27
    if-nez v1, :cond_24

    :cond_28
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWP()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWP()Z

    move-result v1

    if-eqz v0, :cond_2c

    :cond_29
    if-nez v0, :cond_30

    :cond_2a
    return v4

    :cond_2b
    if-eqz v1, :cond_25

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    return v4

    :cond_2c
    if-nez v1, :cond_29

    :cond_2d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWZ()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWZ()Z

    move-result v1

    if-eqz v0, :cond_31

    :cond_2e
    if-nez v0, :cond_35

    :cond_2f
    return v4

    :cond_30
    if-eqz v1, :cond_2a

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2d

    return v4

    :cond_31
    if-nez v1, :cond_2e

    :cond_32
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXa()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXa()Z

    move-result v1

    if-eqz v0, :cond_36

    :cond_33
    if-nez v0, :cond_3a

    :cond_34
    return v4

    :cond_35
    if-eqz v1, :cond_2f

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->isOnline:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->isOnline:Z

    if-eq v0, v1, :cond_32

    return v4

    :cond_36
    if-nez v1, :cond_33

    :cond_37
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWO()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWO()Z

    move-result v1

    if-eqz v0, :cond_3b

    :cond_38
    if-nez v0, :cond_3f

    :cond_39
    return v4

    :cond_3a
    if-eqz v1, :cond_34

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_37

    return v4

    :cond_3b
    if-nez v1, :cond_38

    :cond_3c
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWJ()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWJ()Z

    move-result v1

    if-eqz v0, :cond_40

    :cond_3d
    if-nez v0, :cond_44

    :cond_3e
    return v4

    :cond_3f
    if-eqz v1, :cond_39

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3c

    return v4

    :cond_40
    if-nez v1, :cond_3d

    :cond_41
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWS()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWS()Z

    move-result v1

    if-eqz v0, :cond_45

    :cond_42
    if-nez v0, :cond_49

    :cond_43
    return v4

    :cond_44
    if-eqz v1, :cond_3e

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_41

    return v4

    :cond_45
    if-nez v1, :cond_42

    :cond_46
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXk()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXk()Z

    move-result v1

    if-eqz v0, :cond_4a

    :cond_47
    if-nez v0, :cond_4e

    :cond_48
    return v4

    :cond_49
    if-eqz v1, :cond_43

    iget-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceStatus:S

    iget-short v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceStatus:S

    if-eq v0, v1, :cond_46

    return v4

    :cond_4a
    if-nez v1, :cond_47

    :cond_4b
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWM()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWM()Z

    move-result v1

    if-eqz v0, :cond_4f

    :cond_4c
    if-nez v0, :cond_53

    :cond_4d
    return v4

    :cond_4e
    if-eqz v1, :cond_48

    iget-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->geoMsgStatus:S

    iget-short v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->geoMsgStatus:S

    if-eq v0, v1, :cond_4b

    return v4

    :cond_4f
    if-nez v1, :cond_4c

    :cond_50
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXe()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXe()Z

    move-result v1

    if-eqz v0, :cond_54

    :cond_51
    if-nez v0, :cond_58

    :cond_52
    return v4

    :cond_53
    if-eqz v1, :cond_4d

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_50

    return v4

    :cond_54
    if-nez v1, :cond_51

    :cond_55
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWE()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWE()Z

    move-result v1

    if-eqz v0, :cond_59

    :cond_56
    if-nez v0, :cond_5d

    :cond_57
    return v4

    :cond_58
    if-eqz v1, :cond_52

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_55

    return v4

    :cond_59
    if-nez v1, :cond_56

    :cond_5a
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWR()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWR()Z

    move-result v1

    if-eqz v0, :cond_5e

    :cond_5b
    if-nez v0, :cond_60

    :cond_5c
    return v4

    :cond_5d
    if-eqz v1, :cond_57

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->passThrough:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->passThrough:I

    if-eq v0, v1, :cond_5a

    return v4

    :cond_5e
    if-nez v1, :cond_5b

    :cond_5f
    return v5

    :cond_60
    if-eqz v1, :cond_5c

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5f

    return v4
.end method

.method public cWY(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    return-object p0
.end method

.method public cWZ()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXa()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cXb()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cXc()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cXd()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    if-eqz v0, :cond_1

    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'id\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'appId\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cXe()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cXf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    return-object p0
.end method

.method public cXg()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXh(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cXi(S)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;
    .locals 1

    int-to-short v0, p1

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceStatus:S

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWV(Z)V

    return-object p0
.end method

.method public cXj(J)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;
    .locals 1

    iput-wide p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->messageTs:J

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWU(Z)V

    return-object p0
.end method

.method public cXk()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXl(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWL(Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    if-nez v0, :cond_1

    return v1

    :cond_0
    return v1

    :cond_1
    check-cast p1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWX(Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "XmPushActionAckMessage("

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWH()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWG()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    if-eqz v0, :cond_5

    :goto_2
    const-string/jumbo v0, "id:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->id:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "appId:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->appId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "messageTs:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->messageTs:J

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWI()Z

    move-result v0

    if-nez v0, :cond_8

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWQ()Z

    move-result v0

    if-nez v0, :cond_a

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXb()Z

    move-result v0

    if-nez v0, :cond_c

    :goto_7
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWK()Z

    move-result v0

    if-nez v0, :cond_e

    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWP()Z

    move-result v0

    if-nez v0, :cond_10

    :goto_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWZ()Z

    move-result v0

    if-nez v0, :cond_12

    :goto_a
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXa()Z

    move-result v0

    if-nez v0, :cond_13

    :goto_b
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWO()Z

    move-result v0

    if-nez v0, :cond_15

    :goto_c
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWJ()Z

    move-result v0

    if-nez v0, :cond_17

    :goto_d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWS()Z

    move-result v0

    if-nez v0, :cond_19

    :goto_e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXk()Z

    move-result v0

    if-nez v0, :cond_1a

    :goto_f
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWM()Z

    move-result v0

    if-nez v0, :cond_1b

    :goto_10
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cXe()Z

    move-result v0

    if-nez v0, :cond_1d

    :goto_11
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWE()Z

    move-result v0

    if-nez v0, :cond_1f

    :goto_12
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->cWR()Z

    move-result v0

    if-nez v0, :cond_20

    :goto_13
    const-string/jumbo v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "debug:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->debug:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_14
    move v0, v1

    goto/16 :goto_0

    :cond_1
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_14

    :cond_2
    if-eqz v0, :cond_3

    :goto_15
    const-string/jumbo v0, "target:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_16
    move v0, v1

    goto/16 :goto_1

    :cond_3
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_15

    :cond_4
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_16

    :cond_5
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_6
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_7
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    :cond_8
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "topic:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->topic:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_9
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_a
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "aliasName:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->aliasName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_b
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_c
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "request:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->request:Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_d
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_e
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "packageName:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_f
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_10
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "category:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->category:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_11
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_12
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "isOnline:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->isOnline:Z

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    :cond_13
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "regId:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->regId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    :cond_14
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    :cond_15
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "callbackUrl:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->callbackUrl:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    :cond_16
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    :cond_17
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "userAccount:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->userAccount:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    :cond_18
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    :cond_19
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "deviceStatus:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceStatus:S

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    :cond_1a
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "geoMsgStatus:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->geoMsgStatus:S

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_f

    :cond_1b
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "imeiMd5:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->imeiMd5:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_10

    :cond_1c
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_10

    :cond_1d
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "deviceId:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    :cond_1e
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    :cond_1f
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "passThrough:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->passThrough:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_12

    :cond_20
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "extra:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;->extra:Ljava/util/Map;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_13

    :cond_21
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_13
.end method
