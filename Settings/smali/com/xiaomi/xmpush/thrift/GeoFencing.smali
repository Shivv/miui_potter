.class public Lcom/xiaomi/xmpush/thrift/GeoFencing;
.super Ljava/lang/Object;
.source "GeoFencing.java"

# interfaces
.implements Lorg/apache/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final dvA:Lorg/apache/thrift/protocol/i;

.field private static final dvB:Lorg/apache/thrift/protocol/i;

.field private static final dvC:Lorg/apache/thrift/protocol/i;

.field private static final dvD:Lorg/apache/thrift/protocol/i;

.field public static final dvE:Ljava/util/Map;

.field private static final dvF:Lorg/apache/thrift/protocol/i;

.field private static final dvG:Lorg/apache/thrift/protocol/i;

.field private static final dvH:Lorg/apache/thrift/protocol/i;

.field private static final dvI:Lorg/apache/thrift/protocol/i;

.field private static final dvx:Lorg/apache/thrift/protocol/i;

.field private static final dvy:Lorg/apache/thrift/protocol/c;

.field private static final dvz:Lorg/apache/thrift/protocol/i;


# instance fields
.field private __isset_bit_vector:Ljava/util/BitSet;

.field public appId:J

.field public circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

.field public circleRadius:D

.field public coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

.field public createTime:J

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public packageName:Ljava/lang/String;

.field public polygonPoints:Ljava/util/List;

.field public type:Lcom/xiaomi/xmpush/thrift/GeoType;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/16 v6, 0xa

    const/16 v5, 0xb

    const/4 v7, 0x1

    new-instance v0, Lorg/apache/thrift/protocol/c;

    const-string/jumbo v1, "GeoFencing"

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvy:Lorg/apache/thrift/protocol/c;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "id"

    invoke-direct {v0, v1, v5, v7}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvx:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "name"

    invoke-direct {v0, v1, v5, v8}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvF:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "appId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvz:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "packageName"

    invoke-direct {v0, v1, v5, v9}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvB:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "createTime"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvD:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "type"

    const/16 v2, 0x8

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvH:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "circleCenter"

    const/16 v2, 0xc

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvA:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "circleRadius"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v9, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvC:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "polygonPoints"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v6}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvI:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "coordinateProvider"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v5}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvG:Lorg/apache/thrift/protocol/i;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvL:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "id"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvP:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "name"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvJ:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "appId"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvT:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "packageName"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvR:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "createTime"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvO:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/EnumMetaData;

    const/16 v4, 0x10

    const-class v5, Lcom/xiaomi/xmpush/thrift/GeoType;

    invoke-direct {v3, v4, v5}, Lorg/apache/thrift/meta_data/EnumMetaData;-><init>(BLjava/lang/Class;)V

    const-string/jumbo v4, "type"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvM:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/StructMetaData;

    const/16 v4, 0xc

    const-class v5, Lcom/xiaomi/xmpush/thrift/Location;

    invoke-direct {v3, v4, v5}, Lorg/apache/thrift/meta_data/StructMetaData;-><init>(BLjava/lang/Class;)V

    const-string/jumbo v4, "circleCenter"

    invoke-direct {v2, v4, v8, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvU:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v9}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "circleRadius"

    invoke-direct {v2, v4, v8, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvK:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/ListMetaData;

    new-instance v4, Lorg/apache/thrift/meta_data/StructMetaData;

    const/16 v5, 0xc

    const-class v6, Lcom/xiaomi/xmpush/thrift/Location;

    invoke-direct {v4, v5, v6}, Lorg/apache/thrift/meta_data/StructMetaData;-><init>(BLjava/lang/Class;)V

    const/16 v5, 0xf

    invoke-direct {v3, v5, v4}, Lorg/apache/thrift/meta_data/ListMetaData;-><init>(BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    const-string/jumbo v4, "polygonPoints"

    invoke-direct {v2, v4, v8, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;->dvS:Lcom/xiaomi/xmpush/thrift/GeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/EnumMetaData;

    const/16 v4, 0x10

    const-class v5, Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    invoke-direct {v3, v4, v5}, Lorg/apache/thrift/meta_data/EnumMetaData;-><init>(BLjava/lang/Class;)V

    const-string/jumbo v4, "coordinateProvider"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvE:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvE:Ljava/util/Map;

    const-class v1, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    invoke-static {v1, v0}, Lorg/apache/thrift/meta_data/FieldMetaData;->esm(Ljava/lang/Class;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->__isset_bit_vector:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public cTq(Lorg/apache/thrift/protocol/a;)V
    .locals 9

    const/16 v8, 0xa

    const/16 v7, 0x8

    const/16 v6, 0xb

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v2, :cond_b

    iget-short v2, v0, Lorg/apache/thrift/protocol/i;->eXX:S

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    :goto_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_0

    :pswitch_1
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_1

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v8, :cond_2

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erw()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    invoke-virtual {p0, v5}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcr(Z)V

    goto :goto_1

    :pswitch_4
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v6, :cond_3

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    goto :goto_1

    :pswitch_5
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v8, :cond_4

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erw()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    invoke-virtual {p0, v5}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcN(Z)V

    goto :goto_1

    :pswitch_6
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v7, :cond_5

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    invoke-static {v0}, Lcom/xiaomi/xmpush/thrift/GeoType;->daK(I)Lcom/xiaomi/xmpush/thrift/GeoType;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    goto :goto_1

    :pswitch_7
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xc

    if-eq v2, v3, :cond_6

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_6
    new-instance v0, Lcom/xiaomi/xmpush/thrift/Location;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/Location;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/Location;->cTq(Lorg/apache/thrift/protocol/a;)V

    goto/16 :goto_1

    :pswitch_8
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/4 v3, 0x4

    if-eq v2, v3, :cond_7

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erG()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    invoke-virtual {p0, v5}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcO(Z)V

    goto/16 :goto_1

    :pswitch_9
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xf

    if-eq v2, v3, :cond_8

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erX()Lorg/apache/thrift/protocol/g;

    move-result-object v2

    new-instance v0, Ljava/util/ArrayList;

    iget v3, v2, Lorg/apache/thrift/protocol/g;->eXU:I

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    move v0, v1

    :goto_2
    iget v3, v2, Lorg/apache/thrift/protocol/g;->eXU:I

    if-lt v0, v3, :cond_9

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erv()V

    goto/16 :goto_1

    :cond_9
    new-instance v3, Lcom/xiaomi/xmpush/thrift/Location;

    invoke-direct {v3}, Lcom/xiaomi/xmpush/thrift/Location;-><init>()V

    invoke-virtual {v3, p1}, Lcom/xiaomi/xmpush/thrift/Location;->cTq(Lorg/apache/thrift/protocol/a;)V

    iget-object v4, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_a
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v7, :cond_a

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    invoke-static {v0}, Lcom/xiaomi/xmpush/thrift/CoordinateProvider;->dff(I)Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    goto/16 :goto_1

    :cond_b
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erU()V

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcp()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcy()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcH()V

    return-void

    :cond_c
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'appId\' was not found in serialized data! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'createTime\' was not found in serialized data! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public cTu(Lorg/apache/thrift/protocol/a;)V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcH()V

    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvy:Lorg/apache/thrift/protocol/c;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erx(Lorg/apache/thrift/protocol/c;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    if-nez v0, :cond_2

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    if-nez v0, :cond_3

    :goto_1
    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvz:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    invoke-virtual {p1, v0, v1}, Lorg/apache/thrift/protocol/a;->erF(J)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    if-nez v0, :cond_4

    :goto_2
    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvD:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    invoke-virtual {p1, v0, v1}, Lorg/apache/thrift/protocol/a;->erF(J)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    if-nez v0, :cond_5

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    if-nez v0, :cond_6

    :cond_0
    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dci()Z

    move-result v0

    if-nez v0, :cond_7

    :goto_5
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    if-nez v0, :cond_8

    :cond_1
    :goto_6
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    if-nez v0, :cond_a

    :goto_7
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erQ()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erL()V

    return-void

    :cond_2
    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvx:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvF:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvB:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvH:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/GeoType;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dch()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvA:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/Location;->cTu(Lorg/apache/thrift/protocol/a;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_4

    :cond_7
    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvC:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    invoke-virtual {p1, v0, v1}, Lorg/apache/thrift/protocol/a;->erH(D)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_5

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcx()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvI:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    new-instance v0, Lorg/apache/thrift/protocol/g;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xc

    invoke-direct {v0, v2, v1}, Lorg/apache/thrift/protocol/g;-><init>(BI)V

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->esd(Lorg/apache/thrift/protocol/g;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->ese()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_6

    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/Location;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/Location;->cTu(Lorg/apache/thrift/protocol/a;)V

    goto :goto_8

    :cond_a
    sget-object v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dvG:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/CoordinateProvider;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_7
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcL(Lcom/xiaomi/xmpush/thrift/GeoFencing;)I

    move-result v0

    return v0
.end method

.method public dcA(Lcom/xiaomi/xmpush/thrift/GeoFencing;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcF()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcF()Z

    move-result v1

    if-eqz v0, :cond_3

    :cond_0
    if-nez v0, :cond_7

    :cond_1
    return v4

    :cond_2
    return v4

    :cond_3
    if-nez v1, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcE()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcE()Z

    move-result v1

    if-eqz v0, :cond_8

    :cond_5
    if-nez v0, :cond_a

    :cond_6
    return v4

    :cond_7
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    return v4

    :cond_8
    if-nez v1, :cond_5

    :cond_9
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_b

    return v4

    :cond_a
    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    return v4

    :cond_b
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcm()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcm()Z

    move-result v1

    if-eqz v0, :cond_e

    :cond_c
    if-nez v0, :cond_10

    :cond_d
    return v4

    :cond_e
    if-nez v1, :cond_c

    :cond_f
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_11

    return v4

    :cond_10
    if-eqz v1, :cond_d

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    return v4

    :cond_11
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcw()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcw()Z

    move-result v1

    if-eqz v0, :cond_14

    :cond_12
    if-nez v0, :cond_18

    :cond_13
    return v4

    :cond_14
    if-nez v1, :cond_12

    :cond_15
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dch()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dch()Z

    move-result v1

    if-eqz v0, :cond_19

    :cond_16
    if-nez v0, :cond_1d

    :cond_17
    return v4

    :cond_18
    if-eqz v1, :cond_13

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/GeoType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    return v4

    :cond_19
    if-nez v1, :cond_16

    :cond_1a
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dci()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dci()Z

    move-result v1

    if-eqz v0, :cond_1e

    :cond_1b
    if-nez v0, :cond_22

    :cond_1c
    return v4

    :cond_1d
    if-eqz v1, :cond_17

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/Location;->deT(Lcom/xiaomi/xmpush/thrift/Location;)Z

    move-result v0

    if-nez v0, :cond_1a

    return v4

    :cond_1e
    if-nez v1, :cond_1b

    :cond_1f
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcx()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcx()Z

    move-result v1

    if-eqz v0, :cond_23

    :cond_20
    if-nez v0, :cond_27

    :cond_21
    return v4

    :cond_22
    if-eqz v1, :cond_1c

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1f

    return v4

    :cond_23
    if-nez v1, :cond_20

    :cond_24
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcn()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcn()Z

    move-result v1

    if-eqz v0, :cond_28

    :cond_25
    if-nez v0, :cond_2a

    :cond_26
    return v4

    :cond_27
    if-eqz v1, :cond_21

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_24

    return v4

    :cond_28
    if-nez v1, :cond_25

    :cond_29
    return v5

    :cond_2a
    if-eqz v1, :cond_26

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/CoordinateProvider;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_29

    return v4
.end method

.method public dcB()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    return-object v0
.end method

.method public dcC()D
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    return-wide v0
.end method

.method public dcD()Lcom/xiaomi/xmpush/thrift/Location;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    return-object v0
.end method

.method public dcE()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dcF()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dcG()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    return-wide v0
.end method

.method public dcH()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    if-eqz v0, :cond_4

    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'id\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'name\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'packageName\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'type\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'coordinateProvider\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dcI(Ljava/util/List;)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    return-object p0
.end method

.method public dcJ(Lcom/xiaomi/xmpush/thrift/Location;)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    return-object p0
.end method

.method public dcK(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    return-object p0
.end method

.method public dcL(Lcom/xiaomi/xmpush/thrift/GeoFencing;)I
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcF()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcF()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcF()Z

    move-result v0

    if-nez v0, :cond_c

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcE()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcE()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcE()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcp()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcp()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcp()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcm()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcm()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcm()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcy()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcy()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcy()Z

    move-result v0

    if-nez v0, :cond_14

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcw()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcw()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcw()Z

    move-result v0

    if-nez v0, :cond_16

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dch()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dch()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dch()Z

    move-result v0

    if-nez v0, :cond_18

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dci()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dci()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_19

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dci()Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcx()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcx()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1b

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcx()Z

    move-result v0

    if-nez v0, :cond_1c

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcn()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcn()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1d

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcn()Z

    move-result v0

    if-nez v0, :cond_1e

    :cond_9
    return v4

    :cond_a
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_b
    return v0

    :cond_c
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_d
    return v0

    :cond_e
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_f
    return v0

    :cond_10
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    invoke-static {v0, v1, v2, v3}, Lorg/apache/thrift/b;->esF(JJ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    :cond_11
    return v0

    :cond_12
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    :cond_13
    return v0

    :cond_14
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    invoke-static {v0, v1, v2, v3}, Lorg/apache/thrift/b;->esF(JJ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    :cond_15
    return v0

    :cond_16
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esE(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    :cond_17
    return v0

    :cond_18
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esE(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    :cond_19
    return v0

    :cond_1a
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    invoke-static {v0, v1, v2, v3}, Lorg/apache/thrift/b;->esC(DD)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    :cond_1b
    return v0

    :cond_1c
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->eso(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    :cond_1d
    return v0

    :cond_1e
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esE(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0
.end method

.method public dcM()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    return-wide v0
.end method

.method public dcN(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public dcO(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public dch()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dci()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public dcj()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    return-object v0
.end method

.method public dck(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    return-object p0
.end method

.method public dcl(J)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 1

    iput-wide p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcN(Z)V

    return-object p0
.end method

.method public dcm()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dcn()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dco(J)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 1

    iput-wide p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcr(Z)V

    return-object p0
.end method

.method public dcp()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public dcq(D)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 1

    iput-wide p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcO(Z)V

    return-object p0
.end method

.method public dcr(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public dcs()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public dct(Lcom/xiaomi/xmpush/thrift/CoordinateProvider;)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    return-object p0
.end method

.method public dcu(Lcom/xiaomi/xmpush/thrift/GeoType;)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    return-object p0
.end method

.method public dcv()Lcom/xiaomi/xmpush/thrift/CoordinateProvider;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    return-object v0
.end method

.method public dcw()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dcx()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dcy()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public dcz(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/GeoFencing;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    if-nez v0, :cond_1

    return v1

    :cond_0
    return v1

    :cond_1
    check-cast p1, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcA(Lcom/xiaomi/xmpush/thrift/GeoFencing;)Z

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/xiaomi/xmpush/thrift/GeoType;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "GeoFencing("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "name:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "appId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->appId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "packageName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "createTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->createTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->type:Lcom/xiaomi/xmpush/thrift/GeoType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dch()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dci()Z

    move-result v1

    if-nez v1, :cond_6

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcx()Z

    move-result v1

    if-nez v1, :cond_7

    :goto_6
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "coordinateProvider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->coordinateProvider:Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_7
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_1
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_2
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_4
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "circleCenter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleCenter:Lcom/xiaomi/xmpush/thrift/Location;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_5
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_6
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "circleRadius:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->circleRadius:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_7
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "polygonPoints:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/GeoFencing;->polygonPoints:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_8
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_9
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7
.end method
