.class public final enum Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;
.super Ljava/lang/Enum;
.source "XmPushActionNormalConfig.java"


# static fields
.field public static final enum dzU:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

.field private static final dzV:Ljava/util/Map;

.field private static final synthetic dzW:[Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

.field public static final enum dzX:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

.field public static final enum dzY:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;


# instance fields
.field private final _fieldName:Ljava/lang/String;

.field private final _thriftId:S


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    const-string/jumbo v1, "NORMAL_CONFIGS"

    const-string/jumbo v2, "normalConfigs"

    invoke-direct {v0, v1, v4, v5, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzU:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    const-string/jumbo v1, "APP_ID"

    const/4 v2, 0x4

    const-string/jumbo v3, "appId"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzY:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    const-string/jumbo v1, "PACKAGE_NAME"

    const/4 v2, 0x5

    const-string/jumbo v3, "packageName"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzX:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzU:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    aput-object v1, v0, v4

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzY:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzX:Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    aput-object v1, v0, v6

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzW:[Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzV:Ljava/util/Map;

    const-class v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    sget-object v2, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzV:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->cTY()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ISLjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    int-to-short v0, p3

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->_thriftId:S

    iput-object p4, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->_fieldName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;
    .locals 1

    const-class v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;
    .locals 1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->dzW:[Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;

    return-object v0
.end method


# virtual methods
.method public cTY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig$_Fields;->_fieldName:Ljava/lang/String;

    return-object v0
.end method
