.class public Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;
.super Ljava/lang/Object;
.source "RegisteredGeoFencing.java"

# interfaces
.implements Lorg/apache/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final dzI:Lorg/apache/thrift/protocol/c;

.field private static final dzJ:Lorg/apache/thrift/protocol/i;

.field public static final dzK:Ljava/util/Map;


# instance fields
.field public geoFencings:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/16 v8, 0xe

    const/4 v7, 0x1

    new-instance v0, Lorg/apache/thrift/protocol/c;

    const-string/jumbo v1, "RegisteredGeoFencing"

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->dzI:Lorg/apache/thrift/protocol/c;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "geoFencings"

    invoke-direct {v0, v1, v8, v7}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->dzJ:Lorg/apache/thrift/protocol/i;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing$_Fields;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing$_Fields;->dAF:Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/SetMetaData;

    new-instance v4, Lorg/apache/thrift/meta_data/StructMetaData;

    const/16 v5, 0xc

    const-class v6, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    invoke-direct {v4, v5, v6}, Lorg/apache/thrift/meta_data/StructMetaData;-><init>(BLjava/lang/Class;)V

    invoke-direct {v3, v8, v4}, Lorg/apache/thrift/meta_data/SetMetaData;-><init>(BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    const-string/jumbo v4, "geoFencings"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->dzK:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->dzK:Ljava/util/Map;

    const-class v1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;

    invoke-static {v1, v0}, Lorg/apache/thrift/meta_data/FieldMetaData;->esm(Ljava/lang/Class;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cTq(Lorg/apache/thrift/protocol/a;)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v2, :cond_2

    iget-short v2, v0, Lorg/apache/thrift/protocol/i;->eXX:S

    packed-switch v2, :pswitch_data_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    :goto_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_0

    :pswitch_0
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xe

    if-eq v2, v3, :cond_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erB()Lorg/apache/thrift/protocol/h;

    move-result-object v2

    new-instance v0, Ljava/util/HashSet;

    iget v3, v2, Lorg/apache/thrift/protocol/h;->eXW:I

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    move v0, v1

    :goto_2
    iget v3, v2, Lorg/apache/thrift/protocol/h;->eXW:I

    if-lt v0, v3, :cond_1

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erK()V

    goto :goto_1

    :cond_1
    new-instance v3, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    invoke-direct {v3}, Lcom/xiaomi/xmpush/thrift/GeoFencing;-><init>()V

    invoke-virtual {v3, p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->cTq(Lorg/apache/thrift/protocol/a;)V

    iget-object v4, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erU()V

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deY()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public cTu(Lorg/apache/thrift/protocol/a;)V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deY()V

    sget-object v0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->dzI:Lorg/apache/thrift/protocol/c;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erx(Lorg/apache/thrift/protocol/c;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erQ()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erL()V

    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->dzJ:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    new-instance v0, Lorg/apache/thrift/protocol/h;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/16 v2, 0xc

    invoke-direct {v0, v2, v1}, Lorg/apache/thrift/protocol/h;-><init>(BI)V

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erY(Lorg/apache/thrift/protocol/h;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erN()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/GeoFencing;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->cTu(Lorg/apache/thrift/protocol/a;)V

    goto :goto_1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deU(Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;)I

    move-result v0

    return v0
.end method

.method public deU(Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;)I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deW()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deW()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deW()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    return v2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_2
    return v0

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esu(Ljava/util/Set;Ljava/util/Set;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0
.end method

.method public deV(Ljava/util/Set;)Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    return-object p0
.end method

.method public deW()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public deX()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    return-object v0
.end method

.method public deY()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'geoFencings\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public deZ(Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;)Z
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deW()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deW()Z

    move-result v1

    if-eqz v0, :cond_3

    :cond_0
    if-nez v0, :cond_5

    :cond_1
    return v2

    :cond_2
    return v2

    :cond_3
    if-nez v1, :cond_0

    :cond_4
    const/4 v0, 0x1

    return v0

    :cond_5
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;

    if-nez v0, :cond_1

    return v1

    :cond_0
    return v1

    :cond_1
    check-cast p1, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->deZ(Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "RegisteredGeoFencing("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "geoFencings:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/RegisteredGeoFencing;->geoFencings:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_0
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
