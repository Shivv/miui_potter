.class public final enum Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;
.super Ljava/lang/Enum;
.source "XmPushActionUnSubscription.java"


# static fields
.field public static final enum dmf:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

.field public static final enum dmg:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

.field public static final enum dmh:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

.field public static final enum dmi:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

.field private static final dmj:Ljava/util/Map;

.field public static final enum dmk:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

.field private static final synthetic dml:[Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

.field public static final enum dmm:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

.field public static final enum dmn:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;


# instance fields
.field private final _fieldName:Ljava/lang/String;

.field private final _thriftId:S


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const-string/jumbo v1, "DEBUG"

    const-string/jumbo v2, "debug"

    invoke-direct {v0, v1, v5, v6, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmk:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const-string/jumbo v1, "TARGET"

    const-string/jumbo v2, "target"

    invoke-direct {v0, v1, v6, v7, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmf:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const-string/jumbo v1, "ID"

    const-string/jumbo v2, "id"

    invoke-direct {v0, v1, v7, v8, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmi:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const-string/jumbo v1, "APP_ID"

    const-string/jumbo v2, "appId"

    invoke-direct {v0, v1, v8, v9, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmh:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const-string/jumbo v1, "TOPIC"

    const/4 v2, 0x5

    const-string/jumbo v3, "topic"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmm:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const-string/jumbo v1, "PACKAGE_NAME"

    const/4 v2, 0x6

    const-string/jumbo v3, "packageName"

    const/4 v4, 0x5

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmg:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const-string/jumbo v1, "CATEGORY"

    const/4 v2, 0x7

    const-string/jumbo v3, "category"

    const/4 v4, 0x6

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmn:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmk:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmf:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    aput-object v1, v0, v6

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmi:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    aput-object v1, v0, v7

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmh:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    aput-object v1, v0, v8

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmm:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    aput-object v1, v0, v9

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmg:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmn:Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dml:[Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmj:Ljava/util/Map;

    const-class v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    sget-object v2, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dmj:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->cTY()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ISLjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    int-to-short v0, p3

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->_thriftId:S

    iput-object p4, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->_fieldName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;
    .locals 1

    const-class v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;
    .locals 1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->dml:[Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;

    return-object v0
.end method


# virtual methods
.method public cTY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscription$_Fields;->_fieldName:Ljava/lang/String;

    return-object v0
.end method
