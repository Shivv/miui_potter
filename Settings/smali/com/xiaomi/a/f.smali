.class Lcom/xiaomi/a/f;
.super Ljava/lang/Object;
.source "BlobReader.java"


# instance fields
.field private cXR:Ljava/io/InputStream;

.field private cXS:Lcom/xiaomi/a/g;

.field private cXT:Ljava/util/zip/Adler32;

.field private cXU:Ljava/nio/ByteBuffer;

.field private cXV:Ljava/nio/ByteBuffer;

.field private cXW:[B

.field private volatile cXX:Z

.field private cXY:Lcom/xiaomi/a/c;


# direct methods
.method constructor <init>(Ljava/io/InputStream;Lcom/xiaomi/a/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x800

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/a/f;->cXV:Ljava/nio/ByteBuffer;

    new-instance v0, Ljava/util/zip/Adler32;

    invoke-direct {v0}, Ljava/util/zip/Adler32;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/a/f;->cXT:Ljava/util/zip/Adler32;

    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/xiaomi/a/f;->cXR:Ljava/io/InputStream;

    iput-object p2, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    new-instance v0, Lcom/xiaomi/a/c;

    invoke-direct {v0}, Lcom/xiaomi/a/c;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/a/f;->cXY:Lcom/xiaomi/a/c;

    return-void
.end method

.method private cCf(Ljava/nio/ByteBuffer;I)V
    .locals 3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/a/f;->cXR:Ljava/io/InputStream;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {v1, v2, v0, p2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_1
    sub-int/2addr p2, v1

    add-int/2addr v0, v1

    if-gtz p2, :cond_0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    return-void
.end method

.method private cCg()V
    .locals 6

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/a/f;->cXX:Z

    invoke-virtual {p0}, Lcom/xiaomi/a/f;->cCh()Lcom/xiaomi/a/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "CONN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v0}, Lcom/xiaomi/a/g;->getKey()[B

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/a/f;->cXW:[B

    :goto_1
    iget-boolean v0, p0, Lcom/xiaomi/a/f;->cXX:Z

    if-eqz v0, :cond_4

    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/push/a/c;->cII([B)Lcom/xiaomi/push/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/push/a/c;->cIH()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_2
    invoke-virtual {v1}, Lcom/xiaomi/push/a/c;->cIB()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[Slim] CONN: host = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/xiaomi/push/a/c;->cIF()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v1}, Lcom/xiaomi/push/a/c;->cIC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/xiaomi/a/g;->cxt(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Lcom/xiaomi/push/a/c;->cIJ()Lcom/xiaomi/push/a/g;

    move-result-object v2

    new-instance v3, Lcom/xiaomi/a/e;

    invoke-direct {v3}, Lcom/xiaomi/a/e;-><init>()V

    const-string/jumbo v4, "SYNC"

    const-string/jumbo v5, "CONF"

    invoke-virtual {v3, v4, v5}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/xiaomi/push/a/g;->diz()[B

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/xiaomi/a/e;->cBN([BLjava/lang/String;)V

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v2, v3}, Lcom/xiaomi/a/g;->cCn(Lcom/xiaomi/a/e;)V

    goto :goto_3

    :cond_3
    const-string/jumbo v0, "[Slim] Invalid CONN"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Invalid Connection"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/a/f;->cCh()Lcom/xiaomi/a/e;

    move-result-object v1

    iget-object v0, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v0}, Lcom/xiaomi/a/g;->cxl()V

    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cBU()S

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[Slim] unknow blob type "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cBU()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_0
    iget-object v0, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/g;->cCn(Lcom/xiaomi/a/e;)V

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "SECMSG"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/g;->cCn(Lcom/xiaomi/a/e;)V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cCa()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    :try_start_0
    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cBO()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXY:Lcom/xiaomi/a/c;

    iget-object v0, v0, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/xiaomi/a/e;->cBL(Ljava/lang/String;)[B

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v2, v0, v3}, Lcom/xiaomi/a/c;->cBF([BLcom/xiaomi/smack/e;)Lcom/xiaomi/smack/packet/d;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v2, v0}, Lcom/xiaomi/a/g;->cCk(Lcom/xiaomi/smack/packet/d;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[Slim] Parse packet from Blob "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/xiaomi/a/e;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " failure:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/a/f;->cXY:Lcom/xiaomi/a/c;

    invoke-virtual {v1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v0, v2, v3}, Lcom/xiaomi/a/c;->cBF([BLcom/xiaomi/smack/e;)Lcom/xiaomi/smack/packet/d;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXS:Lcom/xiaomi/a/g;

    invoke-virtual {v2, v0}, Lcom/xiaomi/a/g;->cCk(Lcom/xiaomi/smack/packet/d;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[Slim] Parse packet from Blob "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/xiaomi/a/e;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " failure:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private cCi()Ljava/nio/ByteBuffer;
    .locals 8

    const/4 v7, 0x4

    const/16 v4, 0x800

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/a/f;->cCf(Ljava/nio/ByteBuffer;I)V

    iget-object v0, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v1

    const/16 v2, -0x3d02

    if-eq v0, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Malformed Input"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x5

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    const v2, 0x8000

    if-gt v0, v2, :cond_3

    add-int/lit8 v2, v0, 0x4

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-gt v2, v3, :cond_4

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    const/16 v3, 0x1000

    if-gt v2, v3, :cond_5

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v2, v0}, Lcom/xiaomi/a/f;->cCf(Ljava/nio/ByteBuffer;I)V

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXV:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXV:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v2, v7}, Lcom/xiaomi/a/f;->cCf(Ljava/nio/ByteBuffer;I)V

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXV:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXV:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXT:Ljava/util/zip/Adler32;

    invoke-virtual {v3}, Ljava/util/zip/Adler32;->reset()V

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXT:Ljava/util/zip/Adler32;

    iget-object v4, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    invoke-virtual {v3, v4, v6, v5}, Ljava/util/zip/Adler32;->update([BII)V

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXT:Ljava/util/zip/Adler32;

    invoke-virtual {v3}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v4

    long-to-int v3, v4

    if-ne v2, v3, :cond_6

    iget-object v2, p0, Lcom/xiaomi/a/f;->cXW:[B

    if-nez v2, :cond_7

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    return-object v0

    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Blob size too large"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    add-int/lit16 v2, v0, 0x800

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    iget-object v5, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v2, v3, v6, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    iput-object v2, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_5
    if-ge v0, v4, :cond_2

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    iget-object v5, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v2, v3, v6, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    iput-object v2, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CRC = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/a/f;->cXT:Ljava/util/zip/Adler32;

    invoke-virtual {v1}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v4

    long-to-int v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Corrupted Blob bad CRC"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v2, p0, Lcom/xiaomi/a/f;->cXW:[B

    iget-object v3, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v4, v1, v0}, Lcom/xiaomi/push/service/c;->cMA([B[BZII)[B

    goto/16 :goto_1
.end method


# virtual methods
.method cCh()Lcom/xiaomi/a/e;
    .locals 6

    const/16 v5, 0x8

    const/16 v3, 0x80

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/xiaomi/a/f;->cCi()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    :try_start_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    if-eq v1, v5, :cond_0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/a/e;->cBQ(Ljava/nio/ByteBuffer;)Lcom/xiaomi/a/e;

    move-result-object v0

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[Slim] Read {cmd="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";chid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/xiaomi/a/e;->cCe()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/xiaomi/a/b;

    invoke-direct {v0}, Lcom/xiaomi/a/b;-><init>()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[Slim] read Blob ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    if-gt v1, v3, :cond_2

    :goto_3
    invoke-static {v5, v2, v1}, Lcom/xiaomi/channel/commonutils/h/p;->cBA([BII)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "] Err:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/a/f;->cXU:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    goto :goto_2

    :cond_2
    move v1, v3

    goto :goto_3

    :catch_1
    move-exception v0

    move v1, v2

    goto :goto_1
.end method

.method cCj()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/a/f;->cXX:Z

    return-void
.end method

.method start()V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/xiaomi/a/f;->cCg()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    iget-boolean v1, p0, Lcom/xiaomi/a/f;->cXX:Z

    if-nez v1, :cond_0

    throw v0
.end method
