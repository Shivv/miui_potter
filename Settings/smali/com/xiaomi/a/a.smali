.class public Lcom/xiaomi/a/a;
.super Ljava/lang/Object;
.source "BlobWriter.java"


# instance fields
.field private cXD:Lcom/xiaomi/a/g;

.field private cXE:Ljava/io/OutputStream;

.field private cXF:Ljava/util/zip/Adler32;

.field private cXG:I

.field cXH:Ljava/nio/ByteBuffer;

.field private cXI:I

.field private cXJ:Ljava/nio/ByteBuffer;

.field private cXK:[B


# direct methods
.method constructor <init>(Ljava/io/OutputStream;Lcom/xiaomi/a/g;)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0x800

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/a/a;->cXJ:Ljava/nio/ByteBuffer;

    new-instance v1, Ljava/util/zip/Adler32;

    invoke-direct {v1}, Ljava/util/zip/Adler32;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/a/a;->cXF:Ljava/util/zip/Adler32;

    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/xiaomi/a/a;->cXE:Ljava/io/OutputStream;

    iput-object p2, p0, Lcom/xiaomi/a/a;->cXD:Lcom/xiaomi/a/g;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v2

    const v3, 0x36ee80

    div-int/2addr v2, v3

    iput v2, p0, Lcom/xiaomi/a/a;->cXG:I

    invoke-virtual {v1}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    iput v0, p0, Lcom/xiaomi/a/a;->cXI:I

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cBB()V
    .locals 7

    const/16 v6, 0x24

    const/4 v3, 0x0

    new-instance v0, Lcom/xiaomi/push/a/j;

    invoke-direct {v0}, Lcom/xiaomi/push/a/j;-><init>()V

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->cKJ(I)Lcom/xiaomi/push/a/j;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->cKX(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->cKG(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    invoke-static {}, Lcom/xiaomi/push/service/an;->cQA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->cLc(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    invoke-virtual {v0, v6}, Lcom/xiaomi/push/a/j;->cLd(I)Lcom/xiaomi/push/a/j;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXD:Lcom/xiaomi/a/g;

    invoke-virtual {v1}, Lcom/xiaomi/a/g;->cxD()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->cLb(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXD:Lcom/xiaomi/a/g;

    invoke-virtual {v1}, Lcom/xiaomi/a/g;->cxk()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->cKS(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->setLocale(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->cLf(I)Lcom/xiaomi/push/a/j;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXD:Lcom/xiaomi/a/g;

    invoke-virtual {v1}, Lcom/xiaomi/a/g;->getConfiguration()Lcom/xiaomi/smack/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/smack/h;->cxY()[B

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    new-instance v1, Lcom/xiaomi/a/e;

    invoke-direct {v1}, Lcom/xiaomi/a/e;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/xiaomi/a/e;->cBJ(I)V

    const-string/jumbo v2, "CONN"

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "xiaomi.com"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5, v2, v3}, Lcom/xiaomi/a/e;->cCc(JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/xiaomi/push/a/j;->diz()[B

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/xiaomi/a/e;->cBN([BLjava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/xiaomi/a/a;->cBC(Lcom/xiaomi/a/e;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[slim] open conn: andver="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " sdk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " hash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/push/service/an;->cQA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " tz="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/a/a;->cXG:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/a/a;->cXI:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Model="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " os="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {v1}, Lcom/xiaomi/push/a/g;->cKk([B)Lcom/xiaomi/push/a/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/j;->cKQ(Lcom/xiaomi/push/a/g;)Lcom/xiaomi/push/a/j;

    goto/16 :goto_0
.end method

.method public cBC(Lcom/xiaomi/a/e;)I
    .locals 6

    const v3, 0x8000

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->getSerializedSize()I

    move-result v0

    if-gt v0, v3, :cond_1

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    add-int/lit8 v1, v0, 0x8

    add-int/lit8 v1, v1, 0x4

    iget-object v2, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-le v1, v2, :cond_2

    :cond_0
    add-int/lit8 v1, v0, 0x8

    add-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    const/16 v2, -0x3d02

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v2}, Lcom/xiaomi/a/e;->cBE(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "CONN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/a/a;->cXF:Ljava/util/zip/Adler32;

    invoke-virtual {v0}, Ljava/util/zip/Adler32;->reset()V

    iget-object v0, p0, Lcom/xiaomi/a/a;->cXF:Ljava/util/zip/Adler32;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {v0, v1, v5, v2}, Ljava/util/zip/Adler32;->update([BII)V

    iget-object v0, p0, Lcom/xiaomi/a/a;->cXF:Ljava/util/zip/Adler32;

    invoke-virtual {v0}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v0

    long-to-int v0, v0

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXJ:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v5, v0}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/xiaomi/a/a;->cXE:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {v0, v1, v5, v2}, Ljava/io/OutputStream;->write([BII)V

    iget-object v0, p0, Lcom/xiaomi/a/a;->cXE:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/xiaomi/a/a;->cXJ:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v5, v2}, Ljava/io/OutputStream;->write([BII)V

    iget-object v0, p0, Lcom/xiaomi/a/a;->cXE:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    iget-object v0, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[Slim] Wrote {cmd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";chid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";len="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Blob size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " should be less than "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Drop blob chid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return v5

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    const/16 v2, 0x1000

    if-gt v1, v2, :cond_0

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/xiaomi/a/a;->cXK:[B

    if-eqz v2, :cond_4

    :goto_2
    iget-object v2, p0, Lcom/xiaomi/a/a;->cXK:[B

    iget-object v3, p0, Lcom/xiaomi/a/a;->cXH:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v4, v1, v0}, Lcom/xiaomi/push/service/c;->cMA([B[BZII)[B

    goto/16 :goto_1

    :cond_4
    iget-object v2, p0, Lcom/xiaomi/a/a;->cXD:Lcom/xiaomi/a/g;

    invoke-virtual {v2}, Lcom/xiaomi/a/g;->getKey()[B

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/a/a;->cXK:[B

    goto :goto_2
.end method

.method public cBD()V
    .locals 3

    new-instance v0, Lcom/xiaomi/a/e;

    invoke-direct {v0}, Lcom/xiaomi/a/e;-><init>()V

    const-string/jumbo v1, "CLOSE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/a/a;->cBC(Lcom/xiaomi/a/e;)I

    iget-object v0, p0, Lcom/xiaomi/a/a;->cXE:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    return-void
.end method
