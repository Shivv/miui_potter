.class public Lcom/xiaomi/a/g;
.super Lcom/xiaomi/smack/j;
.source "SlimConnection.java"


# instance fields
.field private cXZ:Ljava/lang/Thread;

.field private cYa:[B

.field private cYb:Lcom/xiaomi/a/f;

.field private cYc:Lcom/xiaomi/a/a;


# direct methods
.method public constructor <init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/smack/h;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/smack/j;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/smack/h;)V

    return-void
.end method

.method private cCl()V
    .locals 3

    :try_start_0
    new-instance v0, Lcom/xiaomi/a/f;

    iget-object v1, p0, Lcom/xiaomi/a/g;->cVY:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/xiaomi/a/f;-><init>(Ljava/io/InputStream;Lcom/xiaomi/a/g;)V

    iput-object v0, p0, Lcom/xiaomi/a/g;->cYb:Lcom/xiaomi/a/f;

    new-instance v0, Lcom/xiaomi/a/a;

    iget-object v1, p0, Lcom/xiaomi/a/g;->cVY:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/xiaomi/a/a;-><init>(Ljava/io/OutputStream;Lcom/xiaomi/a/g;)V

    iput-object v0, p0, Lcom/xiaomi/a/g;->cYc:Lcom/xiaomi/a/a;

    new-instance v0, Lcom/xiaomi/a/h;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Blob Reader ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/a/g;->cVE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/a/h;-><init>(Lcom/xiaomi/a/g;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/a/g;->cXZ:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/xiaomi/a/g;->cXZ:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v2, "Error to init reader and writer"

    invoke-direct {v1, v2, v0}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic cCm(Lcom/xiaomi/a/g;)Lcom/xiaomi/a/f;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/g;->cYb:Lcom/xiaomi/a/f;

    return-object v0
.end method

.method private cCo(Z)Lcom/xiaomi/a/e;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/xiaomi/a/b;

    invoke-direct {v0}, Lcom/xiaomi/a/b;-><init>()V

    if-nez p1, :cond_0

    :goto_0
    invoke-static {}, Lcom/xiaomi/e/d;->dgm()[B

    move-result-object v1

    if-nez v1, :cond_1

    :goto_1
    return-object v0

    :cond_0
    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/b;->cBM(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/xiaomi/push/a/l;

    invoke-direct {v2}, Lcom/xiaomi/push/a/l;-><init>()V

    invoke-static {v1}, Lcom/google/protobuf/micro/d;->diF([B)Lcom/google/protobuf/micro/d;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/xiaomi/push/a/l;->cLC(Lcom/google/protobuf/micro/d;)Lcom/xiaomi/push/a/l;

    invoke-virtual {v2}, Lcom/xiaomi/push/a/l;->diz()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/xiaomi/a/b;->cBN([BLjava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method cCk(Lcom/xiaomi/smack/packet/d;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/a/g;->cVB:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/c;

    invoke-virtual {v0, p1}, Lcom/xiaomi/smack/c;->cxf(Lcom/xiaomi/smack/packet/d;)V

    goto :goto_0
.end method

.method cCn(Lcom/xiaomi/a/e;)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBP()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/xiaomi/a/g;->cVB:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    return-void

    :cond_1
    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[Slim] RCV blob chid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; errCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBX()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; err="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "PING"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "CLOSE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xd

    invoke-virtual {p0, v0, v2}, Lcom/xiaomi/a/g;->cyp(ILjava/lang/Exception;)V

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[Slim] RCV ping id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/a/g;->cyj()V

    goto/16 :goto_1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/c;

    invoke-virtual {v0, p1}, Lcom/xiaomi/smack/c;->cxg(Lcom/xiaomi/a/e;)V

    goto/16 :goto_2
.end method

.method public cxB(Lcom/xiaomi/smack/packet/d;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/xiaomi/a/e;->cBY(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;)Lcom/xiaomi/a/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/a/g;->cxz(Lcom/xiaomi/a/e;)V

    return-void
.end method

.method public declared-synchronized cxG(Lcom/xiaomi/push/service/as;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/a/g;->cye()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p0}, Lcom/xiaomi/a/d;->cBG(Lcom/xiaomi/push/service/as;Ljava/lang/String;Lcom/xiaomi/smack/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized cxJ(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2, p0}, Lcom/xiaomi/a/d;->cBH(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/smack/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cxp([Lcom/xiaomi/a/e;)V
    .locals 3

    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/xiaomi/a/g;->cxz(Lcom/xiaomi/a/e;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public cxv()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public cxz(Lcom/xiaomi/a/e;)V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/a/g;->cYc:Lcom/xiaomi/a/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "the writer is null."

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/a/g;->cYc:Lcom/xiaomi/a/a;

    invoke-virtual {v0, p1}, Lcom/xiaomi/a/a;->cBC(Lcom/xiaomi/a/e;)I

    move-result v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/a/g;->cVp:J

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBK()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/a/g;->cVq:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/a/g;->cVt:Lcom/xiaomi/push/service/XMPushService;

    int-to-long v2, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const/4 v4, 0x0

    invoke-static/range {v0 .. v6}, Lcom/xiaomi/smack/c/d;->cwA(Landroid/content/Context;Ljava/lang/String;JZJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/xiaomi/smack/XMPPException;

    invoke-direct {v1, v0}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/c;

    invoke-virtual {v0, p1}, Lcom/xiaomi/smack/c;->cxg(Lcom/xiaomi/a/e;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected cyh(Z)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/a/g;->cYc:Lcom/xiaomi/a/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "The BlobWriter is null."

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/a/g;->cCo(Z)Lcom/xiaomi/a/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[Slim] SND ping id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/a/g;->cxz(Lcom/xiaomi/a/e;)V

    invoke-virtual {p0}, Lcom/xiaomi/a/g;->cyo()V

    return-void
.end method

.method protected declared-synchronized cyn(ILjava/lang/Exception;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/a/g;->cYb:Lcom/xiaomi/a/f;

    if-nez v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/a/g;->cYc:Lcom/xiaomi/a/a;

    if-nez v0, :cond_1

    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/a/g;->cYa:[B

    invoke-super {p0, p1, p2}, Lcom/xiaomi/smack/j;->cyn(ILjava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/a/g;->cYb:Lcom/xiaomi/a/f;

    invoke-virtual {v0}, Lcom/xiaomi/a/f;->cCj()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/a/g;->cYb:Lcom/xiaomi/a/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/xiaomi/a/g;->cYc:Lcom/xiaomi/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/a/a;->cBD()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/xiaomi/a/g;->cYc:Lcom/xiaomi/a/a;

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method protected declared-synchronized cyq()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/xiaomi/a/g;->cCl()V

    iget-object v0, p0, Lcom/xiaomi/a/g;->cYc:Lcom/xiaomi/a/a;

    invoke-virtual {v0}, Lcom/xiaomi/a/a;->cBB()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getKey()[B
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/a/g;->cYa:[B

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/a/g;->cYa:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/a/g;->cVD:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/xiaomi/push/service/an;->cQA()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/xiaomi/a/g;->cVD:Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/a/g;->cVD:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/a/g;->cVD:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/push/service/c;->cMF([B[B)[B

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/a/g;->cYa:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
