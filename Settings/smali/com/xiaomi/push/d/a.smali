.class public Lcom/xiaomi/push/d/a;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "TrafficDatabaseHelper.java"


# static fields
.field private static DATABASE_VERSION:I

.field public static final dkW:Ljava/lang/Object;

.field private static final dkX:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    sput v3, Lcom/xiaomi/push/d/a;->DATABASE_VERSION:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/push/d/a;->dkW:Ljava/lang/Object;

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "package_name"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "TEXT"

    aput-object v1, v0, v3

    const-string/jumbo v1, "message_ts"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, " LONG DEFAULT 0 "

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string/jumbo v1, "bytes"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string/jumbo v1, " LONG DEFAULT 0 "

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "network_type"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, " INT DEFAULT -1 "

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string/jumbo v1, "rcv"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string/jumbo v1, " INT DEFAULT -1 "

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string/jumbo v1, "imsi"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string/jumbo v1, "TEXT"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/push/d/a;->dkX:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    sget v0, Lcom/xiaomi/push/d/a;->DATABASE_VERSION:I

    const-string/jumbo v1, "traffic.db"

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method private cUa(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "CREATE TABLE traffic(_id INTEGER  PRIMARY KEY ,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_0
    sget-object v2, Lcom/xiaomi/push/d/a;->dkX:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-lt v0, v2, :cond_0

    const-string/jumbo v0, ");"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void

    :cond_0
    if-nez v0, :cond_1

    :goto_1
    sget-object v2, Lcom/xiaomi/push/d/a;->dkX:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/push/d/a;->dkX:[Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    sget-object v1, Lcom/xiaomi/push/d/a;->dkW:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/push/d/a;->cUa(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    return-void
.end method
