.class public final Lcom/xiaomi/push/a/c;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private daF:Ljava/lang/String;

.field private daG:Z

.field private daH:Ljava/lang/String;

.field private daI:Z

.field private daJ:Z

.field private daK:I

.field private daL:Lcom/xiaomi/push/a/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/c;->daF:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/c;->daH:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/push/a/c;->daL:Lcom/xiaomi/push/a/g;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/c;->daK:I

    return-void
.end method

.method public static cII([B)Lcom/xiaomi/push/a/c;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/c;

    invoke-direct {v0}, Lcom/xiaomi/push/a/c;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/c;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/c;

    check-cast v0, Lcom/xiaomi/push/a/c;

    return-object v0
.end method


# virtual methods
.method public cIA(Lcom/xiaomi/push/a/g;)Lcom/xiaomi/push/a/c;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/c;->daG:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/c;->daL:Lcom/xiaomi/push/a/g;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
.end method

.method public cIB()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/c;->daG:Z

    return v0
.end method

.method public cIC()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/c;->daF:Ljava/lang/String;

    return-object v0
.end method

.method public cID(Ljava/lang/String;)Lcom/xiaomi/push/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/c;->daJ:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/c;->daH:Ljava/lang/String;

    return-object p0
.end method

.method public cIE(Ljava/lang/String;)Lcom/xiaomi/push/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/c;->daI:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/c;->daF:Ljava/lang/String;

    return-object p0
.end method

.method public cIF()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/c;->daH:Ljava/lang/String;

    return-object v0
.end method

.method public cIG()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/c;->daJ:Z

    return v0
.end method

.method public cIH()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/c;->daI:Z

    return v0
.end method

.method public cIJ()Lcom/xiaomi/push/a/g;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/c;->daL:Lcom/xiaomi/push/a/g;

    return-object v0
.end method

.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/c;->daK:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/c;->daK:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/c;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/c;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/c;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/c;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/c;->cIE(Ljava/lang/String;)Lcom/xiaomi/push/a/c;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/c;->cID(Ljava/lang/String;)Lcom/xiaomi/push/a/c;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/xiaomi/push/a/g;

    invoke-direct {v0}, Lcom/xiaomi/push/a/g;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/a;->dhh(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/c;->cIA(Lcom/xiaomi/push/a/g;)Lcom/xiaomi/push/a/c;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIH()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIG()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIB()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIC()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIF()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIJ()Lcom/xiaomi/push/a/g;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhP(ILcom/google/protobuf/micro/c;)V

    goto :goto_2
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIH()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIG()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIB()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    iput v0, p0, Lcom/xiaomi/push/a/c;->daK:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIC()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/c;->cIJ()Lcom/xiaomi/push/a/g;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhT(ILcom/google/protobuf/micro/c;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2
.end method
