.class public final Lcom/xiaomi/push/a/l;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private dcJ:I

.field private dcK:Z

.field private dcL:Z

.field private dcM:Lcom/google/protobuf/micro/d;

.field private dcN:Lcom/xiaomi/push/a/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    sget-object v0, Lcom/google/protobuf/micro/d;->dCc:Lcom/google/protobuf/micro/d;

    iput-object v0, p0, Lcom/xiaomi/push/a/l;->dcM:Lcom/google/protobuf/micro/d;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/push/a/l;->dcN:Lcom/xiaomi/push/a/g;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/l;->dcJ:I

    return-void
.end method

.method public static cLA([B)Lcom/xiaomi/push/a/l;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/l;

    invoke-direct {v0}, Lcom/xiaomi/push/a/l;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/l;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/l;

    check-cast v0, Lcom/xiaomi/push/a/l;

    return-object v0
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/l;->dcJ:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/l;->dcJ:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/l;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/l;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/l;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/l;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhq()Lcom/google/protobuf/micro/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/l;->cLC(Lcom/google/protobuf/micro/d;)Lcom/xiaomi/push/a/l;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/xiaomi/push/a/g;

    invoke-direct {v0}, Lcom/xiaomi/push/a/g;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/a;->dhh(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/l;->cLD(Lcom/xiaomi/push/a/g;)Lcom/xiaomi/push/a/l;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->cLE()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->cLB()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->cLG()Lcom/google/protobuf/micro/d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dif(ILcom/google/protobuf/micro/d;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->cLF()Lcom/xiaomi/push/a/g;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhP(ILcom/google/protobuf/micro/c;)V

    goto :goto_1
.end method

.method public cLB()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/l;->dcK:Z

    return v0
.end method

.method public cLC(Lcom/google/protobuf/micro/d;)Lcom/xiaomi/push/a/l;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/l;->dcL:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/l;->dcM:Lcom/google/protobuf/micro/d;

    return-object p0
.end method

.method public cLD(Lcom/xiaomi/push/a/g;)Lcom/xiaomi/push/a/l;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/l;->dcK:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/l;->dcN:Lcom/xiaomi/push/a/g;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
.end method

.method public cLE()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/l;->dcL:Z

    return v0
.end method

.method public cLF()Lcom/xiaomi/push/a/g;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/l;->dcN:Lcom/xiaomi/push/a/g;

    return-object v0
.end method

.method public cLG()Lcom/google/protobuf/micro/d;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/l;->dcM:Lcom/google/protobuf/micro/d;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->cLE()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->cLB()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    iput v0, p0, Lcom/xiaomi/push/a/l;->dcJ:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->cLG()Lcom/google/protobuf/micro/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->dhE(ILcom/google/protobuf/micro/d;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/l;->cLF()Lcom/xiaomi/push/a/g;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhT(ILcom/google/protobuf/micro/c;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1
.end method
