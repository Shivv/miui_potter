.class public final Lcom/xiaomi/push/a/d;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private daM:Z

.field private daN:Ljava/lang/String;

.field private daO:I

.field private daP:Z

.field private daQ:Z

.field private daR:Z

.field private daS:J

.field private daT:Z

.field private daU:Z

.field private daV:Ljava/lang/String;

.field private daW:Z

.field private daX:Ljava/lang/String;

.field private daY:Z

.field private daZ:Z

.field private dba:I

.field private dbb:I

.field private dbc:Ljava/lang/String;

.field private dbd:Z

.field private dbe:I

.field private dbf:Ljava/lang/String;

.field private dbg:Z

.field private dbh:I

.field private dbi:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    iput v2, p0, Lcom/xiaomi/push/a/d;->dba:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/push/a/d;->daS:J

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/d;->daX:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/d;->dbf:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/d;->dbc:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/d;->daV:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/d;->daN:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/xiaomi/push/a/d;->dbb:I

    iput v2, p0, Lcom/xiaomi/push/a/d;->dbe:I

    iput v2, p0, Lcom/xiaomi/push/a/d;->dbh:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/d;->dbi:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/d;->daO:I

    return-void
.end method


# virtual methods
.method public cIK(I)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daW:Z

    iput p1, p0, Lcom/xiaomi/push/a/d;->dbb:I

    return-object p0
.end method

.method public cIL(Ljava/lang/String;)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daU:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/d;->dbc:Ljava/lang/String;

    return-object p0
.end method

.method public cIM(Ljava/lang/String;)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daT:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/d;->daV:Ljava/lang/String;

    return-object p0
.end method

.method public cIN(I)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daZ:Z

    iput p1, p0, Lcom/xiaomi/push/a/d;->dbh:I

    return-object p0
.end method

.method public cIO(Ljava/lang/String;)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->dbd:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/d;->dbf:Ljava/lang/String;

    return-object p0
.end method

.method public cIP(Ljava/lang/String;)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daM:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/d;->daX:Ljava/lang/String;

    return-object p0
.end method

.method public cIQ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/d;->daV:Ljava/lang/String;

    return-object v0
.end method

.method public cIR()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daW:Z

    return v0
.end method

.method public cIS()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->dbg:Z

    return v0
.end method

.method public cIT()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/d;->daX:Ljava/lang/String;

    return-object v0
.end method

.method public cIU()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daQ:Z

    return v0
.end method

.method public cIV()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daM:Z

    return v0
.end method

.method public cIW()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/d;->dbc:Ljava/lang/String;

    return-object v0
.end method

.method public cIX()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daY:Z

    return v0
.end method

.method public cIY()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daP:Z

    return v0
.end method

.method public cIZ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/d;->dbf:Ljava/lang/String;

    return-object v0
.end method

.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/d;->daO:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/d;->daO:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/d;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/d;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/d;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/d;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cJb(I)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dho()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/push/a/d;->cJk(J)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cIP(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cIO(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cIL(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cIM(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cJe(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cIK(I)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cJf(I)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cIN(I)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/d;->cJc(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIY()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIS()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIV()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJo()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJm()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJh()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIX()Z

    move-result v0

    if-nez v0, :cond_6

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIR()Z

    move-result v0

    if-nez v0, :cond_7

    :goto_7
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIU()Z

    move-result v0

    if-nez v0, :cond_8

    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJg()Z

    move-result v0

    if-nez v0, :cond_9

    :goto_9
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJq()Z

    move-result v0

    if-nez v0, :cond_a

    :goto_a
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJi()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJp()J

    move-result-wide v0

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/micro/b;->diw(IJ)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIT()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIZ()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIW()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIQ()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_5

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_6

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJd()I

    move-result v0

    const/16 v1, 0x8

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_7

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJj()I

    move-result v0

    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_8

    :cond_9
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJn()I

    move-result v0

    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_9

    :cond_a
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJl()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_a
.end method

.method public cJa()Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daT:Z

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/d;->daV:Ljava/lang/String;

    return-object p0
.end method

.method public cJb(I)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daP:Z

    iput p1, p0, Lcom/xiaomi/push/a/d;->dba:I

    return-object p0
.end method

.method public cJc(Ljava/lang/String;)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daR:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/d;->dbi:Ljava/lang/String;

    return-object p0
.end method

.method public cJd()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/d;->dbb:I

    return v0
.end method

.method public cJe(Ljava/lang/String;)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daY:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/d;->daN:Ljava/lang/String;

    return-object p0
.end method

.method public cJf(I)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->daQ:Z

    iput p1, p0, Lcom/xiaomi/push/a/d;->dbe:I

    return-object p0
.end method

.method public cJg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daZ:Z

    return v0
.end method

.method public cJh()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daT:Z

    return v0
.end method

.method public cJi()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/d;->dba:I

    return v0
.end method

.method public cJj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/d;->dbe:I

    return v0
.end method

.method public cJk(J)Lcom/xiaomi/push/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/d;->dbg:Z

    iput-wide p1, p0, Lcom/xiaomi/push/a/d;->daS:J

    return-object p0
.end method

.method public cJl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/d;->dbi:Ljava/lang/String;

    return-object v0
.end method

.method public cJm()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daU:Z

    return v0
.end method

.method public cJn()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/d;->dbh:I

    return v0
.end method

.method public cJo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->dbd:Z

    return v0
.end method

.method public cJp()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/push/a/d;->daS:J

    return-wide v0
.end method

.method public cJq()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/d;->daR:Z

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/d;->daN:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIY()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIS()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIV()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJo()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJm()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJh()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIX()Z

    move-result v1

    if-nez v1, :cond_6

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIR()Z

    move-result v1

    if-nez v1, :cond_7

    :goto_7
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIU()Z

    move-result v1

    if-nez v1, :cond_8

    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJg()Z

    move-result v1

    if-nez v1, :cond_9

    :goto_9
    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJq()Z

    move-result v1

    if-nez v1, :cond_a

    :goto_a
    iput v0, p0, Lcom/xiaomi/push/a/d;->daO:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJi()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJp()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/b;->dhU(IJ)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIT()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIZ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cIQ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_5

    :cond_6
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_6

    :cond_7
    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJd()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_7

    :cond_8
    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJj()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_8

    :cond_9
    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJn()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_9

    :cond_a
    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/xiaomi/push/a/d;->cJl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_a
.end method
