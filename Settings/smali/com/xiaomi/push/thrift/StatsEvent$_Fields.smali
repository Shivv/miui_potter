.class public final enum Lcom/xiaomi/push/thrift/StatsEvent$_Fields;
.super Ljava/lang/Enum;
.source "StatsEvent.java"


# static fields
.field public static final enum dkK:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field public static final enum dkL:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field public static final enum dkM:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field public static final enum dkN:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field public static final enum dkO:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field public static final enum dkP:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field private static final dkQ:Ljava/util/Map;

.field public static final enum dkR:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field public static final enum dkS:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field public static final enum dkT:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field private static final synthetic dkU:[Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

.field public static final enum dkV:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;


# instance fields
.field private final _fieldName:Ljava/lang/String;

.field private final _thriftId:S


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "CHID"

    const-string/jumbo v2, "chid"

    invoke-direct {v0, v1, v5, v6, v2}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkM:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "TYPE"

    const-string/jumbo v2, "type"

    invoke-direct {v0, v1, v6, v7, v2}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkT:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "VALUE"

    const-string/jumbo v2, "value"

    invoke-direct {v0, v1, v7, v8, v2}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkK:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "CONNPT"

    const-string/jumbo v2, "connpt"

    invoke-direct {v0, v1, v8, v9, v2}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkN:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "HOST"

    const/4 v2, 0x5

    const-string/jumbo v3, "host"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkS:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "SUBVALUE"

    const/4 v2, 0x6

    const-string/jumbo v3, "subvalue"

    const/4 v4, 0x5

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkP:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "ANNOTATION"

    const/4 v2, 0x7

    const-string/jumbo v3, "annotation"

    const/4 v4, 0x6

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkL:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "USER"

    const/16 v2, 0x8

    const-string/jumbo v3, "user"

    const/4 v4, 0x7

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkO:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "TIME"

    const/16 v2, 0x9

    const-string/jumbo v3, "time"

    const/16 v4, 0x8

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkR:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const-string/jumbo v1, "CLIENT_IP"

    const/16 v2, 0xa

    const-string/jumbo v3, "clientIp"

    const/16 v4, 0x9

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkV:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkM:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkT:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    aput-object v1, v0, v6

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkK:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    aput-object v1, v0, v7

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkN:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    aput-object v1, v0, v8

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkS:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    aput-object v1, v0, v9

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkP:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkL:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkO:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkR:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkV:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkU:[Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkQ:Ljava/util/Map;

    const-class v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    sget-object v2, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkQ:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->cTY()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ISLjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    int-to-short v0, p3

    iput-short v0, p0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->_thriftId:S

    iput-object p4, p0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->_fieldName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent$_Fields;
    .locals 1

    const-class v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/push/thrift/StatsEvent$_Fields;
    .locals 1

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkU:[Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    return-object v0
.end method


# virtual methods
.method public cTY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->_fieldName:Ljava/lang/String;

    return-object v0
.end method
