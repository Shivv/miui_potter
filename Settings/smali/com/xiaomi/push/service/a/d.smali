.class public final Lcom/xiaomi/push/service/a/d;
.super Ljava/lang/Object;
.source "Alarm.java"


# static fields
.field private static ddt:Lcom/xiaomi/push/service/a/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cMf(Landroid/content/Context;)V
    .locals 8

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "com.xiaomi.xmsf"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_3

    :cond_0
    :goto_0
    if-eqz v0, :cond_6

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    new-instance v0, Lcom/xiaomi/push/service/a/b;

    invoke-direct {v0, v2}, Lcom/xiaomi/push/service/a/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/push/service/a/d;->ddt:Lcom/xiaomi/push/service/a/c;

    :goto_1
    return-void

    :cond_2
    new-instance v0, Lcom/xiaomi/push/service/a/b;

    invoke-direct {v0, v2}, Lcom/xiaomi/push/service/a/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/push/service/a/d;->ddt:Lcom/xiaomi/push/service/a/c;

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    iget-object v6, v5, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    const-string/jumbo v7, "com.xiaomi.push.service.XMJobService"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    const-string/jumbo v6, "android.permission.BIND_JOB_SERVICE"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    if-eqz v5, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "check service err : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/android/g;->cAq(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Should export service: com.xiaomi.push.service.XMJobService with permission android.permission.BIND_JOB_SERVICE in AndroidManifest.xml file"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static declared-synchronized cMg()Z
    .locals 2

    const-class v1, Lcom/xiaomi/push/service/a/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/service/a/d;->ddt:Lcom/xiaomi/push/service/a/c;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/push/service/a/d;->ddt:Lcom/xiaomi/push/service/a/c;

    invoke-interface {v0}, Lcom/xiaomi/push/service/a/c;->cMa()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized cMh(Z)V
    .locals 2

    const-class v1, Lcom/xiaomi/push/service/a/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/service/a/d;->ddt:Lcom/xiaomi/push/service/a/c;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/push/service/a/d;->ddt:Lcom/xiaomi/push/service/a/c;

    invoke-interface {v0, p0}, Lcom/xiaomi/push/service/a/c;->cMc(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    const-string/jumbo v0, "timer is not initialized"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized stop()V
    .locals 2

    const-class v1, Lcom/xiaomi/push/service/a/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/service/a/d;->ddt:Lcom/xiaomi/push/service/a/c;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/push/service/a/d;->ddt:Lcom/xiaomi/push/service/a/c;

    invoke-interface {v0}, Lcom/xiaomi/push/service/a/c;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
