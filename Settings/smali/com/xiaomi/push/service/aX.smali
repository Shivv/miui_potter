.class Lcom/xiaomi/push/service/aX;
.super Lcom/xiaomi/channel/commonutils/h/b;
.source "ServiceConfig.java"


# instance fields
.field diT:Z

.field final synthetic diU:Lcom/xiaomi/push/service/an;


# direct methods
.method constructor <init>(Lcom/xiaomi/push/service/an;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/push/service/aX;->diU:Lcom/xiaomi/push/service/an;

    invoke-direct {p0}, Lcom/xiaomi/channel/commonutils/h/b;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/push/service/aX;->diT:Z

    return-void
.end method


# virtual methods
.method public cAS()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/service/aX;->diU:Lcom/xiaomi/push/service/an;

    invoke-static {v0, v2}, Lcom/xiaomi/push/service/an;->cQK(Lcom/xiaomi/push/service/an;Lcom/xiaomi/channel/commonutils/h/b;)Lcom/xiaomi/channel/commonutils/h/b;

    iget-boolean v0, p0, Lcom/xiaomi/push/service/aX;->diT:Z

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/xiaomi/push/service/aX;->diU:Lcom/xiaomi/push/service/an;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aX;->diU:Lcom/xiaomi/push/service/an;

    invoke-static {v0}, Lcom/xiaomi/push/service/an;->cQM(Lcom/xiaomi/push/service/an;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/push/service/aX;->diU:Lcom/xiaomi/push/service/an;

    invoke-static {v3}, Lcom/xiaomi/push/service/an;->cQM(Lcom/xiaomi/push/service/an;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/xiaomi/push/service/f;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/push/service/f;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    iget-object v4, p0, Lcom/xiaomi/push/service/aX;->diU:Lcom/xiaomi/push/service/an;

    invoke-static {v4}, Lcom/xiaomi/push/service/an;->cQH(Lcom/xiaomi/push/service/an;)Lcom/xiaomi/push/a/f;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/xiaomi/push/service/f;->cMJ(Lcom/xiaomi/push/a/f;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public cwo()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "http://resolver.msg.xiaomi.net/psc/?t=a"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/xiaomi/c/l;->cVt(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/a/f;->cJO([B)Lcom/xiaomi/push/a/f;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/push/service/aX;->diU:Lcom/xiaomi/push/service/an;

    invoke-static {v1, v0}, Lcom/xiaomi/push/service/an;->cQL(Lcom/xiaomi/push/service/an;Lcom/xiaomi/push/a/f;)Lcom/xiaomi/push/a/f;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/service/aX;->diT:Z

    iget-object v0, p0, Lcom/xiaomi/push/service/aX;->diU:Lcom/xiaomi/push/service/an;

    invoke-static {v0}, Lcom/xiaomi/push/service/an;->cQJ(Lcom/xiaomi/push/service/an;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "fetch config failure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0
.end method
