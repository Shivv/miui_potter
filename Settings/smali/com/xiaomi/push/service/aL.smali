.class public Lcom/xiaomi/push/service/aL;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "GeoFenceDatabaseHelper.java"


# static fields
.field private static final dik:[Ljava/lang/String;

.field private static final dim:[Ljava/lang/String;

.field private static dio:Lcom/xiaomi/push/service/aL;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private dil:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final din:Ljava/lang/Object;

.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "name"

    aput-object v1, v0, v3

    const-string/jumbo v1, "TEXT NOT NULL"

    aput-object v1, v0, v4

    const-string/jumbo v1, "appId"

    aput-object v1, v0, v5

    const-string/jumbo v1, "INTEGER NOT NULL"

    aput-object v1, v0, v6

    const-string/jumbo v1, "package_name"

    aput-object v1, v0, v7

    const-string/jumbo v1, "TEXT NOT NULL"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "create_time"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, "INTEGER NOT NULL"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string/jumbo v1, "type"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string/jumbo v1, "TEXT NOT NULL"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string/jumbo v1, "center_longtitude"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string/jumbo v1, "TEXT"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string/jumbo v1, "center_lantitude"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string/jumbo v1, "TEXT"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string/jumbo v1, "circle_radius"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string/jumbo v1, "REAL"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string/jumbo v1, "polygon_point"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string/jumbo v1, "TEXT"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string/jumbo v1, "coordinate_provider"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const-string/jumbo v1, "TEXT NOT NULL"

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const-string/jumbo v1, "current_status"

    const/16 v2, 0x14

    aput-object v1, v0, v2

    const-string/jumbo v1, "TEXT NOT NULL"

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/push/service/aL;->dik:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "message_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "TEXT NOT NULL"

    aput-object v1, v0, v4

    const-string/jumbo v1, "geo_id"

    aput-object v1, v0, v5

    const-string/jumbo v1, "TEXT NOT NULL"

    aput-object v1, v0, v6

    const-string/jumbo v1, "content"

    aput-object v1, v0, v7

    const-string/jumbo v1, "BLOB NOT NULL"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "action"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, "INTEGER NOT NULL"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string/jumbo v1, "deadline"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string/jumbo v1, "INTEGER NOT NULL"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/push/service/aL;->dim:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string/jumbo v0, "geofencing.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    const-string/jumbo v0, "GeoFenceDatabaseHelper."

    iput-object v0, p0, Lcom/xiaomi/push/service/aL;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/aL;->din:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/aL;->dil:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private cSp(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "CREATE TABLE geofence(id TEXT PRIMARY KEY ,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_0
    sget-object v2, Lcom/xiaomi/push/service/aL;->dik:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-lt v0, v2, :cond_0

    const-string/jumbo v0, ");"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    if-nez v0, :cond_1

    :goto_2
    sget-object v2, Lcom/xiaomi/push/service/aL;->dik:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/push/service/aL;->dik:[Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private cSq(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "CREATE TABLE geoMessage("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_0
    sget-object v2, Lcom/xiaomi/push/service/aL;->dim:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-lt v0, v2, :cond_0

    const-string/jumbo v0, ",PRIMARY KEY(message_id,geo_id));"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    if-nez v0, :cond_1

    :goto_2
    sget-object v2, Lcom/xiaomi/push/service/aL;->dim:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/push/service/aL;->dik:[Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aL;
    .locals 2

    sget-object v0, Lcom/xiaomi/push/service/aL;->dio:Lcom/xiaomi/push/service/aL;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/push/service/aL;->dio:Lcom/xiaomi/push/service/aL;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/push/service/aL;

    const-class v0, Lcom/xiaomi/push/service/aL;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/service/aL;->dio:Lcom/xiaomi/push/service/aL;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/push/service/aL;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aL;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/push/service/aL;->dio:Lcom/xiaomi/push/service/aL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized cSr()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aL;->dil:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/push/service/aL;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized cSs()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aL;->dil:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aL;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/xiaomi/push/service/aL;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/service/aL;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/aL;->din:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/aL;->cSp(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/aL;->cSq(Landroid/database/sqlite/SQLiteDatabase;)V

    const-string/jumbo v0, "GeoFenceDatabaseHelper. create tables"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    return-void
.end method
