.class public Lcom/xiaomi/push/service/H;
.super Ljava/lang/Object;
.source "TinyDataStorage.java"


# static fields
.field public static final dfy:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/push/service/H;->dfy:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cOb(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V
    .locals 4

    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->cAu()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "TinyData TinyDataStorage.cacheTinyData cache data to file begin item:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  ts:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/h/h;->getInstance(Landroid/content/Context;)Lcom/xiaomi/channel/commonutils/h/h;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/push/service/am;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/push/service/am;-><init>(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/channel/commonutils/h/h;->cBe(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->cWd()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.miui.hybrid"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void
.end method

.method static synthetic cOc(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/xiaomi/push/service/H;->cOe(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V

    return-void
.end method

.method private static cOd(Ljava/lang/String;)[B
    .locals 3

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/c/a;->cyV(Ljava/lang/String;)[B

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    const/16 v1, 0x44

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    const/16 v1, 0x54

    const/16 v2, 0xf

    aput-byte v1, v0, v2

    return-object v0
.end method

.method private static cOe(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/xiaomi/push/service/H;->cOf(Landroid/content/Context;)[B

    move-result-object v0

    :try_start_0
    invoke-static {p1}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/channel/commonutils/android/a;->czJ([B[B)[B

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "TinyData write to cache file failed case encryption fail item:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyL(Ljava/io/Writer;)V

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    return-void

    :cond_1
    :try_start_1
    array-length v1, v0

    if-lt v1, v3, :cond_0

    array-length v1, v0

    const/16 v3, 0x2800

    if-gt v1, v3, :cond_2

    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v4, "tiny_data.data"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v4, v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v1, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    array-length v3, v0

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/h/l;->cBp(I)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/BufferedOutputStream;->write([B)V

    invoke-virtual {v1, v0}, Ljava/io/BufferedOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyL(Ljava/io/Writer;)V

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    :goto_0
    return-void

    :cond_2
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "TinyData write to cache file failed case too much data content item:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyL(Ljava/io/Writer;)V

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    return-void

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_4
    const-string/jumbo v3, "TinyData write to cache file failed cause io exception"

    invoke-static {v3, v0}, Lcom/xiaomi/channel/commonutils/e/a;->czv(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyL(Ljava/io/Writer;)V

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_5
    const-string/jumbo v3, "TinyData write to cache file  failed"

    invoke-static {v3, v0}, Lcom/xiaomi/channel/commonutils/e/a;->czv(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyL(Ljava/io/Writer;)V

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyL(Ljava/io/Writer;)V

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method public static cOf(Landroid/content/Context;)[B
    .locals 4

    invoke-static {p0}, Lcom/xiaomi/push/service/ad;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/ad;

    move-result-object v0

    const-string/jumbo v1, "mipush"

    const-string/jumbo v2, "td_key"

    const-string/jumbo v3, ""

    invoke-virtual {v0, v1, v2, v3}, Lcom/xiaomi/push/service/ad;->cPm(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/xiaomi/push/service/H;->cOd(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x14

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/c/b;->cza(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/push/service/ad;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/ad;

    move-result-object v1

    const-string/jumbo v2, "mipush"

    const-string/jumbo v3, "td_key"

    invoke-virtual {v1, v2, v3, v0}, Lcom/xiaomi/push/service/ad;->cPl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
