.class public Lcom/xiaomi/push/service/N;
.super Ljava/lang/Object;
.source "AppRegionStorage.java"


# static fields
.field private static volatile dfE:Lcom/xiaomi/push/service/N;


# instance fields
.field private final dfF:Ljava/lang/String;

.field private final dfG:Ljava/lang/Object;

.field private final dfH:Ljava/lang/String;

.field private volatile dfI:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/N;->dfG:Ljava/lang/Object;

    const-string/jumbo v0, "mipush_region"

    iput-object v0, p0, Lcom/xiaomi/push/service/N;->dfF:Ljava/lang/String;

    const-string/jumbo v0, "mipush_region.lock"

    iput-object v0, p0, Lcom/xiaomi/push/service/N;->dfH:Ljava/lang/String;

    iput-object p1, p0, Lcom/xiaomi/push/service/N;->mContext:Landroid/content/Context;

    return-void
.end method

.method private cOh(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    const-string/jumbo v0, "mipush_region"

    const v2, 0x8000

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyO(Ljava/io/OutputStream;)V

    throw v0
.end method

.method private cOj(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/xiaomi/push/service/N;->dfG:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v4, "mipush_region.lock"

    invoke-direct {v0, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/b/a;->cyN(Ljava/io/File;)Z

    new-instance v2, Ljava/io/RandomAccessFile;

    const-string/jumbo v4, "rw"

    invoke-direct {v2, v0, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v1

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/push/service/N;->cOh(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-nez v1, :cond_3

    :cond_0
    :goto_0
    :try_start_2
    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyM(Ljava/io/RandomAccessFile;)V

    :goto_1
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catch_0
    move-exception v0

    move-object v2, v1

    :goto_2
    :try_start_3
    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-nez v1, :cond_4

    :cond_1
    :goto_3
    :try_start_4
    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyM(Ljava/io/RandomAccessFile;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    move-object v2, v1

    :goto_4
    if-nez v1, :cond_5

    :cond_2
    :goto_5
    :try_start_5
    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyM(Ljava/io/RandomAccessFile;)V

    throw v0

    :cond_3
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    goto :goto_0

    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    goto :goto_3

    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->isValid()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    goto :goto_5

    move-exception v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_5

    :catchall_2
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private cOl(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    const/4 v2, 0x0

    const-string/jumbo v4, ""

    :try_start_0
    const-string/jumbo v0, "mipush_region"

    invoke-virtual {p1, v0}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v2, v4

    :goto_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/b/a;->cyJ(Ljava/io/InputStream;)V

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyH(Ljava/io/Reader;)V

    :goto_1
    return-object v2

    :cond_0
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v3, v4

    :goto_2
    :try_start_4
    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/b/a;->cyJ(Ljava/io/InputStream;)V

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyH(Ljava/io/Reader;)V

    move-object v2, v3

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_3
    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/b/a;->cyJ(Ljava/io/InputStream;)V

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyH(Ljava/io/Reader;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v5, v3

    move-object v3, v2

    move-object v2, v5

    goto :goto_2
.end method

.method private cOm(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const/4 v2, 0x0

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v3, "mipush_region"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/xiaomi/push/service/N;->dfG:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v3, "mipush_region.lock"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/b/a;->cyN(Ljava/io/File;)Z

    new-instance v3, Ljava/io/RandomAccessFile;

    const-string/jumbo v1, "rw"

    invoke-direct {v3, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/N;->cOl(Landroid/content/Context;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v0

    if-nez v1, :cond_4

    :cond_0
    :goto_0
    :try_start_3
    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/b/a;->cyM(Ljava/io/RandomAccessFile;)V

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-object v0

    :cond_1
    const-string/jumbo v0, "Region no ready file to get data."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v2

    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_1
    :try_start_4
    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    if-nez v1, :cond_5

    :cond_2
    :goto_2
    :try_start_5
    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/b/a;->cyM(Ljava/io/RandomAccessFile;)V

    monitor-exit v4

    return-object v2

    :catchall_0
    move-exception v0

    move-object v3, v2

    :goto_3
    if-nez v2, :cond_6

    :cond_3
    :goto_4
    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/b/a;->cyM(Ljava/io/RandomAccessFile;)V

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :cond_4
    :try_start_6
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    goto :goto_0

    move-exception v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->isValid()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    goto :goto_2

    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Ljava/nio/channels/FileLock;->isValid()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Ljava/nio/channels/FileLock;->release()V

    goto :goto_4

    move-exception v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/N;
    .locals 2

    sget-object v0, Lcom/xiaomi/push/service/N;->dfE:Lcom/xiaomi/push/service/N;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/push/service/N;->dfE:Lcom/xiaomi/push/service/N;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/push/service/N;

    const-class v0, Lcom/xiaomi/push/service/N;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/service/N;->dfE:Lcom/xiaomi/push/service/N;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/push/service/N;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/N;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/push/service/N;->dfE:Lcom/xiaomi/push/service/N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public cOi()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/N;->dfI:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/service/N;->dfI:Ljava/lang/String;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/N;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/N;->cOm(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/service/N;->dfI:Ljava/lang/String;

    goto :goto_0
.end method

.method public cOk(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/service/N;->dfI:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/push/service/N;->dfI:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/push/service/N;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/push/service/N;->dfI:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/push/service/N;->cOj(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
