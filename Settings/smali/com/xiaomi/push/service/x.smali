.class public Lcom/xiaomi/push/service/x;
.super Ljava/lang/Object;
.source "JobScheduler.java"


# static fields
.field private static dfg:J

.field private static dfi:J

.field private static dfj:J


# instance fields
.field private final dff:Lcom/xiaomi/push/service/ai;

.field private final dfh:Lcom/xiaomi/push/service/aV;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/16 v0, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gtz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :cond_0
    sput-wide v0, Lcom/xiaomi/push/service/x;->dfj:J

    sget-wide v0, Lcom/xiaomi/push/service/x;->dfj:J

    sput-wide v0, Lcom/xiaomi/push/service/x;->dfi:J

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/x;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/push/service/x;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    new-instance v0, Lcom/xiaomi/push/service/aV;

    invoke-direct {v0, p1, p2}, Lcom/xiaomi/push/service/aV;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    new-instance v0, Lcom/xiaomi/push/service/ai;

    iget-object v1, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-direct {v0, v1}, Lcom/xiaomi/push/service/ai;-><init>(Lcom/xiaomi/push/service/aV;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/x;->dff:Lcom/xiaomi/push/service/ai;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "name == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Z)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Timer-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/push/service/x;->cNK()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/xiaomi/push/service/x;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method private cNJ(Lcom/xiaomi/push/service/C;J)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-static {v2}, Lcom/xiaomi/push/service/aV;->cTi(Lcom/xiaomi/push/service/aV;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/xiaomi/push/service/x;->cNM()J

    move-result-wide v2

    add-long/2addr v2, p2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Illegal delay to start the TimerTask: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Timer was canceled"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/xiaomi/push/service/ag;

    invoke-direct {v0}, Lcom/xiaomi/push/service/ag;-><init>()V

    iget v4, p1, Lcom/xiaomi/push/service/C;->type:I

    iput v4, v0, Lcom/xiaomi/push/service/ag;->type:I

    iput-object p1, v0, Lcom/xiaomi/push/service/ag;->dgt:Lcom/xiaomi/push/service/C;

    iput-wide v2, v0, Lcom/xiaomi/push/service/ag;->dgx:J

    iget-object v2, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-static {v2, v0}, Lcom/xiaomi/push/service/aV;->cTg(Lcom/xiaomi/push/service/aV;Lcom/xiaomi/push/service/ag;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static declared-synchronized cNK()J
    .locals 6

    const-class v1, Lcom/xiaomi/push/service/x;

    monitor-enter v1

    :try_start_0
    sget-wide v2, Lcom/xiaomi/push/service/x;->dfg:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    sput-wide v4, Lcom/xiaomi/push/service/x;->dfg:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized cNM()J
    .locals 8

    const-class v1, Lcom/xiaomi/push/service/x;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-wide v4, Lcom/xiaomi/push/service/x;->dfi:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    sget-wide v4, Lcom/xiaomi/push/service/x;->dfj:J

    sget-wide v6, Lcom/xiaomi/push/service/x;->dfi:J

    sub-long v6, v2, v6

    add-long/2addr v4, v6

    sput-wide v4, Lcom/xiaomi/push/service/x;->dfj:J

    :cond_0
    sput-wide v2, Lcom/xiaomi/push/service/x;->dfi:J

    sget-wide v2, Lcom/xiaomi/push/service/x;->dfj:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-wide v2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public cND(I)V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-static {v0}, Lcom/xiaomi/push/service/aV;->cTf(Lcom/xiaomi/push/service/aV;)Lcom/xiaomi/push/service/ar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/ar;->cQU(I)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cNE()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aV;->cancel()V

    return-void
.end method

.method public cNF(ILcom/xiaomi/push/service/C;)V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-static {v0}, Lcom/xiaomi/push/service/aV;->cTf(Lcom/xiaomi/push/service/aV;)Lcom/xiaomi/push/service/ar;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/push/service/ar;->cQX(ILcom/xiaomi/push/service/C;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cNG(Lcom/xiaomi/push/service/C;)V
    .locals 2

    invoke-static {}, Lcom/xiaomi/channel/commonutils/e/a;->czz()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/push/service/C;->run()V

    return-void

    :cond_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    if-eq v0, v1, :cond_0

    const-string/jumbo v0, "run job outside job job thread"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/RejectedExecutionException;

    const-string/jumbo v1, "Run job outside job thread"

    invoke-direct {v0, v1}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cNH(I)Z
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-static {v0}, Lcom/xiaomi/push/service/aV;->cTf(Lcom/xiaomi/push/service/aV;)Lcom/xiaomi/push/service/ar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/ar;->cQZ(I)Z

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cNI()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aV;->cTe()Z

    move-result v0

    return v0
.end method

.method public cNL()V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/x;->dfh:Lcom/xiaomi/push/service/aV;

    invoke-static {v0}, Lcom/xiaomi/push/service/aV;->cTf(Lcom/xiaomi/push/service/aV;)Lcom/xiaomi/push/service/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/ar;->cRe()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cNN(Lcom/xiaomi/push/service/C;J)V
    .locals 4

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "delay < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/push/service/x;->cNJ(Lcom/xiaomi/push/service/C;J)V

    return-void
.end method
