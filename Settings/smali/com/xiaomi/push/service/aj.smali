.class Lcom/xiaomi/push/service/aj;
.super Lcom/xiaomi/push/service/n;
.source "XMPushService.java"


# instance fields
.field dgR:Lcom/xiaomi/push/service/as;

.field final synthetic dgS:Lcom/xiaomi/push/service/XMPushService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/push/service/aj;->dgS:Lcom/xiaomi/push/service/XMPushService;

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/n;-><init>(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/push/service/aj;->dgR:Lcom/xiaomi/push/service/as;

    iput-object p2, p0, Lcom/xiaomi/push/service/aj;->dgR:Lcom/xiaomi/push/service/as;

    return-void
.end method


# virtual methods
.method public cxd()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "rebind the client. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/aj;->dgR:Lcom/xiaomi/push/service/as;

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cxe()V
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aj;->dgR:Lcom/xiaomi/push/service/as;

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x10

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/as;->cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/aj;->dgS:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/push/service/XMPushService;->cQj(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/smack/e;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/aj;->dgR:Lcom/xiaomi/push/service/as;

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/push/service/aj;->dgR:Lcom/xiaomi/push/service/as;

    iget-object v2, v2, Lcom/xiaomi/push/service/as;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/smack/e;->cxJ(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/aj;->dgR:Lcom/xiaomi/push/service/as;

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfV:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x10

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/as;->cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/aj;->dgS:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/push/service/XMPushService;->cQj(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/smack/e;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/aj;->dgR:Lcom/xiaomi/push/service/as;

    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/e;->cxG(Lcom/xiaomi/push/service/as;)V
    :try_end_0
    .catch Lcom/xiaomi/smack/XMPPException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/aj;->dgS:Lcom/xiaomi/push/service/XMPushService;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    goto :goto_0
.end method
