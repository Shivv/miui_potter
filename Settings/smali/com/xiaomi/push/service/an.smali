.class public Lcom/xiaomi/push/service/an;
.super Ljava/lang/Object;
.source "ServiceConfig.java"


# static fields
.field private static dgX:Lcom/xiaomi/push/service/an;

.field private static dgZ:Ljava/lang/String;


# instance fields
.field private dgY:Lcom/xiaomi/push/a/f;

.field private dha:Lcom/xiaomi/channel/commonutils/h/b;

.field private dhb:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/push/service/an;

    invoke-direct {v0}, Lcom/xiaomi/push/service/an;-><init>()V

    sput-object v0, Lcom/xiaomi/push/service/an;->dgX:Lcom/xiaomi/push/service/an;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/an;->dhb:Ljava/util/List;

    return-void
.end method

.method public static declared-synchronized cQA()Ljava/lang/String;
    .locals 4

    const-class v1, Lcom/xiaomi/push/service/an;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/service/an;->dgZ:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    sget-object v0, Lcom/xiaomi/push/service/an;->dgZ:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v2, "XMPushServiceConfig"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "DeviceUUID"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/xiaomi/push/service/an;->dgZ:Ljava/lang/String;

    sget-object v2, Lcom/xiaomi/push/service/an;->dgZ:Ljava/lang/String;

    if-nez v2, :cond_0

    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->cAr()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/xiaomi/push/service/an;->dgZ:Ljava/lang/String;

    sget-object v2, Lcom/xiaomi/push/service/an;->dgZ:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/push/service/an;->dgZ:Ljava/lang/String;

    const-string/jumbo v3, "DeviceUUID"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private cQD()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dha:Lcom/xiaomi/channel/commonutils/h/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/push/service/aX;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aX;-><init>(Lcom/xiaomi/push/service/an;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/an;->dha:Lcom/xiaomi/channel/commonutils/h/b;

    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dha:Lcom/xiaomi/channel/commonutils/h/b;

    invoke-static {v0}, Lcom/xiaomi/smack/c/b;->cwq(Lcom/xiaomi/channel/commonutils/h/b;)V

    return-void

    :cond_0
    return-void
.end method

.method private cQF()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/push/service/an;->load()V

    goto :goto_0
.end method

.method private cQG()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "XMCloudCfg"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v1}, Lcom/google/protobuf/micro/b;->div(Ljava/io/OutputStream;)Lcom/google/protobuf/micro/b;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/f;->cIm(Lcom/google/protobuf/micro/b;)V

    invoke-virtual {v0}, Lcom/google/protobuf/micro/b;->flush()V

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "save config failure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic cQH(Lcom/xiaomi/push/service/an;)Lcom/xiaomi/push/a/f;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    return-object v0
.end method

.method static synthetic cQJ(Lcom/xiaomi/push/service/an;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/push/service/an;->cQG()V

    return-void
.end method

.method static synthetic cQK(Lcom/xiaomi/push/service/an;Lcom/xiaomi/channel/commonutils/h/b;)Lcom/xiaomi/channel/commonutils/h/b;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/service/an;->dha:Lcom/xiaomi/channel/commonutils/h/b;

    return-object p1
.end method

.method static synthetic cQL(Lcom/xiaomi/push/service/an;Lcom/xiaomi/push/a/f;)Lcom/xiaomi/push/a/f;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    return-object p1
.end method

.method static synthetic cQM(Lcom/xiaomi/push/service/an;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dhb:Ljava/util/List;

    return-object v0
.end method

.method public static getInstance()Lcom/xiaomi/push/service/an;
    .locals 1

    sget-object v0, Lcom/xiaomi/push/service/an;->dgX:Lcom/xiaomi/push/service/an;

    return-object v0
.end method

.method private load()V
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "XMCloudCfg"

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v1}, Lcom/google/protobuf/micro/a;->dht(Ljava/io/InputStream;)Lcom/google/protobuf/micro/a;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/a/f;->cJP(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyJ(Ljava/io/InputStream;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    if-eqz v0, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "load config failure: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyJ(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/b/a;->cyJ(Ljava/io/InputStream;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/xiaomi/push/a/f;

    invoke-direct {v0}, Lcom/xiaomi/push/a/f;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method cQB()I
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/push/service/an;->cQF()V

    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/f;->cJL()I

    move-result v0

    return v0
.end method

.method public cQC()Lcom/xiaomi/push/a/f;
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/push/service/an;->cQF()V

    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dgY:Lcom/xiaomi/push/a/f;

    return-object v0
.end method

.method public declared-synchronized cQE(Lcom/xiaomi/push/service/f;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dhb:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method cQI(Lcom/xiaomi/push/a/g;)V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/push/a/g;->cKe()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dhb:Ljava/util/List;

    iget-object v2, p0, Lcom/xiaomi/push/service/an;->dhb:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/xiaomi/push/service/f;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/push/service/f;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    array-length v2, v0

    :goto_1
    if-lt v1, v2, :cond_2

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/push/a/g;->cKh()I

    move-result v0

    invoke-virtual {p0}, Lcom/xiaomi/push/service/an;->cQB()I

    move-result v2

    if-le v0, v2, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/push/service/an;->cQD()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    aget-object v3, v0, v1

    invoke-virtual {v3, p1}, Lcom/xiaomi/push/service/f;->cMK(Lcom/xiaomi/push/a/g;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method declared-synchronized clear()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/an;->dhb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
