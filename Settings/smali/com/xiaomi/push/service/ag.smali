.class Lcom/xiaomi/push/service/ag;
.super Ljava/lang/Object;
.source "JobScheduler.java"


# instance fields
.field dgt:Lcom/xiaomi/push/service/C;

.field private dgu:J

.field dgv:Z

.field final dgw:Ljava/lang/Object;

.field dgx:J

.field type:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/ag;->dgw:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method cPo(J)V
    .locals 3

    iget-object v1, p0, Lcom/xiaomi/push/service/ag;->dgw:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-wide p1, p0, Lcom/xiaomi/push/service/ag;->dgu:J

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cancel()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/xiaomi/push/service/ag;->dgw:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lcom/xiaomi/push/service/ag;->dgv:Z

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/push/service/ag;->dgv:Z

    monitor-exit v3

    return v0

    :cond_1
    iget-wide v4, p0, Lcom/xiaomi/push/service/ag;->dgx:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-gtz v2, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_0

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
