.class public Lcom/xiaomi/push/service/aw;
.super Ljava/lang/Object;
.source "MIIDSPCacheHelper.java"


# static fields
.field private static volatile dhL:Lcom/xiaomi/push/service/aw;


# instance fields
.field private dhM:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "mipush"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/service/aw;->dhM:Landroid/content/SharedPreferences;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aw;
    .locals 2

    sget-object v0, Lcom/xiaomi/push/service/aw;->dhL:Lcom/xiaomi/push/service/aw;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/push/service/aw;->dhL:Lcom/xiaomi/push/service/aw;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/push/service/aw;

    const-class v0, Lcom/xiaomi/push/service/aw;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/service/aw;->dhL:Lcom/xiaomi/push/service/aw;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/push/service/aw;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aw;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/push/service/aw;->dhL:Lcom/xiaomi/push/service/aw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized cRI(Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aw;->dhM:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "miid"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string/jumbo p1, "0"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
