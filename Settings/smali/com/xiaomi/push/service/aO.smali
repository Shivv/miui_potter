.class Lcom/xiaomi/push/service/aO;
.super Lcom/xiaomi/push/service/n;
.source "PushClientsManager.java"


# instance fields
.field final synthetic dir:Lcom/xiaomi/push/service/as;

.field dis:I

.field dit:Ljava/lang/String;

.field diu:Ljava/lang/String;

.field notifyType:I


# direct methods
.method public constructor <init>(Lcom/xiaomi/push/service/as;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/push/service/aO;->dir:Lcom/xiaomi/push/service/as;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/n;-><init>(I)V

    return-void
.end method


# virtual methods
.method public cSx(IILjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/n;
    .locals 0

    iput p1, p0, Lcom/xiaomi/push/service/aO;->notifyType:I

    iput p2, p0, Lcom/xiaomi/push/service/aO;->dis:I

    iput-object p4, p0, Lcom/xiaomi/push/service/aO;->diu:Ljava/lang/String;

    iput-object p3, p0, Lcom/xiaomi/push/service/aO;->dit:Ljava/lang/String;

    return-object p0
.end method

.method public cxd()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "notify job"

    return-object v0
.end method

.method public cxe()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/push/service/aO;->dir:Lcom/xiaomi/push/service/as;

    iget v1, p0, Lcom/xiaomi/push/service/aO;->notifyType:I

    iget v2, p0, Lcom/xiaomi/push/service/aO;->dis:I

    iget-object v3, p0, Lcom/xiaomi/push/service/aO;->diu:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/push/service/as;->cRo(Lcom/xiaomi/push/service/as;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " ignore notify client :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/aO;->dir:Lcom/xiaomi/push/service/as;

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aO;->dir:Lcom/xiaomi/push/service/as;

    iget v1, p0, Lcom/xiaomi/push/service/aO;->notifyType:I

    iget v2, p0, Lcom/xiaomi/push/service/aO;->dis:I

    iget-object v3, p0, Lcom/xiaomi/push/service/aO;->dit:Ljava/lang/String;

    iget-object v4, p0, Lcom/xiaomi/push/service/aO;->diu:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/xiaomi/push/service/as;->cRr(Lcom/xiaomi/push/service/as;IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
