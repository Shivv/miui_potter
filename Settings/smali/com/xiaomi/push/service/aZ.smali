.class Lcom/xiaomi/push/service/aZ;
.super Lcom/xiaomi/push/service/n;
.source "MIPushEventProcessor.java"


# instance fields
.field final synthetic diX:Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

.field final synthetic diY:Ljava/lang/String;

.field final synthetic diZ:Ljava/lang/String;

.field final synthetic dja:Lcom/xiaomi/push/service/XMPushService;


# direct methods
.method constructor <init>(ILcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p2, p0, Lcom/xiaomi/push/service/aZ;->dja:Lcom/xiaomi/push/service/XMPushService;

    iput-object p3, p0, Lcom/xiaomi/push/service/aZ;->diX:Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    iput-object p4, p0, Lcom/xiaomi/push/service/aZ;->diZ:Ljava/lang/String;

    iput-object p5, p0, Lcom/xiaomi/push/service/aZ;->diY:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/n;-><init>(I)V

    return-void
.end method


# virtual methods
.method public cxd()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "send wrong message ack for message."

    return-object v0
.end method

.method public cxe()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aZ;->dja:Lcom/xiaomi/push/service/XMPushService;

    iget-object v1, p0, Lcom/xiaomi/push/service/aZ;->diX:Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-static {v0, v1}, Lcom/xiaomi/push/service/T;->cOW(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    iget-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    iget-object v2, p0, Lcom/xiaomi/push/service/aZ;->diZ:Ljava/lang/String;

    const-string/jumbo v3, "error"

    invoke-virtual {v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    iget-object v2, p0, Lcom/xiaomi/push/service/aZ;->diY:Ljava/lang/String;

    const-string/jumbo v3, "reason"

    invoke-virtual {v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/aZ;->dja:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v1, v0}, Lcom/xiaomi/push/service/aF;->cSd(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V
    :try_end_0
    .catch Lcom/xiaomi/smack/XMPPException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/aZ;->dja:Lcom/xiaomi/push/service/XMPushService;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    goto :goto_0
.end method
