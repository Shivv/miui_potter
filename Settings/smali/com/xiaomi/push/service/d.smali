.class Lcom/xiaomi/push/service/d;
.super Ljava/lang/Object;
.source "ReconnectionManager.java"


# static fields
.field private static deC:I


# instance fields
.field private deA:I

.field private deB:Lcom/xiaomi/push/service/XMPushService;

.field private deD:I

.field private dez:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x493e0

    sput v0, Lcom/xiaomi/push/service/d;->deC:I

    return-void
.end method

.method public constructor <init>(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/push/service/d;->deD:I

    iput-object p1, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/xiaomi/push/service/d;->deA:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/push/service/d;->dez:J

    return-void
.end method

.method private cMH()I
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/xiaomi/push/service/d;->deD:I

    const/16 v3, 0x8

    if-gt v2, v3, :cond_0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    iget v4, p0, Lcom/xiaomi/push/service/d;->deD:I

    const/4 v5, 0x4

    if-gt v4, v5, :cond_1

    iget v4, p0, Lcom/xiaomi/push/service/d;->deD:I

    if-gt v4, v0, :cond_2

    iget-wide v2, p0, Lcom/xiaomi/push/service/d;->dez:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    return v1

    :cond_0
    const v0, 0x493e0

    return v0

    :cond_1
    const-wide v0, 0x40ed4c0000000000L    # 60000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0

    :cond_2
    const-wide v0, 0x40c3880000000000L    # 10000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/push/service/d;->dez:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x493e0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    :goto_0
    if-nez v0, :cond_6

    iget v0, p0, Lcom/xiaomi/push/service/d;->deA:I

    sget v1, Lcom/xiaomi/push/service/d;->deC:I

    if-ge v0, v1, :cond_5

    iget v0, p0, Lcom/xiaomi/push/service/d;->deA:I

    iget v1, p0, Lcom/xiaomi/push/service/d;->deA:I

    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, p0, Lcom/xiaomi/push/service/d;->deA:I

    return v0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/xiaomi/push/service/d;->deA:I

    return v0

    :cond_6
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/xiaomi/push/service/d;->deA:I

    return v1
.end method


# virtual methods
.method public cMG()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/push/service/d;->dez:J

    iget-object v0, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPp(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/push/service/d;->deD:I

    return-void
.end method

.method public cMI(Z)V
    .locals 6

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/XMPushService;->cPE()Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "should not reconnect as no client or network."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/XMPushService;->cPI(I)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/xiaomi/push/service/d;->cMH()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/service/XMPushService;->cPI(I)Z

    move-result v1

    if-eqz v1, :cond_6

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "schedule reconnect in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    new-instance v2, Lcom/xiaomi/push/service/U;

    iget-object v3, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v3}, Lcom/xiaomi/push/service/U;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Lcom/xiaomi/push/service/XMPushService;->cPu(Lcom/xiaomi/push/service/n;J)V

    iget v0, p0, Lcom/xiaomi/push/service/d;->deD:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_7

    :cond_2
    :goto_2
    iget v0, p0, Lcom/xiaomi/push/service/d;->deD:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/xiaomi/push/service/ap;->cQN()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/XMPushService;->cPI(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/XMPushService;->cPp(I)V

    iget-object v0, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    new-instance v1, Lcom/xiaomi/push/service/U;

    iget-object v2, p0, Lcom/xiaomi/push/service/d;->deB:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v2}, Lcom/xiaomi/push/service/U;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/xiaomi/push/service/d;->deD:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/push/service/d;->deD:I

    goto :goto_3

    :cond_5
    return-void

    :cond_6
    iget v1, p0, Lcom/xiaomi/push/service/d;->deD:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/xiaomi/push/service/d;->deD:I

    goto :goto_1

    :cond_7
    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/e/e;->dgp()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/xiaomi/push/service/ap;->cQQ()V

    goto :goto_2
.end method
