.class Lcom/xiaomi/push/service/Y;
.super Ljava/lang/Object;
.source "XMPushService.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic dge:Ljava/lang/String;

.field final synthetic dgf:Ljava/lang/String;

.field final synthetic dgg:Lcom/xiaomi/push/service/o;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/xiaomi/push/service/o;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/service/Y;->dgg:Lcom/xiaomi/push/service/o;

    iput-object p2, p0, Lcom/xiaomi/push/service/Y;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/xiaomi/push/service/Y;->dgf:Ljava/lang/String;

    iput-object p4, p0, Lcom/xiaomi/push/service/Y;->dge:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/4 v2, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/service/Y;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/O;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/O;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/Y;->dgf:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/O;->cOn(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/service/module/a;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMj()I

    move-result v1

    iget-object v5, p0, Lcom/xiaomi/push/service/Y;->dge:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/xiaomi/push/service/XMPushService;->cPt(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMo()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-ltz v1, :cond_2

    move v1, v2

    :goto_1
    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/push/service/Y;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/push/service/O;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/O;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMr()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/xiaomi/push/service/O;->cOo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "XMPushService remove some geofence message failed. message_id:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMr()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMk()[B

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v1, v6, v7}, Lcom/xiaomi/push/service/T;->cOH([BJ)Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v6, p0, Lcom/xiaomi/push/service/Y;->dgg:Lcom/xiaomi/push/service/o;

    iget-object v6, v6, Lcom/xiaomi/push/service/o;->deS:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v6, v10, v1, v5, v2}, Lcom/xiaomi/push/service/T;->cOM(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;[BLandroid/content/Intent;Z)V

    iget-object v1, p0, Lcom/xiaomi/push/service/Y;->dgg:Lcom/xiaomi/push/service/o;

    iget-object v1, v1, Lcom/xiaomi/push/service/o;->deS:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v1}, Lcom/xiaomi/push/service/O;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/O;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMr()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/xiaomi/push/service/O;->cOo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "show some exit geofence message. then remove this message failed. message_id:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMr()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const-string/jumbo v0, "Geo canBeShownMessage content null"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "Geo canBeShownMessage intent null"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
