.class public abstract Lcom/xiaomi/push/service/b;
.super Ljava/lang/Object;
.source "PushConstants.java"


# static fields
.field public static ddL:Ljava/lang/String;

.field public static ddM:Ljava/lang/String;

.field public static ddN:Ljava/lang/String;

.field public static ddO:Ljava/lang/String;

.field public static ddP:Ljava/lang/String;

.field public static ddQ:Ljava/lang/String;

.field public static ddR:Ljava/lang/String;

.field public static ddS:Ljava/lang/String;

.field public static ddT:Ljava/lang/String;

.field public static ddU:Ljava/lang/String;

.field public static ddV:Ljava/lang/String;

.field public static ddW:Ljava/lang/String;

.field public static ddX:Ljava/lang/String;

.field public static ddY:Ljava/lang/String;

.field public static ddZ:Ljava/lang/String;

.field public static dea:Ljava/lang/String;

.field public static deb:Ljava/lang/String;

.field public static dec:Ljava/lang/String;

.field public static ded:Ljava/lang/String;

.field public static dee:Ljava/lang/String;

.field public static def:Ljava/lang/String;

.field public static deg:Ljava/lang/String;

.field public static deh:Ljava/lang/String;

.field public static dei:Ljava/lang/String;

.field public static dej:Ljava/lang/String;

.field public static dek:Ljava/lang/String;

.field public static del:Ljava/lang/String;

.field public static dem:Ljava/lang/String;

.field public static den:Ljava/lang/String;

.field public static deo:Ljava/lang/String;

.field public static dep:Ljava/lang/String;

.field public static deq:Ljava/lang/String;

.field public static der:Ljava/lang/String;

.field public static des:Ljava/lang/String;

.field public static det:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "1"

    sput-object v0, Lcom/xiaomi/push/service/b;->dep:Ljava/lang/String;

    const-string/jumbo v0, "2"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddR:Ljava/lang/String;

    const-string/jumbo v0, "3"

    sput-object v0, Lcom/xiaomi/push/service/b;->ded:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.OPEN_CHANNEL"

    sput-object v0, Lcom/xiaomi/push/service/b;->dea:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.SEND_MESSAGE"

    sput-object v0, Lcom/xiaomi/push/service/b;->dek:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.SEND_IQ"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddV:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.BATCH_SEND_MESSAGE"

    sput-object v0, Lcom/xiaomi/push/service/b;->der:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.SEND_PRES"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddM:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.CLOSE_CHANNEL"

    sput-object v0, Lcom/xiaomi/push/service/b;->det:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.FORCE_RECONN"

    sput-object v0, Lcom/xiaomi/push/service/b;->del:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.RESET_CONN"

    sput-object v0, Lcom/xiaomi/push/service/b;->den:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.UPDATE_CHANNEL_INFO"

    sput-object v0, Lcom/xiaomi/push/service/b;->dee:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.SEND_STATS"

    sput-object v0, Lcom/xiaomi/push/service/b;->deb:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.CHANGE_HOST"

    sput-object v0, Lcom/xiaomi/push/service/b;->def:Ljava/lang/String;

    const-string/jumbo v0, "com.xiaomi.push.PING_TIMER"

    sput-object v0, Lcom/xiaomi/push/service/b;->dej:Ljava/lang/String;

    const-string/jumbo v0, "ext_user_id"

    sput-object v0, Lcom/xiaomi/push/service/b;->dem:Ljava/lang/String;

    const-string/jumbo v0, "ext_chid"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddO:Ljava/lang/String;

    const-string/jumbo v0, "ext_sid"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddY:Ljava/lang/String;

    const-string/jumbo v0, "ext_token"

    sput-object v0, Lcom/xiaomi/push/service/b;->deh:Ljava/lang/String;

    const-string/jumbo v0, "ext_auth_method"

    sput-object v0, Lcom/xiaomi/push/service/b;->dei:Ljava/lang/String;

    const-string/jumbo v0, "ext_security"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddP:Ljava/lang/String;

    const-string/jumbo v0, "ext_kick"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddQ:Ljava/lang/String;

    const-string/jumbo v0, "ext_client_attr"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddX:Ljava/lang/String;

    const-string/jumbo v0, "ext_cloud_attr"

    sput-object v0, Lcom/xiaomi/push/service/b;->deo:Ljava/lang/String;

    const-string/jumbo v0, "ext_pkg_name"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    const-string/jumbo v0, "ext_notify_id"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddN:Ljava/lang/String;

    const-string/jumbo v0, "ext_notify_type"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddZ:Ljava/lang/String;

    const-string/jumbo v0, "ext_session"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddS:Ljava/lang/String;

    const-string/jumbo v0, "sig"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddL:Ljava/lang/String;

    const-string/jumbo v0, "ext_notify_title"

    sput-object v0, Lcom/xiaomi/push/service/b;->des:Ljava/lang/String;

    const-string/jumbo v0, "ext_notify_description"

    sput-object v0, Lcom/xiaomi/push/service/b;->deg:Ljava/lang/String;

    const-string/jumbo v0, "ext_messenger"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddW:Ljava/lang/String;

    const-string/jumbo v0, "title"

    sput-object v0, Lcom/xiaomi/push/service/b;->ddU:Ljava/lang/String;

    const-string/jumbo v0, "description"

    sput-object v0, Lcom/xiaomi/push/service/b;->deq:Ljava/lang/String;

    const-string/jumbo v0, "notifyId"

    sput-object v0, Lcom/xiaomi/push/service/b;->dec:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cMv(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    const-string/jumbo v0, "ERROR_OK"

    return-object v0

    :pswitch_1
    const-string/jumbo v0, "ERROR_SERVICE_NOT_INSTALLED"

    return-object v0

    :pswitch_2
    const-string/jumbo v0, "ERROR_NETWORK_NOT_AVAILABLE"

    return-object v0

    :pswitch_3
    const-string/jumbo v0, "ERROR_NETWORK_FAILED"

    return-object v0

    :pswitch_4
    const-string/jumbo v0, "ERROR_ACCESS_DENIED"

    return-object v0

    :pswitch_5
    const-string/jumbo v0, "ERROR_AUTH_FAILED"

    return-object v0

    :pswitch_6
    const-string/jumbo v0, "ERROR_MULTI_LOGIN"

    return-object v0

    :pswitch_7
    const-string/jumbo v0, "ERROR_SERVER_ERROR"

    return-object v0

    :pswitch_8
    const-string/jumbo v0, "ERROR_RECEIVE_TIMEOUT"

    return-object v0

    :pswitch_9
    const-string/jumbo v0, "ERROR_READ_ERROR"

    return-object v0

    :pswitch_a
    const-string/jumbo v0, "ERROR_SEND_ERROR"

    return-object v0

    :pswitch_b
    const-string/jumbo v0, "ERROR_RESET"

    return-object v0

    :pswitch_c
    const-string/jumbo v0, "ERROR_NO_CLIENT"

    return-object v0

    :pswitch_d
    const-string/jumbo v0, "ERROR_SERVER_STREAM"

    return-object v0

    :pswitch_e
    const-string/jumbo v0, "ERROR_THREAD_BLOCK"

    return-object v0

    :pswitch_f
    const-string/jumbo v0, "ERROR_SERVICE_DESTROY"

    return-object v0

    :pswitch_10
    const-string/jumbo v0, "ERROR_SESSION_CHANGED"

    return-object v0

    :pswitch_11
    const-string/jumbo v0, "ERROR_READ_TIMEOUT"

    return-object v0

    :pswitch_12
    const-string/jumbo v0, "ERROR_CONNECTIING_TIMEOUT"

    return-object v0

    :pswitch_13
    const-string/jumbo v0, "ERROR_USER_BLOCKED"

    return-object v0

    :pswitch_14
    const-string/jumbo v0, "ERROR_REDIRECT"

    return-object v0

    :pswitch_15
    const-string/jumbo v0, "ERROR_BIND_TIMEOUT"

    return-object v0

    :pswitch_16
    const-string/jumbo v0, "ERROR_PING_TIMEOUT"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method
