.class public Lcom/xiaomi/push/service/XMPushService;
.super Landroid/app/Service;
.source "XMPushService.java"

# interfaces
.implements Lcom/xiaomi/smack/a;


# static fields
.field private static final dgH:I

.field public static dgJ:I


# instance fields
.field private dgA:J

.field private dgB:Ljava/util/ArrayList;

.field private dgC:Lcom/xiaomi/a/g;

.field final dgD:Landroid/content/BroadcastReceiver;

.field dgE:Landroid/os/Messenger;

.field private dgF:Lcom/xiaomi/smack/k;

.field private dgG:Ljava/lang/String;

.field private dgI:Lcom/xiaomi/push/service/aP;

.field private dgK:Lcom/xiaomi/smack/e;

.field private dgL:Lcom/xiaomi/push/service/x;

.field private dgM:Lcom/xiaomi/push/service/J;

.field private dgN:Lcom/xiaomi/push/service/d;

.field private dgO:Lcom/xiaomi/smack/h;

.field private dgP:Lcom/xiaomi/push/service/r;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    sput v0, Lcom/xiaomi/push/service/XMPushService;->dgH:I

    const-string/jumbo v0, "app.chat.xiaomi.net"

    const-string/jumbo v1, "app.chat.xiaomi.net"

    invoke-static {v0, v1}, Lcom/xiaomi/c/g;->cUS(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "app.chat.xiaomi.net"

    const-string/jumbo v1, "42.62.94.2:443"

    invoke-static {v0, v1}, Lcom/xiaomi/c/g;->cUS(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "app.chat.xiaomi.net"

    const-string/jumbo v1, "114.54.23.2"

    invoke-static {v0, v1}, Lcom/xiaomi/c/g;->cUS(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "app.chat.xiaomi.net"

    const-string/jumbo v1, "111.13.142.2"

    invoke-static {v0, v1}, Lcom/xiaomi/c/g;->cUS(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "app.chat.xiaomi.net"

    const-string/jumbo v1, "111.206.200.2"

    invoke-static {v0, v1}, Lcom/xiaomi/c/g;->cUS(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    sput v0, Lcom/xiaomi/push/service/XMPushService;->dgJ:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgA:J

    iput-object v2, p0, Lcom/xiaomi/push/service/XMPushService;->dgI:Lcom/xiaomi/push/service/aP;

    iput-object v2, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    iput-object v2, p0, Lcom/xiaomi/push/service/XMPushService;->dgE:Landroid/os/Messenger;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgB:Ljava/util/ArrayList;

    new-instance v0, Lcom/xiaomi/push/service/W;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/W;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgF:Lcom/xiaomi/smack/k;

    new-instance v0, Lcom/xiaomi/push/service/o;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/o;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgD:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private cPB(Z)V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgA:J

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v0}, Lcom/xiaomi/smack/e;->cxC()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    new-instance v0, Lcom/xiaomi/push/service/aD;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/push/service/aD;-><init>(Lcom/xiaomi/push/service/XMPushService;Z)V

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v3}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v0}, Lcom/xiaomi/smack/e;->cxL()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/g/b;->cAE(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/push/service/S;

    const/4 v1, 0x0

    const/16 v2, 0x11

    invoke-direct {v0, p0, v2, v1}, Lcom/xiaomi/push/service/S;-><init>(Lcom/xiaomi/push/service/XMPushService;ILjava/lang/Exception;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    invoke-virtual {p0, v3}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    goto :goto_0
.end method

.method private cPF(Z)V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->cAs()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.NETWORK_BLOCKED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.NETWORK_CONNECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private cPH(Ljava/lang/String;Landroid/content/Intent;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, Lcom/xiaomi/push/service/b;->dem:Ljava/lang/String;

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v3

    invoke-virtual {v3, p1, v2}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_0

    sget-object v3, Lcom/xiaomi/push/service/b;->ddS:Ljava/lang/String;

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/xiaomi/push/service/b;->ddP:Ljava/lang/String;

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v2, Lcom/xiaomi/push/service/as;->dhv:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    :goto_1
    iget-object v2, v2, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "security changed. chid = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " sechash = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/c/c;->czm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v5, v2, Lcom/xiaomi/push/service/as;->dhv:Ljava/lang/String;

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "session changed. old session="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, v2, Lcom/xiaomi/push/service/as;->dhv:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, ", new session="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " chid = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    move v0, v1

    goto :goto_1
.end method

.method private cPJ()V
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPE()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/xiaomi/push/service/a/d;->stop()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/xiaomi/push/service/a/d;->cMg()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/xiaomi/push/service/a/d;->cMh(Z)V

    goto :goto_0
.end method

.method static synthetic cPL(Lcom/xiaomi/push/service/XMPushService;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgG:Ljava/lang/String;

    return-object v0
.end method

.method private cPM(Landroid/content/Intent;)V
    .locals 5

    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/push/service/b;->ddS:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ext_packet"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "ext_encrypt"

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    new-instance v3, Lcom/xiaomi/smack/packet/c;

    invoke-direct {v3, v2}, Lcom/xiaomi/smack/packet/c;-><init>(Landroid/os/Bundle;)V

    invoke-direct {p0, v3, v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPv(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/smack/packet/d;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/packet/c;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/c;->cvN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/c;->cvx()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v1

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/xiaomi/a/e;->cBY(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;)Lcom/xiaomi/a/e;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/push/service/g;

    invoke-direct {v1, p0, v0}, Lcom/xiaomi/push/service/g;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/a/e;)V

    invoke-direct {p0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    return-void

    :cond_0
    return-void
.end method

.method private cPN()Ljava/lang/String;
    .locals 10

    const/4 v4, 0x0

    invoke-static {}, Lcom/xiaomi/channel/commonutils/h/e;->cAU()V

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    const-wide/16 v0, 0x0

    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "wait region :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " cost = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v2

    :cond_1
    const-string/jumbo v0, "ro.miui.region"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/e;->cAj(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    :try_start_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v6

    const-wide/32 v8, 0x124f80

    cmp-long v3, v0, v8

    if-gtz v3, :cond_4

    const/4 v3, 0x1

    :goto_4
    if-nez v3, :cond_0

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "ro.product.locale.region"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/e;->cAj(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_3
    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_3

    :cond_4
    move v3, v4

    goto :goto_4

    :cond_5
    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/android/e;->cAi(Ljava/lang/String;)Lcom/xiaomi/channel/commonutils/android/Region;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/channel/commonutils/android/Region;->name()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method static synthetic cPP(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/smack/h;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgO:Lcom/xiaomi/smack/h;

    return-object v0
.end method

.method private cPQ(Ljava/lang/String;I)V
    .locals 7

    const/4 v4, 0x0

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/aR;->cSE(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/aR;->cSO(Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/push/service/as;

    if-eqz v2, :cond_2

    new-instance v0, Lcom/xiaomi/push/service/bc;

    move-object v1, p0

    move v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/push/service/bc;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto :goto_0
.end method

.method private cPR(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "Leave"

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p3}, Lcom/xiaomi/push/service/az;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/az;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Lcom/xiaomi/push/service/az;->cRU(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    return v0

    :cond_1
    const-string/jumbo v0, "Enter"

    invoke-static {p3}, Lcom/xiaomi/push/service/az;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/az;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/xiaomi/push/service/az;->cRZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "update geofence statue failed geo_id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return v2
.end method

.method static synthetic cPS(Lcom/xiaomi/push/service/XMPushService;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/XMPushService;->cPT(Landroid/content/Intent;)V

    return-void
.end method

.method private cPT(Landroid/content/Intent;)V
    .locals 12

    const/4 v4, 0x2

    const/4 v9, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    sget-object v0, Lcom/xiaomi/push/service/b;->dea:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/xiaomi/push/service/b;->ddO:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/push/service/b;->ddP:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    if-nez v0, :cond_5

    const-string/jumbo v0, "channel id is empty, do nothing!"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/xiaomi/push/service/b;->del:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/xiaomi/push/service/b;->det:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/xiaomi/push/service/b;->dek:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    sget-object v0, Lcom/xiaomi/push/service/b;->der:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Lcom/xiaomi/push/service/b;->ddV:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    sget-object v0, Lcom/xiaomi/push/service/b;->ddM:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    sget-object v0, Lcom/xiaomi/push/service/b;->den:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/xiaomi/push/service/b;->dee:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.REGISTER_APP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.SEND_MESSAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    :cond_3
    const-string/jumbo v0, "mipush_app_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mipush_payload"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    const-string/jumbo v2, "com.xiaomi.mipush.MESSAGE_CACHE"

    invoke-virtual {p1, v2, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.xiaomi.mipush.UNREGISTER_APP"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    :goto_1
    invoke-virtual {p0, v0, v1, v2}, Lcom/xiaomi/push/service/XMPushService;->cQk(Ljava/lang/String;[BZ)V

    goto/16 :goto_0

    :cond_4
    const-string/jumbo v0, "security is empty. ignore."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0, v0, p1}, Lcom/xiaomi/push/service/XMPushService;->cPH(Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v1

    invoke-direct {p0, v0, p1}, Lcom/xiaomi/push/service/XMPushService;->cQc(Ljava/lang/String;Landroid/content/Intent;)Lcom/xiaomi/push/service/as;

    move-result-object v2

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0, v9}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgP:Lcom/xiaomi/push/service/r;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/r;->cNB(Landroid/content/Context;Lcom/xiaomi/push/service/as;ZILjava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, v2, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v6, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-eq v0, v6, :cond_8

    if-nez v1, :cond_9

    iget-object v0, v2, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfV:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-eq v0, v1, :cond_a

    iget-object v0, v2, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfT:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-ne v0, v1, :cond_1

    iget-object v6, p0, Lcom/xiaomi/push/service/XMPushService;->dgP:Lcom/xiaomi/push/service/r;

    move-object v7, p0

    move-object v8, v2

    move v10, v3

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/xiaomi/push/service/r;->cNB(Landroid/content/Context;Lcom/xiaomi/push/service/as;ZILjava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    new-instance v0, Lcom/xiaomi/push/service/t;

    invoke-direct {v0, p0, v2}, Lcom/xiaomi/push/service/t;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    goto/16 :goto_0

    :cond_9
    new-instance v0, Lcom/xiaomi/push/service/aj;

    invoke-direct {v0, p0, v2}, Lcom/xiaomi/push/service/aj;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    goto/16 :goto_0

    :cond_a
    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, v2, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    aput-object v1, v0, v3

    iget-object v1, v2, Lcom/xiaomi/push/service/as;->userId:Ljava/lang/String;

    const-string/jumbo v2, "the client is binding. %1$s %2$s."

    invoke-static {v1}, Lcom/xiaomi/push/service/as;->cRm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/push/service/b;->ddO:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/push/service/b;->dem:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Service called close channel chid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " res = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Lcom/xiaomi/push/service/as;->cRm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_c

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    move-object v1, p0

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, Lcom/xiaomi/push/service/XMPushService;->cPx(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/aR;->cSJ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, v4}, Lcom/xiaomi/push/service/XMPushService;->cPQ(Ljava/lang/String;I)V

    goto :goto_2

    :cond_d
    invoke-direct {p0, v2, v4}, Lcom/xiaomi/push/service/XMPushService;->cPQ(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_e
    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/XMPushService;->cPM(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_f
    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/XMPushService;->cPZ(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_10
    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/push/service/b;->ddS:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "ext_packet"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    new-instance v4, Lcom/xiaomi/smack/packet/e;

    invoke-direct {v4, v3}, Lcom/xiaomi/smack/packet/e;-><init>(Landroid/os/Bundle;)V

    invoke-direct {p0, v4, v0, v2}, Lcom/xiaomi/push/service/XMPushService;->cPv(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/smack/packet/d;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/d;->cvN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/d;->cvx()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v1

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/xiaomi/a/e;->cBY(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;)Lcom/xiaomi/a/e;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/push/service/g;

    invoke-direct {v1, p0, v0}, Lcom/xiaomi/push/service/g;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/a/e;)V

    invoke-direct {p0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    goto/16 :goto_0

    :cond_11
    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/push/service/b;->ddS:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "ext_packet"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    new-instance v4, Lcom/xiaomi/smack/packet/i;

    invoke-direct {v4, v3}, Lcom/xiaomi/smack/packet/i;-><init>(Landroid/os/Bundle;)V

    invoke-direct {p0, v4, v0, v2}, Lcom/xiaomi/push/service/XMPushService;->cPv(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/smack/packet/d;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/d;->cvN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/d;->cvx()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v1

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/xiaomi/a/e;->cBY(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;)Lcom/xiaomi/a/e;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/push/service/g;

    invoke-direct {v1, p0, v0}, Lcom/xiaomi/push/service/g;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/a/e;)V

    invoke-direct {p0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    goto/16 :goto_0

    :cond_12
    sget-object v0, Lcom/xiaomi/push/service/b;->ddO:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/push/service/b;->dem:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "request reset connection from chid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    sget-object v2, Lcom/xiaomi/push/service/b;->ddP:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfT:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cQs()Lcom/xiaomi/smack/e;

    move-result-object v0

    if-nez v0, :cond_14

    :cond_13
    new-instance v0, Lcom/xiaomi/push/service/aq;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aq;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    goto/16 :goto_0

    :cond_14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3a98

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/xiaomi/smack/e;->cxs(J)Z

    move-result v0

    if-eqz v0, :cond_13

    goto/16 :goto_0

    :cond_15
    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/aR;->cSJ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_17

    sget-object v0, Lcom/xiaomi/push/service/b;->ddO:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/xiaomi/push/service/b;->dem:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_18

    :goto_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_19

    invoke-virtual {v1, v0, v4}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v5

    :cond_16
    :goto_4
    if-eqz v5, :cond_1

    sget-object v0, Lcom/xiaomi/push/service/b;->ddX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    :goto_5
    sget-object v0, Lcom/xiaomi/push/service/b;->deo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/push/service/b;->deo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    goto/16 :goto_0

    :cond_17
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "open channel should be called first before update info, pkg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void

    :cond_18
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_3

    :cond_19
    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/aR;->cSE(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_16

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_16

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/service/as;

    move-object v5, v0

    goto :goto_4

    :cond_1a
    sget-object v0, Lcom/xiaomi/push/service/b;->ddX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/xiaomi/push/service/as;->dht:Ljava/lang/String;

    goto :goto_5

    :cond_1b
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/service/R;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/R;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/R;->cOv()Z

    move-result v0

    if-nez v0, :cond_1e

    :cond_1c
    const-string/jumbo v0, "mipush_payload"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    const-string/jumbo v0, "mipush_app_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v0, "mipush_env_chanage"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string/jumbo v1, "mipush_env_type"

    invoke-virtual {p1, v1, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {p0}, Lcom/xiaomi/push/service/B;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/B;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/xiaomi/push/service/B;->cNU(Ljava/lang/String;)V

    if-nez v0, :cond_1f

    :cond_1d
    invoke-virtual {p0, v4, v5}, Lcom/xiaomi/push/service/XMPushService;->cPY([BLjava/lang/String;)V

    goto/16 :goto_0

    :cond_1e
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/service/R;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/R;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/R;->cOt()I

    move-result v0

    if-nez v0, :cond_1c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "register without being provisioned. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "mipush_app_package"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void

    :cond_1f
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.xmsf"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d

    new-instance v0, Lcom/xiaomi/push/service/ao;

    const/16 v2, 0xe

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/push/service/ao;-><init>(Lcom/xiaomi/push/service/XMPushService;II[BLjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    goto/16 :goto_0

    :cond_20
    const-string/jumbo v0, "com.xiaomi.mipush.UNREGISTER_APP"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/xiaomi/push/service/aA;->dhU:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.CLEAR_NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_29

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.SET_NOTIFICATION_TYPE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2b

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.DISABLE_PUSH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_30

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.DISABLE_PUSH_MESSAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    :cond_21
    const-string/jumbo v0, "mipush_app_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v0, "mipush_payload"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    const-string/jumbo v0, "mipush_app_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "mipush_app_token"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.DISABLE_PUSH_MESSAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_34

    :goto_6
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.ENABLE_PUSH_MESSAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_35

    :goto_7
    if-eqz v5, :cond_36

    invoke-static {v2, v5}, Lcom/xiaomi/push/service/av;->cRD(Ljava/lang/String;[B)V

    new-instance v0, Lcom/xiaomi/push/service/ah;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/push/service/ah;-><init>(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.ENABLE_PUSH_MESSAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    if-nez v0, :cond_1

    new-instance v0, Lcom/xiaomi/push/service/J;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/J;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    invoke-virtual {p0, v1, v0}, Lcom/xiaomi/push/service/XMPushService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_22
    invoke-static {p0}, Lcom/xiaomi/push/service/B;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/B;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/xiaomi/push/service/B;->cNW(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_23
    const-string/jumbo v0, "uninstall_pkg_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_25

    :cond_24
    return-void

    :cond_25
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_24

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move v9, v3

    :goto_8
    const-string/jumbo v1, "com.xiaomi.channel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_27

    :cond_26
    const-string/jumbo v1, "pref_registered_pkg_names"

    invoke-virtual {p0, v1, v3}, Lcom/xiaomi/push/service/XMPushService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v9, :cond_1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {p0, v0}, Lcom/xiaomi/push/service/h;->cMR(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_28

    :goto_9
    invoke-static {p0, v0}, Lcom/xiaomi/push/service/h;->cNm(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    :try_start_1
    invoke-static {v0, v2}, Lcom/xiaomi/push/service/aF;->cSh(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/xiaomi/push/service/aF;->cSd(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "uninstall "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " msg sent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/xiaomi/smack/XMPPException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fail to send Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/smack/XMPPException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    const/16 v1, 0xa

    invoke-virtual {p0, v1, v0}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    goto/16 :goto_0

    :cond_27
    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    const-string/jumbo v2, "1"

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/service/aR;->cSE(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_26

    if-eqz v9, :cond_26

    const-string/jumbo v0, "1"

    invoke-direct {p0, v0, v3}, Lcom/xiaomi/push/service/XMPushService;->cPQ(Ljava/lang/String;I)V

    const-string/jumbo v0, "close the miliao channel as the app is uninstalled."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void

    :cond_28
    invoke-static {p0, v0}, Lcom/xiaomi/push/service/h;->cMW(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_9

    :cond_29
    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/push/service/b;->ddN:Ljava/lang/String;

    const/4 v2, -0x2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, -0x1

    if-ge v1, v2, :cond_2a

    sget-object v1, Lcom/xiaomi/push/service/b;->des:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/push/service/b;->deg:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/xiaomi/push/service/h;->cMZ(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2a
    invoke-static {p0, v0, v1}, Lcom/xiaomi/push/service/h;->cMQ(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_2b
    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/xiaomi/push/service/b;->ddL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/xiaomi/push/service/b;->ddZ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2d

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/c/c;->czl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2e

    :cond_2c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "invalid notification for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2d
    sget-object v0, Lcom/xiaomi/push/service/b;->ddZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/c/c;->czl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v9, v3

    move v3, v1

    goto :goto_a

    :cond_2e
    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2c

    if-nez v9, :cond_2f

    invoke-static {p0, v2, v3}, Lcom/xiaomi/push/service/h;->cMX(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_2f
    invoke-static {p0, v2}, Lcom/xiaomi/push/service/h;->cMW(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_30
    const-string/jumbo v0, "mipush_app_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_31

    :goto_b
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.xmsf"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    if-nez v0, :cond_32

    :goto_c
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/x;->cNL()V

    new-instance v0, Lcom/xiaomi/push/service/Z;

    invoke-direct {v0, p0, v4}, Lcom/xiaomi/push/service/Z;-><init>(Lcom/xiaomi/push/service/XMPushService;I)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aR;->cSL()V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/xiaomi/push/service/aR;->cSI(Landroid/content/Context;I)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aR;->cSK()V

    invoke-static {}, Lcom/xiaomi/push/service/an;->getInstance()Lcom/xiaomi/push/service/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/an;->clear()V

    invoke-static {}, Lcom/xiaomi/push/service/a/d;->stop()V

    goto/16 :goto_0

    :cond_31
    invoke-static {p0}, Lcom/xiaomi/push/service/B;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/B;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/B;->cNX(Ljava/lang/String;)V

    goto :goto_b

    :cond_32
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v5, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    goto :goto_c

    :cond_33
    const-string/jumbo v0, "com.xiaomi.mipush.ENABLE_PUSH_MESSAGE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_21

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mipush.SEND_TINYDATA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_37

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.push.timer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_38

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.push.check_alive"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_39

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQb()V

    goto/16 :goto_0

    :cond_34
    invoke-static {p0}, Lcom/xiaomi/push/service/B;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/B;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/B;->cOa(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_35
    invoke-static {p0}, Lcom/xiaomi/push/service/B;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/B;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/B;->cNY(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/xiaomi/push/service/B;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/B;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/B;->cNT(Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_36
    const-string/jumbo v0, "null payload"

    const v1, 0x42c1d83

    invoke-static {p0, v2, v5, v1, v0}, Lcom/xiaomi/push/service/av;->cRE(Landroid/content/Context;Ljava/lang/String;[BILjava/lang/String;)V

    goto/16 :goto_0

    :cond_37
    const-string/jumbo v0, "mipush_app_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mipush_payload"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    new-instance v2, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;

    invoke-direct {v2}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;-><init>()V

    :try_start_2
    invoke-static {v2, v1}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    invoke-static {p0}, Lcom/xiaomi/g/c;->getInstance(Landroid/content/Context;)Lcom/xiaomi/g/c;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/g/c;->dgQ(Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;Ljava/lang/String;)Z
    :try_end_2
    .catch Lorg/apache/thrift/TException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_38
    const-string/jumbo v0, "Service called on timer"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/xiaomi/push/service/a/d;->cMh(Z)V

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQl()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/xiaomi/push/service/XMPushService;->cPB(Z)V

    goto/16 :goto_0

    :cond_39
    const-string/jumbo v0, "Service called on check alive."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQl()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/xiaomi/push/service/XMPushService;->cPB(Z)V

    goto/16 :goto_0

    :catch_2
    move-exception v1

    goto/16 :goto_8
.end method

.method private cPU()V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgB:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private cPV(Lcom/xiaomi/push/service/n;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/x;->cNG(Lcom/xiaomi/push/service/C;)V

    return-void
.end method

.method static synthetic cPW(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/push/service/XMPushService;->cPR(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic cPX(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/push/service/aP;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgI:Lcom/xiaomi/push/service/aP;

    return-object v0
.end method

.method private cPZ(Landroid/content/Intent;)V
    .locals 8

    const/4 v2, 0x0

    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/xiaomi/push/service/b;->ddS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v0, "ext_packets"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v5

    array-length v0, v5

    new-array v6, v0, [Lcom/xiaomi/smack/packet/c;

    const-string/jumbo v0, "ext_encrypt"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move v1, v2

    :goto_0
    array-length v0, v5

    if-lt v1, v0, :cond_0

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    array-length v1, v6

    new-array v1, v1, [Lcom/xiaomi/a/e;

    :goto_1
    array-length v3, v6

    if-lt v2, v3, :cond_2

    new-instance v0, Lcom/xiaomi/push/service/G;

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/push/service/G;-><init>(Lcom/xiaomi/push/service/XMPushService;[Lcom/xiaomi/a/e;)V

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    return-void

    :cond_0
    new-instance v7, Lcom/xiaomi/smack/packet/c;

    aget-object v0, v5, v1

    check-cast v0, Landroid/os/Bundle;

    invoke-direct {v7, v0}, Lcom/xiaomi/smack/packet/c;-><init>(Landroid/os/Bundle;)V

    aput-object v7, v6, v1

    aget-object v0, v6, v1

    invoke-direct {p0, v0, v3, v4}, Lcom/xiaomi/push/service/XMPushService;->cPv(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/smack/packet/d;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/packet/c;

    aput-object v0, v6, v1

    aget-object v0, v6, v1

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    aget-object v3, v6, v2

    invoke-virtual {v3}, Lcom/xiaomi/smack/packet/c;->cvN()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/xiaomi/smack/packet/c;->cvx()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v4

    iget-object v4, v4, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/xiaomi/a/e;->cBY(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;)Lcom/xiaomi/a/e;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method static synthetic cPq(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/push/service/x;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    return-object v0
.end method

.method private cPr()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgC:Lcom/xiaomi/a/g;

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgF:Lcom/xiaomi/smack/k;

    new-instance v2, Lcom/xiaomi/push/service/k;

    invoke-direct {v2, p0}, Lcom/xiaomi/push/service/k;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/a/g;->cxi(Lcom/xiaomi/smack/k;Lcom/xiaomi/smack/b/a;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgC:Lcom/xiaomi/a/g;

    invoke-virtual {v0}, Lcom/xiaomi/a/g;->cyg()V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgC:Lcom/xiaomi/a/g;

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;
    :try_end_0
    .catch Lcom/xiaomi/smack/XMPPException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "fail to create Slim connection"

    invoke-static {v1, v0}, Lcom/xiaomi/channel/commonutils/e/a;->czv(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgC:Lcom/xiaomi/a/g;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/a/g;->cxH(ILjava/lang/Exception;)V

    goto :goto_0
.end method

.method private cPs()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgO:Lcom/xiaomi/smack/h;

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/h;->cxZ(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cPr()V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-eqz v0, :cond_4

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v0}, Lcom/xiaomi/smack/e;->cxA()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "try to connect while connecting."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v0}, Lcom/xiaomi/smack/e;->cxy()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "try to connect while is connected."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-void

    :cond_4
    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/service/aR;->cSH(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/xiaomi/push/service/XMPushService;->cPF(Z)V

    goto :goto_0
.end method

.method public static cPt(ILjava/lang/String;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "Enter"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const-string/jumbo v0, "Leave"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    return v1

    :cond_2
    if-ne p0, v2, :cond_0

    return v2

    :cond_3
    const/4 v0, 0x2

    if-ne p0, v0, :cond_1

    return v2
.end method

.method private cPv(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/smack/packet/d;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/xiaomi/push/service/aR;->cSJ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, p2}, Lcom/xiaomi/smack/packet/d;->cvF(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvx()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez v1, :cond_4

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "drop a packet as the channel is not opened, chid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    :goto_1
    return-object v5

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "open channel should be called first before sending a packet, pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/xiaomi/smack/packet/d;->cvJ(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "drop a packet as the channel is not connected, chid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v2, v1, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v3, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfT:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-ne v2, v3, :cond_0

    iget-object v0, v1, Lcom/xiaomi/push/service/as;->dhv:Ljava/lang/String;

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    return-object p1

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "invalid session. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic cPw(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/smack/e;)Lcom/xiaomi/smack/e;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    return-object p1
.end method

.method static synthetic cQa()I
    .locals 1

    sget v0, Lcom/xiaomi/push/service/XMPushService;->dgH:I

    return v0
.end method

.method private cQb()V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x0

    :try_start_0
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    const-string/jumbo v0, "network changed, no active network"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    :goto_1
    invoke-static {}, Lcom/xiaomi/e/e;->getContext()Lcom/xiaomi/e/b;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_2
    invoke-static {p0}, Lcom/xiaomi/smack/c/d;->cwD(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgC:Lcom/xiaomi/a/g;

    invoke-virtual {v0}, Lcom/xiaomi/a/g;->cxF()V

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lcom/xiaomi/push/service/S;

    const/4 v2, 0x2

    invoke-direct {v0, p0, v2, v1}, Lcom/xiaomi/push/service/S;-><init>(Lcom/xiaomi/push/service/XMPushService;ILjava/lang/Exception;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    :goto_3
    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cPJ()V

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "], state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "network changed,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v2, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    if-ne v0, v2, :cond_2

    :cond_1
    return-void

    :cond_2
    sget-object v2, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    if-eq v0, v2, :cond_1

    goto/16 :goto_1

    :cond_3
    invoke-static {}, Lcom/xiaomi/e/e;->getContext()Lcom/xiaomi/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/e/b;->dfZ()V

    goto/16 :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_5
    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    :goto_5
    invoke-static {p0}, Lcom/xiaomi/push/b/f;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/b/f;->cLU()V

    goto/16 :goto_3

    :cond_7
    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQl()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v5}, Lcom/xiaomi/push/service/XMPushService;->cPB(Z)V

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPC()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/x;->cND(I)V

    new-instance v0, Lcom/xiaomi/push/service/U;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/U;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto :goto_5
.end method

.method private cQc(Ljava/lang/String;Landroid/content/Intent;)Lcom/xiaomi/push/service/as;
    .locals 3

    sget-object v0, Lcom/xiaomi/push/service/b;->dem:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, v0

    :goto_0
    sget-object v0, Lcom/xiaomi/push/service/b;->ddO:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/service/b;->dem:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->userId:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/service/b;->deh:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->token:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/service/b;->ddT:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->pkgName:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/service/b;->ddX:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->dht:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/service/b;->deo:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/service/b;->ddQ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v1, Lcom/xiaomi/push/service/as;->dhF:Z

    sget-object v0, Lcom/xiaomi/push/service/b;->ddP:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/service/b;->ddS:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->dhv:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/push/service/b;->dei:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->dhw:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgP:Lcom/xiaomi/push/service/r;

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->dhD:Lcom/xiaomi/push/service/r;

    sget-object v0, Lcom/xiaomi/push/service/b;->ddW:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/as;->cRs(Landroid/os/Messenger;)V

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/push/service/as;->dhp:Landroid/content/Context;

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/aR;->cSN(Lcom/xiaomi/push/service/as;)V

    return-object v1

    :cond_0
    new-instance v0, Lcom/xiaomi/push/service/as;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/as;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    move-object v1, v0

    goto :goto_0
.end method

.method private cQe()V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/service/N;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/N;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/push/service/N;->cOi()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/Region;->cWM:Lcom/xiaomi/channel/commonutils/android/Region;

    invoke-virtual {v0}, Lcom/xiaomi/channel/commonutils/android/Region;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgG:Ljava/lang/String;

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQg()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_2
    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQg()Z

    move-result v0

    if-nez v0, :cond_6

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgD:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cQu(Landroid/content/BroadcastReceiver;)V

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/h/h;->getInstance(Landroid/content/Context;)Lcom/xiaomi/channel/commonutils/h/h;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/push/service/s;

    invoke-direct {v1, p0}, Lcom/xiaomi/push/service/s;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    const v2, 0x15180

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/channel/commonutils/h/h;->cBh(Lcom/xiaomi/channel/commonutils/h/i;I)Z

    :try_start_0
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->cAs()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_7

    :goto_4
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cPN()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/N;->cOk(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgG:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/Region;->cWI:Lcom/xiaomi/channel/commonutils/android/Region;

    invoke-virtual {v0}, Lcom/xiaomi/channel/commonutils/android/Region;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/Region;->cWK:Lcom/xiaomi/channel/commonutils/android/Region;

    invoke-virtual {v0}, Lcom/xiaomi/channel/commonutils/android/Region;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/xiaomi/channel/commonutils/android/Region;->cWL:Lcom/xiaomi/channel/commonutils/android/Region;

    invoke-virtual {v0}, Lcom/xiaomi/channel/commonutils/android/Region;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ru.app.chat.global.xiaomi.net"

    invoke-static {v0}, Lcom/xiaomi/smack/h;->cxS(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "app.chat.global.xiaomi.net"

    invoke-static {v0}, Lcom/xiaomi/smack/h;->cxS(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "fr.app.chat.global.xiaomi.net"

    invoke-static {v0}, Lcom/xiaomi/smack/h;->cxS(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/xiaomi/push/service/aK;

    const/16 v1, 0xb

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/push/service/aK;-><init>(Lcom/xiaomi/push/service/XMPushService;I)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto :goto_2

    :cond_6
    new-instance v0, Lcom/xiaomi/push/service/J;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/J;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    invoke-virtual {p0, v1, v0}, Lcom/xiaomi/push/service/XMPushService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_3

    :cond_7
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgP:Lcom/xiaomi/push/service/r;

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/service/r;->cNw(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_4
.end method

.method private cQg()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.xiaomi.xmsf"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/push/service/B;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/B;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/service/B;->cNZ(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method static synthetic cQj(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/smack/e;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    return-object v0
.end method

.method private cQl()Z
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/push/service/XMPushService;->dgA:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7530

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/g/b;->cAM(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static cQn(Landroid/content/Context;)Landroid/app/Notification;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/push/service/XMPushService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string/jumbo v2, "Push Service"

    const-string/jumbo v3, "Push Service"

    invoke-virtual {v1, p0, v2, v3, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    return-object v1

    :cond_0
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->icon:I

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const-string/jumbo v2, "Push Service"

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const-string/jumbo v2, "Push Service"

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private cQo()V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/xiaomi/push/service/aM;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Lcom/xiaomi/push/service/F;

    invoke-direct {v1, p0}, Lcom/xiaomi/push/service/F;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/xiaomi/push/service/XMPushService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void

    :cond_0
    sget v0, Lcom/xiaomi/push/service/XMPushService;->dgH:I

    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/push/service/XMPushService;->startForeground(ILandroid/app/Notification;)V

    return-void
.end method

.method static synthetic cQp(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cPs()V

    return-void
.end method

.method private cQq()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.xmsf"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/push/service/aU;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aU;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ConfigKey;->dpn:Lcom/xiaomi/xmpush/thrift/ConfigKey;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/ConfigKey;->getValue()I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/push/service/aU;->cTb(IZ)Z

    move-result v0

    return v0

    :cond_0
    return v2
.end method

.method static synthetic cQr(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cPJ()V

    return-void
.end method

.method static synthetic cQv(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQe()V

    return-void
.end method


# virtual methods
.method public cPA(Lcom/xiaomi/push/service/n;)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPu(Lcom/xiaomi/push/service/n;J)V

    return-void
.end method

.method public cPC()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v1}, Lcom/xiaomi/smack/e;->cxA()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cPD()Lcom/xiaomi/push/service/r;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgP:Lcom/xiaomi/push/service/r;

    return-object v0
.end method

.method public cPE()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/push/service/aR;->cSQ()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cPO()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQg()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cPG([Lcom/xiaomi/a/e;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "try send msg while connection is null."

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v0, p1}, Lcom/xiaomi/smack/e;->cxp([Lcom/xiaomi/a/e;)V

    return-void
.end method

.method public cPI(I)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/x;->cNH(I)Z

    move-result v0

    return v0
.end method

.method public cPK(ILjava/lang/Exception;)V
    .locals 3

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "disconnect "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-nez v0, :cond_1

    :goto_1
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPp(I)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPp(I)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/xiaomi/push/service/aR;->cSI(Landroid/content/Context;I)V

    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/smack/e;->cxH(ILjava/lang/Exception;)V

    iput-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    goto :goto_1
.end method

.method public cPO()Z
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    const-string/jumbo v1, "miui.os.Build"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string/jumbo v2, "IS_CM_CUSTOMIZATION_TEST"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const-string/jumbo v3, "IS_CU_CUSTOMIZATION_TEST"

    invoke-virtual {v1, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    const-string/jumbo v4, "IS_CT_CUSTOMIZATION_TEST"

    invoke-virtual {v1, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception v1

    return v0
.end method

.method public cPY([BLjava/lang/String;)V
    .locals 7

    const v6, 0x42c1d83

    if-eqz p1, :cond_0

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;-><init>()V

    :try_start_0
    invoke-static {v1, p1}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->action:Lcom/xiaomi/xmpush/thrift/ActionType;

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ActionType;->dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

    if-eq v0, v2, :cond_1

    const-string/jumbo v0, " registration action required."

    const v1, 0x42c1d83

    invoke-static {p0, p2, p1, v1, v0}, Lcom/xiaomi/push/service/av;->cRE(Landroid/content/Context;Ljava/lang/String;[BILjava/lang/String;)V

    const-string/jumbo v0, "register request with invalid payload"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "null payload"

    invoke-static {p0, p2, p1, v6, v0}, Lcom/xiaomi/push/service/av;->cRE(Landroid/content/Context;Ljava/lang/String;[BILjava/lang/String;)V

    const-string/jumbo v0, "register request without payload"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v4, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration;

    invoke-direct {v4}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration;-><init>()V
    :try_end_1
    .catch Lorg/apache/thrift/TException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddM()[B

    move-result-object v0

    invoke-static {v4, v0}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddP()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/xiaomi/push/service/av;->cRF(Ljava/lang/String;[B)V

    new-instance v0, Lcom/xiaomi/push/service/ah;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration;->ddx()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration;->ddo()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/push/service/ah;-><init>(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V
    :try_end_2
    .catch Lorg/apache/thrift/TException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    const-string/jumbo v0, " data action error."

    const v1, 0x42c1d83

    invoke-static {p0, p2, p1, v1, v0}, Lcom/xiaomi/push/service/av;->cRE(Landroid/content/Context;Ljava/lang/String;[BILjava/lang/String;)V
    :try_end_3
    .catch Lorg/apache/thrift/TException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    const-string/jumbo v0, " data container error."

    invoke-static {p0, p2, p1, v6, v0}, Lcom/xiaomi/push/service/av;->cRE(Landroid/content/Context;Ljava/lang/String;[BILjava/lang/String;)V

    goto :goto_0
.end method

.method public cPp(I)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/x;->cND(I)V

    return-void
.end method

.method public cPu(Lcom/xiaomi/push/service/n;J)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/push/service/x;->cNN(Lcom/xiaomi/push/service/C;J)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public cPx(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/push/service/aR;->cSM(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Lcom/xiaomi/push/service/bc;

    move-object v1, p0

    move v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/push/service/bc;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto :goto_0
.end method

.method cPy()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgB:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/service/a;

    invoke-interface {v0}, Lcom/xiaomi/push/service/a;->cMu()V

    goto :goto_0
.end method

.method public cPz()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v1}, Lcom/xiaomi/smack/e;->cxy()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cQd(Lcom/xiaomi/push/service/as;)V
    .locals 6

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/push/service/as;->cRl()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "schedule rebind job in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    new-instance v2, Lcom/xiaomi/push/service/t;

    invoke-direct {v2, p0, p1}, Lcom/xiaomi/push/service/t;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V

    invoke-virtual {p0, v2, v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPu(Lcom/xiaomi/push/service/n;J)V

    goto :goto_0
.end method

.method public cQf(Z)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgN:Lcom/xiaomi/push/service/d;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/d;->cMI(Z)V

    return-void
.end method

.method public cQh()Lcom/xiaomi/push/service/r;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/service/r;

    invoke-direct {v0}, Lcom/xiaomi/push/service/r;-><init>()V

    return-object v0
.end method

.method public cQi(Lcom/xiaomi/a/e;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "try send msg while connection is null."

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    invoke-virtual {v0, p1}, Lcom/xiaomi/smack/e;->cxz(Lcom/xiaomi/a/e;)V

    return-void
.end method

.method cQk(Ljava/lang/String;[BZ)V
    .locals 2

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    const-string/jumbo v1, "5"

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/aR;->cSE(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/service/as;

    iget-object v0, v0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfT:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/xiaomi/push/service/w;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/xiaomi/push/service/w;-><init>(Lcom/xiaomi/push/service/XMPushService;ILjava/lang/String;[B)V

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPV(Lcom/xiaomi/push/service/n;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_0

    invoke-static {p1, p2}, Lcom/xiaomi/push/service/av;->cRD(Ljava/lang/String;[B)V

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_0

    invoke-static {p1, p2}, Lcom/xiaomi/push/service/av;->cRD(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public cQm(Lcom/xiaomi/push/service/n;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    iget v1, p1, Lcom/xiaomi/push/service/n;->type:I

    invoke-virtual {v0, v1, p1}, Lcom/xiaomi/push/service/x;->cNF(ILcom/xiaomi/push/service/C;)V

    return-void
.end method

.method public cQs()Lcom/xiaomi/smack/e;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgK:Lcom/xiaomi/smack/e;

    return-object v0
.end method

.method public cQt(Lcom/xiaomi/push/service/a;)V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgB:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgB:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cQu(Landroid/content/BroadcastReceiver;)V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "com.xiaomi.metoknlp.geofencing.state_change_protected"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "com.xiaomi.metoknlp.permission.NOTIFY_FENCE_STATE"

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/xiaomi/push/service/XMPushService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method public cwY(Lcom/xiaomi/smack/e;)V
    .locals 1

    const-string/jumbo v0, "begin to connect..."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/e/e;->getContext()Lcom/xiaomi/e/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/e/b;->cwY(Lcom/xiaomi/smack/e;)V

    return-void
.end method

.method public cwZ(Lcom/xiaomi/smack/e;)V
    .locals 3

    invoke-static {}, Lcom/xiaomi/e/e;->getContext()Lcom/xiaomi/e/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/e/b;->cwZ(Lcom/xiaomi/smack/e;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPF(Z)V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgN:Lcom/xiaomi/push/service/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/d;->cMG()V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aR;->cSF()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/service/as;

    new-instance v2, Lcom/xiaomi/push/service/t;

    invoke-direct {v2, p0, v0}, Lcom/xiaomi/push/service/t;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V

    invoke-virtual {p0, v2}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto :goto_0
.end method

.method public cxa(Lcom/xiaomi/smack/e;ILjava/lang/Exception;)V
    .locals 1

    invoke-static {}, Lcom/xiaomi/e/e;->getContext()Lcom/xiaomi/e/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/e/b;->cxa(Lcom/xiaomi/smack/e;ILjava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    return-void
.end method

.method public cxb(Lcom/xiaomi/smack/e;Ljava/lang/Exception;)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/xiaomi/e/e;->getContext()Lcom/xiaomi/e/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/e/b;->cxb(Lcom/xiaomi/smack/e;Ljava/lang/Exception;)V

    invoke-direct {p0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPF(Z)V

    invoke-virtual {p0, v1}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgE:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/android/g;->cAp(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/xiaomi/push/service/ac;->cPc(Landroid/content/Context;)Lcom/xiaomi/push/service/ay;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/xiaomi/push/service/P;

    invoke-direct {v1, p0}, Lcom/xiaomi/push/service/P;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgE:Landroid/os/Messenger;

    invoke-static {p0}, Lcom/xiaomi/push/service/bb;->cTl(Lcom/xiaomi/push/service/XMPushService;)V

    new-instance v0, Lcom/xiaomi/push/service/v;

    const-string/jumbo v4, "xiaomi.com"

    const/16 v3, 0x1466

    move-object v1, p0

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/push/service/v;-><init>(Lcom/xiaomi/push/service/XMPushService;Ljava/util/Map;ILjava/lang/String;Lcom/xiaomi/smack/i;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgO:Lcom/xiaomi/smack/h;

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgO:Lcom/xiaomi/smack/h;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/h;->cxX(Z)V

    new-instance v0, Lcom/xiaomi/a/g;

    iget-object v1, p0, Lcom/xiaomi/push/service/XMPushService;->dgO:Lcom/xiaomi/smack/h;

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/a/g;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/smack/h;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgC:Lcom/xiaomi/a/g;

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cQh()Lcom/xiaomi/push/service/r;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgP:Lcom/xiaomi/push/service/r;

    invoke-static {p0}, Lcom/xiaomi/push/service/a/d;->cMf(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgC:Lcom/xiaomi/a/g;

    invoke-virtual {v0, p0}, Lcom/xiaomi/a/g;->cxq(Lcom/xiaomi/smack/a;)V

    new-instance v0, Lcom/xiaomi/push/service/aP;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aP;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgI:Lcom/xiaomi/push/service/aP;

    new-instance v0, Lcom/xiaomi/push/service/d;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/d;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgN:Lcom/xiaomi/push/service/d;

    new-instance v0, Lcom/xiaomi/push/service/aN;

    invoke-direct {v0}, Lcom/xiaomi/push/service/aN;-><init>()V

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aN;->cSv()V

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/e/e;->dgq(Lcom/xiaomi/push/service/XMPushService;)V

    new-instance v0, Lcom/xiaomi/push/service/x;

    const-string/jumbo v1, "Connection Controller Thread"

    invoke-direct {v0, v1}, Lcom/xiaomi/push/service/x;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aR;->cSL()V

    new-instance v1, Lcom/xiaomi/push/service/aW;

    invoke-direct {v1, p0}, Lcom/xiaomi/push/service/aW;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/aR;->cSP(Lcom/xiaomi/push/service/aI;)V

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQq()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {p0}, Lcom/xiaomi/g/c;->getInstance(Landroid/content/Context;)Lcom/xiaomi/g/c;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/push/service/aT;

    invoke-direct {v1, p0}, Lcom/xiaomi/push/service/aT;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    const-string/jumbo v2, "UPLOADER_PUSH_CHANNEL"

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/g/c;->dgS(Lcom/xiaomi/g/b;Ljava/lang/String;)V

    new-instance v0, Lcom/xiaomi/g/d;

    invoke-direct {v0, p0}, Lcom/xiaomi/g/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cQt(Lcom/xiaomi/push/service/a;)V

    new-instance v0, Lcom/xiaomi/push/service/aG;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aG;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "XMPushService created pid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/xiaomi/push/service/XMPushService;->dgH:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void

    :cond_0
    iget v0, v0, Lcom/xiaomi/push/service/ay;->dhR:I

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/k;->cBo(I)V

    goto/16 :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cQo()V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    if-nez v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgD:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/x;->cNL()V

    new-instance v0, Lcom/xiaomi/push/service/bd;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/push/service/bd;-><init>(Lcom/xiaomi/push/service/XMPushService;I)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    new-instance v0, Lcom/xiaomi/push/service/E;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/E;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aR;->cSL()V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p0, v1}, Lcom/xiaomi/push/service/aR;->cSI(Landroid/content/Context;I)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/aR;->cSK()V

    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgC:Lcom/xiaomi/a/g;

    invoke-virtual {v0, p0}, Lcom/xiaomi/a/g;->cxn(Lcom/xiaomi/smack/a;)V

    invoke-static {}, Lcom/xiaomi/push/service/an;->getInstance()Lcom/xiaomi/push/service/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/an;->clear()V

    invoke-static {}, Lcom/xiaomi/push/service/a/d;->stop()V

    invoke-direct {p0}, Lcom/xiaomi/push/service/XMPushService;->cPU()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string/jumbo v0, "Service destroyed"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgM:Lcom/xiaomi/push/service/J;

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/service/b;->ddO:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "onStart() with intent.Action = %s, chid = %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    :goto_0
    if-nez p1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string/jumbo v0, "onStart() with intent NULL"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.push.timer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/push/service/XMPushService;->dgL:Lcom/xiaomi/push/service/x;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/x;->cNI()Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Lcom/xiaomi/push/service/A;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/push/service/A;-><init>(Lcom/xiaomi/push/service/XMPushService;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "com.xiaomi.push.check_alive"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.push.network_status_changed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/push/service/A;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/push/service/A;-><init>(Lcom/xiaomi/push/service/XMPushService;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto :goto_1

    :cond_5
    const-string/jumbo v0, "ERROR, the job controller is blocked."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, p0, v1}, Lcom/xiaomi/push/service/aR;->cSI(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->stopSelf()V

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-virtual {p0, p1, p3}, Lcom/xiaomi/push/service/XMPushService;->onStart(Landroid/content/Intent;I)V

    sget v0, Lcom/xiaomi/push/service/XMPushService;->dgJ:I

    return v0
.end method
