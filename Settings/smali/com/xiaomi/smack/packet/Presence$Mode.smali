.class public final enum Lcom/xiaomi/smack/packet/Presence$Mode;
.super Ljava/lang/Enum;
.source "Presence.java"


# static fields
.field private static final synthetic cUA:[Lcom/xiaomi/smack/packet/Presence$Mode;

.field public static final enum cUB:Lcom/xiaomi/smack/packet/Presence$Mode;

.field public static final enum cUC:Lcom/xiaomi/smack/packet/Presence$Mode;

.field public static final enum cUD:Lcom/xiaomi/smack/packet/Presence$Mode;

.field public static final enum cUy:Lcom/xiaomi/smack/packet/Presence$Mode;

.field public static final enum cUz:Lcom/xiaomi/smack/packet/Presence$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Mode;

    const-string/jumbo v1, "chat"

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/smack/packet/Presence$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Mode;->cUC:Lcom/xiaomi/smack/packet/Presence$Mode;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Mode;

    const-string/jumbo v1, "available"

    invoke-direct {v0, v1, v3}, Lcom/xiaomi/smack/packet/Presence$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Mode;->cUD:Lcom/xiaomi/smack/packet/Presence$Mode;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Mode;

    const-string/jumbo v1, "away"

    invoke-direct {v0, v1, v4}, Lcom/xiaomi/smack/packet/Presence$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Mode;->cUz:Lcom/xiaomi/smack/packet/Presence$Mode;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Mode;

    const-string/jumbo v1, "xa"

    invoke-direct {v0, v1, v5}, Lcom/xiaomi/smack/packet/Presence$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Mode;->cUB:Lcom/xiaomi/smack/packet/Presence$Mode;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Mode;

    const-string/jumbo v1, "dnd"

    invoke-direct {v0, v1, v6}, Lcom/xiaomi/smack/packet/Presence$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Mode;->cUy:Lcom/xiaomi/smack/packet/Presence$Mode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/xiaomi/smack/packet/Presence$Mode;

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Mode;->cUC:Lcom/xiaomi/smack/packet/Presence$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Mode;->cUD:Lcom/xiaomi/smack/packet/Presence$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Mode;->cUz:Lcom/xiaomi/smack/packet/Presence$Mode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Mode;->cUB:Lcom/xiaomi/smack/packet/Presence$Mode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Mode;->cUy:Lcom/xiaomi/smack/packet/Presence$Mode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Mode;->cUA:[Lcom/xiaomi/smack/packet/Presence$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/smack/packet/Presence$Mode;
    .locals 1

    const-class v0, Lcom/xiaomi/smack/packet/Presence$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/packet/Presence$Mode;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/smack/packet/Presence$Mode;
    .locals 1

    sget-object v0, Lcom/xiaomi/smack/packet/Presence$Mode;->cUA:[Lcom/xiaomi/smack/packet/Presence$Mode;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/smack/packet/Presence$Mode;

    return-object v0
.end method
