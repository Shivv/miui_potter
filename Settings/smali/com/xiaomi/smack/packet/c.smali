.class public Lcom/xiaomi/smack/packet/c;
.super Lcom/xiaomi/smack/packet/d;
.source "Message.java"


# instance fields
.field private cTh:Z

.field private cTi:Ljava/lang/String;

.field private cTj:Ljava/lang/String;

.field private cTk:Ljava/lang/String;

.field private cTl:Ljava/lang/String;

.field private cTm:Ljava/lang/String;

.field private cTn:Ljava/lang/String;

.field private cTo:Z

.field private cTp:Ljava/lang/String;

.field private cTq:Ljava/lang/String;

.field private cTr:Ljava/lang/String;

.field private cTs:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/xiaomi/smack/packet/d;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/xiaomi/smack/packet/c;->cTh:Z

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTp:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTl:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTq:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTm:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/xiaomi/smack/packet/c;->cTo:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/smack/packet/d;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/xiaomi/smack/packet/c;->cTh:Z

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTp:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTl:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTq:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTm:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/xiaomi/smack/packet/c;->cTo:Z

    const-string/jumbo v0, "ext_msg_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_lang"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_thread"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_sub"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_body"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    const-string/jumbo v0, "ext_body_encode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTn:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_appid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTj:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_trans"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/smack/packet/c;->cTh:Z

    const-string/jumbo v0, "ext_msg_encrypt"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/smack/packet/c;->cTo:Z

    const-string/jumbo v0, "ext_msg_seq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTp:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_mseq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTl:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_fseq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTq:Ljava/lang/String;

    const-string/jumbo v0, "ext_msg_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTm:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public cvb()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTq:Ljava/lang/String;

    return-object v0
.end method

.method public cvc(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    return-void
.end method

.method public cvd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    return-object v0
.end method

.method public cve()Landroid/os/Bundle;
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/xiaomi/smack/packet/d;->cve()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    if-nez v1, :cond_1

    :goto_1
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    if-nez v1, :cond_3

    :goto_3
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTn:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_4
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    if-nez v1, :cond_5

    :goto_5
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTj:Ljava/lang/String;

    if-nez v1, :cond_6

    :goto_6
    iget-boolean v1, p0, Lcom/xiaomi/smack/packet/c;->cTh:Z

    if-nez v1, :cond_7

    :goto_7
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTp:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    :goto_8
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    :goto_9
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTq:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    :goto_a
    iget-boolean v1, p0, Lcom/xiaomi/smack/packet/c;->cTo:Z

    if-nez v1, :cond_b

    :goto_b
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTm:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    :goto_c
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_type"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_lang"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_sub"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_body"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTn:Ljava/lang/String;

    const-string/jumbo v2, "ext_body_encode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_thread"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTj:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_appid"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    const-string/jumbo v1, "ext_msg_trans"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_7

    :cond_8
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTp:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_seq"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :cond_9
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTl:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_mseq"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    :cond_a
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTq:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_fseq"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :cond_b
    const-string/jumbo v1, "ext_msg_encrypt"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_b

    :cond_c
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTm:Ljava/lang/String;

    const-string/jumbo v2, "ext_msg_status"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c
.end method

.method public cvf()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTl:Ljava/lang/String;

    return-object v0
.end method

.method public cvg()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTj:Ljava/lang/String;

    return-object v0
.end method

.method public cvh(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTl:Ljava/lang/String;

    return-void
.end method

.method public cvi(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    return-void
.end method

.method public cvj(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTm:Ljava/lang/String;

    return-void
.end method

.method public cvk()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTp:Ljava/lang/String;

    return-object v0
.end method

.method public cvl()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "<message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvz()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvB()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvO()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvk()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvf()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvb()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->getStatus()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    :goto_7
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvx()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvN()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    :goto_9
    iget-boolean v1, p0, Lcom/xiaomi/smack/packet/c;->cTh:Z

    if-nez v1, :cond_b

    :goto_a
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTj:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    :goto_b
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    :goto_c
    iget-boolean v1, p0, Lcom/xiaomi/smack/packet/c;->cTo:Z

    if-nez v1, :cond_e

    :goto_d
    const-string/jumbo v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    if-nez v1, :cond_f

    :goto_e
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    if-nez v1, :cond_10

    :goto_f
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    if-nez v1, :cond_12

    :goto_10
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    const-string/jumbo v2, "error"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_13

    :cond_0
    :goto_11
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "</message>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string/jumbo v1, " xmlns=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvz()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_2
    const-string/jumbo v1, " xml:lang=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvd()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_3
    const-string/jumbo v1, " id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvB()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_4
    const-string/jumbo v1, " to=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvO()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_5
    const-string/jumbo v1, " seq=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvk()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    :cond_6
    const-string/jumbo v1, " mseq=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvf()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_7
    const-string/jumbo v1, " fseq=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvb()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_8
    const-string/jumbo v1, " status=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->getStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_9
    const-string/jumbo v1, " from=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvx()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_a
    const-string/jumbo v1, " chid=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvN()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_b
    const-string/jumbo v1, " transient=\"true\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    :cond_c
    const-string/jumbo v1, " appid=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    :cond_d
    const-string/jumbo v1, " type=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    :cond_e
    const-string/jumbo v1, " s=\"1\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    :cond_f
    const-string/jumbo v1, "<subject>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "</subject>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    :cond_10
    const-string/jumbo v1, "<body"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTn:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    :goto_12
    const-string/jumbo v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</body>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_f

    :cond_11
    const-string/jumbo v1, " encode=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_12

    :cond_12
    const-string/jumbo v1, "<thread>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</thread>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_10

    :cond_13
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/c;->cvA()Lcom/xiaomi/smack/packet/j;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/j;->cwk()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11
.end method

.method public cvm(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTj:Ljava/lang/String;

    return-void
.end method

.method public cvn(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    return-void
.end method

.method public cvo(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    return-void
.end method

.method public cvp(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTq:Ljava/lang/String;

    return-void
.end method

.method public cvq(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    iput-object p2, p0, Lcom/xiaomi/smack/packet/c;->cTn:Ljava/lang/String;

    return-void
.end method

.method public cvr(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTp:Ljava/lang/String;

    return-void
.end method

.method public cvs(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/smack/packet/c;->cTo:Z

    return-void
.end method

.method public cvt(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    return-void
.end method

.method public cvu(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/smack/packet/c;->cTh:Z

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_1

    if-nez p1, :cond_2

    :cond_0
    return v0

    :cond_1
    return v1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    check-cast p1, Lcom/xiaomi/smack/packet/c;

    invoke-super {p0, p1}, Lcom/xiaomi/smack/packet/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    if-nez v2, :cond_9

    :cond_3
    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    if-nez v2, :cond_b

    :cond_4
    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    if-nez v2, :cond_c

    iget-object v2, p1, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    if-nez v2, :cond_d

    :cond_5
    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    if-nez v2, :cond_e

    iget-object v2, p1, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    if-nez v2, :cond_f

    :cond_6
    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    if-eq v2, v3, :cond_10

    :goto_0
    return v0

    :cond_7
    return v0

    :cond_8
    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_9
    return v0

    :cond_a
    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_b
    return v0

    :cond_c
    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_d
    return v0

    :cond_e
    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_f
    return v0

    :cond_10
    move v0, v1

    goto :goto_0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTm:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->type:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTs:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTr:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/smack/packet/c;->cTk:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/xiaomi/smack/packet/c;->cTi:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method
