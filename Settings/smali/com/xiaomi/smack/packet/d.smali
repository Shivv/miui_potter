.class public abstract Lcom/xiaomi/smack/packet/d;
.super Ljava/lang/Object;
.source "Packet.java"


# static fields
.field private static cTA:J

.field private static cTB:Ljava/lang/String;

.field protected static final cTt:Ljava/lang/String;

.field public static final cTv:Ljava/text/DateFormat;

.field private static prefix:Ljava/lang/String;


# instance fields
.field private cTC:Ljava/lang/String;

.field private cTD:Ljava/util/List;

.field private cTu:Ljava/lang/String;

.field private cTw:Ljava/lang/String;

.field private cTx:Ljava/lang/String;

.field private cTy:Ljava/lang/String;

.field private final cTz:Ljava/util/Map;

.field private error:Lcom/xiaomi/smack/packet/j;

.field private packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/smack/packet/d;->cTt:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/smack/packet/d;->cTB:Ljava/lang/String;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/d;->cTv:Ljava/text/DateFormat;

    sget-object v0, Lcom/xiaomi/smack/packet/d;->cTv:Ljava/text/DateFormat;

    const-string/jumbo v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/xiaomi/smack/c/e;->cwF(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/smack/packet/d;->prefix:Ljava/lang/String;

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/xiaomi/smack/packet/d;->cTA:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/xiaomi/smack/packet/d;->cTB:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->packageName:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/xiaomi/smack/packet/d;->cTB:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->packageName:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    const-string/jumbo v0, "ext_to"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    const-string/jumbo v0, "ext_from"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    const-string/jumbo v0, "ext_chid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    const-string/jumbo v0, "ext_pkt_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    const-string/jumbo v0, "ext_exts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    const-string/jumbo v0, "ext_ERROR"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0}, Lcom/xiaomi/smack/packet/g;->cwd(Landroid/os/Bundle;)Lcom/xiaomi/smack/packet/g;

    move-result-object v0

    if-nez v0, :cond_2

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    new-instance v1, Lcom/xiaomi/smack/packet/j;

    invoke-direct {v1, v0}, Lcom/xiaomi/smack/packet/j;-><init>(Landroid/os/Bundle;)V

    iput-object v1, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    goto :goto_0
.end method

.method public static cvG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/xiaomi/smack/packet/d;->cTt:Ljava/lang/String;

    return-object v0
.end method

.method public static declared-synchronized cvL()Ljava/lang/String;
    .locals 6

    const-class v1, Lcom/xiaomi/smack/packet/d;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/xiaomi/smack/packet/d;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-wide v2, Lcom/xiaomi/smack/packet/d;->cTA:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    sput-wide v4, Lcom/xiaomi/smack/packet/d;->cTA:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public cvA()Lcom/xiaomi/smack/packet/j;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    return-object v0
.end method

.method public cvB()Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    const-string/jumbo v1, "ID_NOT_AVAILABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    if-eqz v0, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    return-object v0

    :cond_0
    return-object v2

    :cond_1
    invoke-static {}, Lcom/xiaomi/smack/packet/d;->cvL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    goto :goto_0
.end method

.method public cvC(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    return-void
.end method

.method public cvD(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    return-void
.end method

.method public cvE(Lcom/xiaomi/smack/packet/g;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public cvF(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/d;->packageName:Ljava/lang/String;

    return-void
.end method

.method protected declared-synchronized cvH()Ljava/lang/String;
    .locals 8

    const/4 v4, 0x0

    monitor-enter p0

    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/d;->cvQ()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    if-nez v1, :cond_2

    :cond_0
    :goto_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/smack/packet/a;

    invoke-interface {v1}, Lcom/xiaomi/smack/packet/a;->cuZ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "<properties xmlns=\"http://www.jivesoftware.com/xmlns/xmpp/properties\">"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/d;->cvv()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "</properties>"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/xiaomi/smack/packet/d;->cvM(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    const-string/jumbo v3, "<property>"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v3, "<name>"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "</name>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "<value type=\""

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v1, v2, Ljava/lang/Integer;

    if-nez v1, :cond_5

    instance-of v1, v2, Ljava/lang/Long;

    if-nez v1, :cond_6

    instance-of v1, v2, Ljava/lang/Float;

    if-nez v1, :cond_7

    instance-of v1, v2, Ljava/lang/Double;

    if-nez v1, :cond_8

    instance-of v1, v2, Ljava/lang/Boolean;

    if-nez v1, :cond_9

    instance-of v1, v2, Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v1, :cond_a

    :try_start_3
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    new-instance v3, Ljava/io/ObjectOutputStream;

    invoke-direct {v3, v5}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :try_start_5
    invoke-virtual {v3, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    const-string/jumbo v1, "java-object\">"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/smack/c/e;->cwI([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</value>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    if-nez v3, :cond_b

    :goto_3
    if-nez v5, :cond_c

    :cond_4
    :goto_4
    :try_start_6
    const-string/jumbo v1, "</property>"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    const-string/jumbo v1, "integer\">"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</value>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_6
    const-string/jumbo v1, "long\">"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</value>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_7
    const-string/jumbo v1, "float\">"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</value>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_8
    const-string/jumbo v1, "double\">"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</value>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_9
    const-string/jumbo v1, "boolean\">"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</value>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_a
    const-string/jumbo v1, "string\">"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    invoke-static {v1}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "</value>"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    :cond_b
    :try_start_7
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    :catch_0
    move-exception v1

    goto :goto_3

    :cond_c
    :try_start_8
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_4

    :catch_1
    move-exception v1

    goto/16 :goto_4

    :catch_2
    move-exception v1

    move-object v2, v4

    move-object v3, v4

    :goto_5
    :try_start_9
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    if-nez v2, :cond_d

    :goto_6
    if-eqz v3, :cond_4

    :try_start_a
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_4

    :catch_3
    move-exception v1

    goto/16 :goto_4

    :cond_d
    :try_start_b
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_6

    :catchall_1
    move-exception v1

    move-object v3, v4

    move-object v5, v4

    :goto_7
    if-nez v3, :cond_e

    :goto_8
    if-nez v5, :cond_f

    :goto_9
    :try_start_c
    throw v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_e
    :try_start_d
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_8

    :catch_5
    move-exception v2

    goto :goto_8

    :cond_f
    :try_start_e
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_9

    :catch_6
    move-exception v2

    goto :goto_9

    :catchall_2
    move-exception v1

    move-object v3, v4

    goto :goto_7

    :catchall_3
    move-exception v1

    goto :goto_7

    :catchall_4
    move-exception v1

    move-object v5, v3

    move-object v3, v2

    goto :goto_7

    :catch_7
    move-exception v1

    move-object v2, v4

    move-object v3, v5

    goto :goto_5

    :catch_8
    move-exception v1

    move-object v2, v3

    move-object v3, v5

    goto :goto_5
.end method

.method public cvI(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    return-void
.end method

.method public cvJ(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    return-void
.end method

.method public cvK(Lcom/xiaomi/smack/packet/j;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    return-void
.end method

.method public declared-synchronized cvM(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cvN()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    return-object v0
.end method

.method public cvO()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    return-object v0
.end method

.method public cvP(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/smack/packet/g;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v3

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/packet/g;

    if-nez p2, :cond_3

    :cond_2
    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/g;->cvW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    :cond_3
    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/g;->cvX()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method public declared-synchronized cvQ()Ljava/util/Collection;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cve()Landroid/os/Bundle;
    .locals 6

    const/4 v0, 0x0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_3
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_4
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    if-nez v1, :cond_5

    :goto_5
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    if-nez v1, :cond_6

    :goto_6
    return-object v2

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    const-string/jumbo v3, "ext_ns"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    const-string/jumbo v3, "ext_from"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    const-string/jumbo v3, "ext_to"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    const-string/jumbo v3, "ext_pkt_id"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    const-string/jumbo v3, "ext_chid"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/j;->cwl()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v3, "ext_ERROR"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_5

    :cond_6
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [Landroid/os/Bundle;

    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_7

    const-string/jumbo v0, "ext_exts"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    goto :goto_6

    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/packet/g;

    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/g;->cwc()Landroid/os/Bundle;

    move-result-object v5

    if-nez v5, :cond_8

    move v0, v1

    :goto_8
    move v1, v0

    goto :goto_7

    :cond_8
    add-int/lit8 v0, v1, 0x1

    aput-object v5, v3, v1

    goto :goto_8
.end method

.method public abstract cvl()Ljava/lang/String;
.end method

.method public declared-synchronized cvv()Ljava/util/Collection;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cvw()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public cvx()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    return-object v0
.end method

.method public cvy(Ljava/lang/String;)Lcom/xiaomi/smack/packet/g;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/smack/packet/d;->cvP(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/smack/packet/g;

    move-result-object v0

    return-object v0
.end method

.method public cvz()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p0, p1, :cond_1

    if-nez p1, :cond_2

    :cond_0
    return v1

    :cond_1
    return v0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    check-cast p1, Lcom/xiaomi/smack/packet/d;

    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    if-nez v2, :cond_b

    iget-object v2, p1, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    if-nez v2, :cond_c

    :cond_3
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    if-nez v2, :cond_e

    :cond_4
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    if-nez v2, :cond_10

    iget-object v2, p1, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    if-nez v2, :cond_11

    :cond_5
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    if-nez v2, :cond_12

    iget-object v2, p1, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    if-nez v2, :cond_13

    :cond_6
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    if-nez v2, :cond_14

    iget-object v2, p1, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    if-nez v2, :cond_15

    :cond_7
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    if-nez v2, :cond_16

    iget-object v2, p1, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    if-nez v2, :cond_17

    :cond_8
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    if-nez v2, :cond_18

    iget-object v2, p1, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    if-eqz v2, :cond_a

    :cond_9
    move v0, v1

    :cond_a
    :goto_0
    return v0

    :cond_b
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_c
    return v1

    :cond_d
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_e
    return v1

    :cond_f
    return v1

    :cond_10
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_11
    return v1

    :cond_12
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_13
    return v1

    :cond_14
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_15
    return v1

    :cond_16
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_17
    return v1

    :cond_18
    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    iget-object v3, p1, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTD:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->cTz:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTw:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTC:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTy:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTx:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/xiaomi/smack/packet/d;->cTu:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lcom/xiaomi/smack/packet/d;->error:Lcom/xiaomi/smack/packet/j;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5
.end method
