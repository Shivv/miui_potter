.class public Lcom/xiaomi/smack/l;
.super Ljava/lang/Object;
.source "ConnectionHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cyt(Ljava/lang/Throwable;)I
    .locals 6

    const/16 v5, 0x69

    const/4 v4, -0x1

    const/4 v3, 0x0

    instance-of v0, p0, Lcom/xiaomi/smack/XMPPException;

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    instance-of v2, v0, Ljava/net/SocketTimeoutException;

    if-nez v2, :cond_3

    instance-of v2, v0, Ljava/net/SocketException;

    if-nez v2, :cond_4

    instance-of v0, v0, Ljava/net/UnknownHostException;

    if-nez v0, :cond_d

    instance-of v0, p0, Lcom/xiaomi/smack/XMPPException;

    if-nez v0, :cond_e

    return v3

    :cond_0
    move-object v0, p0

    check-cast v0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {v0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, p0

    goto :goto_0

    :cond_1
    move-object v0, p0

    check-cast v0, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {v0}, Lcom/xiaomi/smack/XMPPException;->cxc()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    return v5

    :cond_4
    const-string/jumbo v0, "Network is unreachable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_5

    const-string/jumbo v0, "Connection refused"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_6

    const-string/jumbo v0, "Connection timed out"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_7

    const-string/jumbo v0, "EACCES (Permission denied)"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "Connection reset by peer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_9

    const-string/jumbo v0, "Broken pipe"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_a

    const-string/jumbo v0, "No route to host"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_b

    const-string/jumbo v0, "EINVAL (Invalid argument)"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0xc7

    return v0

    :cond_5
    const/16 v0, 0x66

    return v0

    :cond_6
    const/16 v0, 0x67

    return v0

    :cond_7
    return v5

    :cond_8
    const/16 v0, 0x65

    return v0

    :cond_9
    const/16 v0, 0x6d

    return v0

    :cond_a
    const/16 v0, 0x6e

    return v0

    :cond_b
    const/16 v0, 0x68

    return v0

    :cond_c
    const/16 v0, 0x6a

    return v0

    :cond_d
    const/16 v0, 0x6b

    return v0

    :cond_e
    const/16 v0, 0x18f

    return v0
.end method
