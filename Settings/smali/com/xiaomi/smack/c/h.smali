.class public Lcom/xiaomi/smack/c/h;
.super Ljava/lang/Object;
.source "PacketParserUtils.java"


# static fields
.field private static cVb:Lorg/xmlpull/v1/XmlPullParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/smack/c/h;->cVb:Lorg/xmlpull/v1/XmlPullParser;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cwL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "xml:lang"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string/jumbo v2, "lang"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributePrefix(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "xml"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1
.end method

.method private static cwM(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 4

    const-string/jumbo v0, ""

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    if-ne v2, v1, :cond_0

    return-object v0
.end method

.method public static cwN(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/h;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    return-object v1

    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/xiaomi/smack/packet/h;

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/xiaomi/smack/packet/h;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static cwO(Lorg/xmlpull/v1/XmlPullParser;Lcom/xiaomi/smack/e;)Lcom/xiaomi/smack/packet/e;
    .locals 14

    const/4 v3, 0x0

    const/4 v1, 0x0

    const-string/jumbo v0, ""

    const-string/jumbo v2, "id"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v0, ""

    const-string/jumbo v2, "to"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v0, ""

    const-string/jumbo v2, "from"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v0, ""

    const-string/jumbo v2, "chid"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v0, ""

    const-string/jumbo v2, "type"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/smack/packet/b;->cva(Ljava/lang/String;)Lcom/xiaomi/smack/packet/b;

    move-result-object v8

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    move v0, v1

    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    move v2, v1

    move-object v0, v3

    move-object v1, v3

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_6

    :goto_2
    invoke-virtual {v1, v4}, Lcom/xiaomi/smack/packet/e;->cvI(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Lcom/xiaomi/smack/packet/e;->cvD(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Lcom/xiaomi/smack/packet/e;->cvJ(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lcom/xiaomi/smack/packet/e;->cvC(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Lcom/xiaomi/smack/packet/e;->cvR(Lcom/xiaomi/smack/packet/b;)V

    invoke-virtual {v1, v0}, Lcom/xiaomi/smack/packet/e;->cvK(Lcom/xiaomi/smack/packet/j;)V

    invoke-virtual {v1, v9}, Lcom/xiaomi/smack/packet/e;->cvT(Ljava/util/Map;)V

    return-object v1

    :cond_0
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v10, ""

    invoke-interface {p0, v10, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v2, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_2

    const/4 v11, 0x3

    if-eq v10, v11, :cond_4

    :goto_3
    move v13, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v13

    :goto_4
    move v13, v0

    move-object v0, v1

    move-object v1, v2

    move v2, v13

    goto :goto_1

    :cond_2
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "error"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_3

    new-instance v1, Lcom/xiaomi/smack/packet/e;

    invoke-direct {v1}, Lcom/xiaomi/smack/packet/e;-><init>()V

    invoke-static {v10, v11, p0}, Lcom/xiaomi/smack/c/h;->cwT(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/g;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/xiaomi/smack/packet/e;->cvE(Lcom/xiaomi/smack/packet/g;)V

    goto :goto_3

    :cond_3
    invoke-static {p0}, Lcom/xiaomi/smack/c/h;->cwQ(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/j;

    move-result-object v0

    goto :goto_3

    :cond_4
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "iq"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    move v13, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v13

    goto :goto_4

    :cond_5
    const/4 v2, 0x1

    move v13, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v13

    goto :goto_4

    :cond_6
    sget-object v1, Lcom/xiaomi/smack/packet/b;->cTf:Lcom/xiaomi/smack/packet/b;

    if-ne v1, v8, :cond_8

    :cond_7
    new-instance v0, Lcom/xiaomi/smack/c/f;

    invoke-direct {v0}, Lcom/xiaomi/smack/c/f;-><init>()V

    invoke-virtual {v0, v4}, Lcom/xiaomi/smack/packet/e;->cvI(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Lcom/xiaomi/smack/packet/e;->cvD(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/xiaomi/smack/packet/e;->cvC(Ljava/lang/String;)V

    sget-object v1, Lcom/xiaomi/smack/packet/b;->cTg:Lcom/xiaomi/smack/packet/b;

    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/packet/e;->cvR(Lcom/xiaomi/smack/packet/b;)V

    invoke-virtual {v0, v7}, Lcom/xiaomi/smack/packet/e;->cvJ(Ljava/lang/String;)V

    new-instance v1, Lcom/xiaomi/smack/packet/j;

    sget-object v2, Lcom/xiaomi/smack/packet/f;->cUa:Lcom/xiaomi/smack/packet/f;

    invoke-direct {v1, v2}, Lcom/xiaomi/smack/packet/j;-><init>(Lcom/xiaomi/smack/packet/f;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/packet/e;->cvK(Lcom/xiaomi/smack/packet/j;)V

    invoke-virtual {p1, v0}, Lcom/xiaomi/smack/e;->cxB(Lcom/xiaomi/smack/packet/d;)V

    const-string/jumbo v0, "iq usage error. send packet in packet parser."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-object v3

    :cond_8
    sget-object v1, Lcom/xiaomi/smack/packet/b;->cTc:Lcom/xiaomi/smack/packet/b;

    if-eq v1, v8, :cond_7

    new-instance v1, Lcom/xiaomi/smack/c/i;

    invoke-direct {v1}, Lcom/xiaomi/smack/c/i;-><init>()V

    goto/16 :goto_2
.end method

.method private static cwP([B)V
    .locals 3

    sget-object v0, Lcom/xiaomi/smack/c/h;->cVb:Lorg/xmlpull/v1/XmlPullParser;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/smack/c/h;->cVb:Lorg/xmlpull/v1/XmlPullParser;

    new-instance v1, Ljava/io/InputStreamReader;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/smack/c/h;->cVb:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v0, Lcom/xiaomi/smack/c/h;->cVb:Lorg/xmlpull/v1/XmlPullParser;

    const-string/jumbo v1, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0
.end method

.method public static cwQ(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/j;
    .locals 10

    const/4 v2, 0x0

    const/4 v1, 0x0

    const-string/jumbo v0, "urn:ietf:params:xml:ns:xmpp-stanzas"

    const-string/jumbo v0, "-1"

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v3, v2

    move-object v7, v2

    move-object v8, v0

    move v0, v1

    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v4

    if-lt v0, v4, :cond_0

    move-object v4, v2

    move-object v5, v2

    :goto_1
    if-eqz v1, :cond_4

    if-eqz v7, :cond_b

    move-object v2, v7

    :goto_2
    new-instance v0, Lcom/xiaomi/smack/packet/j;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct/range {v0 .. v6}, Lcom/xiaomi/smack/packet/j;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "code"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object v5, v8

    :goto_3
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v8, "type"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object v4, v7

    :goto_4
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "reason"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    :goto_5
    add-int/lit8 v0, v0, 0x1

    move-object v7, v4

    move-object v8, v5

    goto :goto_0

    :cond_1
    const-string/jumbo v4, ""

    const-string/jumbo v5, "code"

    invoke-interface {p0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_2
    const-string/jumbo v4, ""

    const-string/jumbo v7, "type"

    invoke-interface {p0, v4, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    :cond_3
    const-string/jumbo v3, ""

    const-string/jumbo v7, "reason"

    invoke-interface {p0, v3, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    :cond_4
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_8

    const/4 v2, 0x4

    if-eq v0, v2, :cond_a

    move v0, v1

    :goto_6
    move v1, v0

    goto :goto_1

    :cond_5
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "text"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v9, "urn:ietf:params:xml:ns:xmpp-stanzas"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    invoke-static {v0, v2, p0}, Lcom/xiaomi/smack/c/h;->cwT(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/g;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7
    move v0, v1

    goto :goto_6

    :cond_6
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    move v0, v1

    goto :goto_6

    :cond_7
    move-object v4, v0

    goto :goto_7

    :cond_8
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "error"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    goto :goto_6

    :cond_9
    const/4 v0, 0x1

    goto :goto_6

    :cond_a
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v5

    move v0, v1

    goto :goto_6

    :cond_b
    const-string/jumbo v2, "cancel"

    goto/16 :goto_2
.end method

.method public static cwR(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/i;
    .locals 7

    const/4 v1, 0x0

    sget-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTM:Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "type"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    :goto_0
    new-instance v2, Lcom/xiaomi/smack/packet/i;

    invoke-direct {v2, v0}, Lcom/xiaomi/smack/packet/i;-><init>(Lcom/xiaomi/smack/packet/Presence$Type;)V

    const-string/jumbo v0, ""

    const-string/jumbo v3, "to"

    invoke-interface {p0, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/xiaomi/smack/packet/i;->cvD(Ljava/lang/String;)V

    const-string/jumbo v0, ""

    const-string/jumbo v3, "from"

    invoke-interface {p0, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/xiaomi/smack/packet/i;->cvC(Ljava/lang/String;)V

    const-string/jumbo v0, ""

    const-string/jumbo v3, "chid"

    invoke-interface {p0, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/xiaomi/smack/packet/i;->cvJ(Ljava/lang/String;)V

    const-string/jumbo v0, ""

    const-string/jumbo v3, "id"

    invoke-interface {p0, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {v2, v0}, Lcom/xiaomi/smack/packet/i;->cvI(Ljava/lang/String;)V

    move v0, v1

    :cond_1
    :goto_2
    if-eqz v0, :cond_4

    return-object v2

    :cond_2
    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :try_start_0
    invoke-static {v2}, Lcom/xiaomi/smack/packet/Presence$Type;->valueOf(Ljava/lang/String;)Lcom/xiaomi/smack/packet/Presence$Type;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v3

    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Found invalid presence type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "ID_NOT_AVAILABLE"

    goto :goto_1

    :cond_4
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_5

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "presence"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "status"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string/jumbo v5, "priority"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    const-string/jumbo v5, "show"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    const-string/jumbo v5, "error"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    invoke-static {v3, v4, p0}, Lcom/xiaomi/smack/c/h;->cwT(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/g;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/xiaomi/smack/packet/i;->cvE(Lcom/xiaomi/smack/packet/g;)V

    goto :goto_2

    :cond_6
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/xiaomi/smack/packet/i;->cwg(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    :try_start_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/xiaomi/smack/packet/i;->cwi(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_2

    :catch_1
    move-exception v3

    goto/16 :goto_2

    :catch_2
    move-exception v3

    invoke-virtual {v2, v1}, Lcom/xiaomi/smack/packet/i;->cwi(I)V

    goto/16 :goto_2

    :cond_8
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    :try_start_2
    invoke-static {v3}, Lcom/xiaomi/smack/packet/Presence$Mode;->valueOf(Ljava/lang/String;)Lcom/xiaomi/smack/packet/Presence$Mode;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/xiaomi/smack/packet/i;->cwf(Lcom/xiaomi/smack/packet/Presence$Mode;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_2

    :catch_3
    move-exception v4

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Found invalid presence mode "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    invoke-static {p0}, Lcom/xiaomi/smack/c/h;->cwQ(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/j;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/xiaomi/smack/packet/i;->cvK(Lcom/xiaomi/smack/packet/j;)V

    goto/16 :goto_2
.end method

.method public static cwS(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/d;
    .locals 14

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string/jumbo v0, ""

    const-string/jumbo v4, "s"

    invoke-interface {p0, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "1"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v4, Lcom/xiaomi/smack/packet/c;

    invoke-direct {v4}, Lcom/xiaomi/smack/packet/c;-><init>()V

    const-string/jumbo v0, ""

    const-string/jumbo v5, "id"

    invoke-interface {p0, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    :goto_0
    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvI(Ljava/lang/String;)V

    const-string/jumbo v0, ""

    const-string/jumbo v5, "to"

    invoke-interface {p0, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvD(Ljava/lang/String;)V

    const-string/jumbo v0, ""

    const-string/jumbo v5, "from"

    invoke-interface {p0, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvC(Ljava/lang/String;)V

    const-string/jumbo v0, ""

    const-string/jumbo v5, "chid"

    invoke-interface {p0, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvJ(Ljava/lang/String;)V

    const-string/jumbo v0, ""

    const-string/jumbo v5, "appid"

    invoke-interface {p0, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvm(Ljava/lang/String;)V

    :try_start_0
    const-string/jumbo v0, ""

    const-string/jumbo v5, "transient"

    invoke-interface {p0, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    :try_start_1
    const-string/jumbo v5, ""

    const-string/jumbo v6, "seq"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    if-eqz v6, :cond_e

    :goto_2
    :try_start_2
    const-string/jumbo v5, ""

    const-string/jumbo v6, "mseq"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v6

    if-eqz v6, :cond_f

    :goto_3
    :try_start_3
    const-string/jumbo v5, ""

    const-string/jumbo v6, "fseq"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result v6

    if-eqz v6, :cond_10

    :goto_4
    :try_start_4
    const-string/jumbo v5, ""

    const-string/jumbo v6, "status"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    move-result v6

    if-eqz v6, :cond_11

    :goto_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_12

    :cond_0
    move v0, v2

    :goto_6
    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvu(Z)V

    const-string/jumbo v0, ""

    const-string/jumbo v5, "type"

    invoke-interface {p0, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvo(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/xiaomi/smack/c/h;->cwL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_13

    :cond_1
    invoke-static {}, Lcom/xiaomi/smack/packet/d;->cvG()Ljava/lang/String;

    :goto_7
    if-eqz v2, :cond_14

    invoke-virtual {v4, v1}, Lcom/xiaomi/smack/packet/c;->cvi(Ljava/lang/String;)V

    return-object v4

    :cond_2
    const-string/jumbo v0, ""

    const-string/jumbo v4, "chid"

    invoke-interface {p0, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v0, ""

    const-string/jumbo v4, "id"

    invoke-interface {p0, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v0, ""

    const-string/jumbo v4, "from"

    invoke-interface {p0, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v0, ""

    const-string/jumbo v4, "to"

    invoke-interface {p0, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v0, ""

    const-string/jumbo v4, "type"

    invoke-interface {p0, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v5, v8}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_8
    if-eqz v0, :cond_5

    move-object v4, v1

    :cond_3
    :goto_9
    if-eqz v2, :cond_6

    if-nez v4, :cond_c

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "error while receiving a encrypted message with wrong format"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v5, v7}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    goto :goto_8

    :cond_5
    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "the channel id is wrong while receiving a encrypted message"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v10

    if-eq v10, v12, :cond_7

    if-ne v10, v13, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "message"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v2, v3

    goto :goto_9

    :cond_7
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v10, "s"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    const/4 v10, 0x4

    if-ne v4, v10, :cond_a

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v10, "5"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    :cond_8
    new-instance v10, Lcom/xiaomi/smack/packet/c;

    invoke-direct {v10}, Lcom/xiaomi/smack/packet/c;-><init>()V

    invoke-virtual {v10, v5}, Lcom/xiaomi/smack/packet/c;->cvJ(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Lcom/xiaomi/smack/packet/c;->cvs(Z)V

    invoke-virtual {v10, v7}, Lcom/xiaomi/smack/packet/c;->cvC(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Lcom/xiaomi/smack/packet/c;->cvD(Ljava/lang/String;)V

    invoke-virtual {v10, v6}, Lcom/xiaomi/smack/packet/c;->cvI(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Lcom/xiaomi/smack/packet/c;->cvo(Ljava/lang/String;)V

    new-instance v3, Lcom/xiaomi/smack/packet/g;

    move-object v0, v1

    check-cast v0, [Ljava/lang/String;

    move-object v2, v1

    check-cast v2, [Ljava/lang/String;

    const-string/jumbo v5, "s"

    invoke-direct {v3, v5, v1, v0, v2}, Lcom/xiaomi/smack/packet/g;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/xiaomi/smack/packet/g;->cwb(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Lcom/xiaomi/smack/packet/c;->cvE(Lcom/xiaomi/smack/packet/g;)V

    return-object v10

    :cond_9
    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "error while receiving a encrypted message with wrong format"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "error while receiving a encrypted message with wrong format"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    const-string/jumbo v10, "6"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    iget-object v10, v0, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-static {v10, v6}, Lcom/xiaomi/push/service/c;->cMy(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v10

    invoke-static {v10, v4}, Lcom/xiaomi/push/service/c;->cMB([BLjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/smack/c/h;->cwP([B)V

    sget-object v4, Lcom/xiaomi/smack/c/h;->cVb:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    sget-object v4, Lcom/xiaomi/smack/c/h;->cVb:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v4}, Lcom/xiaomi/smack/c/h;->cwS(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/d;

    move-result-object v4

    goto/16 :goto_9

    :cond_c
    return-object v4

    :cond_d
    const-string/jumbo v0, "ID_NOT_AVAILABLE"

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto/16 :goto_1

    :cond_e
    :try_start_5
    invoke-virtual {v4, v5}, Lcom/xiaomi/smack/packet/c;->cvr(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v5

    goto/16 :goto_2

    :cond_f
    :try_start_6
    invoke-virtual {v4, v5}, Lcom/xiaomi/smack/packet/c;->cvh(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_3

    :catch_2
    move-exception v5

    goto/16 :goto_3

    :cond_10
    :try_start_7
    invoke-virtual {v4, v5}, Lcom/xiaomi/smack/packet/c;->cvp(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_4

    :catch_3
    move-exception v5

    goto/16 :goto_4

    :cond_11
    :try_start_8
    invoke-virtual {v4, v5}, Lcom/xiaomi/smack/packet/c;->cvj(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_5

    :catch_4
    move-exception v5

    goto/16 :goto_5

    :cond_12
    const-string/jumbo v5, "true"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto/16 :goto_6

    :cond_13
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvc(Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_14
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-eq v0, v12, :cond_15

    if-eq v0, v13, :cond_1d

    move v0, v2

    :goto_a
    move v2, v0

    goto/16 :goto_7

    :cond_15
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_16

    :goto_b
    const-string/jumbo v6, "subject"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_17

    const-string/jumbo v6, "body"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_18

    const-string/jumbo v6, "thread"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1a

    const-string/jumbo v6, "error"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1c

    invoke-static {v5, v0, p0}, Lcom/xiaomi/smack/c/h;->cwT(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/g;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvE(Lcom/xiaomi/smack/packet/g;)V

    move-object v0, v1

    :goto_c
    move-object v1, v0

    move v0, v2

    goto :goto_a

    :cond_16
    const-string/jumbo v0, "xm"

    goto :goto_b

    :cond_17
    invoke-static {p0}, Lcom/xiaomi/smack/c/h;->cwL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/smack/c/h;->cwM(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvn(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_c

    :cond_18
    const-string/jumbo v0, ""

    const-string/jumbo v5, "encode"

    invoke-interface {p0, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/xiaomi/smack/c/h;->cwM(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_19

    invoke-virtual {v4, v5}, Lcom/xiaomi/smack/packet/c;->cvt(Ljava/lang/String;)V

    :goto_d
    move-object v0, v1

    goto :goto_c

    :cond_19
    invoke-virtual {v4, v5, v0}, Lcom/xiaomi/smack/packet/c;->cvq(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_d

    :cond_1a
    if-eqz v1, :cond_1b

    move-object v0, v1

    goto :goto_c

    :cond_1b
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    :cond_1c
    invoke-static {p0}, Lcom/xiaomi/smack/c/h;->cwQ(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/j;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/xiaomi/smack/packet/c;->cvK(Lcom/xiaomi/smack/packet/j;)V

    move-object v0, v1

    goto :goto_c

    :cond_1d
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v5, "message"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    move v0, v2

    goto/16 :goto_a

    :cond_1e
    move v0, v3

    goto/16 :goto_a
.end method

.method public static cwT(Ljava/lang/String;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/g;
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/xiaomi/smack/d/a;->getInstance()Lcom/xiaomi/smack/d/a;

    move-result-object v0

    const-string/jumbo v1, "all"

    const-string/jumbo v2, "xm:chat"

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/smack/d/a;->cwW(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-object v3

    :cond_1
    instance-of v1, v0, Lcom/xiaomi/push/service/aN;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/xiaomi/push/service/aN;

    invoke-virtual {v0, p2}, Lcom/xiaomi/push/service/aN;->cSw(Lorg/xmlpull/v1/XmlPullParser;)Lcom/xiaomi/smack/packet/g;

    move-result-object v0

    return-object v0
.end method
