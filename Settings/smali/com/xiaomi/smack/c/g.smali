.class Lcom/xiaomi/smack/c/g;
.super Ljava/lang/Object;
.source "TrafficUtils.java"


# instance fields
.field public cUX:Ljava/lang/String;

.field public cUY:I

.field public cUZ:I

.field public cVa:J

.field public messageTs:J

.field public packageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JIILjava/lang/String;J)V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/c/g;->packageName:Ljava/lang/String;

    iput-wide v2, p0, Lcom/xiaomi/smack/c/g;->messageTs:J

    iput v1, p0, Lcom/xiaomi/smack/c/g;->cUZ:I

    iput v1, p0, Lcom/xiaomi/smack/c/g;->cUY:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/c/g;->cUX:Ljava/lang/String;

    iput-wide v2, p0, Lcom/xiaomi/smack/c/g;->cVa:J

    iput-object p1, p0, Lcom/xiaomi/smack/c/g;->packageName:Ljava/lang/String;

    iput-wide p2, p0, Lcom/xiaomi/smack/c/g;->messageTs:J

    iput p4, p0, Lcom/xiaomi/smack/c/g;->cUZ:I

    iput p5, p0, Lcom/xiaomi/smack/c/g;->cUY:I

    iput-object p6, p0, Lcom/xiaomi/smack/c/g;->cUX:Ljava/lang/String;

    iput-wide p7, p0, Lcom/xiaomi/smack/c/g;->cVa:J

    return-void
.end method


# virtual methods
.method public cwK(Lcom/xiaomi/smack/c/g;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p1, Lcom/xiaomi/smack/c/g;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/smack/c/g;->packageName:Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return v2

    :cond_1
    iget-object v0, p1, Lcom/xiaomi/smack/c/g;->cUX:Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/smack/c/g;->cUX:Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/xiaomi/smack/c/g;->cUZ:I

    iget v3, p0, Lcom/xiaomi/smack/c/g;->cUZ:I

    if-ne v0, v3, :cond_0

    iget v0, p1, Lcom/xiaomi/smack/c/g;->cUY:I

    iget v3, p0, Lcom/xiaomi/smack/c/g;->cUY:I

    if-ne v0, v3, :cond_0

    iget-wide v4, p1, Lcom/xiaomi/smack/c/g;->messageTs:J

    iget-wide v6, p0, Lcom/xiaomi/smack/c/g;->messageTs:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/16 v6, 0x1388

    cmp-long v0, v4, v6

    if-gtz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    return v1

    :cond_2
    move v0, v2

    goto :goto_0
.end method
