.class public abstract Lcom/xiaomi/smack/j;
.super Lcom/xiaomi/smack/e;
.source "SocketConnection.java"


# instance fields
.field protected cVW:Lcom/xiaomi/push/service/XMPushService;

.field private cVX:I

.field protected cVY:Ljava/net/Socket;

.field cVZ:Ljava/lang/String;

.field protected cWa:Ljava/lang/Exception;

.field private cWb:Ljava/lang/String;

.field protected volatile cWc:J

.field protected volatile cWd:J

.field protected volatile cWe:J


# direct methods
.method public constructor <init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/smack/h;)V
    .locals 3

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/smack/e;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/smack/h;)V

    iput-object v2, p0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    iput-object v2, p0, Lcom/xiaomi/smack/j;->cVZ:Ljava/lang/String;

    iput-wide v0, p0, Lcom/xiaomi/smack/j;->cWe:J

    iput-wide v0, p0, Lcom/xiaomi/smack/j;->cWc:J

    iput-wide v0, p0, Lcom/xiaomi/smack/j;->cWd:J

    iput-object p1, p0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    return-void
.end method

.method private cyf(Lcom/xiaomi/smack/h;)V
    .locals 2

    invoke-virtual {p1}, Lcom/xiaomi/smack/h;->cxV()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/smack/h;->cyb()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/smack/j;->cyl(Ljava/lang/String;I)V

    return-void
.end method

.method private cyl(Ljava/lang/String;I)V
    .locals 16

    const/4 v4, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "get bucket for host : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/e/a;->czx(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual/range {p0 .. p1}, Lcom/xiaomi/smack/j;->cyd(Ljava/lang/String;)Lcom/xiaomi/c/e;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/xiaomi/channel/commonutils/e/a;->czw(Ljava/lang/Integer;)V

    if-nez v2, :cond_1

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    :goto_1
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/xiaomi/smack/j;->cWd:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v5}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    move v9, v4

    :cond_0
    :goto_3
    invoke-static {}, Lcom/xiaomi/c/g;->getInstance()Lcom/xiaomi/c/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/c/g;->cVa()V

    if-eqz v9, :cond_7

    return-void

    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/xiaomi/c/e;->cUE(Z)Ljava/util/ArrayList;

    move-result-object v3

    goto :goto_0

    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget v5, v0, Lcom/xiaomi/smack/j;->cVz:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/xiaomi/smack/j;->cVz:I

    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "begin to connect to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/xiaomi/smack/j;->cym()Ljava/net/Socket;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/xiaomi/smack/j;->cVY:Ljava/net/Socket;

    move/from16 v0, p2

    invoke-static {v3, v0}, Lcom/xiaomi/c/f;->cUK(Ljava/lang/String;I)Ljava/net/InetSocketAddress;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/xiaomi/smack/j;->cVY:Ljava/net/Socket;

    const/16 v7, 0x1f40

    invoke-virtual {v6, v5, v7}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    const-string/jumbo v5, "tcp connected"

    invoke-static {v5}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/xiaomi/smack/j;->cVY:Ljava/net/Socket;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/xiaomi/smack/j;->cWb:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/xiaomi/smack/j;->cyq()V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v8, 0x1

    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/xiaomi/smack/j;->cVy:J

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/xiaomi/smack/j;->cVo:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/xiaomi/smack/j;->cWd:J

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "connected to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/xiaomi/smack/j;->cVy:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    move v9, v8

    goto/16 :goto_3

    :cond_4
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/xiaomi/smack/j;->cVy:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/xiaomi/c/e;->cUz(Ljava/lang/String;JJ)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_4

    :catch_0
    move-exception v4

    move v9, v8

    :goto_5
    :try_start_2
    new-instance v4, Ljava/net/SocketTimeoutException;

    const-string/jumbo v5, "Connection timeout"

    invoke-direct {v4, v5}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    if-eqz v9, :cond_8

    :cond_5
    :goto_6
    move v3, v9

    move v4, v3

    goto/16 :goto_2

    :catch_1
    move-exception v5

    move v9, v4

    move-object v4, v5

    :goto_7
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-nez v9, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SMACK: Could not connect to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    const-string/jumbo v4, "SMACK: Could not connect to "

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " port:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-static {v3, v4}, Lcom/xiaomi/e/d;->dgf(Ljava/lang/String;Ljava/lang/Exception;)V

    if-nez v2, :cond_a

    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_6

    :catch_2
    move-exception v5

    move v9, v4

    move-object v4, v5

    :goto_9
    :try_start_3
    new-instance v5, Ljava/lang/Exception;

    const-string/jumbo v6, "abnormal exception"

    invoke-direct {v5, v6, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-nez v9, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SMACK: Could not connect to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    const-string/jumbo v4, "SMACK: Could not connect to "

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " port:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-static {v3, v4}, Lcom/xiaomi/e/d;->dgf(Ljava/lang/String;Ljava/lang/Exception;)V

    if-nez v2, :cond_b

    :goto_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_6

    :catchall_0
    move-exception v5

    move-object v9, v5

    move v10, v4

    :goto_b
    if-eqz v10, :cond_c

    :cond_6
    throw v9

    :cond_7
    new-instance v2, Lcom/xiaomi/smack/XMPPException;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SMACK: Could not connect to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    const-string/jumbo v4, "SMACK: Could not connect to "

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " port:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-static {v3, v4}, Lcom/xiaomi/e/d;->dgf(Ljava/lang/String;Ljava/lang/Exception;)V

    if-nez v2, :cond_9

    :goto_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_6

    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/xiaomi/c/e;->cUw(Ljava/lang/String;JJLjava/lang/Exception;)V

    goto :goto_c

    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/xiaomi/c/e;->cUw(Ljava/lang/String;JJLjava/lang/Exception;)V

    goto/16 :goto_8

    :cond_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/xiaomi/c/e;->cUw(Ljava/lang/String;JJLjava/lang/Exception;)V

    goto/16 :goto_a

    :cond_c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SMACK: Could not connect to:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    const-string/jumbo v4, "SMACK: Could not connect to "

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " port:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    invoke-static {v3, v4}, Lcom/xiaomi/e/d;->dgf(Ljava/lang/String;Ljava/lang/Exception;)V

    if-nez v2, :cond_d

    :goto_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    move v9, v10

    goto/16 :goto_3

    :cond_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/xiaomi/smack/j;->cWa:Ljava/lang/Exception;

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/xiaomi/c/e;->cUw(Ljava/lang/String;JJLjava/lang/Exception;)V

    goto :goto_d

    :catchall_1
    move-exception v4

    move-object v9, v4

    move v10, v8

    goto/16 :goto_b

    :catchall_2
    move-exception v4

    move v10, v9

    move-object v9, v4

    goto/16 :goto_b

    :catch_3
    move-exception v4

    move v9, v8

    goto/16 :goto_9

    :catch_4
    move-exception v4

    move v9, v8

    goto/16 :goto_7

    :catch_5
    move-exception v5

    move v9, v4

    goto/16 :goto_5
.end method


# virtual methods
.method public cxH(ILjava/lang/Exception;)V
    .locals 4

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/smack/j;->cyn(ILjava/lang/Exception;)V

    if-eqz p2, :cond_2

    :cond_0
    iget-wide v0, p0, Lcom/xiaomi/smack/j;->cWd:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    invoke-virtual {p0, p2}, Lcom/xiaomi/smack/j;->cyk(Ljava/lang/Exception;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/16 v0, 0x12

    if-eq p1, v0, :cond_0

    goto :goto_0
.end method

.method public cxk()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/j;->cWb:Ljava/lang/String;

    return-object v0
.end method

.method public cxp([Lcom/xiaomi/a/e;)V
    .locals 2

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "Don\'t support send Blob"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cxu(Z)V
    .locals 5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, p1}, Lcom/xiaomi/smack/j;->cyh(Z)V

    if-eqz p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    new-instance v3, Lcom/xiaomi/smack/b;

    const/16 v4, 0xd

    invoke-direct {v3, p0, v4, v0, v1}, Lcom/xiaomi/smack/b;-><init>(Lcom/xiaomi/smack/j;IJ)V

    const-wide/16 v0, 0x2710

    invoke-virtual {v2, v3, v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPu(Lcom/xiaomi/push/service/n;J)V

    goto :goto_0
.end method

.method cyd(Ljava/lang/String;)Lcom/xiaomi/c/e;
    .locals 5

    const/4 v2, 0x0

    invoke-static {}, Lcom/xiaomi/c/g;->getInstance()Lcom/xiaomi/c/g;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/xiaomi/c/g;->cUO(Ljava/lang/String;Z)Lcom/xiaomi/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/c/e;->cUo()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iput v2, p0, Lcom/xiaomi/smack/j;->cVv:I

    :try_start_0
    iget-object v1, v0, Lcom/xiaomi/c/e;->dlf:Ljava/lang/String;

    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    const/4 v2, 0x0

    aget-byte v2, v1, v2

    and-int/lit16 v2, v2, 0xff

    iput v2, p0, Lcom/xiaomi/smack/j;->cVv:I

    iget v2, p0, Lcom/xiaomi/smack/j;->cVv:I

    const/4 v3, 0x1

    aget-byte v3, v1, v3

    shl-int/lit8 v3, v3, 0x8

    const v4, 0xff00

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    iput v2, p0, Lcom/xiaomi/smack/j;->cVv:I

    iget v2, p0, Lcom/xiaomi/smack/j;->cVv:I

    const/4 v3, 0x2

    aget-byte v3, v1, v3

    shl-int/lit8 v3, v3, 0x10

    const/high16 v4, 0xff0000

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    iput v2, p0, Lcom/xiaomi/smack/j;->cVv:I

    iget v2, p0, Lcom/xiaomi/smack/j;->cVv:I

    const/4 v3, 0x3

    aget-byte v1, v1, v3

    shl-int/lit8 v1, v1, 0x18

    const/high16 v3, -0x1000000

    and-int/2addr v1, v3

    or-int/2addr v1, v2

    iput v1, p0, Lcom/xiaomi/smack/j;->cVv:I
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object v0

    :cond_0
    new-instance v1, Lcom/xiaomi/smack/g;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/smack/g;-><init>(Lcom/xiaomi/smack/j;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/xiaomi/smack/c/b;->cwp(Ljava/lang/Runnable;)V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public cye()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/j;->cVD:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized cyg()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/smack/j;->cxy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "WARNING: current xmpp has connected"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/xiaomi/smack/j;->cxA()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lcom/xiaomi/smack/j;->cxm(IILjava/lang/Exception;)V

    iget-object v0, p0, Lcom/xiaomi/smack/j;->cVm:Lcom/xiaomi/smack/h;

    invoke-direct {p0, v0}, Lcom/xiaomi/smack/j;->cyf(Lcom/xiaomi/smack/h;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/xiaomi/smack/XMPPException;

    invoke-direct {v1, v0}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract cyh(Z)V
.end method

.method protected cyi(Ljava/lang/String;JLjava/lang/Exception;)V
    .locals 8

    invoke-static {}, Lcom/xiaomi/smack/h;->cyc()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/c/g;->getInstance()Lcom/xiaomi/c/g;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/xiaomi/c/g;->cUO(Ljava/lang/String;Z)Lcom/xiaomi/c/e;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-wide/16 v4, 0x0

    move-object v1, p1

    move-wide v2, p2

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/xiaomi/c/e;->cUw(Ljava/lang/String;JJLjava/lang/Exception;)V

    invoke-static {}, Lcom/xiaomi/c/g;->getInstance()Lcom/xiaomi/c/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/c/g;->cVa()V

    goto :goto_0
.end method

.method public cyj()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/smack/j;->cWc:J

    return-void
.end method

.method protected cyk(Ljava/lang/Exception;)V
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/smack/j;->cWd:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x493e0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/xiaomi/smack/j;->cVX:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/smack/j;->cVX:I

    iget v0, p0, Lcom/xiaomi/smack/j;->cVX:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/smack/j;->cxk()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "max short conn time reached, sink down current host:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3, p1}, Lcom/xiaomi/smack/j;->cyi(Ljava/lang/String;JLjava/lang/Exception;)V

    iput v1, p0, Lcom/xiaomi/smack/j;->cVX:I

    goto :goto_1

    :cond_3
    iput v1, p0, Lcom/xiaomi/smack/j;->cVX:I

    goto :goto_1
.end method

.method public cym()Ljava/net/Socket;
    .locals 1

    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    return-object v0
.end method

.method protected declared-synchronized cyn(ILjava/lang/Exception;)V
    .locals 2

    const/4 v1, 0x2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/smack/j;->cxw()I

    move-result v0

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1, p2}, Lcom/xiaomi/smack/j;->cxm(IILjava/lang/Exception;)V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/j;->cVD:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/smack/j;->cVY:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const-wide/16 v0, 0x0

    :try_start_2
    iput-wide v0, p0, Lcom/xiaomi/smack/j;->cWe:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/smack/j;->cWc:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public cyo()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/smack/j;->cWe:J

    return-void
.end method

.method public cyp(ILjava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    new-instance v1, Lcom/xiaomi/smack/d;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/xiaomi/smack/d;-><init>(Lcom/xiaomi/smack/j;IILjava/lang/Exception;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    return-void
.end method

.method protected declared-synchronized cyq()V
    .locals 0

    monitor-enter p0

    monitor-exit p0

    return-void
.end method
