.class public final Lcom/google/protobuf/micro/b;
.super Ljava/lang/Object;
.source "CodedOutputStreamMicro.java"


# instance fields
.field private final dBX:I

.field private final dBY:Ljava/io/OutputStream;

.field private final dBZ:[B

.field private dCa:I


# direct methods
.method private constructor <init>(Ljava/io/OutputStream;[B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/protobuf/micro/b;->dBY:Ljava/io/OutputStream;

    iput-object p2, p0, Lcom/google/protobuf/micro/b;->dBZ:[B

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/micro/b;->dCa:I

    array-length v0, p2

    iput v0, p0, Lcom/google/protobuf/micro/b;->dBX:I

    return-void
.end method

.method private constructor <init>([BII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/micro/b;->dBY:Ljava/io/OutputStream;

    iput-object p1, p0, Lcom/google/protobuf/micro/b;->dBZ:[B

    iput p2, p0, Lcom/google/protobuf/micro/b;->dCa:I

    add-int v0, p2, p3

    iput v0, p0, Lcom/google/protobuf/micro/b;->dBX:I

    return-void
.end method

.method public static dhD(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/protobuf/micro/e;->diR(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/micro/b;->dij(I)I

    move-result v0

    return v0
.end method

.method public static dhE(ILcom/google/protobuf/micro/d;)I
    .locals 2

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dhD(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/micro/b;->dip(Lcom/google/protobuf/micro/d;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dhF(J)I
    .locals 4

    const-wide/16 v2, 0x0

    const-wide/16 v0, -0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const-wide/16 v0, -0x4000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x2

    return v0

    :cond_1
    const-wide/32 v0, -0x200000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const/4 v0, 0x3

    return v0

    :cond_2
    const-wide/32 v0, -0x10000000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const/4 v0, 0x4

    return v0

    :cond_3
    const-wide v0, -0x800000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const/4 v0, 0x5

    return v0

    :cond_4
    const-wide v0, -0x40000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    const/4 v0, 0x6

    return v0

    :cond_5
    const-wide/high16 v0, -0x2000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    const/4 v0, 0x7

    return v0

    :cond_6
    const-wide/high16 v0, -0x100000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    const/16 v0, 0x8

    return v0

    :cond_7
    const-wide/high16 v0, -0x8000000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    const/16 v0, 0x9

    return v0

    :cond_8
    const/16 v0, 0xa

    return v0
.end method

.method private dhL()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/protobuf/micro/b;->dBY:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/micro/b;->dBY:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/protobuf/micro/b;->dBZ:[B

    iget v2, p0, Lcom/google/protobuf/micro/b;->dCa:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    iput v3, p0, Lcom/google/protobuf/micro/b;->dCa:I

    return-void

    :cond_0
    new-instance v0, Lcom/google/protobuf/micro/CodedOutputStreamMicro$OutOfSpaceException;

    invoke-direct {v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro$OutOfSpaceException;-><init>()V

    throw v0
.end method

.method public static dhN(Ljava/lang/String;)I
    .locals 2

    :try_start_0
    const-string/jumbo v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-static {v1}, Lcom/google/protobuf/micro/b;->dij(I)I

    move-result v1

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v1

    return v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "UTF-8 not supported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static dhO(I)I
    .locals 1

    if-gez p0, :cond_0

    const/16 v0, 0xa

    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dij(I)I

    move-result v0

    return v0
.end method

.method public static dhT(ILcom/google/protobuf/micro/c;)I
    .locals 2

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dhD(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/micro/b;->dig(Lcom/google/protobuf/micro/c;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dhU(IJ)I
    .locals 3

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dhD(I)I

    move-result v0

    invoke-static {p1, p2}, Lcom/google/protobuf/micro/b;->dhW(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dhV(II)I
    .locals 2

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dhD(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/micro/b;->dhO(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dhW(J)I
    .locals 2

    invoke-static {p0, p1}, Lcom/google/protobuf/micro/b;->dhF(J)I

    move-result v0

    return v0
.end method

.method public static dhY(IJ)I
    .locals 3

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dhD(I)I

    move-result v0

    invoke-static {p1, p2}, Lcom/google/protobuf/micro/b;->dix(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dia(II)I
    .locals 2

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dhD(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/micro/b;->dis(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dic(Z)I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static die(Ljava/io/OutputStream;I)Lcom/google/protobuf/micro/b;
    .locals 2

    new-instance v0, Lcom/google/protobuf/micro/b;

    new-array v1, p1, [B

    invoke-direct {v0, p0, v1}, Lcom/google/protobuf/micro/b;-><init>(Ljava/io/OutputStream;[B)V

    return-object v0
.end method

.method public static dig(Lcom/google/protobuf/micro/c;)I
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/c;->getSerializedSize()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/micro/b;->dij(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dih(IZ)I
    .locals 2

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dhD(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/micro/b;->dic(Z)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dij(I)I
    .locals 1

    and-int/lit8 v0, p0, -0x80

    if-eqz v0, :cond_0

    and-int/lit16 v0, p0, -0x4000

    if-eqz v0, :cond_1

    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-eqz v0, :cond_2

    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x2

    return v0

    :cond_2
    const/4 v0, 0x3

    return v0

    :cond_3
    const/4 v0, 0x4

    return v0
.end method

.method public static dip(Lcom/google/protobuf/micro/d;)I
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/d;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/micro/b;->dij(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/protobuf/micro/d;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static diq(ILjava/lang/String;)I
    .locals 2

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dhD(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/micro/b;->dhN(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static dis(I)I
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/micro/b;->dij(I)I

    move-result v0

    return v0
.end method

.method public static diu([BII)Lcom/google/protobuf/micro/b;
    .locals 1

    new-instance v0, Lcom/google/protobuf/micro/b;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/protobuf/micro/b;-><init>([BII)V

    return-object v0
.end method

.method public static div(Ljava/io/OutputStream;)Lcom/google/protobuf/micro/b;
    .locals 1

    const/16 v0, 0x1000

    invoke-static {p0, v0}, Lcom/google/protobuf/micro/b;->die(Ljava/io/OutputStream;I)Lcom/google/protobuf/micro/b;

    move-result-object v0

    return-object v0
.end method

.method public static dix(J)I
    .locals 2

    invoke-static {p0, p1}, Lcom/google/protobuf/micro/b;->dhF(J)I

    move-result v0

    return v0
.end method


# virtual methods
.method public dhG(J)V
    .locals 5

    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    long-to-int v0, p1

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dii(I)V

    return-void

    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dii(I)V

    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method public dhH(B)V
    .locals 3

    iget v0, p0, Lcom/google/protobuf/micro/b;->dCa:I

    iget v1, p0, Lcom/google/protobuf/micro/b;->dBX:I

    if-eq v0, v1, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/google/protobuf/micro/b;->dBZ:[B

    iget v1, p0, Lcom/google/protobuf/micro/b;->dCa:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/protobuf/micro/b;->dCa:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/protobuf/micro/b;->dhL()V

    goto :goto_0
.end method

.method public dhI(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Lcom/google/protobuf/micro/b;->dio(I)V

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dhJ([B)V

    return-void
.end method

.method public dhJ([B)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/protobuf/micro/b;->dit([BII)V

    return-void
.end method

.method public dhK()I
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/micro/b;->dBY:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protobuf/micro/b;->dBX:I

    iget v1, p0, Lcom/google/protobuf/micro/b;->dCa:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public dhM(IJ)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/micro/b;->dir(II)V

    invoke-virtual {p0, p2, p3}, Lcom/google/protobuf/micro/b;->dil(J)V

    return-void
.end method

.method public dhP(ILcom/google/protobuf/micro/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/micro/b;->dir(II)V

    invoke-virtual {p0, p2}, Lcom/google/protobuf/micro/b;->din(Lcom/google/protobuf/micro/c;)V

    return-void
.end method

.method public dhQ(J)V
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/micro/b;->dhG(J)V

    return-void
.end method

.method public dhR(II)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/micro/b;->dir(II)V

    invoke-virtual {p0, p2}, Lcom/google/protobuf/micro/b;->dhS(I)V

    return-void
.end method

.method public dhS(I)V
    .locals 2

    if-gez p1, :cond_0

    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/google/protobuf/micro/b;->dhG(J)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/protobuf/micro/b;->dio(I)V

    goto :goto_0
.end method

.method public dhX(Lcom/google/protobuf/micro/d;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/protobuf/micro/d;->diE()[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Lcom/google/protobuf/micro/b;->dio(I)V

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dhJ([B)V

    return-void
.end method

.method public dhZ(IZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/micro/b;->dir(II)V

    invoke-virtual {p0, p2}, Lcom/google/protobuf/micro/b;->dik(Z)V

    return-void
.end method

.method public dib(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/protobuf/micro/b;->dio(I)V

    return-void
.end method

.method public did()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/b;->dhK()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dif(ILcom/google/protobuf/micro/d;)V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/micro/b;->dir(II)V

    invoke-virtual {p0, p2}, Lcom/google/protobuf/micro/b;->dhX(Lcom/google/protobuf/micro/d;)V

    return-void
.end method

.method public dii(I)V
    .locals 1

    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dhH(B)V

    return-void
.end method

.method public dik(Z)V
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dii(I)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dil(J)V
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/micro/b;->dhG(J)V

    return-void
.end method

.method public dim(ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/micro/b;->dir(II)V

    invoke-virtual {p0, p2}, Lcom/google/protobuf/micro/b;->dhI(Ljava/lang/String;)V

    return-void
.end method

.method public din(Lcom/google/protobuf/micro/c;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/protobuf/micro/c;->cIj()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dio(I)V

    invoke-virtual {p1, p0}, Lcom/google/protobuf/micro/c;->cIm(Lcom/google/protobuf/micro/b;)V

    return-void
.end method

.method public dio(I)V
    .locals 1

    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dii(I)V

    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/protobuf/micro/b;->dii(I)V

    return-void
.end method

.method public dir(II)V
    .locals 1

    invoke-static {p1, p2}, Lcom/google/protobuf/micro/e;->diR(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/b;->dio(I)V

    return-void
.end method

.method public dit([BII)V
    .locals 4

    iget v0, p0, Lcom/google/protobuf/micro/b;->dBX:I

    iget v1, p0, Lcom/google/protobuf/micro/b;->dCa:I

    sub-int/2addr v0, v1

    if-ge v0, p3, :cond_0

    iget v0, p0, Lcom/google/protobuf/micro/b;->dBX:I

    iget v1, p0, Lcom/google/protobuf/micro/b;->dCa:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/protobuf/micro/b;->dBZ:[B

    iget v2, p0, Lcom/google/protobuf/micro/b;->dCa:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int v1, p2, v0

    sub-int v0, p3, v0

    iget v2, p0, Lcom/google/protobuf/micro/b;->dBX:I

    iput v2, p0, Lcom/google/protobuf/micro/b;->dCa:I

    invoke-direct {p0}, Lcom/google/protobuf/micro/b;->dhL()V

    iget v2, p0, Lcom/google/protobuf/micro/b;->dBX:I

    if-le v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/protobuf/micro/b;->dBY:Ljava/io/OutputStream;

    invoke-virtual {v2, p1, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/micro/b;->dBZ:[B

    iget v1, p0, Lcom/google/protobuf/micro/b;->dCa:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/google/protobuf/micro/b;->dCa:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/protobuf/micro/b;->dCa:I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/protobuf/micro/b;->dBZ:[B

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v0, p0, Lcom/google/protobuf/micro/b;->dCa:I

    goto :goto_0
.end method

.method public diw(IJ)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/micro/b;->dir(II)V

    invoke-virtual {p0, p2, p3}, Lcom/google/protobuf/micro/b;->dhQ(J)V

    return-void
.end method

.method public diy(II)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/micro/b;->dir(II)V

    invoke-virtual {p0, p2}, Lcom/google/protobuf/micro/b;->dib(I)V

    return-void
.end method

.method public flush()V
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/micro/b;->dBY:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/protobuf/micro/b;->dhL()V

    goto :goto_0
.end method
