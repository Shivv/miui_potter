.class Lse/dirac/acs/api/AsyncHelper$Task;
.super Landroid/os/AsyncTask;
.source "AsyncHelper.java"


# instance fields
.field private final eZp:Lse/dirac/acs/api/AsyncHelper$Function;

.field final synthetic eZq:Lse/dirac/acs/api/AsyncHelper;


# direct methods
.method constructor <init>(Lse/dirac/acs/api/AsyncHelper;Lse/dirac/acs/api/AsyncHelper$Function;)V
    .locals 0

    iput-object p1, p0, Lse/dirac/acs/api/AsyncHelper$Task;->eZq:Lse/dirac/acs/api/AsyncHelper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lse/dirac/acs/api/AsyncHelper$Task;->eZp:Lse/dirac/acs/api/AsyncHelper$Function;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lse/dirac/acs/api/AsyncHelper$Task;->eZq:Lse/dirac/acs/api/AsyncHelper;

    invoke-static {v0}, Lse/dirac/acs/api/AsyncHelper;->etj(Lse/dirac/acs/api/AsyncHelper;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v1, p0, Lse/dirac/acs/api/AsyncHelper$Task;->eZq:Lse/dirac/acs/api/AsyncHelper;

    invoke-static {v1}, Lse/dirac/acs/api/AsyncHelper;->eth(Lse/dirac/acs/api/AsyncHelper;)Lse/dirac/acs/api/AsyncHelper$Function;

    move-result-object v1

    invoke-interface {v1, v0}, Lse/dirac/acs/api/AsyncHelper$Function;->etN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v0, p0, Lse/dirac/acs/api/AsyncHelper$Task;->eZq:Lse/dirac/acs/api/AsyncHelper;

    invoke-static {v0}, Lse/dirac/acs/api/AsyncHelper;->etj(Lse/dirac/acs/api/AsyncHelper;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_1
    iget-object v0, p0, Lse/dirac/acs/api/AsyncHelper$Task;->eZq:Lse/dirac/acs/api/AsyncHelper;

    invoke-static {v0}, Lse/dirac/acs/api/AsyncHelper;->eth(Lse/dirac/acs/api/AsyncHelper;)Lse/dirac/acs/api/AsyncHelper$Function;

    move-result-object v0

    iget-object v1, p0, Lse/dirac/acs/api/AsyncHelper$Task;->eZq:Lse/dirac/acs/api/AsyncHelper;

    invoke-static {v1}, Lse/dirac/acs/api/AsyncHelper;->etg(Lse/dirac/acs/api/AsyncHelper;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lse/dirac/acs/api/AsyncHelper$Function;->etN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AsyncHelper$Task;->eZp:Lse/dirac/acs/api/AsyncHelper$Function;

    invoke-interface {v0, p1}, Lse/dirac/acs/api/AsyncHelper$Function;->etN(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
