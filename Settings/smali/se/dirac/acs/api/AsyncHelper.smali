.class Lse/dirac/acs/api/AsyncHelper;
.super Ljava/lang/Object;
.source "AsyncHelper.java"


# instance fields
.field private eYr:Ljava/lang/Object;

.field private final eYs:Lse/dirac/acs/api/AsyncHelper$Function;

.field private eYt:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lse/dirac/acs/api/AsyncHelper$Function;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lse/dirac/acs/api/AsyncHelper;->eYt:Ljava/lang/Integer;

    iput-object p1, p0, Lse/dirac/acs/api/AsyncHelper;->eYs:Lse/dirac/acs/api/AsyncHelper$Function;

    return-void
.end method

.method static synthetic etg(Lse/dirac/acs/api/AsyncHelper;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AsyncHelper;->eYr:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic eth(Lse/dirac/acs/api/AsyncHelper;)Lse/dirac/acs/api/AsyncHelper$Function;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AsyncHelper;->eYs:Lse/dirac/acs/api/AsyncHelper$Function;

    return-object v0
.end method

.method private eti()Ljava/lang/Object;
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lse/dirac/acs/api/AsyncHelper;->eYt:Ljava/lang/Integer;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lse/dirac/acs/api/AsyncHelper;->eYt:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v1, v3, :cond_0

    :goto_0
    iget-object v3, p0, Lse/dirac/acs/api/AsyncHelper;->eYt:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gtz v3, :cond_1

    :goto_1
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lse/dirac/acs/api/AsyncHelper;->eYt:Ljava/lang/Integer;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    monitor-exit v2

    return-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Service push settings overload, ignoring "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lse/dirac/acs/api/AsyncHelper;->eYt:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " requests, keeping the newest."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "se.dirac.acs-api"

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v0, p0, Lse/dirac/acs/api/AsyncHelper;->eYr:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method static synthetic etj(Lse/dirac/acs/api/AsyncHelper;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lse/dirac/acs/api/AsyncHelper;->eti()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
