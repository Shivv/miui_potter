.class public Lse/dirac/acs/api/OutputSettings;
.super Ljava/lang/Object;
.source "OutputSettings.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CONTENTS_FILE_DESCRIPTOR:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final NUM_EQ_BANDS:I = 0x7


# instance fields
.field private eYE:Z

.field public final eYF:Lse/dirac/acs/api/Filter;

.field private eYG:Z

.field public final eYH:Lse/dirac/acs/api/Device;

.field private final eYI:[F

.field private eYJ:Z

.field private eYK:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lse/dirac/acs/api/OutputSettings$1;

    invoke-direct {v0}, Lse/dirac/acs/api/OutputSettings$1;-><init>()V

    sput-object v0, Lse/dirac/acs/api/OutputSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x7

    new-array v0, v0, [F

    iput-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYI:[F

    :try_start_0
    const-class v0, Lse/dirac/acs/api/Device;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lse/dirac/acs/api/Device;

    iput-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYH:Lse/dirac/acs/api/Device;

    iget-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYH:Lse/dirac/acs/api/Device;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYJ:Z

    const/4 v1, 0x1

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYE:Z

    const/4 v1, 0x2

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYK:Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lse/dirac/acs/api/OutputSettings;->eYG:Z

    iget-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYI:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readFloatArray([F)V

    const-class v0, Lse/dirac/acs/api/Filter;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lse/dirac/acs/api/Filter;

    iput-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYF:Lse/dirac/acs/api/Filter;

    return-void

    :cond_0
    new-instance v0, Landroid/os/BadParcelableException;

    const-string/jumbo v1, "No valid device in parcel"

    invoke-direct {v0, v1}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Landroid/os/BadParcelableException;

    invoke-direct {v1, v0}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lse/dirac/acs/api/OutputSettings$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lse/dirac/acs/api/OutputSettings;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lse/dirac/acs/api/Device;Lse/dirac/acs/api/Filter;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x7

    new-array v0, v0, [F

    iput-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYI:[F

    if-eqz p1, :cond_1

    iput-object p1, p0, Lse/dirac/acs/api/OutputSettings;->eYH:Lse/dirac/acs/api/Device;

    if-nez p2, :cond_2

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "invalid filter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Not a valid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p1, Lse/dirac/acs/api/Device;->eYD:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p2, p0, Lse/dirac/acs/api/OutputSettings;->eYF:Lse/dirac/acs/api/Filter;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast p1, Lse/dirac/acs/api/OutputSettings;

    iget-object v1, p0, Lse/dirac/acs/api/OutputSettings;->eYH:Lse/dirac/acs/api/Device;

    iget-object v2, p1, Lse/dirac/acs/api/OutputSettings;->eYH:Lse/dirac/acs/api/Device;

    invoke-virtual {v1, v2}, Lse/dirac/acs/api/Device;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    :goto_0
    return v0

    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, Lse/dirac/acs/api/OutputSettings;->eYF:Lse/dirac/acs/api/Filter;

    iget-object v2, p1, Lse/dirac/acs/api/OutputSettings;->eYF:Lse/dirac/acs/api/Filter;

    invoke-virtual {v1, v2}, Lse/dirac/acs/api/Filter;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYJ:Z

    iget-boolean v2, p1, Lse/dirac/acs/api/OutputSettings;->eYJ:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYE:Z

    iget-boolean v2, p1, Lse/dirac/acs/api/OutputSettings;->eYE:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYK:Z

    iget-boolean v2, p1, Lse/dirac/acs/api/OutputSettings;->eYK:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYG:Z

    iget-boolean v2, p1, Lse/dirac/acs/api/OutputSettings;->eYG:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lse/dirac/acs/api/OutputSettings;->eYI:[F

    iget-object v2, p1, Lse/dirac/acs/api/OutputSettings;->eYI:[F

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public etH(Z)Lse/dirac/acs/api/OutputSettings;
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    iput-boolean p1, p0, Lse/dirac/acs/api/OutputSettings;->eYK:Z

    return-object p0

    :cond_1
    iget-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYF:Lse/dirac/acs/api/Filter;

    iget-boolean v0, v0, Lse/dirac/acs/api/Filter;->eYU:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "filter do not support SFX"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public etI()Z
    .locals 1

    iget-boolean v0, p0, Lse/dirac/acs/api/OutputSettings;->eYJ:Z

    return v0
.end method

.method public etJ(Z)Lse/dirac/acs/api/OutputSettings;
    .locals 0

    iput-boolean p1, p0, Lse/dirac/acs/api/OutputSettings;->eYE:Z

    return-object p0
.end method

.method public etK(IF)Lse/dirac/acs/api/OutputSettings;
    .locals 3

    if-gez p1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not a valid eq band"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x7

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYI:[F

    aput p2, v0, p1

    return-object p0
.end method

.method public etL(Z)Lse/dirac/acs/api/OutputSettings;
    .locals 0

    iput-boolean p1, p0, Lse/dirac/acs/api/OutputSettings;->eYJ:Z

    return-object p0
.end method

.method public etM(Z)Lse/dirac/acs/api/OutputSettings;
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    iput-boolean p1, p0, Lse/dirac/acs/api/OutputSettings;->eYG:Z

    return-object p0

    :cond_1
    iget-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYF:Lse/dirac/acs/api/Filter;

    iget-boolean v0, v0, Lse/dirac/acs/api/Filter;->eYQ:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "filter do not support EQ"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYH:Lse/dirac/acs/api/Device;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    const/4 v0, 0x4

    new-array v0, v0, [Z

    iget-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYJ:Z

    aput-boolean v1, v0, v3

    iget-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYE:Z

    const/4 v2, 0x1

    aput-boolean v1, v0, v2

    iget-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYK:Z

    const/4 v2, 0x2

    aput-boolean v1, v0, v2

    iget-boolean v1, p0, Lse/dirac/acs/api/OutputSettings;->eYG:Z

    const/4 v2, 0x3

    aput-boolean v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    iget-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYI:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    iget-object v0, p0, Lse/dirac/acs/api/OutputSettings;->eYF:Lse/dirac/acs/api/Filter;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
