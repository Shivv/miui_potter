.class public abstract Lse/dirac/acs/api/AudioControlService$Connection;
.super Ljava/lang/Object;
.source "AudioControlService.java"


# instance fields
.field private final eYp:Landroid/content/ServiceConnection;

.field private eYq:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lse/dirac/acs/api/AudioControlService$Connection$1;

    invoke-direct {v0, p0}, Lse/dirac/acs/api/AudioControlService$Connection$1;-><init>(Lse/dirac/acs/api/AudioControlService$Connection;)V

    iput-object v0, p0, Lse/dirac/acs/api/AudioControlService$Connection;->eYp:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic etd(Lse/dirac/acs/api/AudioControlService$Connection;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$Connection;->eYq:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic ete(Lse/dirac/acs/api/AudioControlService$Connection;Landroid/content/Context;)Landroid/content/Context;
    .locals 0

    iput-object p1, p0, Lse/dirac/acs/api/AudioControlService$Connection;->eYq:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic etf(Lse/dirac/acs/api/AudioControlService$Connection;)Landroid/content/ServiceConnection;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$Connection;->eYp:Landroid/content/ServiceConnection;

    return-object v0
.end method


# virtual methods
.method public abstract csT(Lse/dirac/acs/api/AudioControlService;)V
.end method

.method public abstract onServiceDisconnected()V
.end method
