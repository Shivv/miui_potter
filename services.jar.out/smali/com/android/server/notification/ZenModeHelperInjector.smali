.class public Lcom/android/server/notification/ZenModeHelperInjector;
.super Ljava/lang/Object;
.source "ZenModeHelperInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static applyMiuiRestrictions(Lcom/android/server/notification/ZenModeHelper;Landroid/app/AppOpsManager;Landroid/content/Context;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    sget-boolean v7, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-nez v7, :cond_0

    return-void

    :cond_0
    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v7, "com.android.cellbroadcastreceiver"

    aput-object v7, v3, v8

    const/4 v7, 0x4

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v7, "com.android.systemui"

    aput-object v7, v4, v8

    const-string/jumbo v7, "android"

    aput-object v7, v4, v9

    const-string/jumbo v7, "com.android.cellbroadcastreceiver"

    const/4 v8, 0x2

    aput-object v7, v4, v8

    const-string/jumbo v7, "com.android.server.telecom"

    const/4 v8, 0x3

    aput-object v7, v4, v8

    invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getConfig()Landroid/service/notification/ZenModeConfig;

    move-result-object v2

    const/4 v1, 0x1

    const/4 v0, 0x1

    const/4 v5, 0x0

    packed-switch v6, :pswitch_data_0

    const/4 v0, 0x1

    const/4 v1, 0x1

    :goto_0
    if-eqz v5, :cond_2

    move-object v7, v4

    :goto_1
    const/4 v8, 0x6

    invoke-static {v1, v8, p1, v7}, Lcom/android/server/notification/ZenModeHelperInjector;->applyRestriction(ZILandroid/app/AppOpsManager;[Ljava/lang/String;)V

    if-eqz v5, :cond_3

    :goto_2
    const/4 v7, 0x5

    invoke-static {v0, v7, p1, v4}, Lcom/android/server/notification/ZenModeHelperInjector;->applyRestriction(ZILandroid/app/AppOpsManager;[Ljava/lang/String;)V

    return-void

    :pswitch_0
    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-boolean v7, v2, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-nez v7, :cond_1

    iget-boolean v5, v2, Landroid/service/notification/ZenModeConfig;->allowRepeatCallers:Z

    goto :goto_0

    :cond_1
    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    move-object v7, v3

    goto :goto_1

    :cond_3
    move-object v4, v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static applyRestriction(ZILandroid/app/AppOpsManager;[Ljava/lang/String;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const/16 v3, 0x1c

    invoke-virtual {p2, v3, p1, v0, p3}, Landroid/app/AppOpsManager;->setRestriction(III[Ljava/lang/String;)V

    if-eqz p0, :cond_1

    :goto_1
    const/4 v0, 0x3

    invoke-virtual {p2, v0, p1, v1, p3}, Landroid/app/AppOpsManager;->setRestriction(III[Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method static applyRingerModeToZen(Lcom/android/server/notification/ZenModeHelper;Landroid/content/Context;I)I
    .locals 3

    invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I

    move-result v1

    const/4 v0, -0x1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :pswitch_1
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->isQuietModeEnable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static applyRingerModeToZen(Lcom/android/server/notification/ZenModeHelper;Landroid/content/Context;III)I
    .locals 4

    const/4 v3, 0x4

    sget-boolean v2, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-nez v2, :cond_0

    invoke-static {p0, p1, p3}, Lcom/android/server/notification/ZenModeHelperInjector;->applyRingerModeToZen(Lcom/android/server/notification/ZenModeHelper;Landroid/content/Context;I)I

    move-result v2

    return v2

    :cond_0
    if-eq p3, p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I

    move-result v1

    packed-switch p3, :pswitch_data_0

    :cond_1
    :goto_1
    return p4

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_3

    const/4 p4, 0x4

    goto :goto_1

    :cond_3
    if-ne v3, v1, :cond_1

    const/4 p4, -0x1

    goto :goto_1

    :pswitch_1
    if-eqz v0, :cond_1

    if-ne v1, v3, :cond_1

    const/4 p4, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static getOutRingerMode(IIII)I
    .locals 1

    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-nez v0, :cond_0

    return p3

    :cond_0
    const/4 v0, -0x1

    if-ne p0, v0, :cond_1

    move p0, p1

    :cond_1
    const/4 v0, 0x4

    if-ne p0, v0, :cond_2

    :goto_0
    return p2

    :cond_2
    move p2, p3

    goto :goto_0
.end method

.method static miuiComputeZenMode(Ljava/lang/String;Landroid/service/notification/ZenModeConfig;)I
    .locals 5

    const/4 v3, 0x0

    monitor-enter p1

    if-nez p1, :cond_0

    monitor-exit p1

    return v3

    :cond_0
    :try_start_0
    iget-object v3, p1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-eqz v3, :cond_1

    const-string/jumbo v3, "conditionChanged"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    const-string/jumbo v3, "setNotificationPolicy"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    const-string/jumbo v3, "updateAutomaticZenRule"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    iget-object v3, p1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget v3, v3, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    return v3

    :cond_1
    :try_start_1
    iget-object v3, p1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-nez v3, :cond_4

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p1, Landroid/service/notification/ZenModeConfig;->automaticRules:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/service/notification/ZenModeConfig$ZenRule;

    invoke-virtual {v0}, Landroid/service/notification/ZenModeConfig$ZenRule;->isAutomaticActive()Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    invoke-static {v3}, Lcom/android/server/notification/ZenModeHelperInjector;->zenSeverity(I)I

    move-result v3

    invoke-static {v2}, Lcom/android/server/notification/ZenModeHelperInjector;->zenSeverity(I)I

    move-result v4

    if-gt v3, v4, :cond_3

    iget v3, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    if-nez v2, :cond_2

    :cond_3
    iget v2, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    goto :goto_1

    :cond_4
    iget-object v3, p1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget v2, v3, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_5
    monitor-exit p1

    return v2

    :catchall_0
    move-exception v3

    monitor-exit p1

    throw v3
.end method

.method private static zenSeverity(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    const/4 v0, 0x2

    return v0

    :pswitch_2
    const/4 v0, 0x3

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
