.class public Lcom/android/server/DisplayFeatureControl;
.super Landroid/os/IDisplayFeatureControl$Stub;
.source "DisplayFeatureControl.java"


# static fields
.field private static final ENHANCE_SCREEN_MODE:I = 0x1

.field private static final EYESHIELD_MODE:I = 0x3

.field private static LENGTH:I = 0x0

.field private static LEVEL:I = 0x0

.field private static OFFSET:I = 0x0

.field private static final SMART_ENVIRONMENT_MODE:I = 0x0

.field private static final STANDARD_MODE:I = 0x2

.field public static TAG:Ljava/lang/String;

.field private static color:I

.field private static mDeviceName:Ljava/lang/String;


# instance fields
.field private final COLOR_AD_PROP:Ljava/lang/String;

.field private final COLOR_CABC_PROP:Ljava/lang/String;

.field private final COLOR_CE_PROP:Ljava/lang/String;

.field private final COLOR_GAMMA_PROP:Ljava/lang/String;

.field private UNKNOWN:Ljava/lang/String;

.field private cabc_exist:Ljava/lang/String;

.field private ce_exist:Ljava/lang/String;

.field private huaxing_nt36672_length:Ljava/lang/String;

.field private huaxing_nt36672_offset:Ljava/lang/String;

.field private jdi_nt36672_length:Ljava/lang/String;

.field private jdi_nt36672_offset:Ljava/lang/String;

.field private mIsCABL:Z

.field private mIsSVI:Z

.field private papermode_level:Ljava/lang/String;

.field private sc_td4310_length:Ljava/lang/String;

.field private sc_td4310_offset:Ljava/lang/String;

.field private tm_nt36672_length:Ljava/lang/String;

.field private tm_nt36672_offset:Ljava/lang/String;

.field private tm_nt36672a_length:Ljava/lang/String;

.field private tm_nt36672a_offset:Ljava/lang/String;

.field private tm_td4310_length:Ljava/lang/String;

.field private tm_td4310_offset:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "DisplayFeatureControl"

    sput-object v0, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    const-string/jumbo v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/DisplayFeatureControl;->mDeviceName:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v5, 0x1

    invoke-direct {p0}, Landroid/os/IDisplayFeatureControl$Stub;-><init>()V

    iput-boolean v5, p0, Lcom/android/server/DisplayFeatureControl;->mIsCABL:Z

    iput-boolean v5, p0, Lcom/android/server/DisplayFeatureControl;->mIsSVI:Z

    const-string/jumbo v5, "sys.ad.enable"

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->COLOR_AD_PROP:Ljava/lang/String;

    const-string/jumbo v5, "persist.sys.display_cabc"

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->COLOR_CABC_PROP:Ljava/lang/String;

    const-string/jumbo v5, "persist.sys.display_ce"

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->COLOR_CE_PROP:Ljava/lang/String;

    const-string/jumbo v5, "persist.sys.display_gamma"

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->COLOR_GAMMA_PROP:Ljava/lang/String;

    const-string/jumbo v5, "1"

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    sget-object v5, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "DisplayFeatureControl initial..."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/server/DisplayFeatureControl;->nativeInitDisplayMode()I

    :try_start_0
    const-string/jumbo v1, ""

    const/16 v5, 0x400

    new-array v4, v5, [B

    new-instance v2, Ljava/io/File;

    const-string/jumbo v5, "sys/android_lcd/lcd_name"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    array-length v5, v4

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6, v5}, Ljava/io/FileInputStream;->read([BII)I

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v5, "sys.paper_mode_max_level"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->papermode_level:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->papermode_level:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->LEVEL:I

    if-eqz v1, :cond_2

    const-string/jumbo v5, "nt36672"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string/jumbo v5, "tianma"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string/jumbo v5, "sys.tianma_nt36672_offset"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_nt36672_offset:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_nt36672_offset:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->OFFSET:I

    const-string/jumbo v5, "sys.tianma_nt36672_length"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_nt36672_length:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_nt36672_length:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->LENGTH:I

    :cond_0
    :goto_0
    const-string/jumbo v5, "tulip"

    sget-object v6, Lcom/android/server/DisplayFeatureControl;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string/jumbo v5, "lilium"

    sget-object v6, Lcom/android/server/DisplayFeatureControl;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    const-string/jumbo v5, "nt36672a"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string/jumbo v5, "tianma"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string/jumbo v5, "sys.tianma_nt36672a_offset"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_nt36672a_offset:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_nt36672a_offset:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->OFFSET:I

    const-string/jumbo v5, "sys.tianma_nt36672a_length"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_nt36672a_length:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_nt36672a_length:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->LENGTH:I

    :cond_2
    :goto_1
    return-void

    :cond_3
    const-string/jumbo v5, "jdi"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string/jumbo v5, "sys.jdi_nt36672_offset"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->jdi_nt36672_offset:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->jdi_nt36672_offset:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->OFFSET:I

    const-string/jumbo v5, "sys.jdi_nt36672_length"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->jdi_nt36672_length:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->jdi_nt36672_length:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->LENGTH:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_4
    :try_start_1
    const-string/jumbo v5, "huaxing"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string/jumbo v5, "sys.huaxing_nt36672_offset"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->huaxing_nt36672_offset:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->huaxing_nt36672_offset:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->OFFSET:I

    const-string/jumbo v5, "sys.huaxing_nt36672_length"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->huaxing_nt36672_length:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->huaxing_nt36672_length:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->LENGTH:I

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v5, "td4310"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string/jumbo v5, "tianma"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string/jumbo v5, "sys.tianma_td4310_offset"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_td4310_offset:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_td4310_offset:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->OFFSET:I

    const-string/jumbo v5, "sys.tianma_td4310_length"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_td4310_length:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->tm_td4310_length:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->LENGTH:I

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v5, "shenchao"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string/jumbo v5, "sys.shenchao_td4310_offset"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->sc_td4310_offset:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->sc_td4310_offset:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->OFFSET:I

    const-string/jumbo v5, "sys.shenchao_td4310_length"

    iget-object v6, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/DisplayFeatureControl;->sc_td4310_length:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/DisplayFeatureControl;->sc_td4310_length:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/android/server/DisplayFeatureControl;->LENGTH:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private native nativeInitDisplayMode()I
.end method

.method private native nativeSetActiveMode(I)I
.end method

.method private native nativeSetColorBalance(I)I
.end method

.method private setCABL(Z)V
    .locals 7

    sget-object v4, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setCABL : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/android/server/DisplayFeatureControl;->mIsCABL:Z

    if-ne v4, p1, :cond_0

    sget-object v4, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Current CABL mode is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string/jumbo v2, "cabl:on\n"

    :goto_0
    :try_start_0
    new-instance v3, Landroid/net/LocalSocket;

    invoke-direct {v3}, Landroid/net/LocalSocket;-><init>()V

    new-instance v4, Landroid/net/LocalSocketAddress;

    const-string/jumbo v5, "pps"

    sget-object v6, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v4, v5, v6}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    invoke-virtual {v3, v4}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v3}, Landroid/net/LocalSocket;->close()V

    iput-boolean p1, p0, Lcom/android/server/DisplayFeatureControl;->mIsCABL:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_1
    const-string/jumbo v2, "cabl:off\n"

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    sget-object v4, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setCABL IOException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setSVI(Z)V
    .locals 7

    sget-object v4, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setSVI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/android/server/DisplayFeatureControl;->mIsSVI:Z

    if-ne v4, p1, :cond_0

    sget-object v4, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Current SVI mode is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string/jumbo v2, "svi:on\n"

    :goto_0
    :try_start_0
    new-instance v3, Landroid/net/LocalSocket;

    invoke-direct {v3}, Landroid/net/LocalSocket;-><init>()V

    new-instance v4, Landroid/net/LocalSocketAddress;

    const-string/jumbo v5, "pps"

    sget-object v6, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v4, v5, v6}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    invoke-virtual {v3, v4}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v3}, Landroid/net/LocalSocket;->close()V

    iput-boolean p1, p0, Lcom/android/server/DisplayFeatureControl;->mIsSVI:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_1
    const-string/jumbo v2, "svi:off\n"

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    sget-object v4, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setSVI IOException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public setFeature(IIII)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v1, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "GUOQ setFeature value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    packed-switch p2, :pswitch_data_0

    sget-object v1, Lcom/android/server/DisplayFeatureControl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "unknown mode"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, -0x32

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0, p3}, Lcom/android/server/DisplayFeatureControl;->nativeSetActiveMode(I)I

    move-result v0

    const-string/jumbo v1, "sys.ad.enable"

    const-string/jumbo v2, "on"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/android/server/DisplayFeatureControl;->nativeSetActiveMode(I)I

    move-result v0

    const-string/jumbo v1, "sys.ad.enable"

    const-string/jumbo v2, "off"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/server/DisplayFeatureControl;->nativeSetActiveMode(I)I

    move-result v0

    const-string/jumbo v1, "sys.ad.enable"

    const-string/jumbo v2, "off"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    sget v1, Lcom/android/server/DisplayFeatureControl;->OFFSET:I

    sget v2, Lcom/android/server/DisplayFeatureControl;->LEVEL:I

    sub-int/2addr v2, p3

    sget v3, Lcom/android/server/DisplayFeatureControl;->LENGTH:I

    mul-int/2addr v2, v3

    sget v3, Lcom/android/server/DisplayFeatureControl;->LEVEL:I

    add-int/lit8 v3, v3, -0x1

    div-int/2addr v2, v3

    add-int/2addr v1, v2

    sput v1, Lcom/android/server/DisplayFeatureControl;->color:I

    sget v1, Lcom/android/server/DisplayFeatureControl;->color:I

    invoke-direct {p0, v1}, Lcom/android/server/DisplayFeatureControl;->nativeSetColorBalance(I)I

    move-result v0

    const-string/jumbo v1, "sys.ad.enable"

    const-string/jumbo v2, "on"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "persist.sys.display_ce"

    iget-object v2, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/DisplayFeatureControl;->ce_exist:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/DisplayFeatureControl;->ce_exist:Ljava/lang/String;

    const-string/jumbo v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "persist.sys.display_ce"

    const-string/jumbo v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string/jumbo v1, "persist.sys.display_cabc"

    iget-object v2, p0, Lcom/android/server/DisplayFeatureControl;->UNKNOWN:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/DisplayFeatureControl;->cabc_exist:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/DisplayFeatureControl;->cabc_exist:Ljava/lang/String;

    const-string/jumbo v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "persist.sys.display_cabc"

    const-string/jumbo v2, "1"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string/jumbo v1, "whyred"

    sget-object v2, Lcom/android/server/DisplayFeatureControl;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "tulip"

    sget-object v2, Lcom/android/server/DisplayFeatureControl;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "lilium"

    sget-object v2, Lcom/android/server/DisplayFeatureControl;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const-string/jumbo v1, "persist.sys.display_gamma"

    const-string/jumbo v2, "2"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
