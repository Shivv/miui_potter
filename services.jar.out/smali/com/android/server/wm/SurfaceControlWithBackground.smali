.class Lcom/android/server/wm/SurfaceControlWithBackground;
.super Landroid/view/SurfaceControl;
.source "SurfaceControlWithBackground.java"


# instance fields
.field private mBackgroundControl:Landroid/view/SurfaceControl;

.field private mHiddenForCrop:Z

.field private mLastDsDx:F

.field private mLastDsDy:F

.field private mLastHeight:F

.field private mLastWidth:F

.field private mLastX:F

.field private mLastY:F

.field private mTmpContainerRect:Landroid/graphics/Rect;

.field private mVisible:Z

.field private mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;


# direct methods
.method public constructor <init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIIIIILcom/android/server/wm/WindowSurfaceController;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    invoke-direct/range {p0 .. p8}, Landroid/view/SurfaceControl;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIIIII)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDx:F

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDy:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mHiddenForCrop:Z

    const/4 v1, 0x1

    if-eq p7, v1, :cond_1

    const/4 v1, 0x3

    if-eq p7, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p9

    iget-object v1, v0, Lcom/android/server/wm/WindowSurfaceController;->mAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-object v1, v1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isLetterboxedAppWindow()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    int-to-float v1, p3

    iput v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastWidth:F

    int-to-float v1, p4

    iput v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastHeight:F

    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v2, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowSurfaceController;->getContainerRect(Landroid/graphics/Rect;)V

    new-instance v1, Landroid/view/SurfaceControl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Background for - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v2, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v5

    const/high16 v2, 0x20000

    or-int v7, p6, v2

    const/4 v6, -0x1

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Landroid/view/SurfaceControl;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    iput-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    return-void
.end method

.method public constructor <init>(Lcom/android/server/wm/SurfaceControlWithBackground;)V
    .locals 2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1}, Landroid/view/SurfaceControl;-><init>(Landroid/view/SurfaceControl;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    iput v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDx:F

    iput v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDy:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mHiddenForCrop:Z

    iget-object v0, p1, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    iput-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    iget-boolean v0, p1, Lcom/android/server/wm/SurfaceControlWithBackground;->mVisible:Z

    iput-boolean v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mVisible:Z

    iget-object v0, p1, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iput-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    return-void
.end method

.method private calculateBgCrop(Landroid/graphics/Rect;)V
    .locals 14

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    const/high16 v10, 0x3f800000    # 1.0f

    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v8, v8, Lcom/android/server/wm/WindowSurfaceController;->mAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-object v8, v8, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-object v3, v8, Lcom/android/server/wm/WindowState;->mContentInsets:Landroid/graphics/Rect;

    iget v8, v3, Landroid/graphics/Rect;->top:I

    if-nez v8, :cond_0

    const/4 v4, 0x0

    :goto_0
    cmpl-float v8, v4, v10

    if-lez v8, :cond_1

    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->setEmpty()V

    return-void

    :cond_0
    iget v8, p1, Landroid/graphics/Rect;->top:I

    int-to-float v8, v8

    iget v9, v3, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    div-float v4, v8, v9

    goto :goto_0

    :cond_1
    const v8, 0x3ccccccd    # 0.025f

    cmpg-float v8, v4, v8

    if-gez v8, :cond_2

    const/4 v4, 0x0

    :cond_2
    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v9, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v8, v9}, Lcom/android/server/wm/WindowSurfaceController;->getContainerRect(Landroid/graphics/Rect;)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v8, v8, Lcom/android/server/wm/WindowSurfaceController;->mAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-object v8, v8, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-object v7, v8, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    iget v8, v7, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDx:F

    mul-float/2addr v8, v9

    float-to-int v5, v8

    iget v8, v7, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDy:F

    mul-float/2addr v8, v9

    float-to-int v6, v8

    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v8, v8, Lcom/android/server/wm/WindowSurfaceController;->mAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-object v8, v8, Lcom/android/server/wm/WindowStateAnimator;->mService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v8}, Lcom/android/server/wm/WindowManagerService;->getNavBarPosition()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    add-int v9, v5, v1

    add-int v10, v6, v0

    invoke-virtual {v8, v5, v6, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    return-void

    :pswitch_1
    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastWidth:F

    sub-float/2addr v8, v9

    sub-float v9, v10, v4

    mul-float/2addr v8, v9

    float-to-double v8, v8

    add-double/2addr v8, v12

    double-to-int v1, v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v8, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v1

    add-int/2addr v5, v8

    iget v8, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v8

    goto :goto_1

    :pswitch_2
    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastWidth:F

    sub-float/2addr v8, v9

    sub-float v9, v10, v4

    mul-float/2addr v8, v9

    float-to-double v8, v8

    add-double/2addr v8, v12

    double-to-int v1, v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v8, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v8

    iget v8, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v8

    goto :goto_1

    :pswitch_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v8, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastHeight:F

    sub-float/2addr v8, v9

    sub-float v9, v10, v4

    mul-float/2addr v8, v9

    float-to-double v8, v8

    add-double/2addr v8, v12

    double-to-int v0, v8

    iget v8, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v8

    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v8

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private updateBackgroundVisibility()V
    .locals 2

    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v1, v1, Lcom/android/server/wm/WindowSurfaceController;->mAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-object v1, v1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-object v0, v1, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    iget-boolean v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mHiddenForCrop:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mVisible:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/AppWindowToken;->fillsParent()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v1}, Landroid/view/SurfaceControl;->show()V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v1}, Landroid/view/SurfaceControl;->hide()V

    goto :goto_0
.end method

.method private updateBgPosition()V
    .locals 6

    iget-object v3, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v4, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Lcom/android/server/wm/WindowSurfaceController;->getContainerRect(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v3, v3, Lcom/android/server/wm/WindowSurfaceController;->mAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-object v3, v3, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-object v2, v3, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDx:F

    mul-float v0, v3, v4

    iget-object v3, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDy:F

    mul-float v1, v3, v4

    iget-object v3, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    iget v4, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastX:F

    add-float/2addr v4, v0

    iget v5, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastY:F

    add-float/2addr v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/view/SurfaceControl;->setPosition(FF)V

    return-void
.end method


# virtual methods
.method public deferTransactionUntil(Landroid/os/IBinder;J)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Landroid/view/SurfaceControl;->deferTransactionUntil(Landroid/os/IBinder;J)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/SurfaceControl;->deferTransactionUntil(Landroid/os/IBinder;J)V

    return-void
.end method

.method public deferTransactionUntil(Landroid/view/Surface;J)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Landroid/view/SurfaceControl;->deferTransactionUntil(Landroid/view/Surface;J)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/SurfaceControl;->deferTransactionUntil(Landroid/view/Surface;J)V

    return-void
.end method

.method public destroy()V
    .locals 1

    invoke-super {p0}, Landroid/view/SurfaceControl;->destroy()V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->destroy()V

    return-void
.end method

.method public hide()V
    .locals 1

    invoke-super {p0}, Landroid/view/SurfaceControl;->hide()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mVisible:Z

    invoke-direct {p0}, Lcom/android/server/wm/SurfaceControlWithBackground;->updateBackgroundVisibility()V

    return-void
.end method

.method public release()V
    .locals 1

    invoke-super {p0}, Landroid/view/SurfaceControl;->release()V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->release()V

    return-void
.end method

.method public setAlpha(F)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/SurfaceControl;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, p1}, Landroid/view/SurfaceControl;->setAlpha(F)V

    return-void
.end method

.method public setFinalCrop(Landroid/graphics/Rect;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/SurfaceControl;->setFinalCrop(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowSurfaceController;->getContainerRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->setFinalCrop(Landroid/graphics/Rect;)V

    return-void
.end method

.method public setLayer(I)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/SurfaceControl;->setLayer(I)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->setLayer(I)V

    return-void
.end method

.method public setLayerStack(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/SurfaceControl;->setLayerStack(I)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, p1}, Landroid/view/SurfaceControl;->setLayerStack(I)V

    return-void
.end method

.method public setMatrix(FFFF)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/SurfaceControl;->setMatrix(FFFF)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/SurfaceControl;->setMatrix(FFFF)V

    iput p1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDx:F

    iput p4, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastDsDy:F

    invoke-direct {p0}, Lcom/android/server/wm/SurfaceControlWithBackground;->updateBgPosition()V

    return-void
.end method

.method public setOpaque(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/SurfaceControl;->setOpaque(Z)V

    invoke-direct {p0}, Lcom/android/server/wm/SurfaceControlWithBackground;->updateBackgroundVisibility()V

    return-void
.end method

.method public setPosition(FF)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/view/SurfaceControl;->setPosition(FF)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastX:F

    iput p2, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastY:F

    invoke-direct {p0}, Lcom/android/server/wm/SurfaceControlWithBackground;->updateBgPosition()V

    return-void
.end method

.method public setSecure(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/SurfaceControl;->setSecure(Z)V

    return-void
.end method

.method public setSize(II)V
    .locals 3

    invoke-super {p0, p1, p2}, Landroid/view/SurfaceControl;->setSize(II)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    int-to-float v0, p1

    iput v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastWidth:F

    int-to-float v0, p2

    iput v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mLastHeight:F

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mWindowSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowSurfaceController;->getContainerRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/SurfaceControl;->setSize(II)V

    return-void
.end method

.method public setTransparentRegionHint(Landroid/graphics/Region;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/SurfaceControl;->setTransparentRegionHint(Landroid/graphics/Region;)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, p1}, Landroid/view/SurfaceControl;->setTransparentRegionHint(Landroid/graphics/Region;)V

    return-void
.end method

.method public setWindowCrop(Landroid/graphics/Rect;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/SurfaceControl;->setWindowCrop(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/SurfaceControlWithBackground;->calculateBgCrop(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mBackgroundControl:Landroid/view/SurfaceControl;

    iget-object v1, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->setWindowCrop(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mTmpContainerRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mHiddenForCrop:Z

    invoke-direct {p0}, Lcom/android/server/wm/SurfaceControlWithBackground;->updateBackgroundVisibility()V

    return-void
.end method

.method public show()V
    .locals 1

    invoke-super {p0}, Landroid/view/SurfaceControl;->show()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/SurfaceControlWithBackground;->mVisible:Z

    invoke-direct {p0}, Lcom/android/server/wm/SurfaceControlWithBackground;->updateBackgroundVisibility()V

    return-void
.end method
