.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;
.super Landroid/os/Handler;
.source "BaseMiuiPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field static final MSG_KEY_FUNCTION:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method private constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v10, 0x0

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-get9(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v8

    if-eqz v8, :cond_0

    return-void

    :cond_0
    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v8, v8, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v8}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v8

    if-nez v8, :cond_1

    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string/jumbo v9, "shortcut"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const-string/jumbo v1, "virtual_key_longpress"

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_2

    return-void

    :cond_2
    const-string/jumbo v8, "launch_camera"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap0(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Landroid/content/Intent;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap3(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)Z

    move-result v7

    :cond_3
    :goto_0
    if-eqz v7, :cond_4

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-get5(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lmiui/util/HapticFeedbackUtil;

    move-result-object v8

    invoke-virtual {v8, v1, v12}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;Z)Z

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap9(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    :cond_4
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :cond_5
    const-string/jumbo v8, "screen_shot"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v8, v8, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getScreenshotChordLongPress()Ljava/lang/Runnable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v8, v8, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getScreenshotChordLongPress()Ljava/lang/Runnable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v8, v8, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string/jumbo v9, "screenshot"

    const-string/jumbo v10, "key_shortcut"

    invoke-static {v8, v9, v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendRecordCountEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x1

    goto :goto_0

    :cond_6
    const-string/jumbo v8, "launch_voice_assistant"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap6(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)Z

    move-result v7

    const-string/jumbo v1, "screen_button_voice_assist"

    goto :goto_0

    :cond_7
    const-string/jumbo v8, "launch_google_search"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8, v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap4(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)Z

    move-result v7

    const-string/jumbo v1, "screen_button_voice_assist"

    goto :goto_0

    :cond_8
    const-string/jumbo v8, "go_to_sleep"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v8, v8, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Landroid/os/PowerManager;->goToSleep(J)V

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v8, "turn_on_torch"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getTelecommService()Landroid/telecom/TelecomManager;

    move-result-object v6

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-get12(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v8

    if-nez v8, :cond_a

    if-eqz v6, :cond_c

    invoke-virtual {v6}, Landroid/telecom/TelecomManager;->getCallState()I

    move-result v8

    if-nez v8, :cond_b

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_3

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap7(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-set10(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)Z

    goto/16 :goto_0

    :cond_a
    const/4 v4, 0x1

    goto :goto_1

    :cond_b
    const/4 v4, 0x0

    goto :goto_1

    :cond_c
    const/4 v4, 0x0

    goto :goto_1

    :cond_d
    const-string/jumbo v8, "close_app"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    const-string/jumbo v8, "close_app"

    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v9, v9, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressBackKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap2(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)Z

    move-result v7

    goto/16 :goto_0

    :cond_e
    const-string/jumbo v8, "show_menu"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap8(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v7

    goto/16 :goto_0

    :cond_f
    const-string/jumbo v8, "mi_pay"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap1(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v8, "event_source"

    const-string/jumbo v9, "double_click_power"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap3(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)Z

    move-result v7

    goto/16 :goto_0

    :cond_10
    const-string/jumbo v8, "launch_recents"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap10(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-wrap5(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v7

    goto/16 :goto_0

    :cond_11
    const-string/jumbo v8, "split_screen"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->toggleSplitScreenInternal()V

    const/4 v7, 0x1

    goto/16 :goto_0
.end method
