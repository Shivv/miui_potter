.class public Lcom/android/server/VibratorServiceInjector;
.super Ljava/lang/Object;
.source "VibratorServiceInjector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/VibratorServiceInjector$VibrationInfo;
    }
.end annotation


# static fields
.field private static VIBRATION_THRESHOLD_IN_CALL:J

.field private static sIncall:Z

.field private static sListener:Landroid/telephony/PhoneStateListener;

.field private static sVibrationsCollection:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/server/VibratorServiceInjector$VibrationInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private static sVibrationsLimitPerPkg:I


# direct methods
.method static synthetic -set0(Z)Z
    .locals 0

    sput-boolean p0, Lcom/android/server/VibratorServiceInjector;->sIncall:Z

    return p0
.end method

.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x1e

    sput-wide v0, Lcom/android/server/VibratorServiceInjector;->VIBRATION_THRESHOLD_IN_CALL:J

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Lcom/android/server/VibratorServiceInjector;->sVibrationsCollection:Ljava/util/Map;

    const/4 v0, 0x5

    sput v0, Lcom/android/server/VibratorServiceInjector;->sVibrationsLimitPerPkg:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addToVibrationsCollection(JLandroid/os/VibrationEffect;IILjava/lang/String;)V
    .locals 8

    new-instance v1, Lcom/android/server/VibratorServiceInjector$VibrationInfo;

    move-wide v2, p0

    move-object v4, p2

    move v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/android/server/VibratorServiceInjector$VibrationInfo;-><init>(JLandroid/os/VibrationEffect;IILjava/lang/String;)V

    sget-object v2, Lcom/android/server/VibratorServiceInjector;->sVibrationsCollection:Ljava/util/Map;

    invoke-static {v1}, Lcom/android/server/VibratorServiceInjector$VibrationInfo;->-get0(Lcom/android/server/VibratorServiceInjector$VibrationInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sget-object v2, Lcom/android/server/VibratorServiceInjector;->sVibrationsCollection:Ljava/util/Map;

    invoke-static {v1}, Lcom/android/server/VibratorServiceInjector$VibrationInfo;->-get0(Lcom/android/server/VibratorServiceInjector$VibrationInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v2

    sget v3, Lcom/android/server/VibratorServiceInjector;->sVibrationsLimitPerPkg:I

    if-le v2, v3, :cond_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    return-void
.end method

.method public static dumpVibrations(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    const-string/jumbo v4, "Previous vibrations of per Pkg:"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    sget-object v4, Lcom/android/server/VibratorServiceInjector;->sVibrationsCollection:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/VibratorServiceInjector$VibrationInfo;

    const-string/jumbo v4, "    "

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/server/VibratorServiceInjector$VibrationInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static listenForCallState(Landroid/content/Context;)V
    .locals 3

    new-instance v0, Lcom/android/server/VibratorServiceInjector$1;

    invoke-direct {v0}, Lcom/android/server/VibratorServiceInjector$1;-><init>()V

    sput-object v0, Lcom/android/server/VibratorServiceInjector;->sListener:Landroid/telephony/PhoneStateListener;

    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    sget-object v1, Lcom/android/server/VibratorServiceInjector;->sListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method

.method public static shouldVibrateForMiui(Landroid/content/Context;I)Z
    .locals 1

    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public static weakenVibrationIfNecessary(JI)J
    .locals 2

    sget-boolean v0, Lcom/android/server/VibratorServiceInjector;->sIncall:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-wide v0, Lcom/android/server/VibratorServiceInjector;->VIBRATION_THRESHOLD_IN_CALL:J

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    sget-wide p0, Lcom/android/server/VibratorServiceInjector;->VIBRATION_THRESHOLD_IN_CALL:J

    :cond_0
    return-wide p0
.end method
