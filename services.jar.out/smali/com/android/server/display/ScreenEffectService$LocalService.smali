.class Lcom/android/server/display/ScreenEffectService$LocalService;
.super Lcom/android/server/display/ScreenEffectManager;
.source "ScreenEffectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/ScreenEffectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/ScreenEffectService;


# direct methods
.method constructor <init>(Lcom/android/server/display/ScreenEffectService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-direct {p0}, Lcom/android/server/display/ScreenEffectManager;-><init>()V

    return-void
.end method


# virtual methods
.method public getNightLightBrightness()I
    .locals 1

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v0}, Lcom/android/server/display/ScreenEffectService;->-get7(Lcom/android/server/display/ScreenEffectService;)I

    move-result v0

    return v0
.end method

.method public setNightLight(I)V
    .locals 6

    and-int/lit16 v2, p1, 0xff

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x11070015

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iget-object v4, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v4}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x11070014

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/util/MathUtils;->constrain(III)I

    move-result v0

    const v2, 0xff00

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x8

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x11070018

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iget-object v4, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v4}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x11070017

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/util/MathUtils;->constrain(III)I

    move-result v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2}, Lcom/android/server/display/ScreenEffectService;->-get7(Lcom/android/server/display/ScreenEffectService;)I

    move-result v2

    if-eq v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2, v0}, Lcom/android/server/display/ScreenEffectService;->-set4(Lcom/android/server/display/ScreenEffectService;I)I

    invoke-static {}, Lcom/android/server/display/ScreenEffectService;->-get11()Lcom/android/server/display/DisplayPowerController;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/server/display/ScreenEffectService;->-get11()Lcom/android/server/display/DisplayPowerController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    :cond_0
    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2}, Lcom/android/server/display/ScreenEffectService;->-get8(Lcom/android/server/display/ScreenEffectService;)I

    move-result v2

    if-eq v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2, v1}, Lcom/android/server/display/ScreenEffectService;->-set5(Lcom/android/server/display/ScreenEffectService;I)I

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2}, Lcom/android/server/display/ScreenEffectService;->-get3(Lcom/android/server/display/ScreenEffectService;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    return-void
.end method

.method public updateLocalScreenEffect(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v0}, Lcom/android/server/display/ScreenEffectService;->-get10(Lcom/android/server/display/ScreenEffectService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v0, p1}, Lcom/android/server/display/ScreenEffectService;->-set10(Lcom/android/server/display/ScreenEffectService;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lcom/android/server/display/ScreenEffectService;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v0}, Lcom/android/server/display/ScreenEffectService;->-get5(Lcom/android/server/display/ScreenEffectService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v0}, Lcom/android/server/display/ScreenEffectService;->-get6(Lcom/android/server/display/ScreenEffectService;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v0}, Lcom/android/server/display/ScreenEffectService;->-get3(Lcom/android/server/display/ScreenEffectService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public updateScreenEffect(I)V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v1}, Lcom/android/server/display/ScreenEffectService;->-get2(Lcom/android/server/display/ScreenEffectService;)I

    move-result v0

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v1, p1}, Lcom/android/server/display/ScreenEffectService;->-set0(Lcom/android/server/display/ScreenEffectService;I)I

    sget-boolean v1, Lmiui/os/DeviceFeature;->PERSIST_SCREEN_EFFECT:Z

    if-nez v1, :cond_1

    if-ne v0, v3, :cond_1

    if-eq p1, v0, :cond_1

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v1}, Lcom/android/server/display/ScreenEffectService;->-get4(Lcom/android/server/display/ScreenEffectService;)Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v1

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->isLockScreenDisabled(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/display/ScreenEffectService;->-set10(Lcom/android/server/display/ScreenEffectService;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService$LocalService;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v1}, Lcom/android/server/display/ScreenEffectService;->-get3(Lcom/android/server/display/ScreenEffectService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    return-void
.end method
