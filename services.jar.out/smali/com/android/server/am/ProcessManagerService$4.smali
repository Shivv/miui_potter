.class Lcom/android/server/am/ProcessManagerService$4;
.super Ljava/lang/Object;
.source "ProcessManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessManagerService;->notifyForegroundInfoChanged(Landroid/content/pm/ApplicationInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;

.field final synthetic val$foregroundAppInfo:Landroid/content/pm/ApplicationInfo;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessManagerService;Landroid/content/pm/ApplicationInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$4;->this$0:Lcom/android/server/am/ProcessManagerService;

    iput-object p2, p0, Lcom/android/server/am/ProcessManagerService$4;->val$foregroundAppInfo:Landroid/content/pm/ApplicationInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$4;->this$0:Lcom/android/server/am/ProcessManagerService;

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$4;->val$foregroundAppInfo:Landroid/content/pm/ApplicationInfo;

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessManagerService;->-wrap0(Lcom/android/server/am/ProcessManagerService;Landroid/content/pm/ApplicationInfo;)V

    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$4;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-get1(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ForegroundInfoManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$4;->val$foregroundAppInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1}, Lcom/android/server/am/ForegroundInfoManager;->notifyForegroundInfoChanged(Landroid/content/pm/ApplicationInfo;)V

    return-void
.end method
