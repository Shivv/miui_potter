.class final Lcom/android/server/am/ProcessManagerService$LocalService;
.super Lmiui/process/ProcessManagerInternal;
.source "ProcessManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/am/ProcessManagerService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-direct {p0}, Lmiui/process/ProcessManagerInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessManagerService$LocalService;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService$LocalService;-><init>(Lcom/android/server/am/ProcessManagerService;)V

    return-void
.end method


# virtual methods
.method public notifyActivityChanged(Landroid/content/ComponentName;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyActivityChanged(Landroid/content/ComponentName;)V

    return-void
.end method

.method public notifyForegroundInfoChanged(Landroid/content/pm/ApplicationInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyForegroundInfoChanged(Landroid/content/pm/ApplicationInfo;)V

    return-void
.end method

.method public recordKillProcessEventIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/ProcessManagerService;->recordKillProcessEventIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public updateProcessForegroundLocked(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-get4(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->updateProcessForegroundLocked(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method
