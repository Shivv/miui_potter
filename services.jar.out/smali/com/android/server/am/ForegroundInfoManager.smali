.class public Lcom/android/server/am/ForegroundInfoManager;
.super Ljava/lang/Object;
.source "ForegroundInfoManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ProcessManager"


# instance fields
.field private final mActivityChangeListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lmiui/process/IActivityChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityLock:Ljava/lang/Object;

.field private mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

.field private mForegroundInfo:Lmiui/process/ForegroundInfo;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mForegroundLock"
    .end annotation
.end field

.field private final mForegroundInfoListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lmiui/process/IForegroundInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private mForegroundLock:Ljava/lang/Object;

.field private mLastActivityComponent:Landroid/content/ComponentName;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mActivityLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    new-instance v0, Lmiui/process/ForegroundInfo;

    invoke-direct {v0}, Lmiui/process/ForegroundInfo;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    return-void
.end method

.method private notifyForegroundInfoLocked()V
    .locals 4

    iget-object v2, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lmiui/process/IForegroundInfoListener;

    iget-object v3, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    invoke-interface {v2, v3}, Lmiui/process/IForegroundInfoListener;->onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    return-void
.end method


# virtual methods
.method public getForegroundInfo()Lmiui/process/ForegroundInfo;
    .locals 3

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmiui/process/ForegroundInfo;

    iget-object v2, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    invoke-direct {v0, v2}, Lmiui/process/ForegroundInfo;-><init>(Lmiui/process/ForegroundInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public notifyActivitiesChangedLocked(Landroid/content/ComponentName;)V
    .locals 5

    iget-object v4, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    :goto_0
    if-ltz v2, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lmiui/process/IActivityChangeListener;

    iget-object v4, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v3, v0, p1}, Lcom/android/server/am/ForegroundInfoManager;->notifyActivityChangedIfNeededLocked(Lmiui/process/IActivityChangeListener;Ljava/lang/Object;Landroid/content/ComponentName;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    :cond_0
    iget-object v4, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    return-void
.end method

.method public notifyActivityChanged(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {p1, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/android/server/am/ForegroundInfoManager;->notifyActivitiesChangedLocked(Landroid/content/ComponentName;)V

    iput-object p1, p0, Lcom/android/server/am/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public notifyActivityChangedIfNeededLocked(Lmiui/process/IActivityChangeListener;Ljava/lang/Object;Landroid/content/ComponentName;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-eqz p2, :cond_0

    instance-of v3, p2, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;

    iget-object v3, p0, Lcom/android/server/am/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/server/am/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget-object v3, p0, Lcom/android/server/am/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/server/am/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v3, v0, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;->targetPackages:Ljava/util/List;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;->targetActivities:Ljava/util/List;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;->targetPackages:Ljava/util/List;

    invoke-virtual {p3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;->targetPackages:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;->targetActivities:Ljava/util/List;

    invoke-virtual {p3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;->targetActivities:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/android/server/am/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    invoke-interface {p1, v3, p3}, Lmiui/process/IActivityChangeListener;->onActivityChanged(Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    :cond_3
    return-void

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public notifyForegroundInfoChanged(Landroid/content/pm/ApplicationInfo;)V
    .locals 5

    iget-object v2, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v2

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v1, v1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v3, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    sget-object v1, Lcom/android/server/am/ForegroundInfoManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip notify foregroundAppInfo:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v3, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v3, v3, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iput-object v3, v1, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v3, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v3, v3, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    iput v3, v1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v3, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v3, v1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v3, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v3, v1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-static {v1}, Lcom/android/server/am/ProcessUtils;->getMultiWindowForegroundAppInfo(Lcom/android/server/am/ActivityManagerService;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v3, v1, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v3, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v3, v1, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    :goto_0
    invoke-direct {p0}, Lcom/android/server/am/ForegroundInfoManager;->notifyForegroundInfoLocked()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    return-void

    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    const/4 v3, 0x0

    iput-object v3, v1, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    const/4 v3, -0x1

    iput v3, v1, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public registerActivityChangeListener(Ljava/util/List;Ljava/util/List;Lmiui/process/IActivityChangeListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lmiui/process/IActivityChangeListener;",
            ")V"
        }
    .end annotation

    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v0, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/am/ForegroundInfoManager$ActivityChangeInfo;-><init>(Lcom/android/server/am/ForegroundInfoManager;Ljava/util/List;Ljava/util/List;)V

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p3, v0}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public unregisterActivityChangeListener(Lmiui/process/IActivityChangeListener;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
