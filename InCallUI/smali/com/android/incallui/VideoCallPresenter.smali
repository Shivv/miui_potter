.class public Lcom/android/incallui/VideoCallPresenter;
.super Lcom/android/incallui/Presenter;
.source "VideoCallPresenter.java"

# interfaces
.implements Lcom/android/incallui/InCallPresenter$IncomingCallListener;
.implements Lcom/android/incallui/InCallPresenter$InCallOrientationListener;
.implements Lcom/android/incallui/InCallPresenter$InCallStateListener;
.implements Lcom/android/incallui/InCallPresenter$InCallDetailsListener;
.implements Lcom/android/incallui/InCallVideoCallCallbackNotifier$SurfaceChangeListener;
.implements Lcom/android/incallui/InCallVideoCallCallbackNotifier$VideoEventListener;
.implements Lcom/android/incallui/InCallVideoCallCallbackNotifier$SessionModificationListener;
.implements Lcom/android/incallui/InCallCameraManager$CameraSelectionListener;
.implements Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;
.implements Lcom/android/incallui/InCallPresenter$InCallDialPadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;,
        Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;,
        Lcom/android/incallui/VideoCallPresenter$VideoCallUi;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/Presenter",
        "<",
        "Lcom/android/incallui/VideoCallPresenter$VideoCallUi;",
        ">;",
        "Lcom/android/incallui/InCallPresenter$IncomingCallListener;",
        "Lcom/android/incallui/InCallPresenter$InCallOrientationListener;",
        "Lcom/android/incallui/InCallPresenter$InCallStateListener;",
        "Lcom/android/incallui/InCallPresenter$InCallDetailsListener;",
        "Lcom/android/incallui/InCallVideoCallCallbackNotifier$SurfaceChangeListener;",
        "Lcom/android/incallui/InCallVideoCallCallbackNotifier$VideoEventListener;",
        "Lcom/android/incallui/InCallVideoCallCallbackNotifier$SessionModificationListener;",
        "Lcom/android/incallui/InCallCameraManager$CameraSelectionListener;",
        "Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;",
        "Lcom/android/incallui/InCallPresenter$InCallDialPadListener;"
    }
.end annotation


# static fields
.field private static mIsVideoMode:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentCallState:I

.field private mCurrentVideoState:I

.field private mDeviceOrientation:I

.field private mForceProcessIncoming:Z

.field private mIsFullScreen:Z

.field private mIsVideoConference:Z

.field private mMinimumVideoDimension:F

.field private mOrientationMode:I

.field private mPeerOrientation:I

.field private mPreviewSurfaceState:I

.field private mPrimaryCall:Lcom/android/incallui/Call;

.field mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

.field private mVideoCall:Landroid/telecom/InCallService$VideoCall;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/VideoCallPresenter;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    return v0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/VideoCallPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->toggleFullScreen()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/incallui/VideoCallPresenter;->mIsVideoMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/incallui/Presenter;-><init>()V

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentCallState:I

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPeerOrientation:I

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    iput-boolean v0, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mOrientationMode:I

    return-void
.end method

.method private autoFullScreen()V
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    invoke-virtual {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private cancleFullScreen()V
    .locals 3

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    invoke-virtual {v1, v2}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    invoke-virtual {v1, v2}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->removeMessages(I)V

    :cond_0
    iget-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    if-nez v1, :cond_1

    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    invoke-virtual {v1, v2}, Lcom/android/incallui/InCallPresenter;->setFullScreenVideoState(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setFullScreenMode(Z)V

    :cond_2
    return-void
.end method

.method private changeVideoCall(Lcom/android/incallui/Call;)V
    .locals 4

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getTelecommCall()Landroid/telecom/Call;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "changeVideoCall to videoCall="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " mVideoCall="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    if-eqz v2, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const-string/jumbo v2, "Video call or primary call is null. Return"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/android/incallui/VideoCallPresenter;->isVideoCallOrCrbt(Lcom/android/incallui/Call;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/incallui/Call;->earlyRegisterVideoCallbak()V

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->enterVideoMode(Lcom/android/incallui/Call;)V

    :cond_3
    return-void
.end method

.method private checkForCallStateChange(Lcom/android/incallui/Call;)V
    .locals 8

    const/4 v7, 0x1

    invoke-static {p1}, Lcom/android/incallui/VideoCallPresenter;->isVideoCallOrCrbt(Lcom/android/incallui/Call;)Z

    move-result v2

    iget v5, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentCallState:I

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v6

    if-eq v5, v6, :cond_0

    const/4 v1, 0x1

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "checkForCallStateChange: isVideoCallOrCrbt= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " hasCallStateChanged="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " isVideoMode="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez v1, :cond_1

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_3

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/incallui/InCallPresenter;->getInCallCameraManager()Lcom/android/incallui/InCallCameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallCameraManager;->getActiveCameraId()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/android/incallui/VideoCallPresenter;->updateCameraSelection(Lcom/android/incallui/Call;)V

    invoke-virtual {v0}, Lcom/android/incallui/InCallCameraManager;->getActiveCameraId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {p1}, Lcom/android/incallui/CallUtils;->isActiveVideoCall(Lcom/android/incallui/Call;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v5

    invoke-virtual {p0, v5, v7}, Lcom/android/incallui/VideoCallPresenter;->enableCamera(Landroid/telecom/InCallService$VideoCall;Z)V

    :cond_2
    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v5

    invoke-direct {p0, v5, p1}, Lcom/android/incallui/VideoCallPresenter;->showVideoUi(ILcom/android/incallui/Call;)V

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->configPreviewSize()V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/incallui/InCallPresenter;->enableInCallOrientationEventListener(Z)V

    :cond_3
    return-void
.end method

.method private checkForOrientationAllowedChange(Lcom/android/incallui/Call;)V
    .locals 3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getTelecommCall()Landroid/telecom/Call;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Lcom/android/incallui/VoLTEProxy;->getOrientationMode(Lcom/android/incallui/Call;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "checkForOrientationAllowedChange: currentOrientationMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/incallui/VideoCallPresenter;->mOrientationMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", newOrientationMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    iget v1, p0, Lcom/android/incallui/VideoCallPresenter;->mOrientationMode:I

    if-eq v0, v1, :cond_1

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mOrientationMode:I

    :cond_1
    return-void
.end method

.method private checkForVideoCallChange(Lcom/android/incallui/Call;)V
    .locals 3

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getTelecommCall()Landroid/telecom/Call;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "checkForVideoCallChange: videoCall="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " mVideoCall="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->changeVideoCall(Lcom/android/incallui/Call;)V

    :cond_0
    return-void
.end method

.method private checkForVideoStateChange(Lcom/android/incallui/Call;)V
    .locals 4

    invoke-static {p1}, Lcom/android/incallui/VideoCallPresenter;->isVideoCallOrCrbt(Lcom/android/incallui/Call;)Z

    move-result v1

    iget v2, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentVideoState:I

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v3

    if-eq v2, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "checkForVideoStateChange: isVideoCallOrCrbt= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " hasVideoStateChanged="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isVideoMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez v0, :cond_1

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/android/incallui/VideoCallPresenter;->updateCameraSelection(Lcom/android/incallui/Call;)V

    if-eqz v1, :cond_3

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->enterVideoMode(Lcom/android/incallui/Call;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->exitVideoMode()V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/InCallPresenter;->isShowingInCallUi()Z

    move-result v2

    if-nez v2, :cond_2

    const v2, 0x7f0b00aa

    invoke-static {v2}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    goto :goto_1
.end method

.method private cleanupSurfaces()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    const-string/jumbo v1, "cleanupSurfaces"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->cleanupSurfaces()V

    return-void
.end method

.method private configPreviewSize()V
    .locals 5

    const-string/jumbo v2, "config preview view size"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v1, :cond_0

    const-string/jumbo v2, "Error VideoCallUi is null. Return."

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-interface {v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getPreviewSurfaceSize()Landroid/graphics/Point;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/android/incallui/VideoCallPresenter;->setPreviewSize(IIZ)V

    :cond_1
    return-void
.end method

.method private enableZoomControl(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    const-string/jumbo v1, "Error VideoCallUi is null. Return."

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-interface {v0, p1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->enableZoomControl(Z)V

    return-void
.end method

.method private enterVideoMode(Lcom/android/incallui/Call;)V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "enterVideoMode videoCall= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " videoState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v1, :cond_0

    const-string/jumbo v3, "Error VideoCallUi is null so returning"

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/android/incallui/VideoCallPresenter;->showVideoUi(ILcom/android/incallui/Call;)V

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->isDisplayVideoSurfaceCreated()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Calling setDisplaySurface with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getDisplayVideoSurface()Landroid/view/Surface;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getDisplayVideoSurface()Landroid/view/Surface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/telecom/InCallService$VideoCall;->setDisplaySurface(Landroid/view/Surface;)V

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "enterVideoMode mDeviceOrientation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget v3, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    invoke-virtual {v2, v3}, Landroid/telecom/InCallService$VideoCall;->setDeviceOrientation(I)V

    invoke-interface {v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->isPreviewVideoSurfaceCreated()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lcom/android/incallui/VideoCallPresenter;->isCameraRequired(I)Z

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/android/incallui/VideoCallPresenter;->enableCamera(Landroid/telecom/InCallService$VideoCall;Z)V

    :cond_2
    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentVideoState:I

    sput-boolean v5, Lcom/android/incallui/VideoCallPresenter;->mIsVideoMode:Z

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/InCallPresenter;->getProximitySensor()Lcom/android/incallui/ProximitySensor;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/incallui/Call;->isVolteCallPlayingVideoCrbt()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/incallui/ProximitySensor;->onVideoMode(Z)V

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/android/incallui/InCallPresenter;->enableInCallOrientationEventListener(Z)V

    :cond_3
    return-void
.end method

.method private exitVideoMode()V
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v1, "exitVideoMode"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iput v3, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentVideoState:I

    iget v1, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentVideoState:I

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-direct {p0, v1, v2}, Lcom/android/incallui/VideoCallPresenter;->showVideoUi(ILcom/android/incallui/Call;)V

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {p0, v1, v3}, Lcom/android/incallui/VideoCallPresenter;->enableCamera(Landroid/telecom/InCallService$VideoCall;Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "exitVideoMode mIsFullScreen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->toggleFullScreen()V

    :cond_1
    sput-boolean v3, Lcom/android/incallui/VideoCallPresenter;->mIsVideoMode:Z

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->removeMessages(I)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getProximitySensor()Lcom/android/incallui/ProximitySensor;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/android/incallui/ProximitySensor;->onVideoMode(Z)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/android/incallui/InCallPresenter;->enableInCallOrientationEventListener(Z)V

    return-void
.end method

.method private static isCameraDirectionSet(Lcom/android/incallui/Call;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoCallOrCrbt(Lcom/android/incallui/Call;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoSettings()Lcom/android/incallui/Call$VideoSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/Call$VideoSettings;->getCameraDir()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isCameraRequired()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v0

    invoke-static {v0}, Lcom/android/incallui/VideoCallPresenter;->isCameraRequired(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isCameraRequired(I)Z
    .locals 1

    invoke-static {p0}, Lcom/android/incallui/CallAdapterUtils;->isBidirectional(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/incallui/CallAdapterUtils;->isTransmissionEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/android/incallui/VideoCallPresenter;->isVideoPausedByUser()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isVideoCallOrCrbt(Lcom/android/incallui/Call;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v0

    invoke-static {v0}, Lcom/android/incallui/CallAdapterUtils;->isVideo(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVideoPausedByUser()Z
    .locals 2

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getCallToolsFragment()Lcom/android/incallui/CallToolsFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->isPauseVideoChecked()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private maybeEnableCamera()V
    .locals 2

    iget v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->isCameraRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/incallui/VideoCallPresenter;->enableCamera(Landroid/telecom/InCallService$VideoCall;Z)V

    :cond_0
    return-void
.end method

.method private onPrimaryCallChanged(Lcom/android/incallui/Call;)V
    .locals 4

    invoke-static {p1}, Lcom/android/incallui/VideoCallPresenter;->isVideoCallOrCrbt(Lcom/android/incallui/Call;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPrimaryCallChanged: isVideoCallOrCrbt="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isVideoMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "onPrimaryCallChanged: Exiting video mode..."

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->exitVideoMode()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v0, :cond_0

    const-string/jumbo v2, "onPrimaryCallChanged: Entering video mode..."

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/incallui/Call;->earlyRegisterVideoCallbak()V

    invoke-static {p1}, Lcom/android/incallui/VideoCallPresenter;->updateCameraSelection(Lcom/android/incallui/Call;)V

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->enterVideoMode(Lcom/android/incallui/Call;)V

    goto :goto_0
.end method

.method private setDisplayVideoSize(III)V
    .locals 4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setDisplayVideoSize:Received rotation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/incallui/VideoCallPresenter;->adjustDisplayToFullScreen(III)Landroid/graphics/Point;

    move-result-object v0

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-interface {v1, v2, v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayVideoSize(II)V

    return-void
.end method

.method private setPreviewSize(IIZ)V
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v6

    check-cast v6, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v6, :cond_0

    return-void

    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setPreviewSize width= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", height="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", isFullScreen="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-ltz p1, :cond_1

    if-gez p2, :cond_2

    :cond_1
    return-void

    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p0, p1, p2}, Lcom/android/incallui/VideoCallPresenter;->adjustPreviewToFullScreen(II)Landroid/graphics/Point;

    move-result-object v5

    iget v4, v5, Landroid/graphics/Point;->x:I

    iget v1, v5, Landroid/graphics/Point;->y:I

    const/16 v7, 0x11

    invoke-interface {v6, v7}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewGravity(I)V

    invoke-interface {v6, v9, v9, v9, v9}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewMargin(IIII)V

    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setPreviewSize previewWidth= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", previewHeight="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget v7, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    invoke-static {v6, v7, v4, v1, p3}, Lcom/android/incallui/VoLTEProxy;->setPreviewSizeAndRotationForMTK(Lcom/android/incallui/VideoCallPresenter$VideoCallUi;IIIZ)Z

    move-result v7

    if-eqz v7, :cond_6

    :goto_1
    return-void

    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    if-lez p1, :cond_4

    if-lez p2, :cond_4

    int-to-float v7, p1

    int-to-float v8, p2

    div-float v0, v7, v8

    :cond_4
    iget v7, p0, Lcom/android/incallui/VideoCallPresenter;->mMinimumVideoDimension:F

    float-to-int v4, v7

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v7, v0, v7

    if-lez v7, :cond_5

    iget v7, p0, Lcom/android/incallui/VideoCallPresenter;->mMinimumVideoDimension:F

    mul-float/2addr v7, v0

    float-to-int v1, v7

    :goto_2
    iget-object v7, p0, Lcom/android/incallui/VideoCallPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090015

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v7, p0, Lcom/android/incallui/VideoCallPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090016

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Call is active, set margin top= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", right="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v7, 0x35

    invoke-interface {v6, v7}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewGravity(I)V

    invoke-interface {v6, v9, v3, v2, v9}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewMargin(IIII)V

    goto/16 :goto_0

    :cond_5
    iget v7, p0, Lcom/android/incallui/VideoCallPresenter;->mMinimumVideoDimension:F

    div-float/2addr v7, v0

    float-to-int v1, v7

    goto :goto_2

    :cond_6
    invoke-interface {v6, v4, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewSize(II)V

    goto :goto_1
.end method

.method private showVideoUi(ILcom/android/incallui/Call;)V
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    const-string/jumbo v1, "showVideoUi, VideoCallUi is null returning"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "showVideoUi, videoState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/incallui/CallAdapterUtils;->isBidirectional(I)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/android/incallui/Call;->getState()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    :cond_1
    :goto_0
    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showVideoBidrectionalUi(Z)V

    :goto_1
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-static {p1}, Lcom/android/incallui/CallAdapterUtils;->isAudioOnly(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/InCallPresenter;->enableScreenTimeout(Z)V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lcom/android/incallui/CallAdapterUtils;->isTransmissionEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showVideoTransmissionUi()V

    goto :goto_1

    :cond_4
    invoke-static {p1}, Lcom/android/incallui/CallAdapterUtils;->isReceptionEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showVideoReceptionUi()V

    goto :goto_1

    :cond_5
    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->hideVideoUi()V

    goto :goto_1
.end method

.method private static toCameraDirection(I)I
    .locals 1

    invoke-static {p0}, Lcom/android/incallui/CallAdapterUtils;->isTransmissionEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/incallui/CallAdapterUtils;->isBidirectional(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static toSimpleString(Lcom/android/incallui/Call;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/Call;->toSimpleString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private toggleFullScreen()V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    invoke-virtual {v1, v4}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->removeMessages(I)V

    iget-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    invoke-virtual {v1, v2}, Lcom/android/incallui/InCallPresenter;->setFullScreenVideoState(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setFullScreenMode(Z)V

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "toggleFullScreen: mIsFullScreen="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", isVideoMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method private updateCallCache(Lcom/android/incallui/Call;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iput v1, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentVideoState:I

    iput v1, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentCallState:I

    iput-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    iput-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v0

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentVideoState:I

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentCallState:I

    iput-object p1, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    goto :goto_0
.end method

.method private static updateCameraSelection(Lcom/android/incallui/Call;)V
    .locals 7

    const/4 v3, 0x0

    const-string/jumbo v4, "VideoCallPresenter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateCameraSelection: call="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v4, "VideoCallPresenter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateCameraSelection: call="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0}, Lcom/android/incallui/VideoCallPresenter;->toSimpleString(Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/CallList;->getActiveCall()Lcom/android/incallui/Call;

    move-result-object v0

    const/4 v1, -0x1

    if-nez p0, :cond_1

    const/4 v1, -0x1

    const-string/jumbo v4, "VideoCallPresenter"

    const-string/jumbo v5, "updateCameraSelection: Call object is null. Setting camera direction to default value (CAMERA_DIRECTION_UNKNOWN)"

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string/jumbo v4, "VideoCallPresenter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateCameraSelection: Setting camera direction to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " Call="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/InCallPresenter;->getInCallCameraManager()Lcom/android/incallui/InCallCameraManager;

    move-result-object v2

    if-nez v1, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-virtual {v2, v3}, Lcom/android/incallui/InCallCameraManager;->setUseFrontFacingCamera(Z)V

    return-void

    :cond_1
    invoke-static {p0}, Lcom/android/incallui/CallUtils;->isAudioCall(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoSettings()Lcom/android/incallui/Call$VideoSettings;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/incallui/Call$VideoSettings;->setCameraDir(I)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/android/incallui/VideoCallPresenter;->isVideoCallOrCrbt(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {p0}, Lcom/android/incallui/CallUtils;->isIncomingVideoCall(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getVideoSettings()Lcom/android/incallui/Call$VideoSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/Call$VideoSettings;->getCameraDir()I

    move-result v1

    goto :goto_0

    :cond_3
    invoke-static {p0}, Lcom/android/incallui/CallUtils;->isOutgoingVideoCall(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {p0}, Lcom/android/incallui/VideoCallPresenter;->isCameraDirectionSet(Lcom/android/incallui/Call;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v4

    invoke-static {v4}, Lcom/android/incallui/VideoCallPresenter;->toCameraDirection(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoSettings()Lcom/android/incallui/Call$VideoSettings;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/incallui/Call$VideoSettings;->setCameraDir(I)V

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lcom/android/incallui/CallUtils;->isOutgoingVideoCall(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoSettings()Lcom/android/incallui/Call$VideoSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/Call$VideoSettings;->getCameraDir()I

    move-result v1

    goto/16 :goto_0

    :cond_5
    invoke-static {p0}, Lcom/android/incallui/CallUtils;->isActiveVideoCall(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-static {p0}, Lcom/android/incallui/VideoCallPresenter;->isCameraDirectionSet(Lcom/android/incallui/Call;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v4

    invoke-static {v4}, Lcom/android/incallui/VideoCallPresenter;->toCameraDirection(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoSettings()Lcom/android/incallui/Call$VideoSettings;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/incallui/Call$VideoSettings;->setCameraDir(I)V

    goto/16 :goto_0

    :cond_6
    invoke-static {p0}, Lcom/android/incallui/CallUtils;->isActiveVideoCall(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoSettings()Lcom/android/incallui/Call$VideoSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/Call$VideoSettings;->getCameraDir()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v4

    invoke-static {v4}, Lcom/android/incallui/VideoCallPresenter;->toCameraDirection(I)I

    move-result v1

    goto/16 :goto_0
.end method

.method private updateFullScreen()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->autoFullScreen()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->isCallButtonVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->toggleFullScreen()V

    goto :goto_0
.end method

.method private updateUi(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 8

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_2

    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq p1, v3, :cond_2

    iget v3, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    if-eqz v3, :cond_1

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayRotation(I)V

    :cond_1
    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setFullScreenMode(Z)V

    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setFullScreenPreviewPosition()V

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewOnTouchListener(Z)V

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->changePreviewTopExtraHeight(Z)V

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showMask(Z)V

    :cond_2
    if-eqz p3, :cond_e

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v3

    if-eqz v3, :cond_e

    iget-boolean v3, p0, Lcom/android/incallui/VideoCallPresenter;->mIsVideoConference:Z

    if-eqz v3, :cond_3

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showPreviewVideo(Z)V

    :cond_3
    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_8

    invoke-virtual {p3}, Lcom/android/incallui/Call;->isDelayDisplayVideoShown()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getDisplayVideoAlpha()F

    move-result v3

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_4

    invoke-interface {v0, v4}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayVideoAlpha(F)V

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;

    invoke-direct {v4, p0, p3}, Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;-><init>(Lcom/android/incallui/VideoCallPresenter;Lcom/android/incallui/Call;)V

    const-wide/16 v6, 0x352

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    :goto_0
    invoke-interface {v0, v1, v5}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->updateVideoTextureViews(ZLjava/lang/String;)V

    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->initPreviewPosition()V

    invoke-interface {v0, v2}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showBackground(Z)V

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showMask(Z)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/CallList;->getBackgroundCall()Lcom/android/incallui/Call;

    move-result-object v3

    if-eqz v3, :cond_5

    move v1, v2

    :cond_5
    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->changePreviewTopExtraHeight(Z)V

    invoke-interface {v0, v2}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewOnTouchListener(Z)V

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->updateFullScreen()V

    :cond_6
    :goto_1
    return-void

    :cond_7
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v0, v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayVideoAlpha(F)V

    goto :goto_0

    :cond_8
    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->PENDING_OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_9

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->cancleFullScreen()V

    invoke-interface {v0, v4}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayVideoAlpha(F)V

    goto :goto_1

    :cond_9
    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_b

    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setFullScreenPreviewPosition()V

    invoke-virtual {p3}, Lcom/android/incallui/Call;->isPlayingVideoCrbt()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {p3}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->updateVideoTextureViews(ZLjava/lang/String;)V

    goto :goto_1

    :cond_a
    invoke-interface {v0, v4}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayVideoAlpha(F)V

    invoke-interface {v0, v1, v5}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->updateVideoTextureViews(ZLjava/lang/String;)V

    goto :goto_1

    :cond_b
    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->INCOMING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_d

    iget-boolean v3, p0, Lcom/android/incallui/VideoCallPresenter;->mForceProcessIncoming:Z

    if-eqz v3, :cond_c

    invoke-interface {v0, v2}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showMask(Z)V

    iget-boolean v2, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    if-nez v2, :cond_c

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->toggleFullScreen()V

    :cond_c
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/CallList;->getCurrentCallCount()I

    move-result v2

    if-nez v2, :cond_6

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showPreviewVideo(Z)V

    goto :goto_1

    :cond_d
    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->changePreviewTopExtraHeight(Z)V

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showMask(Z)V

    goto :goto_1

    :cond_e
    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewOnTouchListener(Z)V

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->changePreviewTopExtraHeight(Z)V

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->showBackground(Z)V

    invoke-interface {v0, v4}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayVideoAlpha(F)V

    invoke-interface {v0, v1, v5}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->updateVideoTextureViews(ZLjava/lang/String;)V

    goto :goto_1
.end method

.method private updateVideoCall(Lcom/android/incallui/Call;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->checkForVideoCallChange(Lcom/android/incallui/Call;)V

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->checkForVideoStateChange(Lcom/android/incallui/Call;)V

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->checkForCallStateChange(Lcom/android/incallui/Call;)V

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->checkForOrientationAllowedChange(Lcom/android/incallui/Call;)V

    return-void
.end method


# virtual methods
.method public adjustDisplayToFullScreen(III)Landroid/graphics/Point;
    .locals 9

    const/high16 v8, 0x3f800000    # 1.0f

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p2, p3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v2, :cond_0

    const-string/jumbo v5, "adjustDisplayToFullScreen ui is null"

    invoke-static {p0, v5}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_0
    invoke-interface {v2}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getScreenSize()Landroid/graphics/Point;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "adjustToFullScreen rotation="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", width="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", height="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "adjustToFullScreen screen.x="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", screen.y="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget v5, v1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    int-to-float v6, p2

    div-float/2addr v5, v6

    iget v6, v1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    int-to-float v7, p3

    div-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    const/4 v4, 0x1

    :goto_0
    iget v5, v1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    int-to-float v6, p3

    div-float/2addr v5, v6

    iget v6, v1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    int-to-float v7, p2

    div-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    const/4 v3, 0x1

    :goto_1
    sget-boolean v5, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-eqz v5, :cond_1

    xor-int/lit8 v4, v4, 0x1

    xor-int/lit8 v3, v3, 0x1

    :cond_1
    rem-int/lit16 v5, p1, 0xb4

    if-nez v5, :cond_6

    int-to-float v5, p2

    int-to-float v6, p3

    div-float/2addr v5, v6

    cmpl-float v5, v5, v8

    if-ltz v5, :cond_4

    iget v5, v1, Landroid/graphics/Point;->x:I

    iput v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, p3

    iget v6, v1, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    int-to-float v6, p2

    div-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Point;->y:I

    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "adjustToFullScreen result.x="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", result.y="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    if-eqz v4, :cond_5

    iget v5, v1, Landroid/graphics/Point;->x:I

    iput v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, p3

    iget v6, v1, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    int-to-float v6, p2

    div-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Point;->y:I

    goto :goto_2

    :cond_5
    int-to-float v5, p2

    iget v6, v1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    int-to-float v6, p3

    div-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Point;->x:I

    iget v5, v1, Landroid/graphics/Point;->y:I

    iput v5, v0, Landroid/graphics/Point;->y:I

    goto :goto_2

    :cond_6
    int-to-float v5, p2

    int-to-float v6, p3

    div-float/2addr v5, v6

    cmpl-float v5, v5, v8

    if-ltz v5, :cond_8

    if-eqz v3, :cond_7

    int-to-float v5, p2

    iget v6, v1, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    int-to-float v6, p3

    div-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Point;->x:I

    iget v5, v1, Landroid/graphics/Point;->x:I

    iput v5, v0, Landroid/graphics/Point;->y:I

    goto :goto_2

    :cond_7
    iget v5, v1, Landroid/graphics/Point;->y:I

    iput v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, p3

    iget v6, v1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    int-to-float v6, p2

    div-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Point;->y:I

    goto :goto_2

    :cond_8
    int-to-float v5, p2

    iget v6, v1, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    int-to-float v6, p3

    div-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Point;->x:I

    iget v5, v1, Landroid/graphics/Point;->x:I

    iput v5, v0, Landroid/graphics/Point;->y:I

    goto :goto_2
.end method

.method public adjustPreviewToFullScreen(II)Landroid/graphics/Point;
    .locals 7

    if-le p1, p2, :cond_0

    move v2, p1

    move p1, p2

    move p2, v2

    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v3, :cond_1

    return-object v0

    :cond_1
    invoke-interface {v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getScreenSize()Landroid/graphics/Point;

    move-result-object v1

    iget v4, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    int-to-float v5, p1

    div-float/2addr v4, v5

    iget v5, v1, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    int-to-float v6, p2

    div-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    iget v4, v1, Landroid/graphics/Point;->x:I

    iput v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, p2

    iget v5, v1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    int-to-float v5, p1

    div-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v0, Landroid/graphics/Point;->y:I

    :goto_0
    return-object v0

    :cond_2
    int-to-float v4, p1

    iget v5, v1, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    int-to-float v5, p2

    div-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v0, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->y:I

    iput v4, v0, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public enableCamera(Landroid/telecom/InCallService$VideoCall;Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "enableCamera: VideoCall="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " enabling="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string/jumbo v2, "enableCamera: VideoCall is null."

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    if-eqz p2, :cond_2

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/InCallPresenter;->getInCallCameraManager()Lcom/android/incallui/InCallCameraManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallCameraManager;->getActiveCameraId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/telecom/InCallService$VideoCall;->setCamera(Ljava/lang/String;)V

    const/4 v2, 0x1

    iput v2, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    if-nez v0, :cond_1

    const-string/jumbo v2, "enableCamera: the active camera id is null"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/telecom/InCallService$VideoCall;->requestCameraCapabilities()V

    :goto_0
    return-void

    :cond_2
    iput v5, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    invoke-virtual {p1, v4}, Landroid/telecom/InCallService$VideoCall;->setCamera(Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/android/incallui/VideoCallPresenter;->enableZoomControl(Z)V

    goto :goto_0
.end method

.method public forceToggleFullScreen()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->toggleFullScreen()V

    :cond_0
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/incallui/VideoCallPresenter;->mMinimumVideoDimension:F

    new-instance v0, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;-><init>(Lcom/android/incallui/VideoCallPresenter;Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;)V

    iput-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getInCallState()Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/incallui/VideoCallPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void
.end method

.method public isVideoMode()Z
    .locals 1

    sget-boolean v0, Lcom/android/incallui/VideoCallPresenter;->mIsVideoMode:Z

    return v0
.end method

.method public onActiveCameraSelectionChanged(Z)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onActiveCameraSelectionChanged: front facing camera "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    const-string/jumbo v1, "onActiveCameraSelectionChanged: VideoCallUi is null"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/incallui/VideoCallPresenter;->enableZoomControl(Z)V

    return-void
.end method

.method public onCallDataUsageChange(J)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCallDataUsageChange dataUsage="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    const-string/jumbo v1, "onCallDataUsageChange: VideoCallUi is null"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    return-void
.end method

.method public onCallSessionEvent(I)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCallSessionEvent event ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    const-string/jumbo v1, "onCallSessionEvent: VideoCallUi is null"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    return-void
.end method

.method public onCameraDimensionsChange(Lcom/android/incallui/Call;II)V
    .locals 6

    const/4 v5, 0x3

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onCameraDimensionsChange call="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " width="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " height="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v1, :cond_0

    const-string/jumbo v2, "onCameraDimensionsChange ui is null"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {p1, v3}, Lcom/android/incallui/Call;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v2, "Call is not primary call"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    iget v3, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    if-nez v3, :cond_2

    const-string/jumbo v2, "onCameraDimensionsChange camera is off!"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_2
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    invoke-interface {v1, p2, p3}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setPreviewSurfaceSize(II)V

    invoke-virtual {p1}, Lcom/android/incallui/Call;->isVolteCallPlayingVideoCrbt()Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onCameraDimensionsChange isCrbtPlaying="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v3

    if-eq v3, v5, :cond_3

    xor-int/lit8 v2, v0, 0x1

    :cond_3
    invoke-direct {p0, p2, p3, v2}, Lcom/android/incallui/VideoCallPresenter;->setPreviewSize(IIZ)V

    invoke-interface {v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->isPreviewVideoSurfaceCreated()Z

    move-result v2

    if-eqz v2, :cond_4

    iput v5, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-interface {v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getPreviewVideoSurface()Landroid/view/Surface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/telecom/InCallService$VideoCall;->setPreviewSurface(Landroid/view/Surface;)V

    :cond_4
    return-void
.end method

.method public onDetailsChanged(Lcom/android/incallui/Call;Landroid/telecom/Call$Details;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " onDetailsChanged call="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " mPrimaryCall="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {p1, v0}, Lcom/android/incallui/Call;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, " onDetailsChanged: Details not for current active call so returning. "

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->updateVideoCall(Lcom/android/incallui/Call;)V

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->updateCallCache(Lcom/android/incallui/Call;)V

    return-void
.end method

.method public onDeviceOrientationChanged(I)V
    .locals 10

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "onDeviceOrientationChanged: orientation="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", mIsVideoMode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/android/incallui/VideoCallPresenter;->mIsVideoMode:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iput p1, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-eqz v3, :cond_0

    sget-boolean v6, Lcom/android/incallui/VideoCallPresenter;->mIsVideoMode:Z

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1

    :cond_0
    const-string/jumbo v4, "onDeviceOrientationChanged: VideoCallUi is null"

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-interface {v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getPreviewSurfaceSize()Landroid/graphics/Point;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v6, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    if-nez v6, :cond_3

    :cond_2
    return-void

    :cond_3
    iget v6, p0, Lcom/android/incallui/VideoCallPresenter;->mOrientationMode:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_6

    iget v6, p0, Lcom/android/incallui/VideoCallPresenter;->mOrientationMode:I

    if-eq v6, v4, :cond_6

    const/4 v0, 0x1

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "onDeviceOrientationChanged: orientation="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "currMode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/incallui/VideoCallPresenter;->mOrientationMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    iget-object v6, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    iget v7, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    invoke-virtual {v6, v7}, Landroid/telecom/InCallService$VideoCall;->setDeviceOrientation(I)V

    iget v6, v2, Landroid/graphics/Point;->x:I

    iget v7, v2, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {v8}, Lcom/android/incallui/Call;->getState()I

    move-result v8

    const/4 v9, 0x3

    if-eq v8, v9, :cond_7

    :goto_1
    invoke-direct {p0, v6, v7, v4}, Lcom/android/incallui/VideoCallPresenter;->setPreviewSize(IIZ)V

    :cond_4
    invoke-interface {v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getDisplaySurfaceSize()Landroid/graphics/Point;

    move-result-object v1

    if-eqz v1, :cond_5

    iget v4, v1, Landroid/graphics/Point;->x:I

    if-lez v4, :cond_5

    iget v4, v1, Landroid/graphics/Point;->y:I

    if-lez v4, :cond_5

    iget v4, p0, Lcom/android/incallui/VideoCallPresenter;->mPeerOrientation:I

    iget v5, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    add-int/2addr v4, v5

    invoke-interface {v3, v4}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayRotation(I)V

    iget v4, p0, Lcom/android/incallui/VideoCallPresenter;->mPeerOrientation:I

    iget v5, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    add-int/2addr v4, v5

    iget v5, v1, Landroid/graphics/Point;->x:I

    iget v6, v1, Landroid/graphics/Point;->y:I

    invoke-direct {p0, v4, v5, v6}, Lcom/android/incallui/VideoCallPresenter;->setDisplayVideoSize(III)V

    :cond_5
    return-void

    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    :cond_7
    move v4, v5

    goto :goto_1
.end method

.method public onDialPadVisibleChanged(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->hasLiveCall()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    invoke-interface {v0, p1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDialPadPreviewPosition(Z)V

    invoke-interface {v0, p1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDialPadIncomingPosition(Z)V

    return-void
.end method

.method public onDowngradeToAudio(Lcom/android/incallui/Call;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/incallui/Call;->setSessionModificationState(I)V

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/android/incallui/VoLTEProxy;->handleSessionModifyResult(Lcom/android/incallui/Call;Z)V

    return-void
.end method

.method public onIncomingCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/incallui/VideoCallPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void
.end method

.method public onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V
    .locals 4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onStateChange oldState"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " newState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isVideoMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->exitVideoMode()V

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->cleanupSurfaces()V

    :cond_1
    const/4 v0, 0x0

    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->INCOMING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v2, :cond_5

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getActiveCall()Lcom/android/incallui/Call;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isActiveVideoCall(Lcom/android/incallui/Call;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getIncomingCall()Lcom/android/incallui/Call;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isVideoConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/incallui/VideoCallPresenter;->mIsVideoConference:Z

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-static {v2, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v1, v2, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onStateChange primaryChanged="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onStateChange primary= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onStateChange mPrimaryCall = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_8

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    if-nez v2, :cond_3

    iput-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    :cond_3
    invoke-direct {p0, v0}, Lcom/android/incallui/VideoCallPresenter;->onPrimaryCallChanged(Lcom/android/incallui/Call;)V

    :cond_4
    :goto_1
    invoke-direct {p0, v0}, Lcom/android/incallui/VideoCallPresenter;->updateCallCache(Lcom/android/incallui/Call;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/VideoCallPresenter;->updateUi(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V

    return-void

    :cond_5
    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v2, :cond_6

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v0

    goto :goto_0

    :cond_6
    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->PENDING_OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v2, :cond_7

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getPendingOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v0

    goto :goto_0

    :cond_7
    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v2, :cond_2

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getActiveCall()Lcom/android/incallui/Call;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    if-eqz v2, :cond_4

    invoke-direct {p0, v0}, Lcom/android/incallui/VideoCallPresenter;->updateVideoCall(Lcom/android/incallui/Call;)V

    goto :goto_1
.end method

.method public onSurfaceClick(I)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getInCallState()Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v0

    sget-object v1, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->isVideoMode()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->toggleFullScreen()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onSurfaceCreated(I)V
    .locals 4

    const/4 v3, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSurfaceCreated surface="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " mVideoCall="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSurfaceCreated PreviewSurfaceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSurfaceCreated presenter="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSurfaceCreated: Error bad state VideoCallUi="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " mVideoCall="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    if-ne p1, v3, :cond_4

    iget v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    if-ne v1, v3, :cond_3

    const/4 v1, 0x3

    iput v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getPreviewVideoSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/telecom/InCallService$VideoCall;->setPreviewSurface(Landroid/view/Surface;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->maybeEnableCamera()V

    goto :goto_0

    :cond_4
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-interface {v0}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->getDisplayVideoSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/telecom/InCallService$VideoCall;->setDisplaySurface(Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public onSurfaceDestroyed(I)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSurfaceDestroyed: mSurfaceId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->isChangingConfigurations()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSurfaceDestroyed: isChangingConfigurations="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/incallui/VideoCallPresenter;->enableCamera(Landroid/telecom/InCallService$VideoCall;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string/jumbo v1, "onSurfaceDestroyed: Activity is being destroyed due to configuration changes. Not closing the camera."

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSurfaceReleased(I)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onSurfaceReleased: mSurfaceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onSurfaceReleased: VideoCall is null. mSurfaceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {v0, v2}, Landroid/telecom/InCallService$VideoCall;->setDisplaySurface(Landroid/view/Surface;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {v0, v2}, Landroid/telecom/InCallService$VideoCall;->setPreviewSurface(Landroid/view/Surface;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/incallui/VideoCallPresenter;->enableCamera(Landroid/telecom/InCallService$VideoCall;Z)V

    goto :goto_0
.end method

.method public bridge synthetic onUiReady(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->onUiReady(Lcom/android/incallui/VideoCallPresenter$VideoCallUi;)V

    return-void
.end method

.method public onUiReady(Lcom/android/incallui/VideoCallPresenter$VideoCallUi;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiReady(Lcom/android/incallui/Ui;)V

    const-string/jumbo v0, "onUiReady:"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addDetailsListener(Lcom/android/incallui/InCallPresenter$InCallDetailsListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addOrientationListener(Lcom/android/incallui/InCallPresenter$InCallOrientationListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addInCallDialPadListener(Lcom/android/incallui/InCallPresenter$InCallDialPadListener;)V

    invoke-static {}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->getInstance()Lcom/android/incallui/InCallVideoCallCallbackNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->addSurfaceChangeListener(Lcom/android/incallui/InCallVideoCallCallbackNotifier$SurfaceChangeListener;)V

    invoke-static {}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->getInstance()Lcom/android/incallui/InCallVideoCallCallbackNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->addVideoEventListener(Lcom/android/incallui/InCallVideoCallCallbackNotifier$VideoEventListener;)V

    invoke-static {}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->getInstance()Lcom/android/incallui/InCallVideoCallCallbackNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->addSessionModificationListener(Lcom/android/incallui/InCallVideoCallCallbackNotifier$SessionModificationListener;)V

    iput v1, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentVideoState:I

    iput v1, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentCallState:I

    invoke-static {}, Lcom/android/incallui/InCallUiStateNotifier;->getInstance()Lcom/android/incallui/InCallUiStateNotifier;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/android/incallui/InCallUiStateNotifier;->addListener(Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;Z)V

    return-void
.end method

.method public onUiShowing(Z)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onUiShowing, showing = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " mPrimaryCall = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " mPreviewSurfaceState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isActiveVideoCall(Lcom/android/incallui/Call;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isOutgoingVideoCall(Lcom/android/incallui/Call;)Z

    move-result v0

    :goto_0
    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_0
    const-string/jumbo v0, "onUiShowing, received for non-active or outgoing video call"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->maybeEnableCamera()V

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPreviewSurfaceState:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mVideoCall:Landroid/telecom/InCallService$VideoCall;

    invoke-virtual {p0, v0, v2}, Lcom/android/incallui/VideoCallPresenter;->enableCamera(Landroid/telecom/InCallService$VideoCall;Z)V

    goto :goto_1
.end method

.method public bridge synthetic onUiUnready(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/VideoCallPresenter;->onUiUnready(Lcom/android/incallui/VideoCallPresenter$VideoCallUi;)V

    return-void
.end method

.method public onUiUnready(Lcom/android/incallui/VideoCallPresenter$VideoCallUi;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiUnready(Lcom/android/incallui/Ui;)V

    const-string/jumbo v0, "onUiUnready:"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeDetailsListener(Lcom/android/incallui/InCallPresenter$InCallDetailsListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeOrientationListener(Lcom/android/incallui/InCallPresenter$InCallOrientationListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeInCallDialPadListener(Lcom/android/incallui/InCallPresenter$InCallDialPadListener;)Z

    invoke-static {}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->getInstance()Lcom/android/incallui/InCallVideoCallCallbackNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->removeSurfaceChangeListener(Lcom/android/incallui/InCallVideoCallCallbackNotifier$SurfaceChangeListener;)V

    invoke-static {}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->getInstance()Lcom/android/incallui/InCallVideoCallCallbackNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->removeVideoEventListener(Lcom/android/incallui/InCallVideoCallCallbackNotifier$VideoEventListener;)V

    invoke-static {}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->getInstance()Lcom/android/incallui/InCallVideoCallCallbackNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallVideoCallCallbackNotifier;->removeSessionModificationListener(Lcom/android/incallui/InCallVideoCallCallbackNotifier$SessionModificationListener;)V

    invoke-static {}, Lcom/android/incallui/InCallUiStateNotifier;->getInstance()Lcom/android/incallui/InCallUiStateNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallUiStateNotifier;->removeListener(Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;)V

    return-void
.end method

.method public onUpdatePeerDimensions(Lcom/android/incallui/Call;II)V
    .locals 5

    const/16 v4, 0x2710

    const/4 v3, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onUpdatePeerDimensions: width= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " height= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const-string/jumbo v1, "VideoCallUi is null or call is null. Bail out"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {p1, v1}, Lcom/android/incallui/Call;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "Current call is not equal to primary call. Bail out"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    invoke-static {v1}, Lcom/android/incallui/Call$State;->isMiuiDialing(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v1

    invoke-static {v1}, Lcom/android/incallui/CallAdapterUtils;->isBidirectional(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1, v3, v3}, Lcom/android/incallui/Call;->setPlayingVideoCrbt(ZZ)V

    return-void

    :cond_3
    invoke-virtual {p1}, Lcom/android/incallui/Call;->isPlayingVideoCrbt()Z

    move-result v1

    if-eqz v1, :cond_4

    return-void

    :cond_4
    if-lez p2, :cond_5

    if-lez p3, :cond_5

    if-ge p2, v4, :cond_5

    if-ge p3, v4, :cond_5

    invoke-interface {v0, p2, p3}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplaySurfaceSize(II)V

    iget v1, p0, Lcom/android/incallui/VideoCallPresenter;->mDeviceOrientation:I

    invoke-direct {p0, v1, p2, p3}, Lcom/android/incallui/VideoCallPresenter;->setDisplayVideoSize(III)V

    :cond_5
    return-void
.end method

.method public onUpgradeToVideoFail(ILcom/android/incallui/Call;)V
    .locals 6

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onUpgradeToVideoFail status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", call="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-static {v1, p2}, Lcom/android/incallui/Call;->areSame(Lcom/android/incallui/Call;Lcom/android/incallui/Call;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    const-string/jumbo v1, "UpgradeToVideoFail received for non-primary call"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    if-nez p2, :cond_2

    return-void

    :cond_2
    const-string/jumbo v1, "onUpgradeToVideoFail VoLTEProxy.SESSION_MODIFY_REQUEST_REJECTED_BY_REMOTE=5"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    invoke-virtual {v1, v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->removeMessages(I)V

    if-ne p1, v4, :cond_3

    invoke-virtual {p2, v4}, Lcom/android/incallui/Call;->setSessionModificationState(I)V

    :goto_0
    invoke-static {p2, v3}, Lcom/android/incallui/VoLTEProxy;->handleSessionModifyResult(Lcom/android/incallui/Call;Z)V

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    invoke-virtual {v1, v3, p2}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->dismissVideoUpgradeDialog()V

    return-void

    :cond_3
    if-ne p1, v5, :cond_4

    invoke-virtual {p2, v5}, Lcom/android/incallui/Call;->setSessionModificationState(I)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x2

    invoke-virtual {p2, v1}, Lcom/android/incallui/Call;->setSessionModificationState(I)V

    goto :goto_0
.end method

.method public onUpgradeToVideoRequest(Lcom/android/incallui/Call;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onUpgradeToVideoRequest call = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " new video state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-static {v0, p1}, Lcom/android/incallui/Call;->areSame(Lcom/android/incallui/Call;Lcom/android/incallui/Call;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "UpgradeToVideoRequest received for non-primary call"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    if-nez p1, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mSessionModificationResetHandler:Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallHandler;->removeMessages(I)V

    invoke-virtual {p1, p2}, Lcom/android/incallui/Call;->setSessionModificationTo(I)V

    return-void
.end method

.method public onUpgradeToVideoSuccess(Lcom/android/incallui/Call;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onUpgradeToVideoSuccess call="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-static {v0, p1}, Lcom/android/incallui/Call;->areSame(Lcom/android/incallui/Call;Lcom/android/incallui/Call;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "UpgradeToVideoSuccess received for non-primary call"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    if-nez p1, :cond_2

    return-void

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/incallui/Call;->setSessionModificationState(I)V

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/android/incallui/VoLTEProxy;->handleSessionModifyResult(Lcom/android/incallui/Call;Z)V

    return-void
.end method

.method public onVideoQualityChanged(Lcom/android/incallui/Call;I)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter;->mPrimaryCall:Lcom/android/incallui/Call;

    invoke-virtual {p1, v1}, Lcom/android/incallui/Call;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-nez v0, :cond_1

    const-string/jumbo v1, "Error VideoCallUi is null. Return."

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    return-void
.end method

.method public resetAutoFullScreen()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/incallui/VideoCallPresenter;->mIsFullScreen:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/incallui/VideoCallPresenter;->mCurrentCallState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/VideoCallPresenter;->autoFullScreen()V

    :cond_0
    return-void
.end method

.method public setForceProcessIncoming(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/VideoCallPresenter;->mForceProcessIncoming:Z

    return-void
.end method
