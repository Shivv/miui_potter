.class final Lcom/android/incallui/Log$1;
.super Ljava/lang/Object;
.source "Log.java"

# interfaces
.implements Lmiui/telephony/PhoneDebug$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/Log;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDebugChanged()V
    .locals 3

    const/4 v1, 0x1

    const-string/jumbo v0, "InCall"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lmiui/telephony/PhoneDebug;->VDBG:Z

    :goto_0
    sput-boolean v0, Lcom/android/incallui/Log;->DEBUG:Z

    const-string/jumbo v0, "InCall"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v1, Lmiui/telephony/PhoneDebug;->VDBG:Z

    :cond_0
    sput-boolean v1, Lcom/android/incallui/Log;->VERBOSE:Z

    sget-boolean v0, Lcom/android/incallui/Log;->VERBOSE:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "InCall"

    const-string/jumbo v1, "onDebugChanged"

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method
