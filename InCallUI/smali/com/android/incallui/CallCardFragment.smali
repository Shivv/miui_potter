.class public Lcom/android/incallui/CallCardFragment;
.super Lcom/android/incallui/BaseFragment;
.source "CallCardFragment.java"

# interfaces
.implements Lcom/android/incallui/CallCardPresenter$CallCardUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/CallCardFragment$AutoTextViewChangeListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/CallCardPresenter;",
        "Lcom/android/incallui/CallCardPresenter$CallCardUi;",
        ">;",
        "Lcom/android/incallui/CallCardPresenter$CallCardUi;"
    }
.end annotation


# instance fields
.field private mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

.field private mAvatar:Landroid/widget/ImageView;

.field private mAvatarLayout:Landroid/view/View;

.field private mAvatarTransAnim:Landroid/animation/ObjectAnimator;

.field private mAvatarTranslateY:I

.field private mCallCardConferenceManageButton:Landroid/widget/ImageView;

.field private mCallCardOptionalInfo:Lcom/android/incallui/view/CallCardOptionalInfoView;

.field private mCallCardStateEllipsisView:Lcom/android/incallui/view/EllipsisTextView;

.field private mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

.field private mCallInfoMarginTop:I

.field private mCallerInfoItemHeight:I

.field private mCallerInfoTranslateY:I

.field private mCallerInfoTranslateYDialpadPressed:I

.field private mCallerInfoTranslateYIncoming:I

.field private mContext:Landroid/content/Context;

.field private mCrbtCallInfoDialpadMarginTop:I

.field private mCrbtCallInfoToolsGlobalMarginTop:I

.field private mCrbtCallInfoToolsMarginTop:I

.field private mCrbtPrompt:Landroid/widget/TextView;

.field private mDefaultFrontGroundResId:I

.field private mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

.field private mFrontGround:Landroid/widget/ImageView;

.field private mIsAnswering:Z

.field private mIsRejecting:Z

.field private mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

.field private mLastSmallAvatarKey:Ljava/lang/String;

.field private mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

.field private mPostDialTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

.field private mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

.field private mPrimaryCallBanner:Landroid/widget/LinearLayout;

.field private mPrimaryName:Lcom/android/incallui/view/AutoTextView;

.field private mPrimaryNameColor:I

.field private mPrimaryNameColorNormal:I

.field private mPrimaryNameColorSmall:I

.field private mPrimaryNameSize:I

.field private mPrimaryNameSizeBig:I

.field private mPrimaryNameSizeNormal:I

.field private mPrimaryNameSizeSmall:I

.field private mRootView:Landroid/view/ViewGroup;

.field private mSingleCallInfo:Landroid/view/View;

.field private mSingleCallLayout:Landroid/widget/LinearLayout;

.field private mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

.field private mSpeakerOnMask:Landroid/view/View;

.field private mSuspectView:Landroid/view/View;

.field private mUserLockedPrompt:Landroid/widget/TextView;

.field private mVerifyimg:Landroid/widget/ImageView;

.field private mVideoCallFragment:Lcom/android/incallui/VideoCallFragment;

.field private mVideoCallStateLabel:Landroid/widget/TextView;

.field private mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/CallCardFragment;)Lcom/android/incallui/view/AnimationCircleLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/CallCardFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/incallui/CallCardFragment;)Lcom/android/incallui/view/EllipsisTextView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateEllipsisView:Lcom/android/incallui/view/EllipsisTextView;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/incallui/CallCardFragment;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/incallui/CallCardFragment;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/incallui/CallCardFragment;)Lcom/android/incallui/view/AutoTextView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/incallui/CallCardFragment;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVerifyimg:Landroid/widget/ImageView;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    const v0, 0x7f020095

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mDefaultFrontGroundResId:I

    iput v1, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoTranslateY:I

    iput v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSize:I

    iput v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColor:I

    iput v1, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoItemHeight:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLastSmallAvatarKey:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTranslateY:I

    iput-boolean v1, p0, Lcom/android/incallui/CallCardFragment;->mIsAnswering:Z

    iput-boolean v1, p0, Lcom/android/incallui/CallCardFragment;->mIsRejecting:Z

    return-void
.end method

.method private dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p2, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private initPrimary()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-virtual {v0}, Lcom/android/incallui/view/AutoTextView;->getMaxTextSize()F

    move-result v0

    iget v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSize:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    iget v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/AutoTextView;->resizeMaxTextSize(F)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    iget v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColor:I

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/AutoTextView;->setTextColor(I)V

    :cond_0
    return-void
.end method

.method private isSpeakerOn()Z
    .locals 3

    invoke-static {}, Lcom/android/incallui/AudioModeProvider;->getInstance()Lcom/android/incallui/AudioModeProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/AudioModeProvider;->getAudioMode()I

    move-result v0

    and-int/lit8 v1, v0, 0x8

    const/16 v2, 0x8

    if-ne v2, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private resizePrimary()V
    .locals 1

    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSizeBig:I

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSize:I

    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColorNormal:I

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColor:I

    invoke-direct {p0}, Lcom/android/incallui/CallCardFragment;->initPrimary()V

    return-void
.end method

.method private setMultiSimIndicator(IZ)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-virtual {v0, p1, v1}, Lcom/android/incallui/view/LabelAndNumberView;->setMultiSimIndicator(IZ)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v0, p1, v2}, Lcom/android/incallui/view/CallCardStateInfoView;->setConferenceMultiSimIndicator(IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-virtual {v0, p1, v2}, Lcom/android/incallui/view/LabelAndNumberView;->setMultiSimIndicator(IZ)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v0, p1, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->setConferenceMultiSimIndicator(IZ)V

    goto :goto_0
.end method

.method private setPrimaryMode(Z)V
    .locals 5

    const/4 v4, -0x2

    if-eqz p1, :cond_0

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    :goto_0
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-virtual {v2, v0}, Lcom/android/incallui/view/AutoTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallInfo:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    sub-int v2, v1, v2

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallInfo:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {v0, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto :goto_0
.end method

.method private setSingleCallStateEllipsis(I)V
    .locals 3

    const/16 v0, 0x8

    const/16 v1, 0x9

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getDisconnectingListener()Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    new-instance v1, Lcom/android/incallui/CallCardFragment$6;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardFragment$6;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->addDisconnectingListener(Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateEllipsisView:Lcom/android/incallui/view/EllipsisTextView;

    iget-boolean v2, p0, Lcom/android/incallui/CallCardFragment;->mIsAnswering:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/incallui/CallCardFragment;->mIsRejecting:Z

    if-eqz v2, :cond_2

    :cond_1
    const/4 v0, 0x0

    :cond_2
    invoke-virtual {v1, v0}, Lcom/android/incallui/view/EllipsisTextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateEllipsisView:Lcom/android/incallui/view/EllipsisTextView;

    invoke-virtual {v1, v0}, Lcom/android/incallui/view/EllipsisTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private showHDAudioIndicator(ZI)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/LabelAndNumberView;->showHDAudioIndicator(ZI)V

    return-void
.end method

.method private updatePhoneNumberField(Lcom/android/incallui/model/CallCardInfo;)V
    .locals 6

    iget-boolean v2, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->fullDialString:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/incallui/util/Utils;->isSameNumber(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/incallui/util/Utils;->isSameNumber(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p1, Lcom/android/incallui/model/CallCardInfo;->isMtImsConference:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/incallui/CallCardFragment;->setSinglePhoneNumber(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->fullDialString:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/incallui/CallCardFragment;->setSinglePhoneNumber(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->leftPostDialString:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->leftPostDialString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPostDialTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p1, Lcom/android/incallui/model/CallCardInfo;->leftPostDialString:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-virtual {v2, v1}, Lcom/android/incallui/view/LabelAndNumberView;->showPhoneNumber(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private updatePrimaryBannerAnimatorSet()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    return-void
.end method


# virtual methods
.method public blurCallCardOnIncomingMessageShow(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public controlBigAvatar(ZZ)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->resetLayout()V

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0, v6, v6}, Lcom/android/incallui/CallCardFragment;->showSpeakerOnOrIncomingMask(ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-static {}, Lcom/android/incallui/util/Utils;->getSmallAvatarRect()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->resetLayout()V

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->getCircleAnimationBuilder()Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetX(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetY(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetR(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setNoNeedTranslateY()Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v3

    new-instance v4, Lcom/android/incallui/CallCardFragment$9;

    invoke-direct {v4, p0}, Lcom/android/incallui/CallCardFragment$9;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v3, v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOnAnimationListener(Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->configAnimationParam()V

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->startCircleAnimation()V

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v4, v3, v5

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v6

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/android/incallui/CallCardFragment$10;

    invoke-direct {v3, p0}, Lcom/android/incallui/CallCardFragment$10;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/CallCardPresenter;

    invoke-virtual {v3, v6}, Lcom/android/incallui/CallCardPresenter;->hideBanner(Z)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->isAnimationRuning()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/CallCardFragment;->isSpeakerOn()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0, v5, v5}, Lcom/android/incallui/CallCardFragment;->showSpeakerOnOrIncomingMask(ZZ)V

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->resetLayout()V

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    iget v4, p0, Lcom/android/incallui/CallCardFragment;->mDefaultFrontGroundResId:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method

.method public controlPrimaryCallBannerTopMargin(ZZZZ)V
    .locals 3

    if-eqz p2, :cond_1

    iget v2, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoTranslateYDialpadPressed:I

    :goto_0
    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoTranslateY:I

    if-eqz p1, :cond_2

    iget v2, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoTranslateY:I

    neg-int v1, v2

    :goto_1
    if-eqz p3, :cond_6

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/CallCardPresenter;

    invoke-virtual {v2}, Lcom/android/incallui/CallCardPresenter;->isDialPadVisible()Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/android/incallui/CallCardFragment;->mCrbtCallInfoDialpadMarginTop:I

    add-int/2addr v1, v2

    :goto_2
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v1, v2, :cond_0

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_0
    return-void

    :cond_1
    iget v2, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoTranslateYIncoming:I

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/android/incallui/util/Utils;->isInternationalBuild()Z

    move-result v2

    if-nez v2, :cond_4

    xor-int/lit8 v2, p4, 0x1

    if-eqz v2, :cond_5

    :cond_4
    iget v2, p0, Lcom/android/incallui/CallCardFragment;->mCrbtCallInfoToolsGlobalMarginTop:I

    add-int/2addr v1, v2

    goto :goto_2

    :cond_5
    iget v2, p0, Lcom/android/incallui/CallCardFragment;->mCrbtCallInfoToolsMarginTop:I

    add-int/2addr v1, v2

    goto :goto_2

    :cond_6
    iget v2, p0, Lcom/android/incallui/CallCardFragment;->mCallInfoMarginTop:I

    add-int/2addr v1, v2

    goto :goto_2
.end method

.method public controlPrimaryName(ZZZ)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSizeBig:I

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSize:I

    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColorNormal:I

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColor:I

    :goto_0
    invoke-direct {p0}, Lcom/android/incallui/CallCardFragment;->initPrimary()V

    return-void

    :cond_0
    if-nez p2, :cond_1

    if-eqz p3, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallCardPresenter;->isDialPadVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSizeSmall:I

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSize:I

    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColorSmall:I

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColor:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSizeNormal:I

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSize:I

    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColorNormal:I

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColor:I

    goto :goto_0
.end method

.method public controlSingleCallLayoutTopMargin()V
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/CallCardPresenter;

    invoke-virtual {v2}, Lcom/android/incallui/CallCardPresenter;->getCallCount()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/incallui/CallCardFragment;->getSingleCallerInfoTopSpace(I)I

    move-result v1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v2, v1, :cond_1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_1
    return-void
.end method

.method public controlSmallAvatarTranslateY()V
    .locals 2

    invoke-static {}, Lcom/android/incallui/util/Utils;->getSmallAvatarRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    neg-int v0, v0

    iput v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTranslateY:I

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-static {}, Lcom/android/incallui/util/Utils;->getSmallAvatarRect()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method protected createPresenter()Lcom/android/incallui/CallCardPresenter;
    .locals 1

    new-instance v0, Lcom/android/incallui/CallCardPresenter;

    invoke-direct {v0}, Lcom/android/incallui/CallCardPresenter;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->createPresenter()Lcom/android/incallui/CallCardPresenter;

    move-result-object v0

    return-object v0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/CallCardFragment;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-virtual {v0}, Lcom/android/incallui/view/LabelAndNumberView;->getPhoneNumber()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/CallCardFragment;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v0}, Lcom/android/incallui/view/CallCardStateInfoView;->getCallStateLabel()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/CallCardFragment;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/CallCardFragment;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-virtual {v0}, Lcom/android/incallui/view/LabelAndNumberView;->getPhoneNumber()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/CallCardFragment;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V

    :cond_3
    return-void
.end method

.method public getSingleCallInfoHeight()F
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallInfo:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallInfo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_0
    int-to-float v0, v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSingleCallerInfoTopSpace(I)I
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoItemHeight:I

    add-int/lit8 v1, p1, -0x1

    mul-int/2addr v0, v1

    return v0
.end method

.method protected getUi()Lcom/android/incallui/CallCardPresenter$CallCardUi;
    .locals 0

    return-object p0
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getUi()Lcom/android/incallui/CallCardPresenter$CallCardUi;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCallFragment()Lcom/android/incallui/VideoCallFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoCallFragment:Lcom/android/incallui/VideoCallFragment;

    return-object v0
.end method

.method public hideBanner(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/android/incallui/CallCardFragment;->updatePrimaryBannerAnimatorSet()V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public hideSmallAvatar(Z)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    neg-int v1, v1

    iput v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTranslateY:I

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v4, v3, v5

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v1}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lcom/android/incallui/CallCardFragment$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardFragment$2;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mVerifyimg:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public initSingleVideoCallInfoRotation()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->initRotation()V

    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getFirstCall()Lcom/android/incallui/Call;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/CallCardPresenter;

    invoke-virtual {v2, v0}, Lcom/android/incallui/CallCardPresenter;->init(Lcom/android/incallui/Call;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const v2, 0x7f030008

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoItemHeight:I

    const v2, 0x7f090006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoTranslateYIncoming:I

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mCallerInfoTranslateYDialpadPressed:I

    const v2, 0x7f09000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSizeSmall:I

    const v2, 0x7f090009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSizeNormal:I

    const v2, 0x7f09000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameSizeBig:I

    const v2, 0x7f060001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColorNormal:I

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryNameColorSmall:I

    const v2, 0x7f090008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mCallInfoMarginTop:I

    const v2, 0x7f090023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mCrbtCallInfoToolsMarginTop:I

    const v2, 0x7f090024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mCrbtCallInfoToolsGlobalMarginTop:I

    const v2, 0x7f090025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mCrbtCallInfoDialpadMarginTop:I

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    return-object v2
.end method

.method public onDestroyView()V
    .locals 4

    const/16 v1, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSpeakerOnMask:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatar:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatar:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mLastSmallAvatarKey:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVerifyimg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-virtual {v0}, Lcom/android/incallui/view/LabelAndNumberView;->clearAllView()V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardOptionalInfo:Lcom/android/incallui/view/CallCardOptionalInfoView;

    invoke-virtual {v0}, Lcom/android/incallui/view/CallCardOptionalInfoView;->clearAllView()V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v0}, Lcom/android/incallui/view/CallCardStateInfoView;->clearAllView()V

    invoke-virtual {p0, v3, v3}, Lcom/android/incallui/CallCardFragment;->showSpeakerOnOrIncomingMask(ZZ)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    const v4, 0x7f0a003f

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/incallui/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a003b

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallFragment;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoCallFragment:Lcom/android/incallui/VideoCallFragment;

    const v0, 0x7f0a0038

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/AnimationCircleLayout;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    const-string/jumbo v1, "AVATAR"

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/AnimationCircleLayout;->setTagForLog(Ljava/lang/String;)V

    const v0, 0x7f0a0039

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    const v0, 0x7f0a003a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSpeakerOnMask:Landroid/view/View;

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0d0012

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    const v0, 0x7f0a003c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    const v0, 0x7f0a003d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatar:Landroid/widget/ImageView;

    const v0, 0x7f0a003e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVerifyimg:Landroid/widget/ImageView;

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallInfo:Landroid/view/View;

    const v0, 0x7f0a009b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/LabelAndNumberView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    const v0, 0x7f0a0005

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/AutoTextView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    new-instance v1, Lcom/android/incallui/CallCardFragment$AutoTextViewChangeListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/incallui/CallCardFragment$AutoTextViewChangeListener;-><init>(Lcom/android/incallui/CallCardFragment;Lcom/android/incallui/CallCardFragment$AutoTextViewChangeListener;)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-virtual {v0, v1, v2}, Lcom/android/incallui/view/AutoTextView;->setChangeLayoutListener(Lcom/android/incallui/view/AutoTextView$ChangeLayoutListener;Landroid/view/View;)V

    const v0, 0x7f0a009f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoCallStateLabel:Landroid/widget/TextView;

    const v0, 0x7f0a00a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mUserLockedPrompt:Landroid/widget/TextView;

    const v0, 0x7f0a00a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCrbtPrompt:Landroid/widget/TextView;

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0a009a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    const v0, 0x7f0a00a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/incallui/CallCardFragment$1;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardFragment$1;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a009c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CallCardOptionalInfoView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardOptionalInfo:Lcom/android/incallui/view/CallCardOptionalInfoView;

    const v0, 0x7f0a009d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CallCardStateInfoView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    const v0, 0x7f0a009e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/EllipsisTextView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateEllipsisView:Lcom/android/incallui/view/EllipsisTextView;

    invoke-virtual {p0, v3, v3}, Lcom/android/incallui/CallCardFragment;->showConferenceControl(ZI)V

    new-instance v6, Landroid/text/style/TextAppearanceSpan;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f0d0011

    invoke-direct {v6, v0, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    new-instance v7, Landroid/text/style/TextAppearanceSpan;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f0d0014

    invoke-direct {v7, v0, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {v7}, Landroid/text/style/TextAppearanceSpan;->getFamily()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Landroid/text/style/TextAppearanceSpan;->getTextStyle()I

    move-result v2

    invoke-virtual {v7}, Landroid/text/style/TextAppearanceSpan;->getTextSize()I

    move-result v3

    invoke-virtual {v6}, Landroid/text/style/TextAppearanceSpan;->getTextColor()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v6}, Landroid/text/style/TextAppearanceSpan;->getLinkTextColor()Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPostDialTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-direct {p0}, Lcom/android/incallui/CallCardFragment;->resizePrimary()V

    return-void
.end method

.method public playAudioButtonClickAnim(ZII)V
    .locals 11

    const/16 v10, 0x15e

    const/4 v9, 0x0

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout;->getHeight()I

    move-result v1

    mul-int v4, v3, v3

    mul-int v5, v1, v1

    add-int/2addr v4, v5

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    double-to-float v2, v4

    if-eqz p1, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0, v4, v9}, Lcom/android/incallui/CallCardFragment;->showSpeakerOnOrIncomingMask(ZZ)V

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout;->resetLayout()V

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout;->getCircleAnimationBuilder()Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v0

    int-to-float v4, p2

    invoke-virtual {v0, v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOriginX(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    int-to-float v5, p3

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOriginY(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOriginR(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    div-int/lit8 v5, v3, 0x2

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetX(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    div-int/lit8 v5, v1, 0x2

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetY(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetR(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setNoNeedTranslateY()Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    new-instance v5, Lcom/android/incallui/CallCardFragment$11;

    invoke-direct {v5, p0}, Lcom/android/incallui/CallCardFragment$11;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOnAnimationListener(Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setCircleChangeDuration(I)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->configAnimationParam()V

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/incallui/CallCardFragment;->mDefaultFrontGroundResId:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout;->startCircleAnimation()V

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout;->resetLayout()V

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout;->getCircleAnimationBuilder()Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v0

    div-int/lit8 v4, v3, 0x2

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOriginX(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    div-int/lit8 v5, v1, 0x2

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOriginY(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOriginR(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    int-to-float v5, p2

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetX(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    int-to-float v5, p3

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetY(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setTargetR(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setNoNeedTranslateY()Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    new-instance v5, Lcom/android/incallui/CallCardFragment$12;

    invoke-direct {v5, p0}, Lcom/android/incallui/CallCardFragment$12;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setOnAnimationListener(Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->setCircleChangeDuration(I)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->configAnimationParam()V

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mAnimationCircleLayout:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v4}, Lcom/android/incallui/view/AnimationCircleLayout;->startCircleAnimation()V

    goto :goto_0
.end method

.method public setAnswering(Z)V
    .locals 4

    const/4 v3, 0x4

    iput-boolean p1, p0, Lcom/android/incallui/CallCardFragment;->mIsAnswering:Z

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    iget-boolean v0, p0, Lcom/android/incallui/CallCardFragment;->mIsAnswering:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0040

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0, v3}, Lcom/android/incallui/view/CallCardStateInfoView;->setCallCardStateLabel(Ljava/lang/String;I)V

    invoke-direct {p0, v3}, Lcom/android/incallui/CallCardFragment;->setSingleCallStateEllipsis(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0041

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setCallCardImage(Landroid/graphics/drawable/Drawable;ZZ)V
    .locals 3

    const v2, 0x7f0200a2

    move-object v0, p1

    if-eqz p2, :cond_0

    iput v2, p0, Lcom/android/incallui/CallCardFragment;->mDefaultFrontGroundResId:I

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/incallui/InCallPresenter;->setCallBackgroundDrawableResource(I)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void

    :cond_0
    const v1, 0x7f020095

    iput v1, p0, Lcom/android/incallui/CallCardFragment;->mDefaultFrontGroundResId:I

    if-eqz p3, :cond_1

    if-eqz p1, :cond_2

    const-string/jumbo v1, "task_load_image"

    invoke-static {v1}, Lcom/android/incallui/util/SimpleTask;->destroyTask(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mFrontGround:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v1, Lcom/android/incallui/CallCardFragment$3;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardFragment$3;-><init>(Lcom/android/incallui/CallCardFragment;)V

    const-string/jumbo v2, "task_load_image"

    invoke-virtual {v1, v2}, Lcom/android/incallui/CallCardFragment$3;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/util/SimpleTask;->run()V

    goto :goto_0
.end method

.method public setDialPadVisible(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/DoubleCallInfoView;->setDialPadVisible(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setDialPadVisible(Z)V

    :cond_1
    return-void
.end method

.method public setDoubleCallInfoVisible(Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    const v2, 0x7f030010

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/DoubleCallInfoView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    new-instance v1, Lcom/android/incallui/CallCardFragment$4;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardFragment$4;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/DoubleCallInfoView;->setOnDoubleCallInfoOperationListener(Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, v3}, Lcom/android/incallui/view/DoubleCallInfoView;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/DoubleCallInfoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    goto :goto_0
.end method

.method public setDoublePrimary(Lcom/android/incallui/model/CallCardInfo;ZZZZI)V
    .locals 7

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoublePrimary(Lcom/android/incallui/model/CallCardInfo;ZZZZI)V

    :cond_0
    return-void
.end method

.method public setDoublePrimaryCallElapsedTime(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoublePrimaryCallElapsedTime(ZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setDoublePrimaryCallInfoVisible(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoublePrimaryCallInfoVisible(Z)V

    :cond_0
    return-void
.end method

.method public setDoublePrimaryCallState(ILandroid/telecom/DisconnectCause;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoublePrimaryCallState(ILandroid/telecom/DisconnectCause;Z)V

    :cond_0
    return-void
.end method

.method public setDoublePrimaryName(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoublePrimaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setDoubleSecondary(Lcom/android/incallui/model/CallCardInfo;ZZZZI)V
    .locals 7

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoubleSecondary(Lcom/android/incallui/model/CallCardInfo;ZZZZI)V

    :cond_0
    return-void
.end method

.method public setDoubleSecondaryCallElapsedTime(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoubleSecondaryCallElapsedTime(ZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setDoubleSecondaryCallInfoVisible(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoubleSecondaryCallInfoVisible(Z)V

    :cond_0
    return-void
.end method

.method public setDoubleSecondaryCallState(ILandroid/telecom/DisconnectCause;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoubleSecondaryCallState(ILandroid/telecom/DisconnectCause;Z)V

    :cond_0
    return-void
.end method

.method public setDoubleSecondaryName(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mDoubleCallInfo:Lcom/android/incallui/view/DoubleCallInfoView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoubleSecondaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setRejecting(Z)V
    .locals 4

    const/4 v3, 0x4

    iput-boolean p1, p0, Lcom/android/incallui/CallCardFragment;->mIsRejecting:Z

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    iget-boolean v0, p0, Lcom/android/incallui/CallCardFragment;->mIsRejecting:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0044

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0, v3}, Lcom/android/incallui/view/CallCardStateInfoView;->setCallCardStateLabel(Ljava/lang/String;I)V

    invoke-direct {p0, v3}, Lcom/android/incallui/CallCardFragment;->setSingleCallStateEllipsis(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0045

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setSingleCallElapsedTime(ZLjava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/android/incallui/view/CallCardStateInfoView;->setCallElapsedTime(ZLjava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setCallElapsedTime(ZLjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/android/incallui/view/CallCardStateInfoView;->setCallElapsedTime(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public setSingleCallInfo(Lcom/android/incallui/model/CallCardInfo;ZZIZZ)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    if-eqz p6, :cond_5

    iput-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isMtImsConference:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0b00a5

    :goto_0
    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    aput-object v4, v2, v1

    invoke-virtual {v3, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0, v1, v1}, Lcom/android/incallui/CallCardFragment;->showConferenceControl(ZI)V

    :cond_0
    :goto_2
    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-boolean v2, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iget-object v4, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    move-object v0, p0

    move v3, p5

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/incallui/CallCardFragment;->setSingleCallName(Ljava/lang/String;ZZLjava/lang/String;Z)V

    invoke-direct {p0, p1}, Lcom/android/incallui/CallCardFragment;->updatePhoneNumberField(Lcom/android/incallui/model/CallCardInfo;)V

    invoke-virtual {p0, p1}, Lcom/android/incallui/CallCardFragment;->setYellowPageInfo(Lcom/android/incallui/model/CallCardInfo;)V

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isSpeechCodecHD:Z

    iget v1, p1, Lcom/android/incallui/model/CallCardInfo;->speechHDResId:I

    invoke-direct {p0, v0, v1}, Lcom/android/incallui/CallCardFragment;->showHDAudioIndicator(ZI)V

    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/incallui/CallCardFragment;->setSingleTelocation(Ljava/lang/String;)V

    return-void

    :cond_1
    const v0, 0x7f0b00a4

    goto :goto_0

    :cond_2
    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v0}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iput-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->leftPostDialString:Ljava/lang/String;

    iput-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    iput-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v0}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    if-lez p4, :cond_4

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_3
    invoke-virtual {p0, v0, p4}, Lcom/android/incallui/CallCardFragment;->showConferenceControl(ZI)V

    iget v0, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    invoke-direct {p0, v0, v2}, Lcom/android/incallui/CallCardFragment;->setMultiSimIndicator(IZ)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    if-nez p2, :cond_6

    if-eqz p3, :cond_8

    :cond_6
    invoke-static {p2, p3, v1}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iput-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    if-nez p3, :cond_0

    if-lez p4, :cond_7

    move v1, v2

    :cond_7
    invoke-virtual {p0, v1, p4}, Lcom/android/incallui/CallCardFragment;->showConferenceControl(ZI)V

    iget v0, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    invoke-direct {p0, v0, v2}, Lcom/android/incallui/CallCardFragment;->setMultiSimIndicator(IZ)V

    goto :goto_2

    :cond_8
    invoke-virtual {p0, v1, v1}, Lcom/android/incallui/CallCardFragment;->showConferenceControl(ZI)V

    iget v0, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    invoke-direct {p0, v0, v1}, Lcom/android/incallui/CallCardFragment;->setMultiSimIndicator(IZ)V

    goto :goto_2
.end method

.method public setSingleCallInfoVisible(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleCallLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSingleCallName(Ljava/lang/String;ZZLjava/lang/String;Z)V
    .locals 6

    invoke-direct {p0, p5}, Lcom/android/incallui/CallCardFragment;->setPrimaryMode(Z)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Lcom/android/incallui/view/AutoTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/incallui/view/AutoTextView;->setResizeSwitch(Z)V

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/CallCardPresenter;

    invoke-virtual {v2}, Lcom/android/incallui/CallCardPresenter;->isDialPadVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Lcom/android/incallui/view/AutoTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-static {p1, p2}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/incallui/view/AutoTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Lcom/android/incallui/view/AutoTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {p1, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/incallui/view/AutoTextView;->setResizeSwitch(Z)V

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-virtual {v2, v1}, Lcom/android/incallui/view/AutoTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 v0, 0x0

    if-eqz p2, :cond_2

    const/4 v0, 0x3

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-virtual {v2, v0}, Lcom/android/incallui/view/AutoTextView;->setTextDirection(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryName:Lcom/android/incallui/view/AutoTextView;

    invoke-static {p1, p2}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/incallui/view/AutoTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setSingleCallState(IIIZLandroid/telecom/DisconnectCause;I)V
    .locals 6

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setSingleCallState: state="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ",videoState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ",sessionModificationState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ",isConference="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p5, v3, p6}, Lcom/android/incallui/CallUtils;->getCallStateLabelFromState(ILandroid/telecom/DisconnectCause;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, p3, p4}, Lcom/android/incallui/CallUtils;->getVideoCallStateLabelFromState(IIIZ)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setSingleCallState: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "DisconnectCause: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/incallui/CallCardFragment;->mVideoCallStateLabel:Landroid/widget/TextView;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v3, 0x8

    :cond_0
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mVideoCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v3, v0, p1}, Lcom/android/incallui/view/CallCardStateInfoView;->setCallCardStateLabel(Ljava/lang/String;I)V

    invoke-direct {p0, p1}, Lcom/android/incallui/CallCardFragment;->setSingleCallStateEllipsis(I)V

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v3, :cond_1

    invoke-static {p1, p2, p3}, Lcom/android/incallui/CallUtils;->getVideoCallSessionLabel(III)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v3, v2}, Lcom/android/incallui/view/SingleVideoCallInfoView;->showVideoCallSessionLabel(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/InCallPresenter;->forceToggleFullScreen()V

    :cond_1
    return-void
.end method

.method public setSinglePhoneNumber(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-static {p1}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->showPhoneNumber(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSingleTelocation(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/LabelAndNumberView;->showTelocation(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSmallAvatar(Landroid/graphics/drawable/Drawable;Z)V
    .locals 5

    const/4 v4, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mLastSmallAvatarKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iput-object v1, p0, Lcom/android/incallui/CallCardFragment;->mLastSmallAvatarKey:Ljava/lang/String;

    if-eqz p1, :cond_2

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/android/incallui/ImageUtils;->getCircleAvatarBitmap(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mAvatar:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mAvatar:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz p2, :cond_3

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mVerifyimg:Landroid/widget/ImageView;

    const v3, 0x7f02009e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mVerifyimg:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mAvatar:Landroid/widget/ImageView;

    const v3, 0x7f020027

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mVerifyimg:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setVideoCallElapsedTime(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setCallElapsedTime(ZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setVideoHoldCallElapsedTime(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVideoHoldCallElapsedTime(ZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setVideoHoldCallInfoVisible(Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    const v2, 0x7f03001c

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/VideoHoldCallInfoView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    new-instance v1, Lcom/android/incallui/CallCardFragment$5;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardFragment$5;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVideoHoldCallInfoOperationListener(Lcom/android/incallui/view/VideoHoldCallInfoView$VideoHoldCallInfoOperationListener;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    invoke-virtual {v0, v3}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVideoHoldInfoView(Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setHasHoldCall(Z)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    goto :goto_0
.end method

.method public setVideoHoldCallState(ILandroid/telecom/DisconnectCause;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVideoHoldCallState(ILandroid/telecom/DisconnectCause;Z)V

    :cond_0
    return-void
.end method

.method public setVideoHoldInfo(Lcom/android/incallui/model/CallCardInfo;ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVideoHold(Lcom/android/incallui/model/CallCardInfo;ZZ)V

    :cond_0
    return-void
.end method

.method public setVisible(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setVisibleFullScreenVideo(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz p1, :cond_2

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVideoHoldInfoView(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setYellowPageInfo(Lcom/android/incallui/model/CallCardInfo;)V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mLabelAndNumberView:Lcom/android/incallui/view/LabelAndNumberView;

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->markTitle:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->markCount:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->markProviderIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/view/LabelAndNumberView;->showMarkInfo(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardOptionalInfo:Lcom/android/incallui/view/CallCardOptionalInfoView;

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->company:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->showProviderInfo(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardOptionalInfo:Lcom/android/incallui/view/CallCardOptionalInfoView;

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->extraInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->showExtraInfo(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardOptionalInfo:Lcom/android/incallui/view/CallCardOptionalInfoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->setVisibility(I)V

    return-void
.end method

.method public showAnswerIncomingBanner()V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/android/incallui/CallCardFragment;->showVideoBanner(Z)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v4, v8, [F

    const/high16 v5, 0x43160000    # 150.0f

    aput v5, v4, v6

    aput v9, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v8, [F

    aput v9, v4, v6

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v2, Lcom/android/incallui/CallCardFragment$7;

    invoke-direct {v2, p0}, Lcom/android/incallui/CallCardFragment$7;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-direct {p0}, Lcom/android/incallui/CallCardFragment;->updatePrimaryBannerAnimatorSet()V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    new-array v3, v8, [Landroid/animation/Animator;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v3, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v3}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public showConferenceControl(ZI)V
    .locals 8

    const/4 v7, 0x0

    const/16 v6, 0xd

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v2}, Lcom/android/incallui/view/CallCardStateInfoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    const v3, 0x800013

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    const v3, 0x7f02008c

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0034

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/incallui/util/Utils;->localizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/incallui/view/CallCardStateInfoView;->setConferenceCallNumber(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :goto_0
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v2, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v2, v7}, Lcom/android/incallui/view/CallCardStateInfoView;->setConferenceCallNumber(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0
.end method

.method public showCrbtPrompt(ZLjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCrbtPrompt:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mCrbtPrompt:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCrbtPrompt:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showDialBanner()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/android/incallui/CallCardFragment;->showVideoBanner(Z)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->clearAnimation()V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v4, v3, v5

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/incallui/CallCardFragment$8;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardFragment$8;-><init>(Lcom/android/incallui/CallCardFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-direct {p0}, Lcom/android/incallui/CallCardFragment;->updatePrimaryBannerAnimatorSet()V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    new-array v2, v6, [Landroid/animation/Animator;

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v2, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v2}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryBannerAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public showIncomingBanner()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/incallui/CallCardFragment;->showVideoBanner(Z)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public showSmallAvatar(I)V
    .locals 6

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTranslateY:I

    if-eq v0, p1, :cond_2

    iput p1, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTranslateY:I

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTransAnim:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTransAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTransAnim:Landroid/animation/ObjectAnimator;

    new-array v1, v2, [F

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    aput v2, v1, v4

    int-to-float v2, p1

    aput v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    :goto_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTransAnim:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTransAnim:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTransAnim:Landroid/animation/ObjectAnimator;

    new-instance v1, Lmiui/maml/animation/interpolater/ElasticEaseOutInterpolater;

    const/high16 v2, 0x40000000    # 2.0f

    const v3, 0x3f99999a    # 1.2f

    invoke-direct {v1, v2, v3}, Lmiui/maml/animation/interpolater/ElasticEaseOutInterpolater;-><init>(FF)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTransAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v2, [F

    iget-object v3, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTranslationY()F

    move-result v3

    aput v3, v2, v4

    int-to-float v3, p1

    aput v3, v2, v5

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarTransAnim:Landroid/animation/ObjectAnimator;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mAvatarLayout:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public showSpeakerOnOrIncomingMask(ZZ)V
    .locals 3

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    const v0, 0x7f020018

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSpeakerOnMask:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSpeakerOnMask:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    const v0, 0x7f0200b9

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSpeakerOnMask:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public showUserLockedPrompt(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mUserLockedPrompt:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mUserLockedPrompt:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showVideoBanner(Z)V
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f030015

    invoke-virtual {v0, v3, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/SingleVideoCallInfoView;

    iput-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mVideoHoldCallInfo:Lcom/android/incallui/view/VideoHoldCallInfoView;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setHasHoldCall(Z)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallCardPresenter;->isDialPadVisible()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setDialPadVisible(Z)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    goto :goto_1
.end method

.method public showVideoDigitsToField(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mSingleVideoCallInfo:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->showDigitsToField(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public updateConferenceMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardFragment;->mCallCardStateInfoView:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/CallCardStateInfoView;->setConferenceMode(Z)V

    return-void
.end method

.method public updateSuspectInfo(Z)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    const v3, 0x7f030009

    invoke-virtual {v1, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    const v2, 0x7f0a0040

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/incallui/CallCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0093

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallCardFragment;->mRootView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    iput-object v3, p0, Lcom/android/incallui/CallCardFragment;->mSuspectView:Landroid/view/View;

    goto :goto_0
.end method
