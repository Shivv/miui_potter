.class Lcom/android/incallui/CallerInfo$1$1;
.super Lcom/android/incallui/util/SimpleTask;
.source "CallerInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/CallerInfo$1;->onPostExecute(Lcom/android/incallui/model/YellowPageInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/util/SimpleTask",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/incallui/CallerInfo$1;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$cookie:Ljava/lang/Object;

.field final synthetic val$isIncoming:Z

.field final synthetic val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

.field final synthetic val$number:Ljava/lang/String;

.field final synthetic val$phoneInfo:Lcom/android/incallui/model/YellowPageInfo;

.field final synthetic val$previousResult:Lcom/android/incallui/CallerInfo;


# direct methods
.method constructor <init>(Lcom/android/incallui/CallerInfo$1;Landroid/content/Context;Lcom/android/incallui/model/YellowPageInfo;Ljava/lang/String;ZLcom/android/incallui/CallerInfo;Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallerInfo$1$1;->this$1:Lcom/android/incallui/CallerInfo$1;

    iput-object p2, p0, Lcom/android/incallui/CallerInfo$1$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/incallui/CallerInfo$1$1;->val$phoneInfo:Lcom/android/incallui/model/YellowPageInfo;

    iput-object p4, p0, Lcom/android/incallui/CallerInfo$1$1;->val$number:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/android/incallui/CallerInfo$1$1;->val$isIncoming:Z

    iput-object p6, p0, Lcom/android/incallui/CallerInfo$1$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    iput-object p7, p0, Lcom/android/incallui/CallerInfo$1$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    iput-object p8, p0, Lcom/android/incallui/CallerInfo$1$1;->val$cookie:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallerInfo$1$1;->doInBackground()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/lang/Void;
    .locals 8

    const/4 v7, 0x0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/android/incallui/CallerInfo$1$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/incallui/CallerInfo$1$1;->val$phoneInfo:Lcom/android/incallui/model/YellowPageInfo;

    invoke-virtual {v3}, Lcom/android/incallui/model/YellowPageInfo;->getYellowPageId()J

    move-result-wide v4

    iget-object v3, p0, Lcom/android/incallui/CallerInfo$1$1;->val$number:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/android/incallui/CallerInfo$1$1;->val$isIncoming:Z

    invoke-static {v2, v4, v5, v3, v6}, Lmiui/yellowpage/YellowPageImgLoader;->loadPhoneDisplayAd(Landroid/content/Context;JLjava/lang/String;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/incallui/CallerInfo$1$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/android/incallui/CallerInfo$1$1;->val$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v3, v2, Lcom/android/incallui/CallerInfo;->cachedPhoto:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/incallui/CallerInfo$1$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/android/incallui/CallerInfo;->isCachedPhotoCurrent:Z

    :cond_0
    return-object v7

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string/jumbo v2, "doYellowPageQuery: loadPhoneDisplayAd has error"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/incallui/CallerInfo$1$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/CallerInfo$1$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallerInfo$1$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    iget-object v1, p0, Lcom/android/incallui/CallerInfo$1$1;->val$cookie:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/incallui/CallerInfo$1$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    const/16 v3, 0xb

    invoke-interface {v0, v3, v1, v2}, Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;->onQueryComplete(ILjava/lang/Object;Lcom/android/incallui/CallerInfo;)V

    :cond_0
    return-void
.end method
