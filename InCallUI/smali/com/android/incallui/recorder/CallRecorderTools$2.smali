.class Lcom/android/incallui/recorder/CallRecorderTools$2;
.super Lcom/android/incallui/util/SimpleTask;
.source "CallRecorderTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/recorder/CallRecorderTools;->checkToRecord(ZLjava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/util/SimpleTask",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/recorder/CallRecorderTools;

.field final synthetic val$contactExists:Z

.field final synthetic val$isSipCall:Z

.field final synthetic val$name:Ljava/lang/String;

.field final synthetic val$number:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/incallui/recorder/CallRecorderTools;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    iput-object p2, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$number:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$contactExists:Z

    iput-boolean p4, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$isSipCall:Z

    iput-object p5, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$name:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected doInBackground()Ljava/lang/Boolean;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/recorder/RecorderUtils;->isAutoRecordOn(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4

    :cond_0
    invoke-static {v0}, Lcom/android/incallui/recorder/RecorderUtils;->isAutoRecordForAllOrCustom(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4

    :cond_1
    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$number:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/android/incallui/recorder/RecorderUtils;->isContainWhiteList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4

    :cond_2
    invoke-static {v0}, Lcom/android/incallui/recorder/RecorderUtils;->isAutoRecordForYellowPage(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-static {v0}, Lcom/android/incallui/recorder/RecorderUtils;->isAutoRecordForUnknown(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$contactExists:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4

    :cond_3
    const/4 v3, 0x1

    :goto_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$number:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lmiui/yellowpage/YellowPageUtils;->getPhoneInfo(Landroid/content/Context;Ljava/lang/String;Z)Lmiui/yellowpage/YellowPagePhone;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-eqz v4, :cond_8

    const/4 v2, 0x1

    :goto_1
    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->-get0()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "recordYellowPage = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " isYellowPage = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_4

    if-nez v2, :cond_5

    :cond_4
    if-nez v3, :cond_9

    xor-int/lit8 v4, v2, 0x1

    if-eqz v4, :cond_9

    :cond_5
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4

    :cond_6
    invoke-static {v0}, Lcom/android/incallui/recorder/RecorderUtils;->isAutoRecordForUnknown(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-boolean v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$contactExists:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_7

    const/4 v3, 0x0

    goto :goto_0

    :cond_7
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4

    :cond_8
    const/4 v2, 0x0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->-get0()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Check is yellow page error"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderTools$2;->doInBackground()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    iget-boolean v1, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$isSipCall:Z

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$name:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools$2;->val$number:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/android/incallui/recorder/CallRecorderTools;->-wrap0(Lcom/android/incallui/recorder/CallRecorderTools;ZLjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/incallui/recorder/CallRecorderTools$2;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
