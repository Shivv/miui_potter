.class public Lcom/android/incallui/recorder/CallRecorder;
.super Ljava/lang/Object;
.source "CallRecorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;
    }
.end annotation


# instance fields
.field private mAudioEncodingBitRate:I

.field private mAudioSamplingRate:I

.field private mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

.field private mContext:Landroid/content/Context;

.field private mMediaRecorder:Lmiui/media/Mp3Recorder;

.field private mOutFilePath:Ljava/lang/String;

.field private mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorder;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/recorder/CallRecorder;->mAudioSamplingRate:I

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/recorder/CallRecorder;->mAudioEncodingBitRate:I

    return-void
.end method

.method private formatNames(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v1, 0x0

    :goto_1
    const-string/jumbo v3, "\\/:*?\"<>|"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\\/:*?\"<>|"

    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {p1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object p1
.end method

.method private notifyRecording(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 7

    new-instance v0, Lcom/android/incallui/recorder/CallRecorder$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/incallui/recorder/CallRecorder$3;-><init>(Lcom/android/incallui/recorder/CallRecorder;Landroid/content/Context;Ljava/lang/String;J)V

    invoke-static {v0}, Lcom/android/incallui/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private supportCallStreamRecording(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getRecordingTimeInMillis()J
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v0}, Lmiui/media/Mp3Recorder;->getRecordingTimeInMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method public isCallRecording()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCallRecordStateListener(Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    return-void
.end method

.method public startCallRecord(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v12, 0x1
    # ORIGINAL IS 0x4
    # potter uses 0x1

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, -0x1

    const-string/jumbo v5, "CallRecorder"

    const-string/jumbo v8, "startCallRecord()"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;

    if-nez v5, :cond_0

    new-instance v5, Lcom/android/incallui/recorder/StorageMonitor;

    iget-object v8, p0, Lcom/android/incallui/recorder/CallRecorder;->mContext:Landroid/content/Context;

    invoke-direct {v5, v8}, Lcom/android/incallui/recorder/StorageMonitor;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;

    :cond_0
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;

    new-instance v8, Lcom/android/incallui/recorder/CallRecorder$1;

    invoke-direct {v8, p0}, Lcom/android/incallui/recorder/CallRecorder$1;-><init>(Lcom/android/incallui/recorder/CallRecorder;)V

    invoke-virtual {v5, v8}, Lcom/android/incallui/recorder/StorageMonitor;->startMonitor(Lcom/android/incallui/recorder/StorageMonitor$Listener;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;

    invoke-virtual {v5}, Lcom/android/incallui/recorder/StorageMonitor;->isUsingExternalStorage()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_5

    :cond_1
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;

    invoke-virtual {v5}, Lcom/android/incallui/recorder/StorageMonitor;->stopMonitor()V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;

    invoke-virtual {v5}, Lcom/android/incallui/recorder/StorageMonitor;->isUsingExternalStorage()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    invoke-interface {v5, v11}, Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;->onCallRecordFailed(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    if-ne v1, v10, :cond_4

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    invoke-interface {v5, v10}, Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;->onCallRecordFailed(I)V

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    const/4 v8, 0x6

    invoke-interface {v5, v8}, Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;->onCallRecordFailed(I)V

    goto :goto_0

    :cond_5
    const-string/jumbo v3, "/MIUI/sound_recorder/call_rec"

    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lmiui/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v4, v9, v9, v9}, Lmiui/os/FileUtils;->mkdirs(Ljava/io/File;III)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    invoke-interface {v5, v12}, Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;->onCallRecordFailed(I)V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;

    invoke-virtual {v5}, Lcom/android/incallui/recorder/StorageMonitor;->stopMonitor()V

    return-void

    :cond_6
    new-instance v2, Ljava/io/File;

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/incallui/recorder/CallRecorder;->formatNames(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    const-string/jumbo v9, ".mp3"

    invoke-static {v5, v8, p2, v9}, Lmiui/util/RecordingNameUtils;->generatCallRecordingName(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mOutFilePath:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    if-eqz v5, :cond_7

    :try_start_0
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v5}, Lmiui/media/Mp3Recorder;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v5}, Lmiui/media/Mp3Recorder;->release()V

    :cond_7
    new-instance v5, Lmiui/media/Mp3Recorder;

    invoke-direct {v5}, Lmiui/media/Mp3Recorder;-><init>()V

    iput-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mContext:Landroid/content/Context;

    invoke-direct {p0, v5}, Lcom/android/incallui/recorder/CallRecorder;->supportCallStreamRecording(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v5, v12}, Lmiui/media/Mp3Recorder;->setAudioSource(I)V

    :goto_2
    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v8, "omega"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v8, "nikel"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    :cond_8
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    const/16 v8, 0xc

    invoke-virtual {v5, v8}, Lmiui/media/Mp3Recorder;->setAudioChannel(I)V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    const/16 v8, 0x3e80

    invoke-virtual {v5, v8}, Lmiui/media/Mp3Recorder;->setAudioSamplingRate(I)V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    const/16 v8, 0x18

    invoke-virtual {v5, v8}, Lmiui/media/Mp3Recorder;->setOutBitRate(I)V

    :goto_3
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v5, v11}, Lmiui/media/Mp3Recorder;->setQuality(I)V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    iget-object v8, p0, Lcom/android/incallui/recorder/CallRecorder;->mOutFilePath:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lmiui/media/Mp3Recorder;->setOutputFile(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    new-instance v8, Lcom/android/incallui/recorder/CallRecorder$2;

    invoke-direct {v8, p0}, Lcom/android/incallui/recorder/CallRecorder$2;-><init>(Lcom/android/incallui/recorder/CallRecorder;)V

    invoke-virtual {v5, v8}, Lmiui/media/Mp3Recorder;->setOnErrorListener(Lmiui/media/Mp3Recorder$RecordingErrorListener;)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v6

    :try_start_1
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v5}, Lmiui/media/Mp3Recorder;->prepare()V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v5}, Lmiui/media/Mp3Recorder;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    const-string/jumbo v5, "CallRecorder"

    const-string/jumbo v8, "Call record started!"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    invoke-interface {v5}, Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;->onCallRecordStarted()V

    :cond_9
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v5, "CallRecorder"

    const-string/jumbo v8, "Error when stop call recording"

    invoke-static {v5, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :cond_a
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v5, v10}, Lmiui/media/Mp3Recorder;->setAudioSource(I)V

    goto :goto_2

    :cond_b
    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    iget v8, p0, Lcom/android/incallui/recorder/CallRecorder;->mAudioSamplingRate:I

    invoke-virtual {v5, v8}, Lmiui/media/Mp3Recorder;->setAudioSamplingRate(I)V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    iget v8, p0, Lcom/android/incallui/recorder/CallRecorder;->mAudioEncodingBitRate:I

    invoke-virtual {v5, v8}, Lmiui/media/Mp3Recorder;->setOutBitRate(I)V

    goto :goto_3

    :catch_1
    move-exception v0

    :try_start_2
    const-string/jumbo v5, "CallRecorder"

    const-string/jumbo v8, "Error when start call recording"

    invoke-static {v5, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v5, 0x3

    invoke-virtual {p0, v5}, Lcom/android/incallui/recorder/CallRecorder;->stopCallRecord(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-void

    :catchall_0
    move-exception v5

    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v5
.end method

.method public stopCallRecord()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/recorder/CallRecorder;->stopCallRecord(I)V

    return-void
.end method

.method public declared-synchronized stopCallRecord(I)V
    .locals 6

    monitor-enter p0

    :try_start_0
    const-string/jumbo v3, "CallRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "stopRecording("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lmiui/media/Mp3Recorder;->setOnErrorListener(Lmiui/media/Mp3Recorder$RecordingErrorListener;)V

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mStorageMonitor:Lcom/android/incallui/recorder/StorageMonitor;

    invoke-virtual {v3}, Lcom/android/incallui/recorder/StorageMonitor;->stopMonitor()V

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v3}, Lmiui/media/Mp3Recorder;->getRecordingTimeInMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :try_start_1
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v3}, Lmiui/media/Mp3Recorder;->stop()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    invoke-virtual {v3}, Lmiui/media/Mp3Recorder;->release()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mMediaRecorder:Lmiui/media/Mp3Recorder;

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorder;->mOutFilePath:Ljava/lang/String;

    invoke-interface {v3, p1, v4}, Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;->onCallRecordStoped(ILjava/lang/String;)V

    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mCallRecordStateListener:Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;

    invoke-interface {v3, p1}, Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;->onCallRecordFailed(I)V

    :cond_0
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorder;->mOutFilePath:Ljava/lang/String;

    invoke-direct {p0, v3, v4, v0, v1}, Lcom/android/incallui/recorder/CallRecorder;->notifyRecording(Landroid/content/Context;Ljava/lang/String;J)V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/incallui/recorder/CallRecorder;->mOutFilePath:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v2

    :try_start_3
    const-string/jumbo v3, "CallRecorder"

    const-string/jumbo v4, "Error when stop call recording"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
