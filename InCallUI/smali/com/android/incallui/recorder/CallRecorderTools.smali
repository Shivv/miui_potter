.class public Lcom/android/incallui/recorder/CallRecorderTools;
.super Ljava/lang/Object;
.source "CallRecorderTools.java"

# interfaces
.implements Lcom/android/incallui/InCallPresenter$InCallStateListener;
.implements Lcom/android/incallui/InCallPresenter$IncomingCallListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/recorder/CallRecorderTools$1;,
        Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static sCallRecorderTools:Lcom/android/incallui/recorder/CallRecorderTools;


# instance fields
.field private mCall:Lcom/android/incallui/Call;

.field private mCallRecorderRemoteService:Landroid/content/Intent;

.field private mIsTimeToRecord:Z

.field private mLastCallKey:Ljava/lang/String;

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/incallui/recorder/CallRecorderToolsListener;",
            ">;"
        }
    .end annotation
.end field

.field private mNeedRecord:Z

.field private mPreviousFgCallState:I

.field private mRecorderCallback:Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;

.field private mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mWaitingToRecord:Z


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/recorder/CallRecorderTools;)Lcom/android/incallui/Call;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/incallui/recorder/CallRecorderTools;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/incallui/recorder/CallRecorderTools;)Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderCallback:Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/incallui/recorder/CallRecorderTools;)Lcom/android/incallui/recorder/ICallRecorderCommand;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/incallui/recorder/CallRecorderTools;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mWaitingToRecord:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/incallui/recorder/CallRecorderTools;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/incallui/recorder/CallRecorderTools;Lcom/android/incallui/recorder/ICallRecorderCommand;)Lcom/android/incallui/recorder/ICallRecorderCommand;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    return-object p1
.end method

.method static synthetic -set2(Lcom/android/incallui/recorder/CallRecorderTools;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mWaitingToRecord:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/recorder/CallRecorderTools;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/incallui/recorder/CallRecorderTools;->startToRecord(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "CallRecorderTools"

    sput-object v0, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-direct {v0}, Lcom/android/incallui/recorder/CallRecorderTools;-><init>()V

    sput-object v0, Lcom/android/incallui/recorder/CallRecorderTools;->sCallRecorderTools:Lcom/android/incallui/recorder/CallRecorderTools;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mNeedRecord:Z

    iput-object v5, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mLastCallKey:Ljava/lang/String;

    iput v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mPreviousFgCallState:I

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x8

    const v2, 0x3f666666    # 0.9f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mListeners:Ljava/util/Set;

    iput-boolean v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mIsTimeToRecord:Z

    iput-boolean v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mWaitingToRecord:Z

    new-instance v0, Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;

    invoke-direct {v0, p0, v5}, Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;-><init>(Lcom/android/incallui/recorder/CallRecorderTools;Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;)V

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderCallback:Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;

    new-instance v0, Lcom/android/incallui/recorder/CallRecorderTools$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/recorder/CallRecorderTools$1;-><init>(Lcom/android/incallui/recorder/CallRecorderTools;)V

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mServiceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method private checkToRecord(ZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    new-instance v0, Lcom/android/incallui/recorder/CallRecorderTools$2;

    move-object v1, p0

    move-object v2, p3

    move v3, p4

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/incallui/recorder/CallRecorderTools$2;-><init>(Lcom/android/incallui/recorder/CallRecorderTools;Ljava/lang/String;ZZLjava/lang/String;)V

    const-string/jumbo v1, "task_query_recordable"

    invoke-virtual {v0, v1}, Lcom/android/incallui/recorder/CallRecorderTools$2;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v0

    const-wide/16 v2, 0x5dc

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/android/incallui/util/SimpleTask;->run(JZLjava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/android/incallui/recorder/CallRecorderTools;
    .locals 1

    sget-object v0, Lcom/android/incallui/recorder/CallRecorderTools;->sCallRecorderTools:Lcom/android/incallui/recorder/CallRecorderTools;

    return-object v0
.end method

.method private isNeedUpdateUI(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)Z
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-static {v2}, Lcom/android/incallui/Call;->getCallKeyForCallPresenter(Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v0

    if-ne p1, p2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mLastCallKey:Ljava/lang/String;

    invoke-static {v2, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v1, v2, 0x1

    :goto_0
    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mLastCallKey:Ljava/lang/String;

    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private readyToRecord(Z)V
    .locals 8

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderTools;->bindService()V

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v3, v4

    :goto_0
    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->getInstance()Lcom/android/incallui/ContactInfoCache;

    move-result-object v5

    iget-object v6, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/incallui/ContactInfoCache;->getInfo(Ljava/lang/String;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v1

    const/4 v0, 0x0

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-boolean v0, v1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->contactExists:Z

    iget-boolean v2, v1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isSipCall:Z

    iget-object v4, v1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->number:Ljava/lang/String;

    iget-object v5, v1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v3, v1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    :cond_0
    if-eqz p1, :cond_3

    sget-object v5, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mNeedRecord = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mNeedRecord:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mNeedRecord:Z

    if-eqz v5, :cond_2

    invoke-direct {p0, v2, v3, v4}, Lcom/android/incallui/recorder/CallRecorderTools;->startToRecord(ZLjava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    const-string/jumbo v3, ""

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2, v3, v4, v0}, Lcom/android/incallui/recorder/CallRecorderTools;->checkToRecord(ZLjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_3
    sget-object v5, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "startCallRecord."

    invoke-static {v5, v6}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2, v3, v4}, Lcom/android/incallui/recorder/CallRecorderTools;->startToRecord(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private startToRecord(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v1

    const v2, 0x7f0b002b

    invoke-static {v1, v2}, Lcom/android/incallui/util/Utils;->checkUserUnlocked(Landroid/content/Context;I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    if-eqz p1, :cond_2

    const v1, 0x7f0b002c

    invoke-static {v1}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mNeedRecord:Z

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    if-eqz v1, :cond_3

    :try_start_0
    sget-object v1, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startCallRecord."

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2, p3}, Lcom/android/incallui/recorder/ICallRecorderCommand;->startCallRecord(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startCallRecord RemoteException"

    invoke-static {v1, v2, v0}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "record service not bind to activity, waiting to bind"

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mWaitingToRecord:Z

    goto :goto_0
.end method


# virtual methods
.method public addCallRecorderToolsListener(Lcom/android/incallui/recorder/CallRecorderToolsListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public bindService()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "bindService"

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/incallui/recorder/CallRecorderRemoteService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Could not bind service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public checkRecordTime()V
    .locals 3

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/incallui/recorder/ICallRecorderCommand;->checkCallRecordTime(Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "checkCallRecordTime RemoteException"

    invoke-static {v1, v2, v0}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onIncomingCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/incallui/recorder/CallRecorderTools;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void
.end method

.method public onRecordClick(Z)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isCDMASupportRealActiveTime(Lcom/android/incallui/Call;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mIsTimeToRecord:Z

    if-eqz v1, :cond_2

    :cond_0
    invoke-direct {p0, v3}, Lcom/android/incallui/recorder/CallRecorderTools;->readyToRecord(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mNeedRecord:Z

    goto :goto_0

    :cond_3
    iput-boolean v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mNeedRecord:Z

    iput-boolean v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mWaitingToRecord:Z

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    invoke-interface {v1}, Lcom/android/incallui/recorder/ICallRecorderCommand;->stopCallRecord()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopCallRecord RemoteException"

    invoke-static {v1, v2, v0}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq p2, v3, :cond_0

    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->PENDING_OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_2

    :cond_0
    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    if-nez v3, :cond_1

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getPendingOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    :cond_1
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/android/incallui/recorder/CallRecorderTools;->isNeedUpdateUI(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)Z

    move-result v3

    if-nez v3, :cond_5

    sget-object v3, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ignore NeedUpdateUI:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mLastCallKey:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_3

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getActiveOrBackgroundCall()Lcom/android/incallui/Call;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->INCOMING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_4

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getIncomingCall()Lcom/android/incallui/Call;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_4
    iput-object v6, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderTools;->checkRecordTime()V

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->getState()I

    move-result v3

    if-ne v3, v7, :cond_8

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-static {v3}, Lcom/android/incallui/CallUtils;->isCDMAPhone(Lcom/android/incallui/Call;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-static {v3}, Lcom/android/incallui/CallUtils;->isCDMASupportRealActiveTime(Lcom/android/incallui/Call;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mPreviousFgCallState:I

    if-eq v3, v7, :cond_6

    iget v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mPreviousFgCallState:I

    const/4 v6, 0x4

    if-ne v3, v6, :cond_9

    :cond_6
    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mIsTimeToRecord:Z

    sget-object v3, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "onStateChange mIsTimeToRecord:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mIsTimeToRecord:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " isCDMASupportRealActiveTime:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mIsTimeToRecord:Z

    if-nez v3, :cond_7

    xor-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_8

    :cond_7
    invoke-direct {p0, v4}, Lcom/android/incallui/recorder/CallRecorderTools;->readyToRecord(Z)V

    :cond_8
    :goto_2
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->getState()I

    move-result v3

    iput v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mPreviousFgCallState:I

    :goto_3
    sget-object v3, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v3, :cond_f

    iput-boolean v5, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mNeedRecord:Z

    iput-boolean v5, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mWaitingToRecord:Z

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mListeners:Ljava/util/Set;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/recorder/CallRecorderToolsListener;

    invoke-interface {v1}, Lcom/android/incallui/recorder/CallRecorderToolsListener;->stopManualAutoRecordIfNotStartedCallback()V

    goto :goto_4

    :cond_9
    iget v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mPreviousFgCallState:I

    const/16 v6, 0x8

    if-ne v3, v6, :cond_a

    move v3, v4

    goto :goto_1

    :cond_a
    move v3, v5

    goto :goto_1

    :cond_b
    move v3, v5

    goto :goto_1

    :cond_c
    iget v3, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mPreviousFgCallState:I

    if-eq v3, v7, :cond_8

    invoke-direct {p0, v4}, Lcom/android/incallui/recorder/CallRecorderTools;->readyToRecord(Z)V

    goto :goto_2

    :cond_d
    iput v5, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mPreviousFgCallState:I

    goto :goto_3

    :cond_e
    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderTools;->unbindService()V

    :cond_f
    return-void
.end method

.method public removeCallRecorderToolsListener(Lcom/android/incallui/recorder/CallRecorderToolsListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public unbindService()V
    .locals 6

    :try_start_0
    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    invoke-interface {v4}, Lcom/android/incallui/recorder/ICallRecorderCommand;->isBound()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "unbindService"

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    invoke-interface {v4}, Lcom/android/incallui/recorder/ICallRecorderCommand;->stopCallRecord()V

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderCallback:Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;

    invoke-interface {v4, v5}, Lcom/android/incallui/recorder/ICallRecorderCommand;->unregisterCallback(Lcom/android/incallui/recorder/IRecorderCallback;)V

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;

    invoke-virtual {v0, v4}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mRecorderService:Lcom/android/incallui/recorder/ICallRecorderCommand;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools;->mCallRecorderRemoteService:Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v3

    sget-object v4, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "stopService fail"

    invoke-static {v4, v5, v3}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v2

    sget-object v4, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "unbindService fail"

    invoke-static {v4, v5, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_2
    move-exception v1

    sget-object v4, Lcom/android/incallui/recorder/CallRecorderTools;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "mRecorderService.isBound RemoteException"

    invoke-static {v4, v5, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
