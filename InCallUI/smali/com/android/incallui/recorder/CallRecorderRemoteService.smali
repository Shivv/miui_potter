.class public Lcom/android/incallui/recorder/CallRecorderRemoteService;
.super Landroid/app/Service;
.source "CallRecorderRemoteService.java"

# interfaces
.implements Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;,
        Lcom/android/incallui/recorder/CallRecorderRemoteService$RecordPhoneStateListener;
    }
.end annotation


# instance fields
.field private mBound:Z

.field private mCallId:Ljava/lang/String;

.field private mCallRecorder:Lcom/android/incallui/recorder/CallRecorder;

.field private mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/android/incallui/recorder/IRecorderCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalBinder:Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;

.field private mNotificationMgr:Lcom/android/incallui/recorder/RecorderNotificationMgr;

.field stateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mBound:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorder:Lcom/android/incallui/recorder/CallRecorder;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Landroid/os/RemoteCallbackList;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/recorder/CallRecorderRemoteService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallId:Ljava/lang/String;

    return-object p1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    new-instance v0, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;-><init>(Lcom/android/incallui/recorder/CallRecorderRemoteService;)V

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mLocalBinder:Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;

    new-instance v0, Lcom/android/incallui/recorder/CallRecorderRemoteService$RecordPhoneStateListener;

    invoke-direct {v0, p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService$RecordPhoneStateListener;-><init>(Lcom/android/incallui/recorder/CallRecorderRemoteService;)V

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->stateListener:Landroid/telephony/PhoneStateListener;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string/jumbo v0, "CallRecorderRemoteService"

    const-string/jumbo v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mBound:Z

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mLocalBinder:Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;

    return-object v0
.end method

.method public onCallRecordFailed(I)V
    .locals 6

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/recorder/IRecorderCallback;

    invoke-interface {v3, p1}, Lcom/android/incallui/recorder/IRecorderCallback;->onCallRecorderError(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string/jumbo v3, "CallRecorderRemoteService"

    const-string/jumbo v5, "Callback onFmTurnedOn Failed"

    invoke-static {v3, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v4

    return-void
.end method

.method public onCallRecordStarted()V
    .locals 6

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/recorder/IRecorderCallback;

    invoke-interface {v3}, Lcom/android/incallui/recorder/IRecorderCallback;->onCallRecorderStarted()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string/jumbo v3, "CallRecorderRemoteService"

    const-string/jumbo v5, "Callback onFmTurnedOn Failed"

    invoke-static {v3, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mNotificationMgr:Lcom/android/incallui/recorder/RecorderNotificationMgr;

    invoke-virtual {v3}, Lcom/android/incallui/recorder/RecorderNotificationMgr;->notifyCallRecord()V

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v4

    return-void
.end method

.method public onCallRecordStoped(ILjava/lang/String;)V
    .locals 8

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/recorder/IRecorderCallback;

    invoke-interface {v3, p1, p2}, Lcom/android/incallui/recorder/IRecorderCallback;->onCallRecorderStoped(ILjava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string/jumbo v3, "CallRecorderRemoteService"

    const-string/jumbo v5, "Callback onFmTurnedOn Failed"

    invoke-static {v3, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mNotificationMgr:Lcom/android/incallui/recorder/RecorderNotificationMgr;

    invoke-virtual {v3}, Lcom/android/incallui/recorder/RecorderNotificationMgr;->cancelCallRecord()V

    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v4

    return-void

    :pswitch_0
    :try_start_4
    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3}, Landroid/provider/MiuiSettings$Telephony;->isRecordNotificationEnabled(Landroid/content/ContentResolver;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mNotificationMgr:Lcom/android/incallui/recorder/RecorderNotificationMgr;

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const v7, 0x7f0b008a

    invoke-virtual {v5, v7, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v3, v6, v5, p2}, Lcom/android/incallui/recorder/RecorderNotificationMgr;->updateCallRecordNotification(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_1
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mNotificationMgr:Lcom/android/incallui/recorder/RecorderNotificationMgr;

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const v7, 0x7f0b008b

    invoke-virtual {v5, v7, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v3, v6, v5, p2}, Lcom/android/incallui/recorder/RecorderNotificationMgr;->updateCallRecordNotification(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_2
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mNotificationMgr:Lcom/android/incallui/recorder/RecorderNotificationMgr;

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const v7, 0x7f0b008c

    invoke-virtual {v5, v7, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v3, v6, v5, p2}, Lcom/android/incallui/recorder/RecorderNotificationMgr;->updateCallRecordNotification(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCallRecordTimeCheck(J)V
    .locals 7

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/recorder/IRecorderCallback;

    invoke-interface {v3, p1, p2}, Lcom/android/incallui/recorder/IRecorderCallback;->onCheckRecordTime(J)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string/jumbo v3, "CallRecorderRemoteService"

    const-string/jumbo v5, "Callback onFmTurnedOn Failed"

    invoke-static {v3, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v4

    return-void
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string/jumbo v0, "CallRecorderRemoteService"

    const-string/jumbo v1, "start service CallRecorderRemoteService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorder:Lcom/android/incallui/recorder/CallRecorder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/incallui/recorder/CallRecorder;

    invoke-direct {v0, p0}, Lcom/android/incallui/recorder/CallRecorder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorder:Lcom/android/incallui/recorder/CallRecorder;

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorder:Lcom/android/incallui/recorder/CallRecorder;

    invoke-virtual {v0, p0}, Lcom/android/incallui/recorder/CallRecorder;->setCallRecordStateListener(Lcom/android/incallui/recorder/CallRecorder$CallRecordStateListener;)V

    :cond_0
    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->stateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    invoke-static {}, Lcom/android/incallui/recorder/RecorderNotificationMgr;->init()Lcom/android/incallui/recorder/RecorderNotificationMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mNotificationMgr:Lcom/android/incallui/recorder/RecorderNotificationMgr;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mCallRecorderStateListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->stateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2

    const-string/jumbo v0, "CallRecorderRemoteService"

    const-string/jumbo v1, "onRebind"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mBound:Z

    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService;->mBound:Z

    const/4 v0, 0x1

    return v0
.end method
