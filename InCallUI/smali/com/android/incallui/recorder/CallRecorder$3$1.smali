.class Lcom/android/incallui/recorder/CallRecorder$3$1;
.super Lcom/android/incallui/util/SimpleTask;
.source "CallRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/recorder/CallRecorder$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/util/SimpleTask",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/incallui/recorder/CallRecorder$3;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$duration:J

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/incallui/recorder/CallRecorder$3;Landroid/content/Context;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorder$3$1;->this$1:Lcom/android/incallui/recorder/CallRecorder$3;

    iput-object p2, p0, Lcom/android/incallui/recorder/CallRecorder$3$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/incallui/recorder/CallRecorder$3$1;->val$path:Ljava/lang/String;

    iput-wide p4, p0, Lcom/android/incallui/recorder/CallRecorder$3$1;->val$duration:J

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorder$3$1;->doInBackground()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/lang/Void;
    .locals 6

    :try_start_0
    iget-object v1, p0, Lcom/android/incallui/recorder/CallRecorder$3$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorder$3$1;->val$path:Ljava/lang/String;

    iget-wide v4, p0, Lcom/android/incallui/recorder/CallRecorder$3$1;->val$duration:J

    invoke-static {v1, v2, v4, v5}, Lmiui/provider/Recordings;->notifyRecording(Landroid/content/Context;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return-object v1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "CallRecorder"

    const-string/jumbo v2, "Error when notify call recording"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
