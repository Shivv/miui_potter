.class Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;
.super Lcom/android/incallui/recorder/IRecorderCallback$Stub;
.source "CallRecorderTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/recorder/CallRecorderTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecorderCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/recorder/CallRecorderTools;


# direct methods
.method private constructor <init>(Lcom/android/incallui/recorder/CallRecorderTools;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-direct {p0}, Lcom/android/incallui/recorder/IRecorderCallback$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/incallui/recorder/CallRecorderTools;Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;-><init>(Lcom/android/incallui/recorder/CallRecorderTools;)V

    return-void
.end method


# virtual methods
.method public onCallRecorderError(I)V
    .locals 6

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->-get0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "call recording failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v3}, Lcom/android/incallui/recorder/CallRecorderTools;->-get2(Lcom/android/incallui/recorder/CallRecorderTools;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/recorder/CallRecorderToolsListener;

    invoke-interface {v0}, Lcom/android/incallui/recorder/CallRecorderToolsListener;->callRecordingFailed()V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    const v2, 0x7f0b0087

    :goto_1
    invoke-static {v2}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    return-void

    :pswitch_0
    const v2, 0x7f0b0085

    goto :goto_1

    :pswitch_1
    const v2, 0x7f0b0084

    goto :goto_1

    :pswitch_2
    const v2, 0x7f0b0083

    goto :goto_1

    :pswitch_3
    const v2, 0x7f0b0082

    goto :goto_1

    :pswitch_4
    const v2, 0x7f0b0086

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCallRecorderStarted()V
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v2}, Lcom/android/incallui/recorder/CallRecorderTools;->-get2(Lcom/android/incallui/recorder/CallRecorderTools;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/recorder/CallRecorderToolsListener;

    invoke-interface {v0}, Lcom/android/incallui/recorder/CallRecorderToolsListener;->callRecordingStarted()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCallRecorderStoped(ILjava/lang/String;)V
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v2}, Lcom/android/incallui/recorder/CallRecorderTools;->-get2(Lcom/android/incallui/recorder/CallRecorderTools;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/recorder/CallRecorderToolsListener;

    invoke-interface {v0, p1, p2}, Lcom/android/incallui/recorder/CallRecorderToolsListener;->callRecordingStopped(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCheckRecordTime(J)V
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v2}, Lcom/android/incallui/recorder/CallRecorderTools;->-get2(Lcom/android/incallui/recorder/CallRecorderTools;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/recorder/CallRecorderToolsListener;

    invoke-interface {v0, p1, p2}, Lcom/android/incallui/recorder/CallRecorderToolsListener;->recordTimeCallback(J)V

    goto :goto_0

    :cond_0
    return-void
.end method
