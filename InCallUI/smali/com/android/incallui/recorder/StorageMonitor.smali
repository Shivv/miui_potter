.class public Lcom/android/incallui/recorder/StorageMonitor;
.super Ljava/lang/Object;
.source "StorageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/recorder/StorageMonitor$1;,
        Lcom/android/incallui/recorder/StorageMonitor$Listener;,
        Lcom/android/incallui/recorder/StorageMonitor$MonitorHandler;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsUsingExternalStorage:Z

.field private mListener:Lcom/android/incallui/recorder/StorageMonitor$Listener;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mReceiverRegistered:Z


# direct methods
.method static synthetic -get0(Lcom/android/incallui/recorder/StorageMonitor;)Lcom/android/incallui/recorder/StorageMonitor$Listener;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mListener:Lcom/android/incallui/recorder/StorageMonitor$Listener;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/recorder/StorageMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/recorder/StorageMonitor;->checkStorageSpace()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mReceiverRegistered:Z

    new-instance v0, Lcom/android/incallui/recorder/StorageMonitor$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/recorder/StorageMonitor$1;-><init>(Lcom/android/incallui/recorder/StorageMonitor;)V

    iput-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/incallui/recorder/StorageMonitor;->mContext:Landroid/content/Context;

    invoke-static {}, Lmiui/os/Environment;->isExternalStorageMounted()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mIsUsingExternalStorage:Z

    new-instance v0, Lcom/android/incallui/recorder/StorageMonitor$MonitorHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/incallui/recorder/StorageMonitor$MonitorHandler;-><init>(Lcom/android/incallui/recorder/StorageMonitor;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private checkStorageSpace()V
    .locals 4

    invoke-direct {p0}, Lcom/android/incallui/recorder/StorageMonitor;->isStorageSpaceLow()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mListener:Lcom/android/incallui/recorder/StorageMonitor$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mListener:Lcom/android/incallui/recorder/StorageMonitor$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/incallui/recorder/StorageMonitor$Listener;->onError(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/recorder/StorageMonitor;->stopMonitor()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mHandler:Landroid/os/Handler;

    const-wide/32 v2, 0xea60

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private isStorageSpaceLow()Z
    .locals 12

    iget-boolean v7, p0, Lcom/android/incallui/recorder/StorageMonitor;->mIsUsingExternalStorage:Z

    if-nez v7, :cond_0

    const/4 v7, 0x1

    return v7

    :cond_0
    const/4 v5, 0x1

    :try_start_0
    new-instance v6, Landroid/os/StatFs;

    invoke-static {}, Lmiui/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/os/StatFs;->getAvailableBlocksLong()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    mul-long v8, v0, v2

    const-wide/32 v10, 0x100000

    cmp-long v7, v8, v10

    if-gez v7, :cond_1

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    :catch_0
    move-exception v4

    const-string/jumbo v7, "Exception when get external storage size"

    invoke-static {p0, v7, v4}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public isUsingExternalStorage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mIsUsingExternalStorage:Z

    return v0
.end method

.method public startMonitor(Lcom/android/incallui/recorder/StorageMonitor$Listener;)I
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {}, Lmiui/os/Environment;->isExternalStorageMounted()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/incallui/recorder/StorageMonitor;->mIsUsingExternalStorage:Z

    invoke-direct {p0}, Lcom/android/incallui/recorder/StorageMonitor;->isStorageSpaceLow()Z

    move-result v1

    if-eqz v1, :cond_0

    return v3

    :cond_0
    if-eqz p1, :cond_2

    iput-object p1, p0, Lcom/android/incallui/recorder/StorageMonitor;->mListener:Lcom/android/incallui/recorder/StorageMonitor$Listener;

    iget-boolean v1, p0, Lcom/android/incallui/recorder/StorageMonitor;->mIsUsingExternalStorage:Z

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/recorder/StorageMonitor;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/incallui/recorder/StorageMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v3, p0, Lcom/android/incallui/recorder/StorageMonitor;->mReceiverRegistered:Z

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/recorder/StorageMonitor;->mHandler:Landroid/os/Handler;

    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    return v4
.end method

.method public stopMonitor()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-boolean v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mIsUsingExternalStorage:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mReceiverRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/incallui/recorder/StorageMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v2, p0, Lcom/android/incallui/recorder/StorageMonitor;->mReceiverRegistered:Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/recorder/StorageMonitor;->mListener:Lcom/android/incallui/recorder/StorageMonitor$Listener;

    return-void
.end method
