.class Lcom/android/incallui/recorder/CallRecorderTools$1;
.super Ljava/lang/Object;
.source "CallRecorderTools.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/recorder/CallRecorderTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/recorder/CallRecorderTools;


# direct methods
.method constructor <init>(Lcom/android/incallui/recorder/CallRecorderTools;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8

    const/4 v7, 0x0

    const-string/jumbo v4, "CRT.oSC"

    invoke-static {v4}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->-get0()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onServiceConnected mWaitingToRecord:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v6}, Lcom/android/incallui/recorder/CallRecorderTools;->-get5(Lcom/android/incallui/recorder/CallRecorderTools;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {p2}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/incallui/recorder/ICallRecorderCommand;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/incallui/recorder/CallRecorderTools;->-set1(Lcom/android/incallui/recorder/CallRecorderTools;Lcom/android/incallui/recorder/ICallRecorderCommand;)Lcom/android/incallui/recorder/ICallRecorderCommand;

    :try_start_0
    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v4}, Lcom/android/incallui/recorder/CallRecorderTools;->-get4(Lcom/android/incallui/recorder/CallRecorderTools;)Lcom/android/incallui/recorder/ICallRecorderCommand;

    move-result-object v4

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v5}, Lcom/android/incallui/recorder/CallRecorderTools;->-get3(Lcom/android/incallui/recorder/CallRecorderTools;)Lcom/android/incallui/recorder/CallRecorderTools$RecorderCallback;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/android/incallui/recorder/ICallRecorderCommand;->registerCallback(Lcom/android/incallui/recorder/IRecorderCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v4}, Lcom/android/incallui/recorder/CallRecorderTools;->-get5(Lcom/android/incallui/recorder/CallRecorderTools;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v4}, Lcom/android/incallui/recorder/CallRecorderTools;->-get1(Lcom/android/incallui/recorder/CallRecorderTools;)Lcom/android/incallui/Call;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v4}, Lcom/android/incallui/recorder/CallRecorderTools;->-get1(Lcom/android/incallui/recorder/CallRecorderTools;)Lcom/android/incallui/Call;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    move-object v1, v3

    :goto_1
    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->getInstance()Lcom/android/incallui/ContactInfoCache;

    move-result-object v4

    iget-object v5, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v5}, Lcom/android/incallui/recorder/CallRecorderTools;->-get1(Lcom/android/incallui/recorder/CallRecorderTools;)Lcom/android/incallui/Call;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/incallui/ContactInfoCache;->getInfo(Ljava/lang/String;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->number:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v1, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v4, v7, v1, v3}, Lcom/android/incallui/recorder/CallRecorderTools;->-wrap0(Lcom/android/incallui/recorder/CallRecorderTools;ZLjava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v4, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    invoke-static {v4, v7}, Lcom/android/incallui/recorder/CallRecorderTools;->-set2(Lcom/android/incallui/recorder/CallRecorderTools;Z)Z

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :catch_0
    move-exception v2

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->-get0()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "mRecorderService.registerCallback RemoteException"

    invoke-static {v4, v5, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, ""

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->-get0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "RecorderService has disconnected"

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderTools$1;->this$0:Lcom/android/incallui/recorder/CallRecorderTools;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/incallui/recorder/CallRecorderTools;->-set0(Lcom/android/incallui/recorder/CallRecorderTools;Landroid/content/Intent;)Landroid/content/Intent;

    return-void
.end method
