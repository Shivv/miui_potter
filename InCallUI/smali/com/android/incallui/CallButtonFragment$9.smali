.class Lcom/android/incallui/CallButtonFragment$9;
.super Landroid/animation/AnimatorListenerAdapter;
.source "CallButtonFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/CallButtonFragment;->startCallButtonAnimator(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/CallButtonFragment;


# direct methods
.method constructor <init>(Lcom/android/incallui/CallButtonFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v1}, Lcom/android/incallui/CallButtonFragment;->-get0(Lcom/android/incallui/CallButtonFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v1}, Lcom/android/incallui/CallButtonFragment;->-get2(Lcom/android/incallui/CallButtonFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v1}, Lcom/android/incallui/CallButtonFragment;->-get4(Lcom/android/incallui/CallButtonFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallButtonPresenter;->isServiceNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallButtonPresenter;->isVideoCall()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallButtonPresenter;->isVideoCall()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v1}, Lcom/android/incallui/CallButtonFragment;->-get1(Lcom/android/incallui/CallButtonFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v1, v3}, Lcom/android/incallui/CallButtonFragment;->displayDialpad(Z)V

    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment$9;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v1, v3}, Lcom/android/incallui/CallButtonFragment;->displayToolPanel(Z)V

    goto :goto_0
.end method
