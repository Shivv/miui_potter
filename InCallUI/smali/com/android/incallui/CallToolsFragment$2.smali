.class Lcom/android/incallui/CallToolsFragment$2;
.super Lcom/android/incallui/view/OnEffectiveClickListener;
.source "CallToolsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/CallToolsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/CallToolsFragment;


# direct methods
.method constructor <init>(Lcom/android/incallui/CallToolsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-direct {p0}, Lcom/android/incallui/view/OnEffectiveClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onEffectiveClick(Landroid/view/View;)V
    .locals 7

    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onClick(View "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->isVideoCall()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->resetVideoAutoFullScreen()V

    :cond_0
    sparse-switch v0, :sswitch_data_0

    const-string/jumbo v1, "onClick: unexpected"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->onContactClick()V

    const-string/jumbo v1, "voice_call"

    const-string/jumbo v2, "voice_call_contact"

    invoke-static {v1, v2}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->onAddCallClick()V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_conference_add_call"

    aput-object v3, v2, v4

    const-string/jumbo v3, "video_call_voice_add_call"

    aput-object v3, v2, v5

    invoke-static {v1, v5, v4, v2}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->switchToVideoClicked()V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_voice_switch_to_video"

    aput-object v3, v2, v4

    invoke-static {v1, v4, v4, v2}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->switchToVoiceClicked()V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_voice_switch_to_voice"

    aput-object v3, v2, v4

    invoke-static {v1, v4, v4, v2}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    iget-object v2, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v2}, Lcom/android/incallui/CallToolsFragment;->-get4(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->isSelected()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/CallToolsPresenter;->switchCameraClicked(Z)V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v1}, Lcom/android/incallui/CallToolsFragment;->-get4(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_switch_camera"

    aput-object v3, v2, v4

    invoke-static {v1, v4, v4, v2}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    iget-object v2, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v2}, Lcom/android/incallui/CallToolsFragment;->-get2(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/CallToolsPresenter;->pauseVideoClicked(Z)V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_pause_video"

    aput-object v3, v2, v4

    invoke-static {v1, v4, v4, v2}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    iget-object v2, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v2}, Lcom/android/incallui/CallToolsFragment;->-get0(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/CallToolsPresenter;->onHoldClick(Z)V

    const-string/jumbo v1, "voice_call"

    const-string/jumbo v2, "voice_call_hold"

    invoke-static {v1, v2}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->onMergeClick()V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_voice_message"

    aput-object v3, v2, v4

    invoke-static {v1, v4, v4, v2}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    iget-object v2, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v2}, Lcom/android/incallui/CallToolsFragment;->-get1(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/CallToolsPresenter;->onMuteClick(Z)V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    new-array v2, v3, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_conference_mute"

    aput-object v3, v2, v4

    const-string/jumbo v3, "video_call_mute"

    aput-object v3, v2, v5

    const-string/jumbo v3, "video_call_voice_mute"

    aput-object v3, v2, v6

    invoke-static {v1, v5, v5, v2}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v1}, Lcom/android/incallui/CallToolsFragment;->-wrap0(Lcom/android/incallui/CallToolsFragment;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->onSwapClick()V

    const-string/jumbo v1, "voice_call"

    const-string/jumbo v2, "voice_call_swap"

    invoke-static {v1, v2}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->onNoteClick()V

    const-string/jumbo v1, "voice_call"

    const-string/jumbo v2, "voice_call_notes"

    invoke-static {v1, v2}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->onAddMessageClicked()V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment$2;->this$0:Lcom/android/incallui/CallToolsFragment;

    new-array v2, v3, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_conference_message"

    aput-object v3, v2, v4

    const-string/jumbo v3, "video_call_message"

    aput-object v3, v2, v5

    const-string/jumbo v3, "video_call_voice_message"

    aput-object v3, v2, v6

    invoke-static {v1, v5, v5, v2}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0a0048 -> :sswitch_8
        0x7f0a0049 -> :sswitch_6
        0x7f0a004a -> :sswitch_a
        0x7f0a004b -> :sswitch_b
        0x7f0a004c -> :sswitch_c
        0x7f0a004e -> :sswitch_0
        0x7f0a004f -> :sswitch_2
        0x7f0a0050 -> :sswitch_1
        0x7f0a0051 -> :sswitch_7
        0x7f0a0052 -> :sswitch_9
        0x7f0a00bd -> :sswitch_3
        0x7f0a00be -> :sswitch_5
        0x7f0a00bf -> :sswitch_4
        0x7f0a00c1 -> :sswitch_8
    .end sparse-switch
.end method
