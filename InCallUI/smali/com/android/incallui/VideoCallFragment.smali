.class public Lcom/android/incallui/VideoCallFragment;
.super Lcom/android/incallui/BaseFragment;
.source "VideoCallFragment.java"

# interfaces
.implements Lcom/android/incallui/VideoCallPresenter$VideoCallUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/VideoCallFragment$VideoCallSurface;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/VideoCallPresenter;",
        "Lcom/android/incallui/VideoCallPresenter$VideoCallUi;",
        ">;",
        "Lcom/android/incallui/VideoCallPresenter$VideoCallUi;"
    }
.end annotation


# static fields
.field private static sDisplaySize:Landroid/graphics/Point;

.field private static sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

.field private static sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

.field private static sVideoSurfacesInUse:Z


# instance fields
.field private mBackground:Landroid/widget/LinearLayout;

.field private mHasHoldCall:Z

.field private mIsActivityRestart:Z

.field private mIsLandscape:Z

.field private mIsLayoutComplete:Z

.field private mIsVideoCrbt:Z

.field private mMask:Landroid/view/View;

.field private mVideoIncoming:Landroid/view/TextureView;

.field private mVideoIncomingLayout:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

.field private mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

.field private mVideoViewLayout:Landroid/widget/FrameLayout;

.field private mVideoViews:Landroid/view/View;

.field private mVideoViewsStub:Landroid/view/ViewStub;

.field private mZoomControl:Lcom/android/incallui/ZoomControlBar;


# direct methods
.method static synthetic -set0(Lcom/android/incallui/VideoCallFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/VideoCallFragment;->mIsLayoutComplete:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/VideoCallFragment;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallFragment;->centerDisplayView(Landroid/view/View;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/incallui/VideoCallFragment;->sVideoSurfacesInUse:Z

    sput-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    sput-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    sput-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySize:Landroid/graphics/Point;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/VideoCallFragment;->mIsLayoutComplete:Z

    return-void
.end method

.method private centerDisplayView(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method private inflateVideoCallViews()V
    .locals 8

    const/4 v2, 0x1

    const v7, 0x7f0a00c7

    const v4, 0x7f0a00c5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "inflateVideoCallViews mVideoViews="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewsStub:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    new-instance v1, Lcom/android/incallui/VideoCallFragment$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/VideoCallFragment$2;-><init>(Lcom/android/incallui/VideoCallFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v1, 0x7f0a00c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/ZoomControlBar;

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/incallui/ZoomControlBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v1, 0x7f0a00c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mBackground:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v1, 0x7f0a00c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mMask:Landroid/view/View;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mMask:Landroid/view/View;

    new-instance v1, Lcom/android/incallui/VideoCallFragment$3;

    invoke-direct {v1, p0}, Lcom/android/incallui/VideoCallFragment$3;-><init>(Lcom/android/incallui/VideoCallFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v1, 0x7f0a00c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/VoLTEVideoPreview;

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoIncomingLayout:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoIncoming:Landroid/view/TextureView;

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "inflateVideoCallViews: sVideoSurfacesInUse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/incallui/VideoCallFragment;->sVideoSurfacesInUse:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sDisplaySize:Landroid/graphics/Point;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getScreenSize()Landroid/graphics/Point;

    move-result-object v6

    :goto_0
    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoIncoming:Landroid/view/TextureView;

    invoke-direct {p0, v0, v6}, Lcom/android/incallui/VideoCallFragment;->setSurfaceSizeAndTranslation(Landroid/view/TextureView;Landroid/graphics/Point;)V

    sget-boolean v0, Lcom/android/incallui/VideoCallFragment;->sVideoSurfacesInUse:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " inflateVideoCallViews screenSize"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/VideoCallPresenter;

    iget-object v3, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/TextureView;

    iget v4, v6, Landroid/graphics/Point;->x:I

    iget v5, v6, Landroid/graphics/Point;->y:I

    invoke-direct/range {v0 .. v5}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;-><init>(Lcom/android/incallui/VideoCallPresenter;ILandroid/view/TextureView;II)V

    sput-object v0, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    new-instance v3, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter;

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/TextureView;

    const/4 v4, 0x2

    invoke-direct {v3, v0, v4, v1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;-><init>(Lcom/android/incallui/VideoCallPresenter;ILandroid/view/TextureView;)V

    sput-object v3, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    sput-boolean v2, Lcom/android/incallui/VideoCallFragment;->sVideoSurfacesInUse:Z

    :cond_2
    :goto_1
    return-void

    :cond_3
    sget-object v6, Lcom/android/incallui/VideoCallFragment;->sDisplaySize:Landroid/graphics/Point;

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->recreateView(Landroid/view/TextureView;)V

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->recreateView(Landroid/view/TextureView;)V

    goto :goto_1
.end method

.method private inflateVideoUi(Z)V
    .locals 2

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/VideoCallFragment;->inflateVideoCallViews()V

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private onPresenterChanged(Lcom/android/incallui/VideoCallPresenter;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onPresenterChanged: Presenter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v0, p1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->resetPresenter(Lcom/android/incallui/VideoCallPresenter;)V

    :cond_0
    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v0, p1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->resetPresenter(Lcom/android/incallui/VideoCallPresenter;)V

    :cond_1
    return-void
.end method

.method private setSurfaceSizeAndTranslation(Landroid/view/TextureView;Landroid/graphics/Point;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p2, Landroid/graphics/Point;->x:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSurfaceSizeAndTranslation: Size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "IsLayoutComplete="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/incallui/VideoCallFragment;->mIsLayoutComplete:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "IsLandscape="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/incallui/VideoCallFragment;->mIsLandscape:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/android/incallui/VideoCallFragment;->mIsLayoutComplete:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/incallui/VideoCallFragment;->centerDisplayView(Landroid/view/View;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public changePreviewTopExtraHeight(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/VideoCallFragment;->mHasHoldCall:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/VoLTEVideoPreview;->changePreviewTopExtraHeight(Z)V

    iput-boolean p1, p0, Lcom/android/incallui/VideoCallFragment;->mHasHoldCall:Z

    :cond_1
    return-void
.end method

.method public cleanupSurfaces()V
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "cleanupSurfaces"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v0}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->setDoneWithSurface()V

    sput-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    :cond_0
    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v0}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->setDoneWithSurface()V

    sput-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    :cond_1
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/incallui/VideoCallFragment;->sVideoSurfacesInUse:Z

    return-void
.end method

.method public bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->createPresenter()Lcom/android/incallui/VideoCallPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/android/incallui/VideoCallPresenter;
    .locals 2

    const-string/jumbo v1, "createPresenter"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/android/incallui/VideoCallPresenter;

    invoke-direct {v0}, Lcom/android/incallui/VideoCallPresenter;-><init>()V

    invoke-direct {p0, v0}, Lcom/android/incallui/VideoCallFragment;->onPresenterChanged(Lcom/android/incallui/VideoCallPresenter;)V

    return-object v0
.end method

.method public enableZoomControl(Z)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    if-nez v0, :cond_0

    const-string/jumbo v0, "Zoom control is null. Can\'t reset zoom control"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    invoke-virtual {v0, p1}, Lcom/android/incallui/ZoomControlBar;->setEnabled(Z)V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    invoke-virtual {v0, v1}, Lcom/android/incallui/ZoomControlBar;->setZoomIndex(I)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    invoke-virtual {v0, v1}, Lcom/android/incallui/ZoomControlBar;->setZoomMax(I)V

    :cond_1
    return-void
.end method

.method public getDisplaySurfaceSize()Landroid/graphics/Point;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v0}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getSurfaceDimensions()Landroid/graphics/Point;

    move-result-object v0

    goto :goto_0
.end method

.method public getDisplayVideoAlpha()F
    .locals 4

    const/4 v1, 0x0

    sget-object v2, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v2}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getTextureView()Landroid/view/TextureView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/TextureView;->getAlpha()F

    move-result v1

    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getDisplayVideoAlpha: value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    return v1

    :cond_1
    const-string/jumbo v2, "Display Surface is null."

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDisplayVideoSurface()Landroid/view/Surface;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v0}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getSurface()Landroid/view/Surface;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreviewSurfaceSize()Landroid/graphics/Point;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v0}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getSurfaceDimensions()Landroid/graphics/Point;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreviewVideoSurface()Landroid/view/Surface;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v0}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getSurface()Landroid/view/Surface;

    move-result-object v0

    goto :goto_0
.end method

.method public getScreenSize()Landroid/graphics/Point;
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    return-object v1
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getUi()Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    move-result-object v0

    return-object v0
.end method

.method protected getUi()Lcom/android/incallui/VideoCallPresenter$VideoCallUi;
    .locals 0

    return-object p0
.end method

.method public hideVideoUi()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v3, 0x7f0a00c5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v3, 0x7f0a00c7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-direct {p0, v4}, Lcom/android/incallui/VideoCallFragment;->inflateVideoUi(Z)V

    invoke-virtual {p0, v4}, Lcom/android/incallui/VideoCallFragment;->showZoomControl(Z)V

    return-void
.end method

.method public initPreviewPosition()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    invoke-virtual {v0}, Lcom/android/incallui/view/VoLTEVideoPreview;->initPreviewPosition()V

    :cond_0
    return-void
.end method

.method public isDisplayVideoSurfaceCreated()Z
    .locals 3

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getSurface()Landroid/view/Surface;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " isDisplayVideoSurfaceCreated returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreviewVideoSurfaceCreated()Z
    .locals 3

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getSurface()Landroid/view/Surface;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " isPreviewVideoSurfaceCreated returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/incallui/VideoCallFragment;->mIsLandscape:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onActivityCreated: IsLandscape="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/incallui/VideoCallFragment;->mIsLandscape:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter;

    invoke-virtual {p0}, Lcom/android/incallui/VideoCallFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/VideoCallPresenter;->init(Landroid/content/Context;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    sget-boolean v0, Lcom/android/incallui/VideoCallFragment;->sVideoSurfacesInUse:Z

    iput-boolean v0, p0, Lcom/android/incallui/VideoCallFragment;->mIsActivityRestart:Z

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lcom/android/incallui/BaseFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    const v2, 0x7f030019

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/android/incallui/VideoCallFragment$1;

    invoke-direct {v2, p0, v1}, Lcom/android/incallui/VideoCallFragment$1;-><init>(Lcom/android/incallui/VideoCallFragment;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onDestroyView()V

    const-string/jumbo v0, "onDestroyView:"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onPause()V

    const-string/jumbo v0, "onPause:"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    if-nez v1, :cond_1

    :cond_0
    const-string/jumbo v0, "onSaveInstanceState: outState or zoom control is null"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    const-string/jumbo v1, "zoomIndex"

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    invoke-virtual {v2}, Lcom/android/incallui/ZoomControlBar;->getZoomIndex()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "maxZoom"

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    invoke-virtual {v2}, Lcom/android/incallui/ZoomControlBar;->getZoomMax()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "zoomVisibility"

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    invoke-virtual {v2}, Lcom/android/incallui/ZoomControlBar;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onSaveInstanceState: savedInstanceState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onStop()V

    const-string/jumbo v0, "onStop:"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/incallui/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onViewCreated: VideoSurfacesInUse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/incallui/VideoCallFragment;->sVideoSurfacesInUse:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0a00ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewsStub:Landroid/view/ViewStub;

    sget-boolean v0, Lcom/android/incallui/VideoCallFragment;->sVideoSurfacesInUse:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/VideoCallFragment;->inflateVideoCallViews()V

    :cond_0
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onViewStateRestored : savedInstanceState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, "onViewStateRestored: saved Instance or zoom control is null"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    const-string/jumbo v1, "maxZoom"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/ZoomControlBar;->setZoomMax(I)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    const-string/jumbo v1, "zoomIndex"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/ZoomControlBar;->setZoomIndex(I)V

    const-string/jumbo v0, "zoomVisibility"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/VideoCallFragment;->showZoomControl(Z)V

    return-void
.end method

.method public setDialPadIncomingPosition(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoIncomingLayout:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->setDialPadPreviewPosition(Z)V

    :cond_0
    return-void
.end method

.method public setDialPadPreviewPosition(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/VoLTEVideoPreview;->setDialPadPreviewPosition(Z)V

    :cond_0
    return-void
.end method

.method public setDisplayRotation(I)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDisplayRotation: orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getTextureView()Landroid/view/TextureView;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setRotation(F)V

    :cond_1
    return-void
.end method

.method public setDisplaySurfaceSize(II)V
    .locals 3

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDisplaySurfaceSize: width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " isDisplaySurfaceAvailable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v1, p1, p2}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->setSurfaceDimensions(II)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDisplayVideoAlpha(F)V
    .locals 3

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDisplayVideoAlpha: value="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getTextureView()Landroid/view/TextureView;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v1, "Display Video texture view is null."

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/TextureView;->setAlpha(F)V

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "Display Surface is null."

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDisplayVideoSize(II)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDisplayVideoSize: width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getTextureView()Landroid/view/TextureView;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v1, "Display Video texture view is null. Bail out"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySize:Landroid/graphics/Point;

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sDisplaySize:Landroid/graphics/Point;

    invoke-direct {p0, v0, v1}, Lcom/android/incallui/VideoCallFragment;->setSurfaceSizeAndTranslation(Landroid/view/TextureView;Landroid/graphics/Point;)V

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "Display Video Surface is null. Bail out"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setFullScreenMode(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/VoLTEVideoPreview;->setFullScreenMode(Z)V

    iget-boolean v0, p0, Lcom/android/incallui/VideoCallFragment;->mHasHoldCall:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    iget-boolean v1, p0, Lcom/android/incallui/VideoCallFragment;->mHasHoldCall:Z

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->changePreviewTopExtraHeight(Z)V

    :cond_0
    return-void
.end method

.method public setFullScreenPreviewPosition()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    invoke-virtual {v0}, Lcom/android/incallui/view/VoLTEVideoPreview;->setFullScreenPreviewPosition()V

    :cond_0
    return-void
.end method

.method public setPreviewGravity(I)V
    .locals 4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setPreviewGravity: gravity="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v2}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getTextureView()Landroid/view/TextureView;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method public setPreviewMargin(IIII)V
    .locals 4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setPreviewMargin: left="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " top="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " right="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " bottom="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v2}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getTextureView()Landroid/view/TextureView;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->setAlpha(F)V

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method public setPreviewOnTouchListener(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/VoLTEVideoPreview;->setOnTouchListener(Z)V

    :cond_0
    return-void
.end method

.method public setPreviewRotation(I)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPreviewRotation: orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v1}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getTextureView()Landroid/view/TextureView;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    rsub-int v1, p1, 0x168

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setRotation(F)V

    :cond_1
    return-void
.end method

.method public setPreviewSize(II)V
    .locals 4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setPreviewSize: width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v2}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->getTextureView()Landroid/view/TextureView;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method public setPreviewSurfaceSize(II)V
    .locals 3

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPreviewSurfaceSize: width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " isPreviewSurfaceAvailable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    sget-object v1, Lcom/android/incallui/VideoCallFragment;->sPreviewSurface:Lcom/android/incallui/VideoCallFragment$VideoCallSurface;

    invoke-virtual {v1, p1, p2}, Lcom/android/incallui/VideoCallFragment$VideoCallSurface;->setSurfaceDimensions(II)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showBackground(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mBackground:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mBackground:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showMask(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mMask:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mMask:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showPreviewVideo(Z)V
    .locals 3

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v2, 0x7f0a00c7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public showVideoBidrectionalUi(Z)V
    .locals 6

    const/16 v3, 0x8

    const/4 v4, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/incallui/VideoCallFragment;->inflateVideoUi(Z)V

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v5, 0x7f0a00c5

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v5, 0x7f0a00c7

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_2

    move v2, v3

    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v1, :cond_1

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    return-void

    :cond_2
    move v2, v4

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public showVideoReceptionUi()V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/incallui/VideoCallFragment;->inflateVideoUi(Z)V

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v3, 0x7f0a00c5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v3, 0x7f0a00c7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v1, :cond_1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0, v4}, Lcom/android/incallui/VideoCallFragment;->showZoomControl(Z)V

    return-void
.end method

.method public showVideoTransmissionUi()V
    .locals 4

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/incallui/VideoCallFragment;->inflateVideoUi(Z)V

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v3, 0x7f0a00c5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViews:Landroid/view/View;

    const v3, 0x7f0a00c7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v1, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    return-void
.end method

.method public showZoomControl(Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Show zoom control = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    if-nez v0, :cond_0

    const-string/jumbo v0, "Zoom control null. Can\'t show zoom control"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mZoomControl:Lcom/android/incallui/ZoomControlBar;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/incallui/ZoomControlBar;->setVisibility(I)V

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateVideoTextureViews(ZLjava/lang/String;)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/incallui/VideoCallFragment;->mIsVideoCrbt:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoIncomingLayout:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoIncomingLayout:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->initIncomingPosition(ZLjava/lang/String;)V

    iput-boolean p1, p0, Lcom/android/incallui/VideoCallFragment;->mIsVideoCrbt:Z

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoIncomingLayout:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/incallui/VideoCallFragment;->mVideoViewLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/incallui/VideoCallFragment;->mVideoPreview:Lcom/android/incallui/view/VoLTEVideoPreview;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
