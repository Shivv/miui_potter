.class public Lcom/android/incallui/ZoomControlBar;
.super Lcom/android/incallui/ZoomControl;
.source "ZoomControlBar.java"


# instance fields
.field private mBar:Landroid/view/View;

.field private mIconWidth:I

.field private mSliderLength:I

.field private mSliderPosition:I

.field private mStartChanging:Z

.field private mTotalIconWidth:I

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/ZoomControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/incallui/ZoomControlBar;->mSliderPosition:I

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/incallui/ZoomControlBar;->mBar:Landroid/view/View;

    iget-object v0, p0, Lcom/android/incallui/ZoomControlBar;->mBar:Landroid/view/View;

    const v1, 0x7f0200b8

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/incallui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/incallui/ZoomControlBar;->addView(Landroid/view/View;)V

    return-void
.end method

.method private getSliderPosition(I)I
    .locals 3

    iget v1, p0, Lcom/android/incallui/ZoomControlBar;->mOrientation:I

    const/16 v2, 0x5a

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    iget v2, p0, Lcom/android/incallui/ZoomControlBar;->mTotalIconWidth:I

    sub-int/2addr v1, v2

    sub-int v0, v1, p1

    :goto_0
    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget v1, p0, Lcom/android/incallui/ZoomControlBar;->mSliderLength:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/android/incallui/ZoomControlBar;->mSliderLength:I

    :cond_1
    return v0

    :cond_2
    iget v1, p0, Lcom/android/incallui/ZoomControlBar;->mTotalIconWidth:I

    sub-int v0, p1, v1

    goto :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v8, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/ZoomControlBar;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    if-nez v3, :cond_1

    :cond_0
    return v4

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v8

    :pswitch_0
    invoke-virtual {p0, v4}, Lcom/android/incallui/ZoomControlBar;->setActivated(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/ZoomControlBar;->closeZoomControl()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v8}, Lcom/android/incallui/ZoomControlBar;->setActivated(Z)V

    iput-boolean v4, p0, Lcom/android/incallui/ZoomControlBar;->mStartChanging:Z

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {p0, v3}, Lcom/android/incallui/ZoomControlBar;->getSliderPosition(I)I

    move-result v2

    iget-boolean v3, p0, Lcom/android/incallui/ZoomControlBar;->mStartChanging:Z

    if-nez v3, :cond_3

    iget v3, p0, Lcom/android/incallui/ZoomControlBar;->mSliderPosition:I

    sub-int v1, v3, v2

    const/16 v3, 0xa

    if-gt v1, v3, :cond_2

    const/16 v3, -0xa

    if-ge v1, v3, :cond_3

    :cond_2
    iput-boolean v8, p0, Lcom/android/incallui/ZoomControlBar;->mStartChanging:Z

    :cond_3
    iget-boolean v3, p0, Lcom/android/incallui/ZoomControlBar;->mStartChanging:Z

    if-eqz v3, :cond_4

    int-to-double v4, v2

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v6

    iget v3, p0, Lcom/android/incallui/ZoomControlBar;->mSliderLength:I

    int-to-double v6, v3

    div-double/2addr v4, v6

    invoke-virtual {p0, v4, v5}, Lcom/android/incallui/ZoomControlBar;->performZoom(D)V

    iput v2, p0, Lcom/android/incallui/ZoomControlBar;->mSliderPosition:I

    :cond_4
    invoke-virtual {p0}, Lcom/android/incallui/ZoomControlBar;->requestLayout()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    const/4 v8, 0x0

    iget v4, p0, Lcom/android/incallui/ZoomControlBar;->mZoomMax:I

    if-nez v4, :cond_0

    return-void

    :cond_0
    sub-int v0, p5, p3

    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mBar:Landroid/view/View;

    iget v5, p0, Lcom/android/incallui/ZoomControlBar;->mTotalIconWidth:I

    iget v6, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    iget v7, p0, Lcom/android/incallui/ZoomControlBar;->mTotalIconWidth:I

    sub-int/2addr v6, v7

    invoke-virtual {v4, v5, v8, v6, v0}, Landroid/view/View;->layout(IIII)V

    iget v4, p0, Lcom/android/incallui/ZoomControlBar;->mSliderPosition:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    iget v2, p0, Lcom/android/incallui/ZoomControlBar;->mSliderPosition:I

    :goto_0
    iget v4, p0, Lcom/android/incallui/ZoomControlBar;->mOrientation:I

    const/16 v5, 0x5a

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mZoomIn:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/incallui/ZoomControlBar;->mIconWidth:I

    invoke-virtual {v4, v8, v8, v5, v0}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mZoomOut:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    iget v6, p0, Lcom/android/incallui/ZoomControlBar;->mIconWidth:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    invoke-virtual {v4, v5, v8, v6, v0}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    sub-int v1, v4, v2

    :goto_1
    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mZoomSlider:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mZoomSlider:Landroid/widget/ImageView;

    div-int/lit8 v5, v3, 0x2

    sub-int v5, v1, v5

    div-int/lit8 v6, v3, 0x2

    add-int/2addr v6, v1

    invoke-virtual {v4, v5, v8, v6, v0}, Landroid/widget/ImageView;->layout(IIII)V

    return-void

    :cond_1
    iget v4, p0, Lcom/android/incallui/ZoomControlBar;->mSliderLength:I

    int-to-double v4, v4

    iget v6, p0, Lcom/android/incallui/ZoomControlBar;->mZoomIndex:I

    int-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p0, Lcom/android/incallui/ZoomControlBar;->mZoomMax:I

    int-to-double v6, v6

    div-double/2addr v4, v6

    double-to-int v2, v4

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mZoomOut:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/incallui/ZoomControlBar;->mIconWidth:I

    invoke-virtual {v4, v8, v8, v5, v0}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mZoomIn:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    iget v6, p0, Lcom/android/incallui/ZoomControlBar;->mIconWidth:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    invoke-virtual {v4, v5, v8, v6, v0}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v4, p0, Lcom/android/incallui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    add-int v1, v4, v2

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    iput p1, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    iget-object v0, p0, Lcom/android/incallui/ZoomControlBar;->mZoomIn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/android/incallui/ZoomControlBar;->mIconWidth:I

    iget v0, p0, Lcom/android/incallui/ZoomControlBar;->mIconWidth:I

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lcom/android/incallui/ZoomControlBar;->mTotalIconWidth:I

    iget v0, p0, Lcom/android/incallui/ZoomControlBar;->mWidth:I

    iget v1, p0, Lcom/android/incallui/ZoomControlBar;->mTotalIconWidth:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/incallui/ZoomControlBar;->mSliderLength:I

    return-void
.end method

.method public setActivated(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/ZoomControl;->setActivated(Z)V

    iget-object v0, p0, Lcom/android/incallui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setActivated(Z)V

    return-void
.end method

.method public setZoomIndex(I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/ZoomControl;->setZoomIndex(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/incallui/ZoomControlBar;->mSliderPosition:I

    invoke-virtual {p0}, Lcom/android/incallui/ZoomControlBar;->requestLayout()V

    return-void
.end method
