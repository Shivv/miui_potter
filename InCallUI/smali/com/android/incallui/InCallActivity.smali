.class public Lcom/android/incallui/InCallActivity;
.super Landroid/app/Activity;
.source "InCallActivity.java"

# interfaces
.implements Landroid/support/v4/app/ActivityCompat$OnRequestPermissionsResultCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/InCallActivity$1;,
        Lcom/android/incallui/InCallActivity$MainHandler;
    }
.end annotation


# instance fields
.field private mAlphaToFinish:Landroid/animation/ObjectAnimator;

.field mAnswerFragment:Lcom/android/incallui/AnswerFragment;

.field private mButtonContainer:Landroid/view/View;

.field mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

.field mCallCardFragment:Lcom/android/incallui/CallCardFragment;

.field private mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

.field private mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mDialog:Lcom/android/incallui/view/InCallDialog;

.field private mDialpadFragment:Lcom/android/incallui/DialpadFragment;

.field private mEmergencyWaitingDialog:Lmiui/app/ProgressDialog;

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private mHandler:Lcom/android/incallui/BaseUIHandler;

.field private mInCallOrientationEventListener:Lcom/android/incallui/InCallOrientationEventListener;

.field private mIsForegroundActivity:Z

.field private mIsNeedFinish:Z

.field private mIsPausing:Z

.field private mNeedResumeUnlocked:Z

.field private mNormalCallContainer:Landroid/view/View;

.field private mShowDialpadRequested:Z

.field private mShowPostCharWaitDialogCallId:Ljava/lang/String;

.field private mShowPostCharWaitDialogChars:Ljava/lang/String;

.field private mShowPostCharWaitDialogOnResume:Z

.field private mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

.field private mVideoUpgradeDialog:Lcom/android/incallui/view/InCallDialog;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/InCallActivity;)Landroid/animation/ObjectAnimator;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mAlphaToFinish:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/InCallActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mNormalCallContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/InCallActivity;Lcom/android/incallui/view/SimCardPickDialog;)Lcom/android/incallui/view/SimCardPickDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/InCallActivity;->mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/InCallActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->initDelayOnCreate()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/incallui/InCallActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->initDelayOnStart()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mIsNeedFinish:Z

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mIsPausing:Z

    new-instance v0, Lcom/android/incallui/InCallActivity$1;

    invoke-static {}, Lcom/android/incallui/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/incallui/InCallActivity$1;-><init>(Lcom/android/incallui/InCallActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mContentObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/incallui/InCallActivity$MainHandler;

    invoke-direct {v0, p0}, Lcom/android/incallui/InCallActivity$MainHandler;-><init>(Lcom/android/incallui/InCallActivity;)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mHandler:Lcom/android/incallui/BaseUIHandler;

    return-void
.end method

.method private checkToFinish()Z
    .locals 6

    const/4 v5, 0x0

    iget-boolean v2, p0, Lcom/android/incallui/InCallActivity;->mIsNeedFinish:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->isFinishing()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/InCallPresenter;->getCallList()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/InCallPresenter;->getPotentialStateFromCallList(Lcom/android/incallui/CallList;)Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v1

    const-string/jumbo v2, "InCallActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Check to finish. InCallState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->finish()V

    const/4 v2, 0x1

    return v2

    :cond_0
    iput-boolean v5, p0, Lcom/android/incallui/InCallActivity;->mIsNeedFinish:Z

    return v5
.end method

.method private createNewFragmentForTag(Ljava/lang/String;)Landroid/app/Fragment;
    .locals 3

    const-string/jumbo v0, "tag_call_card_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/incallui/CallCardFragment;

    invoke-direct {v0}, Lcom/android/incallui/CallCardFragment;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    return-object v0

    :cond_0
    const-string/jumbo v0, "tag_call_button_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/incallui/CallButtonFragment;

    invoke-direct {v0}, Lcom/android/incallui/CallButtonFragment;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    return-object v0

    :cond_1
    const-string/jumbo v0, "tag_call_dialpad_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/android/incallui/DialpadFragment;

    invoke-direct {v0}, Lcom/android/incallui/DialpadFragment;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    return-object v0

    :cond_2
    const-string/jumbo v0, "tag_call_tools_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/android/incallui/CallToolsFragment;

    invoke-direct {v0}, Lcom/android/incallui/CallToolsFragment;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    return-object v0

    :cond_3
    const-string/jumbo v0, "tag_conference_manager_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/android/incallui/ConferenceManagerFragment;

    invoke-direct {v0}, Lcom/android/incallui/ConferenceManagerFragment;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    return-object v0

    :cond_4
    const-string/jumbo v0, "tag_answer_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/android/incallui/AnswerFragment;

    invoke-direct {v0}, Lcom/android/incallui/AnswerFragment;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mAnswerFragment:Lcom/android/incallui/AnswerFragment;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mAnswerFragment:Lcom/android/incallui/AnswerFragment;

    return-object v0

    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected fragment: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private fakeFinish()V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->moveTaskToBack(Z)Z

    const/4 v0, 0x0

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lcom/android/incallui/InCallActivity;->overridePendingTransition(II)V

    return-void
.end method

.method private getContainerIdForFragment(Ljava/lang/String;)I
    .locals 3

    const v1, 0x7f0a0092

    const-string/jumbo v0, "tag_call_card_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0091

    return v0

    :cond_0
    const-string/jumbo v0, "tag_conference_manager_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0093

    return v0

    :cond_1
    const-string/jumbo v0, "tag_call_tools_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    return v1

    :cond_3
    const-string/jumbo v0, "tag_call_button_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "tag_answer_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "tag_call_dialpad_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    return v1

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected fragment: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getFragmentManagerForTag(Ljava/lang/String;)Landroid/app/FragmentManager;
    .locals 3

    const-string/jumbo v0, "tag_call_card_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mFragmentManager:Landroid/app/FragmentManager;

    return-object v0

    :cond_1
    const-string/jumbo v0, "tag_call_button_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tag_call_dialpad_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tag_call_tools_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tag_conference_manager_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tag_answer_fragment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected fragment: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private handleDialerKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "handleDialerKeyDown: keyCode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->isDialpadVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0, p2}, Lcom/android/incallui/DialpadFragment;->onDialerKeyDown(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private hasPendingErrorDialog()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initDelayOnCreate()V
    .locals 3

    const/4 v2, 0x1

    const-string/jumbo v0, "ICA.iDOC"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/incallui/InCallActivity;->internalResolveIntent(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x300

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    const-string/jumbo v0, "tag_conference_manager_fragment"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/android/incallui/InCallActivity;->showFragment(Ljava/lang/String;ZZZ)V

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void
.end method

.method private initDelayOnStart()V
    .locals 4

    const-string/jumbo v0, "ICA.iDOS"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/util/SmartCoverUtil;->supportSmartCover()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "is_small_window"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void
.end method

.method private initializeInCall()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mFragmentManager:Landroid/app/FragmentManager;

    const v0, 0x7f0a0090

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mNormalCallContainer:Landroid/view/View;

    const v0, 0x7f0a0092

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mButtonContainer:Landroid/view/View;

    return-void
.end method

.method private internalResolveIntent(Landroid/content/Intent;)V
    .locals 12

    const/4 v10, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v8, "android.intent.action.MAIN"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string/jumbo v8, "InCallActivity.show_dialpad"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string/jumbo v8, "InCallActivity.show_dialpad"

    invoke-virtual {p1, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "- internalResolveIntent: SHOW_DIALPAD_EXTRA: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/android/incallui/InCallActivity;->relaunchedFromDialer(Z)V

    :cond_0
    const-string/jumbo v8, "InCallActivity.new_outgoing_call"

    invoke-virtual {p1, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string/jumbo v8, "InCallActivity.new_outgoing_call"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/incallui/CallList;->getOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/incallui/CallList;->getPendingOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/incallui/Call;->isEccTurnOnRadioCall()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/incallui/InCallActivity;->mEmergencyWaitingDialog:Lmiui/app/ProgressDialog;

    if-nez v8, :cond_1

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->showEmergencyCallWaitingDialog()V

    :cond_1
    const-string/jumbo v8, "InCallActivity.from_incallui"

    invoke-virtual {p1, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v8

    if-eqz v8, :cond_2

    const-string/jumbo v8, "SystemUI.answer"

    invoke-virtual {p1, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    xor-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/android/incallui/InCallPresenter;->forceProcessIncoming(Z)V

    :cond_2
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/incallui/CallList;->getWaitingForAccountCall()Lcom/android/incallui/Call;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {v3}, Lcom/android/incallui/CallAdapterUtils;->getOriginalPlaceIntentExtras(Lcom/android/incallui/Call;)Landroid/os/Bundle;

    move-result-object v2

    const-wide/16 v4, -0x1

    if-eqz v2, :cond_4

    const-string/jumbo v8, "selectPhoneAccountAccounts"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    const-string/jumbo v8, "com.android.phone.ORIGINAL_SIM_ID"

    const-wide/16 v10, -0x1

    invoke-virtual {v2, v8, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    :goto_0
    invoke-direct {p0, v6, v4, v5}, Lcom/android/incallui/InCallActivity;->launchMSimDialer(Ljava/util/List;J)V

    :cond_3
    return-void

    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method private launchMSimDialer(Ljava/util/List;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;J)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/SimCardPickDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/incallui/view/SimCardPickDialog;

    invoke-direct {v0, p0, p2, p3}, Lcom/android/incallui/view/SimCardPickDialog;-><init>(Lcom/android/incallui/InCallActivity;J)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

    new-instance v1, Lcom/android/incallui/InCallActivity$4;

    invoke-direct {v1, p0}, Lcom/android/incallui/InCallActivity$4;-><init>(Lcom/android/incallui/InCallActivity;)V

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SimCardPickDialog;->setOnSimCardPickCallBack(Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/SimCardPickDialog;->showSimCardPickDialog(Ljava/util/List;)V

    return-void
.end method

.method private maybeDismissSimPickDialog()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mSimCardPickDialog:Lcom/android/incallui/view/SimCardPickDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/SimCardPickDialog;->maybeCancelDialog()V

    :cond_0
    return-void
.end method

.method private relaunchedFromDialer(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/incallui/InCallActivity;->mShowDialpadRequested:Z

    iget-boolean v1, p0, Lcom/android/incallui/InCallActivity;->mShowDialpadRequested:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getActiveOrBackgroundCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/TelecomAdapter;->unholdCall(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private reuseFragmentForTag(Landroid/app/Fragment;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "tag_call_card_fragment"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p1, Lcom/android/incallui/CallCardFragment;

    iput-object p1, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "tag_call_button_fragment"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast p1, Lcom/android/incallui/CallButtonFragment;

    iput-object p1, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "tag_call_dialpad_fragment"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    check-cast p1, Lcom/android/incallui/DialpadFragment;

    iput-object p1, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "tag_call_tools_fragment"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    check-cast p1, Lcom/android/incallui/CallToolsFragment;

    iput-object p1, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "tag_conference_manager_fragment"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    check-cast p1, Lcom/android/incallui/ConferenceManagerFragment;

    iput-object p1, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    goto :goto_0

    :cond_5
    const-string/jumbo v0, "tag_answer_fragment"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/incallui/AnswerFragment;

    iput-object p1, p0, Lcom/android/incallui/InCallActivity;->mAnswerFragment:Lcom/android/incallui/AnswerFragment;

    goto :goto_0
.end method

.method private showEmergencyCallWaitingDialog()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->dismissPendingDialogs()V

    const-string/jumbo v0, ""

    const v1, 0x7f0b0077

    invoke-virtual {p0, v1}, Lcom/android/incallui/InCallActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lmiui/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lmiui/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mEmergencyWaitingDialog:Lmiui/app/ProgressDialog;

    return-void
.end method

.method private showFragment(Ljava/lang/String;ZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/incallui/InCallActivity;->showFragment(Ljava/lang/String;ZZZ)V

    return-void
.end method

.method private showFragment(Ljava/lang/String;ZZZ)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/android/incallui/InCallActivity;->getFragmentManagerForTag(Ljava/lang/String;)Landroid/app/FragmentManager;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Fragment manager is null for : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Activity is finishing or fm is destroy for :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez p2, :cond_3

    if-nez v1, :cond_3

    return-void

    :cond_3
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    if-eqz p2, :cond_6

    if-nez v1, :cond_5

    invoke-direct {p0, p1}, Lcom/android/incallui/InCallActivity;->createNewFragmentForTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/android/incallui/InCallActivity;->getContainerIdForFragment(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3, v1, p1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :goto_0
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    if-eqz p3, :cond_4

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    :cond_4
    return-void

    :cond_5
    invoke-direct {p0, v1, p1}, Lcom/android/incallui/InCallActivity;->reuseFragmentForTag(Landroid/app/Fragment;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    :cond_6
    if-eqz p4, :cond_7

    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    :cond_7
    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method


# virtual methods
.method public animationToFinish()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string/jumbo v3, "InCallActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "animationToFinish().  Dialog showing: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->dismissVideoUpgradeDialogs()V

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->hasPendingErrorDialog()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mAlphaToFinish:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mNormalCallContainer:Landroid/view/View;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v4, v2

    const/4 v2, 0x0

    aput v2, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mAlphaToFinish:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mAlphaToFinish:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mAlphaToFinish:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mHandler:Lcom/android/incallui/BaseUIHandler;

    new-instance v1, Lcom/android/incallui/InCallActivity$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/InCallActivity$2;-><init>(Lcom/android/incallui/InCallActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/BaseUIHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public clearResources()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->updateIsChangingConfigurations()V

    invoke-static {}, Lcom/android/incallui/util/PermissionGrantHelper;->release()V

    return-void
.end method

.method public closeAllPanel()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0, v1}, Lcom/android/incallui/CallButtonFragment;->displayDialpad(Z)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0, v1}, Lcom/android/incallui/CallButtonFragment;->displayToolPanel(Z)V

    :cond_0
    return-void
.end method

.method public dismissEmergencyPendingDialogs()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mEmergencyWaitingDialog:Lmiui/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mEmergencyWaitingDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/incallui/InCallActivity;->mEmergencyWaitingDialog:Lmiui/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method public dismissKeyguard(Z)V
    .locals 2

    const/high16 v1, 0x400000

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method public dismissPendingDialogs()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/InCallDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->maybeDismissSimPickDialog()V

    return-void
.end method

.method public dismissVideoUpgradeDialogs()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mVideoUpgradeDialog:Lcom/android/incallui/view/InCallDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mVideoUpgradeDialog:Lcom/android/incallui/view/InCallDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/InCallDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/incallui/InCallActivity;->mVideoUpgradeDialog:Lcom/android/incallui/view/InCallDialog;

    :cond_0
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    invoke-virtual {v0, p1}, Lcom/android/incallui/CallCardFragment;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    if-eqz v0, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const-string/jumbo v1, "InCallActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "dispatchTouchEvent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method public displayCallButton()V
    .locals 3

    const-string/jumbo v0, "ICA.dCB"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    if-nez v0, :cond_0

    const-string/jumbo v0, "tag_call_button_fragment"

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/incallui/InCallActivity;->showFragment(Ljava/lang/String;ZZ)V

    :cond_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void
.end method

.method public displayCallCard()V
    .locals 3

    const-string/jumbo v0, "ICA.dCC"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    if-nez v0, :cond_0

    const-string/jumbo v0, "tag_call_card_fragment"

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/incallui/InCallActivity;->showFragment(Ljava/lang/String;ZZ)V

    :cond_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void
.end method

.method public displayDialpad(ZZ)V
    .locals 3

    const-string/jumbo v0, "ICA.dD"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/incallui/DialpadFragment;->showDialer()V

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/incallui/InCallPresenter;->notifyDialPadVisibleChange(Z)V

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/incallui/DialpadFragment;->hideDialer()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0, p1}, Lcom/android/incallui/DialpadFragment;->setVisible(Z)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_0

    const-string/jumbo v0, "tag_call_dialpad_fragment"

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/incallui/InCallActivity;->showFragment(Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0, p2}, Lcom/android/incallui/DialpadFragment;->setNeedLaunchShow(Z)V

    goto :goto_0
.end method

.method public displayToolPanel(ZZ)V
    .locals 3

    const-string/jumbo v0, "ICA.dTP"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->showToolPanel()V

    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->hideToolPanel()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0, p1}, Lcom/android/incallui/CallToolsFragment;->setVisible(Z)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_0

    const-string/jumbo v0, "tag_call_tools_fragment"

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/incallui/InCallActivity;->showFragment(Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0, p2}, Lcom/android/incallui/CallToolsFragment;->setNeedLaunchShow(Z)V

    goto :goto_0
.end method

.method public enableInCallOrientationEventListener(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mInCallOrientationEventListener:Lcom/android/incallui/InCallOrientationEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/incallui/InCallOrientationEventListener;

    invoke-direct {v0, p0}, Lcom/android/incallui/InCallOrientationEventListener;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mInCallOrientationEventListener:Lcom/android/incallui/InCallOrientationEventListener;

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mInCallOrientationEventListener:Lcom/android/incallui/InCallOrientationEventListener;

    invoke-virtual {v0, p1}, Lcom/android/incallui/InCallOrientationEventListener;->enable(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mInCallOrientationEventListener:Lcom/android/incallui/InCallOrientationEventListener;

    invoke-virtual {v0}, Lcom/android/incallui/InCallOrientationEventListener;->disable()V

    goto :goto_0
.end method

.method public finish()V
    .locals 2

    const-string/jumbo v0, "finish activity"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mIsPausing:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mIsNeedFinish:Z

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "Finish activity when pause, return ..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mIsForegroundActivity:Z

    if-nez v0, :cond_1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->unsetActivity(Lcom/android/incallui/InCallActivity;)V

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x0

    const v1, 0x7f040002

    invoke-virtual {p0, v0, v1}, Lcom/android/incallui/InCallActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public getAnswerFragment()Lcom/android/incallui/AnswerFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mAnswerFragment:Lcom/android/incallui/AnswerFragment;

    return-object v0
.end method

.method public getCallButtonFragment()Lcom/android/incallui/CallButtonFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    return-object v0
.end method

.method public getCallCardFragment()Lcom/android/incallui/CallCardFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    return-object v0
.end method

.method public getCallToolsFragment()Lcom/android/incallui/CallToolsFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    return-object v0
.end method

.method public getDialPadFragment()Lcom/android/incallui/DialpadFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    return-object v0
.end method

.method public getDialpadHideTime()I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/incallui/DialpadFragment;->getDialpadHideTime()I

    move-result v0

    return v0
.end method

.method public getToolPanelHideTime()I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->getToolPanelHideTime()I

    move-result v0

    return v0
.end method

.method public hideAndDismissKeyguard()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mNeedResumeUnlocked:Z

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->fakeFinish()V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mHandler:Lcom/android/incallui/BaseUIHandler;

    new-instance v1, Lcom/android/incallui/InCallActivity$3;

    invoke-direct {v1, p0}, Lcom/android/incallui/InCallActivity$3;-><init>(Lcom/android/incallui/InCallActivity;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/BaseUIHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public isDialpadVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/incallui/DialpadFragment;->isDialpadVisible()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isForegroundActivity()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mIsForegroundActivity:Z

    return v0
.end method

.method public isNeedResumeUnlocked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mNeedResumeUnlocked:Z

    return v0
.end method

.method public isToolPanelVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallToolsFragment:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->isToolPanelVisible()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maybeShowEmergencyCallWaitingDialog()V
    .locals 2

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getPendingOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isEccTurnOnRadioCall()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->mEmergencyWaitingDialog:Lmiui/app/ProgressDialog;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->showEmergencyCallWaitingDialog()V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    const-string/jumbo v1, "onBackPressed()..."

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    invoke-virtual {v1}, Lcom/android/incallui/ConferenceManagerFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/incallui/InCallActivity;->showConferenceFragment(Z)V

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getIncomingCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v1, "Consume Back press for an incoming call"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->fakeFinish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getProximitySensor()Lcom/android/incallui/ProximitySensor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/incallui/ProximitySensor;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onConfigurationChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const-string/jumbo v1, "InCallActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "InCallActivity onCreate()...  this = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "InCallActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "InCallActivity onCreate()...  this = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->si(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x288000

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addExtraFlags(I)V

    const v1, 0x7f030011

    invoke-virtual {p0, v1}, Lcom/android/incallui/InCallActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->initializeInCall()V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/incallui/InCallActivity;->setIntent(Landroid/content/Intent;)V

    if-eqz p1, :cond_0

    const-string/jumbo v1, "InCallActivity.show_dialpad"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/incallui/InCallActivity;->mShowDialpadRequested:Z

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->mHandler:Lcom/android/incallui/BaseUIHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/incallui/BaseUIHandler;->sendEmptyMessage(I)Z

    const-string/jumbo v1, "onCreate(): exit"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    const-string/jumbo v0, "InCallActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDestroy()...  this = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "InCallActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDestroy()...  this = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->si(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->unsetActivity(Lcom/android/incallui/InCallActivity;)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->clearResources()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onDialogDismissed()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6

    const/4 v5, 0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/InCallActivity;->handleDialerKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_2

    return v5

    :sswitch_1
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/InCallPresenter;->handleCallKey()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v3, "InCallActivity should always handle KEYCODE_CALL in onKeyDown"

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    return v5

    :sswitch_2
    return v5

    :sswitch_3
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v3

    invoke-static {}, Lcom/android/incallui/AudioModeProvider;->getInstance()Lcom/android/incallui/AudioModeProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/AudioModeProvider;->getMute()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/incallui/TelecomAdapter;->mute(Z)V

    return v5

    :sswitch_4
    sget-boolean v3, Lcom/android/incallui/Log;->VERBOSE:Z

    if-eqz v3, :cond_0

    const-string/jumbo v3, "----------- InCallActivity View dump --------------"

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "View dump:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return v5

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    return v3

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1b -> :sswitch_2
        0x46 -> :sswitch_0
        0x4c -> :sswitch_4
        0x5b -> :sswitch_3
        0xa4 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->isDialpadVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0, p2}, Lcom/android/incallui/DialpadFragment;->onDialerKeyUp(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    return v1

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    const-string/jumbo v0, "InCallActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewIntent: intent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "InCallActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNewIntent: intent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->si(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/android/incallui/InCallActivity;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/android/incallui/InCallActivity;->internalResolveIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->onBackPressed()V

    const/4 v1, 0x1

    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method protected onPause()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "InCallActivity onPause()..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "InCallActivity onPause()..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->si(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/android/incallui/InCallActivity;->mIsForegroundActivity:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mIsPausing:Z

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialpadFragment:Lcom/android/incallui/DialpadFragment;

    invoke-virtual {v0, v3}, Lcom/android/incallui/DialpadFragment;->onDialerKeyUp(Landroid/view/KeyEvent;)Z

    :cond_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/incallui/InCallPresenter;->onUiShowing(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->unsetActivity(Lcom/android/incallui/InCallActivity;)V

    :cond_1
    invoke-static {}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordPageEnd()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    invoke-static {}, Lcom/android/incallui/util/PermissionGrantHelper;->getInstance()Lcom/android/incallui/util/PermissionGrantHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/incallui/util/PermissionGrantHelper;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "InCallActivity onResume()..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "InCallActivity onResume()..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->si(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iput-boolean v2, p0, Lcom/android/incallui/InCallActivity;->mIsPausing:Z

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->checkToFinish()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mAlphaToFinish:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mAlphaToFinish:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mNormalCallContainer:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    iput-boolean v2, p0, Lcom/android/incallui/InCallActivity;->mNeedResumeUnlocked:Z

    iput-boolean v3, p0, Lcom/android/incallui/InCallActivity;->mIsForegroundActivity:Z

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/incallui/InCallPresenter;->onUiShowing(Z)V

    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mShowDialpadRequested:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0, v3}, Lcom/android/incallui/CallButtonFragment;->displayDialpad(Z)V

    :cond_1
    iput-boolean v2, p0, Lcom/android/incallui/InCallActivity;->mShowDialpadRequested:Z

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mHandler:Lcom/android/incallui/BaseUIHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/incallui/BaseUIHandler;->sendEmptyMessage(I)Z

    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogOnResume:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogCallId:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogChars:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/incallui/InCallActivity;->showPostCharWaitDialog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mHandler:Lcom/android/incallui/BaseUIHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/incallui/BaseUIHandler;->sendEmptyMessage(I)Z

    invoke-static {p0}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordPageStart(Landroid/app/Activity;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "InCallActivity onStart()..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "InCallActivity onStart()..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->si(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mHandler:Lcom/android/incallui/BaseUIHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/incallui/BaseUIHandler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mHandler:Lcom/android/incallui/BaseUIHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/incallui/BaseUIHandler;->sendEmptyMessage(I)Z

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->setActivity(Lcom/android/incallui/InCallActivity;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onActivityStarted()V

    invoke-static {}, Lcom/android/incallui/InCallOrientationEventListener;->initCurrentOrientation()V

    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "InCallActivity onStop()..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "InCallActivity onStop()..."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->si(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->updateIsChangingConfigurations()V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onActivityStopped()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iput-boolean v2, p0, Lcom/android/incallui/InCallActivity;->mIsPausing:Z

    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->checkToFinish()Z

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->dismissPendingDialogs()V

    invoke-static {}, Lcom/android/incallui/util/SmartCoverUtil;->supportSmartCover()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-static {v0}, Lcom/android/incallui/util/SmartCoverUtil;->removeTargetSmartCover(Landroid/app/FragmentManager;)Z

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mNormalCallContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onSuppServiceFailed(I)V
    .locals 3

    const-string/jumbo v0, "InCallActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSuppServiceFailed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/InCallDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->dismissPendingDialogs()V

    new-instance v0, Lcom/android/incallui/view/SuppServiceFailedDialog;

    invoke-direct {v0, p0, p1}, Lcom/android/incallui/view/SuppServiceFailedDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/InCallDialog;->show()V

    return-void
.end method

.method public onVolteNotAvaliable()V
    .locals 2

    const-string/jumbo v0, "InCallActivity"

    const-string/jumbo v1, "onVolteNotAvaliable. "

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/InCallDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->dismissPendingDialogs()V

    new-instance v0, Lcom/android/incallui/view/VolteServiceFailedDialog;

    invoke-direct {v0, p0}, Lcom/android/incallui/view/VolteServiceFailedDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/InCallDialog;->show()V

    return-void
.end method

.method public playAudioButtonClickAnim(ZII)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/incallui/CallCardFragment;->playAudioButtonClickAnim(ZII)V

    :cond_0
    return-void
.end method

.method public setCallBackgroundDrawableResource(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    return-void
.end method

.method public showAnswerFragment(Z)V
    .locals 2

    const/4 v1, 0x1

    const-string/jumbo v0, "ICA.sAF"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    const-string/jumbo v0, "tag_answer_fragment"

    invoke-direct {p0, v0, p1, v1, v1}, Lcom/android/incallui/InCallActivity;->showFragment(Ljava/lang/String;ZZZ)V

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mAnswerFragment:Lcom/android/incallui/AnswerFragment;

    :cond_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void
.end method

.method public showConferenceFragment(Z)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const-string/jumbo v2, "ICA.sCF"

    invoke-static {v2}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    const-string/jumbo v2, "tag_conference_manager_fragment"

    invoke-direct {p0, v2, p1, v6, v6}, Lcom/android/incallui/InCallActivity;->showFragment(Ljava/lang/String;ZZZ)V

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    invoke-virtual {v2, p1}, Lcom/android/incallui/ConferenceManagerFragment;->onVisibilityChanged(Z)V

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Lcom/android/incallui/CallCardFragment;->setVisible(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/incallui/util/SmartCoverUtil;->isSmartCoverClosed(Landroid/content/Context;)Z

    move-result v1

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v2, v5}, Lcom/android/incallui/CallButtonFragment;->displayToolPanel(Z)V

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v2, v5}, Lcom/android/incallui/CallButtonFragment;->displayDialpad(Z)V

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v2, v5}, Lcom/android/incallui/CallButtonFragment;->setVisible(Z)V

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-static {v2}, Lcom/android/incallui/util/SmartCoverUtil;->removeTargetSmartCover(Landroid/app/FragmentManager;)Z

    :cond_1
    :goto_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_2
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/InCallPresenter;->getInCallState()Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v0

    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->INCOMING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq v0, v2, :cond_3

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mCallButtonFragment:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v2, v6}, Lcom/android/incallui/CallButtonFragment;->setVisible(Z)V

    :cond_3
    iput-object v4, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-static {v2}, Lcom/android/incallui/util/SmartCoverUtil;->showTargetSmartCover(Landroid/app/FragmentManager;)Z

    goto :goto_0
.end method

.method public showPostCharWaitDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->isForegroundActivity()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1, p2}, Lcom/android/incallui/PostCharDialogFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/android/incallui/PostCharDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->mFragmentManager:Landroid/app/FragmentManager;

    const-string/jumbo v2, "postCharWait"

    invoke-virtual {v0, v1, v2}, Lcom/android/incallui/PostCharDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogOnResume:Z

    iput-object v3, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogCallId:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogChars:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogOnResume:Z

    iput-object p1, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogCallId:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/incallui/InCallActivity;->mShowPostCharWaitDialogChars:Ljava/lang/String;

    goto :goto_0
.end method

.method public showSmartCover()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->isForegroundActivity()Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/util/SmartCoverUtil;->supportSmartCover()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/incallui/util/SmartCoverUtil;->isSmartCoverClosed(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-static {v2}, Lcom/android/incallui/util/SmartCoverUtil;->showTargetSmartCover(Landroid/app/FragmentManager;)Z

    move-result v0

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mButtonContainer:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/InCallPresenter;->notifySmartCoverCallListChange()V

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mConferenceManagerFragment:Lcom/android/incallui/ConferenceManagerFragment;

    invoke-virtual {v2}, Lcom/android/incallui/ConferenceManagerFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v4}, Lcom/android/incallui/InCallActivity;->showConferenceFragment(Z)V

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-static {v2}, Lcom/android/incallui/util/SmartCoverUtil;->removeTargetSmartCover(Landroid/app/FragmentManager;)Z

    move-result v0

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mNormalCallContainer:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->mButtonContainer:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public showUnlockedDialog(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->dismissPendingDialogs()V

    new-instance v0, Lcom/android/incallui/view/UnlockPhoneDialog;

    invoke-direct {v0, p0, p1}, Lcom/android/incallui/view/UnlockPhoneDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mDialog:Lcom/android/incallui/view/InCallDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/InCallDialog;->show()V

    return-void
.end method

.method public updateConferenceMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mCallCardFragment:Lcom/android/incallui/CallCardFragment;

    invoke-virtual {v0, p1}, Lcom/android/incallui/CallCardFragment;->updateConferenceMode(Z)V

    :cond_0
    return-void
.end method

.method public videoUpgradeRequestCallDialog()V
    .locals 1

    new-instance v0, Lcom/android/incallui/view/VideoUpgradeDialog;

    invoke-direct {v0, p0}, Lcom/android/incallui/view/VideoUpgradeDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->mVideoUpgradeDialog:Lcom/android/incallui/view/InCallDialog;

    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->mVideoUpgradeDialog:Lcom/android/incallui/view/InCallDialog;

    invoke-virtual {v0}, Lcom/android/incallui/view/InCallDialog;->show()V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->playUpgradePromptTone()V

    return-void
.end method
