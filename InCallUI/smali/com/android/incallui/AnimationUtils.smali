.class public Lcom/android/incallui/AnimationUtils;
.super Ljava/lang/Object;
.source "AnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/AnimationUtils$Fade;,
        Lcom/android/incallui/AnimationUtils$Translation;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/incallui/AnimationUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/AnimationUtils;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static startRotationAnimator(IILjava/util/Collection;)Landroid/animation/AnimatorSet;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/animation/AnimatorSet;"
        }
    .end annotation

    const/16 v10, 0x10e

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getRotation()F

    move-result v5

    float-to-int v5, v5

    rem-int/lit16 v5, v5, 0x168

    rem-int/lit16 v6, p0, 0x168

    if-eq v5, v6, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getRotation()F

    move-result v5

    float-to-int v2, v5

    if-ne v2, v10, :cond_1

    if-nez p0, :cond_1

    const/16 p0, 0x168

    :cond_1
    if-nez v2, :cond_2

    if-ne p0, v10, :cond_2

    const/16 v2, 0x168

    :cond_2
    const-string/jumbo v5, "rotation"

    const/4 v6, 0x2

    new-array v6, v6, [F

    int-to-float v7, v2

    aput v7, v6, v8

    int-to-float v7, p0

    aput v7, v6, v9

    invoke-static {v3, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-array v5, v9, [Landroid/animation/Animator;

    aput-object v0, v5, v8

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_0

    :cond_3
    int-to-long v6, p1

    invoke-virtual {v1, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    return-object v1
.end method

.method public static varargs startRotationAnimator(II[Landroid/view/View;)Landroid/animation/AnimatorSet;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    array-length v3, p2

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, p2, v2

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0, p1, v1}, Lcom/android/incallui/AnimationUtils;->startRotationAnimator(IILjava/util/Collection;)Landroid/animation/AnimatorSet;

    move-result-object v2

    return-object v2
.end method
