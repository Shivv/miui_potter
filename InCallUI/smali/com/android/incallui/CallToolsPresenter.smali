.class public Lcom/android/incallui/CallToolsPresenter;
.super Lcom/android/incallui/Presenter;
.source "CallToolsPresenter.java"

# interfaces
.implements Lcom/android/incallui/InCallPresenter$InCallStateListener;
.implements Lcom/android/incallui/AudioModeProvider$AudioModeListener;
.implements Lcom/android/incallui/InCallPresenter$IncomingCallListener;
.implements Lcom/android/incallui/InCallCameraManager$CameraSelectionListener;
.implements Lcom/android/incallui/InCallPresenter$InCallOrientationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/CallToolsPresenter$CallToolsUi;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/Presenter",
        "<",
        "Lcom/android/incallui/CallToolsPresenter$CallToolsUi;",
        ">;",
        "Lcom/android/incallui/InCallPresenter$InCallStateListener;",
        "Lcom/android/incallui/AudioModeProvider$AudioModeListener;",
        "Lcom/android/incallui/InCallPresenter$IncomingCallListener;",
        "Lcom/android/incallui/InCallCameraManager$CameraSelectionListener;",
        "Lcom/android/incallui/InCallPresenter$InCallOrientationListener;"
    }
.end annotation


# instance fields
.field private isMute:Z

.field private mCall:Lcom/android/incallui/Call;

.field private mLastCallKey:Ljava/lang/String;

.field private mLastOrientation:I

.field private mMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/Presenter;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mLastCallKey:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/incallui/CallToolsPresenter;->mMode:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/incallui/CallToolsPresenter;->mLastOrientation:I

    return-void
.end method

.method private isNeedUpdateUI(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)Z
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v2}, Lcom/android/incallui/Call;->getCallKeyForCallPresenter(Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v0

    if-ne p1, p2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mLastCallKey:Ljava/lang/String;

    invoke-static {v2, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v1, v2, 0x1

    :goto_0
    iput-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mLastCallKey:Ljava/lang/String;

    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private maybeShowNotesOrMessage()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Lcom/android/incallui/CallToolsPresenter$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/CallToolsPresenter$1;-><init>(Lcom/android/incallui/CallToolsPresenter;)V

    const-string/jumbo v1, "task_query_package_is_available"

    invoke-virtual {v0, v1}, Lcom/android/incallui/CallToolsPresenter$1;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v0

    const-wide/16 v2, 0xbb8

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/android/incallui/util/SimpleTask;->run(JZLjava/lang/Object;)V

    return-void
.end method

.method private updateUi(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 20

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v17

    check-cast v17, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    if-nez v17, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/CallToolsPresenter;->isVideoConferenceCall()Z

    move-result v18

    if-eqz v18, :cond_8

    const/4 v10, 0x3

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/incallui/CallToolsPresenter;->mMode:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v0, v10, :cond_1

    move-object/from16 v0, p0

    iput v10, v0, Lcom/android/incallui/CallToolsPresenter;->mMode:I

    invoke-interface/range {v17 .. v17}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->changeCallToolMode()V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/incallui/CallToolsPresenter;->isMute:Z

    move/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setMute(Z)V

    const/16 v18, 0x1

    move/from16 v0, v18

    if-eq v10, v0, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/incallui/CallToolsPresenter;->mLastOrientation:I

    move/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->startRotationAnimator(I)V

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    move-object/from16 v18, v0

    if-nez v18, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/incallui/CallToolsPresenter;->mLastOrientation:I

    move/from16 v18, v0

    if-eqz v18, :cond_2

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->startRotationAnimator(I)V

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/incallui/CallToolsPresenter;->mLastOrientation:I

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/android/incallui/InCallPresenter$InCallState;->isConnectingOrConnected()Z

    move-result v18

    if-nez v18, :cond_3

    sget-object v18, Lcom/android/incallui/InCallPresenter$InCallState;->PENDING_OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_c

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/android/incallui/InCallPresenter$InCallState;->isIncoming()Z

    move-result v18

    xor-int/lit8 v18, v18, 0x1

    if-eqz v18, :cond_c

    if-eqz p2, :cond_b

    const/4 v8, 0x1

    :goto_2
    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setEnabled(Z)V

    const-string/jumbo v18, "Updating call UI for call: "

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    if-eqz v8, :cond_7

    invoke-static/range {p2 .. p2}, Lcom/android/incallui/CallUtils;->isImsRegistered(Lcom/android/incallui/Call;)Z

    move-result v9

    invoke-virtual/range {p2 .. p2}, Lcom/android/incallui/Call;->isGenericConferenceCall()Z

    move-result v18

    if-eqz v18, :cond_d

    xor-int/lit8 v18, v9, 0x1

    if-eqz v18, :cond_d

    const/4 v3, 0x0

    :goto_3
    const/16 v18, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/incallui/Call;->can(I)Z

    move-result v18

    if-eqz v18, :cond_f

    invoke-static/range {p2 .. p2}, Lcom/android/incallui/CallCapabilitiesUtils;->canHold(Lcom/android/incallui/Call;)Z

    move-result v4

    :goto_4
    const/16 v18, 0x2

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/incallui/Call;->can(I)Z

    move-result v16

    const/16 v18, 0x4

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/incallui/Call;->can(I)Z

    move-result v14

    move v11, v3

    const/16 v18, 0x8

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/incallui/Call;->can(I)Z

    move-result v18

    if-eqz v18, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/android/incallui/Call;->isGenericConferenceCall()Z

    move-result v18

    if-nez v18, :cond_10

    :cond_4
    invoke-static {}, Lcom/android/incallui/CallCapabilitiesUtils;->canSwap()Z

    move-result v18

    if-eqz v18, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/CallToolsPresenter;->isVideoCall()Z

    move-result v18

    xor-int/lit8 v15, v18, 0x1

    :goto_5
    if-nez v15, :cond_12

    move v12, v4

    :goto_6
    const/16 v18, 0x40

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/incallui/Call;->can(I)Z

    move-result v18

    if-eqz v18, :cond_13

    invoke-static/range {p2 .. p2}, Lcom/android/incallui/CallCapabilitiesUtils;->canMute(Lcom/android/incallui/Call;)Z

    move-result v5

    :goto_7
    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableMute(Z)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/incallui/CallToolsPresenter;->isMute:Z

    move/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setMute(Z)V

    if-nez v12, :cond_5

    if-eqz v15, :cond_14

    :cond_5
    if-eqz v14, :cond_14

    move v6, v11

    :goto_8
    if-nez v6, :cond_15

    move v13, v14

    :goto_9
    invoke-virtual/range {p2 .. p2}, Lcom/android/incallui/Call;->getSessionModificationState()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_17

    sget v18, Lcom/android/incallui/CallUtils;->callSessionRequestType:I

    const/16 v19, 0x64

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_16

    const/4 v7, 0x1

    :goto_a
    if-eqz v13, :cond_18

    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showMerge(Z)V

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showAddCall(Z)V

    :goto_b
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/CallToolsPresenter;->isVideoCall()Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-virtual/range {p2 .. p2}, Lcom/android/incallui/Call;->getState()I

    move-result v18

    const/16 v19, 0x6

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_1a

    xor-int/lit8 v18, v7, 0x1

    :goto_c
    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableSwitchToVoiceButton(Z)V

    invoke-virtual/range {p2 .. p2}, Lcom/android/incallui/Call;->getState()I

    move-result v18

    invoke-static/range {v18 .. v18}, Lcom/android/incallui/Call$State;->isMiuiDialing(I)Z

    move-result v18

    if-nez v18, :cond_1b

    xor-int/lit8 v18, v7, 0x1

    :goto_d
    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enablePauseVideo(Z)V

    invoke-virtual/range {p2 .. p2}, Lcom/android/incallui/Call;->getSessionModificationState()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    sget v18, Lcom/android/incallui/CallUtils;->callSessionRequestType:I

    const/16 v19, 0x64

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setPauseVideoButton(Z)V

    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableSwitchCamera(Z)V

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/CallToolsPresenter;->maybeShowContactOrVideoButton()V

    if-eqz v12, :cond_1e

    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showHold(Z)V

    invoke-virtual/range {p2 .. p2}, Lcom/android/incallui/Call;->getState()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1c

    const/16 v18, 0x1

    :goto_e
    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setHold(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/CallToolsPresenter;->isVideoCall()Z

    move-result v18

    if-eqz v18, :cond_1d

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/android/incallui/CallList;->getCurrentCallCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1d

    const/16 v18, 0x1

    :goto_f
    xor-int/lit8 v18, v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableHold(Z)V

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showSwap(Z)V

    :goto_10
    if-eqz v6, :cond_7

    invoke-static/range {p2 .. p2}, Lcom/android/incallui/CallUtils;->isCDMAPhone(Lcom/android/incallui/Call;)Z

    move-result v18

    if-eqz v18, :cond_7

    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showMerge(Z)V

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showAddCall(Z)V

    :cond_7
    return-void

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/CallToolsPresenter;->isVideoCall()Z

    move-result v18

    if-eqz v18, :cond_9

    const/4 v10, 0x2

    goto/16 :goto_0

    :cond_9
    const/4 v10, 0x1

    goto/16 :goto_0

    :cond_a
    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->startRotationAnimator(I)V

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/incallui/CallToolsPresenter;->mLastOrientation:I

    goto/16 :goto_1

    :cond_b
    const/4 v8, 0x0

    goto/16 :goto_2

    :cond_c
    const/4 v8, 0x0

    goto/16 :goto_2

    :cond_d
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/android/incallui/TelecomAdapter;->canAddCall()Z

    move-result v18

    if-eqz v18, :cond_e

    invoke-static/range {p2 .. p2}, Lcom/android/incallui/CallCapabilitiesUtils;->canAdd(Lcom/android/incallui/Call;)Z

    move-result v3

    goto/16 :goto_3

    :cond_e
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_10
    const/4 v15, 0x1

    goto/16 :goto_5

    :cond_11
    const/4 v15, 0x0

    goto/16 :goto_5

    :cond_12
    const/4 v12, 0x0

    goto/16 :goto_6

    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_14
    const/4 v6, 0x0

    goto/16 :goto_8

    :cond_15
    const/4 v13, 0x0

    goto/16 :goto_9

    :cond_16
    const/4 v7, 0x0

    goto/16 :goto_a

    :cond_17
    const/4 v7, 0x0

    goto/16 :goto_a

    :cond_18
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/android/incallui/CallList;->getCurrentCallCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_19

    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showMerge(Z)V

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showAddCall(Z)V

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableMerge(Z)V

    goto/16 :goto_b

    :cond_19
    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showMerge(Z)V

    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showAddCall(Z)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableAddCall(Z)V

    goto/16 :goto_b

    :cond_1a
    const/16 v18, 0x0

    goto/16 :goto_c

    :cond_1b
    const/16 v18, 0x0

    goto/16 :goto_d

    :cond_1c
    const/16 v18, 0x0

    goto/16 :goto_e

    :cond_1d
    const/16 v18, 0x0

    goto/16 :goto_f

    :cond_1e
    if-eqz v15, :cond_1f

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showHold(Z)V

    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showSwap(Z)V

    goto/16 :goto_10

    :cond_1f
    if-eqz v16, :cond_21

    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showHold(Z)V

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableHold(Z)V

    invoke-virtual/range {p2 .. p2}, Lcom/android/incallui/Call;->getState()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_20

    const/16 v18, 0x1

    :goto_11
    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setHold(Z)V

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showSwap(Z)V

    goto/16 :goto_10

    :cond_20
    const/16 v18, 0x0

    goto :goto_11

    :cond_21
    const/16 v18, 0x1

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showHold(Z)V

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableHold(Z)V

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showSwap(Z)V

    goto/16 :goto_10
.end method


# virtual methods
.method public getCallToolMode()I
    .locals 1

    iget v0, p0, Lcom/android/incallui/CallToolsPresenter;->mMode:I

    return v0
.end method

.method public initCallTool()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getInCallState()Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/incallui/CallToolsPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    invoke-direct {p0}, Lcom/android/incallui/CallToolsPresenter;->maybeShowNotesOrMessage()V

    :cond_0
    return-void
.end method

.method public initVtView()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->maybeShowContactOrVideoButton()V

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setPauseVideoButton(Z)V

    :cond_0
    return-void
.end method

.method public isSipCall()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v2, :cond_0

    return v1

    :cond_0
    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->getInstance()Lcom/android/incallui/ContactInfoCache;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/incallui/ContactInfoCache;->getInfo(Ljava/lang/String;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isSipCall:Z

    :cond_1
    return v1
.end method

.method public isVideoCall()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isVideoCall(Lcom/android/incallui/Call;)Z

    move-result v0

    return v0
.end method

.method public isVideoConferenceCall()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isVideoConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v0

    return v0
.end method

.method public isVtConferenceCall()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isVtConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v0

    return v0
.end method

.method public maybeShowContactOrVideoButton()V
    .locals 10

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v5

    check-cast v5, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    if-nez v5, :cond_0

    return-void

    :cond_0
    iget-object v8, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v8, :cond_1

    invoke-interface {v5, v7}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showSwitchToVideoButton(Z)V

    invoke-interface {v5, v6}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showContactButton(Z)V

    return-void

    :cond_1
    iget-object v8, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v8}, Lcom/android/incallui/CallUtils;->isImsRegistered(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v8, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v8}, Lcom/android/incallui/CallAdapterUtils;->hasLocalVoiceCapabilities(Lcom/android/incallui/Call;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v8}, Lcom/android/incallui/CallAdapterUtils;->hasLocalVideoCapabilities(Lcom/android/incallui/Call;)Z

    move-result v1

    :goto_0
    const-string/jumbo v8, "Show video "

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {p0, v8, v9}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v8, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v8}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v2

    const/4 v4, 0x0

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/incallui/CallList;->getCurrentCallCount()I

    move-result v0

    if-eqz v3, :cond_2

    xor-int/lit8 v8, v2, 0x1

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/android/incallui/CallUtils;->isVtEnabledByPlatform()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/android/incallui/util/Utils;->isInternationalBuild()Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v4, 0x1

    :cond_2
    :goto_1
    if-eqz v4, :cond_3

    if-eqz v1, :cond_8

    iget-object v8, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v8}, Lcom/android/incallui/Call;->getSessionModificationState()I

    move-result v8

    if-eq v8, v6, :cond_8

    iget-object v8, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v8}, Lcom/android/incallui/Call;->getState()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_7

    :goto_2
    invoke-interface {v5, v6}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableSwitchToVideoButton(Z)V

    :cond_3
    invoke-interface {v5, v4}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showSwitchToVideoButton(Z)V

    xor-int/lit8 v6, v4, 0x1

    invoke-interface {v5, v6}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->showContactButton(Z)V

    return-void

    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    if-eq v0, v6, :cond_6

    if-nez v0, :cond_2

    iget-object v8, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v8}, Lcom/android/incallui/Call;->getState()I

    move-result v8

    invoke-static {v8}, Lcom/android/incallui/Call$State;->isMiuiDialing(I)Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_6
    const/4 v4, 0x1

    goto :goto_1

    :cond_7
    move v6, v7

    goto :goto_2

    :cond_8
    move v6, v7

    goto :goto_2
.end method

.method public onActiveCameraSelectionChanged(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    xor-int/lit8 v1, p1, 0x1

    invoke-interface {v0, v1}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setSwitchCameraButton(Z)V

    return-void
.end method

.method public onAddCallClick()V
    .locals 2

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    const v1, 0x7f0b0025

    invoke-static {v0, v1}, Lcom/android/incallui/util/Utils;->checkUserUnlocked(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/TelecomAdapter;->addCall()V

    return-void
.end method

.method public onAddMessageClicked()V
    .locals 7

    const v6, 0x7f0b009f

    const/4 v5, 0x0

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/android/incallui/util/Utils;->checkUserUnlocked(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_1
    return-void

    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SENDTO"

    const-string/jumbo v3, "smsto"

    iget-object v4, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string/jumbo v2, "com.android.mms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/incallui/InCallApp;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v2, "Can not start mms "

    invoke-static {p0, v2, v0}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v6}, Lcom/android/incallui/util/Utils;->displayPackageNotAvailabeMsg(I)V

    goto :goto_0
.end method

.method public onAudioMode(I)V
    .locals 0

    return-void
.end method

.method public onContactClick()V
    .locals 4

    const v3, 0x7f0b0033

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/android/incallui/util/Utils;->checkUserUnlocked(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string/jumbo v2, "vnd.android.cursor.dir/contact"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/incallui/InCallApp;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v2, "Can not start Contact "

    invoke-static {p0, v2, v0}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v3}, Lcom/android/incallui/util/Utils;->displayPackageNotAvailabeMsg(I)V

    goto :goto_0
.end method

.method public onDeviceOrientationChanged(I)V
    .locals 2

    iget v1, p0, Lcom/android/incallui/CallToolsPresenter;->mLastOrientation:I

    if-ne v1, p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->isVideoCall()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0, p1}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->startRotationAnimator(I)V

    :cond_1
    iput p1, p0, Lcom/android/incallui/CallToolsPresenter;->mLastOrientation:I

    return-void
.end method

.method public onHoldClick(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Putting the call on hold: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->holdCall(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Removing the call from hold: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->unholdCall(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onIncomingCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/incallui/CallToolsPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void
.end method

.method public onMergeClick()V
    .locals 2

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/CallList;->isConferenceExceeded()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b00a6

    invoke-static {v0}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->merge(Ljava/lang/String;)V

    return-void
.end method

.method public onMute(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setMute(Z)V

    :cond_0
    iput-boolean p1, p0, Lcom/android/incallui/CallToolsPresenter;->isMute:Z

    return-void
.end method

.method public onMuteClick(Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "turning on mute: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/incallui/TelecomAdapter;->mute(Z)V

    return-void
.end method

.method public onNoteClick()V
    .locals 10

    const v9, 0x7f0b0031

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v7

    invoke-static {v7, v9}, Lcom/android/incallui/util/Utils;->checkUserUnlocked(Landroid/content/Context;I)Z

    move-result v7

    if-nez v7, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/incallui/CallList;->getFirstCall()Lcom/android/incallui/Call;

    move-result-object v0

    const-string/jumbo v5, ""

    const-wide/16 v2, 0x0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_2

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getChildCallIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_2

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v8

    const/4 v7, 0x0

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Lcom/android/incallui/CallList;->getCallById(Ljava/lang/String;)Lcom/android/incallui/Call;

    move-result-object v0

    :cond_2
    invoke-virtual {v0}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getCallCreationTime()J

    move-result-wide v2

    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v7, 0x10000000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string/jumbo v7, "vnd.android.cursor.item/call_note"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v7, "android.intent.extra.PHONE_NUMBER"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v7, "com.miui.notes.call_date"

    invoke-virtual {v6, v7, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :try_start_0
    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/android/incallui/InCallApp;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v4

    const-string/jumbo v7, "Can not start note "

    invoke-static {p0, v7, v4}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v9}, Lcom/android/incallui/util/Utils;->displayPackageNotAvailabeMsg(I)V

    goto :goto_0
.end method

.method public onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "CTP.oSC"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    invoke-interface {v0}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->isInCallToolPanelContentInit()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_0
    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->PENDING_OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v0, :cond_3

    :cond_1
    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    iget-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_2

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getPendingOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    :cond_2
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/android/incallui/CallToolsPresenter;->isNeedUpdateUI(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ignore NeedUpdateUI:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mLastCallKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_3
    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v0, :cond_4

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getActiveOrBackgroundCall()Lcom/android/incallui/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->INCOMING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v0, :cond_5

    iput-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_5
    iput-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-direct {p0, p2, v0}, Lcom/android/incallui/CallToolsPresenter;->updateUi(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V

    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v0, :cond_7

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setPauseVideoButton(Z)V

    :cond_7
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void
.end method

.method public onSupportedAudioMode(I)V
    .locals 0

    return-void
.end method

.method public onSwapClick()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Swapping the call: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->swap(Ljava/lang/String;)V

    return-void
.end method

.method public onUiReady(Lcom/android/incallui/CallToolsPresenter$CallToolsUi;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiReady(Lcom/android/incallui/Ui;)V

    invoke-static {}, Lcom/android/incallui/AudioModeProvider;->getInstance()Lcom/android/incallui/AudioModeProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/AudioModeProvider;->addListener(Lcom/android/incallui/AudioModeProvider$AudioModeListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getInCallCameraManager()Lcom/android/incallui/InCallCameraManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallCameraManager;->addCameraSelectionListener(Lcom/android/incallui/InCallCameraManager$CameraSelectionListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addOrientationListener(Lcom/android/incallui/InCallPresenter$InCallOrientationListener;)V

    return-void
.end method

.method public bridge synthetic onUiReady(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/CallToolsPresenter;->onUiReady(Lcom/android/incallui/CallToolsPresenter$CallToolsUi;)V

    return-void
.end method

.method public onUiUnready(Lcom/android/incallui/CallToolsPresenter$CallToolsUi;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiUnready(Lcom/android/incallui/Ui;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/AudioModeProvider;->getInstance()Lcom/android/incallui/AudioModeProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/AudioModeProvider;->removeListener(Lcom/android/incallui/AudioModeProvider$AudioModeListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getInCallCameraManager()Lcom/android/incallui/InCallCameraManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallCameraManager;->removeCameraSelectionListener(Lcom/android/incallui/InCallCameraManager$CameraSelectionListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeOrientationListener(Lcom/android/incallui/InCallPresenter$InCallOrientationListener;)V

    return-void
.end method

.method public bridge synthetic onUiUnready(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/CallToolsPresenter;->onUiUnready(Lcom/android/incallui/CallToolsPresenter$CallToolsUi;)V

    return-void
.end method

.method public pauseVideoClicked(Z)V
    .locals 4

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    xor-int/lit8 v3, p1, 0x1

    invoke-interface {v2, v3}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->enableSwitchCamera(Z)V

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v2, p1}, Lcom/android/incallui/VoLTEProxy;->closeCamera(Lcom/android/incallui/Call;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pauseVideoClicked exception="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public switchCameraClicked(Z)V
    .locals 5

    iget-object v4, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v4, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/InCallPresenter;->getInCallCameraManager()Lcom/android/incallui/InCallCameraManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/incallui/InCallCameraManager;->setUseFrontFacingCamera(Z)V

    iget-object v4, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    invoke-virtual {v2}, Lcom/android/incallui/InCallCameraManager;->getActiveCameraId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/android/incallui/InCallCameraManager;->isUsingFrontFacingCamera()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->getVideoSettings()Lcom/android/incallui/Call$VideoSettings;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/android/incallui/Call$VideoSettings;->setCameraDir(I)V

    invoke-virtual {v3, v1}, Landroid/telecom/InCallService$VideoCall;->setCamera(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/telecom/InCallService$VideoCall;->requestCameraCapabilities()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public switchToVideoClicked()V
    .locals 4

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v2}, Lcom/android/incallui/CallAdapterUtils;->hasRemoteVoiceCapabilities(Lcom/android/incallui/Call;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-static {v2}, Lcom/android/incallui/CallAdapterUtils;->hasRemoteVideoCapabilities(Lcom/android/incallui/Call;)Z

    move-result v0

    :goto_0
    const-string/jumbo v2, "switchToVideoClicked suport video "

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {p0, v2, v3}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v0, :cond_3

    const v2, 0x7f0b00a7

    invoke-static {v2}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    return-void

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/incallui/Call;->setSessionModificationState(I)V

    new-instance v2, Landroid/telecom/VideoProfile;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/telecom/VideoProfile;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyRequest(Landroid/telecom/VideoProfile;)V

    return-void
.end method

.method public switchToVoiceClicked()V
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v1, Landroid/telecom/VideoProfile;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/telecom/VideoProfile;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->sendSessionModifyRequest(Landroid/telecom/VideoProfile;)V

    return-void
.end method

.method public updateHold()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallToolsPresenter;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0, v1}, Lcom/android/incallui/CallToolsPresenter$CallToolsUi;->setHold(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
