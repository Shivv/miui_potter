.class public Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;
.super Lcom/android/incallui/Presenter;
.source "SmartCoverAnswerPresenter.java"

# interfaces
.implements Lcom/android/incallui/InCallPresenter$InCallStateListener;
.implements Lcom/android/incallui/InCallPresenter$IncomingCallListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$1;,
        Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/Presenter",
        "<",
        "Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;",
        ">;",
        "Lcom/android/incallui/InCallPresenter$InCallStateListener;",
        "Lcom/android/incallui/InCallPresenter$IncomingCallListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

.field private mCallTimer:Lcom/android/incallui/CallTimer;

.field private mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

.field private mCurrentCall:Lcom/android/incallui/Call;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;)Lcom/android/incallui/Call;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->updateSmartCoverInfo()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/incallui/Presenter;-><init>()V

    new-instance v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$1;-><init>(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    new-instance v0, Lcom/android/incallui/CallTimer;

    new-instance v1, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$2;-><init>(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;)V

    invoke-direct {v0, v1}, Lcom/android/incallui/CallTimer;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    return-void
.end method

.method private updateCallEntry()V
    .locals 4

    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->getInstance()Lcom/android/incallui/ContactInfoCache;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/ContactInfoCache;->maybeUpdateContactInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-void
.end method

.method private updateSingleDisplayInfo(Lcom/android/incallui/model/CallCardInfo;)V
    .locals 8

    const/4 v5, 0x0

    sget-object v1, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateSingleDisplayInfo: update primary display "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    if-nez v0, :cond_0

    sget-object v1, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "updateSingleDisplayInfo called but ui is null!"

    invoke-static {v1, v4}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isThreeWayIncoming(Lcom/android/incallui/Call;)Z

    move-result v7

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_2

    iget-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    if-eqz v1, :cond_3

    const/4 v2, 0x0

    :goto_0
    if-eqz v7, :cond_1

    const/4 v2, 0x0

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isThreeWayOutgoing(Lcom/android/incallui/Call;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    :cond_2
    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getPrimaryCallNum()I

    move-result v4

    move-object v1, p1

    move v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->setPrimary(Lcom/android/incallui/model/CallCardInfo;ZZIZZ)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v4

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getRedialTimes()I

    move-result v5

    invoke-interface {v0, v1, v4, v5}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->setCallState(ILandroid/telecom/DisconnectCause;I)V

    :goto_1
    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isConference(Lcom/android/incallui/Call;)Z

    move-result v2

    goto :goto_0

    :cond_4
    new-instance v1, Landroid/telecom/DisconnectCause;

    invoke-direct {v1, v5}, Landroid/telecom/DisconnectCause;-><init>(I)V

    const/4 v4, 0x2

    const/4 v5, -0x1

    invoke-interface {v0, v4, v1, v5}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->setCallState(ILandroid/telecom/DisconnectCause;I)V

    goto :goto_1
.end method

.method private updateSmartCoverInfo()V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    if-nez v3, :cond_0

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iget-object v7, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-static {v4, v7}, Lcom/android/incallui/model/CallCardInfo;->createCallCardInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v0

    iget-boolean v4, v0, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    invoke-interface {v4}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->initAnswerOperLayout()V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    invoke-interface {v4, v6}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->showTextButton(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    invoke-interface {v4, v5}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->showAnswerUi(Z)V

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/model/CallCardInfo;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v4

    invoke-static {v4}, Lcom/android/incallui/InCallPresenter;->getPotentialStateFromCallList(Lcom/android/incallui/CallList;)Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v2

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    sget-object v4, Lcom/android/incallui/InCallPresenter$InCallState;->OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq v2, v4, :cond_1

    sget-object v4, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq v2, v4, :cond_1

    const/4 v1, 0x0

    :cond_1
    if-eqz v1, :cond_3

    move v4, v5

    :goto_1
    invoke-interface {v3, v4}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->showHangUpButton(Z)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    invoke-interface {v4, v6}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->showAnswerUi(Z)V

    goto :goto_0

    :cond_3
    move v4, v6

    goto :goto_1
.end method


# virtual methods
.method public endCallClicked()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->disconnectCall(Ljava/lang/String;)V

    return-void
.end method

.method public getPrimaryCallNum()I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getChildCallIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onAnswer(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/incallui/TelecomAdapter;->answerCall(Ljava/lang/String;I)V

    return-void
.end method

.method public onDecline()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/TelecomAdapter;->rejectCall(Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method public onIncomingCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onIncomingCall(): state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " , call = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void
.end method

.method public onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStateChange(): state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , callList = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getFirstCall()Lcom/android/incallui/Call;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->updateCallEntry()V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->updateSmartCoverInfo()V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->updateCallTime()V

    return-void
.end method

.method public bridge synthetic onUiReady(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->onUiReady(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;)V

    return-void
.end method

.method public onUiReady(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;)V
    .locals 4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onUiReady ui="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiReady(Lcom/android/incallui/Ui;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/InCallPresenter;->getPotentialStateFromCallList(Lcom/android/incallui/CallList;)Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v1

    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-virtual {p0, v2, v1, v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/incallui/InCallPresenter;->addListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/incallui/InCallPresenter;->addIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    return-void
.end method

.method public bridge synthetic onUiUnready(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->onUiUnready(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;)V

    return-void
.end method

.method public onUiUnready(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiUnready(Lcom/android/incallui/Ui;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    iput-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iput-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-void
.end method

.method public updateCallTime()V
    .locals 12

    const-wide/16 v10, 0x3e8

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v5

    check-cast v5, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getState()I

    move-result v6

    invoke-static {v6}, Lcom/android/incallui/Call$State;->isRinging(I)Z

    move-result v4

    :goto_0
    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v6, :cond_3

    :cond_0
    if-eqz v5, :cond_1

    invoke-interface {v5, v8}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->setPrimaryCallElapsedTime(Ljava/lang/String;)V

    :cond_1
    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v6}, Lcom/android/incallui/CallTimer;->cancel()V

    :goto_1
    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getState()I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_4

    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getState()I

    move-result v6

    const/16 v7, 0x8

    if-ne v6, v7, :cond_0

    :cond_4
    if-nez v4, :cond_0

    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v6}, Lcom/android/incallui/CallTimer;->isRunning()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v6, v10, v11}, Lcom/android/incallui/CallTimer;->start(J)Z

    :cond_5
    iget-object v6, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v0

    div-long v6, v2, v10

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;->setPrimaryCallElapsedTime(Ljava/lang/String;)V

    goto :goto_1
.end method
