.class public Lcom/android/incallui/smartcover/b7/B7CoverFragment;
.super Lcom/android/incallui/BaseFragment;
.source "B7CoverFragment.java"

# interfaces
.implements Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;
.implements Lcom/android/incallui/OnAnimatorChanged;
.implements Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/smartcover/b7/B7CoverFragment$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/smartcover/b7/B7CoverPresenter;",
        "Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;",
        ">;",
        "Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;",
        "Lcom/android/incallui/OnAnimatorChanged;",
        "Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isFirstShow:Z

.field private isShowingAnswerUI:Z

.field private mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

.field private mAnswerImg:Lcom/android/incallui/view/CircleImageView;

.field private mAnswerOperView:Landroid/view/View;

.field private mAnswerText:Landroid/widget/TextView;

.field private mArrowList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mBounceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/incallui/view/CircleImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mCircleAnimatorSet:Landroid/animation/AnimatorSet;

.field private mCircleImageAnswerHeight:I

.field private mCircleImageRejectHeight:I

.field private mContext:Landroid/content/Context;

.field private mDefaultPanelHeight:I

.field private mElapsedTime:Landroid/widget/TextView;

.field private mEndBtn:Landroid/widget/Button;

.field protected mHandler:Landroid/os/Handler;

.field private mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

.field private mOperateLayout:Landroid/widget/LinearLayout;

.field private mPhoneNumber:Landroid/widget/TextView;

.field private mPrimaryName:Landroid/widget/TextView;

.field private mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

.field private mRejectImg:Lcom/android/incallui/view/CircleImageView;

.field private mRejectText:Landroid/widget/TextView;

.field private mRootView:Landroid/view/View;

.field private mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

.field private mTelocation:Landroid/widget/TextView;

.field private mTextList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mTranslateInList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/incallui/view/CircleImageView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTranslateInList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mArrowList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTextList:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mDefaultPanelHeight:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isShowingAnswerUI:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isFirstShow:Z

    new-instance v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment$1;-><init>(Lcom/android/incallui/smartcover/b7/B7CoverFragment;)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private getDefaultPanelHeight()I
    .locals 3

    iget v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mDefaultPanelHeight:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09009f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mDefaultPanelHeight:I

    invoke-static {}, Landroid/content/res/MiuiConfiguration;->getScaleMode()I

    move-result v0

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mDefaultPanelHeight:I

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mDefaultPanelHeight:I

    :cond_0
    iget v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mDefaultPanelHeight:I

    return v0
.end method

.method private hideOperateItems()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerOperView:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->stopCircleBounceAnimator()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setMaxTransparent()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/CircleImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/CircleImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/ArrowImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/ArrowImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private playArrowAnimator(Landroid/view/View;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/ArrowImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/incallui/view/ArrowImageView;->playArrowMoveUpAnimator()V

    :cond_2
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setCircleText(IZ)V

    return-void
.end method

.method private setCircleText(IZ)V
    .locals 5

    const/4 v4, 0x0

    if-gez p1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTextList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aget-object v2, v1, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private setMaxTransparent()V
    .locals 3

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/view/CircleImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/android/incallui/view/CircleImageView;->setAlpha(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updatePhoneNumberField(Ljava/lang/String;Z)V
    .locals 0

    if-eqz p2, :cond_0

    const-string/jumbo p1, ""

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setPrimaryPhoneNumber(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public cancelArrowAnimator(Landroid/view/View;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/ArrowImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/incallui/view/ArrowImageView;->cancelAllAnimator()V

    :cond_2
    return-void
.end method

.method public bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->createPresenter()Lcom/android/incallui/smartcover/b7/B7CoverPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/android/incallui/smartcover/b7/B7CoverPresenter;
    .locals 1

    new-instance v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;

    invoke-direct {v0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;-><init>()V

    return-object v0
.end method

.method public getTranslateInAnimatorSet(Lcom/android/incallui/view/CircleImageView;)Landroid/animation/ObjectAnimator;
    .locals 17

    const/4 v11, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_3

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleImageAnswerHeight:I

    const/4 v2, 0x1

    :cond_0
    :goto_0
    const v14, 0x438b8000    # 279.0f

    const/4 v15, 0x0

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    neg-int v14, v11

    int-to-float v14, v14

    const v15, 0x3e99999a    # 0.3f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_4

    const/4 v14, 0x5

    :goto_1
    int-to-float v14, v14

    const/high16 v15, 0x3f000000    # 0.5f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_5

    const/4 v14, -0x3

    :goto_2
    int-to-float v14, v14

    const v15, 0x3f19999a    # 0.6f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_6

    const/4 v14, 0x2

    :goto_3
    int-to-float v14, v14

    const v15, 0x3f333333    # 0.7f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_7

    const/high16 v14, -0x40800000    # -1.0f

    :goto_4
    const v15, 0x3f4ccccd    # 0.8f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_1

    :cond_1
    const/4 v14, 0x0

    int-to-float v14, v14

    const v15, 0x3f666666    # 0.9f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    invoke-static {v14, v15}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    sget-object v14, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v15, 0x8

    new-array v15, v15, [Landroid/animation/Keyframe;

    const/16 v16, 0x0

    aput-object v3, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    const/16 v16, 0x2

    aput-object v5, v15, v16

    const/16 v16, 0x3

    aput-object v6, v15, v16

    const/16 v16, 0x4

    aput-object v7, v15, v16

    const/16 v16, 0x5

    aput-object v8, v15, v16

    const/16 v16, 0x6

    aput-object v9, v15, v16

    const/16 v16, 0x7

    aput-object v10, v15, v16

    invoke-static {v14, v15}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v12

    const/4 v14, 0x1

    new-array v14, v14, [Landroid/animation/PropertyValuesHolder;

    const/4 v15, 0x0

    aput-object v12, v14, v15

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_8

    const/16 v14, 0x640

    :goto_5
    int-to-long v14, v14

    invoke-virtual {v13, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v14, Lcom/android/incallui/smartcover/b7/B7CoverFragment$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v14, v0, v1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment$3;-><init>(Lcom/android/incallui/smartcover/b7/B7CoverFragment;Lcom/android/incallui/view/CircleImageView;)V

    invoke-virtual {v13, v14}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_9

    const-wide/16 v14, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    :cond_2
    :goto_6
    return-object v13

    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_0

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleImageRejectHeight:I

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v14, 0x3

    goto/16 :goto_1

    :cond_5
    const/4 v14, -0x2

    goto/16 :goto_2

    :cond_6
    const/4 v14, 0x1

    goto/16 :goto_3

    :cond_7
    const/high16 v14, -0x41000000    # -0.5f

    goto/16 :goto_4

    :cond_8
    const/16 v14, 0x4b0

    goto :goto_5

    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_2

    const-wide/16 v14, 0xfa

    invoke-virtual {v13, v14, v15}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    goto :goto_6
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getUi()Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;

    move-result-object v0

    return-object v0
.end method

.method protected getUi()Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;
    .locals 0

    return-object p0
.end method

.method public initAnswerOperLayout()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerOperView:Landroid/view/View;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a001c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerOperView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0020

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CircleImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CircleImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/ArrowImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/ArrowImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a0019

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerText:Landroid/widget/TextView;

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onAnimatorPause()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->pauseCircleBounceAnimator()V

    return-void
.end method

.method public onAnimatorResume()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->playCircleBounceAnimator()V

    return-void
.end method

.method public onAnswer(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;

    invoke-virtual {v0, p1}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->onAnswer(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->endCallClicked()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a002c
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {}, Lcom/android/incallui/util/SmartCoverUtil;->getDisplayWindowSizeInSmartCover()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    iget v4, v1, Landroid/graphics/Rect;->left:I

    iget v5, v1, Landroid/graphics/Rect;->top:I

    iget v6, v1, Landroid/graphics/Rect;->right:I

    sub-int v6, v2, v6

    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v7, v0, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/InCallApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09005e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleImageAnswerHeight:I

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/InCallApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleImageRejectHeight:I

    iget-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    return-object v3
.end method

.method public onDecline()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->onDecline()V

    return-void
.end method

.method public onDestroyView()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/AnimatorObservable;->unRegisterListener(Lcom/android/incallui/OnAnimatorChanged;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    :cond_0
    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onDestroyView()V

    return-void
.end method

.method public onPanelCollapsed(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "onPanelCollapsed()"

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->log(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->onViewTouchUp(Landroid/view/View;)V

    return-void
.end method

.method public onPanelExpanded(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v1, "onPanelExpanded()"

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setMaxTransparent()V

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->onDecline()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    if-ne p1, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->onAnswer(I)V

    goto :goto_0
.end method

.method public onPanelSlide(Landroid/view/View;FLandroid/view/View;)V
    .locals 7

    const v6, 0x3f19999a    # 0.6f

    const v5, 0x3ec28f5c    # 0.38f

    cmpg-float v4, p2, v5

    if-ltz v4, :cond_0

    cmpl-float v4, p2, v6

    if-lez v4, :cond_2

    :cond_0
    cmpl-float v4, p2, v6

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setCircleText(IZ)V

    :cond_1
    return-void

    :cond_2
    sub-float v4, p2, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v4, Lcom/android/incallui/view/SlidingUpPanelLayout;->DIFF_SLIDE_OFFSET:F

    div-float v2, v0, v4

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, 0x1

    invoke-direct {p0, v4, v5}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setCircleText(IZ)V

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    if-eqz v4, :cond_4

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_3

    if-ne v3, p3, :cond_3

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v4, v2

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method public onPanelViewCollapsedReleased(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "onPanelViewCollapsedReleased()"

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onPanelViewExpandReleased(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "onPanelViewExpandReleased()"

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onStart()V

    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/AnimatorObservable;->registerListener(Lcom/android/incallui/OnAnimatorChanged;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/incallui/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a0018

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/SlidingUpPanelLayout;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setSlidingEnabled(Z)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setOverlayed(Z)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setPanelSlideListener(Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;)V

    const v0, 0x7f0a0029

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPhoneNumber:Landroid/widget/TextView;

    const v0, 0x7f0a0005

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPrimaryName:Landroid/widget/TextView;

    const v0, 0x7f0a002b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTelocation:Landroid/widget/TextView;

    const v0, 0x7f0a002a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mElapsedTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mEndBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mEndBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0d0012

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    return-void
.end method

.method public onViewMove(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getSlideOffset()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3fe6666666666666L    # 0.7

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->cancelArrowAnimator(Landroid/view/View;)V

    return-void
.end method

.method public onViewTouchDown(Landroid/view/View;Z)V
    .locals 5

    iget-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    iget-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/view/CircleImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/android/incallui/view/CircleImageView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, p1, v3}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setSlidingView(Landroid/view/View;Z)V

    if-eqz p2, :cond_2

    iget-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v3}, Lcom/android/incallui/view/ArrowImageView;->cancelAllAnimator()V

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->playArrowAnimator(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method public onViewTouchUp(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/view/CircleImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/android/incallui/view/CircleImageView;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v1, v4}, Lcom/android/incallui/view/CircleImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTextList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setSlidingView(Landroid/view/View;Z)V

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->cancelArrowAnimator(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/incallui/view/ArrowImageView;->playArrowRepeatTranslateAnimator(Z)V

    :cond_1
    return-void
.end method

.method public pauseCircleBounceAnimator()V
    .locals 6

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/view/CircleImageView;

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    invoke-virtual {v3, v2}, Lcom/android/incallui/view/CircleImageView;->getBounceAnimatorSet(I)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->pause()V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public playCircleBounceAnimator()V
    .locals 6

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/view/CircleImageView;

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/android/incallui/view/CircleImageView;->getBounceAnimatorSet(I)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isPaused()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->resume()V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/ArrowImageView;->playArrowRepeatTranslateAnimator(Z)V

    :cond_3
    return-void
.end method

.method public playCircleTranslateInAnimator()V
    .locals 8

    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [Landroid/animation/ObjectAnimator;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {p0, v3}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getTranslateInAnimatorSet(Lcom/android/incallui/view/CircleImageView;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    aput-object v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v5, Lcom/android/incallui/smartcover/b7/B7CoverFragment$2;

    invoke-direct {v5, p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment$2;-><init>(Lcom/android/incallui/smartcover/b7/B7CoverFragment;)V

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    iget-object v4, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public setPrimary(Lcom/android/incallui/model/CallCardInfo;ZZZZ)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-eqz p5, :cond_4

    iput-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isMtImsConference:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0b00a5

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    :cond_0
    :goto_1
    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, p4, v2}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setPrimaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    iget-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    invoke-direct {p0, v0, v1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->updatePhoneNumberField(Ljava/lang/String;Z)V

    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->setPrimaryTelocation(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    const v0, 0x7f0b00a4

    goto :goto_0

    :cond_2
    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v0}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iput-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    iput-boolean v4, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v0}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    goto :goto_1

    :cond_4
    if-nez p2, :cond_5

    if-eqz p3, :cond_0

    :cond_5
    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v0}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iput-boolean v4, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    goto :goto_1
.end method

.method public setPrimaryCallElapsedTime(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mElapsedTime:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/incallui/AnimationUtils$Fade;->show(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mElapsedTime:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPrimaryName(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPrimaryName:Landroid/widget/TextView;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_1

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPrimaryName:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPrimaryName:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {p1, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 v0, 0x0

    if-eqz p2, :cond_2

    const/4 v0, 0x3

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextDirection(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setPrimaryPhoneNumber(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPhoneNumber:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPhoneNumber:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPhoneNumber:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mPhoneNumber:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextDirection(I)V

    goto :goto_0
.end method

.method public setPrimaryTelocation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Telephony;->isTelocationEnable(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getLocation(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTelocation:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTelocation:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTelocation:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method setSlidingView(Landroid/view/View;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setTouchDragView(Landroid/view/View;Z)V

    return-void
.end method

.method public showAnswerUi(Z)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isFirstShow:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isShowingAnswerUI:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isShowingAnswerUI:Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-direct {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->getDefaultPanelHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setPanelHeight(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setSlidingUpLayoutPaddingTop(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->collapsePanelManual()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->playCircleTranslateInAnimator()V

    :goto_0
    iput-boolean v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isFirstShow:Z

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isFirstShow:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isShowingAnswerUI:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    return-void

    :cond_2
    iput-boolean v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->isShowingAnswerUI:Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setSlidingUpLayoutPaddingTop(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->expandPanelManual()V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->hideOperateItems()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTextList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public showHangUpButton(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mEndBtn:Landroid/widget/Button;

    const v1, 0x7f020088

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mEndBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mEndBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public showTelocation(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTelocation:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showTextButton()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTranslateInList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTranslateInList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mArrowList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mArrowList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTextList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTextList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mRejectText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mTextList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mAnswerText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void
.end method

.method public stopCircleBounceAnimator()V
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0}, Lcom/android/incallui/view/CircleImageView;->releaseBounceAnimatorSet()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->end()V

    iput-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    :cond_1
    return-void
.end method
