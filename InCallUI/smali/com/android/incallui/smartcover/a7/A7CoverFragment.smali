.class public Lcom/android/incallui/smartcover/a7/A7CoverFragment;
.super Lcom/android/incallui/BaseFragment;
.source "A7CoverFragment.java"

# interfaces
.implements Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/smartcover/a7/A7CoverPresenter;",
        "Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;",
        ">;",
        "Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;"
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mIsIncoming:Z

.field private mSlideLayout:Lcom/android/incallui/view/VerticalSlideLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mIsIncoming:Z

    return-void
.end method

.method private showNameOrPhoneNumber(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mSlideLayout:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/VerticalSlideLayout;->setPhoneName(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public configSmartCover(Lcom/android/incallui/model/CallCardInfo;ZZI)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    iget-boolean v2, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v2}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iput-boolean v4, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    const/4 v2, 0x0

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    if-nez p3, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0034

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/incallui/util/Utils;->localizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    :cond_1
    iget-boolean v2, p1, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mIsIncoming:Z

    if-nez v2, :cond_2

    iput-boolean v5, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mIsIncoming:Z

    :cond_2
    :goto_0
    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->showNameOrPhoneNumber(Ljava/lang/String;)V

    return-void

    :cond_3
    iget-boolean v2, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mIsIncoming:Z

    if-eqz v2, :cond_2

    iput-boolean v4, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mIsIncoming:Z

    goto :goto_0
.end method

.method public configSmartCoverEndCallSlide(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mSlideLayout:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->hideAnswerIndicator()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mSlideLayout:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->showAnswerIndicator()V

    goto :goto_0
.end method

.method protected bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->createPresenter()Lcom/android/incallui/smartcover/a7/A7CoverPresenter;

    move-result-object v0

    return-object v0
.end method

.method protected createPresenter()Lcom/android/incallui/smartcover/a7/A7CoverPresenter;
    .locals 1

    new-instance v0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;

    invoke-direct {v0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->getUi()Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;

    move-result-object v0

    return-object v0
.end method

.method protected getUi()Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;
    .locals 0

    return-object p0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mSlideLayout:Lcom/android/incallui/view/VerticalSlideLayout;

    new-instance v1, Lcom/android/incallui/smartcover/a7/A7CoverFragment$1;

    invoke-direct {v1, p0}, Lcom/android/incallui/smartcover/a7/A7CoverFragment$1;-><init>(Lcom/android/incallui/smartcover/a7/A7CoverFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/VerticalSlideLayout;->setOnSlideFinishListener(Lcom/android/incallui/view/VerticalSlideLayout$OnSlideFinishListener;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mSlideLayout:Lcom/android/incallui/view/VerticalSlideLayout;

    new-instance v1, Lcom/android/incallui/smartcover/a7/A7CoverFragment$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/smartcover/a7/A7CoverFragment$2;-><init>(Lcom/android/incallui/smartcover/a7/A7CoverFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/VerticalSlideLayout;->setOnSlideEndCallListener(Lcom/android/incallui/view/VerticalSlideLayout$OnEndCallSlideListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f030001

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onPause()V

    iget-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onResume()V

    iget-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/view/VerticalSlideLayout;

    iput-object p1, p0, Lcom/android/incallui/smartcover/a7/A7CoverFragment;->mSlideLayout:Lcom/android/incallui/view/VerticalSlideLayout;

    return-void
.end method
