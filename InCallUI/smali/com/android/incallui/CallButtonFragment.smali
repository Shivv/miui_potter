.class public Lcom/android/incallui/CallButtonFragment;
.super Lcom/android/incallui/BaseFragment;
.source "CallButtonFragment.java"

# interfaces
.implements Lcom/android/incallui/CallButtonPresenter$CallButtonUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/CallButtonFragment$1;,
        Lcom/android/incallui/CallButtonFragment$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/CallButtonPresenter;",
        "Lcom/android/incallui/CallButtonPresenter$CallButtonUi;",
        ">;",
        "Lcom/android/incallui/CallButtonPresenter$CallButtonUi;"
    }
.end annotation


# instance fields
.field private isCallButtonVisible:Z

.field private isInternationalVersion:Z

.field private mAlertDialog:Lmiui/app/AlertDialog;

.field private mAudioButton:Landroid/widget/TextView;

.field private mAudioButtonImage:Landroid/widget/ImageView;

.field private mAudioButtonLayout:Landroid/view/View;

.field private mContentView:Landroid/widget/RelativeLayout;

.field private mDialpadShowStatus:Z

.field private mDialpadShowed:Z

.field private mEndCallButton:Landroid/view/View;

.field private mInCallActivity:Lcom/android/incallui/InCallActivity;

.field private mMutiAudioImage:Landroid/widget/ImageView;

.field private mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

.field private mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mReducedCallButtonPaddingBottom:I

.field private mRotationAnimatorSet:Landroid/animation/AnimatorSet;

.field private mToolButton:Landroid/widget/TextView;

.field private mToolButtonImage:Landroid/widget/ImageView;

.field private mToolsButtonLayout:Landroid/view/View;

.field private mVideoCallButtonPaddingBottom:I

.field private mVideoConferenceManagerButton:Landroid/widget/TextView;

.field private mVoiceCallButtonPaddingBottom:I


# direct methods
.method static synthetic -get0(Lcom/android/incallui/CallButtonFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/CallButtonFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->mDialpadShowed:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/incallui/CallButtonFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/incallui/CallButtonFragment;)Lcom/android/incallui/InCallActivity;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/incallui/CallButtonFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolsButtonLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/CallButtonFragment;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/incallui/CallButtonFragment;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/incallui/CallButtonFragment;->isSupported(I)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/incallui/CallButtonFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/CallButtonFragment;->handleToolPadButton(Z)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/incallui/CallButtonFragment;ZZ[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/incallui/CallButtonFragment;->videoCallStateRecord(ZZ[Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isCallButtonVisible:Z

    invoke-static {}, Lcom/android/incallui/util/Utils;->isInternationalBuild()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    new-instance v0, Lcom/android/incallui/CallButtonFragment$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/CallButtonFragment$1;-><init>(Lcom/android/incallui/CallButtonFragment;)V

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    new-instance v0, Lcom/android/incallui/CallButtonFragment$2;

    invoke-direct {v0, p0}, Lcom/android/incallui/CallButtonFragment$2;-><init>(Lcom/android/incallui/CallButtonFragment;)V

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method private closeDialpad()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v2}, Lcom/android/incallui/CallButtonFragment;->handleToolPadButton(Z)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0, v1, v2}, Lcom/android/incallui/InCallActivity;->displayDialpad(ZZ)V

    iput-boolean v1, p0, Lcom/android/incallui/CallButtonFragment;->mDialpadShowStatus:Z

    return-void
.end method

.method private closeToolpad()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/incallui/CallButtonFragment;->handleToolPadButton(Z)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/android/incallui/InCallActivity;->displayToolPanel(ZZ)V

    return-void
.end method

.method private getAudioNameArray()[Ljava/lang/CharSequence;
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/android/incallui/CallButtonFragment;->isSupported(I)Z

    move-result v1

    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/CharSequence;

    const v2, 0x7f0b0026

    invoke-virtual {p0, v2}, Lcom/android/incallui/CallButtonFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    if-eqz v1, :cond_0

    const v2, 0x7f0b0029

    invoke-virtual {p0, v2}, Lcom/android/incallui/CallButtonFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    :goto_0
    const v2, 0x7f0b002a

    invoke-virtual {p0, v2}, Lcom/android/incallui/CallButtonFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v0, v3

    return-object v0

    :cond_0
    const v2, 0x7f0b0028

    invoke-virtual {p0, v2}, Lcom/android/incallui/CallButtonFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    goto :goto_0
.end method

.method private getCallAudioButtonView()Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    goto :goto_0
.end method

.method private getCallToolButtonView()Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    goto :goto_0
.end method

.method private getCurrentAudioIndex()I
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private handleToolPadButton(Z)V
    .locals 4

    const v3, 0x7f0b0070

    const v2, 0x7f0b006f

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    const v1, 0x7f020031

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    const v1, 0x7f020032

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    const v1, 0x7f0b00b0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    const v1, 0x7f0b00af

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private isAudio(I)Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->getAudioMode()I

    move-result v0

    and-int/2addr v0, p1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSupported(I)Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->getSupportedAudio()I

    move-result v0

    and-int/2addr v0, p1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showAudioSelectDialog()V
    .locals 4

    new-instance v1, Lmiui/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-direct {v1, v2}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b00b3

    invoke-virtual {v1, v2}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCurrentAudioIndex()I

    move-result v0

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getAudioNameArray()[Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/android/incallui/CallButtonFragment$6;

    invoke-direct {v3, p0}, Lcom/android/incallui/CallButtonFragment$6;-><init>(Lcom/android/incallui/CallButtonFragment;)V

    invoke-virtual {v1, v2, v0, v3}, Lmiui/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v2, Lcom/android/incallui/CallButtonFragment$7;

    invoke-direct {v2, p0}, Lcom/android/incallui/CallButtonFragment$7;-><init>(Lcom/android/incallui/CallButtonFragment;)V

    const v3, 0x7f0b0073

    invoke-virtual {v1, v3, v2}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v2, Lcom/android/incallui/CallButtonFragment$8;

    invoke-direct {v2, p0}, Lcom/android/incallui/CallButtonFragment$8;-><init>(Lcom/android/incallui/CallButtonFragment;)V

    invoke-virtual {v1, v2}, Lmiui/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v1}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/incallui/CallButtonFragment;->mAlertDialog:Lmiui/app/AlertDialog;

    iget-object v2, p0, Lcom/android/incallui/CallButtonFragment;->mAlertDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v2}, Lmiui/app/AlertDialog;->show()V

    return-void
.end method

.method private startCallButtonAnimator(Z)V
    .locals 11

    if-eqz p1, :cond_0

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    const-wide/16 v6, 0xfa

    invoke-virtual {v0, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v6, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v6}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    sget-object v7, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v9, v8, v10

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x1

    aput v9, v8, v10

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    sget-object v7, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    fill-array-data v8, :array_0

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    sget-object v7, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    fill-array-data v8, :array_1

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    sget-object v7, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v9, v8, v10

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x1

    aput v9, v8, v10

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mToolsButtonLayout:Landroid/view/View;

    sget-object v7, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v9, v8, v10

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x1

    aput v9, v8, v10

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const/4 v6, 0x5

    new-array v6, v6, [Landroid/animation/Animator;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    const/4 v7, 0x2

    aput-object v4, v6, v7

    const/4 v7, 0x3

    aput-object v1, v6, v7

    const/4 v7, 0x4

    aput-object v5, v6, v7

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    new-instance v6, Lcom/android/incallui/CallButtonFragment$9;

    invoke-direct {v6, p0}, Lcom/android/incallui/CallButtonFragment$9;-><init>(Lcom/android/incallui/CallButtonFragment;)V

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mToolsButtonLayout:Landroid/view/View;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v6}, Lcom/android/incallui/InCallActivity;->closeAllPanel()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private varargs videoCallStateRecord(ZZ[Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->isVtConferenceCall()Z

    move-result v1

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->isVideoCall()Z

    move-result v0

    invoke-static {p1, p2, v1, v0, p3}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordVideoCallCountEvent(ZZZZ[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public cancelDialog()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAlertDialog:Lmiui/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAlertDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mAlertDialog:Lmiui/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public changeContentViewPaddingBottom(Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/incallui/util/Utils;->isReducedCallButtonPaddingBottom()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mContentView:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/android/incallui/CallButtonFragment;->mReducedCallButtonPaddingBottom:I

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mContentView:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/android/incallui/CallButtonFragment;->mVideoCallButtonPaddingBottom:I

    :goto_1
    invoke-virtual {v1, v2, v2, v2, v0}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/incallui/CallButtonFragment;->mVoiceCallButtonPaddingBottom:I

    goto :goto_1
.end method

.method public checkToShowPanel()V
    .locals 5

    new-instance v0, Lcom/android/incallui/CallButtonFragment$3;

    invoke-direct {v0, p0}, Lcom/android/incallui/CallButtonFragment$3;-><init>(Lcom/android/incallui/CallButtonFragment;)V

    const-string/jumbo v1, "task_query_is_service_number"

    invoke-virtual {v0, v1}, Lcom/android/incallui/CallButtonFragment$3;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/android/incallui/util/SimpleTask;->run(JZLjava/lang/Object;)V

    return-void
.end method

.method protected createPresenter()Lcom/android/incallui/CallButtonPresenter;
    .locals 1

    new-instance v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-direct {v0}, Lcom/android/incallui/CallButtonPresenter;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->createPresenter()Lcom/android/incallui/CallButtonPresenter;

    move-result-object v0

    return-object v0
.end method

.method public displayDialpad(Z)V
    .locals 6

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->isDialpadVisible()Z

    move-result v1

    if-ne p1, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    xor-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v1}, Lcom/android/incallui/CallButtonFragment;->handleToolPadButton(Z)V

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->isToolPanelVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->getToolPanelHideTime()I

    move-result v0

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->closeToolpad()V

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    new-instance v2, Lcom/android/incallui/CallButtonFragment$5;

    invoke-direct {v2, p0, p1}, Lcom/android/incallui/CallButtonFragment$5;-><init>(Lcom/android/incallui/CallButtonFragment;Z)V

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    iput-boolean p1, p0, Lcom/android/incallui/CallButtonFragment;->mDialpadShowStatus:Z

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Lcom/android/incallui/InCallActivity;->displayDialpad(ZZ)V

    goto :goto_0
.end method

.method public displayToolPanel(Z)V
    .locals 6

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->isToolPanelVisible()Z

    move-result v1

    if-ne p1, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/incallui/CallButtonFragment;->handleToolPadButton(Z)V

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->isDialpadVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->getDialpadHideTime()I

    move-result v0

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->closeDialpad()V

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mToolsButtonLayout:Landroid/view/View;

    new-instance v2, Lcom/android/incallui/CallButtonFragment$4;

    invoke-direct {v2, p0, p1}, Lcom/android/incallui/CallButtonFragment$4;-><init>(Lcom/android/incallui/CallButtonFragment;Z)V

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Lcom/android/incallui/InCallActivity;->displayToolPanel(ZZ)V

    goto :goto_0
.end method

.method protected getUi()Lcom/android/incallui/CallButtonPresenter$CallButtonUi;
    .locals 0

    return-object p0
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getUi()Lcom/android/incallui/CallButtonPresenter$CallButtonUi;

    move-result-object v0

    return-object v0
.end method

.method public isCallButtonVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isCallButtonVisible:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/android/incallui/CallUtils;->isConferenceModeSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolsButtonLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onAudioButtonClick()V
    .locals 11

    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    invoke-direct {p0, v8}, Lcom/android/incallui/CallButtonFragment;->isSupported(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->showAudioSelectDialog()V

    :goto_0
    return-void

    :cond_0
    new-array v1, v8, [I

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallAudioButtonView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationInWindow([I)V

    iget-object v2, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-direct {p0, v10}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    aget v4, v1, v9

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallAudioButtonView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x3

    add-int/2addr v4, v5

    aget v5, v1, v7

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallAudioButtonView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/incallui/InCallActivity;->playAudioButtonClickAnim(ZII)V

    invoke-direct {p0, v10}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "video_call_conference_speaker"

    aput-object v3, v2, v9

    const-string/jumbo v3, "video_call_speaker"

    aput-object v3, v2, v7

    const-string/jumbo v3, "video_call_voice_speaker"

    aput-object v3, v2, v8

    invoke-direct {p0, v7, v7, v2}, Lcom/android/incallui/CallButtonFragment;->videoCallStateRecord(ZZ[Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v2}, Lcom/android/incallui/CallButtonPresenter;->onSpeakerClick()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/InCallActivity;

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f030007

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    const/4 v1, 0x4

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onDestroyView()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isCallButtonVisible:Z

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolsButtonLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onToolButtonClicked()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mInCallActivity:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->isToolPanelVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/CallButtonFragment;->displayDialpad(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/incallui/CallButtonFragment;->displayToolPanel(Z)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/incallui/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0a002d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mContentView:Landroid/widget/RelativeLayout;

    const v0, 0x7f0a002e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    const v0, 0x7f0a0030

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    const v0, 0x7f0a0031

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    const v0, 0x7f0a002f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    const v0, 0x7f0a0033

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolsButtonLayout:Landroid/view/View;

    const v0, 0x7f0a0034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    const v0, 0x7f0a0035

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    const v0, 0x7f0a0036

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mVideoConferenceManagerButton:Landroid/widget/TextView;

    const v0, 0x7f0a0032

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mMutiAudioImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/CallButtonFragment;->mVoiceCallButtonPaddingBottom:I

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/CallButtonFragment;->mVideoCallButtonPaddingBottom:I

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/CallButtonFragment;->mReducedCallButtonPaddingBottom:I

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolButton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallAudioButtonView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallToolButtonView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mVideoConferenceManagerButton:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public setVisible(Z)V
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "CBF.sV"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isCallButtonVisible:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isCallButtonVisible:Z

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mToolsButtonLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->checkToShowPanel()V

    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isCallButtonVisible:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/android/incallui/CallButtonFragment;->isCallButtonVisible:Z

    invoke-direct {p0, v1}, Lcom/android/incallui/CallButtonFragment;->startCallButtonAnimator(Z)V

    goto :goto_0
.end method

.method public setVisibleFullScreenVideo(Z)V
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->mDialpadShowStatus:Z

    :goto_0
    iput-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->mDialpadShowed:Z

    :cond_0
    xor-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lcom/android/incallui/CallButtonFragment;->startCallButtonAnimator(Z)V

    xor-int/lit8 v0, p1, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isCallButtonVisible:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startRotationAnimator(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mEndCallButton:Landroid/view/View;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallAudioButtonView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallToolButtonView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/incallui/CallButtonFragment;->mVideoConferenceManagerButton:Landroid/widget/TextView;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const/16 v1, 0x12c

    invoke-static {p1, v1, v0}, Lcom/android/incallui/AnimationUtils;->startRotationAnimator(II[Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    return-void
.end method

.method public updateAudioMode(I)V
    .locals 11

    const v10, 0x7f0b0026

    const v5, 0x7f02003f

    const/4 v9, 0x2

    const v8, 0x7f0b002a

    const/16 v7, 0x8

    invoke-direct {p0, v9}, Lcom/android/incallui/CallButtonFragment;->isSupported(I)Z

    move-result v3

    invoke-direct {p0, v7}, Lcom/android/incallui/CallButtonFragment;->isSupported(I)Z

    move-result v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v3, :cond_3

    const-string/jumbo v6, "updateAudioButton - popup menu mode"

    invoke-static {p0, v6}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v9}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    if-eqz v3, :cond_c

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mMutiAudioImage:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setSelected(Z)V

    if-eqz v0, :cond_6

    iget-boolean v5, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    const v6, 0x7f02002d

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_1
    invoke-direct {p0, v7}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x4

    invoke-direct {p0, v6}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    if-eqz v4, :cond_4

    invoke-direct {p0, v7}, Lcom/android/incallui/CallButtonFragment;->isAudio(I)Z

    move-result v2

    goto :goto_0

    :cond_4
    const-string/jumbo v6, "updateAudioButton - disabled..."

    invoke-static {p0, v6}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_6
    if-eqz v1, :cond_8

    iget-boolean v5, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    const v6, 0x7f020039

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b0029

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    const v6, 0x7f0b0029

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_8
    if-eqz v2, :cond_a

    iget-boolean v6, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_9
    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_a
    iget-boolean v5, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    const v6, 0x7f020036

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b0028

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_b
    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    const v6, 0x7f0b0028

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    :cond_c
    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->cancelDialog()V

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mMutiAudioImage:Landroid/widget/ImageView;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v6, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v6, :cond_e

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    if-eqz v2, :cond_d

    :goto_2
    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButtonImage:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_d
    const v5, 0x7f02003c

    goto :goto_2

    :cond_e
    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/android/incallui/CallButtonFragment;->mAudioButton:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_1
.end method

.method public updateToolLayoutButton()V
    .locals 3

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->getCallButtonMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mVideoConferenceManagerButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallToolButtonView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/android/incallui/CallButtonFragment;->isInternationalVersion:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mVideoConferenceManagerButton:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mVideoConferenceManagerButton:Landroid/widget/TextView;

    const v1, 0x7f020042

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mVideoConferenceManagerButton:Landroid/widget/TextView;

    const v1, 0x7f0b00b2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment;->mVideoConferenceManagerButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/incallui/CallButtonFragment;->getCallToolButtonView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
