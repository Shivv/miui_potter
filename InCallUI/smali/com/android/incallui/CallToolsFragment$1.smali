.class Lcom/android/incallui/CallToolsFragment$1;
.super Ljava/lang/Object;
.source "CallToolsFragment.java"

# interfaces
.implements Lcom/android/incallui/recorder/CallRecorderToolsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/CallToolsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/CallToolsFragment;


# direct methods
.method constructor <init>(Lcom/android/incallui/CallToolsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callRecordingFailed()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->isInCallToolPanelContentInit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->onCallRecordingStoped()V

    :cond_0
    return-void
.end method

.method public callRecordingStarted()V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->isInCallToolPanelContentInit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallToolsFragment;->onCallRecordingStarted(J)V

    :cond_0
    return-void
.end method

.method public callRecordingStopped(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->isInCallToolPanelContentInit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->onCallRecordingStoped()V

    :cond_0
    return-void
.end method

.method public recordTimeCallback(J)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->isInCallToolPanelContentInit()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/incallui/CallToolsFragment$1$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/incallui/CallToolsFragment$1$1;-><init>(Lcom/android/incallui/CallToolsFragment$1;J)V

    invoke-static {v0}, Lcom/android/incallui/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public stopManualAutoRecordIfNotStartedCallback()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->isInCallToolPanelContentInit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$1;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsFragment;->stopManualAutoRecordIfNotStarted()V

    :cond_0
    return-void
.end method
