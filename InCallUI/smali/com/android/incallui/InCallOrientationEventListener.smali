.class public Lcom/android/incallui/InCallOrientationEventListener;
.super Landroid/view/OrientationEventListener;
.source "InCallOrientationEventListener.java"


# static fields
.field private static ROTATION_THRESHOLD:I

.field private static SCREEN_ORIENTATION_UNKNOWN:I

.field private static mCurrentOrientation:I


# instance fields
.field private mEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/android/incallui/InCallOrientationEventListener;->SCREEN_ORIENTATION_UNKNOWN:I

    const/16 v0, 0xa

    sput v0, Lcom/android/incallui/InCallOrientationEventListener;->ROTATION_THRESHOLD:I

    const/4 v0, 0x0

    sput v0, Lcom/android/incallui/InCallOrientationEventListener;->mCurrentOrientation:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/InCallOrientationEventListener;->mEnabled:Z

    return-void
.end method

.method public static getCurrentOrientation()I
    .locals 1

    sget v0, Lcom/android/incallui/InCallOrientationEventListener;->mCurrentOrientation:I

    return v0
.end method

.method public static initCurrentOrientation()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/incallui/InCallOrientationEventListener;->mCurrentOrientation:I

    return-void
.end method

.method private static isInLeftRange(III)Z
    .locals 1

    sub-int v0, p1, p2

    invoke-static {p0, v0, p1}, Lcom/android/incallui/InCallOrientationEventListener;->isWithinRange(III)Z

    move-result v0

    return v0
.end method

.method private static isInRightRange(III)Z
    .locals 1

    add-int v0, p1, p2

    invoke-static {p0, p1, v0}, Lcom/android/incallui/InCallOrientationEventListener;->isWithinRange(III)Z

    move-result v0

    return v0
.end method

.method private static isWithinRange(III)Z
    .locals 1

    const/4 v0, 0x0

    if-lt p0, p1, :cond_0

    if-ge p0, p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static isWithinThreshold(III)Z
    .locals 2

    sub-int v0, p1, p2

    add-int v1, p1, p2

    invoke-static {p0, v0, v1}, Lcom/android/incallui/InCallOrientationEventListener;->isWithinRange(III)Z

    move-result v0

    return v0
.end method

.method private toScreenOrientation(I)I
    .locals 6

    const/16 v5, 0x10e

    const/16 v4, 0xb4

    const/16 v3, 0x5a

    const/4 v2, 0x0

    sget v0, Lcom/android/incallui/InCallOrientationEventListener;->ROTATION_THRESHOLD:I

    const/16 v1, 0x168

    invoke-static {p1, v1, v0}, Lcom/android/incallui/InCallOrientationEventListener;->isInLeftRange(III)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/android/incallui/InCallOrientationEventListener;->ROTATION_THRESHOLD:I

    invoke-static {p1, v2, v0}, Lcom/android/incallui/InCallOrientationEventListener;->isInRightRange(III)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return v2

    :cond_1
    sget v0, Lcom/android/incallui/InCallOrientationEventListener;->ROTATION_THRESHOLD:I

    invoke-static {p1, v3, v0}, Lcom/android/incallui/InCallOrientationEventListener;->isWithinThreshold(III)Z

    move-result v0

    if-eqz v0, :cond_2

    return v5

    :cond_2
    sget v0, Lcom/android/incallui/InCallOrientationEventListener;->ROTATION_THRESHOLD:I

    invoke-static {p1, v4, v0}, Lcom/android/incallui/InCallOrientationEventListener;->isWithinThreshold(III)Z

    move-result v0

    if-eqz v0, :cond_3

    return v4

    :cond_3
    sget v0, Lcom/android/incallui/InCallOrientationEventListener;->ROTATION_THRESHOLD:I

    invoke-static {p1, v5, v0}, Lcom/android/incallui/InCallOrientationEventListener;->isWithinThreshold(III)Z

    move-result v0

    if-eqz v0, :cond_4

    return v3

    :cond_4
    sget v0, Lcom/android/incallui/InCallOrientationEventListener;->SCREEN_ORIENTATION_UNKNOWN:I

    return v0
.end method


# virtual methods
.method public disable()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/InCallOrientationEventListener;->mEnabled:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "enable: Orientation listener is already disabled. Ignoring..."

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/InCallOrientationEventListener;->mEnabled:Z

    invoke-super {p0}, Landroid/view/OrientationEventListener;->disable()V

    return-void
.end method

.method public enable()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallOrientationEventListener;->enable(Z)V

    return-void
.end method

.method public enable(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/incallui/InCallOrientationEventListener;->mEnabled:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "enable: Orientation listener is already enabled. Ignoring..."

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-super {p0}, Landroid/view/OrientationEventListener;->enable()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallOrientationEventListener;->mEnabled:Z

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    sget v1, Lcom/android/incallui/InCallOrientationEventListener;->mCurrentOrientation:I

    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallPresenter;->onDeviceOrientationChange(I)V

    :cond_1
    return-void
.end method

.method public onOrientationChanged(I)V
    .locals 3

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const-string/jumbo v1, "onOrientationChanged - Unknown orientation. Return"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/incallui/InCallOrientationEventListener;->toScreenOrientation(I)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onOrientationChanged currentOrientation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/incallui/InCallOrientationEventListener;->mCurrentOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " newOrientation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/android/incallui/InCallOrientationEventListener;->SCREEN_ORIENTATION_UNKNOWN:I

    if-eq v0, v1, :cond_1

    sget v1, Lcom/android/incallui/InCallOrientationEventListener;->mCurrentOrientation:I

    if-eq v1, v0, :cond_1

    sput v0, Lcom/android/incallui/InCallOrientationEventListener;->mCurrentOrientation:I

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    sget v2, Lcom/android/incallui/InCallOrientationEventListener;->mCurrentOrientation:I

    invoke-virtual {v1, v2}, Lcom/android/incallui/InCallPresenter;->onDeviceOrientationChange(I)V

    :cond_1
    return-void
.end method
