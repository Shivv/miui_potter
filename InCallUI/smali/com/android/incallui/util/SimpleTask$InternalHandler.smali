.class Lcom/android/incallui/util/SimpleTask$InternalHandler;
.super Landroid/os/Handler;
.source "SimpleTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/util/SimpleTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InternalHandler"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/incallui/util/SimpleTask$SimpleTaskResult;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, v0, Lcom/android/incallui/util/SimpleTask$SimpleTaskResult;->mTask:Lcom/android/incallui/util/SimpleTask;

    iget-object v2, v0, Lcom/android/incallui/util/SimpleTask$SimpleTaskResult;->mData:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/android/incallui/util/SimpleTask;->-wrap1(Lcom/android/incallui/util/SimpleTask;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
