.class public Lcom/android/incallui/util/PermissionGrantHelper;
.super Ljava/lang/Object;
.source "PermissionGrantHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/incallui/util/PermissionGrantHelper;


# instance fields
.field private BASE_RUNTIME_REQUEST_CODE:I

.field private listeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/incallui/util/PermissionGrantHelper;->sInstance:Lcom/android/incallui/util/PermissionGrantHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/incallui/util/PermissionGrantHelper;->BASE_RUNTIME_REQUEST_CODE:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/util/PermissionGrantHelper;->listeners:Landroid/util/SparseArray;

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/android/incallui/util/PermissionGrantHelper;
    .locals 3

    const-class v1, Lcom/android/incallui/util/PermissionGrantHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/incallui/util/PermissionGrantHelper;->sInstance:Lcom/android/incallui/util/PermissionGrantHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/incallui/util/PermissionGrantHelper;

    invoke-direct {v0}, Lcom/android/incallui/util/PermissionGrantHelper;-><init>()V

    sput-object v0, Lcom/android/incallui/util/PermissionGrantHelper;->sInstance:Lcom/android/incallui/util/PermissionGrantHelper;

    sget-object v0, Lcom/android/incallui/util/PermissionGrantHelper;->sInstance:Lcom/android/incallui/util/PermissionGrantHelper;

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, v0, Lcom/android/incallui/util/PermissionGrantHelper;->listeners:Landroid/util/SparseArray;

    :cond_0
    sget-object v0, Lcom/android/incallui/util/PermissionGrantHelper;->sInstance:Lcom/android/incallui/util/PermissionGrantHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static release()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/android/incallui/util/PermissionGrantHelper;->sInstance:Lcom/android/incallui/util/PermissionGrantHelper;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/util/PermissionGrantHelper;->sInstance:Lcom/android/incallui/util/PermissionGrantHelper;

    iput-object v1, v0, Lcom/android/incallui/util/PermissionGrantHelper;->listeners:Landroid/util/SparseArray;

    :cond_0
    sput-object v1, Lcom/android/incallui/util/PermissionGrantHelper;->sInstance:Lcom/android/incallui/util/PermissionGrantHelper;

    return-void
.end method


# virtual methods
.method public varargs checkPermissions(Landroid/app/Activity;Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;[Ljava/lang/String;)V
    .locals 7

    const/4 v5, 0x0

    if-eqz p1, :cond_0

    iget-object v6, p0, Lcom/android/incallui/util/PermissionGrantHelper;->listeners:Landroid/util/SparseArray;

    if-nez v6, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    array-length v6, p3

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    array-length v6, p3

    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v1, p3, v5

    invoke-virtual {p0, p1, v1}, Lcom/android/incallui/util/PermissionGrantHelper;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/incallui/util/PermissionGrantHelper;->getRequestCode()I

    move-result v2

    iget-object v5, p0, Lcom/android/incallui/util/PermissionGrantHelper;->listeners:Landroid/util/SparseArray;

    invoke-virtual {v5, v2, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p0, p1, v3, v2}, Lcom/android/incallui/util/PermissionGrantHelper;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    :goto_1
    return-void

    :cond_4
    invoke-interface {p2}, Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;->onPermissionGranted()V

    goto :goto_1
.end method

.method public checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {p1, p2}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getRequestCode()I
    .locals 2

    iget v0, p0, Lcom/android/incallui/util/PermissionGrantHelper;->BASE_RUNTIME_REQUEST_CODE:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/incallui/util/PermissionGrantHelper;->BASE_RUNTIME_REQUEST_CODE:I

    return v0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 5

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/incallui/util/PermissionGrantHelper;->listeners:Landroid/util/SparseArray;

    if-nez v4, :cond_0

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/util/PermissionGrantHelper;->listeners:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;

    if-nez v2, :cond_1

    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/incallui/util/PermissionGrantHelper;->listeners:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->remove(I)V

    const/4 v1, 0x0

    array-length v4, p3

    :goto_0
    if-ge v3, v4, :cond_2

    aget v0, p3, v3

    if-nez v0, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_4

    :cond_2
    if-eqz v1, :cond_5

    invoke-interface {v2}, Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;->onPermissionGranted()V

    :goto_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    invoke-interface {v2}, Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;->onPermissionDenied()V

    goto :goto_2
.end method

.method public requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V
    .locals 0

    invoke-static {p1, p2, p3}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    return-void
.end method
