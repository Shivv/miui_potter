.class public Lcom/android/incallui/util/ThreadUtil;
.super Ljava/lang/Object;
.source "ThreadUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/util/ThreadUtil$HandlerHolder;,
        Lcom/android/incallui/util/ThreadUtil$WorkHandler;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getUiThreadHandler()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/android/incallui/util/ThreadUtil$HandlerHolder;->sInstance:Landroid/os/Handler;

    return-object v0
.end method

.method public static getWorkHandlerLooper()Landroid/os/Looper;
    .locals 1

    invoke-static {}, Lcom/android/incallui/util/ThreadUtil$WorkHandler;->getInstance()Lcom/android/incallui/util/ThreadUtil$WorkHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/util/ThreadUtil$WorkHandler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public static postDelayedOnUiThread(Ljava/lang/Runnable;J)V
    .locals 1

    invoke-static {}, Lcom/android/incallui/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static postOnUiThread(Ljava/lang/Runnable;)V
    .locals 1

    invoke-static {}, Lcom/android/incallui/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
