.class Lcom/android/incallui/util/CallStatisticUtil$1;
.super Landroid/os/Handler;
.source "CallStatisticUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/util/CallStatisticUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/util/CallStatisticUtil;


# direct methods
.method constructor <init>(Lcom/android/incallui/util/CallStatisticUtil;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/util/CallStatisticUtil$1;->this$0:Lcom/android/incallui/util/CallStatisticUtil;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimeoutOfDisconnectingToDisconnectedCountEvent()V

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil$1;->this$0:Lcom/android/incallui/util/CallStatisticUtil;

    invoke-static {v1}, Lcom/android/incallui/util/CallStatisticUtil;->-get1(Lcom/android/incallui/util/CallStatisticUtil;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil$1;->this$0:Lcom/android/incallui/util/CallStatisticUtil;

    invoke-static {v1}, Lcom/android/incallui/util/CallStatisticUtil;->-get0(Lcom/android/incallui/util/CallStatisticUtil;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimeOutOfAnswer()V

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil$1;->this$0:Lcom/android/incallui/util/CallStatisticUtil;

    invoke-static {v1, v0}, Lcom/android/incallui/util/CallStatisticUtil;->-wrap0(Lcom/android/incallui/util/CallStatisticUtil;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil$1;->this$0:Lcom/android/incallui/util/CallStatisticUtil;

    invoke-static {v1}, Lcom/android/incallui/util/CallStatisticUtil;->-get0(Lcom/android/incallui/util/CallStatisticUtil;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimeOutOfReject()V

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil$1;->this$0:Lcom/android/incallui/util/CallStatisticUtil;

    invoke-static {v1, v0}, Lcom/android/incallui/util/CallStatisticUtil;->-wrap0(Lcom/android/incallui/util/CallStatisticUtil;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
