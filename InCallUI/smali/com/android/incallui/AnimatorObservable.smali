.class public Lcom/android/incallui/AnimatorObservable;
.super Ljava/lang/Object;
.source "AnimatorObservable.java"

# interfaces
.implements Lcom/android/incallui/OnAnimatorChanged;


# static fields
.field private static mInstance:Lcom/android/incallui/AnimatorObservable;


# instance fields
.field private mAnimatorListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/incallui/OnAnimatorChanged;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/incallui/AnimatorObservable;->mInstance:Lcom/android/incallui/AnimatorObservable;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    return-void
.end method

.method public static createInstacne()Lcom/android/incallui/AnimatorObservable;
    .locals 2

    const-class v1, Lcom/android/incallui/AnimatorObservable;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/incallui/AnimatorObservable;->mInstance:Lcom/android/incallui/AnimatorObservable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/incallui/AnimatorObservable;

    invoke-direct {v0}, Lcom/android/incallui/AnimatorObservable;-><init>()V

    sput-object v0, Lcom/android/incallui/AnimatorObservable;->mInstance:Lcom/android/incallui/AnimatorObservable;

    :cond_0
    sget-object v0, Lcom/android/incallui/AnimatorObservable;->mInstance:Lcom/android/incallui/AnimatorObservable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onAnimatorPause()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/OnAnimatorChanged;

    invoke-interface {v1}, Lcom/android/incallui/OnAnimatorChanged;->onAnimatorPause()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onAnimatorResume()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/OnAnimatorChanged;

    invoke-interface {v1}, Lcom/android/incallui/OnAnimatorChanged;->onAnimatorResume()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onViewMove(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/OnAnimatorChanged;

    invoke-interface {v1, p1}, Lcom/android/incallui/OnAnimatorChanged;->onViewMove(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onViewTouchDown(Landroid/view/View;Z)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/OnAnimatorChanged;

    invoke-interface {v1, p1, p2}, Lcom/android/incallui/OnAnimatorChanged;->onViewTouchDown(Landroid/view/View;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onViewTouchUp(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/OnAnimatorChanged;

    invoke-interface {v1, p1}, Lcom/android/incallui/OnAnimatorChanged;->onViewTouchUp(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public registerListener(Lcom/android/incallui/OnAnimatorChanged;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_1

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public unRegisterListener(Lcom/android/incallui/OnAnimatorChanged;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/AnimatorObservable;->mAnimatorListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
