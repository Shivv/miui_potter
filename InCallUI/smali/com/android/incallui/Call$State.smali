.class public Lcom/android/incallui/Call$State;
.super Ljava/lang/Object;
.source "Call.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/Call;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "State"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDialing(I)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x6

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMiuiDialing(I)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x6

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0xd

    if-eq p0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRinging(I)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string/jumbo v0, "UNKNOWN"

    return-object v0

    :pswitch_0
    const-string/jumbo v0, "INVALID"

    return-object v0

    :pswitch_1
    const-string/jumbo v0, "NEW"

    return-object v0

    :pswitch_2
    const-string/jumbo v0, "IDLE"

    return-object v0

    :pswitch_3
    const-string/jumbo v0, "ACTIVE"

    return-object v0

    :pswitch_4
    const-string/jumbo v0, "INCOMING"

    return-object v0

    :pswitch_5
    const-string/jumbo v0, "CALL_WAITING"

    return-object v0

    :pswitch_6
    const-string/jumbo v0, "DIALING"

    return-object v0

    :pswitch_7
    const-string/jumbo v0, "REDIALING"

    return-object v0

    :pswitch_8
    const-string/jumbo v0, "ONHOLD"

    return-object v0

    :pswitch_9
    const-string/jumbo v0, "DISCONNECTING"

    return-object v0

    :pswitch_a
    const-string/jumbo v0, "DISCONNECTED"

    return-object v0

    :pswitch_b
    const-string/jumbo v0, "CONFERENCED"

    return-object v0

    :pswitch_c
    const-string/jumbo v0, "PRE_DIAL_WAIT"

    return-object v0

    :pswitch_d
    const-string/jumbo v0, "CONNECTING"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
