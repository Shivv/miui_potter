.class Lcom/android/incallui/Call$3;
.super Lcom/android/incallui/util/SimpleTask;
.source "Call.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/Call;->initFromTelecommCall()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/util/SimpleTask",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/Call;


# direct methods
.method constructor <init>(Lcom/android/incallui/Call;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/Call$3;->this$0:Lcom/android/incallui/Call;

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/Call$3;->doInBackground()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/lang/Void;
    .locals 1

    const/4 v0, -0x2

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    iget-object v0, p0, Lcom/android/incallui/Call$3;->this$0:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/Call;->-wrap1(Lcom/android/incallui/Call;)V

    iget-object v0, p0, Lcom/android/incallui/Call$3;->this$0:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/Call;->-wrap3(Lcom/android/incallui/Call;)V

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/incallui/Call$3;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/Call$3;->this$0:Lcom/android/incallui/Call;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/incallui/Call;->-set0(Lcom/android/incallui/Call;Z)Z

    iget-object v0, p0, Lcom/android/incallui/Call$3;->this$0:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/Call;->-wrap0(Lcom/android/incallui/Call;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/Call$3;->this$0:Lcom/android/incallui/Call;

    invoke-virtual {v0, v1}, Lcom/android/incallui/CallList;->onCallInitialized(Lcom/android/incallui/Call;)V

    iget-object v0, p0, Lcom/android/incallui/Call$3;->this$0:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/Call;->-get1(Lcom/android/incallui/Call;)Landroid/telecom/Call;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call;->getParent()Landroid/telecom/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/incallui/Call$3$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/Call$3$1;-><init>(Lcom/android/incallui/Call$3;)V

    invoke-static {v0}, Lcom/android/incallui/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
