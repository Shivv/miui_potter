.class Lcom/android/incallui/CallToolsFragment$3;
.super Ljava/lang/Object;
.source "CallToolsFragment.java"

# interfaces
.implements Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/CallToolsFragment;->checkPermissionOrPerformRecord()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/CallToolsFragment;


# direct methods
.method constructor <init>(Lcom/android/incallui/CallToolsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallToolsFragment$3;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPermissionDenied()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$3;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v0}, Lcom/android/incallui/CallToolsFragment;->-get3(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$3;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v0}, Lcom/android/incallui/CallToolsFragment;->-get3(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    :cond_0
    const v0, 0x7f0b008d

    invoke-static {v0}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    return-void
.end method

.method public onPermissionGranted()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$3;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v0}, Lcom/android/incallui/CallToolsFragment;->-get3(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$3;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v0, v3}, Lcom/android/incallui/CallToolsFragment;->-set0(Lcom/android/incallui/CallToolsFragment;Z)Z

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$3;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v0, v3}, Lcom/android/incallui/CallToolsFragment;->-wrap1(Lcom/android/incallui/CallToolsFragment;Z)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$3;->this$0:Lcom/android/incallui/CallToolsFragment;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "video_call_conference_record"

    aput-object v2, v1, v4

    const-string/jumbo v2, "video_call_voice_record"

    aput-object v2, v1, v3

    invoke-static {v0, v3, v4, v1}, Lcom/android/incallui/CallToolsFragment;->-wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment$3;->this$0:Lcom/android/incallui/CallToolsFragment;

    invoke-static {v0, v4}, Lcom/android/incallui/CallToolsFragment;->-wrap1(Lcom/android/incallui/CallToolsFragment;Z)V

    goto :goto_0
.end method
