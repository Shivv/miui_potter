.class public Lcom/android/incallui/model/CallCardInfo;
.super Ljava/lang/Object;
.source "CallCardInfo.java"


# instance fields
.field public company:Ljava/lang/String;

.field public extraInfo:Ljava/lang/String;

.field public fullDialString:Ljava/lang/String;

.field public isIncoming:Z

.field public isMtImsConference:Z

.field public isSpeechCodecHD:Z

.field public isSuspect:Z

.field public isVideoConference:Z

.field public isYellowPage:Z

.field public leftPostDialString:Ljava/lang/String;

.field public markCount:Ljava/lang/String;

.field public markProviderIcon:Landroid/graphics/Bitmap;

.field public markTitle:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public nameIsNumber:Z

.field public phoneNumber:Ljava/lang/String;

.field public phoneTag:Ljava/lang/String;

.field public photo:Landroid/graphics/drawable/Drawable;

.field public simIndicatorResId:I

.field public speechHDResId:I

.field public telocation:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    iput v0, p0, Lcom/android/incallui/model/CallCardInfo;->speechHDResId:I

    return-void
.end method

.method public static createCallCardInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/model/CallCardInfo;
    .locals 11

    const/4 v10, 0x1

    const/4 v8, 0x0

    invoke-static {}, Lcom/android/incallui/model/CallCardInfo;->createEmptyCardInfo()Lcom/android/incallui/model/CallCardInfo;

    move-result-object v0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getState()I

    move-result v7

    invoke-static {v7}, Lcom/android/incallui/Call$State;->isRinging(I)Z

    move-result v2

    :goto_0
    if-eqz p1, :cond_0

    if-nez p0, :cond_2

    :cond_0
    iput-boolean v2, v0, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    iget-boolean v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isEmergencyNumber:Z

    if-eqz v7, :cond_9

    iget-object v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->number:Ljava/lang/String;

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->number:Ljava/lang/String;

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->fullDialString:Ljava/lang/String;

    :goto_1
    iget-object v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_d

    iget-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    :goto_2
    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->location:Ljava/lang/String;

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->company:Ljava/lang/String;

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->company:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getLeftPostDialString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->leftPostDialString:Ljava/lang/String;

    invoke-static {p0}, Lcom/android/incallui/CallAdapterUtils;->isSpeechCodecHD(Lcom/android/incallui/Call;)Z

    move-result v7

    if-eqz v7, :cond_e

    iget-boolean v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isEmergencyNumber:Z

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_e

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v7

    invoke-static {v7}, Lcom/android/incallui/CallAdapterUtils;->isVideo(I)Z

    move-result v7

    xor-int/lit8 v7, v7, 0x1

    :goto_3
    iput-boolean v7, v0, Lcom/android/incallui/model/CallCardInfo;->isSpeechCodecHD:Z

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getSlotId()I

    move-result v4

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v1

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/telephony/TelephonyManager;->getIccCardCount()I

    move-result v7

    if-le v7, v10, :cond_10

    invoke-static {v4}, Lmiui/telephony/SubscriptionManager;->isValidSlotId(I)Z

    move-result v7

    if-eqz v7, :cond_10

    if-nez v4, :cond_f

    const v7, 0x7f02007b

    :goto_4
    iput v7, v0, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    :goto_5
    iget-boolean v7, v0, Lcom/android/incallui/model/CallCardInfo;->isSpeechCodecHD:Z

    if-eqz v7, :cond_3

    const-string/jumbo v7, "46001"

    invoke-static {v4, v7}, Lcom/android/incallui/CallUtils;->isSameOperator(ILjava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_11

    const v7, 0x7f020090

    iput v7, v0, Lcom/android/incallui/model/CallCardInfo;->speechHDResId:I

    :cond_3
    :goto_6
    invoke-virtual {p0}, Lcom/android/incallui/Call;->isForwardedCall()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0b003a

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->extraInfo:Ljava/lang/String;

    :cond_4
    iget-object v6, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    if-eqz v6, :cond_8

    iput-boolean v10, v0, Lcom/android/incallui/model/CallCardInfo;->isYellowPage:Z

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->isAntispam()Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->isUserMarked()Z

    move-result v7

    if-eqz v7, :cond_7

    :cond_5
    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getTag()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->markTitle:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getProviderIcon()Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->markProviderIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->isAntispam()Z

    move-result v7

    if-eqz v7, :cond_6

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getMarkedCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const v9, 0x7f0b008f

    invoke-virtual {v1, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->markCount:Ljava/lang/String;

    :cond_6
    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->isUserMarked()Z

    move-result v7

    if-eqz v7, :cond_7

    const v7, 0x7f0b0090

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->markCount:Ljava/lang/String;

    :cond_7
    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->isYellowPage()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getTag()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getYellowPageName()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getIsIncoming()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getSlogan()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_12

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getSlogan()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->company:Ljava/lang/String;

    :cond_8
    :goto_7
    iget-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-object v8, v0, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    iput-boolean v7, v0, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-boolean v2, v0, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    iget-object v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->photo:Landroid/graphics/drawable/Drawable;

    const-string/jumbo v7, "incoming"

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getImsConferenceType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iput-boolean v7, v0, Lcom/android/incallui/model/CallCardInfo;->isMtImsConference:Z

    invoke-static {p0}, Lcom/android/incallui/CallUtils;->isVideoConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v7

    iput-boolean v7, v0, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    const-string/jumbo v7, "CallCardInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "createCallCardInfo:  markTitle:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/incallui/model/CallCardInfo;->markTitle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " markCount:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/incallui/model/CallCardInfo;->markCount:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " phoneTag:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " extraInfo:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/incallui/model/CallCardInfo;->extraInfo:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " isSupect:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, v0, Lcom/android/incallui/model/CallCardInfo;->isSuspect:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_9
    iget-boolean v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isVioceMailNumber:Z

    if-eqz v7, :cond_b

    iget-object v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->number:Ljava/lang/String;

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getPostDialString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_8
    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->fullDialString:Ljava/lang/String;

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v7, ""

    goto :goto_8

    :cond_b
    invoke-virtual {p0}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_c

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/incallui/Call;->getPostDialString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_9
    iput-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->fullDialString:Ljava/lang/String;

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v7, ""

    goto :goto_9

    :cond_d
    iget-object v7, p1, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    goto/16 :goto_2

    :cond_e
    move v7, v8

    goto/16 :goto_3

    :cond_f
    const v7, 0x7f02007c

    goto/16 :goto_4

    :cond_10
    const/4 v7, -0x1

    iput v7, v0, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    goto/16 :goto_5

    :cond_11
    const v7, 0x7f020091

    iput v7, v0, Lcom/android/incallui/model/CallCardInfo;->speechHDResId:I

    goto/16 :goto_6

    :cond_12
    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getProviderName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v3, v7, v8

    const v8, 0x7f0b0091

    invoke-virtual {v1, v8, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/incallui/model/CallCardInfo;->company:Ljava/lang/String;

    goto/16 :goto_7
.end method

.method public static createEmptyCardInfo()Lcom/android/incallui/model/CallCardInfo;
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v0, Lcom/android/incallui/model/CallCardInfo;

    invoke-direct {v0}, Lcom/android/incallui/model/CallCardInfo;-><init>()V

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->extraInfo:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->company:Ljava/lang/String;

    iput-boolean v2, v0, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->photo:Landroid/graphics/drawable/Drawable;

    iput-boolean v2, v0, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    iput v3, v0, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->leftPostDialString:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->fullDialString:Ljava/lang/String;

    iput-boolean v2, v0, Lcom/android/incallui/model/CallCardInfo;->isSpeechCodecHD:Z

    iput v3, v0, Lcom/android/incallui/model/CallCardInfo;->speechHDResId:I

    iput-boolean v2, v0, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    return-object v0
.end method

.method public static getCardInfoPhotoType(Landroid/graphics/drawable/Drawable;)I
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/incallui/ContactInfoCache;->isBigPhoto(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " { "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nname: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nphoneNumber: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    invoke-static {v1}, Lmiui/telephony/PhoneNumberUtils;->toLogSafePhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\ntelocation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nextraInfo: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/model/CallCardInfo;->extraInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nmarkTitle: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/model/CallCardInfo;->markTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nmarkCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/model/CallCardInfo;->markCount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nphoneTag: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nisSuspect: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/incallui/model/CallCardInfo;->isSuspect:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
