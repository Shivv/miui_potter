.class public Lcom/android/incallui/view/VoLTEVideoIncomingLayout;
.super Landroid/widget/FrameLayout;
.source "VoLTEVideoIncomingLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/VoLTEVideoIncomingLayout$TextureVideoViewOutlineProvider;
    }
.end annotation


# instance fields
.field private mAnimatorSet:Landroid/animation/AnimatorSet;

.field private mBottomExtraHeight:F

.field private mCallButtonHeight:F

.field private mCallButtonMarginTop:F

.field private mCallButtonPaddingBottom:F

.field private mIsCrbt:Z

.field private mIsDialpad:Z

.field private mNewTextureViewHeight:F

.field private mNewTextureViewWidth:F

.field private mScreenHeight:F

.field private mScreenWidth:F

.field private mSingleCallInfoHeight:F

.field private mTextView:Landroid/widget/TextView;

.field private mTextViewMarginTop:F

.field private mTextureView:Landroid/view/TextureView;

.field private mTextureViewCurrentPosition:I

.field private mTextureViewHeight:F

.field private mTextureViewMaxHeight:F

.field private mTextureViewMaxWidth:F

.field private mTextureViewPosition1MarginTop:F

.field private mTextureViewPosition2MarginTop:F

.field private mTextureViewPosition3MarginTop:F

.field private mTextureViewStandardMargin:F

.field private mTextureViewWidth:F


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mIsCrbt:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mIsDialpad:Z

    return v0
.end method

.method static synthetic -get3(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewHeight:F

    return v0
.end method

.method static synthetic -get4(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewWidth:F

    return v0
.end method

.method static synthetic -get5(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/view/TextureView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewCurrentPosition:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mIsCrbt:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mIsDialpad:Z

    return p1
.end method

.method static synthetic -set2(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;F)F
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewHeight:F

    return p1
.end method

.method static synthetic -set3(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;F)F
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewWidth:F

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->moveInComingAnimator(II)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->setVideoCrbtTitle(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->updateBottomExtraHeight(Z)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->updateNewWidthHeight()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->updateSingleCallInfoHeight()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->init()V

    return-void
.end method

.method private moveInComingAnimator(II)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    if-nez v14, :cond_0

    return-void

    :cond_0
    const-string/jumbo v14, "VoLTEVideoInComingLayout"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "moveInComingAnimator position = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v14}, Landroid/view/TextureView;->getY()F

    move-result v11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getY()F

    move-result v5

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-direct/range {p0 .. p0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->updateNewWidthHeight()V

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewWidth:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewWidth:F

    cmpl-float v14, v14, v15

    if-lez v14, :cond_3

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewWidth:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewWidth:F

    div-float v10, v14, v15

    move v8, v10

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_0
    packed-switch p1, :pswitch_data_0

    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v14}, Landroid/view/TextureView;->getY()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getY()F

    move-result v4

    const/4 v14, 0x0

    cmpg-float v14, v6, v14

    if-gez v14, :cond_1

    const/4 v6, 0x0

    :cond_1
    const/4 v14, 0x0

    cmpg-float v14, v4, v14

    if-gez v14, :cond_2

    const/4 v4, 0x0

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    sget-object v15, Landroid/view/View;->Y:Landroid/util/Property;

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput v6, v16, v17

    const/16 v17, 0x1

    aput v11, v16, v17

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextView:Landroid/widget/TextView;

    sget-object v15, Landroid/view/View;->Y:Landroid/util/Property;

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput v4, v16, v17

    const/16 v17, 0x1

    aput v5, v16, v17

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    sget-object v15, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput v8, v16, v17

    const/16 v17, 0x1

    aput v7, v16, v17

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    sget-object v15, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput v10, v16, v17

    const/16 v17, 0x1

    aput v9, v16, v17

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mAnimatorSet:Landroid/animation/AnimatorSet;

    const/4 v15, 0x4

    new-array v15, v15, [Landroid/animation/Animator;

    const/16 v16, 0x0

    aput-object v12, v15, v16

    const/16 v16, 0x1

    aput-object v2, v15, v16

    const/16 v16, 0x2

    aput-object v3, v15, v16

    const/16 v16, 0x3

    aput-object v13, v15, v16

    invoke-virtual {v14, v15}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mAnimatorSet:Landroid/animation/AnimatorSet;

    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v14

    invoke-virtual {v14}, Landroid/animation/AnimatorSet;->start()V

    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewCurrentPosition:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewWidth:F

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewWidth:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewHeight:F

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewHeight:F

    return-void

    :cond_3
    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewWidth:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewWidth:F

    div-float v9, v14, v15

    move v7, v9

    goto/16 :goto_0

    :pswitch_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition1MarginTop:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mSingleCallInfoHeight:F

    add-float v11, v14, v15

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition1MarginTop:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mSingleCallInfoHeight:F

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewHeight:F

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextViewMarginTop:F

    add-float v5, v14, v15

    goto/16 :goto_1

    :pswitch_1
    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition2MarginTop:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mSingleCallInfoHeight:F

    add-float v11, v14, v15

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition2MarginTop:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mSingleCallInfoHeight:F

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewHeight:F

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextViewMarginTop:F

    add-float v5, v14, v15

    goto/16 :goto_1

    :pswitch_2
    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition3MarginTop:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mSingleCallInfoHeight:F

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewHeight:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewHeight:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    sub-float v11, v14, v15

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition3MarginTop:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mSingleCallInfoHeight:F

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewHeight:F

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextViewMarginTop:F

    add-float v5, v14, v15

    goto/16 :goto_1

    :pswitch_3
    const/4 v11, 0x0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setVideoCrbtTitle(Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;

    invoke-direct {v0, p0, p1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;-><init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Ljava/lang/String;)V

    const-string/jumbo v1, "task_get_video_crbt_operator"

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v0

    const-wide/16 v2, 0x5dc

    const-string/jumbo v1, ""

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/android/incallui/util/SimpleTask;->run(JZLjava/lang/Object;)V

    return-void
.end method

.method private updateBottomExtraHeight(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getVideoDialpadHeight()F

    move-result v0

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonHeight:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonPaddingBottom:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonMarginTop:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mBottomExtraHeight:F

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getVideoBottomPanelHeight()F

    move-result v0

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getVideoTopPanelHeight()F

    move-result v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonHeight:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonPaddingBottom:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonMarginTop:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mBottomExtraHeight:F

    goto :goto_0
.end method

.method private updateNewWidthHeight()V
    .locals 4

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mScreenHeight:F

    iget v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mSingleCallInfoHeight:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mBottomExtraHeight:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewStandardMargin:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    sub-float v0, v1, v2

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewMaxHeight:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewHeight:F

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewHeight:F

    const v2, 0x3faa9fbe    # 1.333f

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mNewTextureViewWidth:F

    return-void
.end method

.method private updateSingleCallInfoHeight()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getVideoSingleCallInfoHeight()F

    move-result v0

    iput v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mSingleCallInfoHeight:F

    return-void
.end method


# virtual methods
.method public init()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mScreenWidth:F

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mScreenHeight:F

    const v3, 0x7f090026

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition1MarginTop:F

    const v3, 0x7f090027

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition2MarginTop:F

    const v3, 0x7f090028

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition3MarginTop:F

    const v3, 0x7f090029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextViewMarginTop:F

    const v3, 0x7f0900a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonHeight:F

    const v3, 0x7f0900a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonPaddingBottom:F

    const v3, 0x7f09002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mCallButtonMarginTop:F

    iget v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewPosition1MarginTop:F

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewStandardMargin:F

    iget v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mScreenWidth:F

    iget v4, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewStandardMargin:F

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewMaxWidth:F

    iget v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewMaxWidth:F

    const v4, 0x3faa9fbe    # 1.333f

    div-float/2addr v3, v4

    iput v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureViewMaxHeight:F

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/InCallPresenter;->getInCallActivity()Lcom/android/incallui/InCallActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->isDialpadVisible()Z

    move-result v3

    :goto_0
    iput-boolean v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mIsDialpad:Z

    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public initIncomingPosition(ZLjava/lang/String;)V
    .locals 4

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string/jumbo v1, "VoLTEVideoInComingLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "initIncomingPosition isVideoCrbt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_1

    new-instance v1, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;

    invoke-direct {v1, p0, p2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;-><init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Ljava/lang/String;)V

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->setClipToOutline(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mScreenWidth:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mScreenHeight:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    new-instance v2, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$3;

    invoke-direct {v2, p0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$3;-><init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0a00c5

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextureView:Landroid/view/TextureView;

    const v0, 0x7f0a00c6

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mTextView:Landroid/widget/TextView;

    return-void
.end method

.method public setDialPadPreviewPosition(Z)V
    .locals 4

    const-string/jumbo v0, "VoLTEVideoInComingLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDialPadPreviewPosition miscrbt = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mIsCrbt:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mIsCrbt:Z

    if-nez v0, :cond_0

    iput-boolean p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->mIsDialpad:Z

    return-void

    :cond_0
    new-instance v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;

    invoke-direct {v0, p0, p1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;-><init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)V

    const-wide/16 v2, 0x32

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
