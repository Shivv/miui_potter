.class public final Lcom/android/incallui/view/LabelAndNumberView;
.super Lcom/android/incallui/view/IndexSortedView;
.source "LabelAndNumberView.java"


# instance fields
.field private mCallCardTelocation:Landroid/widget/TextView;

.field private mMarkCount:Landroid/widget/TextView;

.field private mMarkProvider:Landroid/widget/ImageView;

.field private mMarkTitle:Landroid/widget/TextView;

.field private mPhoneNumber:Landroid/widget/TextView;

.field private mSimCardInfo:Landroid/widget/ImageView;

.field private mSpeechHD:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/LabelAndNumberView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/view/LabelAndNumberView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/incallui/view/IndexSortedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x4

    const/4 v1, 0x6

    filled-new-array {v2, v3, v0, v1}, [I

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/LabelAndNumberView;->configIndexFrontNoDivider([I)V

    new-array v0, v3, [I

    aput v2, v0, v2

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/LabelAndNumberView;->configIndexBackNoDivider([I)V

    return-void
.end method

.method private createViewByIndex(I)Landroid/view/View;
    .locals 12

    const v11, 0x7f060002

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v7, -0x2

    const/16 v8, 0x10

    const/4 v6, 0x0

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    packed-switch p1, :pswitch_data_0

    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v8, "error index"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :pswitch_0
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/view/LabelAndNumberView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v4, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput v8, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    const v7, 0x7f090045

    invoke-virtual {p0, v7}, Lcom/android/incallui/view/LabelAndNumberView;->getSize(I)I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v6, v4

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v6

    :pswitch_1
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/incallui/view/LabelAndNumberView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v1, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput v8, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setMaxLines(I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v11}, Lcom/android/incallui/view/LabelAndNumberView;->getColor(I)I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextColor(I)V

    const v7, 0x7f09000b

    invoke-virtual {p0, v7}, Lcom/android/incallui/view/LabelAndNumberView;->getSize(I)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v1, v9, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    const/4 v7, 0x3

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextDirection(I)V

    const/16 v7, 0x9

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setMaxEms(I)V

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    move-object v6, v1

    goto :goto_0

    :pswitch_2
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/incallui/view/LabelAndNumberView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput v8, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setMaxLines(I)V

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v11}, Lcom/android/incallui/view/LabelAndNumberView;->getColor(I)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    const v7, 0x7f09000c

    invoke-virtual {p0, v7}, Lcom/android/incallui/view/LabelAndNumberView;->getSize(I)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v5, v9, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    const/4 v7, 0x2

    if-ne p1, v7, :cond_0

    sget-object v7, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    const/4 v7, 0x6

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setMaxEms(I)V

    :cond_0
    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    move-object v6, v5

    goto :goto_0

    :pswitch_3
    new-instance v3, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/view/LabelAndNumberView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput v8, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {p0}, Lcom/android/incallui/view/LabelAndNumberView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->density:F

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v7, v8

    float-to-int v7, v7

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v6, v3

    goto/16 :goto_0

    :pswitch_4
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/view/LabelAndNumberView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput v8, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    const v7, 0x7f090046

    invoke-virtual {p0, v7}, Lcom/android/incallui/view/LabelAndNumberView;->getSize(I)I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v6, v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private getViewByIndex(I)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "error index"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v1, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSimCardInfo:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/LabelAndNumberView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSimCardInfo:Landroid/widget/ImageView;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/incallui/view/LabelAndNumberView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkTitle:Landroid/widget/TextView;

    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/android/incallui/view/LabelAndNumberView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkTitle:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkCount:Landroid/widget/TextView;

    if-nez v1, :cond_3

    invoke-direct {p0, p1}, Lcom/android/incallui/view/LabelAndNumberView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkCount:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkProvider:Landroid/widget/ImageView;

    if-nez v1, :cond_4

    invoke-direct {p0, p1}, Lcom/android/incallui/view/LabelAndNumberView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkProvider:Landroid/widget/ImageView;

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/android/incallui/view/LabelAndNumberView;->mCallCardTelocation:Landroid/widget/TextView;

    if-nez v1, :cond_5

    invoke-direct {p0, p1}, Lcom/android/incallui/view/LabelAndNumberView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mCallCardTelocation:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSpeechHD:Landroid/widget/ImageView;

    if-nez v1, :cond_6

    invoke-direct {p0, p1}, Lcom/android/incallui/view/LabelAndNumberView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSpeechHD:Landroid/widget/ImageView;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private showMarkCount(Ljava/lang/String;Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x3

    if-eqz p2, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkCount:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkCount:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkCount:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/LabelAndNumberView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkCount:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkCount:Landroid/widget/TextView;

    goto :goto_0
.end method

.method private showMarkProvider(Landroid/graphics/Bitmap;Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x4

    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkProvider:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkProvider:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkProvider:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/LabelAndNumberView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkProvider:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkProvider:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method private showMarkTitle(Ljava/lang/String;Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x2

    if-eqz p2, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkTitle:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/LabelAndNumberView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/LabelAndNumberView;->mMarkTitle:Landroid/widget/TextView;

    goto :goto_0
.end method


# virtual methods
.method protected getDividerView(I)Landroid/view/View;
    .locals 5

    const/4 v4, -0x2

    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/view/LabelAndNumberView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/android/incallui/view/LabelAndNumberView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    float-to-int v0, v3

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    const/16 v3, 0x10

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v3, 0x7f02008f

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object v1
.end method

.method public getPhoneNumber()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method protected getTotalViewCount()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public removeViewByIndex(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/incallui/view/IndexSortedView;->removeViewByIndex(I)V

    return-void
.end method

.method public setMultiSimIndicator(IZ)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSimCardInfo:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/LabelAndNumberView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSimCardInfo:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSimCardInfo:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method public showHDAudioIndicator(ZI)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x6

    if-eqz p1, :cond_1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSpeechHD:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSpeechHD:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSpeechHD:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/LabelAndNumberView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSpeechHD:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/LabelAndNumberView;->mSpeechHD:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method public showMarkInfo(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v0, v1, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/LabelAndNumberView;->showMarkTitle(Ljava/lang/String;Z)V

    invoke-direct {p0, p2, v0}, Lcom/android/incallui/view/LabelAndNumberView;->showMarkCount(Ljava/lang/String;Z)V

    invoke-direct {p0, p3, v0}, Lcom/android/incallui/view/LabelAndNumberView;->showMarkProvider(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public showPhoneNumber(Ljava/lang/CharSequence;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v2}, Lcom/android/incallui/view/LabelAndNumberView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextDirection(I)V

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v0}, Lcom/android/incallui/view/LabelAndNumberView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/LabelAndNumberView;->removeViewByIndex(I)V

    iput-object v1, p0, Lcom/android/incallui/view/LabelAndNumberView;->mPhoneNumber:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public showTelocation(Ljava/lang/CharSequence;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mCallCardTelocation:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mCallCardTelocation:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mCallCardTelocation:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/LabelAndNumberView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/LabelAndNumberView;->mCallCardTelocation:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/LabelAndNumberView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/LabelAndNumberView;->mCallCardTelocation:Landroid/widget/TextView;

    goto :goto_0
.end method
