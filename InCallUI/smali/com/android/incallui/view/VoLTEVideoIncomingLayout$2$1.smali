.class Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2$1;
.super Ljava/lang/Object;
.source "VoLTEVideoIncomingLayout.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2$1;->this$1:Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eq p6, p2, :cond_0

    invoke-static {}, Lcom/android/incallui/util/Utils;->isInternationalBuild()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2$1;->this$1:Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;

    iget-object v0, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0, v2, v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;II)V

    :goto_0
    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2$1;->this$1:Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;

    iget-object v0, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get6(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/view/TextureView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2$1;->this$1:Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;

    iget-object v0, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0, v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-set0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2$1;->this$1:Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;

    iget-object v0, v0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0, v1, v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;II)V

    goto :goto_0
.end method
