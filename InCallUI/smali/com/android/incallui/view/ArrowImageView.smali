.class public Lcom/android/incallui/view/ArrowImageView;
.super Landroid/widget/ImageView;
.source "ArrowImageView.java"


# instance fields
.field private moveUpAnim:Landroid/animation/AnimatorSet;

.field private translateFadeIn:Landroid/animation/AnimatorSet;

.field private translateFadeOut:Landroid/animation/AnimatorSet;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/ArrowImageView;)Landroid/animation/AnimatorSet;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/view/ArrowImageView;)Landroid/animation/AnimatorSet;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public cancelAllAnimator()V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/ArrowImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/ArrowImageView;->setTranslationY(F)V

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/ArrowImageView;->setScaleX(F)V

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/ArrowImageView;->setScaleY(F)V

    iput-object v1, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    iput-object v1, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    iput-object v1, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    return-void
.end method

.method public cancelArrowTranslateAnimator()V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/ArrowImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/ArrowImageView;->setTranslationY(F)V

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/ArrowImageView;->setScaleX(F)V

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/ArrowImageView;->setScaleY(F)V

    iput-object v1, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    iput-object v1, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    return-void
.end method

.method public playArrowMoveUpAnimator()V
    .locals 11

    const/high16 v10, 0x3f800000    # 1.0f

    const v9, 0x3f451eb8    # 0.77f

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/view/ArrowImageView;->cancelArrowTranslateAnimator()V

    iget-object v3, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    if-nez v3, :cond_0

    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v4, v8, [F

    const/4 v5, 0x0

    aput v5, v4, v6

    const/high16 v5, -0x3cbd0000    # -195.0f

    aput v5, v4, v7

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    sget-object v3, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v4, v8, [F

    aput v10, v4, v6

    aput v9, v4, v7

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget-object v3, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v4, v8, [F

    aput v10, v4, v6

    aput v9, v4, v7

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v3, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    new-instance v4, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v4}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v3, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x15e

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v3, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v2, v4, v6

    aput-object v0, v4, v7

    aput-object v1, v4, v8

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v3, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    new-instance v4, Lcom/android/incallui/view/ArrowImageView$3;

    invoke-direct {v4, p0}, Lcom/android/incallui/view/ArrowImageView$3;-><init>(Lcom/android/incallui/view/ArrowImageView;)V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    iget-object v3, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/view/ArrowImageView;->moveUpAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    :cond_1
    return-void
.end method

.method public playArrowRepeatTranslateAnimator(Z)V
    .locals 13

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0, v12}, Lcom/android/incallui/view/ArrowImageView;->setScaleX(F)V

    invoke-virtual {p0, v12}, Lcom/android/incallui/view/ArrowImageView;->setScaleY(F)V

    invoke-virtual {p0, v11}, Lcom/android/incallui/view/ArrowImageView;->setTranslationY(F)V

    const/4 v4, 0x0

    :goto_0
    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    if-nez v5, :cond_0

    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v10, [F

    sub-float v7, v11, v4

    aput v7, v6, v8

    const/high16 v7, -0x3df40000    # -35.0f

    sub-float/2addr v7, v4

    aput v7, v6, v9

    invoke-static {p0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v10, [F

    aput v12, v6, v8

    aput v11, v6, v9

    invoke-static {p0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x190

    invoke-virtual {v5, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    new-instance v6, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v6}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    new-array v6, v10, [Landroid/animation/Animator;

    aput-object v3, v6, v8

    aput-object v1, v6, v9

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v10, [F

    const/high16 v7, 0x420c0000    # 35.0f

    sub-float/2addr v7, v4

    aput v7, v6, v8

    sub-float v7, v11, v4

    aput v7, v6, v9

    invoke-static {p0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v10, [F

    aput v11, v6, v8

    aput v12, v6, v9

    invoke-static {p0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x190

    invoke-virtual {v5, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x258

    invoke-virtual {v5, v6, v7}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    new-instance v6, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v6}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    new-array v6, v10, [Landroid/animation/Animator;

    aput-object v2, v6, v8

    aput-object v0, v6, v9

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    new-instance v6, Lcom/android/incallui/view/ArrowImageView$1;

    invoke-direct {v6, p0}, Lcom/android/incallui/view/ArrowImageView$1;-><init>(Lcom/android/incallui/view/ArrowImageView;)V

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeOut:Landroid/animation/AnimatorSet;

    new-instance v6, Lcom/android/incallui/view/ArrowImageView$2;

    invoke-direct {v6, p0}, Lcom/android/incallui/view/ArrowImageView$2;-><init>(Lcom/android/incallui/view/ArrowImageView;)V

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/android/incallui/view/ArrowImageView;->translateFadeIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    :cond_1
    return-void

    :cond_2
    const v5, 0x3f451eb8    # 0.77f

    invoke-virtual {p0, v5}, Lcom/android/incallui/view/ArrowImageView;->setScaleX(F)V

    const v5, 0x3f451eb8    # 0.77f

    invoke-virtual {p0, v5}, Lcom/android/incallui/view/ArrowImageView;->setScaleY(F)V

    const/high16 v4, 0x43430000    # 195.0f

    neg-float v5, v4

    invoke-virtual {p0, v5}, Lcom/android/incallui/view/ArrowImageView;->setTranslationY(F)V

    goto/16 :goto_0
.end method
