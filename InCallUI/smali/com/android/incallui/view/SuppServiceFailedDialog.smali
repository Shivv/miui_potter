.class public Lcom/android/incallui/view/SuppServiceFailedDialog;
.super Lcom/android/incallui/view/InCallDialog;
.source "SuppServiceFailedDialog.java"


# static fields
.field private static final synthetic -com-android-incallui-util-Utils$SuppServiceSwitchesValues:[I


# instance fields
.field private mResult:Lcom/android/incallui/util/Utils$SuppService;


# direct methods
.method private static synthetic -getcom-android-incallui-util-Utils$SuppServiceSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/incallui/view/SuppServiceFailedDialog;->-com-android-incallui-util-Utils$SuppServiceSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/view/SuppServiceFailedDialog;->-com-android-incallui-util-Utils$SuppServiceSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/incallui/util/Utils$SuppService;->values()[Lcom/android/incallui/util/Utils$SuppService;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->CONFERENCE:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->HANGUP:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->HOLD:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->REJECT:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->RESUME:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->SEPARATE:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->SWITCH:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->TRANSFER:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->UNKNOWN:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v1}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    sput-object v0, Lcom/android/incallui/view/SuppServiceFailedDialog;->-com-android-incallui-util-Utils$SuppServiceSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1

    :catch_8
    move-exception v1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/InCallDialog;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p2}, Lcom/android/incallui/view/SuppServiceFailedDialog;->configureDialog(I)V

    return-void
.end method

.method private configureDialog(I)V
    .locals 3

    invoke-static {}, Lcom/android/incallui/util/Utils$SuppService;->values()[Lcom/android/incallui/util/Utils$SuppService;

    move-result-object v1

    aget-object v1, v1, p1

    iput-object v1, p0, Lcom/android/incallui/view/SuppServiceFailedDialog;->mResult:Lcom/android/incallui/util/Utils$SuppService;

    invoke-static {}, Lcom/android/incallui/view/SuppServiceFailedDialog;->-getcom-android-incallui-util-Utils$SuppServiceSwitchesValues()[I

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/SuppServiceFailedDialog;->mResult:Lcom/android/incallui/util/Utils$SuppService;

    invoke-virtual {v2}, Lcom/android/incallui/util/Utils$SuppService;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const v0, 0x7f0b001d

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/incallui/view/SuppServiceFailedDialog;->setMessage(I)V

    const v1, 0x7f0b0072

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/SuppServiceFailedDialog;->setPositiveButton(I)V

    invoke-virtual {p0}, Lcom/android/incallui/view/SuppServiceFailedDialog;->setCancelListener()V

    invoke-virtual {p0}, Lcom/android/incallui/view/SuppServiceFailedDialog;->setDismissListener()V

    return-void

    :pswitch_0
    const v0, 0x7f0b001e

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b001f

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b0020

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0b0021

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0b0022

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0b0023

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0b0024

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private updateUiState()V
    .locals 6

    iget-object v4, p0, Lcom/android/incallui/view/SuppServiceFailedDialog;->mResult:Lcom/android/incallui/util/Utils$SuppService;

    sget-object v5, Lcom/android/incallui/util/Utils$SuppService;->SWITCH:Lcom/android/incallui/util/Utils$SuppService;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/android/incallui/view/SuppServiceFailedDialog;->mResult:Lcom/android/incallui/util/Utils$SuppService;

    sget-object v5, Lcom/android/incallui/util/Utils$SuppService;->RESUME:Lcom/android/incallui/util/Utils$SuppService;

    if-ne v4, v5, :cond_4

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/InCallPresenter;->getCallToolsFragment()Lcom/android/incallui/CallToolsFragment;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v4}, Lcom/android/incallui/CallToolsPresenter;->updateHold()V

    :cond_1
    iget-object v4, p0, Lcom/android/incallui/view/SuppServiceFailedDialog;->mResult:Lcom/android/incallui/util/Utils$SuppService;

    sget-object v5, Lcom/android/incallui/util/Utils$SuppService;->SWITCH:Lcom/android/incallui/util/Utils$SuppService;

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/android/incallui/view/SuppServiceFailedDialog;->mResult:Lcom/android/incallui/util/Utils$SuppService;

    sget-object v5, Lcom/android/incallui/util/Utils$SuppService;->HOLD:Lcom/android/incallui/util/Utils$SuppService;

    if-ne v4, v5, :cond_6

    :cond_2
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/InCallPresenter;->getCallList()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/InCallPresenter;->getInCallActivity()Lcom/android/incallui/InCallActivity;

    move-result-object v2

    if-eqz v0, :cond_3

    if-nez v2, :cond_5

    :cond_3
    return-void

    :cond_4
    iget-object v4, p0, Lcom/android/incallui/view/SuppServiceFailedDialog;->mResult:Lcom/android/incallui/util/Utils$SuppService;

    sget-object v5, Lcom/android/incallui/util/Utils$SuppService;->HOLD:Lcom/android/incallui/util/Utils$SuppService;

    if-ne v4, v5, :cond_1

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/android/incallui/CallList;->getIncomingCall()Lcom/android/incallui/Call;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/InCallPresenter;->resetAnswerUI()V

    :cond_6
    return-void
.end method


# virtual methods
.method protected cancelDialog()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    invoke-direct {p0}, Lcom/android/incallui/view/SuppServiceFailedDialog;->updateUiState()V

    return-void
.end method

.method protected dismissDialog()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    invoke-direct {p0}, Lcom/android/incallui/view/SuppServiceFailedDialog;->updateUiState()V

    return-void
.end method

.method protected onPositive()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    invoke-direct {p0}, Lcom/android/incallui/view/SuppServiceFailedDialog;->updateUiState()V

    return-void
.end method
