.class public Lcom/android/incallui/view/AnimationCircleLayout;
.super Landroid/widget/FrameLayout;
.source "AnimationCircleLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;,
        Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;
    }
.end annotation


# instance fields
.field private cdr:F

.field private cdx:F

.field private cdy:F

.field private mAnimationRunning:Z

.field private mAnimationSet:Landroid/animation/AnimatorSet;

.field private mDurationCircleShrink:J

.field private mIsNeedTranslateY:Z

.field private mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

.field private mOnAnimationListener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

.field private mOriginR:F

.field private mOriginX:F

.field private mOriginY:F

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private mShouldClipView:Z

.field private mTagForLog:Ljava/lang/String;

.field private mTargetR:F

.field private mTargetX:F

.field private mTargetY:F


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/AnimationCircleLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mIsNeedTranslateY:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/incallui/view/AnimationCircleLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginR:F

    return v0
.end method

.method static synthetic -get2(Lcom/android/incallui/view/AnimationCircleLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginX:F

    return v0
.end method

.method static synthetic -get3(Lcom/android/incallui/view/AnimationCircleLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginY:F

    return v0
.end method

.method static synthetic -get4(Lcom/android/incallui/view/AnimationCircleLayout;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTagForLog:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/incallui/view/AnimationCircleLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetR:F

    return v0
.end method

.method static synthetic -get6(Lcom/android/incallui/view/AnimationCircleLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetX:F

    return v0
.end method

.method static synthetic -get7(Lcom/android/incallui/view/AnimationCircleLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetY:F

    return v0
.end method

.method static synthetic -set0(Lcom/android/incallui/view/AnimationCircleLayout;F)F
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdr:F

    return p1
.end method

.method static synthetic -set1(Lcom/android/incallui/view/AnimationCircleLayout;F)F
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdx:F

    return p1
.end method

.method static synthetic -set2(Lcom/android/incallui/view/AnimationCircleLayout;F)F
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdy:F

    return p1
.end method

.method static synthetic -set3(Lcom/android/incallui/view/AnimationCircleLayout;J)J
    .locals 1

    iput-wide p1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mDurationCircleShrink:J

    return-wide p1
.end method

.method static synthetic -set4(Lcom/android/incallui/view/AnimationCircleLayout;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mIsNeedTranslateY:Z

    return p1
.end method

.method static synthetic -set5(Lcom/android/incallui/view/AnimationCircleLayout;Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;)Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOnAnimationListener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/view/AnimationCircleLayout;FFFFFF)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/incallui/view/AnimationCircleLayout;->setValues(FFFFFF)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/AnimationCircleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/view/AnimationCircleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/high16 v0, -0x40800000    # -1.0f

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetX:F

    iput v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetY:F

    iput v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetR:F

    iput v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginX:F

    iput v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginY:F

    iput v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginR:F

    iput v1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdr:F

    iput v1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdx:F

    iput v1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdy:F

    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mDurationCircleShrink:J

    iput-boolean v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationRunning:Z

    iput-boolean v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mShouldClipView:Z

    iput-object v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOnAnimationListener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mIsNeedTranslateY:Z

    iput-object v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    iput-object v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/AnimationCircleLayout;->setWillNotDraw(Z)V

    return-void
.end method

.method private clearAnimator()V
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v0, "AnimationCircleLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTagForLog:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " clear animator"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    iput-object v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    return-void
.end method

.method private getTranslateYAnimator()Landroid/animation/Animator;
    .locals 5

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v3, v2, v4

    iget v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetY:F

    neg-float v3, v3

    iget v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetR:F

    sub-float/2addr v3, v4

    const/4 v4, 0x1

    aput v3, v2, v4

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/incallui/view/AnimationCircleLayout$3;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/AnimationCircleLayout$3;-><init>(Lcom/android/incallui/view/AnimationCircleLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v1, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v1}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    return-object v0
.end method

.method private setValues(FFFFFF)V
    .locals 2

    iput p4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetX:F

    iput p5, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetY:F

    iput p6, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTargetR:F

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginX:F

    iput p2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginY:F

    iput p3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginR:F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPath:Landroid/graphics/Path;

    return-void
.end method

.method private startAnimation()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    new-array v2, v6, [F

    const/4 v3, 0x0

    aput v3, v2, v4

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v2, v5

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    new-instance v2, Lcom/android/incallui/view/AnimationCircleLayout$1;

    invoke-direct {v2, p0}, Lcom/android/incallui/view/AnimationCircleLayout$1;-><init>(Lcom/android/incallui/view/AnimationCircleLayout;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-wide v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mDurationCircleShrink:J

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v2, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v2}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v2, Lcom/android/incallui/view/AnimationCircleLayout$2;

    invoke-direct {v2, p0}, Lcom/android/incallui/view/AnimationCircleLayout$2;-><init>(Lcom/android/incallui/view/AnimationCircleLayout;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-boolean v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mIsNeedTranslateY:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/view/AnimationCircleLayout;->getTranslateYAnimator()Landroid/animation/Animator;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    new-array v3, v6, [Landroid/animation/Animator;

    aput-object v1, v3, v4

    aput-object v0, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    :goto_0
    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    const-string/jumbo v2, "AnimationCircleLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTagForLog:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " startCircleAnimation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0
.end method


# virtual methods
.method public directAnimationEnd()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    const-string/jumbo v2, "AnimationCircleLayout"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTagForLog:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " directAnimationEnd, isCallBack:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOnAnimationListener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOnAnimationListener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOnAnimationListener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    invoke-interface {v0}, Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;->onAnimationEnd()V

    iput-boolean v1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationRunning:Z

    iput-object v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOnAnimationListener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public getCircleAnimationBuilder()Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    invoke-direct {v0, p0}, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;-><init>(Lcom/android/incallui/view/AnimationCircleLayout;)V

    iput-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    return-object v0
.end method

.method public isAnimationRuning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationRunning:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/android/incallui/view/AnimationCircleLayout;->clearAnimator()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mShouldClipView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPath:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginX:F

    iget v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdx:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginY:F

    iget v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdy:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOriginR:F

    iget v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdr:F

    add-float/2addr v3, v4

    sget-object v4, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPath:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    :cond_0
    return-void
.end method

.method public resetLayout()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const-string/jumbo v0, "AnimationCircleLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTagForLog:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " reset Layout start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v5, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mShouldClipView:Z

    iput-boolean v5, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationRunning:Z

    iput-object v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mDurationCircleShrink:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mIsNeedTranslateY:Z

    iput v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdr:F

    iput v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdy:F

    iput v3, p0, Lcom/android/incallui/view/AnimationCircleLayout;->cdx:F

    iput-object v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPaint:Landroid/graphics/Paint;

    iput-object v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mPath:Landroid/graphics/Path;

    iput-object v4, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mOnAnimationListener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    invoke-direct {p0}, Lcom/android/incallui/view/AnimationCircleLayout;->clearAnimator()V

    invoke-virtual {p0}, Lcom/android/incallui/view/AnimationCircleLayout;->invalidate()V

    invoke-virtual {p0, v3}, Lcom/android/incallui/view/AnimationCircleLayout;->setTranslationY(F)V

    invoke-virtual {p0, v3}, Lcom/android/incallui/view/AnimationCircleLayout;->setTranslationX(F)V

    const-string/jumbo v0, "AnimationCircleLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTagForLog:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " reset Layout end"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setTagForLog(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mTagForLog:Ljava/lang/String;

    return-void
.end method

.method public startCircleAnimation()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mLayoutAnimCircleAnimationBuilder:Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "you should init LayoutBuilder first!!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean v1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mShouldClipView:Z

    iput-boolean v1, p0, Lcom/android/incallui/view/AnimationCircleLayout;->mAnimationRunning:Z

    invoke-direct {p0}, Lcom/android/incallui/view/AnimationCircleLayout;->startAnimation()V

    return-void
.end method
