.class public Lcom/android/incallui/view/VoLTEVideoPreview;
.super Landroid/view/TextureView;
.source "VoLTEVideoPreview.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private mCallButtonHeight:F

.field private mCallButtonPaddingBottom:F

.field private mCurrentPosition:I

.field private mHasDialpad:Z

.field private mIsFullScreen:Z

.field private mIsTouched:Z

.field private mPreviewAnimatorSet:Landroid/animation/AnimatorSet;

.field private mPreviewBottomExtraHeight:F

.field private mPreviewBottomMargin:F

.field private mPreviewHeight:F

.field private mPreviewLeftMargin:F

.field private mPreviewRightMargin:F

.field private mPreviewTopExtraHeight:F

.field private mPreviewTopMargin:F

.field private mPreviewWidth:F

.field private mScreenHeight:F

.field private mScreenWidth:F

.field private mXDelta:F

.field private mYDelta:F


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/VoLTEVideoPreview;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/incallui/view/VoLTEVideoPreview;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewHeight:F

    return v0
.end method

.method static synthetic -get2(Lcom/android/incallui/view/VoLTEVideoPreview;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopMargin:F

    return v0
.end method

.method static synthetic -get3(Lcom/android/incallui/view/VoLTEVideoPreview;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mScreenHeight:F

    return v0
.end method

.method static synthetic -set0(Lcom/android/incallui/view/VoLTEVideoPreview;F)F
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomMargin:F

    return p1
.end method

.method static synthetic -set1(Lcom/android/incallui/view/VoLTEVideoPreview;F)F
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewHeight:F

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/view/VoLTEVideoPreview;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/VoLTEVideoPreview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->init()V

    return-void
.end method

.method private movePreviewAnimator(II)V
    .locals 12

    const/4 v10, 0x0

    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewRightMargin:F

    iget v7, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopMargin:F

    packed-switch p1, :pswitch_data_0

    :goto_0
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getX()F

    move-result v1

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getY()F

    move-result v2

    cmpg-float v8, v1, v10

    if-gez v8, :cond_0

    const/4 v1, 0x0

    :cond_0
    cmpg-float v8, v2, v10

    if-gez v8, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getRotation()F

    move-result v8

    float-to-int v4, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "movePreviewAnimator viewRotation="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", position="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    rem-int/lit16 v8, v4, 0xb4

    if-eqz v8, :cond_3

    const/4 v8, 0x7

    if-eq p1, v8, :cond_5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "movePreviewAnimator getWidth="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getWidth()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", getHeight="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getHeight()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getWidth()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    div-int/lit8 v0, v8, 0x2

    int-to-float v8, v0

    sub-float/2addr v5, v8

    const/4 v8, 0x1

    if-eq p1, v8, :cond_2

    const/4 v8, 0x3

    if-ne p1, v8, :cond_4

    :cond_2
    int-to-float v8, v0

    add-float/2addr v7, v8

    :cond_3
    :goto_1
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v3, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v8, Landroid/view/View;->X:Landroid/util/Property;

    sget-object v9, Landroid/view/View;->Y:Landroid/util/Property;

    invoke-static {p0, v8, v9, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    iget-object v8, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v8, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v8, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewAnimatorSet:Landroid/animation/AnimatorSet;

    int-to-long v10, p2

    invoke-virtual {v8, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v8

    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->start()V

    iput p1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "movePreviewAnimator mCurrentPosition="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", currentX="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", currentY="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "movePreviewAnimator xEnd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", yEnd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :pswitch_0
    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewLeftMargin:F

    iget v8, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopMargin:F

    iget v9, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopExtraHeight:F

    add-float v7, v8, v9

    goto/16 :goto_0

    :pswitch_1
    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewLeftMargin:F

    iget v7, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomMargin:F

    goto/16 :goto_0

    :pswitch_2
    iget v8, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopMargin:F

    iget v9, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopExtraHeight:F

    add-float v7, v8, v9

    goto/16 :goto_0

    :pswitch_3
    iget v7, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomMargin:F

    goto/16 :goto_0

    :pswitch_4
    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewLeftMargin:F

    iget v8, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomMargin:F

    iget v9, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomExtraHeight:F

    sub-float v7, v8, v9

    goto/16 :goto_0

    :pswitch_5
    iget v8, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomMargin:F

    iget v9, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomExtraHeight:F

    sub-float v7, v8, v9

    goto/16 :goto_0

    :pswitch_6
    const/4 v5, 0x0

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_4
    int-to-float v8, v0

    sub-float/2addr v7, v8

    goto/16 :goto_1

    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private updatePreviewBottomExtraHeight()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mHasDialpad:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getVideoDialpadHeight()F

    move-result v0

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCallButtonHeight:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCallButtonPaddingBottom:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomExtraHeight:F

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getVideoBottomPanelHeight()F

    move-result v0

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getVideoTopPanelHeight()F

    move-result v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCallButtonHeight:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCallButtonPaddingBottom:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomExtraHeight:F

    goto :goto_0
.end method


# virtual methods
.method public changePreviewTopExtraHeight(Z)V
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsFullScreen:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    int-to-float v1, v1

    iput v1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopExtraHeight:F

    iget v1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    invoke-direct {p0, v1, v2}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    return-void

    :cond_0
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v3, 0x7f090048

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public init()V
    .locals 4

    new-instance v2, Lcom/android/incallui/view/VoLTEVideoPreview$1;

    invoke-direct {v2, p0}, Lcom/android/incallui/view/VoLTEVideoPreview$1;-><init>(Lcom/android/incallui/view/VoLTEVideoPreview;)V

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/VoLTEVideoPreview;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    iput v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mScreenWidth:F

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    iput v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mScreenHeight:F

    const v2, 0x7f0900a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCallButtonPaddingBottom:F

    const v2, 0x7f090014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewWidth:F

    const v2, 0x7f090015

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopMargin:F

    const v2, 0x7f090016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewLeftMargin:F

    iget v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mScreenWidth:F

    iget v3, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewLeftMargin:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewWidth:F

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewRightMargin:F

    const v2, 0x7f0900a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCallButtonHeight:F

    return-void
.end method

.method public initPreviewPosition()V
    .locals 2

    const/4 v1, 0x3

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v7, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    iget-boolean v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsFullScreen:Z

    if-nez v5, :cond_0

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/incallui/InCallPresenter;->resetVideoAutoFullScreen()V

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    and-int/lit16 v5, v5, 0xff

    packed-switch v5, :pswitch_data_0

    :cond_1
    :goto_0
    return v7

    :pswitch_0
    iput-boolean v7, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsTouched:Z

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v5

    sub-float v5, v1, v5

    iput v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mXDelta:F

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v5

    sub-float v5, v2, v5

    iput v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mYDelta:F

    iget-object v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_2
    invoke-direct {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->updatePreviewBottomExtraHeight()V

    goto :goto_0

    :pswitch_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsTouched:Z

    const/4 v0, 0x3

    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mScreenWidth:F

    div-float/2addr v5, v6

    cmpg-float v5, v1, v5

    if-gtz v5, :cond_5

    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mScreenHeight:F

    div-float/2addr v5, v6

    cmpg-float v5, v2, v5

    if-gtz v5, :cond_4

    const/4 v0, 0x1

    :cond_3
    :goto_1
    const/16 v5, 0x15e

    invoke-direct {p0, v0, v5}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x2

    iget-boolean v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsFullScreen:Z

    if-nez v5, :cond_3

    const/4 v0, 0x5

    goto :goto_1

    :cond_5
    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mScreenHeight:F

    div-float/2addr v5, v6

    cmpg-float v5, v2, v5

    if-gtz v5, :cond_6

    const/4 v0, 0x3

    goto :goto_1

    :cond_6
    const/4 v0, 0x4

    iget-boolean v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsFullScreen:Z

    if-nez v5, :cond_3

    const/4 v0, 0x6

    goto :goto_1

    :pswitch_2
    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mXDelta:F

    sub-float v3, v1, v5

    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mYDelta:F

    sub-float v4, v2, v5

    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewLeftMargin:F

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_7

    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewRightMargin:F

    cmpg-float v5, v3, v5

    if-gtz v5, :cond_7

    invoke-virtual {p1, v3}, Landroid/view/View;->setX(F)V

    :cond_7
    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopMargin:F

    iget v6, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewTopExtraHeight:F

    add-float/2addr v5, v6

    cmpl-float v5, v4, v5

    if-ltz v5, :cond_1

    iget-boolean v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsFullScreen:Z

    if-nez v5, :cond_8

    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomMargin:F

    iget v6, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomExtraHeight:F

    sub-float/2addr v5, v6

    cmpg-float v5, v4, v5

    if-gtz v5, :cond_8

    invoke-virtual {p1, v4}, Landroid/view/View;->setY(F)V

    goto/16 :goto_0

    :cond_8
    iget-boolean v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsFullScreen:Z

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mPreviewBottomMargin:F

    cmpg-float v5, v4, v5

    if-gtz v5, :cond_1

    invoke-virtual {p1, v4}, Landroid/view/View;->setY(F)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setDialPadPreviewPosition(Z)V
    .locals 4

    const/4 v3, 0x3

    const/4 v2, 0x1

    const/16 v1, 0x96

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iput-boolean p1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mHasDialpad:Z

    return-void

    :pswitch_1
    invoke-direct {p0, v2, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v3, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v2, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, v3, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setFullScreenMode(Z)V
    .locals 6

    const/4 v5, 0x6

    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/16 v1, 0x96

    iput-boolean p1, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsFullScreen:Z

    iget-boolean v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsTouched:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/view/VoLTEVideoPreview;->updatePreviewBottomExtraHeight()V

    iget-boolean v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mIsFullScreen:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    if-ne v0, v4, :cond_2

    invoke-direct {p0, v2, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    if-ne v0, v5, :cond_1

    invoke-direct {p0, v3, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    if-ne v0, v2, :cond_4

    invoke-direct {p0, v4, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    if-ne v0, v3, :cond_1

    invoke-direct {p0, v5, v1}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    goto :goto_0
.end method

.method public setFullScreenPreviewPosition()V
    .locals 2

    const/4 v1, 0x7

    iget v0, p0, Lcom/android/incallui/view/VoLTEVideoPreview;->mCurrentPosition:I

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/incallui/view/VoLTEVideoPreview;->movePreviewAnimator(II)V

    return-void
.end method

.method public setOnTouchListener(Z)V
    .locals 1

    if-eqz p1, :cond_0

    move-object v0, p0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/incallui/view/VoLTEVideoPreview;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
