.class Lcom/android/incallui/view/AnimationCircleLayout$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "AnimationCircleLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/AnimationCircleLayout;->startAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/AnimationCircleLayout;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/AnimationCircleLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$2;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3

    const-string/jumbo v0, "AnimationCircleLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$2;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-get4(Lcom/android/incallui/view/AnimationCircleLayout;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " circle anim cancel"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    const-string/jumbo v0, "AnimationCircleLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$2;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-get4(Lcom/android/incallui/view/AnimationCircleLayout;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " circle anim end, mIsNeedTranslateY:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$2;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-get0(Lcom/android/incallui/view/AnimationCircleLayout;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout$2;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v0}, Lcom/android/incallui/view/AnimationCircleLayout;->-get0(Lcom/android/incallui/view/AnimationCircleLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout$2;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/AnimationCircleLayout;->directAnimationEnd()V

    :cond_0
    return-void
.end method
