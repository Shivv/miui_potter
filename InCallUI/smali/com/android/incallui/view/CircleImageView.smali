.class public Lcom/android/incallui/view/CircleImageView;
.super Landroid/widget/ImageView;
.source "CircleImageView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private animationProgress:F

.field private centerX:I

.field private centerY:I

.field private mBounceAniSet:Landroid/animation/AnimatorSet;

.field private mCircleAnimator:Landroid/animation/ObjectAnimator;

.field private mImageAnimatorHeight:I

.field private mIsPlayArrow:Z

.field private mIsPressed:Z

.field private mNormalPaint:Landroid/graphics/Paint;

.field private mPressPaint:Landroid/graphics/Paint;

.field private mTranslateDistanceBase:I

.field private outerRadius:I


# direct methods
.method static synthetic -set0(Lcom/android/incallui/view/CircleImageView;F)F
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/CircleImageView;->animationProgress:F

    return p1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/android/incallui/view/CircleImageView;->mIsPlayArrow:Z

    iput-boolean v0, p0, Lcom/android/incallui/view/CircleImageView;->mIsPressed:Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/incallui/view/CircleImageView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/android/incallui/view/CircleImageView;->mIsPlayArrow:Z

    iput-boolean v0, p0, Lcom/android/incallui/view/CircleImageView;->mIsPressed:Z

    invoke-virtual {p0, p1, p2}, Lcom/android/incallui/view/CircleImageView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v0, p0, Lcom/android/incallui/view/CircleImageView;->mIsPlayArrow:Z

    iput-boolean v0, p0, Lcom/android/incallui/view/CircleImageView;->mIsPressed:Z

    invoke-virtual {p0, p1, p2}, Lcom/android/incallui/view/CircleImageView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private getWaveRingRadius()F
    .locals 2

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/util/SmartCoverUtil;->isSmartCoverClosed(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    int-to-float v0, v0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->showTouchCircle()V

    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/AnimatorObservable;->onAnimatorPause()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->hideTouchCircle()V

    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/AnimatorObservable;->onAnimatorResume()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getAnimatorImageHeight()I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/CircleImageView;->mImageAnimatorHeight:I

    return v0
.end method

.method public getBounceAnimatorSet(I)Landroid/animation/AnimatorSet;
    .locals 14

    iget-object v11, p0, Lcom/android/incallui/view/CircleImageView;->mBounceAniSet:Landroid/animation/AnimatorSet;

    if-nez v11, :cond_1

    new-instance v11, Landroid/animation/AnimatorSet;

    invoke-direct {v11}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v11, p0, Lcom/android/incallui/view/CircleImageView;->mBounceAniSet:Landroid/animation/AnimatorSet;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v11, v12}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v0

    iget v11, p0, Lcom/android/incallui/view/CircleImageView;->mImageAnimatorHeight:I

    neg-int v11, v11

    int-to-float v11, v11

    const v12, 0x3e19999a    # 0.15f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v1

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_2

    const/4 v11, 0x6

    :goto_0
    int-to-float v11, v11

    const v12, 0x3ecccccd    # 0.4f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v2

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_3

    const/4 v11, -0x4

    :goto_1
    int-to-float v11, v11

    const/high16 v12, 0x3f000000    # 0.5f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_4

    const/4 v11, 0x4

    :goto_2
    int-to-float v11, v11

    const v12, 0x3f19999a    # 0.6f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_5

    const/4 v11, -0x3

    :goto_3
    int-to-float v11, v11

    const v12, 0x3f333333    # 0.7f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_6

    const/4 v11, 0x1

    :goto_4
    int-to-float v11, v11

    const/high16 v12, 0x3f400000    # 0.75f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_0

    :cond_0
    const/4 v11, 0x0

    int-to-float v11, v11

    const v12, 0x3f4ccccd    # 0.8f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static {v11, v12}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    sget-object v11, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v12, 0x9

    new-array v12, v12, [Landroid/animation/Keyframe;

    const/4 v13, 0x0

    aput-object v0, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    const/4 v13, 0x2

    aput-object v2, v12, v13

    const/4 v13, 0x3

    aput-object v3, v12, v13

    const/4 v13, 0x4

    aput-object v4, v12, v13

    const/4 v13, 0x5

    aput-object v5, v12, v13

    const/4 v13, 0x6

    aput-object v6, v12, v13

    const/4 v13, 0x7

    aput-object v7, v12, v13

    const/16 v13, 0x8

    aput-object v8, v12, v13

    invoke-static {v11, v12}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v9

    const/4 v11, 0x1

    new-array v11, v11, [Landroid/animation/PropertyValuesHolder;

    const/4 v12, 0x0

    aput-object v9, v11, v12

    invoke-static {p0, v11}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v10

    const-wide/16 v12, 0x5dc

    invoke-virtual {v10, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const/4 v11, -0x1

    invoke-virtual {v10, v11}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    mul-int/lit16 v11, p1, 0x87

    int-to-long v12, v11

    invoke-virtual {v10, v12, v13}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    iget-object v11, p0, Lcom/android/incallui/view/CircleImageView;->mBounceAniSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v11, v10}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_1
    iget-object v11, p0, Lcom/android/incallui/view/CircleImageView;->mBounceAniSet:Landroid/animation/AnimatorSet;

    return-object v11

    :cond_2
    const/4 v11, 0x2

    goto/16 :goto_0

    :cond_3
    const/4 v11, -0x2

    goto/16 :goto_1

    :cond_4
    const/4 v11, 0x1

    goto/16 :goto_2

    :cond_5
    const/4 v11, -0x1

    goto :goto_3

    :cond_6
    const/4 v11, 0x0

    goto :goto_4
.end method

.method public getTranslateInAnimatorSet(I)Landroid/animation/ObjectAnimator;
    .locals 14

    iget v11, p0, Lcom/android/incallui/view/CircleImageView;->mTranslateDistanceBase:I

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v0

    iget v11, p0, Lcom/android/incallui/view/CircleImageView;->mImageAnimatorHeight:I

    neg-int v11, v11

    int-to-float v11, v11

    const v12, 0x3e99999a    # 0.3f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v1

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_2

    const/4 v11, 0x5

    :goto_0
    int-to-float v11, v11

    const/high16 v12, 0x3f000000    # 0.5f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v2

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_3

    const/4 v11, -0x3

    :goto_1
    int-to-float v11, v11

    const v12, 0x3f19999a    # 0.6f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_4

    const/4 v11, 0x2

    :goto_2
    int-to-float v11, v11

    const v12, 0x3f333333    # 0.7f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_5

    const/high16 v11, -0x40800000    # -1.0f

    :goto_3
    const v12, 0x3f4ccccd    # 0.8f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    rem-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_0

    :cond_0
    const/4 v11, 0x0

    int-to-float v11, v11

    const v12, 0x3f666666    # 0.9f

    invoke-static {v12, v11}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static {v11, v12}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    sget-object v11, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v12, 0x8

    new-array v12, v12, [Landroid/animation/Keyframe;

    const/4 v13, 0x0

    aput-object v0, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    const/4 v13, 0x2

    aput-object v2, v12, v13

    const/4 v13, 0x3

    aput-object v3, v12, v13

    const/4 v13, 0x4

    aput-object v4, v12, v13

    const/4 v13, 0x5

    aput-object v5, v12, v13

    const/4 v13, 0x6

    aput-object v6, v12, v13

    const/4 v13, 0x7

    aput-object v7, v12, v13

    invoke-static {v11, v12}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v8

    const/4 v11, 0x1

    new-array v11, v11, [Landroid/animation/PropertyValuesHolder;

    const/4 v12, 0x0

    aput-object v8, v11, v12

    invoke-static {p0, v11}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v9

    if-nez p1, :cond_6

    const/16 v11, 0x640

    :goto_4
    int-to-long v12, v11

    invoke-virtual {v9, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-object v10, p0

    new-instance v11, Lcom/android/incallui/view/CircleImageView$2;

    invoke-direct {v11, p0, p0}, Lcom/android/incallui/view/CircleImageView$2;-><init>(Lcom/android/incallui/view/CircleImageView;Landroid/view/View;)V

    invoke-virtual {v9, v11}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    if-nez p1, :cond_7

    const-wide/16 v12, 0x0

    invoke-virtual {v9, v12, v13}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    :cond_1
    :goto_5
    return-object v9

    :cond_2
    const/4 v11, 0x3

    goto/16 :goto_0

    :cond_3
    const/4 v11, -0x2

    goto :goto_1

    :cond_4
    const/4 v11, 0x1

    goto :goto_2

    :cond_5
    const/high16 v11, -0x41000000    # -0.5f

    goto :goto_3

    :cond_6
    const/16 v11, 0x4b0

    goto :goto_4

    :cond_7
    const/4 v11, 0x1

    if-ne p1, v11, :cond_8

    const-wide/16 v12, 0xfa

    invoke-virtual {v9, v12, v13}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    goto :goto_5

    :cond_8
    const/4 v11, 0x2

    if-ne p1, v11, :cond_1

    const-wide/16 v12, 0x15e

    invoke-virtual {v9, v12, v13}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    goto :goto_5
.end method

.method public hideTouchCircle()V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mCircleAnimator:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    iget v2, p0, Lcom/android/incallui/view/CircleImageView;->animationProgress:F

    const/4 v3, 0x0

    aput v2, v1, v3

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mCircleAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/high16 v9, 0x40400000    # 3.0f

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/android/incallui/view/CircleImageView;->setFocusable(Z)V

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v5}, Lcom/android/incallui/view/CircleImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p0, v8}, Lcom/android/incallui/view/CircleImageView;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090056

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090058

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, p0, Lcom/android/incallui/view/CircleImageView;->mTranslateDistanceBase:I

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mNormalPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mNormalPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mPressPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mPressPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v1, -0x1000000

    const/high16 v3, -0x1000000

    if-eqz p2, :cond_0

    sget-object v5, Lcom/android/incallui/R$styleable;->CircleImageView:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v10, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v8, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09005e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    invoke-virtual {v0, v11, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/android/incallui/view/CircleImageView;->mImageAnimatorHeight:I

    const/4 v5, 0x3

    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    if-nez v2, :cond_1

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    :goto_0
    iget-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mNormalPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mPressPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    invoke-virtual {p0, v1, v3}, Lcom/android/incallui/view/CircleImageView;->setColor(II)V

    const-string/jumbo v5, "animationProgress"

    new-array v6, v11, [F

    fill-array-data v6, :array_0

    invoke-static {p0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    iput-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mCircleAnimator:Landroid/animation/ObjectAnimator;

    iget-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mCircleAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v6, 0x12c

    invoke-virtual {v5, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v5, p0, Lcom/android/incallui/view/CircleImageView;->mCircleAnimator:Landroid/animation/ObjectAnimator;

    new-instance v6, Lcom/android/incallui/view/CircleImageView$1;

    invoke-direct {v6, p0}, Lcom/android/incallui/view/CircleImageView$1;-><init>(Lcom/android/incallui/view/CircleImageView;)V

    invoke-virtual {v5, v6}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    return-void

    :cond_1
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    iget v0, p0, Lcom/android/incallui/view/CircleImageView;->centerX:I

    int-to-float v1, v0

    iget v0, p0, Lcom/android/incallui/view/CircleImageView;->centerY:I

    int-to-float v2, v0

    iget v0, p0, Lcom/android/incallui/view/CircleImageView;->outerRadius:I

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/android/incallui/view/CircleImageView;->getWaveRingRadius()F

    move-result v3

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/android/incallui/view/CircleImageView;->animationProgress:F

    add-float/2addr v0, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v3, v0, v3

    iget-boolean v0, p0, Lcom/android/incallui/view/CircleImageView;->mIsPressed:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/incallui/view/CircleImageView;->animationProgress:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mPressPaint:Landroid/graphics/Paint;

    :goto_0
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mNormalPaint:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    div-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/android/incallui/view/CircleImageView;->centerX:I

    div-int/lit8 v0, p2, 0x2

    iput v0, p0, Lcom/android/incallui/view/CircleImageView;->centerY:I

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/incallui/view/CircleImageView;->outerRadius:I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onTouchEvent ACTION_DOWN: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/android/incallui/view/CircleImageView;->mIsPressed:Z

    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->getTranslationY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x42700000    # 60.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iput-boolean v3, p0, Lcom/android/incallui/view/CircleImageView;->mIsPlayArrow:Z

    :cond_0
    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/incallui/view/CircleImageView;->mIsPlayArrow:Z

    invoke-virtual {v1, p0, v2}, Lcom/android/incallui/AnimatorObservable;->onViewTouchDown(Landroid/view/View;Z)V

    goto :goto_0

    :pswitch_1
    iput-boolean v2, p0, Lcom/android/incallui/view/CircleImageView;->mIsPressed:Z

    iput-boolean v2, p0, Lcom/android/incallui/view/CircleImageView;->mIsPlayArrow:Z

    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/incallui/AnimatorObservable;->onViewTouchUp(Landroid/view/View;)V

    goto :goto_0

    :pswitch_2
    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/incallui/AnimatorObservable;->onViewMove(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public releaseBounceAnimatorSet()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mBounceAniSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mBounceAniSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    :cond_0
    iput-object v1, p0, Lcom/android/incallui/view/CircleImageView;->mBounceAniSet:Landroid/animation/AnimatorSet;

    return-void
.end method

.method public setColor(II)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mNormalPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mPressPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/android/incallui/view/CircleImageView;->invalidate()V

    return-void
.end method

.method public showTouchCircle()V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mCircleAnimator:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    iget v2, p0, Lcom/android/incallui/view/CircleImageView;->animationProgress:F

    const/4 v3, 0x0

    aput v2, v1, v3

    invoke-direct {p0}, Lcom/android/incallui/view/CircleImageView;->getWaveRingRadius()F

    move-result v2

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/android/incallui/view/CircleImageView;->mCircleAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method
