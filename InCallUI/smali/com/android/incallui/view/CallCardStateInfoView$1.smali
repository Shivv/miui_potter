.class Lcom/android/incallui/view/CallCardStateInfoView$1;
.super Landroid/os/Handler;
.source "CallCardStateInfoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/view/CallCardStateInfoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/CallCardStateInfoView;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/CallCardStateInfoView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    const-string/jumbo v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->setCallElapsedTime(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-static {v0}, Lcom/android/incallui/view/CallCardStateInfoView;->-get0(Lcom/android/incallui/view/CallCardStateInfoView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-static {v0}, Lcom/android/incallui/view/CallCardStateInfoView;->-get1(Lcom/android/incallui/view/CallCardStateInfoView;)Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-static {v0}, Lcom/android/incallui/view/CallCardStateInfoView;->-get1(Lcom/android/incallui/view/CallCardStateInfoView;)Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;->onDisconnectingWeakSignal()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-static {v0}, Lcom/android/incallui/view/CallCardStateInfoView;->-get0(Lcom/android/incallui/view/CallCardStateInfoView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-virtual {v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-static {v0}, Lcom/android/incallui/view/CallCardStateInfoView;->-get1(Lcom/android/incallui/view/CallCardStateInfoView;)Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView$1;->this$0:Lcom/android/incallui/view/CallCardStateInfoView;

    invoke-static {v0}, Lcom/android/incallui/view/CallCardStateInfoView;->-get1(Lcom/android/incallui/view/CallCardStateInfoView;)Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;->onDisconnectingRestart()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
