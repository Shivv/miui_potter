.class Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;
.super Lcom/android/incallui/view/ViewDragHelper$Callback;
.source "SlidingUpPanelLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/view/SlidingUpPanelLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DragHelperCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/SlidingUpPanelLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-direct {p0}, Lcom/android/incallui/view/ViewDragHelper$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public clampViewPositionHorizontal(Landroid/view/View;II)I
    .locals 1

    sub-int v0, p2, p3

    return v0
.end method

.method public clampViewPositionVertical(Landroid/view/View;II)I
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get2(Lcom/android/incallui/view/SlidingUpPanelLayout;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-wrap0(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get6(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v2

    add-int v0, v1, v2

    :goto_0
    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    return v2

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v0

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get6(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v2

    sub-int v1, v0, v2

    goto :goto_0
.end method

.method public getViewVerticalDragRange(Landroid/view/View;)I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get6(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v0

    return v0
.end method

.method public onViewCaptured(Landroid/view/View;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setAllChildrenVisible()V

    return-void
.end method

.method public onViewDragStateChanged(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onViewDragStateChanged: mViewDragHelper.getViewDragState() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get8(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/ViewDragHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/view/ViewDragHelper;->getViewDragState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , mSlideState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get7(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , mSlideOffset = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-wrap1(Lcom/android/incallui/view/SlidingUpPanelLayout;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get8(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/ViewDragHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/view/ViewDragHelper;->getViewDragState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get7(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    move-result-object v0

    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->EXPANDED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->updateObscuredViewVisibility()V

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->EXPANDED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    invoke-static {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-set0(Lcom/android/incallui/view/SlidingUpPanelLayout;Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;)Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get0(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->dispatchOnPanelExpanded(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get7(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    move-result-object v0

    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->COLLAPSED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->COLLAPSED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    invoke-static {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-set0(Lcom/android/incallui/view/SlidingUpPanelLayout;Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;)Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get0(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->dispatchOnPanelCollapsed(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get7(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    move-result-object v0

    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->COLLAPSED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get0(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->dispatchPanelCollapsedReleased(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get0(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->dispatchOnPanelCollapsed(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v0, p3}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-wrap2(Lcom/android/incallui/view/SlidingUpPanelLayout;I)V

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->invalidate()V

    return-void
.end method

.method public onViewReleased(Landroid/view/View;FF)V
    .locals 6

    const/4 v5, 0x0

    const v4, 0x3f19999a    # 0.6f

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-wrap0(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v0

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onViewReleased: mDragView:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v3}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get1(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " xVelocity = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " yVelocity = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " mSlideRange = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v3}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get6(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " mMinFlingVelocity = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v3}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get4(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-wrap1(Lcom/android/incallui/view/SlidingUpPanelLayout;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v1

    const v2, 0x3f666666    # 0.9f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    cmpg-float v1, p3, v5

    if-gez v1, :cond_0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v3}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v3}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get4(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get0(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->dispatchOnPanelViewExpandReleased(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get8(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/ViewDragHelper;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3, v0}, Lcom/android/incallui/view/ViewDragHelper;->flingCapturedView(IIII)V

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->invalidate()V

    return-void

    :cond_0
    cmpl-float v1, p3, v5

    if-gtz v1, :cond_1

    cmpl-float v1, p3, v5

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get6(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    const-string/jumbo v2, "onViewReleased: error happened, mSlideOffset > DEFAULT_SLIDE_OFFSET_THRESHOLD & top = getPaddingTop()"

    invoke-static {v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-wrap1(Lcom/android/incallui/view/SlidingUpPanelLayout;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get6(Lcom/android/incallui/view/SlidingUpPanelLayout;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v1

    cmpg-float v1, v1, v4

    if-gez v1, :cond_4

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v1

    if-le v0, v1, :cond_4

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    const-string/jumbo v2, "onViewReleased: error happened, mSlideOffset < DEFAULT_SLIDE_OFFSET_THRESHOLD & top > getPaddingTop()"

    invoke-static {v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-wrap1(Lcom/android/incallui/view/SlidingUpPanelLayout;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_4
    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onViewReleased: slideOffSet = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v3}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " top = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-wrap1(Lcom/android/incallui/view/SlidingUpPanelLayout;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v1

    if-ne v0, v1, :cond_6

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get0(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->dispatchOnPanelViewExpandReleased(Landroid/view/View;)V

    :cond_5
    :goto_0
    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get8(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/ViewDragHelper;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/android/incallui/view/ViewDragHelper;->settleCapturedViewAt(II)Z

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->invalidate()V

    return-void

    :cond_6
    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v1

    if-le v0, v1, :cond_5

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get0(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->dispatchPanelCollapsedReleased(Landroid/view/View;)V

    goto :goto_0
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;->this$0:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-static {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-get3(Lcom/android/incallui/view/SlidingUpPanelLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->slideable:Z

    return v0
.end method
