.class public abstract Lcom/android/incallui/view/OnEffectiveClickListener;
.super Ljava/lang/Object;
.source "OnEffectiveClickListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private lastClickTime:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/incallui/view/OnEffectiveClickListener;->lastClickTime:J

    return-void
.end method


# virtual methods
.method public isEffectiveClick()Z
    .locals 6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/incallui/view/OnEffectiveClickListener;->lastClickTime:J

    sub-long v2, v0, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x15e

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iput-wide v0, p0, Lcom/android/incallui/view/OnEffectiveClickListener;->lastClickTime:J

    const/4 v2, 0x1

    return v2

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/view/OnEffectiveClickListener;->isEffectiveClick()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/incallui/view/OnEffectiveClickListener;->onEffectiveClick(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Landroid/widget/CompoundButton;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->toggle()V

    goto :goto_0
.end method

.method public abstract onEffectiveClick(Landroid/view/View;)V
.end method
