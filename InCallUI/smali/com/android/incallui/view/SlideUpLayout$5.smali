.class Lcom/android/incallui/view/SlideUpLayout$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SlideUpLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/SlideUpLayout;->playCircleTranslateInAnimator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/SlideUpLayout;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/SlideUpLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6

    const/16 v3, 0x64

    iget-object v2, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    iget-object v2, v2, Lcom/android/incallui/view/SlideUpLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    iget-object v2, v2, Lcom/android/incallui/view/SlideUpLayout;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v2, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlideUpLayout;->-get0(Lcom/android/incallui/view/SlideUpLayout;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlideUpLayout;->-get2(Lcom/android/incallui/view/SlideUpLayout;)Lcom/android/incallui/view/CircleImageView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/view/CircleImageView;->isFocused()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlideUpLayout;->-get3(Lcom/android/incallui/view/SlideUpLayout;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CircleImageView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/CircleImageView;->setFocusableInTouchMode(Z)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    invoke-static {v2}, Lcom/android/incallui/view/SlideUpLayout;->-get2(Lcom/android/incallui/view/SlideUpLayout;)Lcom/android/incallui/view/CircleImageView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/view/CircleImageView;->requestFocus()Z

    :cond_1
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    invoke-static {v0}, Lcom/android/incallui/view/SlideUpLayout;->-get1(Lcom/android/incallui/view/SlideUpLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlideUpLayout$5;->this$0:Lcom/android/incallui/view/SlideUpLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/incallui/view/SlideUpLayout;->-wrap1(Lcom/android/incallui/view/SlideUpLayout;Z)V

    :cond_0
    return-void
.end method
