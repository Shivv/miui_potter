.class public final Lcom/android/incallui/view/CallCardOptionalInfoView;
.super Lcom/android/incallui/view/IndexSortedView;
.source "CallCardOptionalInfoView.java"


# instance fields
.field private mCallCardExtraTitle:Landroid/widget/TextView;

.field private mCallProviderInfo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/CallCardOptionalInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/view/CallCardOptionalInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/incallui/view/IndexSortedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/CallCardOptionalInfoView;->configIndexFrontNoDivider([I)V

    return-void
.end method

.method private createViewByIndex(I)Landroid/view/View;
    .locals 5

    const/4 v3, -0x2

    const/4 v2, 0x0

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    packed-switch p1, :pswitch_data_0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "error index"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_0
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/incallui/view/CallCardOptionalInfoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v3, 0x7f060002

    invoke-virtual {p0, v3}, Lcom/android/incallui/view/CallCardOptionalInfoView;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const v3, 0x7f09000c

    invoke-virtual {p0, v3}, Lcom/android/incallui/view/CallCardOptionalInfoView;->getSize(I)I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-virtual {v1, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    move-object v2, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getViewByIndex(I)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "error index"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v1, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallProviderInfo:Landroid/widget/TextView;

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallProviderInfo:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallCardExtraTitle:Landroid/widget/TextView;

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallCardExtraTitle:Landroid/widget/TextView;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected getDividerView(I)Landroid/view/View;
    .locals 5

    const/4 v4, -0x2

    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/view/CallCardOptionalInfoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/android/incallui/view/CallCardOptionalInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    float-to-int v0, v3

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    const/16 v3, 0x10

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v3, 0x7f02008f

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object v1
.end method

.method protected getTotalViewCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public removeViewByIndex(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/incallui/view/IndexSortedView;->removeViewByIndex(I)V

    return-void
.end method

.method public showExtraInfo(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallCardExtraTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallCardExtraTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallCardExtraTitle:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/CallCardOptionalInfoView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallCardExtraTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallCardExtraTitle:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public showProviderInfo(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallProviderInfo:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallProviderInfo:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallProviderInfo:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/CallCardOptionalInfoView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallProviderInfo:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardOptionalInfoView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/CallCardOptionalInfoView;->mCallProviderInfo:Landroid/widget/TextView;

    goto :goto_0
.end method
