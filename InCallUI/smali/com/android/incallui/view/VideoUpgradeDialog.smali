.class public Lcom/android/incallui/view/VideoUpgradeDialog;
.super Lcom/android/incallui/view/InCallDialog;
.source "VideoUpgradeDialog.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/incallui/view/VideoUpgradeDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/view/VideoUpgradeDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/InCallDialog;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/incallui/view/VideoUpgradeDialog;->configureDialog()V

    return-void
.end method

.method private configureDialog()V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x7f0b00a1

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/VideoUpgradeDialog;->setMessage(I)V

    const v0, 0x7f0b0072

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/VideoUpgradeDialog;->setPositiveButton(I)V

    const v0, 0x7f0b0073

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/VideoUpgradeDialog;->setNegativeButton(I)V

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/VideoUpgradeDialog;->setCancelable(Z)V

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/VideoUpgradeDialog;->setCanceledOnTouchOutside(Z)V

    return-void
.end method


# virtual methods
.method protected onNegative()V
    .locals 3

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getVideoUpgradeRequestCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getSessionModificationState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/android/incallui/view/VideoUpgradeDialog;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "processVideoUpgradeRequestCall: User decline."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/incallui/InCallPresenter;->declineUpgradeRequest(Landroid/content/Context;)V

    :goto_0
    const-string/jumbo v1, "video_call"

    const-string/jumbo v2, "video_call_reject_video"

    invoke-static {v1, v2}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    sget-object v1, Lcom/android/incallui/view/VideoUpgradeDialog;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "processVideoUpgradeRequestCall: no upgrade video call"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onPositive()V
    .locals 4

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getVideoUpgradeRequestCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getSessionModificationState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/android/incallui/view/VideoUpgradeDialog;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "processVideoUpgradeRequestCall: User accept."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getModifyToVideoState()I

    move-result v2

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/incallui/InCallPresenter;->acceptUpgradeRequest(ILandroid/content/Context;)V

    :goto_0
    const-string/jumbo v1, "video_call"

    const-string/jumbo v2, "video_call_agreen_video"

    invoke-static {v1, v2}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const v1, 0x7f0b009b

    invoke-static {v1}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    goto :goto_0
.end method
