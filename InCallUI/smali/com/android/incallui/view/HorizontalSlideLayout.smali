.class public Lcom/android/incallui/view/HorizontalSlideLayout;
.super Landroid/view/ViewGroup;
.source "HorizontalSlideLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/HorizontalSlideLayout$1;,
        Lcom/android/incallui/view/HorizontalSlideLayout$OnSlideFinishListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

.field mAnswerFinalLeft:I

.field mAnswerHeight:I

.field mAnswerIndicator:Landroid/view/View;

.field mAnswerInitLeft:I

.field mAnswerLayout:Landroid/view/View;

.field mAnswerRange:I

.field mAnswerTop:I

.field mAnswerWidth:I

.field private mCallback:Landroid/support/v4/widget/ViewDragHelper$Callback;

.field mIndicatorSpace:I

.field mLongCoverIndicatorDistance:I

.field mOnSlideFinishListener:Lcom/android/incallui/view/HorizontalSlideLayout$OnSlideFinishListener;

.field mRejectAnimatorSet:Landroid/animation/AnimatorSet;

.field mRejectFinalLeft:I

.field mRejectHeight:I

.field mRejectIndicator:Landroid/view/View;

.field mRejectInitLeft:I

.field mRejectLayout:Landroid/view/View;

.field mRejectRange:I

.field mRejectTop:I

.field mRejectWidth:I

.field mScaleRate:F

.field mShortCoverIndicatorDistance:I

.field private mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

.field mViewHeight:I

.field mViewWidth:I


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/incallui/view/HorizontalSlideLayout;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/view/HorizontalSlideLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/HorizontalSlideLayout;->internalDoAnswer(Z)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/incallui/view/HorizontalSlideLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/HorizontalSlideLayout;->internalDoReject(Z)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/incallui/view/HorizontalSlideLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/HorizontalSlideLayout;->internalInitAnswer(Z)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/incallui/view/HorizontalSlideLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/HorizontalSlideLayout;->internalInitReject(Z)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/incallui/view/HorizontalSlideLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/HorizontalSlideLayout;->startAnswerIndicatorAnim(Z)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/incallui/view/HorizontalSlideLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/HorizontalSlideLayout;->startRejectIndicatorAnim(Z)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/incallui/view/HorizontalSlideLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->stopAnswerIndicatorAnim()V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/incallui/view/HorizontalSlideLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->stopRejectIndicatorAnim()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/incallui/view/HorizontalSlideLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/view/HorizontalSlideLayout;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/HorizontalSlideLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/view/HorizontalSlideLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v0, 0x3f933333    # 1.15f

    iput v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mScaleRate:F

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mOnSlideFinishListener:Lcom/android/incallui/view/HorizontalSlideLayout$OnSlideFinishListener;

    new-instance v0, Lcom/android/incallui/view/HorizontalSlideLayout$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/view/HorizontalSlideLayout$1;-><init>(Lcom/android/incallui/view/HorizontalSlideLayout;)V

    iput-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mCallback:Landroid/support/v4/widget/ViewDragHelper$Callback;

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mCallback:Landroid/support/v4/widget/ViewDragHelper$Callback;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {p0, v1, v0}, Landroid/support/v4/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mIndicatorSpace:I

    invoke-virtual {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mShortCoverIndicatorDistance:I

    invoke-virtual {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mLongCoverIndicatorDistance:I

    return-void
.end method

.method private internalDoAnswer(Z)V
    .locals 6

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerFinalLeft:I

    iget v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerFinalLeft:I

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    iget v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerFinalLeft:I

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerWidth:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    iget v5, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerHeight:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private internalDoReject(Z)V
    .locals 6

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectFinalLeft:I

    iget v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectFinalLeft:I

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    iget v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectFinalLeft:I

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectWidth:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    iget v5, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectHeight:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private internalInitAnswer(Z)V
    .locals 6

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerInitLeft:I

    iget v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerInitLeft:I

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    iget v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerInitLeft:I

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerWidth:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    iget v5, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerHeight:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private internalInitReject(Z)V
    .locals 6

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectInitLeft:I

    iget v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectInitLeft:I

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    iget v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectInitLeft:I

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectWidth:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    iget v5, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectHeight:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private startAnswerIndicatorAnim(Z)V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    invoke-direct {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->stopAnswerIndicatorAnim()V

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    const-string/jumbo v3, "translationX"

    new-array v4, v7, [F

    aput v5, v4, v9

    iget v5, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mLongCoverIndicatorDistance:I

    neg-int v5, v5

    int-to-float v5, v5

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v7, [F

    fill-array-data v4, :array_0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :goto_0
    invoke-virtual {v1, v6}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    invoke-virtual {v1, v8}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    invoke-virtual {v0, v6}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    invoke-virtual {v0, v8}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    const-string/jumbo v3, "translationX"

    new-array v4, v7, [F

    aput v5, v4, v9

    iget v5, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mShortCoverIndicatorDistance:I

    neg-int v5, v5

    int-to-float v5, v5

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v7, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x4b0

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    goto :goto_0

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private startRejectIndicatorAnim(Z)V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    invoke-direct {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->stopRejectIndicatorAnim()V

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    const-string/jumbo v3, "translationX"

    new-array v4, v7, [F

    aput v5, v4, v9

    iget v5, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mLongCoverIndicatorDistance:I

    int-to-float v5, v5

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v7, [F

    fill-array-data v4, :array_0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :goto_0
    invoke-virtual {v1, v6}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    invoke-virtual {v1, v8}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    invoke-virtual {v0, v6}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    invoke-virtual {v0, v8}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    const-string/jumbo v3, "translationX"

    new-array v4, v7, [F

    aput v5, v4, v9

    iget v5, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mShortCoverIndicatorDistance:I

    int-to-float v5, v5

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v7, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x4b0

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    goto :goto_0

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private stopAnswerIndicatorAnim()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    iput-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerAnimatorSet:Landroid/animation/AnimatorSet;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method private stopRejectIndicatorAnim()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    iput-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectAnimatorSet:Landroid/animation/AnimatorSet;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public hidePanel()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->isPanelShowing()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/HorizontalSlideLayout;->setVisibility(I)V

    return-void
.end method

.method internalShowPanel()V
    .locals 2

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->requestLayout()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/HorizontalSlideLayout;->setVisibility(I)V

    return-void
.end method

.method public isPanelShowing()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    const v0, 0x7f0a000d

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/HorizontalSlideLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    const v0, 0x7f0a0011

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/HorizontalSlideLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    const v0, 0x7f0a000f

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/HorizontalSlideLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    const v0, 0x7f0a0010

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/HorizontalSlideLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 14

    iget-object v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    iget v9, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectInitLeft:I

    iget v10, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    iget v11, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectInitLeft:I

    iget v12, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectWidth:I

    add-int/2addr v11, v12

    iget v12, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    iget v13, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    sub-int/2addr v12, v13

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    iget-object v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    iget v9, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerInitLeft:I

    iget v10, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    iget v11, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerInitLeft:I

    iget v12, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerWidth:I

    add-int/2addr v11, v12

    iget v12, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    iget v13, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    sub-int/2addr v12, v13

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    iget-object v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    iget-object v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget-object v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectInitLeft:I

    iget v9, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectWidth:I

    add-int/2addr v8, v9

    iget v9, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mIndicatorSpace:I

    add-int v6, v8, v9

    iget v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    div-int/lit8 v8, v8, 0x2

    div-int/lit8 v9, v4, 0x2

    sub-int v7, v8, v9

    iget-object v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectIndicator:Landroid/view/View;

    add-int v9, v6, v5

    iget v10, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    sub-int/2addr v10, v7

    invoke-virtual {v8, v6, v7, v9, v10}, Landroid/view/View;->layout(IIII)V

    iget v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerInitLeft:I

    iget v9, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mIndicatorSpace:I

    sub-int/2addr v8, v9

    sub-int v2, v8, v1

    iget v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    div-int/lit8 v8, v8, 0x2

    div-int/lit8 v9, v0, 0x2

    sub-int v3, v8, v9

    iget-object v8, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerIndicator:Landroid/view/View;

    add-int v9, v2, v1

    iget v10, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    sub-int/2addr v10, v3

    invoke-virtual {v8, v2, v3, v9, v10}, Landroid/view/View;->layout(IIII)V

    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/android/incallui/view/HorizontalSlideLayout;->startRejectIndicatorAnim(Z)V

    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/android/incallui/view/HorizontalSlideLayout;->startAnswerIndicatorAnim(Z)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {p0, p1, p2}, Lcom/android/incallui/view/HorizontalSlideLayout;->measureChildren(II)V

    iget-object v3, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v3, v0

    iget v4, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mScaleRate:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    add-float/2addr v3, v4

    float-to-int v1, v3

    sget-object v3, Lcom/android/incallui/view/HorizontalSlideLayout;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onMeasure:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2, v1}, Lcom/android/incallui/view/HorizontalSlideLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4

    sget-object v1, Lcom/android/incallui/view/HorizontalSlideLayout;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onSizeChanged:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput p1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewWidth:I

    iput p2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectWidth:I

    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectHeight:I

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mScaleRate:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    add-float/2addr v1, v2

    float-to-int v1, v1

    div-int/lit8 v0, v1, 0x2

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectHeight:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectTop:I

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectRange:I

    iput v0, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectInitLeft:I

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectRange:I

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectWidth:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mRejectFinalLeft:I

    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerWidth:I

    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerHeight:I

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewHeight:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerHeight:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerTop:I

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerRange:I

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewWidth:I

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerWidth:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerInitLeft:I

    iget v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewWidth:I

    iget v2, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerRange:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mAnswerFinalLeft:I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mViewDragHelper:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setOnSlideFinishListener(Lcom/android/incallui/view/HorizontalSlideLayout$OnSlideFinishListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/HorizontalSlideLayout;->mOnSlideFinishListener:Lcom/android/incallui/view/HorizontalSlideLayout$OnSlideFinishListener;

    return-void
.end method

.method public showPanel()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->isPanelShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/view/HorizontalSlideLayout;->internalShowPanel()V

    return-void
.end method
