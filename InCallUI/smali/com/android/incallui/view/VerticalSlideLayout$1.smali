.class Lcom/android/incallui/view/VerticalSlideLayout$1;
.super Landroid/support/v4/widget/ViewDragHelper$Callback;
.source "VerticalSlideLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/view/VerticalSlideLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/VerticalSlideLayout;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/VerticalSlideLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-direct {p0}, Landroid/support/v4/widget/ViewDragHelper$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public clampViewPositionHorizontal(Landroid/view/View;II)I
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    return v0
.end method

.method public clampViewPositionVertical(Landroid/view/View;II)I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->-get9(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v0

    if-le p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->-get9(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->-get10(Lcom/android/incallui/view/VerticalSlideLayout;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->-get1(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v0

    if-ge p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->-get1(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result p2

    :cond_1
    :goto_0
    return p2

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->-get8(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v0

    if-ge p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->-get8(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result p2

    goto :goto_0
.end method

.method public getViewHorizontalDragRange(Landroid/view/View;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onViewCaptured(Landroid/view/View;I)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/support/v4/widget/ViewDragHelper$Callback;->onViewCaptured(Landroid/view/View;I)V

    return-void
.end method

.method public onViewDragStateChanged(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get2(Lcom/android/incallui/view/VerticalSlideLayout;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get1(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get6(Lcom/android/incallui/view/VerticalSlideLayout;)Lcom/android/incallui/view/VerticalSlideLayout$OnSlideFinishListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get6(Lcom/android/incallui/view/VerticalSlideLayout;)Lcom/android/incallui/view/VerticalSlideLayout$OnSlideFinishListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/incallui/view/VerticalSlideLayout$OnSlideFinishListener;->onAnswer()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get9(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get10(Lcom/android/incallui/view/VerticalSlideLayout;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get6(Lcom/android/incallui/view/VerticalSlideLayout;)Lcom/android/incallui/view/VerticalSlideLayout$OnSlideFinishListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get6(Lcom/android/incallui/view/VerticalSlideLayout;)Lcom/android/incallui/view/VerticalSlideLayout$OnSlideFinishListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/incallui/view/VerticalSlideLayout$OnSlideFinishListener;->onReject()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get5(Lcom/android/incallui/view/VerticalSlideLayout;)Lcom/android/incallui/view/VerticalSlideLayout$OnEndCallSlideListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VerticalSlideLayout;->-get5(Lcom/android/incallui/view/VerticalSlideLayout;)Lcom/android/incallui/view/VerticalSlideLayout$OnEndCallSlideListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/incallui/view/VerticalSlideLayout$OnEndCallSlideListener;->onEndCallSlide()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
    .locals 0

    return-void
.end method

.method public onViewReleased(Landroid/view/View;FF)V
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-get2(Lcom/android/incallui/view/VerticalSlideLayout;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-get8(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v2

    int-to-float v2, v2

    sub-float v0, v1, v2

    cmpg-float v2, v0, v3

    if-gez v2, :cond_3

    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-get1(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v3}, Lcom/android/incallui/view/VerticalSlideLayout;->-get8(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-get4(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v2, p3, v2

    if-gez v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-wrap0(Lcom/android/incallui/view/VerticalSlideLayout;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-wrap2(Lcom/android/incallui/view/VerticalSlideLayout;)V

    goto :goto_0

    :cond_3
    cmpl-float v2, v0, v3

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-get9(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v3}, Lcom/android/incallui/view/VerticalSlideLayout;->-get3(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v3}, Lcom/android/incallui/view/VerticalSlideLayout;->-get7(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-gtz v2, :cond_4

    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-get4(Lcom/android/incallui/view/VerticalSlideLayout;)I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, p3, v2

    if-lez v2, :cond_5

    :cond_4
    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-wrap1(Lcom/android/incallui/view/VerticalSlideLayout;)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VerticalSlideLayout;->-wrap3(Lcom/android/incallui/view/VerticalSlideLayout;)V

    goto :goto_0
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
    .locals 3

    invoke-static {}, Lcom/android/incallui/view/VerticalSlideLayout;->-get0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "tryCaptureView child view\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/view/VerticalSlideLayout$1;->this$0:Lcom/android/incallui/view/VerticalSlideLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VerticalSlideLayout;->-get2(Lcom/android/incallui/view/VerticalSlideLayout;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
