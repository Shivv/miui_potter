.class public Lcom/android/incallui/view/SimCardPickDialog;
.super Ljava/lang/Object;
.source "SimCardPickDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/SimCardPickDialog$1;,
        Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;,
        Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;,
        Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAlertDialog:Landroid/app/Dialog;

.field private mDisableTextColor:I

.field mHandler:Landroid/os/Handler;

.field private mHighlightTextColor:I

.field private mInflater:Landroid/view/LayoutInflater;

.field private mOnSimCardPickCallBack:Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;

.field private mOriginalSubId:J

.field private mOtherPhoneAccountHandles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

.field private mSimPhoneAccountHandles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;"
        }
    .end annotation
.end field

.field private mSipPhoneAccountHandles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/SimCardPickDialog;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/view/SimCardPickDialog;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mDisableTextColor:I

    return v0
.end method

.method static synthetic -get2(Lcom/android/incallui/view/SimCardPickDialog;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mHighlightTextColor:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/incallui/view/SimCardPickDialog;)Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/view/SimCardPickDialog;Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/SimCardPickDialog;->accountSelectStateRecord(Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/incallui/view/SimCardPickDialog;Landroid/telecom/PhoneAccountHandle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/SimCardPickDialog;->onPickSimCard(Landroid/telecom/PhoneAccountHandle;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/incallui/view/SimCardPickDialog;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/view/SimCardPickDialog;->updateSimCardView(II)V

    return-void
.end method

.method public constructor <init>(Lcom/android/incallui/InCallActivity;J)V
    .locals 4

    const/4 v2, 0x4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSimPhoneAccountHandles:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSipPhoneAccountHandles:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOtherPhoneAccountHandles:Ljava/util/List;

    new-instance v0, Lcom/android/incallui/view/SimCardPickDialog$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/view/SimCardPickDialog$1;-><init>(Lcom/android/incallui/view/SimCardPickDialog;)V

    iput-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mHighlightTextColor:I

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mDisableTextColor:I

    iput-wide p2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOriginalSubId:J

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private accountSelectStateRecord(Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getType()I

    move-result v1

    const/16 v2, 0xb

    if-ne v1, v2, :cond_1

    const-string/jumbo v1, "type"

    const-string/jumbo v2, "sim"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "slotId"

    invoke-virtual {p1}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    const-string/jumbo v1, "account_select"

    const-string/jumbo v2, "select_sim_or_sip_account"

    invoke-static {v1, v2, v0}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getType()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    const-string/jumbo v1, "type"

    const-string/jumbo v2, "sip"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "id"

    invoke-virtual {p1}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private closeDialog()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iput-object v2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method private getCurrentCallSlotId()I
    .locals 2

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getActiveOrBackgroundCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getSlotId()I

    move-result v1

    return v1

    :cond_0
    sget v1, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    return v1
.end method

.method private getDisplayName(ILmiui/telephony/SubscriptionInfo;)Ljava/lang/CharSequence;
    .locals 10

    const/4 v5, 0x0

    invoke-virtual {p2}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v6

    if-eq p1, v6, :cond_0

    invoke-virtual {p2}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v5

    return-object v5

    :cond_0
    const-string/jumbo v0, "carrierName"

    const-string/jumbo v1, "getCarrierName"

    const-string/jumbo v2, "content://com.miui.virtualsim.provider.virtualsimInfo"

    const/4 v3, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v1, v8, v9}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    if-nez v3, :cond_1

    :goto_1
    return-object v5

    :catch_0
    move-exception v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "getVirtualSimCarrierName: e"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method private getPhoneAccountHandleBySlotId(I)Landroid/telecom/PhoneAccountHandle;
    .locals 5

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSimPhoneAccountHandles:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v3

    sget v4, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    if-eq v3, v4, :cond_1

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSimPhoneAccountHandles:Ljava/util/List;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/android/incallui/CallAdapterUtils;->getSubIdByAccount(Landroid/telecom/PhoneAccountHandle;)I

    move-result v4

    if-ne v3, v4, :cond_0

    move-object v0, v1

    :cond_1
    return-object v0
.end method

.method private getTitleView()Landroid/view/View;
    .locals 10

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/android/incallui/view/SimCardPickDialog;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f030002

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v6, 0x7f0a0016

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v6, 0x7f0a0017

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->getCurrentCallSlotId()I

    move-result v0

    sget v6, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-eq v0, v6, :cond_1

    iget-object v6, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    new-array v7, v5, [Ljava/lang/Object;

    add-int/lit8 v8, v0, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    const v8, 0x7f0b007f

    invoke-virtual {v6, v8, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    new-array v7, v4, [Ljava/lang/Object;

    add-int/lit8 v8, v0, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    if-nez v0, :cond_0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v5

    const v4, 0x7f0b007e

    invoke-virtual {v6, v4, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-object v3

    :cond_0
    move v4, v5

    goto :goto_0

    :cond_1
    const v4, 0x7f0b007c

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private initAdapter()V
    .locals 21

    new-instance v3, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/incallui/view/SimCardPickDialog;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;-><init>(Lcom/android/incallui/view/SimCardPickDialog;Landroid/view/LayoutInflater;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/incallui/view/SimCardPickDialog;->mSimPhoneAccountHandles:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v18

    invoke-direct/range {p0 .. p0}, Lcom/android/incallui/view/SimCardPickDialog;->getCurrentCallSlotId()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Landroid/provider/MiuiSettings$VirtualSim;->isVirtualSimEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Landroid/provider/MiuiSettings$VirtualSim;->getVirtualSimSlotId(Landroid/content/Context;)I

    move-result v20

    :goto_0
    const/4 v7, 0x0

    :goto_1
    move/from16 v0, v18

    if-ge v7, v0, :cond_4

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3, v7}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v19

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    new-instance v3, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;

    sget v4, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-eq v15, v4, :cond_0

    if-ne v7, v15, :cond_3

    :cond_0
    const/4 v6, 0x1

    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/android/incallui/view/SimCardPickDialog;->getDisplayName(ILmiui/telephony/SubscriptionInfo;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual/range {v19 .. v19}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/incallui/view/SimCardPickDialog;->getPhoneAccountHandleBySlotId(I)Landroid/telecom/PhoneAccountHandle;

    move-result-object v10

    const/16 v5, 0xb

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v10}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;-><init>(Lcom/android/incallui/view/SimCardPickDialog;IZILjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/telecom/PhoneAccountHandle;)V

    invoke-virtual {v11, v3}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->add(Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;)V

    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_2
    sget v20, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    :cond_4
    sget v3, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-ne v15, v3, :cond_5

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/incallui/view/SimCardPickDialog;->mSipPhoneAccountHandles:Ljava/util/List;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/telecom/PhoneAccountHandle;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    new-instance v8, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;

    add-int/lit8 v16, v12, 0x1

    invoke-virtual {v14}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v13

    const/16 v10, 0xc

    const/4 v11, 0x1

    move-object/from16 v9, p0

    invoke-direct/range {v8 .. v14}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;-><init>(Lcom/android/incallui/view/SimCardPickDialog;IZILjava/lang/String;Landroid/telecom/PhoneAccountHandle;)V

    invoke-virtual {v3, v8}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->add(Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;)V

    move/from16 v12, v16

    goto :goto_3

    :cond_5
    return-void
.end method

.method private onPickSimCard(Landroid/telecom/PhoneAccountHandle;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->closeDialog()V

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOnSimCardPickCallBack:Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOnSimCardPickCallBack:Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;

    invoke-interface {v0, p1}, Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;->onSimCardPick(Landroid/telecom/PhoneAccountHandle;)V

    :cond_0
    return-void
.end method

.method private resizeDialog()V
    .locals 4

    iget-object v2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090072

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method private rewriteSlotId(I)I
    .locals 2

    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->getCurrentCallSlotId()I

    move-result v0

    sget v1, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-eq v0, v1, :cond_0

    if-eq p1, v0, :cond_0

    move p1, v0

    :cond_0
    return p1
.end method

.method private showDialog()V
    .locals 8

    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->initAdapter()V

    new-instance v0, Lmiui/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v4}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->getTitleView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmiui/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Lmiui/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    new-instance v5, Lcom/android/incallui/view/SimCardPickDialog$2;

    invoke-direct {v5, p0}, Lcom/android/incallui/view/SimCardPickDialog$2;-><init>(Lcom/android/incallui/view/SimCardPickDialog;)V

    const/4 v6, -0x1

    invoke-virtual {v0, v4, v6, v5}, Lmiui/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v4, Lcom/android/incallui/view/SimCardPickDialog$3;

    invoke-direct {v4, p0}, Lcom/android/incallui/view/SimCardPickDialog$3;-><init>(Lcom/android/incallui/view/SimCardPickDialog;)V

    const v5, 0x7f0b0073

    invoke-virtual {v0, v5, v4}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v4, Lcom/android/incallui/view/SimCardPickDialog$4;

    invoke-direct {v4, p0}, Lcom/android/incallui/view/SimCardPickDialog$4;-><init>(Lcom/android/incallui/view/SimCardPickDialog;)V

    invoke-virtual {v0, v4}, Lmiui/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090074

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    invoke-virtual {v3, v4, v2, v5, v2}, Landroid/view/View;->setPadding(IIII)V

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    invoke-virtual {v4}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->getCount()I

    move-result v4

    const/4 v5, 0x4

    if-le v4, v5, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->resizeDialog()V

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->getCurrentCallSlotId()I

    move-result v4

    sget v5, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-eq v4, v5, :cond_1

    return-void

    :cond_1
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOriginalSubId:J

    long-to-int v5, v6

    invoke-virtual {v4, v5}, Lmiui/telephony/SubscriptionManager;->getSlotIdForSubscription(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/SimCardPickDialog;->rewriteSlotId(I)I

    move-result v1

    sget v4, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-eq v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/android/incallui/util/Utils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    const/16 v5, 0xb

    invoke-virtual {v4, v5, v1}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->getPositionWithTypeAndId(II)I

    move-result v4

    const/4 v5, 0x5

    invoke-direct {p0, v4, v5}, Lcom/android/incallui/view/SimCardPickDialog;->updateSimCardView(II)V

    :cond_2
    return-void
.end method

.method private splitePhoneAccountHandlesAndShow(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v2}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "E"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_0

    const-string/jumbo v4, "TelephonyConnectionService"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSimPhoneAccountHandles:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string/jumbo v4, "SipConnectionService"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSipPhoneAccountHandles:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOtherPhoneAccountHandles:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->showDialog()V

    return-void
.end method

.method private updateSimCardView(II)V
    .locals 6

    if-gez p1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    invoke-virtual {v2, p1}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->getItem(I)Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->setLastTime(I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->setDefault(Z)V

    if-gez p2, :cond_1

    invoke-direct {p0, v0}, Lcom/android/incallui/view/SimCardPickDialog;->accountSelectStateRecord(Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;)V

    invoke-virtual {v0}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/incallui/view/SimCardPickDialog;->onPickSimCard(Landroid/telecom/PhoneAccountHandle;)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mSelectorAdapter:Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->notifyDataSetChanged()V

    if-ltz p2, :cond_2

    add-int/lit8 p2, p2, -0x1

    iget-object v2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x66

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->arg1:I

    iput p2, v1, Landroid/os/Message;->arg2:I

    iget-object v2, p0, Lcom/android/incallui/view/SimCardPickDialog;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_2
    return-void
.end method


# virtual methods
.method public cancelDialog()V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/view/SimCardPickDialog;->closeDialog()V

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOnSimCardPickCallBack:Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOnSimCardPickCallBack:Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;

    invoke-interface {v0}, Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;->onCancel()V

    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maybeCancelDialog()V
    .locals 2

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    iget-wide v0, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOriginalSubId:J

    long-to-int v0, v0

    invoke-static {v0}, Lmiui/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/view/SimCardPickDialog;->cancelDialog()V

    return-void
.end method

.method public setOnSimCardPickCallBack(Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/SimCardPickDialog;->mOnSimCardPickCallBack:Lcom/android/incallui/view/SimCardPickDialog$OnSimCardPickCallBack;

    return-void
.end method

.method public showSimCardPickDialog(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/incallui/view/SimCardPickDialog;->splitePhoneAccountHandlesAndShow(Ljava/util/List;)V

    return-void
.end method
