.class Lcom/android/incallui/view/AnimationCircleLayout$1;
.super Ljava/lang/Object;
.source "AnimationCircleLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/AnimationCircleLayout;->startAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/AnimationCircleLayout;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/AnimationCircleLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    iget-object v1, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-get7(Lcom/android/incallui/view/AnimationCircleLayout;)F

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->-get3(Lcom/android/incallui/view/AnimationCircleLayout;)F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    invoke-static {v1, v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-set2(Lcom/android/incallui/view/AnimationCircleLayout;F)F

    iget-object v1, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-get5(Lcom/android/incallui/view/AnimationCircleLayout;)F

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->-get1(Lcom/android/incallui/view/AnimationCircleLayout;)F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    invoke-static {v1, v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-set0(Lcom/android/incallui/view/AnimationCircleLayout;F)F

    iget-object v1, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    iget-object v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-get6(Lcom/android/incallui/view/AnimationCircleLayout;)F

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-static {v3}, Lcom/android/incallui/view/AnimationCircleLayout;->-get2(Lcom/android/incallui/view/AnimationCircleLayout;)F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    invoke-static {v1, v2}, Lcom/android/incallui/view/AnimationCircleLayout;->-set1(Lcom/android/incallui/view/AnimationCircleLayout;F)F

    iget-object v1, p0, Lcom/android/incallui/view/AnimationCircleLayout$1;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-virtual {v1}, Lcom/android/incallui/view/AnimationCircleLayout;->invalidate()V

    return-void
.end method
