.class Lcom/android/incallui/CallerInfoUtils$1$1;
.super Ljava/lang/Object;
.source "CallerInfoUtils.java"

# interfaces
.implements Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/CallerInfoUtils$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/incallui/CallerInfoUtils$1;

.field final synthetic val$call:Lcom/android/incallui/Call;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$info:Lcom/android/incallui/CallerInfo;

.field final synthetic val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;


# direct methods
.method constructor <init>(Lcom/android/incallui/CallerInfoUtils$1;Landroid/content/Context;Lcom/android/incallui/CallerInfo;Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;Lcom/android/incallui/Call;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->this$1:Lcom/android/incallui/CallerInfoUtils$1;

    iput-object p2, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->val$info:Lcom/android/incallui/CallerInfo;

    iput-object p4, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    iput-object p5, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->val$call:Lcom/android/incallui/Call;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPermissionDenied()V
    .locals 1

    const v0, 0x7f0b008e

    invoke-static {v0}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    return-void
.end method

.method public onPermissionGranted()V
    .locals 5

    invoke-static {}, Lcom/android/incallui/CallerInfoUtils;->-get0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onPermissionGranted: Actually starting CallerInfoAsyncQuery.startQuery()..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->val$info:Lcom/android/incallui/CallerInfo;

    iget-object v2, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    iget-object v3, p0, Lcom/android/incallui/CallerInfoUtils$1$1;->val$call:Lcom/android/incallui/Call;

    const/4 v4, -0x1

    invoke-static {v4, v0, v1, v2, v3}, Lcom/android/incallui/CallerInfoAsyncQuery;->startQuery(ILandroid/content/Context;Lcom/android/incallui/CallerInfo;Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;Ljava/lang/Object;)Lcom/android/incallui/CallerInfoAsyncQuery;

    return-void
.end method
