.class final Landroid/support/v4/widget/BakedBezierInterpolator;
.super Ljava/lang/Object;
.source "BakedBezierInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# static fields
.field private static final INSTANCE:Landroid/support/v4/widget/BakedBezierInterpolator;

.field private static final STEP_SIZE:F

.field private static final VALUES:[F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    new-instance v0, Landroid/support/v4/widget/BakedBezierInterpolator;

    invoke-direct {v0}, Landroid/support/v4/widget/BakedBezierInterpolator;-><init>()V

    sput-object v0, Landroid/support/v4/widget/BakedBezierInterpolator;->INSTANCE:Landroid/support/v4/widget/BakedBezierInterpolator;

    const/16 v0, 0x65

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v1, v0, v2

    const v1, 0x3951b717    # 2.0E-4f

    const/4 v2, 0x1

    aput v1, v0, v2

    const v1, 0x3a6bedfa    # 9.0E-4f

    const/4 v2, 0x2

    aput v1, v0, v2

    const v1, 0x3af9096c    # 0.0019f

    const/4 v2, 0x3

    aput v1, v0, v2

    const v1, 0x3b6bedfa    # 0.0036f

    const/4 v2, 0x4

    aput v1, v0, v2

    const v1, 0x3bc154ca    # 0.0059f

    const/4 v2, 0x5

    aput v1, v0, v2

    const v1, 0x3c0ce704    # 0.0086f

    const/4 v2, 0x6

    aput v1, v0, v2

    const v1, 0x3c42f838    # 0.0119f

    const/4 v2, 0x7

    aput v1, v0, v2

    const v1, 0x3c809d49    # 0.0157f

    const/16 v2, 0x8

    aput v1, v0, v2

    const v1, 0x3cab367a    # 0.0209f

    const/16 v2, 0x9

    aput v1, v0, v2

    const v1, 0x3cd288ce    # 0.0257f

    const/16 v2, 0xa

    aput v1, v0, v2

    const v1, 0x3d037b4a

    const/16 v2, 0xb

    aput v1, v0, v2

    const v1, 0x3d20902e    # 0.0392f

    const/16 v2, 0xc

    aput v1, v0, v2

    const v1, 0x3d401a37    # 0.0469f

    const/16 v2, 0xd

    aput v1, v0, v2

    const v1, 0x3d67d567    # 0.0566f

    const/16 v2, 0xe

    aput v1, v0, v2

    const v1, 0x3d86594b    # 0.0656f

    const/16 v2, 0xf

    aput v1, v0, v2

    const v1, 0x3d9d4952    # 0.0768f

    const/16 v2, 0x10

    aput v1, v0, v2

    const v1, 0x3db5a858    # 0.0887f

    const/16 v2, 0x11

    aput v1, v0, v2

    const v1, 0x3dd38ef3    # 0.1033f

    const/16 v2, 0x12

    aput v1, v0, v2

    const v1, 0x3df2e48f    # 0.1186f

    const/16 v2, 0x13

    aput v1, v0, v2

    const v1, 0x3e0a233a    # 0.1349f

    const/16 v2, 0x14

    aput v1, v0, v2

    const v1, 0x3e1b8bac    # 0.1519f

    const/16 v2, 0x15

    aput v1, v0, v2

    const v1, 0x3e2dab9f    # 0.1696f

    const/16 v2, 0x16

    aput v1, v0, v2

    const v1, 0x3e456d5d    # 0.1928f

    const/16 v2, 0x17

    aput v1, v0, v2

    const v1, 0x3e5930be    # 0.2121f

    const/16 v2, 0x18

    aput v1, v0, v2

    const v1, 0x3e72b021    # 0.237f

    const/16 v2, 0x19

    aput v1, v0, v2

    const v1, 0x3e86809d    # 0.2627f

    const/16 v2, 0x1a

    aput v1, v0, v2

    const v1, 0x3e941206    # 0.2892f

    const/16 v2, 0x1b

    aput v1, v0, v2

    const v1, 0x3e9f2e49    # 0.3109f

    const/16 v2, 0x1c

    aput v1, v0, v2

    const v1, 0x3ead5cfb    # 0.3386f

    const/16 v2, 0x1d

    aput v1, v0, v2

    const v1, 0x3ebbc01a    # 0.3667f

    const/16 v2, 0x1e

    aput v1, v0, v2

    const v1, 0x3eca57a8    # 0.3952f

    const/16 v2, 0x1f

    aput v1, v0, v2

    const v1, 0x3ed923a3    # 0.4241f

    const/16 v2, 0x20

    aput v1, v0, v2

    const v1, 0x3ee5119d    # 0.4474f

    const/16 v2, 0x21

    aput v1, v0, v2

    const v1, 0x3ef404ea    # 0.4766f

    const/16 v2, 0x22

    aput v1, v0, v2

    const/high16 v1, 0x3f000000    # 0.5f

    const/16 v2, 0x23

    aput v1, v0, v2

    const v1, 0x3f05fd8b    # 0.5234f

    const/16 v2, 0x24

    aput v1, v0, v2

    const v1, 0x3f0bfb16    # 0.5468f

    const/16 v2, 0x25

    aput v1, v0, v2

    const v1, 0x3f11f213    # 0.5701f

    const/16 v2, 0x26

    aput v1, v0, v2

    const v1, 0x3f17e282    # 0.5933f

    const/16 v2, 0x27

    aput v1, v0, v2

    const v1, 0x3f1d07c8    # 0.6134f

    const/16 v2, 0x28

    aput v1, v0, v2

    const v1, 0x3f221ff3    # 0.6333f

    const/16 v2, 0x29

    aput v1, v0, v2

    const v1, 0x3f273190    # 0.6531f

    const/16 v2, 0x2a

    aput v1, v0, v2

    const v1, 0x3f2b7803    # 0.6698f

    const/16 v2, 0x2b

    aput v1, v0, v2

    const v1, 0x3f3068dc    # 0.6891f

    const/16 v2, 0x2c

    aput v1, v0, v2

    const v1, 0x3f349518    # 0.7054f

    const/16 v2, 0x2d

    aput v1, v0, v2

    const v1, 0x3f38adac    # 0.7214f

    const/16 v2, 0x2e

    aput v1, v0, v2

    const v1, 0x3f3c0ebf    # 0.7346f

    const/16 v2, 0x2f

    aput v1, v0, v2

    const v1, 0x3f400d1b    # 0.7502f

    const/16 v2, 0x30

    aput v1, v0, v2

    const v1, 0x3f4353f8    # 0.763f

    const/16 v2, 0x31

    aput v1, v0, v2

    const v1, 0x3f468db9    # 0.7756f

    const/16 v2, 0x32

    aput v1, v0, v2

    const v1, 0x3f49b3d0    # 0.7879f

    const/16 v2, 0x33

    aput v1, v0, v2

    const v1, 0x3f4ccccd    # 0.8f

    const/16 v2, 0x34

    aput v1, v0, v2

    const v1, 0x3f4f8a09    # 0.8107f

    const/16 v2, 0x35

    aput v1, v0, v2

    const v1, 0x3f523a2a    # 0.8212f

    const/16 v2, 0x36

    aput v1, v0, v2

    const v1, 0x3f552546    # 0.8326f

    const/16 v2, 0x37

    aput v1, v0, v2

    const v1, 0x3f576c8b    # 0.8415f

    const/16 v2, 0x38

    aput v1, v0, v2

    const v1, 0x3f59ad43    # 0.8503f

    const/16 v2, 0x39

    aput v1, v0, v2

    const v1, 0x3f5bda51    # 0.8588f

    const/16 v2, 0x3a

    aput v1, v0, v2

    const v1, 0x3f5e00d2    # 0.8672f

    const/16 v2, 0x3b

    aput v1, v0, v2

    const v1, 0x3f601a37    # 0.8754f

    const/16 v2, 0x3c

    aput v1, v0, v2

    const v1, 0x3f621ff3    # 0.8833f

    const/16 v2, 0x3d

    aput v1, v0, v2

    const v1, 0x3f641f21    # 0.8911f

    const/16 v2, 0x3e

    aput v1, v0, v2

    const v1, 0x3f65cfab    # 0.8977f

    const/16 v2, 0x3f

    aput v1, v0, v2

    const v1, 0x3f677319    # 0.9041f

    const/16 v2, 0x40

    aput v1, v0, v2

    const v1, 0x3f694af5    # 0.9113f

    const/16 v2, 0x41

    aput v1, v0, v2

    const v1, 0x3f6a9fbe    # 0.9165f

    const/16 v2, 0x42

    aput v1, v0, v2

    const v1, 0x3f6c56d6    # 0.9232f

    const/16 v2, 0x43

    aput v1, v0, v2

    const v1, 0x3f6d97f6    # 0.9281f

    const/16 v2, 0x44

    aput v1, v0, v2

    const v1, 0x3f6ecbfb    # 0.9328f

    const/16 v2, 0x45

    aput v1, v0, v2

    const v1, 0x3f702de0    # 0.9382f

    const/16 v2, 0x46

    aput v1, v0, v2

    const v1, 0x3f7182aa    # 0.9434f

    const/16 v2, 0x47

    aput v1, v0, v2

    const v1, 0x3f7295ea    # 0.9476f

    const/16 v2, 0x48

    aput v1, v0, v2

    const v1, 0x3f73a92a    # 0.9518f

    const/16 v2, 0x49

    aput v1, v0, v2

    const v1, 0x3f74a8c1    # 0.9557f

    const/16 v2, 0x4a

    aput v1, v0, v2

    const v1, 0x3f75a858    # 0.9596f

    const/16 v2, 0x4b

    aput v1, v0, v2

    const v1, 0x3f769446    # 0.9632f

    const/16 v2, 0x4c

    aput v1, v0, v2

    const v1, 0x3f7758e2    # 0.9662f

    const/16 v2, 0x4d

    aput v1, v0, v2

    const v1, 0x3f783127    # 0.9695f

    const/16 v2, 0x4e

    aput v1, v0, v2

    const v1, 0x3f78e219    # 0.9722f

    const/16 v2, 0x4f

    aput v1, v0, v2

    const v1, 0x3f79ad43    # 0.9753f

    const/16 v2, 0x50

    aput v1, v0, v2

    const v1, 0x3f7a4a8c    # 0.9777f

    const/16 v2, 0x51

    aput v1, v0, v2

    const v1, 0x3f7b020c    # 0.9805f

    const/16 v2, 0x52

    aput v1, v0, v2

    const v1, 0x3f7b8bac    # 0.9826f

    const/16 v2, 0x53

    aput v1, v0, v2

    const v1, 0x3f7c154d    # 0.9847f

    const/16 v2, 0x54

    aput v1, v0, v2

    const v1, 0x3f7c91d1    # 0.9866f

    const/16 v2, 0x55

    aput v1, v0, v2

    const v1, 0x3f7d07c8    # 0.9884f

    const/16 v2, 0x56

    aput v1, v0, v2

    const v1, 0x3f7d7732    # 0.9901f

    const/16 v2, 0x57

    aput v1, v0, v2

    const v1, 0x3f7de00d    # 0.9917f

    const/16 v2, 0x58

    aput v1, v0, v2

    const v1, 0x3f7e3bcd    # 0.9931f

    const/16 v2, 0x59

    aput v1, v0, v2

    const v1, 0x3f7e9100    # 0.9944f

    const/16 v2, 0x5a

    aput v1, v0, v2

    const v1, 0x3f7ed917    # 0.9955f

    const/16 v2, 0x5b

    aput v1, v0, v2

    const v1, 0x3f7f1412    # 0.9964f

    const/16 v2, 0x5c

    aput v1, v0, v2

    const v1, 0x3f7f4f0e    # 0.9973f

    const/16 v2, 0x5d

    aput v1, v0, v2

    const v1, 0x3f7f837b    # 0.9981f

    const/16 v2, 0x5e

    aput v1, v0, v2

    const v1, 0x3f7fa440    # 0.9986f

    const/16 v2, 0x5f

    aput v1, v0, v2

    const v1, 0x3f7fcb92    # 0.9992f

    const/16 v2, 0x60

    aput v1, v0, v2

    const v1, 0x3f7fdf3b    # 0.9995f

    const/16 v2, 0x61

    aput v1, v0, v2

    const v1, 0x3f7ff2e5    # 0.9998f

    const/16 v2, 0x62

    aput v1, v0, v2

    const/16 v1, 0x63

    aput v3, v0, v1

    const/16 v1, 0x64

    aput v3, v0, v1

    sput-object v0, Landroid/support/v4/widget/BakedBezierInterpolator;->VALUES:[F

    sget-object v0, Landroid/support/v4/widget/BakedBezierInterpolator;->VALUES:[F

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    div-float v0, v3, v0

    sput v0, Landroid/support/v4/widget/BakedBezierInterpolator;->STEP_SIZE:F

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getInstance()Landroid/support/v4/widget/BakedBezierInterpolator;
    .locals 1

    sget-object v0, Landroid/support/v4/widget/BakedBezierInterpolator;->INSTANCE:Landroid/support/v4/widget/BakedBezierInterpolator;

    return-object v0
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 7

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    cmpl-float v4, p1, v6

    if-ltz v4, :cond_0

    return v6

    :cond_0
    cmpg-float v4, p1, v5

    if-gtz v4, :cond_1

    return v5

    :cond_1
    sget-object v4, Landroid/support/v4/widget/BakedBezierInterpolator;->VALUES:[F

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v4, p1

    float-to-int v4, v4

    sget-object v5, Landroid/support/v4/widget/BakedBezierInterpolator;->VALUES:[F

    array-length v5, v5

    add-int/lit8 v5, v5, -0x2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v4, v1

    sget v5, Landroid/support/v4/widget/BakedBezierInterpolator;->STEP_SIZE:F

    mul-float v2, v4, v5

    sub-float v0, p1, v2

    sget v4, Landroid/support/v4/widget/BakedBezierInterpolator;->STEP_SIZE:F

    div-float v3, v0, v4

    sget-object v4, Landroid/support/v4/widget/BakedBezierInterpolator;->VALUES:[F

    aget v4, v4, v1

    sget-object v5, Landroid/support/v4/widget/BakedBezierInterpolator;->VALUES:[F

    add-int/lit8 v6, v1, 0x1

    aget v5, v5, v6

    sget-object v6, Landroid/support/v4/widget/BakedBezierInterpolator;->VALUES:[F

    aget v6, v6, v1

    sub-float/2addr v5, v6

    mul-float/2addr v5, v3

    add-float/2addr v4, v5

    return v4
.end method
