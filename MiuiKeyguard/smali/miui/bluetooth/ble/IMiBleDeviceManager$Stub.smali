.class public abstract Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;
.super Landroid/os/Binder;
.source "IMiBleDeviceManager.java"

# interfaces
.implements Lmiui/bluetooth/ble/IMiBleDeviceManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/bluetooth/ble/IMiBleDeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "miui.bluetooth.ble.IMiBleDeviceManager"

.field static final TRANSACTION_deleteSettings:I = 0x6

.field static final TRANSACTION_getBoundDevices:I = 0xd

.field static final TRANSACTION_getDeviceSettings:I = 0x5

.field static final TRANSACTION_getDeviceType:I = 0x7

.field static final TRANSACTION_getRegisterAppForBleEvent:I = 0x10

.field static final TRANSACTION_getScanResult:I = 0x11

.field static final TRANSACTION_getServiceVersion:I = 0xc

.field static final TRANSACTION_getSettingInteger:I = 0x4

.field static final TRANSACTION_getSettingString:I = 0x2

.field static final TRANSACTION_registerAppForBleEvent:I = 0xe

.field static final TRANSACTION_registerBleEventListener:I = 0xa

.field static final TRANSACTION_setSettingInteger:I = 0x3

.field static final TRANSACTION_setSettingString:I = 0x1

.field static final TRANSACTION_setToken:I = 0x12

.field static final TRANSACTION_startScanDevice:I = 0x8

.field static final TRANSACTION_stopScanDevice:I = 0x9

.field static final TRANSACTION_unregisterAppForBleEvent:I = 0xf

.field static final TRANSACTION_unregisterBleEventListener:I = 0xb


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string/jumbo v0, "miui.bluetooth.ble.IMiBleDeviceManager"

    invoke-virtual {p0, p0, v0}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lmiui/bluetooth/ble/IMiBleDeviceManager;
    .locals 2

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return-object v1

    :cond_0
    const-string/jumbo v1, "miui.bluetooth.ble.IMiBleDeviceManager"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lmiui/bluetooth/ble/IMiBleDeviceManager;

    if-eqz v1, :cond_1

    check-cast v0, Lmiui/bluetooth/ble/IMiBleDeviceManager;

    return-object v0

    :cond_1
    new-instance v1, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub$Proxy;

    invoke-direct {v1, p0}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sparse-switch p1, :sswitch_data_0

    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v20

    return v20

    :sswitch_0
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v20, 0x1

    return v20

    :sswitch_1
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8, v11}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->setSettingString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_0

    const/16 v20, 0x1

    :goto_0
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_0
    const/16 v20, 0x0

    goto :goto_0

    :sswitch_2
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->getSettingString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v20, 0x1

    return v20

    :sswitch_3
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8, v10}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->setSettingInteger(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_1

    const/16 v20, 0x1

    :goto_1
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_1
    const/16 v20, 0x0

    goto :goto_1

    :sswitch_4
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->getSettingInteger(Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :sswitch_5
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->getDeviceSettings(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v17

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    const/16 v20, 0x1

    return v20

    :sswitch_6
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->deleteSettings(Ljava/lang/String;)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_2

    const/16 v20, 0x1

    :goto_2
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_2
    const/16 v20, 0x0

    goto :goto_2

    :sswitch_7
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->getDeviceType(Ljava/lang/String;)I

    move-result v14

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :sswitch_8
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v20

    if-eqz v20, :cond_3

    sget-object v20, Landroid/os/ParcelUuid;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/ParcelUuid;

    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lmiui/bluetooth/ble/IScanDeviceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/bluetooth/ble/IScanDeviceCallback;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7, v10, v13}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->startScanDevice(Landroid/os/IBinder;Landroid/os/ParcelUuid;ILmiui/bluetooth/ble/IScanDeviceCallback;)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_4

    const/16 v20, 0x1

    :goto_4
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_3
    const/4 v7, 0x0

    goto :goto_3

    :cond_4
    const/16 v20, 0x0

    goto :goto_4

    :sswitch_9
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v20

    if-eqz v20, :cond_5

    sget-object v20, Landroid/os/ParcelUuid;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/ParcelUuid;

    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->stopScanDevice(Landroid/os/ParcelUuid;)V

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    const/16 v20, 0x1

    return v20

    :cond_5
    const/4 v4, 0x0

    goto :goto_5

    :sswitch_a
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lmiui/bluetooth/ble/IBleEventCallback$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/bluetooth/ble/IBleEventCallback;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v12}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->registerBleEventListener(Ljava/lang/String;ILmiui/bluetooth/ble/IBleEventCallback;)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_6

    const/16 v20, 0x1

    :goto_6
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_6
    const/16 v20, 0x0

    goto :goto_6

    :sswitch_b
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lmiui/bluetooth/ble/IBleEventCallback$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/bluetooth/ble/IBleEventCallback;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v12}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->unregisterBleEventListener(Ljava/lang/String;ILmiui/bluetooth/ble/IBleEventCallback;)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_7

    const/16 v20, 0x1

    :goto_7
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_7
    const/16 v20, 0x0

    goto :goto_7

    :sswitch_c
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->getServiceVersion()I

    move-result v14

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :sswitch_d
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->getBoundDevices()Ljava/util/List;

    move-result-object v16

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    const/16 v20, 0x1

    return v20

    :sswitch_e
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->registerAppForBleEvent(Ljava/lang/String;I)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_8

    const/16 v20, 0x1

    :goto_8
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_8
    const/16 v20, 0x0

    goto :goto_8

    :sswitch_f
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->unregisterAppForBleEvent(Ljava/lang/String;I)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_9

    const/16 v20, 0x1

    :goto_9
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_9
    const/16 v20, 0x0

    goto :goto_9

    :sswitch_10
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->getRegisterAppForBleEvent(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v20, 0x1

    return v20

    :sswitch_11
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->getScanResult(Ljava/lang/String;)Lmiui/bluetooth/ble/ScanResult;

    move-result-object v18

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v18, :cond_a

    const/16 v20, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lmiui/bluetooth/ble/ScanResult;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_a
    const/16 v20, 0x1

    return v20

    :cond_a
    const/16 v20, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_a

    :sswitch_12
    const-string/jumbo v20, "miui.bluetooth.ble.IMiBleDeviceManager"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v9

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v9}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->setToken(Ljava/lang/String;[B)Z

    move-result v19

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v19, :cond_b

    const/16 v20, 0x1

    :goto_b
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v20, 0x1

    return v20

    :cond_b
    const/16 v20, 0x0

    goto :goto_b

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
