.class Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setOtherViewVisibility(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

.field final synthetic val$visibility:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iput p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;->val$visibility:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;->val$visibility:I

    if-nez v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v0, v1, v0

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get19(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method
