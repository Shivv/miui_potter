.class public interface abstract Lcom/android/keyguard/doze/DozeHost;
.super Ljava/lang/Object;
.source "DozeHost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/doze/DozeHost$Callback;,
        Lcom/android/keyguard/doze/DozeHost$PulseCallback;
    }
.end annotation


# virtual methods
.method public abstract abortPulsing()V
.end method

.method public abstract addCallback(Lcom/android/keyguard/doze/DozeHost$Callback;)V
.end method

.method public abstract disableInfo(Z)V
.end method

.method public abstract dozeTimeTick()V
.end method

.method public abstract extendPulse()V
.end method

.method public abstract isBlockingDoze()Z
.end method

.method public abstract isDozing()Z
.end method

.method public abstract isPowerSaveActive()Z
.end method

.method public abstract isProvisioned()Z
.end method

.method public abstract isPulsingBlocked()Z
.end method

.method public abstract onDoubleTap(FF)V
.end method

.method public abstract onIgnoreTouchWhilePulsing(Z)V
.end method

.method public abstract pulseWhileDozing(Lcom/android/keyguard/doze/DozeHost$PulseCallback;I)V
.end method

.method public abstract removeCallback(Lcom/android/keyguard/doze/DozeHost$Callback;)V
.end method

.method public abstract setAnimateWakeup(Z)V
.end method

.method public abstract setAodDimmingScrim(F)V
.end method

.method public abstract setDozeScreenBrightness(I)V
.end method

.method public abstract startDozing()V
.end method

.method public abstract startPendingIntentDismissingKeyguard(Landroid/app/PendingIntent;)V
.end method

.method public abstract stopDozing()V
.end method
