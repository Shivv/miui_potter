.class public Lcom/android/keyguard/doze/DozeTriggers;
.super Ljava/lang/Object;
.source "DozeTriggers.java"

# interfaces
.implements Lcom/android/keyguard/doze/DozeMachine$Part;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/doze/DozeTriggers$1;,
        Lcom/android/keyguard/doze/DozeTriggers$ProximityCheck;,
        Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I = null

.field private static final DEBUG:Z

.field private static final PULSE_ACTION:Ljava/lang/String; = "com.android.systemui.doze.pulse"

.field private static final TAG:Ljava/lang/String; = "DozeTriggers"


# instance fields
.field private final mAllowPulseTriggers:Z

.field private final mBroadcastReceiver:Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;

.field private final mConfig:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

.field private final mContext:Landroid/content/Context;

.field private final mDozeHost:Lcom/android/keyguard/doze/DozeHost;

.field private final mDozeParameters:Lcom/android/keyguard/statusbar/phone/DozeParameters;

.field private final mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

.field private final mHandler:Landroid/os/Handler;

.field private mHostCallback:Lcom/android/keyguard/doze/DozeHost$Callback;

.field private final mMachine:Lcom/android/keyguard/doze/DozeMachine;

.field private mNotificationPulseTime:J

.field private mPulsePending:Z

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field private final mUiModeManager:Landroid/app/UiModeManager;

.field private final mWakeLock:Lcom/android/keyguard/util/wakelock/WakeLock;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/doze/DozeTriggers;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/doze/DozeTriggers;)Lcom/android/keyguard/doze/DozeSensors;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/doze/DozeTriggers;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/doze/DozeTriggers;)Lcom/android/keyguard/doze/DozeMachine;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/doze/DozeTriggers;)Landroid/hardware/SensorManager;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/doze/DozeTriggers;)Lcom/android/keyguard/util/wakelock/WakeLock;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mWakeLock:Lcom/android/keyguard/util/wakelock/WakeLock;

    return-object v0
.end method

.method private static synthetic -getcom-android-keyguard-doze-DozeMachine$StateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/keyguard/doze/DozeTriggers;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/doze/DozeTriggers;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/keyguard/doze/DozeMachine$State;->values()[Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSING:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_PULSE_DONE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_PULSING:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_REQUEST_PULSE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->FINISH:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->INITIALIZED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_8
    :try_start_9
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->UNINITIALIZED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    sput-object v0, Lcom/android/keyguard/doze/DozeTriggers;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1

    :catch_9
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/doze/DozeTriggers;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeTriggers;->onNotification()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/doze/DozeTriggers;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/doze/DozeTriggers;->requestPulse(IZ)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/keyguard/doze/DozeService;->DEBUG:Z

    sput-boolean v0, Lcom/android/keyguard/doze/DozeTriggers;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/doze/DozeMachine;Lcom/android/keyguard/doze/DozeHost;Landroid/app/AlarmManager;Lcom/android/internal/hardware/AmbientDisplayConfiguration;Lcom/android/keyguard/statusbar/phone/DozeParameters;Landroid/hardware/SensorManager;Landroid/os/Handler;Lcom/android/keyguard/util/wakelock/WakeLock;Z)V
    .locals 11

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;-><init>(Lcom/android/keyguard/doze/DozeTriggers;Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;)V

    iput-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mBroadcastReceiver:Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;

    new-instance v1, Lcom/android/keyguard/doze/DozeTriggers$1;

    invoke-direct {v1, p0}, Lcom/android/keyguard/doze/DozeTriggers$1;-><init>(Lcom/android/keyguard/doze/DozeTriggers;)V

    iput-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mHostCallback:Lcom/android/keyguard/doze/DozeHost$Callback;

    iput-object p1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    iput-object p3, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mConfig:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeParameters:Lcom/android/keyguard/statusbar/phone/DozeParameters;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mSensorManager:Landroid/hardware/SensorManager;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mWakeLock:Lcom/android/keyguard/util/wakelock/WakeLock;

    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mAllowPulseTriggers:Z

    new-instance v1, Lcom/android/keyguard/doze/DozeSensors;

    iget-object v4, p0, Lcom/android/keyguard/doze/DozeTriggers;->mSensorManager:Landroid/hardware/SensorManager;

    new-instance v8, Lcom/android/keyguard/doze/-$Lambda$tgoB2SYwZtZnNgbIYMhsDR9WMd0;

    invoke-direct {v8, p0}, Lcom/android/keyguard/doze/-$Lambda$tgoB2SYwZtZnNgbIYMhsDR9WMd0;-><init>(Ljava/lang/Object;)V

    new-instance v9, Lcom/android/keyguard/doze/-$Lambda$tgoB2SYwZtZnNgbIYMhsDR9WMd0$1;

    invoke-direct {v9, p0}, Lcom/android/keyguard/doze/-$Lambda$tgoB2SYwZtZnNgbIYMhsDR9WMd0$1;-><init>(Ljava/lang/Object;)V

    new-instance v10, Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;

    invoke-direct {v10, p1}, Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;-><init>(Landroid/content/Context;)V

    move-object v2, p1

    move-object v3, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p5

    move-object/from16 v7, p9

    invoke-direct/range {v1 .. v10}, Lcom/android/keyguard/doze/DozeSensors;-><init>(Landroid/content/Context;Landroid/app/AlarmManager;Landroid/hardware/SensorManager;Lcom/android/keyguard/statusbar/phone/DozeParameters;Lcom/android/internal/hardware/AmbientDisplayConfiguration;Lcom/android/keyguard/util/wakelock/WakeLock;Lcom/android/keyguard/doze/DozeSensors$Callback;Ljava/util/function/Consumer;Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;)V

    iput-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    const-class v2, Landroid/app/UiModeManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/UiModeManager;

    iput-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mUiModeManager:Landroid/app/UiModeManager;

    return-void
.end method

.method private canPulse()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine;->getState()Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v1

    sget-object v2, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE:Lcom/android/keyguard/doze/DozeMachine$State;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine;->getState()Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v1

    sget-object v2, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkTriggersAtInit()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mUiModeManager:Landroid/app/UiModeManager;

    invoke-virtual {v0}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v0}, Lcom/android/keyguard/doze/DozeHost;->isPowerSaveActive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v0}, Lcom/android/keyguard/doze/DozeHost;->isBlockingDoze()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v0}, Lcom/android/keyguard/doze/DozeHost;->isProvisioned()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->FINISH:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/doze/DozeMachine;->requestState(Lcom/android/keyguard/doze/DozeMachine$State;)V

    :cond_1
    return-void
.end method

.method private continuePulseRequest(I)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mPulsePending:Z

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v0}, Lcom/android/keyguard/doze/DozeHost;->isPulsingBlocked()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeTriggers;->canPulse()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mPulsePending:Z

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    invoke-virtual {v2}, Lcom/android/keyguard/doze/DozeMachine;->getState()Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v3}, Lcom/android/keyguard/doze/DozeHost;->isPulsingBlocked()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/keyguard/doze/DozeLog;->tracePulseDropped(Landroid/content/Context;ZLcom/android/keyguard/doze/DozeMachine$State;Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/doze/DozeMachine;->requestPulse(I)V

    return-void
.end method

.method private onNotification()V
    .locals 2

    sget-boolean v0, Lcom/android/keyguard/doze/DozeMachine;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "DozeTriggers"

    const-string/jumbo v1, "requestNotificationPulse"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mNotificationPulseTime:J

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mConfig:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/android/internal/hardware/AmbientDisplayConfiguration;->pulseOnNotificationEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/keyguard/doze/DozeTriggers;->requestPulse(IZ)V

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeLog;->traceNotificationPulse(Landroid/content/Context;)V

    return-void
.end method

.method private onProximityFar(Z)V
    .locals 9

    xor-int/lit8 v2, p1, 0x1

    iget-object v6, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    invoke-virtual {v6}, Lcom/android/keyguard/doze/DozeMachine;->getState()Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v5

    sget-object v6, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSED:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne v5, v6, :cond_5

    const/4 v3, 0x1

    :goto_0
    sget-object v6, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSING:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne v5, v6, :cond_6

    const/4 v4, 0x1

    :goto_1
    sget-object v6, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne v5, v6, :cond_7

    const/4 v0, 0x1

    :goto_2
    sget-object v6, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_PULSING:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne v5, v6, :cond_1

    move v1, v2

    sget-boolean v6, Lcom/android/keyguard/doze/DozeTriggers;->DEBUG:Z

    if-eqz v6, :cond_0

    const-string/jumbo v6, "DozeTriggers"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Prox changed, ignore touch = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v6, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v6, v2}, Lcom/android/keyguard/doze/DozeHost;->onIgnoreTouchWhilePulsing(Z)V

    :cond_1
    if-eqz p1, :cond_8

    if-nez v3, :cond_2

    if-eqz v4, :cond_8

    :cond_2
    sget-boolean v6, Lcom/android/keyguard/doze/DozeTriggers;->DEBUG:Z

    if-eqz v6, :cond_3

    const-string/jumbo v6, "DozeTriggers"

    const-string/jumbo v7, "Prox FAR, unpausing AOD"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v6, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    sget-object v7, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v6, v7}, Lcom/android/keyguard/doze/DozeMachine;->requestState(Lcom/android/keyguard/doze/DozeMachine$State;)V

    :cond_4
    :goto_3
    return-void

    :cond_5
    const/4 v3, 0x0

    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    sget-boolean v6, Lcom/android/keyguard/doze/DozeTriggers;->DEBUG:Z

    if-eqz v6, :cond_9

    const-string/jumbo v6, "DozeTriggers"

    const-string/jumbo v7, "Prox NEAR, pausing AOD"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-object v6, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    sget-object v7, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSING:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v6, v7}, Lcom/android/keyguard/doze/DozeMachine;->requestState(Lcom/android/keyguard/doze/DozeMachine$State;)V

    goto :goto_3
.end method

.method private onSensor(IZFF)V
    .locals 10

    const/4 v6, 0x4

    if-ne p1, v6, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v6, 0x3

    if-ne p1, v6, :cond_1

    const/4 v2, 0x1

    :goto_1
    const/4 v6, 0x5

    if-ne p1, v6, :cond_2

    const/4 v1, 0x1

    :goto_2
    iget-object v6, p0, Lcom/android/keyguard/doze/DozeTriggers;->mConfig:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    const/4 v7, -0x2

    invoke-virtual {v6, v7}, Lcom/android/internal/hardware/AmbientDisplayConfiguration;->alwaysOnEnabled(I)Z

    move-result v6

    if-eqz v6, :cond_3

    xor-int/lit8 v6, v1, 0x1

    if-eqz v6, :cond_3

    new-instance v6, Lcom/android/keyguard/doze/-$Lambda$tgoB2SYwZtZnNgbIYMhsDR9WMd0$3;

    invoke-direct {v6, v0, p3, p4, p0}, Lcom/android/keyguard/doze/-$Lambda$tgoB2SYwZtZnNgbIYMhsDR9WMd0$3;-><init>(ZFFLjava/lang/Object;)V

    invoke-direct {p0, v6, p2, p1}, Lcom/android/keyguard/doze/DozeTriggers;->proximityCheckThenCall(Ljava/util/function/IntConsumer;ZI)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/doze/DozeTriggers;->requestPulse(IZ)V

    if-eqz v2, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/android/keyguard/doze/DozeTriggers;->mNotificationPulseTime:J

    sub-long v4, v6, v8

    iget-object v6, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeParameters:Lcom/android/keyguard/statusbar/phone/DozeParameters;

    invoke-virtual {v6}, Lcom/android/keyguard/statusbar/phone/DozeParameters;->getPickupVibrationThreshold()I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v4, v6

    if-gez v6, :cond_5

    const/4 v3, 0x1

    :goto_3
    iget-object v6, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/android/keyguard/doze/DozeLog;->tracePickupPulse(Landroid/content/Context;Z)V

    :cond_4
    return-void

    :cond_5
    const/4 v3, 0x0

    goto :goto_3
.end method

.method private onWhisper()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/keyguard/doze/DozeTriggers;->requestPulse(IZ)V

    return-void
.end method

.method private proximityCheckThenCall(Ljava/util/function/IntConsumer;ZI)V
    .locals 8

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeSensors;->isProximityCurrentlyFar()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz p2, :cond_0

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Ljava/util/function/IntConsumer;->accept(I)V

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    :goto_1
    invoke-interface {p1, v1}, Ljava/util/function/IntConsumer;->accept(I)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    new-instance v1, Lcom/android/keyguard/doze/DozeTriggers$2;

    move-object v2, p0

    move-object v3, p0

    move v6, p3

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/android/keyguard/doze/DozeTriggers$2;-><init>(Lcom/android/keyguard/doze/DozeTriggers;Lcom/android/keyguard/doze/DozeTriggers;JILjava/util/function/IntConsumer;)V

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeTriggers$2;->check()V

    goto :goto_0
.end method

.method private requestPulse(IZ)V
    .locals 4

    const/4 v0, 0x1

    invoke-static {}, Lcom/android/keyguard/util/Assert;->isMainThread()V

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v1}, Lcom/android/keyguard/doze/DozeHost;->extendPulse()V

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mPulsePending:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mAllowPulseTriggers:Z

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeTriggers;->canPulse()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mAllowPulseTriggers:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mPulsePending:Z

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    invoke-virtual {v2}, Lcom/android/keyguard/doze/DozeMachine;->getState()Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v3}, Lcom/android/keyguard/doze/DozeHost;->isPulsingBlocked()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/keyguard/doze/DozeLog;->tracePulseDropped(Landroid/content/Context;ZLcom/android/keyguard/doze/DozeMachine$State;Z)V

    :cond_1
    return-void

    :cond_2
    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mPulsePending:Z

    new-instance v1, Lcom/android/keyguard/doze/-$Lambda$tgoB2SYwZtZnNgbIYMhsDR9WMd0$2;

    invoke-direct {v1, p1, p0}, Lcom/android/keyguard/doze/-$Lambda$tgoB2SYwZtZnNgbIYMhsDR9WMd0$2;-><init>(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeParameters:Lcom/android/keyguard/statusbar/phone/DozeParameters;

    invoke-virtual {v2}, Lcom/android/keyguard/statusbar/phone/DozeParameters;->getProxCheckBeforePulse()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_0
    invoke-direct {p0, v1, p2, p1}, Lcom/android/keyguard/doze/DozeTriggers;->proximityCheckThenCall(Ljava/util/function/IntConsumer;ZI)V

    return-void

    :cond_3
    move p2, v0

    goto :goto_0
.end method


# virtual methods
.method synthetic -com_android_keyguard_doze_DozeTriggers-mthref-0(IZFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/keyguard/doze/DozeTriggers;->onSensor(IZFF)V

    return-void
.end method

.method synthetic -com_android_keyguard_doze_DozeTriggers-mthref-1(Ljava/lang/Boolean;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/keyguard/doze/DozeTriggers;->onProximityFar(Z)V

    return-void
.end method

.method public disableProxListening(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/doze/DozeSensors;->disableProxListening(Z)V

    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 4

    const-string/jumbo v0, " notificationPulseTime="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mNotificationPulseTime:J

    invoke-static {v0, v2, v3}, Landroid/text/format/Formatter;->formatShortElapsedTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, " pulsePending="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mPulsePending:Z

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "DozeSensors:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/doze/DozeSensors;->dump(Ljava/io/PrintWriter;)V

    return-void
.end method

.method synthetic lambda$-com_android_keyguard_doze_DozeTriggers_5504(ZFFI)V
    .locals 1

    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v0, p2, p3}, Lcom/android/keyguard/doze/DozeHost;->onDoubleTap(FF)V

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    invoke-virtual {v0}, Lcom/android/keyguard/doze/DozeMachine;->wakeUp()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    invoke-interface {v0}, Lcom/android/keyguard/doze/DozeHost;->extendPulse()V

    goto :goto_0
.end method

.method synthetic lambda$-com_android_keyguard_doze_DozeTriggers_9601(II)V
    .locals 1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mPulsePending:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/keyguard/doze/DozeTriggers;->continuePulseRequest(I)V

    goto :goto_0
.end method

.method public transitionTo(Lcom/android/keyguard/doze/DozeMachine$State;Lcom/android/keyguard/doze/DozeMachine$State;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/keyguard/doze/DozeTriggers;->-getcom-android-keyguard-doze-DozeMachine$StateSwitchesValues()[I

    move-result-object v2

    invoke-virtual {p2}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mBroadcastReceiver:Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;->register(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mHostCallback:Lcom/android/keyguard/doze/DozeHost$Callback;

    invoke-interface {v0, v1}, Lcom/android/keyguard/doze/DozeHost;->addCallback(Lcom/android/keyguard/doze/DozeHost$Callback;)V

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeTriggers;->checkTriggersAtInit()V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    sget-object v3, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE:Lcom/android/keyguard/doze/DozeMachine$State;

    if-eq p2, v3, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v2, v0}, Lcom/android/keyguard/doze/DozeSensors;->setProxListening(Z)V

    sget-object v0, Lcom/android/keyguard/doze/DozeMachine$State;->INITIALIZED:Lcom/android/keyguard/doze/DozeMachine$State;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v0}, Lcom/android/keyguard/doze/DozeSensors;->reregisterAllSensors()V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/doze/DozeSensors;->setListening(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v2, v1}, Lcom/android/keyguard/doze/DozeSensors;->setProxListening(Z)V

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/doze/DozeSensors;->setListening(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v2, v0}, Lcom/android/keyguard/doze/DozeSensors;->setTouchscreenSensorsListening(Z)V

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/doze/DozeSensors;->setProxListening(Z)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mBroadcastReceiver:Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/doze/DozeTriggers$TriggerReceiver;->unregister(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeTriggers;->mHostCallback:Lcom/android/keyguard/doze/DozeHost$Callback;

    invoke-interface {v1, v2}, Lcom/android/keyguard/doze/DozeHost;->removeCallback(Lcom/android/keyguard/doze/DozeHost$Callback;)V

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/doze/DozeSensors;->setListening(Z)V

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeTriggers;->mDozeSensors:Lcom/android/keyguard/doze/DozeSensors;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/doze/DozeSensors;->setProxListening(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method
