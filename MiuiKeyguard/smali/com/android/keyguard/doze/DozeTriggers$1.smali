.class Lcom/android/keyguard/doze/DozeTriggers$1;
.super Ljava/lang/Object;
.source "DozeTriggers.java"

# interfaces
.implements Lcom/android/keyguard/doze/DozeHost$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/doze/DozeTriggers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/doze/DozeTriggers;


# direct methods
.method constructor <init>(Lcom/android/keyguard/doze/DozeTriggers;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/doze/DozeTriggers$1;->this$0:Lcom/android/keyguard/doze/DozeTriggers;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotificationHeadsUp()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers$1;->this$0:Lcom/android/keyguard/doze/DozeTriggers;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeTriggers;->-wrap0(Lcom/android/keyguard/doze/DozeTriggers;)V

    return-void
.end method

.method public onPowerSaveChanged(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeTriggers$1;->this$0:Lcom/android/keyguard/doze/DozeTriggers;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeTriggers;->-get3(Lcom/android/keyguard/doze/DozeTriggers;)Lcom/android/keyguard/doze/DozeMachine;

    move-result-object v0

    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->FINISH:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/doze/DozeMachine;->requestState(Lcom/android/keyguard/doze/DozeMachine$State;)V

    :cond_0
    return-void
.end method
