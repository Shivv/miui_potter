.class Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;
.super Landroid/hardware/TriggerEventListener;
.source "DozeSensors.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/doze/DozeSensors;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TriggerSensor"
.end annotation


# instance fields
.field final mConfigured:Z

.field private mDisabled:Z

.field final mPulseReason:I

.field private mRegistered:Z

.field final mReportsTouchCoordinates:Z

.field private mRequested:Z

.field final mRequiresTouchscreen:Z

.field final mSensor:Landroid/hardware/Sensor;

.field final mSetting:Ljava/lang/String;

.field final mSettingDefault:Z

.field final synthetic this$0:Lcom/android/keyguard/doze/DozeSensors;


# direct methods
.method public constructor <init>(Lcom/android/keyguard/doze/DozeSensors;Landroid/hardware/Sensor;Ljava/lang/String;ZIZZ)V
    .locals 9

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;-><init>(Lcom/android/keyguard/doze/DozeSensors;Landroid/hardware/Sensor;Ljava/lang/String;ZZIZZ)V

    return-void
.end method

.method public constructor <init>(Lcom/android/keyguard/doze/DozeSensors;Landroid/hardware/Sensor;Ljava/lang/String;ZZIZZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-direct {p0}, Landroid/hardware/TriggerEventListener;-><init>()V

    iput-object p2, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSensor:Landroid/hardware/Sensor;

    iput-object p3, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSetting:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSettingDefault:Z

    iput-boolean p5, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mConfigured:Z

    iput p6, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mPulseReason:I

    iput-boolean p7, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mReportsTouchCoordinates:Z

    iput-boolean p8, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRequiresTouchscreen:Z

    return-void
.end method

.method private enabledBySetting()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSetting:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get6(Lcom/android/keyguard/doze/DozeSensors;)Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSetting:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSettingDefault:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const/4 v5, -0x2

    invoke-static {v3, v4, v0, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private triggerEventToString(Landroid/hardware/TriggerEvent;)Ljava/lang/String;
    .locals 7

    const/16 v6, 0x2c

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return-object v2

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "TriggerEvent["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Landroid/hardware/TriggerEvent;->timestamp:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/TriggerEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/TriggerEvent;->values:[F

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p1, Landroid/hardware/TriggerEvent;->values:[F

    array-length v2, v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/TriggerEvent;->values:[F

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method synthetic lambda$-com_android_keyguard_doze_DozeSensors$TriggerSensor_12745(Landroid/hardware/TriggerEvent;)V
    .locals 8

    const/4 v7, 0x0

    invoke-static {}, Lcom/android/keyguard/doze/DozeSensors;->-get0()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "DozeSensors"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onTrigger: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, p1}, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->triggerEventToString(Landroid/hardware/TriggerEvent;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSensor:Landroid/hardware/Sensor;

    invoke-virtual {v4}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    const/16 v5, 0x19

    if-ne v4, v5, :cond_1

    iget-object v4, p1, Landroid/hardware/TriggerEvent;->values:[F

    aget v4, v4, v7

    float-to-int v3, v4

    iget-object v4, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v4}, Lcom/android/keyguard/doze/DozeSensors;->-get2(Lcom/android/keyguard/doze/DozeSensors;)Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0x19b

    invoke-static {v4, v5, v3}, Lcom/android/internal/logging/MetricsLogger;->action(Landroid/content/Context;II)V

    iget-object v4, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v4}, Lcom/android/keyguard/doze/DozeSensors;->-get3(Lcom/android/keyguard/doze/DozeSensors;)Lcom/android/keyguard/statusbar/phone/DozeParameters;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/keyguard/statusbar/phone/DozeParameters;->getPickupSubtypePerformsProxCheck(I)Z

    move-result v2

    :cond_1
    iput-boolean v7, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRegistered:Z

    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, -0x40800000    # -1.0f

    iget-boolean v4, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mReportsTouchCoordinates:Z

    if-eqz v4, :cond_2

    iget-object v4, p1, Landroid/hardware/TriggerEvent;->values:[F

    array-length v4, v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_2

    iget-object v4, p1, Landroid/hardware/TriggerEvent;->values:[F

    aget v0, v4, v7

    iget-object v4, p1, Landroid/hardware/TriggerEvent;->values:[F

    const/4 v5, 0x1

    aget v1, v4, v5

    :cond_2
    iget-object v4, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v4}, Lcom/android/keyguard/doze/DozeSensors;->-get1(Lcom/android/keyguard/doze/DozeSensors;)Lcom/android/keyguard/doze/DozeSensors$Callback;

    move-result-object v4

    iget v5, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mPulseReason:I

    invoke-interface {v4, v5, v2, v0, v1}, Lcom/android/keyguard/doze/DozeSensors$Callback;->onSensorPulse(IZFF)V

    invoke-virtual {p0}, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->updateListener()V

    return-void
.end method

.method public onTrigger(Landroid/hardware/TriggerEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get2(Lcom/android/keyguard/doze/DozeSensors;)Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mPulseReason:I

    invoke-static {v0, v1}, Lcom/android/keyguard/doze/DozeLog;->traceSensor(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get4(Lcom/android/keyguard/doze/DozeSensors;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v1}, Lcom/android/keyguard/doze/DozeSensors;->-get10(Lcom/android/keyguard/doze/DozeSensors;)Lcom/android/keyguard/util/wakelock/WakeLock;

    move-result-object v1

    new-instance v2, Lcom/android/keyguard/doze/-$Lambda$unNZkgy4TqnF3sxxapMjL9TxWgA;

    invoke-direct {v2, p0, p1}, Lcom/android/keyguard/doze/-$Lambda$unNZkgy4TqnF3sxxapMjL9TxWgA;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Lcom/android/keyguard/util/wakelock/WakeLock;->wrap(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public registerSettingsObserver(Landroid/database/ContentObserver;)V
    .locals 5

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mConfigured:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSetting:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get6(Lcom/android/keyguard/doze/DozeSensors;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSetting:Ljava/lang/String;

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v2}, Lcom/android/keyguard/doze/DozeSensors;->-get9(Lcom/android/keyguard/doze/DozeSensors;)Landroid/database/ContentObserver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    :cond_0
    return-void
.end method

.method public setDisabled(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mDisabled:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mDisabled:Z

    invoke-virtual {p0}, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->updateListener()V

    return-void
.end method

.method public setListening(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRequested:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRequested:Z

    invoke-virtual {p0}, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->updateListener()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{mRegistered="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRegistered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mRequested="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRequested:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mDisabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mDisabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mConfigured="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mConfigured:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mSensor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateListener()V
    .locals 4

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mConfigured:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSensor:Landroid/hardware/Sensor;

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRequested:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mDisabled:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->enabledBySetting()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRegistered:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v1}, Lcom/android/keyguard/doze/DozeSensors;->-get7(Lcom/android/keyguard/doze/DozeSensors;)Landroid/hardware/SensorManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v2}, Landroid/hardware/SensorManager;->requestTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRegistered:Z

    invoke-static {}, Lcom/android/keyguard/doze/DozeSensors;->-get0()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "DozeSensors"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "requestTriggerSensor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRegistered:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v1}, Lcom/android/keyguard/doze/DozeSensors;->-get7(Lcom/android/keyguard/doze/DozeSensors;)Landroid/hardware/SensorManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v2}, Landroid/hardware/SensorManager;->cancelTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    move-result v0

    invoke-static {}, Lcom/android/keyguard/doze/DozeSensors;->-get0()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "DozeSensors"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "cancelTriggerSensor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/keyguard/doze/DozeSensors$TriggerSensor;->mRegistered:Z

    goto :goto_0
.end method
