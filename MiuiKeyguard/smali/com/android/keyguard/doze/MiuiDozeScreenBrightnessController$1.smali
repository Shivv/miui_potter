.class Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;
.super Ljava/lang/Object;
.source "MiuiDozeScreenBrightnessController.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;


# direct methods
.method constructor <init>(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;->this$0:Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9

    const/high16 v8, 0x41200000    # 10.0f

    iget-object v3, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    const/4 v4, 0x5

    if-ne v4, v3, :cond_1

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    aget v2, v3, v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v3, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;->this$0:Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;

    invoke-static {v3}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->-get0(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/32 v6, 0xea60

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;->this$0:Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;

    invoke-static {v3, v2}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->-wrap0(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;F)V

    :cond_0
    const/high16 v3, 0x437f0000    # 255.0f

    div-float v4, v2, v8

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v8, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    float-to-int v0, v3

    :try_start_0
    iget-object v3, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;->this$0:Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;

    invoke-static {v3}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->-get1(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;)Lcom/android/keyguard/doze/DozeMachine$Service;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/android/keyguard/doze/DozeMachine$Service;->setDozeScreenBrightness(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;->this$0:Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;

    invoke-static {v3, v2}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->-set0(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;F)F

    :cond_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
