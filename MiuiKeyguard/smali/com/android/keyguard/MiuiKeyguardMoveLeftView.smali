.class public Lcom/android/keyguard/MiuiKeyguardMoveLeftView;
.super Landroid/widget/RelativeLayout;
.source "MiuiKeyguardMoveLeftView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;
    }
.end annotation


# static fields
.field private static final KEYGUARD_CONTROLLER_AUTHORITY:Landroid/net/Uri;

.field private static final KEYGUARD_MIPAY_AND_BUSCARD:Landroid/net/Uri;

.field private static final KEYGUARD_SMART_HOME:Landroid/net/Uri;

.field public static final LEFT_ACTION_MIPAY_BUSCARD_PKGNAME:Ljava/lang/String; = "com.miui.tsmclient"

.field public static final LEFT_ACTION_REMOTE_CONTROLLER_PKGNAME:Ljava/lang/String; = "com.duokan.phone.remotecontroller"

.field public static final LEFT_ACTION_SMART_HOME_PKGNAME:Ljava/lang/String; = "com.xiaomi.smarthome"

.field private static final MOVE_LEFT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MiuiKeyguardMoveLeftView"

.field private static sRegionSupportMiHomeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field listener:Landroid/view/View$OnClickListener;

.field private mAllFourLinearLayout:Landroid/widget/LinearLayout;

.field private mBankCardItemUpdate:Z

.field private mBankCardLinearLayout:Landroid/widget/LinearLayout;

.field private mBankCardNum:Landroid/widget/TextView;

.field private mBankCardNumInfo:Ljava/lang/String;

.field private mBusCardItemUpdate:Z

.field private mBusCardLinearLayout:Landroid/widget/LinearLayout;

.field private mBusCardNum:Landroid/widget/TextView;

.field private mBusCardNumInfo:Ljava/lang/String;

.field private mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

.field private mConsumerIrManager:Landroid/hardware/ConsumerIrManager;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mDirectionCheckPointX:F

.field private mDirectionCheckPointY:F

.field private mHasIrEmitter:Z

.field private mInitialMoveDirection:I

.field private mItemNums:I

.field private mLockScreenMagazineLinearLayout:Landroid/widget/LinearLayout;

.field private mMiuiDefaultLockScreen:Lcom/android/keyguard/MiuiDefaultLockScreen;

.field private mNfcEnable:Z

.field private mRemoteCenterLinearLayout:Landroid/widget/LinearLayout;

.field private mRemoteControllerItemUpdate:Z

.field private mRemoteControllerNum:Landroid/widget/TextView;

.field private mRemoteControllerNumInfo:Ljava/lang/String;

.field private mSmartHomeItemUpdate:Z

.field private mSmartHomeLinearLayout:Landroid/widget/LinearLayout;

.field private mSmartHomeNum:Landroid/widget/TextView;

.field private mSmartHomeNumnInfo:Ljava/lang/String;

.field private mSupportTSMClient:Z

.field private mTorchLightImageView:Landroid/widget/ImageView;

.field private mTwoOrOneItemLeftMargin:I

.field private mTwoOrOneItemRightMargin:I

.field private mTwoOrOneItemTopMargin:I

.field private mViewConfiguration:Landroid/view/ViewConfiguration;


# direct methods
.method static synthetic -get0()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_CONTROLLER_AUTHORITY:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic -get1()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_MIPAY_AND_BUSCARD:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic -get10(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardNum:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get11(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardNumInfo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get12(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Lcom/android/keyguard/MiuiKeyguardScreenCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    return-object v0
.end method

.method static synthetic -get13(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/hardware/ConsumerIrManager;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mConsumerIrManager:Landroid/hardware/ConsumerIrManager;

    return-object v0
.end method

.method static synthetic -get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get15(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method static synthetic -get16(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mItemNums:I

    return v0
.end method

.method static synthetic -get17(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mLockScreenMagazineLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -get18(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Lcom/android/keyguard/MiuiDefaultLockScreen;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mMiuiDefaultLockScreen:Lcom/android/keyguard/MiuiDefaultLockScreen;

    return-object v0
.end method

.method static synthetic -get19(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mNfcEnable:Z

    return v0
.end method

.method static synthetic -get2()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_SMART_HOME:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic -get20(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteCenterLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -get21(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteControllerItemUpdate:Z

    return v0
.end method

.method static synthetic -get22(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteControllerNum:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get23(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteControllerNumInfo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get24(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeItemUpdate:Z

    return v0
.end method

.method static synthetic -get25(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -get26(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeNum:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get27(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeNumnInfo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get28(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSupportTSMClient:Z

    return v0
.end method

.method static synthetic -get29(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mTwoOrOneItemLeftMargin:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mAllFourLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -get30(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mTwoOrOneItemRightMargin:I

    return v0
.end method

.method static synthetic -get31(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mTwoOrOneItemTopMargin:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardItemUpdate:Z

    return v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardNum:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardNumInfo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardItemUpdate:Z

    return v0
.end method

.method static synthetic -get9(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardNumInfo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardNumInfo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -set2(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Landroid/hardware/ConsumerIrManager;)Landroid/hardware/ConsumerIrManager;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mConsumerIrManager:Landroid/hardware/ConsumerIrManager;

    return-object p1
.end method

.method static synthetic -set3(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mHasIrEmitter:Z

    return p1
.end method

.method static synthetic -set4(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;I)I
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mItemNums:I

    return p1
.end method

.method static synthetic -set5(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mNfcEnable:Z

    return p1
.end method

.method static synthetic -set6(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteControllerNumInfo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -set7(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeNumnInfo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -set8(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSupportTSMClient:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->isRegionSupportMiHome()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->supportLockScreenMagazine()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setPreviewButtonClicked()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->startAppStoreToDownload(I)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->startToTSMClientActivity(Z)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->updateItemNumString(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "content://com.xiaomi.smarthome.ext_cp"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_SMART_HOME:Landroid/net/Uri;

    const-string/jumbo v0, "content://com.xiaomi.mitv.phone.remotecontroller.provider.LockScreenProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_CONTROLLER_AUTHORITY:Landroid/net/Uri;

    const-string/jumbo v0, "content://com.miui.tsmclient.provider.public"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_MIPAY_AND_BUSCARD:Landroid/net/Uri;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->sRegionSupportMiHomeList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mConsumerIrManager:Landroid/hardware/ConsumerIrManager;

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mInitialMoveDirection:I

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeItemUpdate:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteControllerItemUpdate:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardItemUpdate:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardItemUpdate:Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mItemNums:I

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;-><init>(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->listener:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mConsumerIrManager:Landroid/hardware/ConsumerIrManager;

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mInitialMoveDirection:I

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeItemUpdate:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteControllerItemUpdate:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardItemUpdate:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardItemUpdate:Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mItemNums:I

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;-><init>(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->listener:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    return-void
.end method

.method private calculateDirection(FFFF)I
    .locals 3

    sub-float v0, p3, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private initKeyguardLeftItemInfo(III)V
    .locals 5

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v4, 0x7f0d0054

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    const v4, 0x7f0d0055

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0d0056

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeNum:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_1
    iput-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteControllerNum:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_2
    iput-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardNum:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_3
    iput-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardNum:Landroid/widget/TextView;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0059
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private isRegionSupportMiHome()Z
    .locals 2

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->sRegionSupportMiHomeList:Ljava/util/List;

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private setPreviewButtonClicked()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "prfe_key_preview_button_clicked"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private startAppStoreToDownload(I)V
    .locals 4

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const v2, 0x7f0d0059

    if-ne p1, v2, :cond_1

    const-string/jumbo v2, "market://details?id=com.xiaomi.smarthome&back=true&ref=keyguard"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :cond_0
    :goto_0
    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v2, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->setPendingIntent(Landroid/content/Intent;)V

    :goto_1
    return-void

    :cond_1
    const v2, 0x7f0d005a

    if-ne p1, v2, :cond_2

    const-string/jumbo v2, "market://details?id=com.duokan.phone.remotecontroller&back=true&ref=keyguard"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    const v2, 0x7f0d005d

    if-ne p1, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "market://details?id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/keyguard/MiuiDefaultLockScreen;->DEFAULT_FASHIONGALLERY_PACKAGE_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "&back=true&ref=keyguard"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method private startToTSMClientActivity(Z)V
    .locals 4

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "com.miui.intent.action.DOUBLE_CLICK"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string/jumbo v2, "event_source"

    const-string/jumbo v3, "shortcut_of_all_card"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :goto_1
    return-void

    :cond_0
    const-string/jumbo v2, "event_source"

    const-string/jumbo v3, "shortcut_of_trans_card"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private supportLockScreenMagazine()Z
    .locals 2

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v1, :cond_0

    const-string/jumbo v1, "IN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private updateItemNumString(I)V
    .locals 1

    return-void
.end method


# virtual methods
.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public initLeftView()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;-><init>(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public isSupportRightMove()Z
    .locals 3

    const-string/jumbo v0, "MiuiKeyguardMoveLeftView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "miuikeyguard is suport right move:mihome="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->isRegionSupportMiHome()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";remotevontroller="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mHasIrEmitter:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";supportTSMClient="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSupportTSMClient:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";supportLockScreenMagazine="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->supportLockScreenMagazine()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->isRegionSupportMiHome()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSupportTSMClient:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->supportLockScreenMagazine()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContentObserver:Landroid/database/ContentObserver;

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    const v7, 0x7f0d005d

    const v6, 0x7f0d005c

    const v5, 0x7f0d005b

    const v4, 0x7f0d005a

    const v3, 0x7f0d0059

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    invoke-virtual {p0, v3}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteCenterLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v6}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v7}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mLockScreenMagazineLinearLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0057

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mTorchLightImageView:Landroid/widget/ImageView;

    const v1, 0x7f0d0058

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mAllFourLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mSmartHomeLinearLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mRemoteCenterLinearLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBankCardLinearLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mBusCardLinearLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mLockScreenMagazineLinearLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mTorchLightImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0091

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mTwoOrOneItemTopMargin:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0092

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mTwoOrOneItemLeftMargin:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0093

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mTwoOrOneItemRightMargin:I

    const v1, 0x7f020037

    const v2, 0x7f0b0060

    invoke-direct {p0, v3, v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->initKeyguardLeftItemInfo(III)V

    const v1, 0x7f020036

    const v2, 0x7f0b0061

    invoke-direct {p0, v4, v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->initKeyguardLeftItemInfo(III)V

    const v1, 0x7f020030

    const v2, 0x7f0b0062

    invoke-direct {p0, v5, v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->initKeyguardLeftItemInfo(III)V

    const v1, 0x7f020031

    const v2, 0x7f0b0063

    invoke-direct {p0, v6, v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->initKeyguardLeftItemInfo(III)V

    const v1, 0x7f020034

    const v2, 0x7f0b009c

    invoke-direct {p0, v7, v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->initKeyguardLeftItemInfo(III)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mViewConfiguration:Landroid/view/ViewConfiguration;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->sRegionSupportMiHomeList:Ljava/util/List;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mDirectionCheckPointY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mDirectionCheckPointX:F

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v3, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mViewConfiguration:Landroid/view/ViewConfiguration;

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mDirectionCheckPointX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mDirectionCheckPointY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_5

    :cond_0
    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mDirectionCheckPointX:F

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mDirectionCheckPointY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->calculateDirection(FFFF)I

    move-result v0

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mInitialMoveDirection:I

    if-nez v2, :cond_2

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mInitialMoveDirection:I

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iput v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mDirectionCheckPointX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iput v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mDirectionCheckPointY:F

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mInitialMoveDirection:I

    if-ne v2, v6, :cond_5

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v7}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return v7

    :cond_2
    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mInitialMoveDirection:I

    if-eq v2, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v6}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return v6

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v6, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    :cond_4
    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mInitialMoveDirection:I

    if-ne v2, v6, :cond_5

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v7}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return v7

    :cond_5
    return v6
.end method

.method public setDefaultScreenCallback(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mMiuiDefaultLockScreen:Lcom/android/keyguard/MiuiDefaultLockScreen;

    return-void
.end method

.method public setKeyguardScreenCallback(Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    return-void
.end method

.method public uploadLeftItemData()V
    .locals 5

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContentObserver:Landroid/database/ContentObserver;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$3;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$3;-><init>(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContentObserver:Landroid/database/ContentObserver;

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_SMART_HOME:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_CONTROLLER_AUTHORITY:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->KEYGUARD_MIPAY_AND_BUSCARD:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->updateItemNumString(I)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
