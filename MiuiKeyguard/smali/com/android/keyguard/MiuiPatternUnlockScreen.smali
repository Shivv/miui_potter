.class Lcom/android/keyguard/MiuiPatternUnlockScreen;
.super Lcom/android/keyguard/MiuiCommonUnlockScreen;
.source "MiuiPatternUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiPatternUnlockScreen$1;,
        Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;
    }
.end annotation


# static fields
.field private static final MIN_PATTERN_BEFORE_POKE_WAKELOCK:I = 0x2

.field private static final PATTERN_CLEAR_TIMEOUT_MS:I = 0x3e8

.field private static final UNLOCK_PATTERN_WAKE_INTERVAL_FIRST_DOTS_MS:I = 0x7d0

.field private static final UNLOCK_PATTERN_WAKE_INTERVAL_MS:I = 0x1b58


# instance fields
.field private mCancelPatternRunnable:Ljava/lang/Runnable;

.field private mEmergencyCallButton:Landroid/widget/TextView;

.field private mLastPokeTime:J

.field private mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

.field private mPatternUnlockScreen:Landroid/view/View;

.field protected final mVibrator:Landroid/os/Vibrator;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiPatternUnlockScreen;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCancelPatternRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiPatternUnlockScreen;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiPatternUnlockScreen;)Lcom/android/keyguard/MiuiKeyguardLockPatternView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiPatternUnlockScreen;IZIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->onPasswordChecked(IZIZ)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/android/keyguard/MiuiCommonUnlockScreen;-><init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-wide/16 v0, -0x1b58

    iput-wide v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLastPokeTime:J

    new-instance v0, Lcom/android/keyguard/MiuiPatternUnlockScreen$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen$1;-><init>(Lcom/android/keyguard/MiuiPatternUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCancelPatternRunnable:Ljava/lang/Runnable;

    iput-object p5, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const-string/jumbo v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->initView()V

    return-void
.end method

.method private handleWrongPassword(IZ)V
    .locals 10

    const-wide/16 v4, 0x0

    iget-object v3, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    sget-object v6, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-virtual {v3, v6}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    const-wide/16 v0, 0x0

    iget-object v3, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v3

    add-int/lit8 v2, v3, 0x1

    if-eqz p2, :cond_5

    const/4 v3, 0x5

    if-lt v2, v3, :cond_5

    iget-object v3, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v3, v2}, Lcom/android/internal/widget/LockPatternUtils;->setKeyguardLockoutAttemptDeadline(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleAttemptLockout(J)V

    :cond_0
    :goto_0
    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    if-eqz p1, :cond_3

    :cond_1
    const-string/jumbo v3, "miui_keyguard_fingerprint"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "lockout timeoutMs="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ";deadline="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    cmp-long v7, v0, v4

    if-lez v7, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v4, v0, v4

    :cond_2
    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz p2, :cond_4

    iget-object v3, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v3}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportFailedUnlockAttempt()V

    :cond_4
    iget-object v3, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v4, 0x96

    invoke-virtual {v3, v4, v5}, Landroid/os/Vibrator;->vibrate(J)V

    return-void

    :cond_5
    iget-object v3, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    iget-object v6, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCancelPatternRunnable:Ljava/lang/Runnable;

    const-wide/16 v8, 0x3e8

    invoke-virtual {v3, v6, v8, v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->postDelayed(Ljava/lang/Runnable;J)Z

    if-eqz p1, :cond_0

    int-to-long v6, p1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-virtual {p0, v6, v7}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleAttemptLockout(J)V

    int-to-long v6, p1

    invoke-virtual {p0, v6, v7}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->writePasswordOutTime(J)V

    goto :goto_0
.end method

.method private initView()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03001b

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPatternUnlockScreen:Landroid/view/View;

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPatternUnlockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->addUnlockView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPatternUnlockScreen:Landroid/view/View;

    const v1, 0x7f0d0081

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPatternUnlockScreen:Landroid/view/View;

    const v1, 0x7f0d0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mEmergencyCallButton:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mEmergencyCallButton:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->processEmergencyCall(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setSaveEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    new-instance v1, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;

    invoke-direct {v1, p0, v3}, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;-><init>(Lcom/android/keyguard/MiuiPatternUnlockScreen;Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isVisiblePatternEnabled()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setInStealthMode(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isTactileFeedbackEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setTactileFeedbackEnabled(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleAttemptLockoutIfNeed()V

    return-void
.end method

.method private onPasswordChecked(IZIZ)V
    .locals 3

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";matched="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";timeoutMs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->isNeedCloseSdcardFs(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleNeedCloseSdcardFs()V

    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->isFBEDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleFirstMatchSpacePassword()V

    return-void

    :cond_1
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    if-eq p1, v0, :cond_3

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->isPhoneCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t switch user to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when calling"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleSwitchUserWhenCalling()V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->isGreenKidActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t switch user to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when green kid active"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleSwitchUserWhenGreenkidActive()V

    return-void

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->switchUser(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    sget-object v1, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->keyguardDone(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_PW:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->recordUnlockWay(Ljava/lang/String;)V

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "unlock by pattern"

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_4
    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->handleWrongPassword(IZ)V

    goto :goto_0
.end method


# virtual methods
.method public applyAnimation()V
    .locals 1

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->applyAnimation()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->startAnimation()V

    return-void
.end method

.method public cleanUp()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLastPokeTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1af4

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLastPokeTime:J

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    :cond_0
    return v0
.end method

.method protected handleAttemptLockout(J)V
    .locals 5

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->clearPattern()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->showLockoutView(J)V

    return-void
.end method

.method protected hideLockoutView()V
    .locals 2

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->hideLockoutView()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->enableInput()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPatternUnlockScreen:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public needsInput()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyboardChange(Z)V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onPause()V

    return-void
.end method

.method public onResume(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->clearPattern()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->asyncHideLockViewOnResume()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->onResume()V

    :cond_0
    return-void
.end method

.method protected onScreenOrientationChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->initView()V

    return-void
.end method

.method protected setPasswordEntryInputEnabled(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->enableInput()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->disableInput()V

    goto :goto_0
.end method

.method protected showLockoutView(J)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showLockoutView(J)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternView:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->disableInput()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPatternUnlockScreen:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected updateEmergencyCallButtonVisibility(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mEmergencyCallButton:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
