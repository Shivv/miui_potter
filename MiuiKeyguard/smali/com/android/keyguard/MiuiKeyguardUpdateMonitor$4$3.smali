.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;
.super Ljava/lang/Thread;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->processFingerprintResultAnalyticsForA7(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

.field final synthetic val$result:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

    iput p2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;->val$result:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string/jumbo v4, "/sdcard/MIUI/debug_log/1.dat"

    const-string/jumbo v5, "r"

    invoke-direct {v2, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v3

    const-class v4, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "temperature: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

    iget-object v4, v4, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$1;

    iget v6, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;->val$result:I

    invoke-direct {v5, p0, v3, v6}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$1;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;II)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    move-object v1, v2

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-class v4, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_2
    :try_start_3
    const-class v4, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

    iget-object v4, v4, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$2;

    iget v6, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;->val$result:I

    invoke-direct {v5, p0, v6}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$2;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;I)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    const-class v4, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :goto_4
    throw v4

    :catch_3
    move-exception v0

    const-class v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :catchall_1
    move-exception v4

    move-object v1, v2

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
