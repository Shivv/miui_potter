.class Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;
.super Ljava/lang/Object;
.source "MiuiUnlockScreenVerticalClock.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;->this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updateNotificationList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;->this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;

    invoke-static {v1, p1}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->-set3(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;Ljava/util/List;)Ljava/util/List;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->-set1(Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;->this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;

    new-instance v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4$1;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4$1;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public onChange(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;->updateNotificationList(Ljava/util/List;)V

    return-void
.end method

.method public onRegistered(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;->updateNotificationList(Ljava/util/List;)V

    return-void
.end method
