.class public Lcom/android/keyguard/MiuiKeyguardNotificationManager;
.super Ljava/lang/Object;
.source "MiuiKeyguardNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;,
        Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;,
        Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/keyguard/MiuiKeyguardNotificationManager;


# instance fields
.field private final mContentObserver:Landroid/database/ContentObserver;

.field private final mContext:Landroid/content/Context;

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mTimestampOfLastItemArrived:J


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiKeyguardNotificationManager;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiKeyguardNotificationManager;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiKeyguardNotificationManager;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiKeyguardNotificationManager;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/keyguard/MiuiKeyguardNotificationManager;J)J
    .locals 1

    iput-wide p1, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mTimestampOfLastItemArrived:J

    return-wide p1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->sInstance:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mListeners:Ljava/util/List;

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;-><init>(Lcom/android/keyguard/MiuiKeyguardNotificationManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mContentObserver:Landroid/database/ContentObserver;

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lmiui/provider/KeyguardNotification;->URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;
    .locals 2

    const-class v1, Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->sInstance:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->sInstance:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    :cond_0
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->sInstance:Lcom/android/keyguard/MiuiKeyguardNotificationManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getTimestampOfLastItemArrived()J
    .locals 2

    iget-wide v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mTimestampOfLastItemArrived:J

    return-wide v0
.end method

.method public registerListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mList:Ljava/util/List;

    invoke-interface {p1, v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;->onRegistered(Ljava/util/List;)V

    return-void
.end method

.method public unregisterListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
