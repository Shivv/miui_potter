.class public final Lcom/android/keyguard/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accessibility_enter_lock_wallpaper:I = 0x7f0b0045

.field public static final app_name:I = 0x7f0b0002

.field public static final btn_camera:I = 0x7f0b0087

.field public static final change_to_ac_charging:I = 0x7f0b000b

.field public static final click_to_enter_text:I = 0x7f0b0033

.field public static final confirm_pin_msg:I = 0x7f0b002e

.field public static final custom_lock_wallpaper_enter:I = 0x7f0b0049

.field public static final default_lockscreen_unlock_hint_text:I = 0x7f0b0027

.field public static final delete:I = 0x7f0b0003

.field public static final digital_clock_dot:I = 0x7f0b009b

.field public static final do_disclosure_generic:I = 0x7f0b0097

.field public static final do_disclosure_with_name:I = 0x7f0b0098

.field public static final doze_brightness_sensor_type:I = 0x7f0b0001

.field public static final doze_pickup_subtype_performs_proximity_check:I = 0x7f0b0000

.field public static final emergency_call_string:I = 0x7f0b000f

.field public static final face_data_input_camera_fail:I = 0x7f0b006c

.field public static final face_data_input_camera_ok:I = 0x7f0b006d

.field public static final face_data_input_cancel:I = 0x7f0b0072

.field public static final face_data_input_cancel_msg:I = 0x7f0b0073

.field public static final face_data_input_msg:I = 0x7f0b006e

.field public static final face_data_input_ok:I = 0x7f0b0074

.field public static final face_data_input_ok_msg:I = 0x7f0b0070

.field public static final face_data_input_ok_msg_angle:I = 0x7f0b0071

.field public static final face_data_input_ok_title:I = 0x7f0b006f

.field public static final face_data_input_title:I = 0x7f0b0064

.field public static final face_data_introduction_msg:I = 0x7f0b0066

.field public static final face_data_introduction_next:I = 0x7f0b0067

.field public static final face_data_introduction_title:I = 0x7f0b0065

.field public static final face_data_manage_delete:I = 0x7f0b0079

.field public static final face_data_manage_delete_msg:I = 0x7f0b0076

.field public static final face_data_manage_delete_sure:I = 0x7f0b007a

.field public static final face_data_manage_delete_title:I = 0x7f0b0075

.field public static final face_data_manage_unlock_msg:I = 0x7f0b0078

.field public static final face_data_manage_unlock_title:I = 0x7f0b0077

.field public static final face_data_siggesstion_next:I = 0x7f0b0068

.field public static final face_data_siggesstion_next_time:I = 0x7f0b0069

.field public static final face_data_suggesstion_first:I = 0x7f0b006a

.field public static final face_data_suggesstion_second:I = 0x7f0b006b

.field public static final face_unlock_adjust_distance:I = 0x7f0b007e

.field public static final face_unlock_be_on_the_screen:I = 0x7f0b0081

.field public static final face_unlock_by_notification_screen_on_msg:I = 0x7f0b007c

.field public static final face_unlock_by_notification_screen_on_title:I = 0x7f0b007b

.field public static final face_unlock_check_failed:I = 0x7f0b0086

.field public static final face_unlock_close_screen:I = 0x7f0b007f

.field public static final face_unlock_down:I = 0x7f0b0094

.field public static final face_unlock_identify_error:I = 0x7f0b0082

.field public static final face_unlock_not_found:I = 0x7f0b007d

.field public static final face_unlock_offset_bottom:I = 0x7f0b0090

.field public static final face_unlock_offset_left:I = 0x7f0b008d

.field public static final face_unlock_offset_right:I = 0x7f0b008f

.field public static final face_unlock_offset_top:I = 0x7f0b008e

.field public static final face_unlock_open_eye:I = 0x7f0b0083

.field public static final face_unlock_passwork:I = 0x7f0b0089

.field public static final face_unlock_passwork_and_fingerprint:I = 0x7f0b0088

.field public static final face_unlock_quality:I = 0x7f0b0095

.field public static final face_unlock_reenter_content:I = 0x7f0b009a

.field public static final face_unlock_reenter_title:I = 0x7f0b0099

.field public static final face_unlock_reveal_eye:I = 0x7f0b0084

.field public static final face_unlock_reveal_mouth:I = 0x7f0b0085

.field public static final face_unlock_rise:I = 0x7f0b0093

.field public static final face_unlock_rotated_left:I = 0x7f0b0091

.field public static final face_unlock_rotated_right:I = 0x7f0b0092

.field public static final face_unlock_stay_away_screen:I = 0x7f0b0080

.field public static final fingerprint_add_text:I = 0x7f0b003b

.field public static final fingerprint_add_title:I = 0x7f0b003a

.field public static final fingerprint_again_identified_msg:I = 0x7f0b0039

.field public static final fingerprint_enter_second_psw_msg:I = 0x7f0b004b

.field public static final fingerprint_enter_second_psw_msg_oldman:I = 0x7f0b004c

.field public static final fingerprint_enter_second_psw_title:I = 0x7f0b004a

.field public static final fingerprint_not_identified_msg:I = 0x7f0b0037

.field public static final fingerprint_not_identified_msg_lock:I = 0x7f0b0038

.field public static final fingerprint_not_identified_title:I = 0x7f0b0036

.field public static final fingerprint_try_again_msg:I = 0x7f0b0035

.field public static final fingerprint_try_again_text:I = 0x7f0b0034

.field public static final forget_password_string:I = 0x7f0b0010

.field public static final gxzw_press_harder:I = 0x7f0b009f

.field public static final gxzw_try_more_again:I = 0x7f0b009d

.field public static final gxzw_use_password:I = 0x7f0b009e

.field public static final incorrect_pincode:I = 0x7f0b0015

.field public static final incorrect_pukcode:I = 0x7f0b0014

.field public static final input_password_after_boot_msg:I = 0x7f0b0040

.field public static final input_password_after_boot_msg_can_not_switch_when_calling:I = 0x7f0b0042

.field public static final input_password_after_boot_msg_can_not_switch_when_greenkid_active:I = 0x7f0b0043

.field public static final input_password_after_boot_msg_must_enter_owner_space:I = 0x7f0b0041

.field public static final input_password_handle_sdcard_fs:I = 0x7f0b0044

.field public static final input_password_hint_text:I = 0x7f0b003e

.field public static final input_pattern_hint_text:I = 0x7f0b003f

.field public static final input_pin_code:I = 0x7f0b0013

.field public static final input_puk_code:I = 0x7f0b0011

.field public static final invalid_confirm_pin_hint:I = 0x7f0b002f

.field public static final invalid_sim_pin_hint:I = 0x7f0b003c

.field public static final invalid_sim_puk_hint:I = 0x7f0b003d

.field public static final keyguard_charging_info_battery_used_time_text:I = 0x7f0b0054

.field public static final keyguard_charging_info_click_to_detail:I = 0x7f0b004e

.field public static final keyguard_charging_info_data_c:I = 0x7f0b005e

.field public static final keyguard_charging_info_data_ma:I = 0x7f0b005c

.field public static final keyguard_charging_info_data_percent:I = 0x7f0b005f

.field public static final keyguard_charging_info_data_v:I = 0x7f0b005d

.field public static final keyguard_charging_info_drained_power_percent_text:I = 0x7f0b0055

.field public static final keyguard_charging_info_last_charge_time:I = 0x7f0b0052

.field public static final keyguard_charging_info_last_charge_today_time:I = 0x7f0b0050

.field public static final keyguard_charging_info_last_charge_yesterday_time:I = 0x7f0b0051

.field public static final keyguard_charging_info_last_charged_time_text:I = 0x7f0b0053

.field public static final keyguard_charging_info_tip_text1:I = 0x7f0b0056

.field public static final keyguard_charging_info_tip_text2:I = 0x7f0b0057

.field public static final keyguard_charging_info_tip_text3:I = 0x7f0b0058

.field public static final keyguard_charging_info_tip_text4:I = 0x7f0b0059

.field public static final keyguard_charging_info_tip_text5:I = 0x7f0b005a

.field public static final keyguard_charging_info_tip_text6:I = 0x7f0b005b

.field public static final keyguard_left_bankcard:I = 0x7f0b0062

.field public static final keyguard_left_buscard:I = 0x7f0b0063

.field public static final keyguard_left_remotecentral:I = 0x7f0b0061

.field public static final keyguard_left_smarthome:I = 0x7f0b0060

.field public static final keyguard_left_view_lock_wallpaper:I = 0x7f0b009c

.field public static final keyguard_quick_charging_info_click_to_detail:I = 0x7f0b004f

.field public static final keyguard_torch_cover_text:I = 0x7f0b002d

.field public static final kg_password_pin_failed:I = 0x7f0b0016

.field public static final lock_screen_date:I = 0x7f0b0017

.field public static final lock_screen_date_12:I = 0x7f0b0018

.field public static final lock_screen_no_sim_card:I = 0x7f0b0096

.field public static final lock_wallpaper_enter:I = 0x7f0b0048

.field public static final lock_wallpaper_learn_more:I = 0x7f0b0047

.field public static final lockscreen_access_pattern_cell_added:I = 0x7f0b0025

.field public static final lockscreen_access_pattern_cleared:I = 0x7f0b0024

.field public static final lockscreen_access_pattern_detected:I = 0x7f0b0026

.field public static final lockscreen_access_pattern_start:I = 0x7f0b0023

.field public static final lockscreen_charging_msg:I = 0x7f0b0007

.field public static final lockscreen_failed_attempts_almost_at_wipe:I = 0x7f0b0029

.field public static final lockscreen_failed_attempts_almost_glogin:I = 0x7f0b0028

.field public static final lockscreen_failed_attempts_now_wiping:I = 0x7f0b002a

.field public static final miui_keyguard_ble_unlock_succeed_msg:I = 0x7f0b002b

.field public static final open_camera_hint_text:I = 0x7f0b0031

.field public static final open_remote_center_hint_text:I = 0x7f0b0032

.field public static final phone_locked_foget_password:I = 0x7f0b001a

.field public static final phone_locked_foget_password_mehod_back:I = 0x7f0b001d

.field public static final phone_locked_foget_password_method:I = 0x7f0b001b

.field public static final phone_locked_foget_password_method_content:I = 0x7f0b001c

.field public static final phone_locked_foget_password_method_next:I = 0x7f0b001e

.field public static final phone_locked_foget_password_ok:I = 0x7f0b0022

.field public static final phone_locked_forget_password_suggesstion_one_content:I = 0x7f0b0020

.field public static final phone_locked_forget_password_suggesstion_two_content:I = 0x7f0b0021

.field public static final phone_locked_forget_password_suggesstions_title:I = 0x7f0b001f

.field public static final phone_locked_string:I = 0x7f0b0019

.field public static final reset_device_detail_tip:I = 0x7f0b0009

.field public static final reset_device_tip:I = 0x7f0b0008

.field public static final symbol_colon:I = 0x7f0b004d

.field public static final unlock_camera_steady:I = 0x7f0b008c

.field public static final unlock_failed:I = 0x7f0b008a

.field public static final unlock_failed_face_not_found:I = 0x7f0b008b

.field public static final unlock_string:I = 0x7f0b0030

.field public static final unlocking_sim_card:I = 0x7f0b0012

.field public static final unlockscreen_low_battery:I = 0x7f0b0006

.field public static final unlockscreen_recharge_completed:I = 0x7f0b0005

.field public static final unlockscreen_recharging_message:I = 0x7f0b0004

.field public static final wallpaper_global_des_drop_down_notify_text:I = 0x7f0b0046

.field public static final wireless_charge_dialog_cancel:I = 0x7f0b000d

.field public static final wireless_charge_dialog_title:I = 0x7f0b000c

.field public static final wireless_charge_stop:I = 0x7f0b000a

.field public static final wireless_current_charge:I = 0x7f0b000e

.field public static final wrong_password:I = 0x7f0b002c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
