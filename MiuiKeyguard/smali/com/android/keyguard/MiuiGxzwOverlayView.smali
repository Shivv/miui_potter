.class public Lcom/android/keyguard/MiuiGxzwOverlayView;
.super Landroid/widget/FrameLayout;
.source "MiuiGxzwOverlayView.java"

# interfaces
.implements Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiGxzwOverlayView$1;,
        Lcom/android/keyguard/MiuiGxzwOverlayView$2;,
        Lcom/android/keyguard/MiuiGxzwOverlayView$3;,
        Lcom/android/keyguard/MiuiGxzwOverlayView$4;,
        Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;
    }
.end annotation


# static fields
.field public static BRIGHTNESS_TO_ALPHA:[F = null

.field private static final TAG:Ljava/lang/String; = "MiuiGxzwOverlayView"


# instance fields
.field private mAddDisplayEventReceiver:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

.field private mDozing:Z

.field private mLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mRemoveDisplayEventReceiver:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

.field private mShowing:Z

.field private mSystemBrightness:I

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiGxzwOverlayView;)Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mAddDisplayEventReceiver:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiGxzwOverlayView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiGxzwOverlayView;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mSystemBrightness:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiGxzwOverlayView;)Landroid/view/WindowManager;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiGxzwOverlayView;I)I
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mSystemBrightness:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiGxzwOverlayView;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->getSystemBrightness()I

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiGxzwOverlayView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->closeHBM()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiGxzwOverlayView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->openHBM()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiGxzwOverlayView;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiGxzwOverlayView;->updateAlpha(F)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x100

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/keyguard/MiuiGxzwOverlayView;->BRIGHTNESS_TO_ALPHA:[F

    return-void

    :array_0
    .array-data 4
        0x3f6d5cfb    # 0.9272f
        0x3f6d5cfb    # 0.9272f
        0x3f6d5cfb    # 0.9272f
        0x3f683127    # 0.907f
        0x3f628f5c    # 0.885f
        0x3f5e76c9    # 0.869f
        0x3f5ae148    # 0.855f
        0x3f5851ec    # 0.845f
        0x3f54bc6a    # 0.831f
        0x3f50e560    # 0.816f
        0x3f4e147b    # 0.805f
        0x3f4b4396    # 0.794f
        0x3f48f5c3    # 0.785f
        0x3f466666    # 0.775f
        0x3f445a1d    # 0.767f
        0x3f424dd3    # 0.759f
        0x3f400000    # 0.75f
        0x3f3e353f    # 0.743f
        0x3f3c6a7f    # 0.736f
        0x3f3ba5e3    # 0.733f
        0x3f39db23    # 0.726f
        0x3f3851ec    # 0.72f
        0x3f370a3d    # 0.715f
        0x3f34fdf4    # 0.707f
        0x3f333333    # 0.7f
        0x3f322d0e    # 0.696f
        0x3f3147ae    # 0.6925f
        0x3f3020c5    # 0.688f
        0x3f2e5604    # 0.681f
        0x3f2ccccd    # 0.675f
        0x3f2b020c    # 0.668f
        0x3f2b020c    # 0.668f
        0x3f2a3d71    # 0.665f
        0x3f2872b0    # 0.658f
        0x3f26e979    # 0.652f
        0x3f256042    # 0.646f
        0x3f256042    # 0.646f
        0x3f245a1d    # 0.642f
        0x3f23d70a    # 0.64f
        0x3f224dd3    # 0.634f
        0x3f2147ae    # 0.63f
        0x3f204189    # 0.626f
        0x3f1eb852    # 0.62f
        0x3f1e353f    # 0.618f
        0x3f1c28f6    # 0.61f
        0x3f1be76d    # 0.609f
        0x3f1ae148    # 0.605f
        0x3f19999a    # 0.6f
        0x3f1851ec    # 0.595f
        0x3f178d50    # 0.592f
        0x3f170a3d    # 0.59f
        0x3f160419    # 0.586f
        0x3f14fdf4    # 0.582f
        0x3f13f7cf    # 0.578f
        0x3f133333    # 0.575f
        0x3f11eb85    # 0.57f
        0x3f1126e9    # 0.567f
        0x3f0fdf3b    # 0.562f
        0x3f0ed917    # 0.558f
        0x3f0dd2f2    # 0.554f
        0x3f0ccccd    # 0.55f
        0x3f0c49ba    # 0.548f
        0x3f0b020c    # 0.543f
        0x3f0a3d71    # 0.54f
        0x3f09374c    # 0.536f
        0x3f083127    # 0.532f
        0x3f07ae14    # 0.53f
        0x3f06a7f0    # 0.526f
        0x3f05a1cb    # 0.522f
        0x3f04dd2f    # 0.519f
        0x3f03d70a    # 0.515f
        0x3f03d70a    # 0.515f
        0x3f03d70a    # 0.515f
        0x3f03d70a    # 0.515f
        0x3f03d70a    # 0.515f
        0x3f028f5c    # 0.51f
        0x3f018937    # 0.506f
        0x3f00c49c    # 0.503f
        0x3f008312    # 0.502f
        0x3eff7cee    # 0.499f
        0x3efdf3b6    # 0.496f
        0x3efbe76d    # 0.492f
        0x3efbe76d    # 0.492f
        0x3efa5e35    # 0.489f
        0x3ef851ec    # 0.485f
        0x3ef7ced9    # 0.484f
        0x3ef5c28f    # 0.48f
        0x3ef43958    # 0.477f
        0x3ef3b646    # 0.476f
        0x3ef1a9fc    # 0.472f
        0x3ef020c5    # 0.469f
        0x3eef9db2    # 0.468f
        0x3eef1aa0    # 0.467f
        0x3eec0831    # 0.461f
        0x3eeb851f    # 0.46f
        0x3ee978d5    # 0.456f
        0x3ee76c8b    # 0.452f
        0x3ee6e979    # 0.451f
        0x3ee6e979    # 0.451f
        0x3ee6e979    # 0.451f
        0x3ee6e979    # 0.451f
        0x3ee5e354    # 0.449f
        0x3ee5e354    # 0.449f
        0x3ee3d70a    # 0.445f
        0x3ee24dd3    # 0.442f
        0x3ee1cac1    # 0.441f
        0x3ee04189    # 0.438f
        0x3edfbe77    # 0.437f
        0x3edfbe77    # 0.437f
        0x3eddb22d    # 0.433f
        0x3edba5e3    # 0.429f
        0x3eda9fbe    # 0.427f
        0x3ed9999a    # 0.425f
        0x3ed89375    # 0.423f
        0x3ed78d50    # 0.421f
        0x3ed58106    # 0.417f
        0x3ed3f7cf    # 0.414f
        0x3ed374bc    # 0.413f
        0x3ed16873    # 0.409f
        0x3ed0e560    # 0.408f
        0x3ecfdf3b    # 0.406f
        0x3ecf5c29    # 0.405f
        0x3ecd4fdf    # 0.401f
        0x3ecc49ba    # 0.399f
        0x3ecc49ba    # 0.399f
        0x3ecc49ba    # 0.399f
        0x3ecbc6a8    # 0.398f
        0x3ec9ba5e    # 0.394f
        0x3ec83127    # 0.391f
        0x3ec7ae14    # 0.39f
        0x3ec624dd    # 0.387f
        0x3ec5a1cb    # 0.386f
        0x3ec41893    # 0.383f
        0x3ec39581    # 0.382f
        0x3ec18937    # 0.378f
        0x3ec10625    # 0.377f
        0x3ec00000    # 0.375f
        0x3ebe76c9    # 0.372f
        0x3ebd70a4    # 0.37f
        0x3ebbe76d    # 0.367f
        0x3ebbe76d    # 0.367f
        0x3ebb645a    # 0.366f
        0x3ebae148    # 0.365f
        0x3eb95810    # 0.362f
        0x3eb8d4fe    # 0.361f
        0x3eb74bc7    # 0.358f
        0x3eb53f7d    # 0.354f
        0x3eb4bc6a    # 0.353f
        0x3eb3b646    # 0.351f
        0x3eb33333    # 0.35f
        0x3ead0e56    # 0.338f
        0x3eac0831    # 0.336f
        0x3eab851f    # 0.335f
        0x3ea978d5    # 0.331f
        0x3ea6e979    # 0.326f
        0x3ea66666    # 0.325f
        0x3ea5e354    # 0.324f
        0x3ea4dd2f    # 0.322f
        0x3ea45a1d    # 0.321f
        0x3ea2d0e5    # 0.318f
        0x3ea147ae    # 0.315f
        0x3ea147ae    # 0.315f
        0x3e9fbe77    # 0.312f
        0x3e9f3b64    # 0.311f
        0x3e9cac08    # 0.306f
        0x3e9ba5e3    # 0.304f
        0x3e9b22d1    # 0.303f
        0x3e99999a    # 0.3f
        0x3e991687    # 0.299f
        0x3e970a3d    # 0.295f
        0x3e960419    # 0.293f
        0x3e960419    # 0.293f
        0x3e94fdf4    # 0.291f
        0x3e9374bc    # 0.288f
        0x3e926e98    # 0.286f
        0x3e916873    # 0.284f
        0x3e8f5c29    # 0.28f
        0x3e8bc6a8    # 0.273f
        0x3e8ac083    # 0.271f
        0x3e8a3d71    # 0.27f
        0x3e89ba5e    # 0.269f
        0x3e89374c    # 0.268f
        0x3e872b02    # 0.264f
        0x3e85a1cb    # 0.261f
        0x3e851eb8    # 0.26f
        0x3e839581    # 0.257f
        0x3e83126f    # 0.256f
        0x3e818937    # 0.253f
        0x3e810625    # 0.252f
        0x3e808312    # 0.251f
        0x3e7ef9db    # 0.249f
        0x3e7df3b6    # 0.248f
        0x3e7be76d    # 0.246f
        0x3e7ae148    # 0.245f
        0x3e77ced9    # 0.242f
        0x3e76c8b4    # 0.241f
        0x3e72b021    # 0.237f
        0x3e70a3d7    # 0.235f
        0x3e6c8b44    # 0.231f
        0x3e6b851f    # 0.23f
        0x3e6978d5    # 0.228f
        0x3e6872b0    # 0.227f
        0x3eac8b44    # 0.337f
        0x3e5f3b64    # 0.218f
        0x3e5d2f1b    # 0.216f
        0x3e5b22d1    # 0.214f
        0x3e5a1cac    # 0.213f
        0x3e54fdf4    # 0.208f
        0x3e52f1aa    # 0.206f
        0x3e52f1aa    # 0.206f
        0x3e51eb85    # 0.205f
        0x3e4dd2f2    # 0.201f
        0x3e4bc6a8    # 0.199f
        0x3e4ac083    # 0.198f
        0x3e47ae14    # 0.195f
        0x3e46a7f0    # 0.194f
        0x3e439581    # 0.191f
        0x3e428f5c    # 0.19f
        0x3e428f5c    # 0.19f
        0x3e3f7cee    # 0.187f
        0x3e3d70a4    # 0.185f
        0x3e3b645a    # 0.183f
        0x3e3a5e35    # 0.182f
        0x3e374bc7    # 0.179f
        0x3e36ae7d    # 0.1784f
        0x3e36ae7d    # 0.1784f
        0x3e3645a2    # 0.178f
        0x3e3020c5    # 0.172f
        0x3e3020c5    # 0.172f
        0x3e29fbe7    # 0.166f
        0x3e28f5c3    # 0.165f
        0x3e26e979    # 0.163f
        0x3e25e354    # 0.162f
        0x3e22d0e5    # 0.159f
        0x3e21cac1    # 0.158f
        0x3e21cac1    # 0.158f
        0x3e1eb852    # 0.155f
        0x3e1db22d    # 0.154f
        0x3e1ba5e3    # 0.152f
        0x3e19999a    # 0.15f
        0x3e178d50    # 0.148f
        0x3e16872b    # 0.147f
        0x3e147ae1    # 0.145f
        0x3e1374bc    # 0.144f
        0x3e126e98    # 0.143f
        0x3e116873    # 0.142f
        0x3e0d4fdf    # 0.138f
        0x3e0d4fdf    # 0.138f
        0x3e0b4396    # 0.136f
        0x3e0a3d71    # 0.135f
        0x3e09374c    # 0.134f
        0x3e051eb8    # 0.13f
        0x3e051eb8    # 0.13f
        0x3e000000    # 0.125f
        0x3df7ced9    # 0.121f
        0x3df1a9fc    # 0.118f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mDozing:Z

    iput v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mSystemBrightness:I

    new-instance v0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/android/keyguard/MiuiGxzwOverlayView$1;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiGxzwOverlayView$1;-><init>(Lcom/android/keyguard/MiuiGxzwOverlayView;)V

    new-instance v3, Lcom/android/keyguard/MiuiGxzwOverlayView$2;

    invoke-direct {v3, p0}, Lcom/android/keyguard/MiuiGxzwOverlayView$2;-><init>(Lcom/android/keyguard/MiuiGxzwOverlayView;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;-><init>(Landroid/os/Looper;Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mAddDisplayEventReceiver:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/android/keyguard/MiuiGxzwOverlayView$3;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiGxzwOverlayView$3;-><init>(Lcom/android/keyguard/MiuiGxzwOverlayView;)V

    new-instance v3, Lcom/android/keyguard/MiuiGxzwOverlayView$4;

    invoke-direct {v3, p0}, Lcom/android/keyguard/MiuiGxzwOverlayView$4;-><init>(Lcom/android/keyguard/MiuiGxzwOverlayView;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;-><init>(Landroid/os/Looper;Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mRemoveDisplayEventReceiver:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->initView()V

    return-void
.end method

.method private closeHBM()V
    .locals 2

    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForO;->setScreenEffect(II)V

    return-void
.end method

.method private getSystemBrightness()I
    .locals 6

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "screen_auto_brightness_adj"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v3, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x437f0000    # 255.0f

    mul-float/2addr v3, v4

    float-to-int v2, v3

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "screen_brightness"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private initView()V
    .locals 7

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030010

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const/16 v0, 0x1300

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->setSystemUiVisibility(I)V

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d9

    const v4, 0x5010518

    const/4 v5, -0x2

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string/jumbo v1, "hbm_overlay"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mWindowManager:Landroid/view/WindowManager;

    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/android/keyguard/MiuiGxzwOverlayView$5;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiGxzwOverlayView$5;-><init>(Lcom/android/keyguard/MiuiGxzwOverlayView;)V

    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private isAutoBrightness()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "screen_brightness_mode"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private openHBM()V
    .locals 2

    const/16 v0, 0xb

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForO;->setScreenEffect(II)V

    return-void
.end method

.method private openHBMAndUpdateAlpha()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiGxzwOverlayView$6;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiGxzwOverlayView$6;-><init>(Lcom/android/keyguard/MiuiGxzwOverlayView;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiGxzwOverlayView$6;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private updateAlpha(F)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mRemoveDisplayEventReceiver:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;->scheduleVsync()V

    return-void
.end method

.method public dozeTimeTick()V
    .locals 0

    return-void
.end method

.method public onScreenTurnedOff()V
    .locals 0

    return-void
.end method

.method public onScreenTurnedOn()V
    .locals 0

    return-void
.end method

.method public onStateChange(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mDozing:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->openHBM()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->closeHBM()V

    goto :goto_0
.end method

.method public show()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->openHBMAndUpdateAlpha()V

    return-void
.end method

.method public startDozing()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mDozing:Z

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->updateAlpha(F)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->closeHBM()V

    :cond_0
    return-void
.end method

.method public stopDozing()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mDozing:Z

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mShowing:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/MiuiGxzwOverlayView;->BRIGHTNESS_TO_ALPHA:[F

    iget v1, p0, Lcom/android/keyguard/MiuiGxzwOverlayView;->mSystemBrightness:I

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->updateAlpha(F)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->openHBM()V

    :cond_0
    return-void
.end method
