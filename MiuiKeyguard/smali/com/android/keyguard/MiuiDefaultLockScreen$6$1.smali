.class Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;
.super Landroid/os/AsyncTask;
.source "MiuiDefaultLockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$6;->musicStateChange(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$6;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    new-instance v1, Lcom/android/keyguard/KeyguardMusicController;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    iget-object v3, v3, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get21(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/ScreenElementRoot$OnExternCommandListener;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/keyguard/KeyguardMusicController;-><init>(Landroid/content/Context;Lmiui/maml/ScreenElementRoot$OnExternCommandListener;)V

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set7(Lcom/android/keyguard/MiuiDefaultLockScreen;Lcom/android/keyguard/KeyguardMusicController;)Lcom/android/keyguard/KeyguardMusicController;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/KeyguardMusicController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/KeyguardMusicController;->getMamlView()Lmiui/maml/component/MamlView;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set10(Lcom/android/keyguard/MiuiDefaultLockScreen;Lmiui/maml/component/MamlView;)Lmiui/maml/component/MamlView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap11(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    :cond_0
    return-void
.end method
