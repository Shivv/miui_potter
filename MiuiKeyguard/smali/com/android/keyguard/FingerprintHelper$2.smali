.class Lcom/android/keyguard/FingerprintHelper$2;
.super Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
.source "FingerprintHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/FingerprintHelper;->identify(Lcom/android/keyguard/FingerprintIdentifyCallback;Ljava/util/List;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/FingerprintHelper;

.field final synthetic val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;


# direct methods
.method constructor <init>(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/FingerprintHelper$2;->this$0:Lcom/android/keyguard/FingerprintHelper;

    iput-object p2, p0, Lcom/android/keyguard/FingerprintHelper$2;->val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthenticationAcquired(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;->onAuthenticationAcquired(I)V

    return-void
.end method

.method public onAuthenticationError(ILjava/lang/CharSequence;)V
    .locals 3

    invoke-super {p0, p1, p2}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;->onAuthenticationError(ILjava/lang/CharSequence;)V

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onAuthenticationError "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper$2;->val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-interface {v0, p1}, Lcom/android/keyguard/FingerprintIdentifyCallback;->onError(I)V

    return-void
.end method

.method public onAuthenticationFailed()V
    .locals 2

    invoke-super {p0}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;->onAuthenticationFailed()V

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper$2;->this$0:Lcom/android/keyguard/FingerprintHelper;

    invoke-static {v0}, Lcom/android/keyguard/FingerprintHelper;->-get0(Lcom/android/keyguard/FingerprintHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper$2;->this$0:Lcom/android/keyguard/FingerprintHelper;

    iget-object v1, p0, Lcom/android/keyguard/FingerprintHelper$2;->val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-static {v0, v1}, Lcom/android/keyguard/FingerprintHelper;->-wrap1(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;)V

    :cond_0
    return-void
.end method

.method public onAuthenticationHelp(ILjava/lang/CharSequence;)V
    .locals 3

    invoke-super {p0, p1, p2}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;->onAuthenticationHelp(ILjava/lang/CharSequence;)V

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onAuthenticationHelp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper$2;->this$0:Lcom/android/keyguard/FingerprintHelper;

    invoke-static {v0}, Lcom/android/keyguard/FingerprintHelper;->-get0(Lcom/android/keyguard/FingerprintHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "capricorn"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper$2;->this$0:Lcom/android/keyguard/FingerprintHelper;

    iget-object v1, p0, Lcom/android/keyguard/FingerprintHelper$2;->val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/keyguard/FingerprintHelper;->-wrap0(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onAuthenticationSucceeded(Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;->onAuthenticationSucceeded(Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper$2;->this$0:Lcom/android/keyguard/FingerprintHelper;

    invoke-static {v0}, Lcom/android/keyguard/FingerprintHelper;->-get0(Lcom/android/keyguard/FingerprintHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper$2;->this$0:Lcom/android/keyguard/FingerprintHelper;

    iget-object v1, p0, Lcom/android/keyguard/FingerprintHelper$2;->val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-static {v0, v1, p1}, Lcom/android/keyguard/FingerprintHelper;->-wrap2(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V

    :cond_0
    return-void
.end method
