.class Lcom/android/keyguard/MiuiKeyguardViewMediator$2;
.super Ljava/lang/Object;
.source "MiuiKeyguardViewMediator.java"

# interfaces
.implements Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$2;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUnlocked(B)V
    .locals 3

    const-string/jumbo v0, "KeyguardViewMediator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isOwnerUser()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$2;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$2;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v0

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->SUCCEED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$2;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap25(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$2;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v0

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->FAILED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$2;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v0

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->FAILED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V

    goto :goto_0
.end method
