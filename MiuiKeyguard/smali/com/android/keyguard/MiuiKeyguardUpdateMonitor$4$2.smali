.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;
.super Ljava/lang/Thread;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->processFingerprintResultAnalyticsForA4(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

.field final synthetic val$result:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

    iput p2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;->val$result:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string/jumbo v5, "/sdcard/MIUI/debug_log/1.dat"

    const-string/jumbo v6, "r"

    invoke-direct {v2, v5, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x8

    sub-long/2addr v6, v8

    invoke-virtual {v2, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v4

    const-class v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "value: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v5, 0x1140000

    if-ne v4, v5, :cond_2

    const-string/jumbo v3, "yinlan"

    :goto_0
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

    iget-object v5, v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v5

    new-instance v6, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2$1;

    iget v7, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;->val$result:I

    invoke-direct {v6, p0, v3, v7}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2$1;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;Ljava/lang/String;I)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_1
    move-object v1, v2

    :cond_1
    :goto_2
    return-void

    :cond_2
    :try_start_3
    const-string/jumbo v3, "default"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    const-class v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_3
    :try_start_4
    const-class v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

    iget-object v5, v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v5

    new-instance v6, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2$2;

    iget v7, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;->val$result:I

    invoke-direct {v6, p0, v7}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2$2;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;I)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    const-class v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catchall_0
    move-exception v5

    :goto_4
    if-eqz v1, :cond_3

    :try_start_6
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :cond_3
    :goto_5
    throw v5

    :catch_3
    move-exception v0

    const-class v6, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    :catchall_1
    move-exception v5

    move-object v1, v2

    goto :goto_4

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_3
.end method
