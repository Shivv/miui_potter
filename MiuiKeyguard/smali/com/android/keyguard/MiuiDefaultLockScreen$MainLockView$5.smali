.class Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const v3, 0x7f0b004e

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_CHARGING_SHOW:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get30(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5$1;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get30(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->startEnterAnim(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
