.class Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;
.super Ljava/lang/Object;
.source "MiuiNumericPasswordUnlockScreen.java"

# interfaces
.implements Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInput(I)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    const/16 v1, 0xa

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get1(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get1(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020069

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v1, v4, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get1(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f02006a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, p1, 0x30

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-wrap2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)V

    goto :goto_1
.end method
