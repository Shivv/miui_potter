.class Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$11;->updateNotificationList(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

.field final synthetic val$list:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$11;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iput-object p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->val$list:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v1

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->val$list:Ljava/util/List;

    invoke-static {v4, v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set11(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/util/List;)Ljava/util/List;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v5, v5, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->scrollToPosition(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v5, v5, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v6, v5}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyItemRangeChanged(II)V

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap6(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v5, v5, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v2, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v5, v5, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_3

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->scrollToPosition(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v4

    invoke-virtual {v4, v6, v3}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyItemRangeInserted(II)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v5, v5, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v6, v5}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyItemRangeChanged(II)V

    goto :goto_0

    :cond_3
    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget v5, v4, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->key:I

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget v4, v4, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->key:I

    if-eq v5, v4, :cond_4

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v5, v5, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-virtual {v4, v0, v5}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyItemRangeChanged(II)V

    goto/16 :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v4

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method
