.class public Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;
.super Lcom/android/keyguard/wallpaper/WallPaperDesView;
.source "WallPaperDesGlobalView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$1;,
        Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$EaseOutBackInterpolator;,
        Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;
    }
.end annotation


# static fields
.field private static final AUTHOR_PREFIX:Ljava/lang/String; = "\u00a9"

.field private static final EVENT_LOCK_LANDING_URL:I = 0xe

.field private static final EVENT_LOCK_PREVIEW:I = 0xf

.field private static final EXPECT_BROWSER_PACKAGE_NAME:[Ljava/lang/String;

.field private static final EXPECT_SYSTEM_RESOLVERACTIVITY_NAME:Ljava/lang/String; = "ResolverActivity"

.field private static IS_LOG_DEBUG:Z = false

.field private static final JUMP_MIN_Y_VELOCITY:F = -1000.0f

.field private static final KEY_AUTHOR:Ljava/lang/String; = "author"

.field private static final MAX_BACK_DISTANCE:I = -0x32

.field private static final MAX_CHAR_SEQUENCE_COUNT:I = 0x32

.field private static final MAX_DISTANCE:I = 0x384

.field private static final MAX_TITLE_TEXT_SIZE:I = 0x48

.field private static final METHOD_RECORD_EVENT:Ljava/lang/String; = "threadRecordEvent"

.field private static final METHOD_REQUEST_JSON:Ljava/lang/String; = "request_json"

.field private static final MIN_TITLE_TEXT_SIZE:I = 0x30

.field private static final MOVE_COEFFICIENT:F = 1.0f

.field private static final RELY_ON_PACKAGE_NAME:Ljava/lang/String; = "com.mfashiongallery.emag"

.field private static final STATE_REFRESH_WALLPAPER_INFO:I = 0x0

.field private static final STATE_UPDATE_WALLPAPER_COLOR:I = 0x1

.field private static final SYSTEM_PACKAGE_NAME:Ljava/lang/String; = "android"

.field private static final TAG:Ljava/lang/String; = "WallPaperDesGlobalView"

.field private static sTypefaces:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAuthor:Ljava/lang/String;

.field private mBottomNotifyArea:Landroid/view/View;

.field private mBottomNotifyAreaLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

.field private mContentTextView:Landroid/widget/TextView;

.field private mDropDownMarkView:Landroid/widget/ImageView;

.field private mDropDownNotifyTextView:Landroid/widget/TextView;

.field private mDropDownValueAnimator:Landroid/animation/ValueAnimator;

.field private mFromContainerView:Landroid/view/View;

.field private mFromLogoImageView:Landroid/widget/ImageView;

.field private mFromTextView:Landroid/widget/TextView;

.field private mHasLinkInfo:Z

.field private mInitBottomNotifyAreaTopMargin:I

.field private mInitHeight:I

.field private mInitY:F

.field private mInitialTouchY:F

.field private mLightMode:Z

.field private mMarginScale:F

.field private mNeedJumpLandingUrl:Z

.field mReMeasureAndSetTitleTask:Ljava/lang/Runnable;

.field private mShowLogoStateController:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

.field private mSpringBackAnimator:Landroid/animation/ValueAnimator;

.field private mTitleTextView:Landroid/widget/TextView;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownNotifyTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)F
    .locals 1

    iget v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mMarginScale:F

    return v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;F)F
    .locals 0

    iput p1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mMarginScale:F

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->animator(F)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;Landroid/widget/TextView;ILjava/lang/String;III)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->measureAndSetTitle(Landroid/widget/TextView;ILjava/lang/String;III)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "com.mi.globalbrowser"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.browser"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.chrome"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->EXPECT_BROWSER_PACKAGE_NAME:[Ljava/lang/String;

    const-string/jumbo v0, "WallPaperDesGlobalView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->IS_LOG_DEBUG:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->sTypefaces:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/keyguard/wallpaper/WallPaperDesView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;-><init>(I)V

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mShowLogoStateController:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

    new-instance v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$1;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)V

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mReMeasureAndSetTitleTask:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/wallpaper/WallPaperDesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;-><init>(I)V

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mShowLogoStateController:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

    new-instance v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$1;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)V

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mReMeasureAndSetTitleTask:Ljava/lang/Runnable;

    return-void
.end method

.method private animator(F)V
    .locals 7

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "WallPaperDesGlobalView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "animator = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/high16 v2, 0x40a00000    # 5.0f

    div-float v1, p1, v2

    add-float v0, v5, v1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->setScaleY(F)V

    iget v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitY:F

    iget v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitHeight:I

    int-to-float v3, v3

    mul-float/2addr v3, v1

    div-float/2addr v3, v6

    add-float/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->setY(F)V

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    add-float v3, v5, p1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    div-float v3, p1, v6

    add-float/2addr v3, v5

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownNotifyTextView:Landroid/widget/TextView;

    div-float v3, v5, v0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setScaleY(F)V

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyAreaLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitBottomNotifyAreaTopMargin:I

    iget-object v4, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyArea:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyArea:Landroid/view/View;

    iget-object v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyAreaLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private static cancelAnim(Landroid/animation/Animator;)V
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/animation/Animator;->cancel()V

    :cond_0
    return-void
.end method

.method private cancelAnimator()V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->cancelSpringBackAnimator()V

    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->cancelDropDownAnimator()V

    return-void
.end method

.method private cancelDropDownAnimator()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownValueAnimator:Landroid/animation/ValueAnimator;

    invoke-static {v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->cancelAnim(Landroid/animation/Animator;)V

    return-void
.end method

.method private cancelSpringBackAnimator()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mSpringBackAnimator:Landroid/animation/ValueAnimator;

    invoke-static {v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->cancelAnim(Landroid/animation/Animator;)V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "WallPaperDesGlobalView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static getInterpolation(F)F
    .locals 2

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr p0, v1

    mul-float v0, p0, p0

    mul-float/2addr v0, p0

    mul-float/2addr v0, p0

    sub-float v0, v1, v0

    return v0
.end method

.method public static getMiuiPreviewNormalTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;
    .locals 1

    const-string/jumbo v0, "fonts/Roboto-Light.ttf"

    invoke-static {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public static getMiuiPreviewTitleTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;
    .locals 1

    const-string/jumbo v0, "fonts/Roboto-Medium.ttf"

    invoke-static {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public static getOptimalPackageName(Landroid/content/Context;Landroid/content/Intent;[Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    const/4 v6, 0x0

    const/4 v9, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v7, 0x0

    invoke-virtual {v1, p1, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    if-nez v5, :cond_0

    return-object v9

    :cond_0
    array-length v7, p2

    :goto_0
    if-ge v6, v7, :cond_3

    aget-object v2, p2, v6

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    invoke-static {v3, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->isPackageResolveInfo(Landroid/content/pm/ResolveInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_1

    return-object v2

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    return-object v9
.end method

.method private getRemoteLogoDrawable(Landroid/content/Context;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;Z)Landroid/graphics/drawable/Drawable;
    .locals 7

    const/4 v6, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return-object v6

    :cond_1
    iget-object v4, p2, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->cp:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string/jumbo v5, "com.mfashiongallery.emag"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    if-nez v1, :cond_2

    return-object v6

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p3, :cond_3

    const-string/jumbo v4, ""

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->cp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "_logo"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "drawable"

    const-string/jumbo v5, "com.mfashiongallery.emag"

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    return-object v4

    :cond_3
    const-string/jumbo v4, "dark_"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return-object v6
.end method

.method private static declared-synchronized getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 3

    const-class v1, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->sTypefaces:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->sTypefaces:Ljava/util/HashMap;

    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    sget-object v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->sTypefaces:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private initLayout()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getMiuiPreviewTitleTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContentTextView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getMiuiPreviewNormalTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromTextView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getMiuiPreviewNormalTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownNotifyTextView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getMiuiPreviewNormalTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method private static isDebug()Z
    .locals 1

    sget-boolean v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->IS_LOG_DEBUG:Z

    return v0
.end method

.method private static isPackageResolveInfo(Landroid/content/pm/ResolveInfo;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_2
    return v0
.end method

.method private jumpLandingUrl()V
    .locals 7

    invoke-virtual {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v5, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    const/16 v6, 0xe

    invoke-direct {p0, v0, v5, v6}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->reportEvent(Landroid/content/Context;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;I)V

    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v6, "xiaomi.intent.action.SHOW_SECURE_KEYGUARD"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v5, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v5, v5, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->landingPageUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {v2, v5, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v5, 0x10000000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {v0, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->noDefaultBrowser(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->EXPECT_BROWSER_PACKAGE_NAME:[Ljava/lang/String;

    invoke-static {v0, v2, v5}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getOptimalPackageName(Landroid/content/Context;Landroid/content/Intent;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private measureAndSetTitle(Landroid/widget/TextView;ILjava/lang/String;III)V
    .locals 7

    const/4 v6, 0x0

    if-nez p3, :cond_0

    const-string/jumbo p3, ""

    :cond_0
    move-object v1, p3

    if-lez p4, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, p4, :cond_1

    invoke-virtual {v1, v6, p4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    iget-object v5, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    int-to-float v5, p2

    cmpg-float v5, v0, v5

    if-gtz v5, :cond_4

    int-to-float v5, p6

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_3

    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v5, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    iget-object v5, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v5, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    int-to-float v5, p2

    div-float v4, v5, v0

    mul-float/2addr v3, v4

    int-to-float v5, p6

    cmpl-float v5, v3, v5

    if-lez v5, :cond_2

    int-to-float v3, p6

    goto :goto_0

    :cond_4
    int-to-float v5, p5

    cmpg-float v5, v3, v5

    if-lez v5, :cond_2

    int-to-float v5, p2

    div-float v4, v5, v0

    mul-float/2addr v3, v4

    int-to-float v5, p5

    cmpg-float v5, v3, v5

    if-gez v5, :cond_2

    int-to-float v3, p5

    goto :goto_0
.end method

.method public static noDefaultBrowser(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 6

    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v4, 0x10000

    invoke-virtual {v1, p1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_1

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-string/jumbo v5, "ResolverActivity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v5, "android"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :cond_0
    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return v3
.end method

.method private reportEvent(Landroid/content/Context;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;I)V
    .locals 4

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p2, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->authority:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$3;

    invoke-direct {v1, p0, p2, p3, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$3;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;ILandroid/content/Context;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$3;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private toggleDropDown(Z)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    if-eqz p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownNotifyTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    xor-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownValueAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownValueAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->cancelDropDownAnimator()V

    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v2, v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    aput v1, v0, v2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownValueAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownValueAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownValueAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;

    invoke-direct {v1, p0, p1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownValueAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private updateFromViewState()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mShowLogoStateController:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

    invoke-static {v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;->-wrap0(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mShowLogoStateController:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

    invoke-static {v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;->-wrap2(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-boolean v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mLightMode:Z

    invoke-direct {p0, v1, v2, v3}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getRemoteLogoDrawable(Landroid/content/Context;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromLogoImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromLogoImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromContainerView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromLogoImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mAuthor:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromContainerView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromContainerView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public canDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->content:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleVerticalDragging(Landroid/view/MotionEvent;F)V
    .locals 9

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/high16 v7, -0x3db80000    # -50.0f

    const/4 v6, 0x0

    const/high16 v5, 0x44610000    # 900.0f

    iget v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitHeight:I

    if-nez v2, :cond_3

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitHeight:I

    invoke-virtual {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getY()F

    move-result v2

    iput v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitY:F

    iput p2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyArea:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyAreaLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyAreaLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v2, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iput v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitBottomNotifyAreaTopMargin:I

    invoke-static {}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "WallPaperDesGlobalView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "handleVerticalDragging mInitialTouchY = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", mInitY = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", mInitHeight = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    sub-float v1, v2, v3

    cmpg-float v2, v1, v5

    if-gez v2, :cond_5

    cmpl-float v2, v1, v7

    if-lez v2, :cond_5

    div-float v2, v1, v5

    invoke-static {v2}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getInterpolation(F)F

    move-result v0

    invoke-static {}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v2, "WallPaperDesGlobalView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "handleVerticalDragging interpolationValue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const v2, 0x3f19999a    # 0.6f

    cmpg-float v2, v0, v2

    if-gez v2, :cond_4

    iput-boolean v6, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mNeedJumpLandingUrl:Z

    invoke-direct {p0, v8}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->toggleDropDown(Z)V

    :goto_1
    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mMarginScale:F

    iget v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mMarginScale:F

    invoke-direct {p0, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->animator(F)V

    return-void

    :cond_3
    iget v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitY:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    goto/16 :goto_0

    :cond_4
    iput-boolean v8, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mNeedJumpLandingUrl:Z

    invoke-direct {p0, v6}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->toggleDropDown(Z)V

    goto :goto_1

    :cond_5
    cmpl-float v2, v1, v5

    if-lez v2, :cond_6

    sub-float v2, v1, v5

    iget v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    :goto_2
    return-void

    :cond_6
    sub-float v2, v1, v7

    iget v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    goto :goto_2
.end method

.method public handleVerticalDraggingUpEvent(Landroid/view/MotionEvent;F)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mNeedJumpLandingUrl:Z

    if-eqz v0, :cond_0

    const/high16 v0, -0x3b860000    # -1000.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->jumpLandingUrl()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->cancelDropDownAnimator()V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownNotifyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAlpha(F)V

    iput v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mInitialTouchY:F

    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->cancelSpringBackAnimator()V

    const/4 v0, 0x2

    new-array v0, v0, [F

    iget v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mMarginScale:F

    const/4 v2, 0x0

    aput v1, v0, v2

    const/4 v1, 0x1

    aput v3, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mSpringBackAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mSpringBackAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mSpringBackAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$EaseOutBackInterpolator;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$EaseOutBackInterpolator;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$EaseOutBackInterpolator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mSpringBackAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$2;

    invoke-direct {v1, p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$2;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mSpringBackAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method public isHasLinkInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mHasLinkInfo:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->cancelAnimator()V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mReMeasureAndSetTitleTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    const v0, 0x7f0d006b

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    const v0, 0x7f0d006c

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContentTextView:Landroid/widget/TextView;

    const v0, 0x7f0d006d

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromContainerView:Landroid/view/View;

    const v0, 0x7f0d006e

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromTextView:Landroid/widget/TextView;

    const v0, 0x7f0d006f

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromLogoImageView:Landroid/widget/ImageView;

    const v0, 0x7f0d0071

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    const v0, 0x7f0d0072

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownNotifyTextView:Landroid/widget/TextView;

    const v0, 0x7f0d0070

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyArea:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->initLayout()V

    return-void
.end method

.method public refreshWallpaperInfo()V
    .locals 6

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mShowLogoStateController:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;->-wrap1(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;I)V

    iget-object v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v1, v3, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->ex:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "author"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mAuthor:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->updateWallpaperInfo()V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    iput-object v5, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mAuthor:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object v5, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mAuthor:Ljava/lang/String;

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 3

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getVisibility()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    const/16 v2, 0xf

    invoke-direct {p0, v0, v1, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->reportEvent(Landroid/content/Context;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;I)V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->setVisibility(I)V

    return-void
.end method

.method public updateColorByWallpaper(Z)V
    .locals 5

    const v4, 0x7f080018

    const v3, 0x7f080017

    const v2, 0x7f08001a

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mShowLogoStateController:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;->-wrap1(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$StateController;I)V

    iput-boolean p1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mLightMode:Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContentTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownNotifyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    const v1, 0x7f020012

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->updateFromViewState()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContentTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownNotifyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    const v1, 0x7f020010

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public updateWallpaperInfo()V
    .locals 11

    const/16 v10, 0x8

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v3, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v8, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->content:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContentTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v7, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mAuthor:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->updateFromViewState()V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->landingPageUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v9, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mHasLinkInfo:Z

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyArea:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mReMeasureAndSetTitleTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->post(Ljava/lang/Runnable;)Z

    :goto_4
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mTitleTextView:Landroid/widget/TextView;

    const/16 v4, 0x32

    const/16 v5, 0x30

    const/16 v6, 0x48

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->measureAndSetTitle(Landroid/widget/TextView;ILjava/lang/String;III)V

    goto :goto_4

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContentTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mContentTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\u00a9"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mFromTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mHasLinkInfo:Z

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mDropDownMarkView:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->mBottomNotifyArea:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method
