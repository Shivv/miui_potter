.class public Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;
.super Lcom/android/keyguard/MiuiKeyguardBaseClock;
.source "MiuiUnlockScreenVerticalClock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$1;,
        Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$2;,
        Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$3;,
        Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;,
        Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$5;,
        Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$6;
    }
.end annotation


# static fields
.field private static final HALF_HORIZONTAL_TIME_HEIGHT:F = 106.0f

.field private static final TRANSLATION_LONG:I = 0x1a9

.field private static final TRANSLATION_SHORT:I = 0xd2

.field private static final TRANSLATION_Y_END:F = -134.0f

.field private static final TRANSLATION_Y_START:F

.field private static mBatteryInfo:Landroid/widget/TextView;

.field private static mCurrentDate:Landroid/widget/TextView;

.field private static mDateAndBatteryInfoLayout:Landroid/widget/FrameLayout;

.field private static mHasNotification:Z

.field private static mHorizontalDot:Landroid/widget/TextView;

.field private static mHorizontalHour:Landroid/widget/TextView;

.field private static mHorizontalMin:Landroid/widget/TextView;

.field private static mHorizontalTimeLayout:Landroid/widget/LinearLayout;

.field private static mOldHasNotification:Z

.field private static mOwnerInfo:Landroid/widget/TextView;

.field private static mShowHorizontalTime:Z

.field private static mSupportVerticalClock:Z

.field private static mTimeLayout:Landroid/widget/FrameLayout;

.field private static mVerticalHour:Landroid/widget/TextView;

.field private static mVerticalMin:Landroid/widget/TextView;


# instance fields
.field private m24HourFormat:Z

.field private mAttached:Z

.field private final mBatteryInfoAndDateTransition:Ljava/lang/Runnable;

.field private mBatteryInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

.field private mCalendar:Lmiui/date/Calendar;

.field private final mHandler:Landroid/os/Handler;

.field private mHorizontalToVerticalAnim:Landroid/animation/AnimatorSet;

.field private mInSmartCoverSmallWindowMode:Z

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mLightColorMode:Z

.field private final mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

.field private mNotificationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPowerManager:Landroid/os/PowerManager;

.field private mShowBatteryInfo:Z

.field private mSimCardInfo:Landroid/widget/TextView;

.field private final mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

.field private mThinFontTypeface:Landroid/graphics/Typeface;

.field private mTranslationDateExtra:F

.field private mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field mUpdateTimeRunnable:Ljava/lang/Runnable;

.field private mVerticalToHorizontalAnim:Landroid/animation/AnimatorSet;


# direct methods
.method static synthetic -get0()Landroid/widget/TextView;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get1()Landroid/widget/TextView;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCurrentDate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;Lmiui/date/Calendar;)Lmiui/date/Calendar;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCalendar:Lmiui/date/Calendar;

    return-object p1
.end method

.method static synthetic -set1(Z)Z
    .locals 0

    sput-boolean p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHasNotification:Z

    return p0
.end method

.method static synthetic -set2(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mInSmartCoverSmallWindowMode:Z

    return p1
.end method

.method static synthetic -set3(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mNotificationList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic -set4(Z)Z
    .locals 0

    sput-boolean p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowHorizontalTime:Z

    return p0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;ZZI)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateBatteryLevelText(ZZI)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowHorizontalTime:Z

    sput-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOldHasNotification:Z

    sput-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHasNotification:Z

    sput-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSupportVerticalClock:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/MiuiKeyguardBaseClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 v0, 0x42e60000    # 115.0f

    iput v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mLightColorMode:Z

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalToVerticalAnim:Landroid/animation/AnimatorSet;

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalToHorizontalAnim:Landroid/animation/AnimatorSet;

    iput-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$1;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$2;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateTimeRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$3;

    invoke-direct {v0, p0, v1}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$3;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$4;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    new-instance v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$5;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$5;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfoAndDateTransition:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$6;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$6;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string/jumbo v1, "fonts/MIround02-Regular.ttf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportVerticalClock(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSupportVerticalClock:Z

    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mPowerManager:Landroid/os/PowerManager;

    return-void
.end method

.method private clearAnim()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalToVerticalAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalToHorizontalAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCurrentDate:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    return-void
.end method

.method private showHorizontalTime()V
    .locals 10

    const/4 v9, 0x1

    const/high16 v6, -0x3cfa0000    # -134.0f

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->clearAnim()V

    sget-boolean v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSupportVerticalClock:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mInSmartCoverSmallWindowMode:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowHorizontalTime:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->playVerticalToHorizontalAnim()V

    :goto_0
    return-void

    :cond_0
    sput-boolean v9, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowHorizontalTime:Z

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setScaleX(F)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setScaleY(F)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mDateAndBatteryInfoLayout:Landroid/widget/FrameLayout;

    iget v5, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    sub-float v5, v6, v5

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    iget v5, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    sub-float v5, v6, v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTranslationY(F)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setAlpha(F)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setAlpha(F)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    iget v5, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    sub-float v5, v6, v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTranslationY(F)V

    sget-boolean v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHasNotification:Z

    sget-boolean v5, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOldHasNotification:Z

    if-eq v2, v5, :cond_3

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportVerticalClock(Landroid/content/Context;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    sget-object v5, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    const-string/jumbo v6, "alpha"

    const/4 v2, 0x2

    new-array v7, v2, [F

    sget-boolean v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHasNotification:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    const/4 v8, 0x0

    aput v2, v7, v8

    sget-boolean v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHasNotification:Z

    if-eqz v2, :cond_2

    :goto_2
    aput v4, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v2, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    sget-boolean v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHasNotification:Z

    sput-boolean v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOldHasNotification:Z

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    move v4, v3

    goto :goto_2

    :cond_3
    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    sget-boolean v5, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHasNotification:Z

    if-eqz v5, :cond_4

    :goto_3
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_0

    :cond_4
    move v4, v3

    goto :goto_3
.end method

.method private showVerticalTime()V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->clearAnim()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowHorizontalTime:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->playHorizontalToVerticalAnim()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowHorizontalTime:Z

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mDateAndBatteryInfoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method

.method private updateBatteryLevelText(ZZI)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowBatteryInfo:Z

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDeviceCharged()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f0b0004

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x14

    if-ge p3, v0, :cond_0

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_3
    iput-boolean v4, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowBatteryInfo:Z

    goto :goto_0
.end method


# virtual methods
.method public getTypeface()Landroid/graphics/Typeface;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 7

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mAttached:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mAttached:Z

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "android.intent.action.TIME_TICK"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerInfoCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->registerListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    const-string/jumbo v0, "is_small_window"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    const/4 v2, 0x0

    invoke-virtual {v0, v6, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mLightColorMode:Z

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateColorByWallpaper(Z)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->unregisterListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    invoke-super {p0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->onDetachedFromWindow()V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mAttached:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mAttached:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->clearAnim()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->onFinishInflate()V

    const v2, 0x7f0d005f

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTimeLayout:Landroid/widget/FrameLayout;

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->hasNavigationBar()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTimeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTimeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    const v2, 0x7f0d0061

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    const v2, 0x7f0d0062

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    const v2, 0x7f0d0064

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalHour:Landroid/widget/TextView;

    const v2, 0x7f0d0065

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalDot:Landroid/widget/TextView;

    const v2, 0x7f0d0066

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalMin:Landroid/widget/TextView;

    const v2, 0x7f0d0063

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0d0067

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mDateAndBatteryInfoLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f0d004a

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCurrentDate:Landroid/widget/TextView;

    const v2, 0x7f0d004c

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalHour:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalDot:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalMin:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    const v2, 0x7f0d0069

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    const v2, 0x7f0d0068

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sput-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    new-instance v2, Lmiui/date/Calendar;

    invoke-direct {v2}, Lmiui/date/Calendar;-><init>()V

    iput-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCalendar:Lmiui/date/Calendar;

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/keyguard/KeyguardCompatibilityHelperForL;->is24HourFormat(Landroid/content/Context;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->m24HourFormat:Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/MiuiSettings$System;->isInSmallWindowMode(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mInSmartCoverSmallWindowMode:Z

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalHour:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalDot:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget-object v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalMin:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateOwnerInfo()V

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mDisplaySimCardInfo:Z

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mPhoneCount:I

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->initCarrier(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateSimCardInfo()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateClockView()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateBatteryInfoAndDate()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateTime()V

    return-void
.end method

.method public playHorizontalToVerticalAnim()V
    .locals 15

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    const-string/jumbo v11, "translationY"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_0

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-wide/16 v10, 0x1a9

    invoke-virtual {v6, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v6, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    const-string/jumbo v11, "alpha"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_1

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const-wide/16 v10, 0x1a9

    invoke-virtual {v5, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Sine;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v5, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    const-string/jumbo v11, "translationY"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_2

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-wide/16 v10, 0x1a9

    invoke-virtual {v4, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    const-string/jumbo v11, "alpha"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_3

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-wide/16 v10, 0x1a9

    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Sine;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v3, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v11, "translationY"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_4

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v10, 0x1a9

    invoke-virtual {v2, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v11, "alpha"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_5

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v10, 0xd2

    invoke-virtual {v1, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mDateAndBatteryInfoLayout:Landroid/widget/FrameLayout;

    const-string/jumbo v11, "translationY"

    const/4 v12, 0x2

    new-array v12, v12, [F

    iget v13, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    const/high16 v14, -0x3cfa0000    # -134.0f

    sub-float v13, v14, v13

    const/4 v14, 0x0

    aput v13, v12, v14

    const/4 v13, 0x0

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v10, 0x1a9

    invoke-virtual {v0, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v10, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    const-string/jumbo v11, "translationY"

    const/4 v12, 0x2

    new-array v12, v12, [F

    iget v13, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    const/high16 v14, -0x3cfa0000    # -134.0f

    sub-float v13, v14, v13

    const/4 v14, 0x0

    aput v13, v12, v14

    const/4 v13, 0x0

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    const-wide/16 v10, 0x1a9

    invoke-virtual {v9, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v9, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    const-string/jumbo v11, "translationY"

    const/4 v12, 0x2

    new-array v12, v12, [F

    iget v13, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    const/high16 v14, -0x3cfa0000    # -134.0f

    sub-float v13, v14, v13

    const/4 v14, 0x0

    aput v13, v12, v14

    const/4 v13, 0x0

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    const-wide/16 v10, 0x1a9

    invoke-virtual {v7, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v7, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v10, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    const-string/jumbo v11, "alpha"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_6

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    const-wide/16 v10, 0x1a9

    invoke-virtual {v8, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v10, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v8, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v10, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalToVerticalAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v10, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v10

    invoke-virtual {v10, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v10, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalToVerticalAnim:Landroid/animation/AnimatorSet;

    new-instance v11, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$8;

    invoke-direct {v11, p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$8;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V

    invoke-virtual {v10, v11}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v10, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalToVerticalAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v10}, Landroid/animation/AnimatorSet;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        -0x3cfa0000    # -134.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        -0x3cfa0000    # -134.0f
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_4
    .array-data 4
        -0x3cfa0000    # -134.0f
        0x0
    .end array-data

    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public playVerticalToHorizontalAnim()V
    .locals 19

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    const-string/jumbo v15, "translationY"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_0

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v10

    const-wide/16 v14, 0x1a9

    invoke-virtual {v10, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v10, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    const-string/jumbo v15, "alpha"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_1

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    const-wide/16 v14, 0xd2

    invoke-virtual {v9, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v9, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    const-string/jumbo v15, "translationY"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_2

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    const-wide/16 v14, 0x1a9

    invoke-virtual {v8, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v8, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    const-string/jumbo v15, "alpha"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_3

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    const-wide/16 v14, 0xd2

    invoke-virtual {v7, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v7, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v15, "scaleX"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_4

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    sget-object v15, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v15}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setPivotX(F)V

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v15, "scaleY"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_5

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const-wide/16 v14, 0x1a9

    invoke-virtual {v5, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v5, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v15, "translationY"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_6

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-wide/16 v14, 0x1a9

    invoke-virtual {v6, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalTimeLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v15, "alpha"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_7

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-wide/16 v14, 0x1a9

    invoke-virtual {v3, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v6, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mDateAndBatteryInfoLayout:Landroid/widget/FrameLayout;

    const-string/jumbo v15, "translationY"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    aput v17, v16, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    move/from16 v17, v0

    const/high16 v18, -0x3cfa0000    # -134.0f

    sub-float v17, v18, v17

    const/16 v18, 0x1

    aput v17, v16, v18

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v14, 0x1a9

    invoke-virtual {v2, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    const-string/jumbo v15, "translationY"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    aput v17, v16, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    move/from16 v17, v0

    const/high16 v18, -0x3cfa0000    # -134.0f

    sub-float v17, v18, v17

    const/16 v18, 0x1

    aput v17, v16, v18

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    const-wide/16 v14, 0x1a9

    invoke-virtual {v13, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v13, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    const-string/jumbo v15, "translationY"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    aput v17, v16, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mTranslationDateExtra:F

    move/from16 v17, v0

    const/high16 v18, -0x3cfa0000    # -134.0f

    sub-float v17, v18, v17

    const/16 v18, 0x1

    aput v17, v16, v18

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    const-wide/16 v14, 0x1a9

    invoke-virtual {v12, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v12, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-object v14, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    const-string/jumbo v15, "alpha"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    fill-array-data v16, :array_8

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v11

    const-wide/16 v14, 0xd2

    invoke-virtual {v11, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v14, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v11, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalToHorizontalAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v14, v10}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v13}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v12}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v11}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalToHorizontalAnim:Landroid/animation/AnimatorSet;

    new-instance v15, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$7;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$7;-><init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V

    invoke-virtual {v14, v15}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalToHorizontalAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v14}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        -0x3cfa0000    # -134.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x0
        -0x3cfa0000    # -134.0f
    .end array-data

    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_5
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_6
    .array-data 4
        0x42d40000    # 106.0f
        -0x3cfa0000    # -134.0f
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_8
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 3

    iput-object p1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->shouldShowBatteryInfo()Z

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDevicePluggedIn()Z

    move-result v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getBatteryLevel()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateBatteryLevelText(ZZI)V

    return-void
.end method

.method public updateBatteryInfoAndDate()V
    .locals 4

    const/4 v2, 0x4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mShowBatteryInfo:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "is_pad"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCurrentDate:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCurrentDate:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfoAndDateTransition:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfoAndDateTransition:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCurrentDate:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateClockView()V
    .locals 1

    sget-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSupportVerticalClock:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mInSmartCoverSmallWindowMode:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHasNotification:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->showVerticalTime()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->showHorizontalTime()V

    goto :goto_0
.end method

.method public updateColorByWallpaper(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mLightColorMode:Z

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalHour:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalDot:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalMin:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCurrentDate:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->updateTime()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public updateOwnerInfo()V
    .locals 7

    const/4 v6, 0x0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mInSmartCoverSmallWindowMode:Z

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/provider/MiuiSettings$System;->getSmallWindowMode()Landroid/provider/MiuiSettings$System$SmallWindowType;

    move-result-object v1

    sget-object v2, Landroid/provider/MiuiSettings$System$SmallWindowType;->A1_STYLE:Landroid/provider/MiuiSettings$System$SmallWindowType;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mNotificationList:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mNotificationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mNotificationList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mNotificationList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const v5, 0x7f0e0004

    invoke-virtual {v2, v5, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfoString:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfoString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mOwnerInfo:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public updateSimCardInfo()V
    .locals 4

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mDisplaySimCardInfo:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const-string/jumbo v1, " | "

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mRealCarrier:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUtils;->join(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0096

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mSimCardInfo:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateTime()V
    .locals 6

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCalendar:Lmiui/date/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lmiui/date/Calendar;->setTimeInMillis(J)Lmiui/date/Calendar;

    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCalendar:Lmiui/date/Calendar;

    const/16 v4, 0x12

    invoke-virtual {v3, v4}, Lmiui/date/Calendar;->get(I)I

    move-result v1

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->m24HourFormat:Z

    if-nez v3, :cond_0

    const/16 v3, 0xc

    if-le v1, v3, :cond_0

    add-int/lit8 v1, v1, -0xc

    :cond_0
    iget-boolean v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->m24HourFormat:Z

    if-nez v3, :cond_1

    if-nez v1, :cond_1

    const/16 v1, 0xc

    :cond_1
    iget-object v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCalendar:Lmiui/date/Calendar;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Lmiui/date/Calendar;->get(I)I

    move-result v2

    sget-object v3, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalHour:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUtils;->formatTime(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalHour:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUtils;->formatTime(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mHorizontalMin:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUtils;->formatTime(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mVerticalMin:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUtils;->formatTime(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->m24HourFormat:Z

    if-eqz v3, :cond_2

    const v0, 0x7f0b0017

    :goto_0
    sget-object v3, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCurrentDate:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mCalendar:Lmiui/date/Calendar;

    iget-object v5, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmiui/date/Calendar;->format(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    const v0, 0x7f0b0018

    goto :goto_0
.end method
