.class Lcom/android/keyguard/MiuiLockPatternKeyguardView$10;
.super Ljava/lang/Object;
.source "MiuiLockPatternKeyguardView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiLockPatternKeyguardView;->startFaceUnlock()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$10;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$10;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get2(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->hasInit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$10;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get2(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$10;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get3(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/TextureView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$10;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get1(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->startFaceUnlock(Landroid/view/TextureView;Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;)V

    :cond_0
    return-void
.end method
