.class Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;

.field final synthetic val$leftViewClicked:Z

.field final synthetic val$v:Landroid/view/View;

.field final synthetic val$viewToDismiss:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;Landroid/view/View;Landroid/view/View;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->this$2:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;

    iput-object p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->val$v:Landroid/view/View;

    iput-object p3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->val$viewToDismiss:Landroid/view/View;

    iput-boolean p4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->val$leftViewClicked:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/4 v4, 0x0

    const/4 v1, 0x1

    new-instance v9, Landroid/util/TypedValue;

    invoke-direct {v9}, Landroid/util/TypedValue;-><init>()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->this$2:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0048

    invoke-virtual {v0, v2, v9, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->val$v:Landroid/view/View;

    invoke-virtual {v9}, Landroid/util/TypedValue;->getFloat()F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->this$2:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/UnlockHintSwitcher;->showPrevious()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->val$viewToDismiss:Landroid/view/View;

    if-eqz v0, :cond_0

    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v4, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->val$leftViewClicked:Z

    if-eqz v2, :cond_1

    const/high16 v2, 0x40000000    # 2.0f

    :goto_0
    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v0, 0x12c

    invoke-virtual {v10, v0, v1}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2$1;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->val$viewToDismiss:Landroid/view/View;

    invoke-direct {v0, p0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;Landroid/view/View;)V

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->val$viewToDismiss:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;->this$2:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-set0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;Z)Z

    return-void

    :cond_1
    const/high16 v2, -0x40000000    # -2.0f

    goto :goto_0
.end method
