.class public Lcom/android/keyguard/MiuiDefaultLockScreen;
.super Landroid/widget/FrameLayout;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardScreen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiDefaultLockScreen$10;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$11;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$12;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$13;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$14;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$15;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$16;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$1;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$2;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$3;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$4;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$5;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$6;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$7;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$8;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$9;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;
    }
.end annotation


# static fields
.field private static final BABY_AUTHORITY:Ljava/lang/String; = "com.miui.gallery.cloud.baby.wallpaper_provider"

.field private static final BOUNCE_ANIMATION_FIRST_PHASE_DURATION:I = 0x96

.field private static final BOUNCE_ANIMATION_SECOND_PHASE_DURATION:I = 0x190

.field public static final CAMERA_PKGNAME:Ljava/lang/String; = "com.android.camera"

.field public static final DEFAULT_CN_LOCKWALLPAPER_PROVIDER:Ljava/lang/String; = "com.xiaomi.tv.gallerylockscreen.lockscreen_magazine_provider"

.field public static final DEFAULT_FASHIONGALLERY_PACKAGE_NAME:Ljava/lang/String;

.field public static final DEFAULT_GLOBAL_LOCKWALLPAPER_PROVIDER:Ljava/lang/String; = "com.xiaomi.tv.gallerylockscreen.lockscreen_magazine_provider"

.field public static final DEFAULT_LOCKWALLPAPER_PROVIDER:Ljava/lang/String;

.field private static final HORIZONTAL_SWIPE_ANIMATION_DURATION:I = 0x64

.field public static final LEFT_ACTION_DEFAULT_PKGNAME:Ljava/lang/String; = "com.xiaomi.smarthome"

.field private static final MAX_LIGHT_IN_SUSPECT_MODE:F = 3.0f

.field private static final MAX_Z_IN_SUSPECT_MODE:F = 2.0f

.field private static final MOVE_DOWN:I = 0x4

.field private static final MOVE_LEFT:I = 0x1

.field private static final MOVE_RIGHT:I = 0x2

.field private static final MOVE_UP:I = 0x3

.field public static final PRFE_KEY_PREVIEW_BUTTON_CLICKED:Ljava/lang/String; = "prfe_key_preview_button_clicked"

.field public static final RIGHT_ACTION_DEFAULT_PKGNAME:Ljava/lang/String; = "com.android.camera"

.field private static final SCREEN_ON_ANIMATION_DELAY:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "MiuiDefaultLockScreen"

.field private static final VALID_CLICK_TIME_UPPER_LIMIT:I = 0xc8

.field private static final VALID_GESTURE_DURATION_LOWER_LIMIT:I = 0x3c

.field private static final VALID_GESTURE_DURATION_UPPER_LIMIT:I = 0x258


# instance fields
.field private mAccelerometerSensor:Landroid/hardware/Sensor;

.field private final mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mBatteryLevel:I

.field private mBeforeUpAnimChargingContainerWidth:I

.field private mBeforeUpAnimChargingViewY:F

.field private mBeginRightMove:Z

.field private mBottomBgView:Landroid/view/View;

.field private mCameraBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mCameraView:Landroid/view/View;

.field private mChargingHintText:Ljava/lang/String;

.field private mCurrentLanguage:Ljava/lang/String;

.field private mCurrentRegion:Ljava/lang/String;

.field private mCurrentUserId:I

.field private mDirectionCheckPointX:F

.field private mDirectionCheckPointY:F

.field private mDisplayMusicController:Z

.field private final mFingerprintCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

.field private mHaveDelayResetClockTask:Z

.field private mHidden:Z

.field private mHorizontalDragging:Z

.field private mIgnoreTouchEventSequence:Z

.field private mInAnimating:Z

.field private final mInLiteMode:Z

.field private mInPocket:Z

.field private mInScrolling:Z

.field private mInSmartCoverSmallWindowMode:Z

.field private mInitialDownTime:J

.field private mInitialMoveDirection:I

.field private mInitialTouchX:F

.field private mInitialTouchY:F

.field private mIsInSuspectMode:Z

.field private mIsPsensorDisabled:Z

.field private mIsSecure:Z

.field private mIsTouchEventInNotificationViews:Z

.field private mIsUserAuthenticatedSinceBoot:Z

.field private mKeyguardHorizontalGestureSlop:I

.field private mKeyguardMusicController:Lcom/android/keyguard/KeyguardMusicController;

.field private mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

.field private mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field private mKeyguardVerticalGestureSlop:I

.field private mLastTimeOfScreenOnAnimation:J

.field private mLeftActionAvailable:Z

.field private mLeftActionLabel:Ljava/lang/CharSequence;

.field private mLeftMoveView:Landroid/view/View;

.field private final mLightAndAccSensorListener:Landroid/hardware/SensorEventListener;

.field private mLigthLux:F

.field private mLigthSensor:Landroid/hardware/Sensor;

.field private mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

.field private mLockWallpaperProviderObserver:Landroid/database/ContentObserver;

.field private final mMamlCommandListener:Lmiui/maml/ScreenElementRoot$OnExternCommandListener;

.field private mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

.field private mMusicControlMamlView:Lmiui/maml/component/MamlView;

.field private final mMusicStateChangeListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;

.field private mNeedRepositionDevice:Z

.field private final mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

.field private mNotificationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mOrientationZ:F

.field private mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

.field private mPhoneSignalChangeCallback:Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

.field private mPluggedIn:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private mProximitySensorTooClose:Z

.field private mProximitySensorWrapper:Lmiui/util/ProximitySensorWrapper;

.field private mProximitySensorWrapperForGestureDetector:Lmiui/util/ProximitySensorWrapper;

.field private mQuickCharging:Ljava/lang/Boolean;

.field mResetClockRunnable:Ljava/lang/Runnable;

.field private mResetNotificationListRunnable:Ljava/lang/Runnable;

.field private mResumed:Z

.field private mRightActionAvailable:Z

.field private mRightActionLabel:Ljava/lang/CharSequence;

.field private mRightIntent:Landroid/content/Intent;

.field private mRunnableScrollEnd:Ljava/lang/Runnable;

.field private mScreenHeight:I

.field private final mScroller:Landroid/widget/Scroller;

.field private final mSecureCameraByDpm:Z

.field private mSelectedNotificationKey:I

.field private final mSensorListener:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

.field private mSensorListenerForGestureDetector:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private final mShowMagazineEntrance:Z

.field private mSignalAvailable:Z

.field private final mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

.field private mStartingLeftAction:Z

.field private mStartingRightAction:Z

.field mSwitchAnimator:Landroid/animation/AnimatorSet;

.field private mTemp:I

.field private final mToggleSmartCoverModeRunnable:Ljava/lang/Runnable;

.field private final mTypeface:Landroid/graphics/Typeface;

.field private final mVelocityTracker:Landroid/view/VelocityTracker;

.field private mVerticalDragging:Z

.field private mViewConfiguration:Landroid/view/ViewConfiguration;

.field private mWakeupForNotification:Z

.field private mWakeupForNotificationObserver:Landroid/database/ContentObserver;

.field private final mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

.field private mWallpaperChanging:Z

.field private mWallpaperPreviewAvailable:Z

.field private mWirelessChargeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiDefaultLockScreen;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBatteryLevel:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic -get10(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsPsensorDisabled:Z

    return v0
.end method

.method static synthetic -get11(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsSecure:Z

    return v0
.end method

.method static synthetic -get12(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsUserAuthenticatedSinceBoot:Z

    return v0
.end method

.method static synthetic -get13(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/KeyguardMusicController;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardMusicController:Lcom/android/keyguard/KeyguardMusicController;

    return-object v0
.end method

.method static synthetic -get14(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardScreenCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    return-object v0
.end method

.method static synthetic -get15(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    return-object v0
.end method

.method static synthetic -get16(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftActionAvailable:Z

    return v0
.end method

.method static synthetic -get17(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftActionLabel:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic -get18(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get19(Lcom/android/keyguard/MiuiDefaultLockScreen;)F
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLigthLux:F

    return v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    return-object v0
.end method

.method static synthetic -get21(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/ScreenElementRoot$OnExternCommandListener;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMamlCommandListener:Lmiui/maml/ScreenElementRoot$OnExternCommandListener;

    return-object v0
.end method

.method static synthetic -get22(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardMoveLeftView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    return-object v0
.end method

.method static synthetic -get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMusicControlMamlView:Lmiui/maml/component/MamlView;

    return-object v0
.end method

.method static synthetic -get24(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNeedRepositionDevice:Z

    return v0
.end method

.method static synthetic -get25(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    return-object v0
.end method

.method static synthetic -get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNotificationList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic -get27(Lcom/android/keyguard/MiuiDefaultLockScreen;)F
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mOrientationZ:F

    return v0
.end method

.method static synthetic -get28(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPluggedIn:Z

    return v0
.end method

.method static synthetic -get29(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/util/ProximitySensorWrapper;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapper:Lmiui/util/ProximitySensorWrapper;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mChargingHintText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get30(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mQuickCharging:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic -get31(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResetNotificationListRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get32(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResumed:Z

    return v0
.end method

.method static synthetic -get33(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightActionAvailable:Z

    return v0
.end method

.method static synthetic -get34(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightActionLabel:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic -get35(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic -get36(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRunnableScrollEnd:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get37(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/widget/Scroller;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mScroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic -get38(Lcom/android/keyguard/MiuiDefaultLockScreen;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSelectedNotificationKey:I

    return v0
.end method

.method static synthetic -get39(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorListener:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get40(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mShowMagazineEntrance:Z

    return v0
.end method

.method static synthetic -get41(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSignalAvailable:Z

    return v0
.end method

.method static synthetic -get42(Lcom/android/keyguard/MiuiDefaultLockScreen;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mTemp:I

    return v0
.end method

.method static synthetic -get43(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mToggleSmartCoverModeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get44(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/graphics/Typeface;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method static synthetic -get45(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWakeupForNotification:Z

    return v0
.end method

.method static synthetic -get46(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChanging:Z

    return v0
.end method

.method static synthetic -get47(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperPreviewAvailable:Z

    return v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDisplayMusicController:Z

    return v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHidden:Z

    return v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    return v0
.end method

.method static synthetic -get8(Lcom/android/keyguard/MiuiDefaultLockScreen;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialDownTime:J

    return-wide v0
.end method

.method static synthetic -get9(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsInSuspectMode:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mChargingHintText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -set10(Lcom/android/keyguard/MiuiDefaultLockScreen;Lmiui/maml/component/MamlView;)Lmiui/maml/component/MamlView;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMusicControlMamlView:Lmiui/maml/component/MamlView;

    return-object p1
.end method

.method static synthetic -set11(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNotificationList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic -set12(Lcom/android/keyguard/MiuiDefaultLockScreen;F)F
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mOrientationZ:F

    return p1
.end method

.method static synthetic -set13(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorTooClose:Z

    return p1
.end method

.method static synthetic -set14(Lcom/android/keyguard/MiuiDefaultLockScreen;Lmiui/util/ProximitySensorWrapper;)Lmiui/util/ProximitySensorWrapper;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapper:Lmiui/util/ProximitySensorWrapper;

    return-object p1
.end method

.method static synthetic -set15(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mQuickCharging:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic -set16(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRunnableScrollEnd:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic -set17(Lcom/android/keyguard/MiuiDefaultLockScreen;I)I
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSelectedNotificationKey:I

    return p1
.end method

.method static synthetic -set18(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSignalAvailable:Z

    return p1
.end method

.method static synthetic -set19(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChanging:Z

    return p1
.end method

.method static synthetic -set2(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDisplayMusicController:Z

    return p1
.end method

.method static synthetic -set20(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperPreviewAvailable:Z

    return p1
.end method

.method static synthetic -set3(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    return p1
.end method

.method static synthetic -set4(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInScrolling:Z

    return p1
.end method

.method static synthetic -set5(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    return p1
.end method

.method static synthetic -set6(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsInSuspectMode:Z

    return p1
.end method

.method static synthetic -set7(Lcom/android/keyguard/MiuiDefaultLockScreen;Lcom/android/keyguard/KeyguardMusicController;)Lcom/android/keyguard/KeyguardMusicController;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardMusicController:Lcom/android/keyguard/KeyguardMusicController;

    return-object p1
.end method

.method static synthetic -set8(Lcom/android/keyguard/MiuiDefaultLockScreen;I)I
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardVerticalGestureSlop:I

    return p1
.end method

.method static synthetic -set9(Lcom/android/keyguard/MiuiDefaultLockScreen;F)F
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLigthLux:F

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->needHideProvider()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiDefaultLockScreen;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getLeftViewBackgroundColor()I

    move-result v0

    return v0
.end method

.method static synthetic -wrap10(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->resetNotificationList()V

    return-void
.end method

.method static synthetic -wrap11(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->resetView()V

    return-void
.end method

.method static synthetic -wrap12(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->reverseSlideAction()V

    return-void
.end method

.method static synthetic -wrap13(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setNeedRepositionDevice(Z)V

    return-void
.end method

.method static synthetic -wrap14(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setPreviewButtonClicked()V

    return-void
.end method

.method static synthetic -wrap15(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/view/View;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->startViewBounceAnimation(Landroid/view/View;IZ)V

    return-void
.end method

.method static synthetic -wrap16(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->switchToLockScreenWallPaper(Z)V

    return-void
.end method

.method static synthetic -wrap17(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updateGaussViewAlpha()V

    return-void
.end method

.method static synthetic -wrap18(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updatePreviewConfig()V

    return-void
.end method

.method static synthetic -wrap19(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updateStatusBarColormode()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiDefaultLockScreen;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v0

    return v0
.end method

.method static synthetic -wrap20(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updateWakeupForNotification()V

    return-void
.end method

.method static synthetic -wrap21(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->wakeupScreenByNotification()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->changeLayouts()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/keyguard/MiuiDefaultLockScreen;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->dragChildrenViews(F)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleSingleClickEvent()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/keyguard/MiuiDefaultLockScreen;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleVerticalAlpha(F)V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/keyguard/MiuiDefaultLockScreen;ZII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->onRefreshBatteryInfo(ZII)V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->registerLightAndAccSensor()V

    return-void
.end method

.method static synthetic -wrap9(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->registerPSensorForGesture()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "com.xiaomi.tv.gallerylockscreen.lockscreen_magazine_provider"

    :goto_0
    sput-object v0, Lcom/android/keyguard/MiuiDefaultLockScreen;->DEFAULT_LOCKWALLPAPER_PROVIDER:Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "com.mfashiongallery.emag"

    :goto_1
    sput-object v0, Lcom/android/keyguard/MiuiDefaultLockScreen;->DEFAULT_FASHIONGALLERY_PACKAGE_NAME:Ljava/lang/String;

    return-void

    :cond_0
    const-string/jumbo v0, "com.xiaomi.tv.gallerylockscreen.lockscreen_magazine_provider"

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "com.mfashiongallery.emag"

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V
    .locals 8

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHorizontalDragging:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVerticalDragging:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChanging:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInScrolling:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInPocket:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsPsensorDisabled:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIgnoreTouchEventSequence:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsTouchEventInNotificationViews:Z

    const/4 v4, 0x0

    iput v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNotificationList:Ljava/util/List;

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSignalAvailable:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    const/4 v4, 0x0

    iput v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSelectedNotificationKey:I

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResumed:Z

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapper:Lmiui/util/ProximitySensorWrapper;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapperForGestureDetector:Lmiui/util/ProximitySensorWrapper;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLigthSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mAccelerometerSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorTooClose:Z

    const/4 v4, 0x0

    iput v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentUserId:I

    const/high16 v4, 0x41200000    # 10.0f

    iput v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLigthLux:F

    const/high16 v4, 0x40000000    # 2.0f

    iput v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mOrientationZ:F

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsInSuspectMode:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNeedRepositionDevice:Z

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$1;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$2;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$2;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWirelessChargeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$3;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$3;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMamlCommandListener:Lmiui/maml/ScreenElementRoot$OnExternCommandListener;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$4;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$4;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$5;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$5;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$6;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$6;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMusicStateChangeListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$7;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$7;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLightAndAccSensorListener:Landroid/hardware/SensorEventListener;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$8;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$8;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorListener:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$9;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$9;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockWallpaperProviderObserver:Landroid/database/ContentObserver;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$10;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$10;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWakeupForNotificationObserver:Landroid/database/ContentObserver;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$11;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLastTimeOfScreenOnAnimation:J

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$12;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$12;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mToggleSmartCoverModeRunnable:Ljava/lang/Runnable;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$13;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$13;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorListenerForGestureDetector:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHaveDelayResetClockTask:Z

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$14;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$14;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResetClockRunnable:Ljava/lang/Runnable;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$15;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$15;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResetNotificationListRunnable:Ljava/lang/Runnable;

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$16;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$16;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mFingerprintCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    iput v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentUserId:I

    invoke-static {}, Lcom/android/keyguard/AwesomeLockScreen;->clearCache()V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "power"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Lmiui/util/MiuiFeatureUtils;->isLiteMode()Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInLiteMode:Z

    iput-object p4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iput-object p5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "wakeup_for_keyguard_notification"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v6

    const/4 v7, 0x1

    invoke-static {v4, v5, v7, v6}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWakeupForNotification:Z

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "show_lockscreen_magazine_entrance"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v6

    const/4 v7, 0x1

    invoke-static {v4, v5, v7, v6}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mShowMagazineEntrance:Z

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v4}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->isSecure()Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsSecure:Z

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsUserAuthenticatedSinceBoot:Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0042

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardVerticalGestureSlop:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0044

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardHorizontalGestureSlop:I

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mViewConfiguration:Landroid/view/ViewConfiguration;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->initMusicState()V

    new-instance v4, Landroid/widget/Scroller;

    sget-object v5, Lcom/android/keyguard/MiuiKeyguardAnimationUtils;->SCROLLER_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-direct {v4, p1, v5}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setOverScrollMode(I)V

    const/4 v4, 0x0

    if-nez v4, :cond_3

    const-string/jumbo v4, "miui"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mTypeface:Landroid/graphics/Typeface;

    :goto_0
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setBackgroundColor(I)V

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setFocusable(Z)V

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setFocusableInTouchMode(Z)V

    new-instance v4, Landroid/view/View;

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBottomBgView:Landroid/view/View;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBottomBgView:Landroid/view/View;

    const v5, 0x7f020026

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020026

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, -0x1

    invoke-direct {v3, v5, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0x50

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBottomBgView:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBottomBgView:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->addView(Landroid/view/View;)V

    new-instance v4, Landroid/view/View;

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getLeftViewBackgroundColor()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030012

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-virtual {v4, v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setKeyguardScreenCallback(Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setTranslationX(F)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v4, p0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setDefaultScreenCallback(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->initLeftView()V

    :cond_0
    new-instance v4, Landroid/view/View;

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setCameraImage()V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->initLocal()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updatePreviewConfig()V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "device_policy"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    if-eqz v2, :cond_1

    iget v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentUserId:I

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v4}, Landroid/app/admin/DevicePolicyManager;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I

    move-result v1

    :cond_1
    iget v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentUserId:I

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v4}, Landroid/app/admin/DevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;I)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSecureCameraByDpm:Z

    :goto_1
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/keyguard/MiuiKeyguardUtils;->isPsensorDisabled(Landroid/content/Context;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsPsensorDisabled:Z

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsPsensorDisabled:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "sensor"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/SensorManager;

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    :cond_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->initKeyguardShortcut()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->resetView()V

    return-void

    :cond_3
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mTypeface:Landroid/graphics/Typeface;

    goto/16 :goto_0

    :cond_4
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_5

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsSecure:Z

    :goto_2
    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSecureCameraByDpm:Z

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private calculateDirection(FFFF)I
    .locals 7

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    sub-float v0, p3, p1

    sub-float v1, p4, p2

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    :cond_0
    mul-float/2addr v1, v6

    :cond_1
    :goto_0
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    cmpl-float v3, v1, v5

    if-lez v3, :cond_3

    const/4 v2, 0x4

    :goto_1
    return v2

    :cond_2
    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    if-eqz v3, :cond_1

    mul-float/2addr v0, v6

    goto :goto_0

    :cond_3
    const/4 v2, 0x3

    goto :goto_1

    :cond_4
    cmpl-float v3, v0, v5

    if-lez v3, :cond_5

    const/4 v2, 0x2

    goto :goto_1

    :cond_5
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private cancelSwitchAnimate()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    :cond_0
    return-void
.end method

.method private changeLayouts()V
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    if-eqz v4, :cond_3

    invoke-static {}, Landroid/provider/MiuiSettings$System;->getDisplayWindowSizeInSmartCover()Landroid/graphics/Rect;

    move-result-object v3

    iget v4, v3, Landroid/graphics/Rect;->left:I

    iget v5, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getHeight()I

    move-result v6

    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v6, v7

    invoke-virtual {p0, v4, v8, v5, v6}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setPadding(IIII)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v4, :cond_1

    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->getBottom()I

    move-result v5

    if-ge v4, v5, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00b7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    :goto_1
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0, v8, v8, v8, v8}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setPadding(IIII)V

    iput v8, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0049

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_1
.end method

.method private createMainLockView()Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;
    .locals 2

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/content/Context;)V

    return-object v0
.end method

.method private dragChildrenViews(F)V
    .locals 4

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->getTranslationY()F

    move-result v0

    sub-float v2, p1, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float v1, v0, v2

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->setTranslationY(F)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    return-void
.end method

.method private enterPocketMode()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInPocket:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeResetClockCallbacks()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->cancelSwitchAnimate()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->resetView()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setAlpha(F)V

    return-void
.end method

.method private getLeftViewBackgroundColor()I
    .locals 4

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lmiui/content/res/ThemeResources;->getLockWallpaperCache(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/keyguard/MiuiKeyguardUtils;->getFastBlurColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {v1, v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->addTwoColor(II)I

    move-result v0

    goto :goto_0
.end method

.method private getScreenWidth()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v0
.end method

.method private handleGlobalWallPaperDesDraggingDown(Landroid/view/MotionEvent;)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHaveDelayResetClockTask:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeResetClockCallbacks()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    invoke-virtual {v0, p1, v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->handleVerticalDragging(Landroid/view/MotionEvent;F)V

    return-void
.end method

.method private handleGlobalWallPaperDesDraggingDownUpEvent(Landroid/view/MotionEvent;)V
    .locals 2

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHaveDelayResetClockTask:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->postDelayedResetClock()V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    invoke-virtual {v1, p1, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->handleVerticalDraggingUpEvent(Landroid/view/MotionEvent;F)V

    return-void
.end method

.method private handleHorizontalDragging(Landroid/view/MotionEvent;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    if-ne v1, v5, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightActionAvailable:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setTranslationX(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    sub-float v0, v1, v2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setTranslationX(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setTranslationX(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float v2, v4, v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setAlpha(F)V

    goto :goto_1

    :cond_3
    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftActionAvailable:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBeginRightMove:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->uploadLeftItemData()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iput-boolean v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBeginRightMove:Z

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    sub-float/2addr v1, v2

    cmpg-float v1, v1, v3

    if-gez v1, :cond_5

    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setTranslationX(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float v2, v4, v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    add-float/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setTranslationX(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setAlpha(F)V

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    sub-float v0, v1, v2

    goto :goto_2
.end method

.method private handleHorizontalDraggingUpEvent(Landroid/view/MotionEvent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-direct {p0, v1, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->isValidDraggingGesture(FZ)Z

    move-result v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    if-ne v1, v4, :cond_2

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightActionAvailable:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->triggerRightAction()V

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->reverseSlideActionAnim(Z)V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftActionAvailable:Z

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBeginRightMove:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->triggerLeftAction()V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->reverseSlideActionAnim(Z)V

    goto :goto_0
.end method

.method private handleMoveDownEvent(Landroid/view/MotionEvent;)V
    .locals 7

    const/4 v6, 0x1

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->getTranslationY()F

    move-result v2

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v3, v2

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v4

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v3, 0x42c80000    # 100.0f

    cmpl-float v3, v1, v3

    if-lez v3, :cond_0

    const/high16 v1, 0x42c80000    # 100.0f

    :cond_0
    const/4 v3, 0x2

    new-array v3, v3, [F

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getAlpha()F

    move-result v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getAlpha()F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    const/4 v5, 0x0

    aput v4, v3, v5

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v6

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    float-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/android/keyguard/MiuiDefaultLockScreen$32;

    invoke-direct {v3, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$32;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v3

    neg-float v4, v2

    float-to-int v4, v4

    invoke-direct {p0, v3, v4, v6}, Lcom/android/keyguard/MiuiDefaultLockScreen;->startViewBounceDownAnimation(Landroid/view/View;IZ)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;

    move-result-object v3

    neg-float v4, v2

    float-to-int v4, v4

    invoke-direct {p0, v3, v4, v6}, Lcom/android/keyguard/MiuiDefaultLockScreen;->startViewBounceDownAnimation(Landroid/view/View;IZ)V

    return-void
.end method

.method private handleMoveUpEvent(Landroid/view/MotionEvent;)V
    .locals 11

    const/4 v10, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    sub-float v9, v0, v2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDisplayMusicController:Z

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v0

    int-to-float v0, v0

    sub-float v8, v0, v9

    :goto_0
    const/4 v0, 0x0

    cmpg-float v0, v8, v0

    if-gez v0, :cond_4

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->unlockScreenExisted()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v7

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->unlockScreenExisted()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v7, :cond_2

    iput-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    invoke-static {}, Lcom/android/keyguard/KeyguardCompatibilityHelperForN;->keyguardGoingAway()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_5

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->startParentViewFadeOutAnimation()V

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->getBottom()I

    move-result v0

    int-to-float v0, v0

    sub-float v8, v0, v9

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    invoke-static {v8, v0, v10}, Lcom/android/keyguard/MiuiKeyguardAnimationUtils;->computeScrollDuration(FFZ)F

    move-result v6

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    neg-float v3, v8

    float-to-int v4, v3

    float-to-int v5, v6

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->postInvalidateOnAnimation()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->unlockScreenExisted()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$29;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$29;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRunnableScrollEnd:Ljava/lang/Runnable;

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$30;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$30;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->post(Ljava/lang/Runnable;)Z

    goto :goto_2
.end method

.method private handleSingleClickEvent()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSelectedNotificationKey:I

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->resetNotificationList()V

    :cond_0
    :goto_0
    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$27;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$27;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void

    :cond_2
    const-string/jumbo v0, "zh"

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentLanguage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "CN"

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    if-eqz v0, :cond_9

    :cond_4
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get15(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/LoadingContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/LoadingContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->isLockScreenMagazine:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    return-void

    :cond_6
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->canDisplay()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->getVisibility()I

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->switchToLockScreenWallPaper(Z)V

    :cond_7
    :goto_1
    const/4 v0, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get15(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/LoadingContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/LoadingContainer;->changeMode()V

    goto/16 :goto_0

    :cond_8
    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->switchToLockScreenWallPaper(Z)V

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x1e

    invoke-direct {p0, v0, v1, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->startViewBounceAnimation(Landroid/view/View;IZ)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x1e

    invoke-direct {p0, v0, v1, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->startViewBounceAnimation(Landroid/view/View;IZ)V

    goto/16 :goto_0
.end method

.method private handleVerticalAlpha(F)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->unlockScreenExisted()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setAlpha(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNotificationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDisplayMusicController:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setGaussViewAlphaFromLockScreen(F)V

    goto :goto_0
.end method

.method private handleVerticalDragging(Landroid/view/MotionEvent;)V
    .locals 4

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->isGlobalWallPaperDesDragDownMode()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleGlobalWallPaperDesDraggingDown(Landroid/view/MotionEvent;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mViewConfiguration:Landroid/view/ViewConfiguration;

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    int-to-float v3, v1

    add-float/2addr v2, v3

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v0, v2

    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-gez v2, :cond_1

    const/4 v0, 0x0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    sub-float/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->dragChildrenViews(F)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-ge v2, v3, :cond_2

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleVerticalAlpha(F)V

    :cond_2
    return-void
.end method

.method private handleVerticalDraggingUpEvent(Landroid/view/MotionEvent;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->isGlobalWallPaperDesDragDownMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleGlobalWallPaperDesDraggingDownUpEvent(Landroid/view/MotionEvent;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    sub-float/2addr v0, v1

    invoke-direct {p0, v0, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->isValidDraggingGesture(FZ)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleMoveUpEvent(Landroid/view/MotionEvent;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleMoveDownEvent(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public static hideLockScreenInActivityManager()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForN;->setLockScreenShown(ZZ)V

    :cond_0
    return-void
.end method

.method private initLocal()V
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentRegion:Ljava/lang/String;

    iget-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentLanguage:Ljava/lang/String;

    return-void
.end method

.method private isGlobalWallPaperDesDragDownMode()Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;

    move-result-object v1

    instance-of v1, v1, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    invoke-virtual {v0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->isHasLinkInfo()Z

    move-result v0

    :cond_0
    return v0
.end method

.method private isPreviewButtonClicked()Z
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "prfe_key_preview_button_clicked"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private isValidDraggingGesture(FZ)Z
    .locals 8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialDownTime:J

    sub-long v0, v4, v6

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorTooClose:Z

    if-eqz v3, :cond_0

    const-wide/16 v4, 0x258

    cmp-long v3, v0, v4

    if-gez v3, :cond_1

    const-wide/16 v4, 0x3c

    cmp-long v3, v0, v4

    if-lez v3, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    if-eqz p2, :cond_2

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardVerticalGestureSlop:I

    :goto_1
    int-to-float v3, v3

    cmpl-float v3, v4, v3

    if-lez v3, :cond_3

    :goto_2
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardHorizontalGestureSlop:I

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private needHideProvider()Z
    .locals 2

    const/4 v0, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const-string/jumbo v0, "IN"

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private onRefreshBatteryInfo(ZII)V
    .locals 8

    const/4 v5, 0x4

    const/4 v3, 0x0

    const-string/jumbo v2, "is_pad"

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iput p3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mTemp:I

    const/4 v0, 0x0

    if-nez p1, :cond_1

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPluggedIn:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/keyguard/UnlockHintSwitcher;->setVisibility(I)V

    :cond_1
    if-eqz p1, :cond_2

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPluggedIn:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/keyguard/UnlockHintSwitcher;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get18(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get18(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get2(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get2(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Ljava/lang/Runnable;

    move-result-object v4

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    :goto_0
    int-to-long v6, v2

    invoke-virtual {p0, v4, v6, v7}, Lcom/android/keyguard/MiuiDefaultLockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->clearAnimation()V

    const/4 v0, 0x1

    :cond_2
    if-eqz p1, :cond_6

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get18(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/keyguard/UnlockHintSwitcher;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    move-result-object v2

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mQuickCharging:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v2, p2, v4}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setChargingLevel(IZ)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setVisibility(I)V

    if-nez v0, :cond_3

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setVisibility(I)V

    :cond_3
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBottomBgView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    :goto_1
    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPluggedIn:Z

    iput p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBatteryLevel:I

    move v1, v0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->refreshChargingInfo(Z)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap6(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPluggedIn:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get6(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/keyguard/UnlockHintSwitcher;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    return-void

    :cond_5
    const/16 v2, 0x1f4

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBottomBgView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->isFullScreen()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v2

    new-instance v3, Lcom/android/keyguard/MiuiDefaultLockScreen$18;

    invoke-direct {v3, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$18;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {v2, v3}, Lcom/android/keyguard/UnlockHintSwitcher;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method private postDelayedResetClock()V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResetClockRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHaveDelayResetClockTask:Z

    return-void
.end method

.method private refreshChargingInfo(Z)V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;

    invoke-direct {v0, p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$19;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private registerLightAndAccSensor()V
    .locals 4

    const/4 v3, 0x3

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsPsensorDisabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLigthSensor:Landroid/hardware/Sensor;

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLightAndAccSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLigthSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mAccelerometerSensor:Landroid/hardware/Sensor;

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLightAndAccSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mAccelerometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_0
    return-void
.end method

.method private registerPSensorForGesture()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapperForGestureDetector:Lmiui/util/ProximitySensorWrapper;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsPsensorDisabled:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResumed:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmiui/util/ProximitySensorWrapper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiui/util/ProximitySensorWrapper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapperForGestureDetector:Lmiui/util/ProximitySensorWrapper;

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapperForGestureDetector:Lmiui/util/ProximitySensorWrapper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorListenerForGestureDetector:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

    invoke-virtual {v0, v1}, Lmiui/util/ProximitySensorWrapper;->registerListener(Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;)V

    :cond_0
    return-void
.end method

.method private removeResetClockCallbacks()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResetClockRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHaveDelayResetClockTask:Z

    return-void
.end method

.method private resetNotificationList()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSelectedNotificationKey:I

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method private resetView()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMusicControlMamlView:Lmiui/maml/component/MamlView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBeginRightMove:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->createMainLockView()Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->addView(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPluggedIn:Z

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBatteryLevel:I

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mTemp:I

    invoke-direct {p0, v0, v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->onRefreshBatteryInfo(ZII)V

    return-void
.end method

.method private reverseSlideAction()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setTranslationX(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setTranslationX(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method private reverseSlideActionAnim(Z)V
    .locals 12

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    if-eqz v2, :cond_1

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getTranslationX()F

    move-result v5

    aput v5, v4, v7

    aput v10, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    aput v5, v4, v7

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v5

    int-to-float v5, v5

    aput v5, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    aput v5, v4, v7

    aput v11, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    const-string/jumbo v3, "alpha"

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    sub-float v5, v11, v5

    aput v5, v4, v7

    aput v10, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_0
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v2, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v2}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    aput v5, v4, v7

    aput v10, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTranslationX()F

    move-result v5

    aput v5, v4, v7

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v5

    int-to-float v5, v5

    aput v5, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :goto_1
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v2, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v2}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getTranslationX()F

    move-result v5

    aput v5, v4, v7

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    aput v5, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    aput v5, v4, v7

    aput v10, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    const-string/jumbo v3, "alpha"

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    sub-float v5, v11, v5

    aput v5, v4, v7

    aput v11, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1
.end method

.method private setCameraImage()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$17;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$17;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$17;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private setNeedRepositionDevice(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNeedRepositionDevice:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNeedRepositionDevice:Z

    invoke-virtual {v0, v1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setNeedRepositionDevice(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setNeedRepositionDevice(Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->refreshChargingInfo(Z)V

    return-void
.end method

.method private setPreviewButtonClicked()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "prfe_key_preview_button_clicked"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private startParentViewFadeOutAnimation()V
    .locals 6

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getAlpha()F

    move-result v4

    const/4 v5, 0x0

    aput v4, v3, v5

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput v4, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$31;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$31;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method private startViewBounceAnimation(Landroid/view/View;IZ)V
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    if-eqz p3, :cond_0

    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    :goto_0
    new-array v3, v9, [F

    neg-int v4, p2

    int-to-float v4, v4

    aput v4, v3, v8

    aput v7, v3, v6

    invoke-static {p1, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v4, 0x190

    invoke-virtual {v1, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v3, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v3}, Landroid/view/animation/BounceInterpolator;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v3, Lcom/android/keyguard/MiuiDefaultLockScreen$34;

    invoke-direct {v3, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$34;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {v1, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-array v3, v9, [F

    aput v7, v3, v8

    neg-int v4, p2

    int-to-float v4, v4

    aput v4, v3, v6

    invoke-static {p1, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v4, 0x96

    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v3, Lcom/android/keyguard/MiuiDefaultLockScreen$35;

    invoke-direct {v3, p0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$35;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/animation/Animator;)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iput-boolean v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    return-void

    :cond_0
    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    goto :goto_0
.end method

.method private startViewBounceDownAnimation(Landroid/view/View;IZ)V
    .locals 6

    const/4 v5, 0x1

    if-eqz p3, :cond_0

    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    :goto_0
    const/4 v2, 0x2

    new-array v2, v2, [F

    neg-int v3, p2

    int-to-float v3, v3

    const/4 v4, 0x0

    aput v3, v2, v4

    const/4 v3, 0x0

    aput v3, v2, v5

    invoke-static {p1, v0, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v2, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v2}, Landroid/view/animation/BounceInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v2, Lcom/android/keyguard/MiuiDefaultLockScreen$33;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$33;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iput-boolean v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    return-void

    :cond_0
    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    goto :goto_0
.end method

.method private switchToLockScreenWallPaper(Z)V
    .locals 12

    const-wide/16 v10, 0x1f4

    const/4 v9, 0x2

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->cancel()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->cancelSwitchAnimate()V

    if-eqz p1, :cond_2

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;

    move-result-object v6

    const-string/jumbo v7, "alpha"

    new-array v8, v9, [F

    fill-array-data v8, :array_0

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v6, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v6

    const-string/jumbo v7, "alpha"

    new-array v8, v9, [F

    fill-array-data v8, :array_1

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v6, Lcom/android/keyguard/Ease$Quint;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get15(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/LoadingContainer;

    move-result-object v6

    const-string/jumbo v7, "alpha"

    new-array v8, v9, [F

    fill-array-data v8, :array_2

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v6, Lcom/android/keyguard/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v3, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v6, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :goto_0
    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    new-instance v7, Lcom/android/keyguard/MiuiDefaultLockScreen$28;

    invoke-direct {v7, p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$28;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)V

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeResetClockCallbacks()V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->postDelayedResetClock()V

    :cond_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v6, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v6

    const-string/jumbo v7, "alpha"

    new-array v8, v9, [F

    fill-array-data v8, :array_3

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v6, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get15(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/LoadingContainer;

    move-result-object v6

    const-string/jumbo v7, "alpha"

    new-array v8, v9, [F

    fill-array-data v8, :array_4

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v6, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;

    move-result-object v6

    const-string/jumbo v7, "alpha"

    new-array v8, v9, [F

    fill-array-data v8, :array_5

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v6, Lcom/android/keyguard/Ease$Quint;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v5, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSwitchAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_0

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private triggerLeftAction()V
    .locals 12

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    aput v5, v4, v8

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v5

    int-to-float v5, v5

    aput v5, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    const-string/jumbo v3, "alpha"

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    sub-float v5, v11, v5

    aput v5, v4, v8

    aput v10, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getTranslationX()F

    move-result v5

    aput v5, v4, v8

    aput v10, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v9, [F

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v5

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    aput v5, v4, v8

    aput v11, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v2, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v2}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v2

    new-instance v3, Lcom/android/keyguard/MiuiDefaultLockScreen$24;

    invoke-direct {v3, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$24;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {v2, v3}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->runOnFaceUnlockWorkerThread(Ljava/lang/Runnable;)V

    iput-boolean v7, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    return-void
.end method

.method private triggerRightAction()V
    .locals 9

    sget-object v4, Lcom/android/keyguard/AnalyticsHelper;->KEY_CAMERA_FROM_KEYGUARD:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v4}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->startFaceUnlock()V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v6, 0x2

    new-array v6, v6, [F

    iget-object v7, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getTranslationX()F

    move-result v7

    const/4 v8, 0x0

    aput v7, v6, v8

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v7

    neg-int v7, v7

    int-to-float v7, v7

    const/4 v8, 0x1

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftMoveView:Landroid/view/View;

    const-string/jumbo v5, "alpha"

    const/4 v6, 0x2

    new-array v6, v6, [F

    iget-object v7, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v7

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    const/4 v8, 0x0

    aput v7, v6, v8

    const/4 v7, 0x0

    const/4 v8, 0x1

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v6, 0x2

    new-array v6, v6, [F

    iget-object v7, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v7

    const/4 v8, 0x0

    aput v7, v6, v8

    const/4 v7, 0x0

    const/4 v8, 0x1

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    const-string/jumbo v5, "alpha"

    const/4 v6, 0x2

    new-array v6, v6, [F

    iget-object v7, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v7

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float v7, v8, v7

    const/4 v8, 0x0

    aput v7, v6, v8

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x1

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    const-wide/16 v4, 0x64

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v4, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v4}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v6, 0x2

    new-array v6, v6, [F

    iget-object v7, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getTranslationX()F

    move-result v7

    const/4 v8, 0x0

    aput v7, v6, v8

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v7

    neg-int v7, v7

    int-to-float v7, v7

    const/4 v8, 0x1

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-wide/16 v4, 0x64

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v4}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->start()V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSecureCameraByDpm:Z

    if-nez v4, :cond_1

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v6, 0x2

    new-array v6, v6, [F

    iget-object v7, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getTranslationX()F

    move-result v7

    const/4 v8, 0x0

    aput v7, v6, v8

    const/4 v7, 0x0

    const/4 v8, 0x1

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v4}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v2, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v4, Lcom/android/keyguard/MiuiDefaultLockScreen$23;

    invoke-direct {v4, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$23;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {v2, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    invoke-static {}, Lcom/android/keyguard/MiuiDefaultLockScreen;->hideLockScreenInActivityManager()V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightIntent:Landroid/content/Intent;

    sget-object v6, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v4}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    goto :goto_0
.end method

.method private unregisterLightAndAccSensor()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsPsensorDisabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLightAndAccSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    :cond_0
    return-void
.end method

.method private unregisterPSensorForGesture()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapperForGestureDetector:Lmiui/util/ProximitySensorWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapperForGestureDetector:Lmiui/util/ProximitySensorWrapper;

    invoke-virtual {v0}, Lmiui/util/ProximitySensorWrapper;->unregisterAllListeners()V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorWrapperForGestureDetector:Lmiui/util/ProximitySensorWrapper;

    :cond_0
    return-void
.end method

.method private updateGaussViewAlpha()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDisplayMusicController:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setGaussViewAlphaFromLockScreen(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setGaussViewAlphaFromLockScreen(F)V

    goto :goto_0
.end method

.method private updatePreviewConfig()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$20;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$20;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$20;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private updateStatusBarColormode()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->isTopLightColorMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateStatusBarColorMode(Z)V

    :cond_0
    return-void
.end method

.method private updateWakeupForNotification()V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "wakeup_for_keyguard_notification"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWakeupForNotification:Z

    return-void
.end method

.method private wakeupScreenByNotification()V
    .locals 2

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sWakeupByNotification:Z

    const-string/jumbo v0, "keyguard_screenon_notification"

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(Ljava/lang/String;Landroid/content/Context;)V

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SCREENON_BY_NOTIFICATION:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public cleanUp()V
    .locals 0

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->isFullScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->isChargingAnimationInDeclining()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public getDefaultUnlockHintText()Ljava/lang/String;
    .locals 3

    sget-boolean v1, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSignalAvailable:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPluggedIn:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mChargingHintText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mChargingHintText:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public initKeyguardShortcut()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSecureCameraByDpm:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUtils;->getCameraIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightIntent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForN;->isUserUnlocked(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightActionAvailable:Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForN;->isUserUnlocked(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "is_pad"

    invoke-static {v1, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->isSupportRightMove()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    :cond_0
    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftActionAvailable:Z

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftActionAvailable:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLeftActionLabel:Ljava/lang/CharSequence;

    :cond_1
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightActionAvailable:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mRightActionLabel:Ljava/lang/CharSequence;

    :cond_2
    return-void
.end method

.method public isBottomLightColorMode()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isLightWallpaperBottom()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDisplayMusicController:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTopLightColorMode()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isLightWallpaperTop()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDisplayMusicController:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public needsInput()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 6

    const/4 v5, -0x1

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    const-string/jumbo v1, "is_small_window"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "lock_wallpaper_provider_authority"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockWallpaperProviderObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "wakeup_for_keyguard_notification"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWakeupForNotificationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->registerListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updateGaussViewAlpha()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updateStatusBarColormode()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWirelessChargeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerWirelessChargeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;)V

    sget-boolean v1, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$21;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$21;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPhoneSignalChangeCallback:Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPhoneSignalChangeCallback:Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerPhoneSignalChangeCallback(Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerRemoteController()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMusicStateChangeListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerMusicStateChangeListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerWallpaperChangeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;)V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string/jumbo v0, "is_pad"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResumed:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->resetView()V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mCameraView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->setCameraImage()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMusicStateChangeListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->unregisterMusicStateChangeListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockWallpaperProviderObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWakeupForNotificationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPhoneSignalChangeCallback:Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWirelessChargeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->unregisterWirelessChargeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->unregisterListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardMusicController:Lcom/android/keyguard/KeyguardMusicController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardMusicController:Lcom/android/keyguard/KeyguardMusicController;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardMusicController;->clearMamlResource()V

    :cond_1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onFaceUnlockFailed()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mPluggedIn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/UnlockHintSwitcher;->reset()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get9(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public onFaceUnlockHelp(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/keyguard/UnlockHintSwitcher;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get9(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onFaceUnlockStart()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get9(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public onFaceUnlockSuccess()V
    .locals 0

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChanging:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInScrolling:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    if-eqz v8, :cond_1

    :cond_0
    iput-boolean v11, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIgnoreTouchEventSequence:Z

    const-string/jumbo v8, "miui_keyguard"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "mWallpaperChanging="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChanging:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";mInAnimating="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";mInScrolling="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInScrolling:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";mStartingRightAction="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";mInSmartCoverSmallWindowMode="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v11

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_3

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorTooClose:Z

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v8}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    :cond_2
    iput-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIgnoreTouchEventSequence:Z

    iput-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsTouchEventInNotificationViews:Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    iput v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v8

    iput v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    iget v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    iput v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDirectionCheckPointY:F

    iget v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    iput v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDirectionCheckPointX:F

    iput-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHorizontalDragging:Z

    iput-boolean v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVerticalDragging:Z

    iput v10, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v8}, Landroid/view/VelocityTracker;->clear()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialDownTime:J

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Ljava/lang/Runnable;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get18(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/keyguard/UnlockHintSwitcher;->clearAnimation()V

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get18(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/android/keyguard/UnlockHintSwitcher;->setVisibility(I)V

    :cond_3
    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildCount()I

    move-result v8

    if-lez v8, :cond_4

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildCount()I

    move-result v3

    if-lez v3, :cond_4

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v8

    int-to-float v1, v8

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v8

    int-to-float v4, v8

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v8

    int-to-float v5, v8

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v8

    add-int/lit8 v9, v3, -0x1

    invoke-virtual {v8, v9}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    move-result v8

    int-to-float v0, v8

    const/4 v8, 0x2

    new-array v2, v8, [I

    iget-object v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v8}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    aget v9, v2, v10

    int-to-float v9, v9

    sub-float v6, v8, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    aget v9, v2, v11

    int-to-float v9, v9

    sub-float v7, v8, v9

    cmpl-float v8, v6, v1

    if-ltz v8, :cond_4

    cmpg-float v8, v6, v4

    if-gtz v8, :cond_4

    cmpg-float v8, v7, v0

    if-gtz v8, :cond_4

    cmpl-float v8, v7, v5

    if-ltz v8, :cond_4

    iput-boolean v11, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsTouchEventInNotificationViews:Z

    return v10

    :cond_4
    iget-boolean v8, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIsTouchEventInNotificationViews:Z

    if-nez v8, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v8

    iget v9, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget-object v9, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mViewConfiguration:Landroid/view/ViewConfiguration;

    invoke-virtual {v9}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-gez v8, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    iget v9, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget-object v9, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mViewConfiguration:Landroid/view/ViewConfiguration;

    invoke-virtual {v9}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_6

    :cond_5
    return v11

    :cond_6
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    return v8
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    :cond_0
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->startFaceUnlock()V

    :cond_1
    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->reverseSlideAction()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    :cond_2
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :cond_3
    const/16 v0, 0x52

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeResetClockCallbacks()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->cancelSwitchAnimate()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->unregisterPSensorForGesture()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->unregisterLightAndAccSensor()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerprintHardwareAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mFingerprintCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->unregisterFingerprintStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;)V

    :cond_0
    iput-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResumed:Z

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->resetView()V

    return-void
.end method

.method public onResume(Z)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->isInSmallWindowMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->changeLayouts()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->reverseSlideAction()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->onResume()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mMiuiKeyguardMoveLeftView:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getScreenWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setTranslationX(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get9(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updateStatusBarColormode()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->updateGaussViewAlpha()V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInLiteMode:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLastTimeOfScreenOnAnimation:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLastTimeOfScreenOnAnimation:J

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap2(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    :cond_0
    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mProximitySensorTooClose:Z

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$22;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$22;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerprintHardwareAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mKeyguardUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mFingerprintCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerFingerprintStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDisplayMusicController:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/keyguard/AnalyticsHelper;->trackMusicShowCount()V

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_CHARGING_SHOW:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    :cond_3
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showTryAgainMessageInDefault()Z

    move-result v0

    if-nez v0, :cond_4

    xor-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get18(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/keyguard/UnlockHintSwitcher;->setVisibility(I)V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showTryAgainMessageInDefault()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mResumed:Z

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x3

    const/4 v3, 0x2

    const/4 v6, 0x1

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChanging:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInScrolling:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIgnoreTouchEventSequence:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    if-eqz v2, :cond_1

    :cond_0
    const-string/jumbo v2, "miui_keyguard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mWallpaperChanging="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChanging:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";mInAnimating="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInAnimating:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";mInScrolling="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInScrolling:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";mIgnoreTouchEventSequence="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mIgnoreTouchEventSequence:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";mStartingRightAction="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";mInSmartCoverSmallWindowMode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInSmartCoverSmallWindowMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    :cond_1
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInPocket:Z

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v7, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v6, :cond_3

    :cond_2
    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInPocket:Z

    :cond_3
    return v6

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v6, :cond_5

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->enterPocketMode()V

    return v6

    :cond_5
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mViewConfiguration:Landroid/view/ViewConfiguration;

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_e

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDirectionCheckPointX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDirectionCheckPointY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_9

    :cond_6
    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDirectionCheckPointX:F

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDirectionCheckPointY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->calculateDirection(FFFF)I

    move-result v0

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    if-nez v2, :cond_c

    iput v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    if-eq v2, v7, :cond_7

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    :cond_7
    iput-boolean v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVerticalDragging:Z

    :cond_8
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iput v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDirectionCheckPointX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iput v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mDirectionCheckPointY:F

    :cond_9
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVerticalDragging:Z

    if-eqz v2, :cond_d

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleVerticalDragging(Landroid/view/MotionEvent;)V

    :cond_a
    :goto_1
    return v6

    :cond_b
    iput-boolean v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHorizontalDragging:Z

    goto :goto_0

    :cond_c
    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialMoveDirection:I

    if-eq v2, v0, :cond_8

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_8

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->isGlobalWallPaperDesDragDownMode()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_8

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->enterPocketMode()V

    return v6

    :cond_d
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHorizontalDragging:Z

    if-eqz v2, :cond_a

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleHorizontalDragging(Landroid/view/MotionEvent;)V

    goto :goto_1

    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v6, :cond_f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v7, :cond_a

    :cond_f
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mVerticalDragging:Z

    if-eqz v2, :cond_10

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleVerticalDraggingUpEvent(Landroid/view/MotionEvent;)V

    goto :goto_1

    :cond_10
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHorizontalDragging:Z

    if-eqz v2, :cond_11

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleHorizontalDraggingUpEvent(Landroid/view/MotionEvent;)V

    goto :goto_1

    :cond_11
    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    int-to-float v3, v1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_a

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mInitialTouchY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    int-to-float v3, v1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_a

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v6, :cond_a

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->handleSingleClickEvent()V

    goto :goto_1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4

    const-wide/16 v2, 0x64

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$25;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$25;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$26;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$26;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->reverseSlideAction()V

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingRightAction:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mStartingLeftAction:Z

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mWallpaperChanging:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap2(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    goto :goto_0
.end method

.method public setHidden(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mHidden:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->unregisterPSensorForGesture()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->unregisterLightAndAccSensor()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->registerPSensorForGesture()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->registerLightAndAccSensor()V

    goto :goto_0
.end method

.method public startInAnimation()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mLockView:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen;->mContext:Landroid/content/Context;

    const/high16 v2, 0x10a0000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
