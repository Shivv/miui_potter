.class Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;
.super Ljava/lang/Object;
.source "MiuiSimPINUnlockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->onSimLockChangedResponse(Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

.field final synthetic val$result:Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iput-object p2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->val$result:Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->hideProgressDialog()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->val$result:Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    iget-boolean v0, v0, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->unlockSuccess:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v1, v1, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget v1, v1, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimId:I

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->reportSimUnlocked(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->val$result:Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    iget v0, v0, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->attemptsRemaining:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v1, v1, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->val$result:Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    iget v2, v2, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->attemptsRemaining:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->val$result:Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    iget v4, v4, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->attemptsRemaining:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const v4, 0x7f0e0001

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->displayErrorMsg(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4$1;->this$1:Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    const v1, 0x7f0b0016

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->displayErrorMsg(I)V

    goto :goto_1
.end method
