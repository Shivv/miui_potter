.class abstract Lcom/android/keyguard/MiuiCommonUnlockScreen;
.super Landroid/widget/RelativeLayout;
.source "MiuiCommonUnlockScreen.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardScreen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiCommonUnlockScreen$1;,
        Lcom/android/keyguard/MiuiCommonUnlockScreen$2;,
        Lcom/android/keyguard/MiuiCommonUnlockScreen$3;,
        Lcom/android/keyguard/MiuiCommonUnlockScreen$4;,
        Lcom/android/keyguard/MiuiCommonUnlockScreen$5;
    }
.end annotation


# static fields
.field private static final PREF_SAVE_PASSWORD_TIME_OUT:Ljava/lang/String; = "pref_password_time_out"

.field private static final PREF_SAVE_PASSWORD_TIME_OUT_VALUE:Ljava/lang/String; = "pref_password_time_out_value"

.field private static final TAG:Ljava/lang/String; = "MiuiCommonUnlockScreen"

.field protected static final VIBRATE_DURATION:I = 0x96


# instance fields
.field private final MAX_SHAKE_TIMES:I

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field protected mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field protected mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

.field protected final mEmergencyCallListener:Landroid/view/View$OnClickListener;

.field protected final mEmergencyCallLongClickListener:Landroid/view/View$OnLongClickListener;

.field protected mFaceUnlockIcon:Landroid/widget/ImageView;

.field private final mFingerprintCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

.field private mFingerprintHintMsg:Landroid/widget/TextView;

.field private mFingerprintHintTitle:Landroid/widget/TextView;

.field private mFingerprintLockout:Z

.field private mFogetPasswordMethod:Landroid/view/View;

.field private mFogetPasswordSuggestion:Landroid/view/View;

.field private mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

.field protected mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field protected mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

.field protected mLockoutView:Landroid/view/View;

.field private mNormalUnlockLayout:Landroid/widget/LinearLayout;

.field protected mPendingLockCheck:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation
.end field

.field private mPhoneSignalChangeCallback:Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

.field private mScreenOrientation:I

.field private mShakeDistance:I

.field private mShakeDuration:I

.field private mShakeTimes:I

.field protected final mShowLockBeforeUnlock:Z

.field private final mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

.field protected mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field private final mVibrator:Landroid/os/Vibrator;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordSuggestion:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Lcom/android/keyguard/MiuiKeyguardViewMediator;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiCommonUnlockScreen;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeTimes:I

    return v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/os/Vibrator;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiCommonUnlockScreen;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintLockout:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiCommonUnlockScreen;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isSecondFingerprint(I)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->notSimUnlockScreen()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiCommonUnlockScreen;J)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->applyFingerHintAnimation(J)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->resetAnimValue()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/keyguard/MiuiCommonUnlockScreen;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->setLockoutViewVisible(I)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/keyguard/MiuiCommonUnlockScreen;Landroid/widget/TextView;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->updateCountDown(Landroid/widget/TextView;J)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintLockout:Z

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$1;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$2;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mEmergencyCallLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$3;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$3;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mEmergencyCallListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$4;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$4;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    const/16 v0, 0x19

    iput v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDuration:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->MAX_SHAKE_TIMES:I

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    iput-object p4, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iput-object p7, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iput-object p5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iput-object p3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iput-boolean p6, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShowLockBeforeUnlock:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mScreenOrientation:I

    new-instance v0, Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v0, v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;-><init>(Lcom/android/internal/widget/LockPatternUtils;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 8

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/android/keyguard/MiuiCommonUnlockScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    return-void
.end method

.method private applyFingerHintAnimation(J)V
    .locals 13

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x2

    iget-object v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    iget v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeTimes:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeTimes:I

    iget v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDistance:I

    iget v6, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDistance:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDistance:I

    iget-object v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const-string/jumbo v6, "X"

    new-array v7, v9, [F

    aput v4, v7, v10

    iget v8, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDistance:I

    int-to-float v8, v8

    add-float/2addr v8, v4

    aput v8, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v5, Lmiui/view/animation/SineEaseOutInterpolator;

    invoke-direct {v5}, Lmiui/view/animation/SineEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDuration:I

    int-to-long v6, v5

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const-string/jumbo v6, "X"

    new-array v7, v9, [F

    iget v8, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDistance:I

    int-to-float v8, v8

    add-float/2addr v8, v4

    aput v8, v7, v10

    iget v8, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDistance:I

    int-to-float v8, v8

    sub-float v8, v4, v8

    aput v8, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v5, Lmiui/view/animation/SineEaseInOutInterpolator;

    invoke-direct {v5}, Lmiui/view/animation/SineEaseInOutInterpolator;-><init>()V

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDuration:I

    mul-int/lit8 v5, v5, 0x2

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const-string/jumbo v6, "X"

    new-array v7, v9, [F

    iget v8, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDistance:I

    int-to-float v8, v8

    sub-float v8, v4, v8

    aput v8, v7, v10

    aput v4, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iget v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeTimes:I

    if-ne v5, v9, :cond_1

    new-instance v5, Lmiui/view/animation/SineEaseOutInterpolator;

    invoke-direct {v5}, Lmiui/view/animation/SineEaseOutInterpolator;-><init>()V

    :goto_0
    invoke-virtual {v2, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget v5, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDuration:I

    int-to-long v6, v5

    invoke-virtual {v2, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/Animator;

    aput-object v0, v5, v10

    aput-object v1, v5, v11

    aput-object v2, v5, v9

    invoke-virtual {v3, v5}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    new-instance v5, Lcom/android/keyguard/MiuiCommonUnlockScreen$20;

    invoke-direct {v5, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$20;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v3, v5}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v3, p1, p2}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    return-void

    :cond_1
    new-instance v5, Lmiui/view/animation/SineEaseInInterpolator;

    invoke-direct {v5}, Lmiui/view/animation/SineEaseInInterpolator;-><init>()V

    goto :goto_0
.end method

.method public static canSwitchUser(Landroid/content/Context;)Z
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lmiui/util/OldmanUtil;->IS_ELDER_MODE:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MiuiCommonUnlockScreen"

    const-string/jumbo v1, "Can\'t switch user when in elder mode."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_0
    invoke-static {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isGreenKidActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "MiuiCommonUnlockScreen"

    const-string/jumbo v1, "Can\'t switch user when green kid active."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_1
    invoke-static {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isPhoneCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "MiuiCommonUnlockScreen"

    const-string/jumbo v1, "Can\'t switch user when phone calling."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method private initView()V
    .locals 19

    new-instance v13, Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-direct {v13, v14}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v15, -0x1

    const/16 v16, -0x1

    invoke-direct/range {v14 .. v16}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    const v15, 0x7f030008

    invoke-static {v13, v15, v14}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    const v14, 0x7f0d002e

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFaceUnlockIcon:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    const v14, 0x7f0d002b

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d0045

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextSwitcher;

    invoke-virtual {v4}, Landroid/widget/TextSwitcher;->removeAllViews()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v4, v13}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d0046

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextSwitcher;

    invoke-virtual {v10}, Landroid/widget/TextSwitcher;->removeAllViews()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v10, v13}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d0048

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextSwitcher;

    invoke-virtual {v12}, Landroid/widget/TextSwitcher;->removeAllViews()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v12, v13}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d0049

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextSwitcher;

    invoke-virtual {v6}, Landroid/widget/TextSwitcher;->removeAllViews()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v6, v13}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d0047

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0057

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    int-to-float v13, v13

    const/4 v14, 0x0

    invoke-virtual {v3, v14, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d004a

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0004

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    int-to-float v13, v13

    const/4 v14, 0x0

    invoke-virtual {v2, v14, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d004b

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0004

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    int-to-float v13, v13

    const/4 v14, 0x0

    invoke-virtual {v1, v14, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d004d

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const v14, 0x7f0d004c

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0023

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    int-to-float v13, v13

    const/4 v14, 0x0

    invoke-virtual {v9, v14, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    const v14, 0x7f0d002c

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    const v14, 0x7f0d002d

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->addView(Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f03001e

    move-object/from16 v0, p0

    invoke-static {v13, v14, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v13, 0x7f0d0090

    invoke-virtual {v7, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v14, 0x7f0d0079

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->processEmergencyCall(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v14, 0x7f0d0093

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v14, 0x7f0d0094

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v14, 0x7f0d0098

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordSuggestion:Landroid/view/View;

    new-instance v13, Lcom/android/keyguard/MiuiCommonUnlockScreen$6;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$6;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    new-instance v14, Lcom/android/keyguard/MiuiCommonUnlockScreen$7;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$7;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    const v14, 0x7f0d0097

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    new-instance v14, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    const v14, 0x7f0d0096

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    new-instance v14, Lcom/android/keyguard/MiuiCommonUnlockScreen$9;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$9;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordSuggestion:Landroid/view/View;

    new-instance v14, Lcom/android/keyguard/MiuiCommonUnlockScreen$10;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$10;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordSuggestion:Landroid/view/View;

    const v14, 0x7f0d009b

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    new-instance v14, Lcom/android/keyguard/MiuiCommonUnlockScreen$11;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$11;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->clearAnimation()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    sget-object v13, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v14, "clover"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v13, v13, Landroid/content/res/Configuration;->orientation:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0006

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    iput v13, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->notSimUnlockScreen()Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const v14, 0x7f0b0040

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isPatternUnlockScren()Z

    move-result v13

    if-eqz v13, :cond_6

    const v13, 0x7f0b003f

    :goto_2
    invoke-virtual {v14, v13}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->hasNavigationBar()Z

    move-result v13

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v14}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v15}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/widget/LinearLayout;->getPaddingEnd()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x10500f8

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    invoke-virtual/range {v13 .. v17}, Landroid/widget/LinearLayout;->setPaddingRelative(IIII)V

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->resetAnimValue()V

    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShowLockBeforeUnlock:Z

    if-nez v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c004f

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    iput v13, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c004f

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    iput v13, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShowLockBeforeUnlock:Z

    if-nez v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->setVisibility(I)V

    goto/16 :goto_1

    :cond_6
    const v13, 0x7f0b003e

    goto/16 :goto_2
.end method

.method protected static isFBEDevice()Z
    .locals 4

    const/4 v2, 0x0

    const-string/jumbo v3, "ro.crypto.state"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "ro.crypto.type"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    return v2

    :cond_1
    const-string/jumbo v3, "encrypted"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    :cond_2
    return v2
.end method

.method protected static isGreenKidActive(Landroid/content/Context;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->isGreenKidActive(Landroid/content/ContentResolver;)Z

    move-result v0

    return v0
.end method

.method protected static isNeedCloseSdcardFs(Landroid/content/Context;I)Z
    .locals 5

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v4, 0x0

    invoke-static {}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isSDdcardFsEnable()Z

    move-result v2

    if-nez v2, :cond_0

    return v4

    :cond_0
    const-string/jumbo v2, "user"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    invoke-virtual {v1, p1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v2, v0, Landroid/content/pm/UserInfo;->flags:I

    and-int/2addr v2, v3

    if-eq v2, v3, :cond_2

    :cond_1
    const-string/jumbo v2, "MiuiCommonUnlockScreen"

    const-string/jumbo v3, "User is not a encrypted space"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v4

    :cond_2
    const-string/jumbo v2, "MiuiCommonUnlockScreen"

    const-string/jumbo v3, "Need close sdcardfs"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    return v2
.end method

.method private isPatternUnlockScren()Z
    .locals 1

    instance-of v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen;

    return v0
.end method

.method protected static isPhoneCalling(Landroid/content/Context;)Z
    .locals 4

    const/4 v2, 0x1

    const-string/jumbo v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eq v0, v2, :cond_0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isSDdcardFsEnable()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string/jumbo v0, "force_on"

    const-string/jumbo v1, "persist.sys.sdcardfs"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v3

    :cond_0
    const-string/jumbo v0, "force_off"

    const-string/jumbo v1, "persist.sys.sdcardfs"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    :cond_1
    const-string/jumbo v0, "1"

    const-string/jumbo v1, "ro.sys.sdcardfs"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v3

    :cond_2
    return v2
.end method

.method private isSecondFingerprint(I)Z
    .locals 6

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "second_user_id"

    const/16 v5, -0x2710

    invoke-static {v3, v4, v5, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Landroid/security/FingerprintIdUtils;->getUserFingerprintIds(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    :cond_0
    return v2
.end method

.method private notSimUnlockScreen()Z
    .locals 1

    instance-of v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetAnimValue()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeTimes:I

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShakeDistance:I

    return-void
.end method

.method private setLockoutViewVisible(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v1, 0x7f0d0091

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v1, 0x7f0d0092

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v1, 0x7f0d0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v1, 0x7f0d0093

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private updateCountDown(Landroid/widget/TextView;J)V
    .locals 8

    const-wide/16 v4, 0x3c

    const/4 v2, 0x1

    const/4 v6, 0x0

    cmp-long v0, p2, v4

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    long-to-int v1, p2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    const v3, 0x7f0e0002

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    long-to-int v1, p2

    div-int/lit8 v1, v1, 0x3c

    new-array v2, v2, [Ljava/lang/Object;

    div-long v4, p2, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    const v3, 0x7f0e0003

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected addUnlockView(Landroid/view/View;)V
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x50

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mNormalUnlockLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method protected applyAnimation()V
    .locals 2

    const-wide/16 v0, 0x1f4

    invoke-direct {p0, v0, v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->applyFingerHintAnimation(J)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->playLockoutViewAnim()V

    return-void
.end method

.method protected asyncHideLockViewOnResume()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordSuggestion:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordSuggestion:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->setLockoutViewVisible(I)V

    return v2

    :cond_1
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mShowLockBeforeUnlock:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToLockScreen()V

    return v2

    :cond_2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected getSecondSpaceId()I
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "second_user_id"

    const/16 v2, -0x2710

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method protected abstract handleAttemptLockout(J)V
.end method

.method public handleAttemptLockoutIfNeed()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$12;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$12;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiCommonUnlockScreen$12;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected handleFirstMatchSpacePassword()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const v1, 0x7f0b0041

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isPatternUnlockScren()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b003f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFaceUnlockIcon:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const v0, 0x7f0b003e

    goto :goto_0
.end method

.method protected handleNeedCloseSdcardFs()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const v1, 0x7f0b0044

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isPatternUnlockScren()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b003f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_0
    const v0, 0x7f0b003e

    goto :goto_0
.end method

.method protected handleSwitchUserWhenCalling()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const v1, 0x7f0b0042

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isPatternUnlockScren()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b003f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFaceUnlockIcon:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const v0, 0x7f0b003e

    goto :goto_0
.end method

.method protected handleSwitchUserWhenGreenkidActive()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const v1, 0x7f0b0043

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isPatternUnlockScren()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b003f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFaceUnlockIcon:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const v0, 0x7f0b003e

    goto :goto_0
.end method

.method protected hideLockoutView()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->hideLockoutView(Z)V

    return-void
.end method

.method protected hideLockoutView(Z)V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;

    invoke-direct {v0, p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;Z)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public needPasswordCheck(ZI)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x19

    if-ge v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v2

    if-ne p2, v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "miui_keyguard_ble_unlock_succeed"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$14;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$14;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mPhoneSignalChangeCallback:Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mPhoneSignalChangeCallback:Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerPhoneSignalChangeCallback(Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerprintHardwareAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->notSimUnlockScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerFingerprintStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;)V

    :cond_1
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    iget v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mScreenOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mScreenOrientation:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->removeAllViews()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->initView()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onScreenOrientationChanged()V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mPhoneSignalChangeCallback:Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerprintHardwareAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->notSimUnlockScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->unregisterFingerprintStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;)V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onFaceUnlockFailed()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFaceUnlockIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->shouldListenForFingerprint()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b0088

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_0
    const v0, 0x7f0b0089

    goto :goto_0
.end method

.method public onFaceUnlockHelp(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFaceUnlockIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public onFaceUnlockStart()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFaceUnlockIcon:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onFaceUnlockSuccess()V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->setPasswordEntryInputEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordSuggestion:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordMethod:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFogetPasswordSuggestion:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->setLockoutViewVisible(I)V

    :cond_2
    const-string/jumbo v0, "KeyguardDismissLoggerCallback"

    const-string/jumbo v1, "onDismissCancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected abstract onScreenOrientationChanged()V
.end method

.method protected playLockoutViewAnim()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public processEmergencyCall(Landroid/view/View;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isPhoneAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mEmergencyCallListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isIndianBuild()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mEmergencyCallLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public resetFingerprintLockoutText()V
    .locals 2

    const/4 v1, 0x4

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintLockout:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintHintMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFingerprintLockout:Z

    :cond_0
    return-void
.end method

.method public setHidden(Z)V
    .locals 0

    return-void
.end method

.method protected abstract setPasswordEntryInputEnabled(Z)V
.end method

.method public setVisibility(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void
.end method

.method public showBLEUnlockSucceedView()V
    .locals 6

    const/16 v5, 0x400

    const/4 v4, 0x0

    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1030011

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    const v2, 0x7f03000a

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v5, v5}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x7f080013

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x7f0f0003

    invoke-virtual {v1, v2}, Landroid/view/Window;->setWindowAnimations(I)V

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Lcom/android/keyguard/MiuiCommonUnlockScreen$18;

    invoke-direct {v1, p0, v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$18;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;Landroid/app/Dialog;)V

    const-wide/16 v2, 0x258

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1, v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setShowBLEUnlockAnimation(Z)V

    return-void
.end method

.method protected showLockoutView(J)V
    .locals 9

    const-wide/16 v4, 0x3e8

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setPasswordLockout(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const v1, 0x7f0d0092

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v7, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {v7, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$15;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$15;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-direct {p0, v6, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->updateCountDown(Landroid/widget/TextView;J)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->stopFingerprintIdentify()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v0

    new-instance v1, Lcom/android/keyguard/MiuiCommonUnlockScreen$16;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$16;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->runOnFaceUnlockWorkerThread(Ljava/lang/Runnable;)V

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;

    mul-long v2, p1, v4

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;JJLandroid/widget/TextView;)V

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->start()Landroid/os/CountDownTimer;

    return-void
.end method

.method public final startAnimation()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->applyAnimation()V

    return-void
.end method

.method protected switchUser(I)V
    .locals 1

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    if-eq v0, p1, :cond_0

    invoke-static {p1}, Lmiui/securityspace/SecuritySpaceEcryptManager;->spaceSwitchUser(I)Z

    :cond_0
    return-void
.end method

.method protected abstract updateEmergencyCallButtonVisibility(Z)V
.end method

.method public writePasswordOutTime(J)V
    .locals 5

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "pref_password_time_out"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pref_password_time_out_value"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
