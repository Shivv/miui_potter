.class Lcom/android/keyguard/MiuiDefaultLockScreen$6;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public musicStateChange(Z)V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set2(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$6;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$6$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set2(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$6;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap11(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    goto :goto_0
.end method
