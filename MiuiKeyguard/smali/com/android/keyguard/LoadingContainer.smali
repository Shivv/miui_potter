.class public Lcom/android/keyguard/LoadingContainer;
.super Landroid/widget/FrameLayout;
.source "LoadingContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/LoadingContainer$1;,
        Lcom/android/keyguard/LoadingContainer$2;
    }
.end annotation


# static fields
.field private static final LOADING_DURATION:J = 0xfaL

.field private static final LOADING_ITEM_ALPHA_MAX:F = 0.9f

.field private static final LOADING_ITEM_ALPHA_MIN:F = 0.5f

.field private static final LOADING_ITEM_COUNT:I = 0x4

.field public static final MODE_CUSTOM:I = 0x2

.field public static final MODE_MAGAZINE:I = 0x3

.field public static final MODE_NORMAL:I = 0x1

.field private static final PREPARE_DURATION:J = 0xfaL

.field private static final SCALE_FACTOR:F = 0.6f


# instance fields
.field private mCurrentMode:I

.field private mLoadingText:Landroid/widget/TextView;

.field private mLoadingTextContainer:Landroid/view/View;

.field private mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

.field private mLoadingView:Landroid/widget/FrameLayout;

.field private mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field private mNextMode:I

.field mResetTextRunnable:Ljava/lang/Runnable;

.field private mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

.field private final mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/LoadingContainer;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextContainer:Landroid/view/View;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/keyguard/LoadingContainer$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/LoadingContainer$1;-><init>(Lcom/android/keyguard/LoadingContainer;)V

    iput-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;

    iput v1, p0, Lcom/android/keyguard/LoadingContainer;->mCurrentMode:I

    iput v1, p0, Lcom/android/keyguard/LoadingContainer;->mNextMode:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    new-instance v0, Lcom/android/keyguard/LoadingContainer$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/LoadingContainer$2;-><init>(Lcom/android/keyguard/LoadingContainer;)V

    iput-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mResetTextRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method private getItemAnimIn(Landroid/view/View;)Landroid/animation/ObjectAnimator;
    .locals 4

    const-string/jumbo v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    return-object v0

    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f666666    # 0.9f
    .end array-data
.end method

.method private getItemAnimOut(Landroid/view/View;)Landroid/animation/ObjectAnimator;
    .locals 4

    const-string/jumbo v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    return-object v0

    nop

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3f000000    # 0.5f
    .end array-data
.end method

.method private showLoadingAlphaAnim(Z)V
    .locals 10

    const-wide/16 v8, 0x1f4

    const-wide/16 v6, 0x96

    const/4 v4, 0x2

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingText:Landroid/widget/TextView;

    const-string/jumbo v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextContainer:Landroid/view/View;

    const-string/jumbo v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    new-instance v1, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v1}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingText:Landroid/widget/TextView;

    const-string/jumbo v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_2

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextContainer:Landroid/view/View;

    const-string/jumbo v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_3

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    sget-object v1, Lcom/android/keyguard/Ease$Sine;->easeOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/android/keyguard/LoadingContainer$3;

    invoke-direct {v1, p0}, Lcom/android/keyguard/LoadingContainer$3;-><init>(Lcom/android/keyguard/LoadingContainer;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private startLoadingTextExpandAnim(Z)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/android/keyguard/LoadingContainer;->showLoadingAlphaAnim(Z)V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mResetTextRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/LoadingContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mResetTextRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/keyguard/LoadingContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public cancelLoadingTextExpandAnim()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextExpandAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_1
    return-void
.end method

.method public changeMode()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/android/keyguard/LoadingContainer;->mCurrentMode:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-boolean v0, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->isLockScreenMagazine:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    :goto_0
    iput v0, p0, Lcom/android/keyguard/LoadingContainer;->mNextMode:I

    :goto_1
    iget v0, p0, Lcom/android/keyguard/LoadingContainer;->mNextMode:I

    iget v1, p0, Lcom/android/keyguard/LoadingContainer;->mCurrentMode:I

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/LoadingContainer;->cancelLoadingTextExpandAnim()V

    iget v0, p0, Lcom/android/keyguard/LoadingContainer;->mNextMode:I

    packed-switch v0, :pswitch_data_0

    :goto_2
    iget v0, p0, Lcom/android/keyguard/LoadingContainer;->mNextMode:I

    iput v0, p0, Lcom/android/keyguard/LoadingContainer;->mCurrentMode:I

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/android/keyguard/LoadingContainer;->mNextMode:I

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/LoadingContainer;->startLoadingTextExpandAnim(Z)V

    goto :goto_2

    :pswitch_1
    invoke-direct {p0, v2}, Lcom/android/keyguard/LoadingContainer;->startLoadingTextExpandAnim(Z)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerWallpaperInfoListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->unregisterWallpaperInfoListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    const v0, 0x7f0d001d

    invoke-virtual {p0, v0}, Lcom/android/keyguard/LoadingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingText:Landroid/widget/TextView;

    const v0, 0x7f0d001c

    invoke-virtual {p0, v0}, Lcom/android/keyguard/LoadingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextContainer:Landroid/view/View;

    const v0, 0x7f0d001e

    invoke-virtual {p0, v0}, Lcom/android/keyguard/LoadingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingView:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingView:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextContainer:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public openAd()Z
    .locals 2

    iget v0, p0, Lcom/android/keyguard/LoadingContainer;->mCurrentMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->opendAd(Landroid/content/Context;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public performLayoutNow(Landroid/view/View;)V
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    :cond_0
    return-void
.end method

.method public refreshWallpaperInfo()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v2, p0, Lcom/android/keyguard/LoadingContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->getBtnText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/LoadingContainer;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    return-void
.end method

.method public updateColorByWallpaper(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingTextContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v0, p0, Lcom/android/keyguard/LoadingContainer;->mLoadingText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/LoadingContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
