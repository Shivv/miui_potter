.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;
.super Landroid/os/Handler;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap10(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    invoke-static {v1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap7(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;

    invoke-static {v1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap9(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleRingerModeChange(I)V

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handlePhoneStateChanged(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap8(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleDeviceProvisioned()V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleDevicePolicyManagerStateChanged()V

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleUserChanged(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
