.class Lcom/android/keyguard/NotificationItemAnimator$7;
.super Lcom/android/keyguard/NotificationItemAnimator$VpaListenerAdapter;
.source "NotificationItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/NotificationItemAnimator;->animateChangeImpl(Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/NotificationItemAnimator;

.field final synthetic val$changeInfo:Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;

.field final synthetic val$oldViewAnim:Landroid/support/v4/view/ViewPropertyAnimatorCompat;


# direct methods
.method constructor <init>(Lcom/android/keyguard/NotificationItemAnimator;Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V
    .locals 1

    iput-object p1, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    iput-object p2, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->val$changeInfo:Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;

    iput-object p3, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->val$oldViewAnim:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/NotificationItemAnimator$VpaListenerAdapter;-><init>(Lcom/android/keyguard/NotificationItemAnimator$VpaListenerAdapter;)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->val$oldViewAnim:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->setAlpha(Landroid/view/View;F)V

    invoke-static {p1, v2}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    invoke-static {p1, v2}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    iget-object v1, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->val$changeInfo:Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;

    iget-object v1, v1, Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/NotificationItemAnimator;->dispatchChangeFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    invoke-static {v0}, Lcom/android/keyguard/NotificationItemAnimator;->-get2(Lcom/android/keyguard/NotificationItemAnimator;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->val$changeInfo:Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;

    iget-object v1, v1, Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    invoke-static {v0}, Lcom/android/keyguard/NotificationItemAnimator;->-wrap3(Lcom/android/keyguard/NotificationItemAnimator;)V

    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    iget-object v1, p0, Lcom/android/keyguard/NotificationItemAnimator$7;->val$changeInfo:Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;

    iget-object v1, v1, Lcom/android/keyguard/NotificationItemAnimator$ChangeInfo;->oldHolder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/NotificationItemAnimator;->dispatchChangeStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    return-void
.end method
