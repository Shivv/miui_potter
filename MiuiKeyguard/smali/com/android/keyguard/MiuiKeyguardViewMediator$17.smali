.class Lcom/android/keyguard/MiuiKeyguardViewMediator$17;
.super Ljava/lang/Object;
.source "MiuiKeyguardViewMediator.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFingerprintStateChanged(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)V
    .locals 4

    const/16 v3, 0x1a

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/keyguard/Dependency;->SUPPORT_AOD:Z

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "keyguard_screenon_finger_firstboot"

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(Ljava/lang/String;Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->dismiss()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerPrintLockOut(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->dismiss()V

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/keyguard/doze/DozeHost;->isDozing()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "keyguard_screenon_finger_lockout_aod"

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(Ljava/lang/String;Landroid/content/Context;)V

    :cond_2
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->FAIL_ONE_TIME:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne p1, v0, :cond_3

    sput-boolean v2, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showTryAgainMessageInDefault:Z

    :cond_3
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->PASS:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne p1, v0, :cond_d

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v3, :cond_5

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/android/keyguard/Dependency;->SUPPORT_AOD:Z

    if-eqz v0, :cond_5

    :cond_4
    const-string/jumbo v0, "keyguard_screenon_finger_pass"

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(Ljava/lang/String;Landroid/content/Context;)V

    :cond_5
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFingerId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap3(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->canSwitchUser(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isUserAuthenticatedSinceBootSecond()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_9

    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_8

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/android/keyguard/Dependency;->SUPPORT_AOD:Z

    if-eqz v0, :cond_8

    :cond_7
    const-string/jumbo v0, "keyguard_screenon_finger_pass"

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(Ljava/lang/String;Landroid/content/Context;)V

    :cond_8
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->dismiss()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->stopFingerprintIdentify()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFingerprintIdentify()V

    return-void

    :cond_9
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set14(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get11(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardViewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewManager;->clearWindowExitAnimation()V

    :cond_a
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap9(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_c

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-boolean v0, Lcom/android/keyguard/Dependency;->SUPPORT_AOD:Z

    if-eqz v0, :cond_c

    :cond_b
    const-string/jumbo v0, "keyguard_screenon_finger_pass"

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(Ljava/lang/String;Landroid/content/Context;)V

    :cond_c
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->keyguardDone(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFingerId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->switchUser(I)V

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$17$1;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator$17;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->runOnWorkerThread(Ljava/lang/Runnable;J)V

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "unlock by fingerprint"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    return-void
.end method

.method public onHelp(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
