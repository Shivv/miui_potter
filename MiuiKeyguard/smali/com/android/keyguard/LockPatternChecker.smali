.class public final Lcom/android/keyguard/LockPatternChecker;
.super Ljava/lang/Object;
.source "LockPatternChecker.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static synthetic -wrap0(Lcom/android/keyguard/LockPatternUtilsWrapper;I)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/android/keyguard/LockPatternChecker;->isPasswordEnable(Lcom/android/keyguard/LockPatternUtilsWrapper;I)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/LockPatternUtilsWrapper;I)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/android/keyguard/LockPatternChecker;->isPatternPasswordEnable(Lcom/android/keyguard/LockPatternUtilsWrapper;I)Z

    move-result v0

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "miui_keyguard_password"

    sput-object v0, Lcom/android/keyguard/LockPatternChecker;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkPasswordForUsers(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/lang/String;Landroid/content/Context;Lcom/android/keyguard/OnCheckForUsersCallback;)Landroid/os/AsyncTask;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/keyguard/LockPatternUtilsWrapper;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/android/keyguard/OnCheckForUsersCallback;",
            ")",
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation

    new-instance v0, Lcom/android/keyguard/LockPatternChecker$2;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/android/keyguard/LockPatternChecker$2;-><init>(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/lang/String;Lcom/android/keyguard/OnCheckForUsersCallback;Landroid/content/Context;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-object v0
.end method

.method public static checkPatternForUsers(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/util/List;Landroid/content/Context;Lcom/android/keyguard/OnCheckForUsersCallback;)Landroid/os/AsyncTask;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/keyguard/LockPatternUtilsWrapper;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/android/keyguard/OnCheckForUsersCallback;",
            ")",
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation

    new-instance v0, Lcom/android/keyguard/LockPatternChecker$1;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/android/keyguard/LockPatternChecker$1;-><init>(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/util/List;Lcom/android/keyguard/OnCheckForUsersCallback;Landroid/content/Context;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-object v0
.end method

.method private static isPasswordEnable(Lcom/android/keyguard/LockPatternUtilsWrapper;I)Z
    .locals 5

    invoke-virtual {p0, p1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getActivePasswordQuality(I)I

    move-result v1

    const/high16 v2, 0x40000

    if-eq v1, v2, :cond_0

    const/high16 v2, 0x50000

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sget-object v3, Lcom/android/keyguard/LockPatternChecker;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "check password enable for userId : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_3

    const-string/jumbo v2, "   enable"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_1
    const/high16 v2, 0x60000

    if-eq v1, v2, :cond_0

    const/high16 v2, 0x20000

    if-eq v1, v2, :cond_0

    const/high16 v2, 0x30000

    if-ne v1, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-string/jumbo v2, "   disable"

    goto :goto_1
.end method

.method private static isPatternPasswordEnable(Lcom/android/keyguard/LockPatternUtilsWrapper;I)Z
    .locals 4

    invoke-virtual {p0, p1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getActivePasswordQuality(I)I

    move-result v1

    const/high16 v2, 0x10000

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lcom/android/keyguard/LockPatternChecker;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "check pattern password enable for userId : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_1

    const-string/jumbo v1, "   enable"

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "   disable"

    goto :goto_1
.end method
