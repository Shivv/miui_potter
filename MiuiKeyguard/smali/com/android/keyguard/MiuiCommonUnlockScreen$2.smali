.class Lcom/android/keyguard/MiuiCommonUnlockScreen$2;
.super Ljava/lang/Object;
.source "MiuiCommonUnlockScreen.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.CALL_EMERGENCY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "tel:112"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get0(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const/4 v1, 0x1

    return v1
.end method
