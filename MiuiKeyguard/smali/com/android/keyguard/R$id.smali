.class public final Lcom/android/keyguard/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final aod_bg:I = 0x7f0d0004

.field public static final aod_mode:I = 0x7f0d0002

.field public static final aod_scrim:I = 0x7f0d000b

.field public static final battery:I = 0x7f0d009c

.field public static final battery_charging_num:I = 0x7f0d001a

.field public static final battery_charging_view:I = 0x7f0d0019

.field public static final battery_level:I = 0x7f0d009d

.field public static final battery_text:I = 0x7f0d0018

.field public static final camera_shortcut_id:I = 0x7f0d0089

.field public static final camera_view:I = 0x7f0d0024

.field public static final camera_view_second:I = 0x7f0d0025

.field public static final charging_list_and_back_arrow_layout_id:I = 0x7f0d003c

.field public static final clock:I = 0x7f0d000d

.field public static final clock_container:I = 0x7f0d0003

.field public static final clock_hour:I = 0x7f0d0005

.field public static final clock_minute:I = 0x7f0d0006

.field public static final content:I = 0x7f0d0074

.field public static final current_date:I = 0x7f0d004a

.field public static final date:I = 0x7f0d0007

.field public static final date_and_battery_info:I = 0x7f0d0067

.field public static final device_manage_text:I = 0x7f0d008b

.field public static final digital_time_layout:I = 0x7f0d0044

.field public static final dot_digital:I = 0x7f0d0047

.field public static final emergencyCallButton:I = 0x7f0d0079

.field public static final face_unlock_icon:I = 0x7f0d002e

.field public static final first:I = 0x7f0d0008

.field public static final first_digital:I = 0x7f0d0045

.field public static final first_dot:I = 0x7f0d007c

.field public static final foget_password:I = 0x7f0d0093

.field public static final forget_password_hint_container:I = 0x7f0d0094

.field public static final forget_password_method_back:I = 0x7f0d0096

.field public static final forget_password_method_content:I = 0x7f0d0095

.field public static final forget_password_method_next:I = 0x7f0d0097

.field public static final forget_password_suggesstion:I = 0x7f0d0098

.field public static final forget_password_suggesstion_ok:I = 0x7f0d009b

.field public static final forget_password_suggesstion_one:I = 0x7f0d0099

.field public static final forget_password_suggesstion_two:I = 0x7f0d009a

.field public static final forth_dot:I = 0x7f0d007f

.field public static final fouth_digital:I = 0x7f0d0049

.field public static final gxzw_anim_textview:I = 0x7f0d004f

.field public static final gxzw_highlight:I = 0x7f0d0052

.field public static final gxzw_icon:I = 0x7f0d0050

.field public static final gxzw_lock:I = 0x7f0d0051

.field public static final gxzw_tip_view:I = 0x7f0d004e

.field public static final horizontal_dot:I = 0x7f0d0065

.field public static final horizontal_hour:I = 0x7f0d0064

.field public static final horizontal_min:I = 0x7f0d0066

.field public static final horizontal_time_layout:I = 0x7f0d0063

.field public static final image:I = 0x7f0d0011

.field public static final image1:I = 0x7f0d0016

.field public static final image2:I = 0x7f0d0014

.field public static final info1:I = 0x7f0d0010

.field public static final info2:I = 0x7f0d0013

.field public static final ir_camera_view:I = 0x7f0d0023

.field public static final item1:I = 0x7f0d001f

.field public static final item2:I = 0x7f0d0020

.field public static final item3:I = 0x7f0d0021

.field public static final item4:I = 0x7f0d0022

.field public static final item_container:I = 0x7f0d001e

.field public static final keyguard_bankcard_info:I = 0x7f0d005b

.field public static final keyguard_buscard_info:I = 0x7f0d005c

.field public static final keyguard_charging_container_bg:I = 0x7f0d0039

.field public static final keyguard_charging_drained_power:I = 0x7f0d0040

.field public static final keyguard_charging_hint:I = 0x7f0d003b

.field public static final keyguard_charging_info_anim_start_cicle_id:I = 0x7f0d003a

.field public static final keyguard_charging_info_back_arrow_id:I = 0x7f0d0042

.field public static final keyguard_charging_info_data:I = 0x7f0d0038

.field public static final keyguard_charging_info_des:I = 0x7f0d0037

.field public static final keyguard_charging_info_icon:I = 0x7f0d0036

.field public static final keyguard_charging_info_middle_list:I = 0x7f0d003d

.field public static final keyguard_charging_last_time:I = 0x7f0d003e

.field public static final keyguard_charging_tip:I = 0x7f0d0041

.field public static final keyguard_charging_used_time:I = 0x7f0d003f

.field public static final keyguard_electric_torch:I = 0x7f0d0057

.field public static final keyguard_left_list_item_img:I = 0x7f0d0054

.field public static final keyguard_left_list_item_name:I = 0x7f0d0055

.field public static final keyguard_left_list_item_number:I = 0x7f0d0056

.field public static final keyguard_lock_screen_magazine_info:I = 0x7f0d005d

.field public static final keyguard_move_left:I = 0x7f0d0058

.field public static final keyguard_notifications:I = 0x7f0d0084

.field public static final keyguard_remote_controller_info:I = 0x7f0d005a

.field public static final keyguard_smarthome_info:I = 0x7f0d0059

.field public static final loading_text:I = 0x7f0d001d

.field public static final loading_text_container:I = 0x7f0d001c

.field public static final lockPattern:I = 0x7f0d0081

.field public static final miui_face_input_msg:I = 0x7f0d0027

.field public static final miui_face_input_title:I = 0x7f0d0026

.field public static final miui_face_recoginition_input_ok:I = 0x7f0d0028

.field public static final miui_face_recoginition_intorduction_next:I = 0x7f0d0029

.field public static final miui_face_recoginition_suggestion_next:I = 0x7f0d002a

.field public static final miui_keyguard_charging_info_id:I = 0x7f0d008a

.field public static final miui_mixed_password_input_field:I = 0x7f0d0077

.field public static final miui_porch_keyguard_hint_text_id:I = 0x7f0d0088

.field public static final miui_porch_keyguard_up_arrow_id:I = 0x7f0d0087

.field public static final miui_porch_notification_and_music_control_container:I = 0x7f0d0083

.field public static final miui_sim_error_msg:I = 0x7f0d008f

.field public static final miui_sim_pin_input_edittext:I = 0x7f0d008d

.field public static final miui_unlock_screen_digital_clock:I = 0x7f0d0043

.field public static final miui_unlock_screen_vertical_clock:I = 0x7f0d005e

.field public static final mixed_password_keyboard_view:I = 0x7f0d007a

.field public static final notification_image_view:I = 0x7f0d002f

.field public static final notification_info:I = 0x7f0d0033

.field public static final notification_message_time:I = 0x7f0d0031

.field public static final notification_message_view:I = 0x7f0d0035

.field public static final notification_msg_and_profile_view:I = 0x7f0d0030

.field public static final notification_profile:I = 0x7f0d0032

.field public static final notification_title_view:I = 0x7f0d0034

.field public static final num:I = 0x7f0d0012

.field public static final num1:I = 0x7f0d0017

.field public static final num2:I = 0x7f0d0015

.field public static final numeric_inputview:I = 0x7f0d0080

.field public static final password_encrypt_dots:I = 0x7f0d007b

.field public static final phone_locked_textview:I = 0x7f0d0091

.field public static final phone_locked_timeout_id:I = 0x7f0d0092

.field public static final porch_keyguard_view_bottom:I = 0x7f0d0085

.field public static final remote_center_id:I = 0x7f0d0086

.field public static final second:I = 0x7f0d0009

.field public static final second_digital:I = 0x7f0d0046

.field public static final second_dot:I = 0x7f0d007d

.field public static final secure_keyguard_fingerprint_hint_msg:I = 0x7f0d002d

.field public static final secure_keyguard_fingerprint_hint_title:I = 0x7f0d002c

.field public static final sim_image_id:I = 0x7f0d008c

.field public static final smart_cover_layout_id:I = 0x7f0d000c

.field public static final ss:I = 0x7f0d0053

.field public static final tag_removed_keyguard_notification_key:I = 0x7f0d0000

.field public static final tag_removed_keyguard_notification_position:I = 0x7f0d0001

.field public static final third:I = 0x7f0d000a

.field public static final third_digital:I = 0x7f0d0048

.field public static final third_dot:I = 0x7f0d007e

.field public static final time_hour:I = 0x7f0d000e

.field public static final time_layout:I = 0x7f0d005f

.field public static final time_minute:I = 0x7f0d000f

.field public static final title:I = 0x7f0d0073

.field public static final unlock:I = 0x7f0d0078

.field public static final unlock_screen_battery_info:I = 0x7f0d0068

.field public static final unlock_screen_battery_info_id:I = 0x7f0d004b

.field public static final unlock_screen_digital_clock_id:I = 0x7f0d002b

.field public static final unlock_screen_owner_info:I = 0x7f0d0069

.field public static final unlock_screen_owner_info_id:I = 0x7f0d004d

.field public static final unlock_screen_sim_card_info:I = 0x7f0d004c

.field public static final unlock_sim_button:I = 0x7f0d008e

.field public static final unlockscreen_lockout_id:I = 0x7f0d0090

.field public static final vertical_hour:I = 0x7f0d0061

.field public static final vertical_min:I = 0x7f0d0062

.field public static final vertical_time_layout:I = 0x7f0d0060

.field public static final wall_paper_and_clock_and_preview_id:I = 0x7f0d0082

.field public static final wallpaper_des:I = 0x7f0d006a

.field public static final wallpaper_global_des_bottom_notify_area:I = 0x7f0d0070

.field public static final wallpaper_global_des_content:I = 0x7f0d006c

.field public static final wallpaper_global_des_drop_down_mark:I = 0x7f0d0071

.field public static final wallpaper_global_des_drop_down_notify_text:I = 0x7f0d0072

.field public static final wallpaper_global_des_from:I = 0x7f0d006e

.field public static final wallpaper_global_des_from_container:I = 0x7f0d006d

.field public static final wallpaper_global_des_from_logo:I = 0x7f0d006f

.field public static final wallpaper_global_des_title:I = 0x7f0d006b

.field public static final wallpaper_preview_button:I = 0x7f0d001b

.field public static final wireless_charge_picture:I = 0x7f0d0076

.field public static final wireless_charge_video:I = 0x7f0d0075


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
