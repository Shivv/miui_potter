.class public final Lcom/android/keyguard/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final aod_mode_layout:I = 0x7f030000

.field public static final full_smart_cover_layout:I = 0x7f030001

.field public static final keyguard_battery_info_layout:I = 0x7f030002

.field public static final keyguard_wireless_charging_layout:I = 0x7f030003

.field public static final loading_container:I = 0x7f030004

.field public static final miui_add_face_data_input:I = 0x7f030005

.field public static final miui_add_face_data_introduction:I = 0x7f030006

.field public static final miui_add_face_data_suggesstion:I = 0x7f030007

.field public static final miui_common_unlock_screen_layout:I = 0x7f030008

.field public static final miui_default_lock_screen_notification_item_layout:I = 0x7f030009

.field public static final miui_keyguard_ble_unlock_succeed_layout:I = 0x7f03000a

.field public static final miui_keyguard_charging_info_item:I = 0x7f03000b

.field public static final miui_keyguard_charging_info_layout:I = 0x7f03000c

.field public static final miui_keyguard_digital_clock:I = 0x7f03000d

.field public static final miui_keyguard_gxzw_anim_view:I = 0x7f03000e

.field public static final miui_keyguard_gxzw_icon_view:I = 0x7f03000f

.field public static final miui_keyguard_gxzw_overlay:I = 0x7f030010

.field public static final miui_keyguard_left_view_item:I = 0x7f030011

.field public static final miui_keyguard_left_view_layout:I = 0x7f030012

.field public static final miui_keyguard_vertical_clock:I = 0x7f030013

.field public static final miui_keyguard_wallpaper_describe_global_layout:I = 0x7f030014

.field public static final miui_keyguard_wallpaper_describe_layout:I = 0x7f030015

.field public static final miui_keyguard_wireless_charge_slowly:I = 0x7f030016

.field public static final miui_legacy_password_unlock_screen:I = 0x7f030017

.field public static final miui_mixed_password_unlock_screen:I = 0x7f030018

.field public static final miui_numeric_keyboard:I = 0x7f030019

.field public static final miui_password_unlock_screen_layout:I = 0x7f03001a

.field public static final miui_pattern_unlock_screen_layout:I = 0x7f03001b

.field public static final miui_porch_keyguard_layout:I = 0x7f03001c

.field public static final miui_sim_unlock_screen_layout:I = 0x7f03001d

.field public static final miui_unlockscreen_lockout:I = 0x7f03001e

.field public static final smart_cover_layout:I = 0x7f03001f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
