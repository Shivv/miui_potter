.class Lcom/android/keyguard/AbstractKeyguardViewManager$3;
.super Ljava/lang/Object;
.source "AbstractKeyguardViewManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/AbstractKeyguardViewManager;->hide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

.field final synthetic val$lastView:Lcom/android/keyguard/MiuiKeyguardViewBase;


# direct methods
.method constructor <init>(Lcom/android/keyguard/AbstractKeyguardViewManager;Lcom/android/keyguard/MiuiKeyguardViewBase;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$3;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    iput-object p2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$3;->val$lastView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$3;->val$lastView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->cleanUp()V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$3;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get3(Lcom/android/keyguard/AbstractKeyguardViewManager;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$3;->val$lastView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$3;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get2(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$3;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get2(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->removeAODView()V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$3;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get2(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->clearAODViewRunnable()V

    :cond_0
    return-void
.end method
