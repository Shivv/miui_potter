.class public Lcom/android/keyguard/NotificationsVerticalLinearLayoutManager;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "NotificationsVerticalLinearLayoutManager.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onMeasure(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)V
    .locals 15

    invoke-static/range {p3 .. p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v11

    invoke-static/range {p4 .. p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    invoke-static/range {p3 .. p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    invoke-static/range {p4 .. p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    const/4 v10, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v13

    if-lez v13, :cond_0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v13

    if-eqz v13, :cond_0

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {p0}, Lcom/android/keyguard/NotificationsVerticalLinearLayoutManager;->getPaddingLeft()I

    move-result v13

    invoke-virtual {p0}, Lcom/android/keyguard/NotificationsVerticalLinearLayoutManager;->getPaddingRight()I

    move-result v14

    add-int/2addr v13, v14

    iget v14, v9, Landroid/support/v7/widget/RecyclerView$LayoutParams;->width:I

    move/from16 v0, p3

    invoke-static {v0, v13, v14}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v5

    invoke-virtual {p0}, Lcom/android/keyguard/NotificationsVerticalLinearLayoutManager;->getPaddingTop()I

    move-result v13

    invoke-virtual {p0}, Lcom/android/keyguard/NotificationsVerticalLinearLayoutManager;->getPaddingBottom()I

    move-result v14

    add-int/2addr v13, v14

    iget v14, v9, Landroid/support/v7/widget/RecyclerView$LayoutParams;->height:I

    move/from16 v0, p4

    invoke-static {v0, v13, v14}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v3

    invoke-virtual {v1, v5, v3}, Landroid/view/View;->measure(II)V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    iget v14, v9, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v13, v14

    iget v14, v9, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int v4, v13, v14

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    iget v14, v9, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v13, v14

    iget v14, v9, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int v2, v13, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$Recycler;->recycleView(Landroid/view/View;)V

    move v10, v4

    invoke-virtual {p0}, Lcom/android/keyguard/NotificationsVerticalLinearLayoutManager;->getItemCount()I

    move-result v13

    mul-int v6, v2, v13

    :cond_0
    sparse-switch v11, :sswitch_data_0

    :cond_1
    :goto_0
    :sswitch_0
    sparse-switch v7, :sswitch_data_1

    :cond_2
    :goto_1
    :sswitch_1
    invoke-virtual {p0, v10, v6}, Lcom/android/keyguard/NotificationsVerticalLinearLayoutManager;->setMeasuredDimension(II)V

    return-void

    :sswitch_2
    move v10, v12

    goto :goto_0

    :sswitch_3
    if-le v10, v12, :cond_1

    move v10, v12

    goto :goto_0

    :sswitch_4
    move v6, v8

    goto :goto_1

    :sswitch_5
    if-le v6, v8, :cond_2

    move v6, v8

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_5
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_4
    .end sparse-switch
.end method
