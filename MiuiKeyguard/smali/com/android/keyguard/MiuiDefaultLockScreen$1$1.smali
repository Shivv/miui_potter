.class Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;
.super Landroid/os/AsyncTask;
.source "MiuiDefaultLockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$1;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$1;

.field final synthetic val$batteryLevel:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$1;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$1;

    iput p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->val$batteryLevel:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 7

    :try_start_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$1;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "content://com.miui.powercenter.provider"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string/jumbo v4, "getPowerSupplyInfo"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$1;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    const-string/jumbo v3, "quick_charge"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set15(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$1;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get30(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Boolean;

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$1;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set15(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    const-string/jumbo v2, "MiuiDefaultLockScreen"

    const-string/jumbo v3, "cannot find the path content://com.miui.powercenter.provider"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$1;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$1;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->val$batteryLevel:I

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setChargingLevel(IZ)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
