.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$1;
.super Ljava/lang/Object;
.source "MiuiKeyguardUpdateMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;

.field final synthetic val$result:I

.field final synthetic val$temperature:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;II)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$1;->this$2:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;

    iput p2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$1;->val$temperature:I

    iput p3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$1;->val$result:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_FINGERPRINT_IDENTIFY_RESULT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$1;->val$temperature:I

    div-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3$1;->val$result:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/AnalyticsHelper;->recordCalculateEvent(Ljava/lang/String;J)V

    return-void
.end method
