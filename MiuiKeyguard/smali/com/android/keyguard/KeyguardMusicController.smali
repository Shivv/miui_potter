.class public Lcom/android/keyguard/KeyguardMusicController;
.super Ljava/lang/Object;
.source "KeyguardMusicController.java"


# static fields
.field private static final MUSIC_CONTROL_VIEW_PATH:Ljava/lang/String; = "/system/media/theme/default/keyguardmusicview"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMusicMamlView:Lmiui/maml/component/MamlView;

.field private mResourceManager:Lmiui/maml/ResourceManager;

.field private mScreenElementRoot:Lmiui/maml/ScreenElementRoot;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiui/maml/ScreenElementRoot$OnExternCommandListener;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/keyguard/KeyguardMusicController;->mContext:Landroid/content/Context;

    new-instance v0, Lmiui/maml/ResourceManager;

    new-instance v1, Lmiui/maml/util/ZipResourceLoader;

    const-string/jumbo v2, "/system/media/theme/default/keyguardmusicview"

    invoke-direct {v1, v2}, Lmiui/maml/util/ZipResourceLoader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lmiui/maml/ResourceManager;-><init>(Lmiui/maml/ResourceLoader;)V

    iput-object v0, p0, Lcom/android/keyguard/KeyguardMusicController;->mResourceManager:Lmiui/maml/ResourceManager;

    new-instance v0, Lmiui/maml/ScreenElementRoot;

    new-instance v1, Lmiui/maml/ScreenContext;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    iget-object v3, p0, Lcom/android/keyguard/KeyguardMusicController;->mContext:Landroid/content/Context;

    sget v4, Lmiui/R$style;->Theme_Light:I

    invoke-direct {v2, v3, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Lcom/android/keyguard/KeyguardMusicController;->mResourceManager:Lmiui/maml/ResourceManager;

    invoke-direct {v1, v2, v3}, Lmiui/maml/ScreenContext;-><init>(Landroid/content/Context;Lmiui/maml/ResourceManager;)V

    invoke-direct {v0, v1}, Lmiui/maml/ScreenElementRoot;-><init>(Lmiui/maml/ScreenContext;)V

    iput-object v0, p0, Lcom/android/keyguard/KeyguardMusicController;->mScreenElementRoot:Lmiui/maml/ScreenElementRoot;

    iget-object v0, p0, Lcom/android/keyguard/KeyguardMusicController;->mScreenElementRoot:Lmiui/maml/ScreenElementRoot;

    invoke-virtual {v0, p2}, Lmiui/maml/ScreenElementRoot;->setOnExternCommandListener(Lmiui/maml/ScreenElementRoot$OnExternCommandListener;)V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardMusicController;->mScreenElementRoot:Lmiui/maml/ScreenElementRoot;

    invoke-virtual {v0}, Lmiui/maml/ScreenElementRoot;->load()Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Lcom/android/keyguard/KeyguardMusicController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "fail to load keyguard music maml resources from /system/media/theme/default/keyguardmusicview"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public clearMamlResource()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/KeyguardMusicController;->mMusicMamlView:Lmiui/maml/component/MamlView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/KeyguardMusicController;->mMusicMamlView:Lmiui/maml/component/MamlView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/maml/component/MamlView;->cleanUp(Z)V

    :cond_0
    return-void
.end method

.method public getMamlView()Lmiui/maml/component/MamlView;
    .locals 3

    new-instance v0, Lmiui/maml/component/MamlView;

    iget-object v1, p0, Lcom/android/keyguard/KeyguardMusicController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/keyguard/KeyguardMusicController;->mScreenElementRoot:Lmiui/maml/ScreenElementRoot;

    invoke-direct {v0, v1, v2}, Lmiui/maml/component/MamlView;-><init>(Landroid/content/Context;Lmiui/maml/ScreenElementRoot;)V

    iput-object v0, p0, Lcom/android/keyguard/KeyguardMusicController;->mMusicMamlView:Lmiui/maml/component/MamlView;

    iget-object v0, p0, Lcom/android/keyguard/KeyguardMusicController;->mMusicMamlView:Lmiui/maml/component/MamlView;

    return-object v0
.end method

.method public isMusicActive()Z
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/KeyguardMusicController;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    return v1
.end method

.method public setDisplayMode(I)V
    .locals 4

    const-string/jumbo v0, "mode"

    iget-object v1, p0, Lcom/android/keyguard/KeyguardMusicController;->mScreenElementRoot:Lmiui/maml/ScreenElementRoot;

    invoke-virtual {v1}, Lmiui/maml/ScreenElementRoot;->getVariables()Lmiui/maml/data/Variables;

    move-result-object v1

    int-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Lmiui/maml/util/Utils;->putVariableNumber(Ljava/lang/String;Lmiui/maml/data/Variables;D)V

    return-void
.end method

.method public setMusicControlMode(I)V
    .locals 6

    iget-object v1, p0, Lcom/android/keyguard/KeyguardMusicController;->mMusicMamlView:Lmiui/maml/component/MamlView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/KeyguardMusicController;->mMusicMamlView:Lmiui/maml/component/MamlView;

    invoke-virtual {v1}, Lmiui/maml/component/MamlView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/KeyguardMusicController;->mMusicMamlView:Lmiui/maml/component/MamlView;

    invoke-virtual {v1}, Lmiui/maml/component/MamlView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/KeyguardMusicController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    :goto_0
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lcom/android/keyguard/KeyguardMusicController;->mMusicMamlView:Lmiui/maml/component/MamlView;

    invoke-virtual {v1, v0}, Lmiui/maml/component/MamlView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    const-string/jumbo v1, "show_notification"

    iget-object v2, p0, Lcom/android/keyguard/KeyguardMusicController;->mScreenElementRoot:Lmiui/maml/ScreenElementRoot;

    invoke-virtual {v2}, Lmiui/maml/ScreenElementRoot;->getVariables()Lmiui/maml/data/Variables;

    move-result-object v2

    int-to-double v4, p1

    invoke-static {v1, v2, v4, v5}, Lmiui/maml/util/Utils;->putVariableNumber(Ljava/lang/String;Lmiui/maml/data/Variables;D)V

    return-void

    :cond_1
    const/4 v1, -0x2

    goto :goto_0
.end method
