.class public Lcom/android/keyguard/CompatibilityL;
.super Ljava/lang/Object;
.source "CompatibilityL.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "CompatibilityL"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertSubId(J)I
    .locals 2

    long-to-int v0, p0

    return v0
.end method

.method public static getSubId(I)Ljava/lang/Long;
    .locals 4

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[I

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    return-object v1
.end method
