.class public interface abstract Lcom/android/keyguard/MiuiKeyguardViewCallback;
.super Ljava/lang/Object;
.source "MiuiKeyguardViewCallback.java"


# virtual methods
.method public abstract disableInfo(Z)V
.end method

.method public abstract disableNavigation(Z)V
.end method

.method public abstract keyguardDone(Z)V
.end method

.method public abstract keyguardDoneDrawing()V
.end method

.method public abstract pokeWakelock()V
.end method

.method public abstract pokeWakelock(I)V
.end method

.method public abstract stopFingerprintIdentification()V
.end method
