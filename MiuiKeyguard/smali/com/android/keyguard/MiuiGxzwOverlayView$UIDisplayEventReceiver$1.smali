.class Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;
.super Ljava/util/TimerTask;
.source "MiuiGxzwOverlayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;->onVsync(JII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

.field final synthetic val$timer:Ljava/util/Timer;

.field final synthetic val$timestampNanos:J

.field final synthetic val$ui_time:J


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;Ljava/util/Timer;JJ)V
    .locals 1

    iput-object p1, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->this$1:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    iput-object p2, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$timer:Ljava/util/Timer;

    iput-wide p3, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$timestampNanos:J

    iput-wide p5, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$ui_time:J

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const-wide/32 v8, 0xf4240

    iget-object v2, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$timer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->this$1:Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    invoke-static {v2}, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;->-get0(Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    const-string/jumbo v2, "MiuiGxzwOverlayView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "timestampNanos = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$timestampNanos:J

    div-long/2addr v4, v8

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", onVsync: ui_time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$ui_time:J

    div-long/2addr v4, v8

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", close_time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-long v4, v0, v8

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v2, "MiuiGxzwOverlayView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "detal: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$ui_time:J

    div-long/2addr v4, v8

    iget-wide v6, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$timestampNanos:J

    div-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-long v4, v0, v8

    iget-wide v6, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;->val$ui_time:J

    div-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
