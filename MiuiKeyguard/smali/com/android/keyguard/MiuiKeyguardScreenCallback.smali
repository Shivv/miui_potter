.class public interface abstract Lcom/android/keyguard/MiuiKeyguardScreenCallback;
.super Ljava/lang/Object;
.source "MiuiKeyguardScreenCallback.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardViewCallback;


# virtual methods
.method public abstract getPendingIntent()Landroid/content/Intent;
.end method

.method public abstract goToLockScreen()V
.end method

.method public abstract goToUnlockScreen()V
.end method

.method public abstract isSecure()Z
.end method

.method public abstract isVerifyUnlockOnly()Z
.end method

.method public abstract recreateMe(Landroid/content/res/Configuration;)V
.end method

.method public abstract reportFailedUnlockAttempt()V
.end method

.method public abstract reportSuccessfulUnlockAttempt()V
.end method

.method public abstract setPendingIntent(Landroid/content/Intent;)V
.end method

.method public abstract startFaceUnlock()V
.end method

.method public abstract takeEmergencyCallAction()V
.end method

.method public abstract updatePinUnlockCancel(I)V
.end method

.method public abstract updatePukUnlockCancel(I)V
.end method
