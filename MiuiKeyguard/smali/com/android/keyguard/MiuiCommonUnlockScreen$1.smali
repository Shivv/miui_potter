.class Lcom/android/keyguard/MiuiCommonUnlockScreen$1;
.super Ljava/lang/Object;
.source "MiuiCommonUnlockScreen.java"

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public makeView()Landroid/view/View;
    .locals 3

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get0(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-object v0
.end method
