.class Lcom/android/keyguard/MiuiNumericInputView$Cell;
.super Landroid/widget/FrameLayout;
.source "MiuiNumericInputView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiNumericInputView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Cell"
.end annotation


# instance fields
.field private final mKey:I

.field final synthetic this$0:Lcom/android/keyguard/MiuiNumericInputView;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiNumericInputView$Cell;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mKey:I

    return v0
.end method

.method public constructor <init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;I)V
    .locals 3

    const/4 v2, -0x1

    iput-object p1, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->this$0:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput p3, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mKey:I

    iget v0, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mKey:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/android/keyguard/MiuiNumericInputView$Cell$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell$1;-><init>(Lcom/android/keyguard/MiuiNumericInputView$Cell;)V

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-direct {p0, p3}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->createCellView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private createCellView(I)Landroid/view/View;
    .locals 10

    const/16 v5, 0x31

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v7, -0x2

    const/4 v6, 0x0

    const/4 v4, -0x1

    if-ne p1, v4, :cond_0

    new-instance v4, Landroid/view/View;

    iget-object v5, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object v4

    :cond_0
    const/16 v4, 0xb

    if-ne p1, v4, :cond_1

    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b000f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c000a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->this$0:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-static {v4, v3}, Lcom/android/keyguard/MiuiNumericInputView;->-set0(Lcom/android/keyguard/MiuiNumericInputView;Landroid/view/View;)Landroid/view/View;

    return-object v3

    :cond_1
    const/16 v4, 0xa

    if-ne p1, v4, :cond_2

    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b0003

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c000a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    return-object v3

    :cond_2
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/16 v4, 0x11

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    const v4, 0x7f020064

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    new-instance v2, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0030

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v2, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const-string/jumbo v4, "miui-light"

    invoke-static {v4, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {v2, v8, v9}, Landroid/widget/TextView;->setLineSpacing(FF)V

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    new-instance v0, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/keyguard/MiuiNumericInputView$Cell;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/keyguard/MiuiNumericInputView;->-get1()Ljava/util/Map;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0031

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v0, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const-string/jumbo v4, "miui-regular"

    invoke-static {v4, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {v0, v8, v9}, Landroid/widget/TextView;->setLineSpacing(FF)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v1
.end method
