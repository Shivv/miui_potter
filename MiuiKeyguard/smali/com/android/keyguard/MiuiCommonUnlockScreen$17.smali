.class Lcom/android/keyguard/MiuiCommonUnlockScreen$17;
.super Landroid/os/CountDownTimer;
.source "MiuiCommonUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;->showLockoutView(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

.field final synthetic val$countDownView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;JJLandroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iput-object p6, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->val$countDownView:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setPasswordLockout(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get5(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFingerprintIdentify()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->hideLockoutView()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->writePasswordOutTime(J)V

    return-void
.end method

.method public onTick(J)V
    .locals 5

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$17;->val$countDownView:Landroid/widget/TextView;

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-wrap5(Lcom/android/keyguard/MiuiCommonUnlockScreen;Landroid/widget/TextView;J)V

    return-void
.end method
