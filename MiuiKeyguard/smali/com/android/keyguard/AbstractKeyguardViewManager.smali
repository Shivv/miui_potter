.class public abstract Lcom/android/keyguard/AbstractKeyguardViewManager;
.super Ljava/lang/Object;
.source "AbstractKeyguardViewManager.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardWindowController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/AbstractKeyguardViewManager$1;,
        Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;,
        Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS:I = -0x80000000

.field private static final SYSTEM_UI_FLAG_LIGHT_STATUS_BAR:I = 0x2000

.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

.field private final mContext:Landroid/content/Context;

.field private mDozeHost:Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

.field private mKeyguardHost:Landroid/widget/FrameLayout;

.field private mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

.field private final mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

.field private mNeedsInput:Z

.field private mScreenOn:Z

.field private final mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field private final mViewManager:Landroid/view/WindowManager;

.field private final mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

.field private mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/AbstractKeyguardViewManager;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mDozeHost:Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/AbstractKeyguardViewManager;)Landroid/widget/FrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewBase;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/AbstractKeyguardViewManager;)Landroid/view/WindowManager;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mViewManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/AbstractKeyguardViewManager;)Landroid/view/WindowManager$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "KeyguardViewManager"

    sput-object v0, Lcom/android/keyguard/AbstractKeyguardViewManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/MiuiKeyguardViewProperties;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mNeedsInput:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mDozeHost:Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

    iput-boolean v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mScreenOn:Z

    new-instance v0, Lcom/android/keyguard/AbstractKeyguardViewManager$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/AbstractKeyguardViewManager$1;-><init>(Lcom/android/keyguard/AbstractKeyguardViewManager;)V

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x103006b

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mViewManager:Landroid/view/WindowManager;

    iput-object p2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    iput-object p3, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

    iput-object p4, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-boolean v0, Lcom/android/keyguard/Dependency;->SUPPORT_AOD:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;-><init>(Lcom/android/keyguard/AbstractKeyguardViewManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mDozeHost:Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mDozeHost:Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;

    invoke-static {v0}, Lcom/android/keyguard/Dependency;->setHost(Lcom/android/keyguard/doze/DozeHost;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public clearWindowExitAnimation()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mViewManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->dismiss()V

    :cond_0
    return-void
.end method

.method public declared-synchronized hide()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    const-wide/16 v2, -0x1

    invoke-virtual {p0, v2, v3}, Lcom/android/keyguard/AbstractKeyguardViewManager;->setUserActivityTimeout(J)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    new-instance v2, Lcom/android/keyguard/AbstractKeyguardViewManager$3;

    invoke-direct {v2, p0, v0}, Lcom/android/keyguard/AbstractKeyguardViewManager$3;-><init>(Lcom/android/keyguard/AbstractKeyguardViewManager;Lcom/android/keyguard/MiuiKeyguardViewBase;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isShowing()Z
    .locals 2

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onScreenTurnedOff()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mScreenOn:Z

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->onScreenTurnedOff()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onScreenTurnedOn(Ljava/lang/Object;)V
    .locals 3

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mScreenOn:Z

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->onScreenTurnedOn()V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    new-instance v2, Lcom/android/keyguard/AbstractKeyguardViewManager$2;

    invoke-direct {v2, p0}, Lcom/android/keyguard/AbstractKeyguardViewManager$2;-><init>(Lcom/android/keyguard/AbstractKeyguardViewManager;)V

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardViewBase;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    invoke-static {p1, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->postScreenTurnedOnCallbackIfNeed(Ljava/lang/Object;Landroid/view/View;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v1, Lcom/android/keyguard/AbstractKeyguardViewManager;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_1
    const/4 v1, 0x0

    :try_start_3
    invoke-static {p1, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->screenTurnedOnCallback(Ljava/lang/Object;Landroid/os/IBinder;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    sget-object v1, Lcom/android/keyguard/AbstractKeyguardViewManager;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :try_start_5
    invoke-static {p1, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->screenTurnedOnCallback(Ljava/lang/Object;Landroid/os/IBinder;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_6
    sget-object v1, Lcom/android/keyguard/AbstractKeyguardViewManager;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized reset()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setHidden(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->setHidden(Z)V

    :cond_0
    return-void
.end method

.method public setNeedsInput(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mNeedsInput:Z

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v2, -0x20001

    and-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mViewManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x20000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method

.method public setUserActivityTimeout(J)V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput-wide p1, v0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mViewManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public declared-synchronized show()V
    .locals 11

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const-string/jumbo v1, "lockscreen.rot_override"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    const v1, 0x1120060

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    if-nez v1, :cond_2

    new-instance v1, Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v5}, Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;)V

    iput-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    const/4 v9, -0x1

    const v4, 0x110900

    iget-boolean v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mNeedsInput:Z

    if-nez v1, :cond_0

    const v4, 0x130900

    :cond_0
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    const/16 v3, 0x7d4

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x1000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    :cond_1
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    const-string/jumbo v1, "Keyguard"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mViewManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    iget-object v5, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-interface {v1, v2, v3, v5, p0}, Lcom/android/keyguard/MiuiKeyguardViewProperties;->createKeyguardView(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardWindowController;)Lcom/android/keyguard/MiuiKeyguardViewBase;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    const v2, 0x10202eb

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardViewBase;->setId(I)V

    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v7, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v1, v2, v7}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    if-eqz v6, :cond_5

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->allowScreenRotation()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    :goto_1
    const v10, 0x2001300

    sget-object v1, Lcom/android/keyguard/AbstractKeyguardViewManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "KGVM: Set visibility on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v10}, Landroid/widget/FrameLayout;->setSystemUiVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->requestFocus()Z

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUtils;->isLiveWallpaper(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AbstractKeyguardViewManager;->updateWindowLayoutParamsByLiveWallpaper(Z)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-wide/16 v2, 0x2710

    iput-wide v2, v1, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mViewManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerWallpaperChangeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_4
    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_5
    :try_start_1
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public startFaceUnlock()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->startFaceUnlock()V

    :cond_0
    return-void
.end method

.method public updateStatusBarColorMode(Z)V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v1, v2, :cond_3

    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mViewManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit8 v2, v2, -0x11

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getSystemUiVisibility()I

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, -0x80000000

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    if-nez p1, :cond_4

    or-int/lit16 v0, v0, 0x2000

    :goto_1
    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setSystemUiVisibility(I)V

    goto :goto_0

    :cond_4
    and-int/lit16 v0, v0, -0x2001

    goto :goto_1
.end method

.method public updateWindowLayoutParamsByLiveWallpaper(Z)V
    .locals 9

    const/high16 v8, 0x8000000

    const/high16 v7, 0x100000

    const/4 v3, -0x1

    const/4 v4, -0x3

    const v6, -0x100001

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isDefaultLockScreenTheme()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v5, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

    invoke-interface {v2}, Lcom/android/keyguard/MiuiKeyguardViewProperties;->isSecure()Z

    move-result v2

    if-eqz v2, :cond_0

    xor-int/lit8 v2, p1, 0x1

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    iput v2, v5, Landroid/view/WindowManager$LayoutParams;->format:I

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/2addr v3, v7

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    const v4, -0x8000001

    and-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    :goto_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-ge v2, v3, :cond_3

    iget-object v3, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

    invoke-interface {v2}, Lcom/android/keyguard/MiuiKeyguardViewProperties;->isSecure()Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f0f0004

    :goto_2
    iput v2, v3, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    :goto_3
    return-void

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v3, v6

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v3, v8

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v3, 0x7f0f0004

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    instance-of v2, v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    check-cast v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const-string/jumbo v5, "displayDesktop"

    invoke-virtual {v2, v5}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    check-cast v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const-string/jumbo v5, "showSysWallpaper"

    invoke-virtual {v2, v5}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    :cond_5
    if-nez v0, :cond_6

    if-eqz p1, :cond_8

    :cond_6
    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v3, v8

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    :goto_4
    if-nez v1, :cond_7

    if-eqz p1, :cond_9

    :cond_7
    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/2addr v3, v7

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    goto :goto_4

    :cond_9
    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v3, v6

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_3
.end method

.method public declared-synchronized verifyUnlock()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->show()V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->verifyUnlock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public wakeWhenReadyTq(I)Z
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager;->mKeyguardView:Lcom/android/keyguard/MiuiKeyguardViewBase;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->wakeWhenReadyTq(I)V

    const/4 v0, 0x1

    return v0

    :cond_0
    sget-object v0, Lcom/android/keyguard/AbstractKeyguardViewManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "mKeyguardView is null in wakeWhenReadyTq"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method
