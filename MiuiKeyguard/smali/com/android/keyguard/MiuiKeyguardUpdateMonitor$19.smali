.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$19;
.super Ljava/lang/Object;
.source "MiuiKeyguardUpdateMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->showWirelessChargeSlowly()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$19;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$19;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get12(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$19;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    new-instance v1, Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$19;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set9(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;)Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$19;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get12(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;->show()V

    return-void
.end method
