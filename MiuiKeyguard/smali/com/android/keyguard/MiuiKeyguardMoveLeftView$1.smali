.class Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;
.super Ljava/lang/Object;
.source "MiuiKeyguardMoveLeftView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardMoveLeftView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v3, "com.android.contacts"

    goto :goto_1

    :pswitch_2
    const-string v3, "com.android.calendar"

    goto :goto_1

    :pswitch_3
    const-string v3, "com.miui.calculator"

    goto :goto_1

    :pswitch_4
    const-string v3, "com.miui.notes"

    goto :goto_1

    :pswitch_5
    new-instance v1, Landroid/content/Intent;

    const-string v3, "miui.intent.action.TOGGLE_TORCH"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "miui.intent.extra.IS_ENABLE"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :goto_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$1;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get12(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    goto :goto_0

    :pswitch_6
    const-string v3, "com.miui.player"

    goto :goto_1

    move-exception v0

    goto/16 :goto_2

    move-exception v0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0057
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method
