.class Lcom/android/keyguard/NotificationItemAnimator$5;
.super Lcom/android/keyguard/NotificationItemAnimator$VpaListenerAdapter;
.source "NotificationItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/NotificationItemAnimator;->animateAddImpl(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/NotificationItemAnimator;

.field final synthetic val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

.field final synthetic val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;


# direct methods
.method constructor <init>(Lcom/android/keyguard/NotificationItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v4/view/ViewPropertyAnimatorCompat;)V
    .locals 1

    iput-object p1, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    iput-object p2, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iput-object p3, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/NotificationItemAnimator$VpaListenerAdapter;-><init>(Lcom/android/keyguard/NotificationItemAnimator$VpaListenerAdapter;)V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    return-void
.end method

.method public onAnimationEnd(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    iget-object v1, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/NotificationItemAnimator;->dispatchAddFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    invoke-static {v0}, Lcom/android/keyguard/NotificationItemAnimator;->-get0(Lcom/android/keyguard/NotificationItemAnimator;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    invoke-static {v0}, Lcom/android/keyguard/NotificationItemAnimator;->-wrap3(Lcom/android/keyguard/NotificationItemAnimator;)V

    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->this$0:Lcom/android/keyguard/NotificationItemAnimator;

    iget-object v1, p0, Lcom/android/keyguard/NotificationItemAnimator$5;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/NotificationItemAnimator;->dispatchAddStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    return-void
.end method
