.class Lcom/android/keyguard/faceunlock/Camera2Util$6;
.super Ljava/lang/Object;
.source "Camera2Util.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/faceunlock/Camera2Util;->closeCamera()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/faceunlock/Camera2Util;


# direct methods
.method constructor <init>(Lcom/android/keyguard/faceunlock/Camera2Util;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    iget-object v1, v1, Lcom/android/keyguard/faceunlock/Camera2Util;->mCaptureSession:Landroid/hardware/camera2/CameraCaptureSession;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    iget-object v1, v1, Lcom/android/keyguard/faceunlock/Camera2Util;->mCaptureSession:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v1}, Landroid/hardware/camera2/CameraCaptureSession;->stopRepeating()V

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    iget-object v1, v1, Lcom/android/keyguard/faceunlock/Camera2Util;->mCaptureSession:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v1}, Landroid/hardware/camera2/CameraCaptureSession;->abortCaptures()V

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    iget-object v1, v1, Lcom/android/keyguard/faceunlock/Camera2Util;->mCaptureSession:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v1}, Landroid/hardware/camera2/CameraCaptureSession;->close()V

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/keyguard/faceunlock/Camera2Util;->mCaptureSession:Landroid/hardware/camera2/CameraCaptureSession;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1}, Lcom/android/keyguard/faceunlock/Camera2Util;->-get0(Lcom/android/keyguard/faceunlock/Camera2Util;)Landroid/hardware/camera2/CameraDevice;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1}, Lcom/android/keyguard/faceunlock/Camera2Util;->-get0(Lcom/android/keyguard/faceunlock/Camera2Util;)Landroid/hardware/camera2/CameraDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/camera2/CameraDevice;->close()V

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1, v3}, Lcom/android/keyguard/faceunlock/Camera2Util;->-set0(Lcom/android/keyguard/faceunlock/Camera2Util;Landroid/hardware/camera2/CameraDevice;)Landroid/hardware/camera2/CameraDevice;

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1}, Lcom/android/keyguard/faceunlock/Camera2Util;->-get4(Lcom/android/keyguard/faceunlock/Camera2Util;)Landroid/media/ImageReader;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1}, Lcom/android/keyguard/faceunlock/Camera2Util;->-get4(Lcom/android/keyguard/faceunlock/Camera2Util;)Landroid/media/ImageReader;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/ImageReader;->close()V

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1, v3}, Lcom/android/keyguard/faceunlock/Camera2Util;->-set1(Lcom/android/keyguard/faceunlock/Camera2Util;Landroid/media/ImageReader;)Landroid/media/ImageReader;

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1}, Lcom/android/keyguard/faceunlock/Camera2Util;->-get5(Lcom/android/keyguard/faceunlock/Camera2Util;)Landroid/media/ImageReader;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1}, Lcom/android/keyguard/faceunlock/Camera2Util;->-get5(Lcom/android/keyguard/faceunlock/Camera2Util;)Landroid/media/ImageReader;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/ImageReader;->close()V

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/Camera2Util$6;->this$0:Lcom/android/keyguard/faceunlock/Camera2Util;

    invoke-static {v1, v3}, Lcom/android/keyguard/faceunlock/Camera2Util;->-set2(Lcom/android/keyguard/faceunlock/Camera2Util;Landroid/media/ImageReader;)Landroid/media/ImageReader;

    :cond_3
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
