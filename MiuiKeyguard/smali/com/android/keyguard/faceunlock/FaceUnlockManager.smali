.class public Lcom/android/keyguard/faceunlock/FaceUnlockManager;
.super Ljava/lang/Object;
.source "FaceUnlockManager.java"

# interfaces
.implements Lcom/android/keyguard/faceunlock/IPreviewCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/faceunlock/FaceUnlockManager$1;,
        Lcom/android/keyguard/faceunlock/FaceUnlockManager$2;,
        Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;
    }
.end annotation


# static fields
.field public static final ACTION_FACE_UNLOCK_RELEASE:Ljava/lang/String; = "face_unlock_release"

.field public static DEBUG:Z = false

.field public static final FACE_UNLOCK_APPLY_FOR_LOCK:Ljava/lang/String; = "face_unlcok_apply_for_lock"

.field public static final FACE_UNLOCK_BY_NOTIFICATION_SCREEN_ON_DEFAULT:I = 0x0

.field public static final FACE_UNLOCK_HAS_FEATURE:Ljava/lang/String; = "face_unlock_has_feature"

.field private static final FACE_UNLOCK_MAX_FAILED_COUNT:I = 0x5

.field private static final FACE_UNLOCK_MAX_LIVE_FAILED_COUNT:I = 0x3

.field public static final FACE_UNLOCK_VALID_FEATURE:Ljava/lang/String; = "face_unlock_valid_feature"

.field public static final FACE_UNLOCK_VERSION:I = 0x7

.field public static final KEY_FACE_UNLOCK_BY_NOTIFICATION_SCREEN_ON:Ljava/lang/String; = "face_unlock_by_notification_screen_on"

.field public static final KEY_FACE_UNLOCK_VERSION:Ljava/lang/String; = "face_unlock_version"

.field public static final MODEL_LIVENESS_1:Ljava/lang/String; = "model_liveness_1"

.field public static final TAG:Ljava/lang/String; = "face_unlock"

.field private static sFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

.field private static sInitFaceUlockUtil:Z

.field public static sStageInFaceUnlockTime:J

.field public static sStartFaceUnlockTime:J


# instance fields
.field private mCallback:Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;

.field private mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

.field private mContext:Landroid/content/Context;

.field private mFaceUnlockView:Landroid/view/TextureView;

.field private mFailedCount:I

.field private mFailedLiveCount:I

.field private mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mHasFace:Z

.field private mHasInit:Z

.field private mHelpRunnable:Ljava/lang/Runnable;

.field private mHelpStringResId:I

.field private mIsDetecting:Z

.field private mIsSuccess:Z

.field private mIsTrigger:Z

.field private mLite:Lcom/megvii/facepp/sdk/Lite;

.field private mLiveAttack:Z

.field private mLiveAttackValue:I

.field private mMainHandler:Landroid/os/Handler;

.field private mPreviewFrame:[B

.field private mReport:[I

.field private mRunTimeoutRunnable:Ljava/lang/Runnable;

.field private mSharedPreferenceUtil:Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;

.field private mSkipIfNotFound:Z

.field private mStartSetMeteringAreasTime:J

.field private mWorkerHandler:Landroid/os/Handler;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCallback:Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasFace:Z

    return v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHelpStringResId:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsSuccess:Z

    return v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsTrigger:Z

    return v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->stopDetect()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->DEBUG:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sInitFaceUlockUtil:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "face_unlock"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHandlerThread:Landroid/os/HandlerThread;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mMainHandler:Landroid/os/Handler;

    const/16 v0, 0x14

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    iput-boolean v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasFace:Z

    iput-boolean v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttack:Z

    new-instance v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager$1;-><init>(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mRunTimeoutRunnable:Ljava/lang/Runnable;

    iput-boolean v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSkipIfNotFound:Z

    new-instance v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager$2;-><init>(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHelpRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->initFaceUnlockUtil()V

    return-void
.end method

.method private deleteFaceModel(Ljava/lang/String;)V
    .locals 6

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    const/4 v2, 0x0

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v1, v3, v2

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;
    .locals 2

    sget-object v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    :cond_0
    :goto_0
    sget-object v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    return-object v0

    :cond_1
    sget-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sInitFaceUlockUtil:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    invoke-direct {v0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->initFaceUnlockUtil()V

    goto :goto_0
.end method

.method private handlePassed()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsSuccess:Z

    invoke-direct {p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->setUnlock()V

    invoke-direct {p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->stopDetect()V

    return-void
.end method

.method private initFaceUnlockUtil()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportFaceUnlock(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSharedPreferenceUtil:Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;

    invoke-static {}, Lcom/megvii/facepp/sdk/Lite;->getInstance()Lcom/megvii/facepp/sdk/Lite;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mWorkerHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportFaceUnlockUseCamera1(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/keyguard/faceunlock/Camera1Util;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mWorkerHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/android/keyguard/faceunlock/Camera1Util;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    :goto_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sInitFaceUlockUtil:Z

    :cond_0
    new-instance v0, Lcom/android/keyguard/FingerprintHelper;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/keyguard/FingerprintHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    return-void

    :cond_1
    new-instance v0, Lcom/android/keyguard/faceunlock/Camera2Util;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mWorkerHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/android/keyguard/faceunlock/Camera2Util;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    goto :goto_0
.end method

.method private isVersionUpdate(I)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-lt p1, v1, :cond_0

    const/4 v1, 0x7

    if-eq p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private setUnlock()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCallback:Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;

    invoke-interface {v0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;->onSuccess()V

    return-void
.end method

.method private stopDetect()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mWorkerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mRunTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHelpRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsTrigger:Z

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    invoke-virtual {v0}, Lcom/android/keyguard/faceunlock/AbstractCameraUtil;->closeCamera()V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    invoke-virtual {v0}, Lcom/megvii/facepp/sdk/Lite;->reset()I

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasFace:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsSuccess:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedCount:I

    iget v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedCount:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/android/keyguard/AnalyticsHelper;->trackFaceUnlockLocked()V

    :cond_0
    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttack:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedLiveCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedLiveCount:I

    iget v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedLiveCount:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/android/keyguard/AnalyticsHelper;->trackFaceUnlockLocked()V

    :cond_1
    return-void
.end method


# virtual methods
.method public deleteFeature()V
    .locals 2

    sget-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "face_unlock"

    const-string/jumbo v1, "deleteFeature"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/megvii/facepp/sdk/Lite;->getInstance()Lcom/megvii/facepp/sdk/Lite;

    move-result-object v0

    invoke-virtual {v0}, Lcom/megvii/facepp/sdk/Lite;->deleteFeature()I

    return-void
.end method

.method public hasInit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasInit:Z

    return v0
.end method

.method public initAll()V
    .locals 15

    const/4 v14, 0x7

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSharedPreferenceUtil:Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;

    const-string/jumbo v10, "face_unlock_version"

    invoke-virtual {v9, v10}, Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;->getIntValueByKey(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->isVersionUpdate(I)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string/jumbo v9, "face_unlock"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "version update  old verion="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ";new version = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "/model"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->deleteFaceModel(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSharedPreferenceUtil:Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;

    const-string/jumbo v10, "model_liveness_1"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;->saveStringValue(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSharedPreferenceUtil:Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;

    const-string/jumbo v10, "model_liveness_1"

    invoke-virtual {v9, v10}, Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;->getStringValueByKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    const-string/jumbo v10, "model"

    const-string/jumbo v11, "liveness1.dlc"

    const v12, 0x7f060001

    invoke-static {v9, v12, v10, v11}, Lcom/android/keyguard/faceunlock/FileUtil;->saveRaw(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSharedPreferenceUtil:Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;

    const-string/jumbo v10, "model_liveness_1"

    invoke-virtual {v9, v10, v1}, Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;->saveStringValue(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {}, Lcom/megvii/facepp/sdk/Lite;->getInstance()Lcom/megvii/facepp/sdk/Lite;

    move-result-object v9

    iget-object v10, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/megvii/facepp/sdk/Lite;->initHandle(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mContext:Landroid/content/Context;

    const/high16 v10, 0x7f060000

    invoke-static {v9, v10}, Lcom/android/keyguard/faceunlock/FileUtil;->getFileContent(Landroid/content/Context;I)[B

    move-result-object v0

    invoke-static {}, Lcom/megvii/facepp/sdk/Lite;->getInstance()Lcom/megvii/facepp/sdk/Lite;

    move-result-object v9

    invoke-virtual {v9, v1, v2, v0}, Lcom/megvii/facepp/sdk/Lite;->initAll(Ljava/lang/String;Ljava/lang/String;[B)I

    move-result v9

    int-to-long v4, v9

    sget-boolean v9, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->DEBUG:Z

    if-eqz v9, :cond_2

    const-string/jumbo v9, "face_unlock"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "init all time="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v6

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-direct {p0, v8}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->isVersionUpdate(I)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    invoke-virtual {v9}, Lcom/megvii/facepp/sdk/Lite;->prepare()I

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    invoke-virtual {v9}, Lcom/megvii/facepp/sdk/Lite;->restoreFeature()I

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    invoke-virtual {v9}, Lcom/megvii/facepp/sdk/Lite;->reset()I

    iget-object v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSharedPreferenceUtil:Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;

    const-string/jumbo v10, "face_unlock_version"

    invoke-virtual {v9, v10, v14}, Lcom/android/keyguard/faceunlock/SharedPreferenceUtil;->saveIntValue(Ljava/lang/String;I)V

    :cond_3
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasInit:Z

    return-void
.end method

.method public isFaceUnlockLocked()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedCount:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedLiveCount:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidFeature()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    invoke-virtual {v1}, Lcom/megvii/facepp/sdk/Lite;->getFeatureCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onPreviewFrame([BLjava/lang/String;)V
    .locals 13

    const/4 v12, 0x5

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsSuccess:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsDetecting:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasFace:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSkipIfNotFound:Z

    if-eqz v0, :cond_2

    iput-boolean v6, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSkipIfNotFound:Z

    return-void

    :cond_2
    iput-boolean v5, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mSkipIfNotFound:Z

    iput-boolean v5, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsDetecting:Z

    iput-object p1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mPreviewFrame:[B

    sget-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->DEBUG:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "face_unlock"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "receive preview frame time="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v10, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStageInFaceUnlockTime:J

    sub-long/2addr v2, v10

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStageInFaceUnlockTime:J

    :cond_3
    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mPreviewFrame:[B

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    iget v2, v2, Lcom/android/keyguard/faceunlock/AbstractCameraUtil;->width:I

    iget-object v3, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    iget v3, v3, Lcom/android/keyguard/faceunlock/AbstractCameraUtil;->height:I

    iget-object v4, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    iget v4, v4, Lcom/android/keyguard/faceunlock/AbstractCameraUtil;->angle:I

    iget-object v7, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    invoke-virtual/range {v0 .. v7}, Lcom/megvii/facepp/sdk/Lite;->compare([BIIIZZ[I)I

    move-result v8

    sget-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->DEBUG:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "face_unlock"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "compare time ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v10, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStageInFaceUnlockTime:J

    sub-long/2addr v2, v10

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; result\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " run: fake = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", low = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    aget v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", compare score:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " live score:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    const/4 v3, 0x3

    aget v2, v2, v3

    int-to-double v2, v2

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v10

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " left:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    const/4 v3, 0x4

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " top:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    aget v2, v2, v12

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " right:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    const/4 v3, 0x6

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bottom:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    const/4 v3, 0x7

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStageInFaceUnlockTime:J

    :cond_4
    if-eqz v8, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mStartSetMeteringAreasTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xc8

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mStartSetMeteringAreasTime:J

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    invoke-virtual {v0, v1}, Lcom/android/keyguard/faceunlock/AbstractCameraUtil;->setMeteringAreas([I)V

    :cond_5
    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsTrigger:Z

    if-nez v0, :cond_6

    return-void

    :cond_6
    if-eq v8, v12, :cond_7

    iput-boolean v5, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasFace:Z

    :cond_7
    if-nez v8, :cond_8

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_FACE:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->recordUnlockWay(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->handlePassed()V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    invoke-virtual {v0}, Lcom/android/keyguard/FingerprintHelper;->resetFingerLockoutTime()V

    :goto_0
    return-void

    :cond_8
    const/4 v9, 0x0

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    const v9, 0x7f0b0086

    :goto_1
    iput v9, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHelpStringResId:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStartFaceUnlockTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHelpRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_9
    iput-boolean v6, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsDetecting:Z

    goto :goto_0

    :pswitch_1
    const v9, 0x7f0b0081

    goto :goto_1

    :pswitch_2
    const v9, 0x7f0b0086

    goto :goto_1

    :pswitch_3
    const v9, 0x7f0b0084

    goto :goto_1

    :pswitch_4
    const v9, 0x7f0b0083

    goto :goto_1

    :pswitch_5
    const v9, 0x7f0b0085

    goto :goto_1

    :pswitch_6
    const v9, 0x7f0b007d

    goto :goto_1

    :pswitch_7
    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    aget v0, v0, v6

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttackValue:I

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    aget v0, v0, v6

    iget v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttackValue:I

    sub-int/2addr v0, v1

    if-ne v0, v5, :cond_a

    iput-boolean v5, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttack:Z

    :cond_a
    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mReport:[I

    aget v0, v0, v6

    iput v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttackValue:I

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttack:Z

    if-eqz v0, :cond_b

    const v9, 0x7f0b0086

    goto :goto_1

    :cond_b
    const v9, 0x7f0b0081

    goto :goto_1

    :pswitch_8
    const v9, 0x7f0b0086

    goto :goto_1

    :pswitch_9
    const v9, 0x7f0b008a

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_9
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public openCameraSucced(Ljava/lang/String;Z)V
    .locals 6

    if-eqz p2, :cond_1

    sget-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "face_unlock"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "open camera succeed time="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStageInFaceUnlockTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStageInFaceUnlockTime:J

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    invoke-virtual {v0}, Lcom/megvii/facepp/sdk/Lite;->prepare()I

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFaceUnlockView:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/faceunlock/AbstractCameraUtil;->startPreview(Landroid/graphics/SurfaceTexture;)V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mWorkerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mRunTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "face_unlock"

    const-string/jumbo v1, "open Camera failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public release()V
    .locals 6

    sget-boolean v2, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string/jumbo v2, "face_unlock"

    const-string/jumbo v3, "release sHasInitLive"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasInit:Z

    if-nez v2, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasInit:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Lcom/megvii/facepp/sdk/Lite;->getInstance()Lcom/megvii/facepp/sdk/Lite;

    move-result-object v2

    invoke-virtual {v2}, Lcom/megvii/facepp/sdk/Lite;->release()V

    const-string/jumbo v2, "face_unlock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "release time="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public resetFailCount()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedCount:I

    iput v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFailedLiveCount:I

    return-void
.end method

.method public runOnFaceUnlockWorkerThread(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHandlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mWorkerHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mWorkerHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public startFaceUnlock(Landroid/view/TextureView;Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;)V
    .locals 7

    const/4 v6, 0x0

    iput-object p1, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mFaceUnlockView:Landroid/view/TextureView;

    iput-object p2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCallback:Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStartFaceUnlockTime:J

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsTrigger:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->isFaceUnlockLocked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    invoke-virtual {v0}, Lcom/megvii/facepp/sdk/Lite;->getFeatureCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasInit:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsTrigger:Z

    iput-boolean v6, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsSuccess:Z

    iput-boolean v6, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsDetecting:Z

    iput-boolean v6, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttack:Z

    iput-boolean v6, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mHasFace:Z

    iput v6, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLiveAttackValue:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mStartSetMeteringAreasTime:J

    const-string/jumbo v0, "face_unlock"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start open camera time="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sScreenTurnedOnTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; sdk version= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mLite:Lcom/megvii/facepp/sdk/Lite;

    invoke-virtual {v2}, Lcom/megvii/facepp/sdk/Lite;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStageInFaceUnlockTime:J

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    invoke-virtual {v0, p0}, Lcom/android/keyguard/faceunlock/AbstractCameraUtil;->setCameraCallback(Lcom/android/keyguard/faceunlock/IPreviewCallback;)V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mCameraUtil:Lcom/android/keyguard/faceunlock/AbstractCameraUtil;

    invoke-virtual {v0, v6}, Lcom/android/keyguard/faceunlock/AbstractCameraUtil;->openCamera(I)V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/faceunlock/FaceUnlockManager$3;

    invoke-direct {v1, p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager$3;-><init>(Lcom/android/keyguard/faceunlock/FaceUnlockManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public stopFaceUnlock()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsTrigger:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->mIsTrigger:Z

    invoke-direct {p0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->stopDetect()V

    :cond_0
    return-void
.end method
