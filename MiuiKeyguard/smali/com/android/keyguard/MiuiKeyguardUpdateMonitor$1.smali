.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;
.super Landroid/database/ContentObserver;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v1, Lcom/android/keyguard/MiuiDefaultLockScreen;->DEFAULT_LOCKWALLPAPER_PROVIDER:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "lock_wallpaper_provider_authority"

    const/4 v4, -0x2

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get18(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get18(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get8(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->isLockScreenMagazine:Z

    :cond_0
    return-void
.end method
