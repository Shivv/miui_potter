.class abstract Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;
.super Ljava/lang/Thread;
.source "MiuiSimPUKUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiSimPUKUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "CheckSimPuk"
.end annotation


# instance fields
.field protected final mPin:Ljava/lang/String;

.field protected final mPuk:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;


# direct methods
.method protected constructor <init>(Lcom/android/keyguard/MiuiSimPUKUnlockScreen;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p3, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;->mPin:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;->mPuk:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method abstract onSimLockChangedResponse(Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;)V
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->-get0(Lcom/android/keyguard/MiuiSimPUKUnlockScreen;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;->mPin:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;->mPuk:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    iget v3, v3, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mSimId:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/keyguard/UnlockSimHelper;->checkPuk(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;->onSimLockChangedResponse(Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;)V

    return-void
.end method
