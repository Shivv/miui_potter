.class Lcom/android/keyguard/FingerprintHelper$1;
.super Landroid/hardware/fingerprint/FingerprintManager$LockoutResetCallback;
.source "FingerprintHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/FingerprintHelper;->register(Lcom/android/keyguard/FingerprintIdentifyCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/FingerprintHelper;

.field final synthetic val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;


# direct methods
.method constructor <init>(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/FingerprintHelper$1;->this$0:Lcom/android/keyguard/FingerprintHelper;

    iput-object p2, p0, Lcom/android/keyguard/FingerprintHelper$1;->val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$LockoutResetCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onLockoutReset()V
    .locals 2

    invoke-super {p0}, Landroid/hardware/fingerprint/FingerprintManager$LockoutResetCallback;->onLockoutReset()V

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "onLockoutReset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper$1;->val$callback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-interface {v0}, Lcom/android/keyguard/FingerprintIdentifyCallback;->onReset()V

    return-void
.end method
