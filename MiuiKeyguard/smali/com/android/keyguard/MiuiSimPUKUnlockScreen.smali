.class Lcom/android/keyguard/MiuiSimPUKUnlockScreen;
.super Lcom/android/keyguard/MiuiSimPINUnlockScreen;
.source "MiuiSimPUKUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiSimPUKUnlockScreen$CheckSimPuk;,
        Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;
    }
.end annotation


# instance fields
.field protected mPinText:Ljava/lang/String;

.field protected mPukText:Ljava/lang/String;

.field protected mStateMachine:Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiSimPUKUnlockScreen;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiSimPUKUnlockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->updateSim()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;ILcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 2

    invoke-direct/range {p0 .. p7}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;ILcom/android/keyguard/MiuiKeyguardViewMediator;)V

    new-instance v0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;-><init>(Lcom/android/keyguard/MiuiSimPUKUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mStateMachine:Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method private updateSim()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->showSimUnlockProgressDialog()V

    new-instance v0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$1;

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mPukText:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mPinText:Ljava/lang/String;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$1;-><init>(Lcom/android/keyguard/MiuiSimPUKUnlockScreen;Lcom/android/keyguard/MiuiSimPUKUnlockScreen;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$1;->start()V

    return-void
.end method


# virtual methods
.method protected checkPin()Z
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mPinText:Ljava/lang/String;

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected checkPuk()Z
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mPukText:Ljava/lang/String;

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public confirmPin()Z
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mPinText:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected getFocusedEditText()Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method protected reloadUnlockButtonState()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mUnlockSimButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mUnlockSimButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected unlockAction()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mStateMachine:Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->next()V

    return-void
.end method
