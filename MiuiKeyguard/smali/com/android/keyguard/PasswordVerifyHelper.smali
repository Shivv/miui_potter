.class public Lcom/android/keyguard/PasswordVerifyHelper;
.super Ljava/lang/Object;
.source "PasswordVerifyHelper.java"


# instance fields
.field private mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

.field private mHandler:Landroid/os/Handler;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

.field private mLockout:Z

.field private mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/PasswordVerifyHelper;)Lcom/android/keyguard/MiuiKeyguardScreenCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    return-object v0
.end method

.method public constructor <init>(Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iput-object p2, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iput-object p3, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    new-instance v0, Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-direct {v0, p1}, Lcom/android/keyguard/LockPatternUtilsWrapper;-><init>(Lcom/android/internal/widget/LockPatternUtils;)V

    iput-object v0, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    return-void
.end method


# virtual methods
.method public isLockout()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mLockout:Z

    return v0
.end method

.method public unlockVerify(Ljava/lang/String;I)Z
    .locals 7

    const/4 v6, 0x0

    const/4 v3, 0x1

    :try_start_0
    iget-boolean v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mLockout:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v1, p1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->checkPassword(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    if-lez p2, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/keyguard/PasswordVerifyHelper$1;

    invoke-direct {v2, p0}, Lcom/android/keyguard/PasswordVerifyHelper$1;-><init>(Lcom/android/keyguard/PasswordVerifyHelper;)V

    int-to-long v4, p2

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_1
    return v3

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->keyguardDone(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportFailedUnlockAttempt()V

    iget-object v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mLockout:Z

    iget-object v1, p0, Lcom/android/keyguard/PasswordVerifyHelper;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
