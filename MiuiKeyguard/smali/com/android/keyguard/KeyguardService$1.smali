.class Lcom/android/keyguard/KeyguardService$1;
.super Lcom/android/internal/policy/IKeyguardService$Stub;
.source "KeyguardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/KeyguardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final TRANSACTION_onStartedWakingUpReason:I = 0xff


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/KeyguardService;


# direct methods
.method constructor <init>(Lcom/android/keyguard/KeyguardService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-direct {p0}, Lcom/android/internal/policy/IKeyguardService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public OnDoubleClickHome()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->OnDoubleClickHome()V

    return-void
.end method

.method public addStateMonitorCallback(Lcom/android/internal/policy/IKeyguardStateCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->addStateMonitorCallback(Ljava/lang/Object;)V

    return-void
.end method

.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->dismiss()V

    return-void
.end method

.method public dismiss(Lcom/android/internal/policy/IKeyguardDismissCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForO;->addDismissCallback(Lcom/android/internal/policy/IKeyguardDismissCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->dismiss()V

    return-void
.end method

.method public dismiss(Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/keyguard/KeyguardService$1;->dismiss()V

    return-void
.end method

.method public dispatch(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->dispatch(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public doKeyguardTimeout(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->doKeyguardTimeout(Landroid/os/Bundle;)V

    return-void
.end method

.method public isDismissable()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isDismissable()Z

    move-result v0

    return v0
.end method

.method public isInputRestricted()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isInputRestricted()Z

    move-result v0

    return v0
.end method

.method public isSecure()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    return v0
.end method

.method public isShowingAndNotHidden()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v0

    return v0
.end method

.method public isShowingAndNotOccluded()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/KeyguardService$1;->isShowingAndNotHidden()Z

    move-result v0

    return v0
.end method

.method public keyguardDone(ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->keyguardDone(ZZ)V

    return-void
.end method

.method public launchCamera()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->launchCamera()V

    return-void
.end method

.method public onActivityDrawn()V
    .locals 0

    return-void
.end method

.method public onBootCompleted()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->onBootCompleted()V

    return-void
.end method

.method public onDreamingStarted()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->onDreamingStarted()V

    return-void
.end method

.method public onDreamingStopped()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->onDreamingStopped()V

    return-void
.end method

.method public onFinishedGoingToSleep(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public onFinishedGoingToSleep(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public onFinishedWakingUp()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public onScreenTurnedOff()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public onScreenTurnedOn()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public onScreenTurningOff()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public onScreenTurningOn(Lcom/android/internal/policy/IKeyguardDrawnCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->onScreenTurnedOn(Ljava/lang/Object;)V

    return-void
.end method

.method public onShortPowerPressedGoHome()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public onStartedGoingToSleep(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->setScreenTurnOnDelayed(Z)V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->onScreenTurnedOff(I)V

    return-void
.end method

.method public onStartedWakingUp(Ljava/lang/String;)J
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "dipper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isFaceUnlockInited()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFaceUnlock()V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->setScreenTurnOnDelayed(Z)V

    const-wide/16 v0, 0x1f4

    return-wide v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->setScreenTurnOnDelayed(Z)V

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onStartedWakingUp()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public onSystemReady()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->onSystemReady()V

    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/internal/policy/IKeyguardService$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    return v1

    :pswitch_0
    const-string/jumbo v1, "com.android.internal.policy.IKeyguardService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/KeyguardService$1;->onStartedWakingUp(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    const/4 v1, 0x1

    return v1

    :pswitch_data_0
    .packed-switch 0xff
        :pswitch_0
    .end packed-switch
.end method

.method public setCurrentUser(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setCurrentUser(I)V

    return-void
.end method

.method public setHidden(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setHidden(Z)V

    return-void
.end method

.method public setKeyguardEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setKeyguardEnabled(Z)V

    return-void
.end method

.method public setOccluded(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/keyguard/KeyguardService$1;->setHidden(Z)V

    return-void
.end method

.method public setOccluded(ZZ)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/keyguard/KeyguardService$1;->setOccluded(Z)V

    return-void
.end method

.method public setSwitchingUser(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    return-void
.end method

.method public showAssistant()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showAssistant()V

    return-void
.end method

.method public startKeyguardExitAnimation(JJ)V
    .locals 0

    return-void
.end method

.method public verifyUnlock(Lcom/android/internal/policy/IKeyguardExitCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-virtual {v0}, Lcom/android/keyguard/KeyguardService;->checkPermission()V

    iget-object v0, p0, Lcom/android/keyguard/KeyguardService$1;->this$0:Lcom/android/keyguard/KeyguardService;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardService;->-get0(Lcom/android/keyguard/KeyguardService;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->verifyUnlock(Lcom/android/internal/policy/IKeyguardExitCallback;)V

    return-void
.end method
