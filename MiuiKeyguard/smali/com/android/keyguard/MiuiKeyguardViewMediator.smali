.class public Lcom/android/keyguard/MiuiKeyguardViewMediator;
.super Ljava/lang/Object;
.source "MiuiKeyguardViewMediator.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardViewCallback;
.implements Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiKeyguardViewMediator$10;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$11;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$12;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$13;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$14;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$15;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$16;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$17;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$1;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$2;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$3;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$4;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$5;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$6;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$7;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$8;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$9;,
        Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-internal-telephony-IccCardConstants$StateSwitchesValues:[I = null

.field private static final ACTION_BLUETOOTH_DEVICE_UNBIND:Ljava/lang/String; = "com.xiaomi.hm.health.ACTION_DEVICE_UNBIND_APPLICATION"

.field private static final ACTION_LOCKSCREEN_MAGAZINE_ENTRANCE:Ljava/lang/String; = "lockscreen.action.LOCKSCREEN_MAGAZINE_ENTRANCE"

.field private static final ACTION_NAVIGATION_BAR_VISIBILITY:Ljava/lang/String; = "com.miui.lockscreen.navigation_bar_visibility"

.field private static final ACTION_SHOW_UNLOCK_SCREEN:Ljava/lang/String; = "xiaomi.intent.action.SHOW_SECURE_KEYGUARD"

.field protected static final AWAKE_INTERVAL_DEFAULT_MS:I = 0x2710

.field private static final BLE_STATUSBAR_ICON_SLOT_NAME:Ljava/lang/String; = "ble_unlock_mode"

.field private static final BLE_UNLOCK_AVAILABLE_VALUE:I = -0x46

.field private static final DBG_WAKE:Z = false

.field private static final DEBUG:Z = false

.field private static final DEBUG_BLE:Z = true

.field private static final DELAYED_KEYGUARD_ACTION:Ljava/lang/String; = "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

.field private static final ENABLE_INSECURE_STATUS_BAR_EXPAND:Z = true

.field private static final EXTRA_SHOW_ENTRANCE:Ljava/lang/String; = "lockscreen.extra.IS_SHOW"

.field private static final FINGERPRINT_STATE_CANCELLING:I = 0x2

.field private static final FINGERPRINT_STATE_CANCELLING_RESTARTING:I = 0x3

.field private static final FINGERPRINT_STATE_RUNNING:I = 0x1

.field public static final FINGERPRINT_STATE_STOPPED:I = 0x0

.field private static final FINGERPRINT_TAG:Ljava/lang/String; = "keyguard_fingerprint"

.field private static final FSG_STATE_CHANGE_ACTION:Ljava/lang/String; = "com.android.systemui.fsgesture"

.field private static final HIDE:I = 0x3

.field private static final IS_FINGERPRINT_UNLOCK:Ljava/lang/String; = "is_fingerprint_unlock"

.field private static final KEYGUARD_DISPLAY_TIMEOUT_DELAY_DEFAULT:I = 0x7530

.field private static final KEYGUARD_DONE:I = 0x9

.field private static final KEYGUARD_DONE_AUTHENTICATING:I = 0xb

.field private static final KEYGUARD_DONE_DRAWING:I = 0xa

.field private static final KEYGUARD_DONE_DRAWING_TIMEOUT_MS:I = 0x7d0

.field private static final KEYGUARD_LOCK_AFTER_DELAY_DEFAULT:I = 0x1388

.field private static final KEYGUARD_TIMEOUT:I = 0xd

.field private static final NOTIFY_SCREEN_OFF:I = 0x6

.field private static final NOTIFY_SCREEN_ON:I = 0x7

.field private static final PICK_UP_GESTURE_WAKEUP_MODE:Ljava/lang/String; = "pick_up_gesture_wakeup_mode"

.field private static final RESET:I = 0x4

.field private static final SET_CURRENT_USER:I = 0xf

.field private static final SET_HIDDEN:I = 0xc

.field private static final SHOW:I = 0x2

.field public static final SHOW_LOCKSCREEN_MAGAZINE_ENTRANCE:Ljava/lang/String; = "show_lockscreen_magazine_entrance"

.field private static final TAG:Ljava/lang/String; = "KeyguardViewMediator"

.field public static final TAG_FOR_CTS_LOG:Ljava/lang/String; = "KeyguardDismissLoggerCallback"

.field private static final TIMEOUT:I = 0x1

.field private static final TYPE_FROM_KEYGUARD:Ljava/lang/String; = "typefrom_keyguard"

.field private static final VERIFY_UNLOCK:I = 0x5

.field private static final WAKEUP_AND_SLEEP_SENSOR:I = 0x1fa263e

.field private static final WAKE_WHEN_READY:I = 0x8

.field private static sFingerprintRunningState:I

.field private static final sWorker:Landroid/os/Handler;

.field private static final sWorkerThread:Landroid/os/HandlerThread;

.field public static showTryAgainMessageInDefault:Z


# instance fields
.field private mActivityManager:Landroid/app/ActivityManager;

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBLEState:I

.field private final mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mBroadCastReceiver:Landroid/content/BroadcastReceiver;

.field private mCharging:Z

.field private mContext:Landroid/content/Context;

.field private mDayOfStatistics:I

.field private mDelayedShowingSequence:I

.field private final mDevicePowerReceiver:Landroid/content/BroadcastReceiver;

.field mDisableInfo:Z

.field mDisableNaviation:Z

.field private mDisplay:Landroid/view/Display;

.field private mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;

.field private mExternallyEnabled:Z

.field private mFaceUnlockApplyLock:Z

.field mFaceUnlockApplyObserver:Landroid/database/ContentObserver;

.field mFaceUnlockFeatureObserver:Landroid/database/ContentObserver;

.field mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

.field private mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

.field private final mFingerprintStateCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

.field private mFull:Z

.field mFullScreenGestureObserver:Landroid/database/ContentObserver;

.field private final mGlobalBluetoothBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mHandler:Landroid/os/Handler;

.field private mHasFaceUnlockData:Z

.field private mHidden:Z

.field mInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

.field private mInitAllRunnable:Ljava/lang/Runnable;

.field private mInputRestricted:Z

.field private mIsFullScreenGestureOpened:Z

.field private mIsSmartCoverClosed:Z

.field private mIsSmartCoverFullMode:Z

.field private mIsSmartCoverLatticeMode:Z

.field private mKeyguardDone:Z

.field private mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

.field private mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

.field private mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

.field private mLevel:I

.field private mListener:Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;

.field private mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

.field private mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

.field private mLockSoundId:I

.field private mLockSoundStreamId:I

.field private final mLockSoundVolume:F

.field private mLockSounds:Landroid/media/SoundPool;

.field private mMasterStreamType:I

.field private mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

.field private final mMusicStateChangeListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;

.field private mNeedToReshowWhenReenabled:Z

.field private mPM:Landroid/os/PowerManager;

.field private mPhoneState:Ljava/lang/String;

.field private mReadyForKeyEvent:Z

.field mScreenOffRunnable:Ljava/lang/Runnable;

.field private mScreenOn:Z

.field private mSendKeyEventScreenOn:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorPickUp:Landroid/hardware/Sensor;

.field private mShouldListenForFingerprint:Z

.field private mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mShowNavigationBar:Z

.field private mShowing:Z

.field private mShowingLockIcon:Z

.field private mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

.field private mStateChangeCallback:Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mSuppressNextLockSound:Z

.field private mSystemReady:Z

.field private mTriggerListener:Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;

.field private mUnlockByFringerPrint:Z

.field private mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

.field private mUnlockSoundId:I

.field private mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field private mUserPresentIntent:Landroid/content/Intent;

.field private mWaitingUntilKeyguardVisible:Z

.field private mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

.field private mWakelockSequence:I

.field private mWakeupAndSleepSensor:Landroid/hardware/Sensor;

.field mWakeupAndSleepSensorListener:Landroid/hardware/SensorEventListener;

.field private mWakeupByPickUp:Z

.field private final mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

.field private mYearOfStatistics:I


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiKeyguardViewMediator;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mBLEState:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mCharging:Z

    return v0
.end method

.method static synthetic -get10(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsFullScreenGestureOpened:Z

    return v0
.end method

.method static synthetic -get11(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardViewManager;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    return-object v0
.end method

.method static synthetic -get12(Lcom/android/keyguard/MiuiKeyguardViewMediator;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLevel:I

    return v0
.end method

.method static synthetic -get13(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mListener:Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;

    return-object v0
.end method

.method static synthetic -get14(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/security/MiuiLockPatternUtils;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    return-object v0
.end method

.method static synthetic -get15(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/os/PowerManager;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic -get16(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPhoneState:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOn:Z

    return v0
.end method

.method static synthetic -get18(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    return v0
.end method

.method static synthetic -get19(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/smartcover/SmartCoverView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get20(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lmiui/bluetooth/ble/MiBleUnlockProfile;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    return-object v0
.end method

.method static synthetic -get21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    return-object v0
.end method

.method static synthetic -get22(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic -get23(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupByPickUp:Z

    return v0
.end method

.method static synthetic -get24()I
    .locals 1

    sget v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sFingerprintRunningState:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiKeyguardViewMediator;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDelayedShowingSequence:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/view/Display;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDisplay:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExternallyEnabled:Z

    return v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFull:Z

    return v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHasFaceUnlockData:Z

    return v0
.end method

.method static synthetic -get9(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    return v0
.end method

.method private static synthetic -getcom-android-internal-telephony-IccCardConstants$StateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-com-android-internal-telephony-IccCardConstants$StateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-com-android-internal-telephony-IccCardConstants$StateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/internal/telephony/IccCardConstants$State;->values()[Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->CARD_IO_ERROR:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->CARD_RESTRICTED:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->NETWORK_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_8
    :try_start_9
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    sput-object v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-com-android-internal-telephony-IccCardConstants$StateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1

    :catch_9
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mCharging:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockApplyLock:Z

    return p1
.end method

.method static synthetic -set10(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mReadyForKeyEvent:Z

    return p1
.end method

.method static synthetic -set11(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSendKeyEventScreenOn:Z

    return p1
.end method

.method static synthetic -set12(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowNavigationBar:Z

    return p1
.end method

.method static synthetic -set13(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSuppressNextLockSound:Z

    return p1
.end method

.method static synthetic -set14(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockByFringerPrint:Z

    return p1
.end method

.method static synthetic -set15(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupByPickUp:Z

    return p1
.end method

.method static synthetic -set2(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFull:Z

    return p1
.end method

.method static synthetic -set3(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHasFaceUnlockData:Z

    return p1
.end method

.method static synthetic -set4(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsFullScreenGestureOpened:Z

    return p1
.end method

.method static synthetic -set5(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverClosed:Z

    return p1
.end method

.method static synthetic -set6(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverFullMode:Z

    return p1
.end method

.method static synthetic -set7(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverLatticeMode:Z

    return p1
.end method

.method static synthetic -set8(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)I
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLevel:I

    return p1
.end method

.method static synthetic -set9(Lcom/android/keyguard/MiuiKeyguardViewMediator;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPhoneState:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->doKeyguardLocked()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->hasStartFingerprintIdentify()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap10(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleKeyguardDoneDrawing()V

    return-void
.end method

.method static synthetic -wrap11(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleKeyguardDone(Z)V

    return-void
.end method

.method static synthetic -wrap12(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleNotifyScreenOff(I)V

    return-void
.end method

.method static synthetic -wrap13(Lcom/android/keyguard/MiuiKeyguardViewMediator;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleNotifyScreenOn(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic -wrap14(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleReset()V

    return-void
.end method

.method static synthetic -wrap15(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleSetCurrentUser(I)V

    return-void
.end method

.method static synthetic -wrap16(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleSetHidden(Z)V

    return-void
.end method

.method static synthetic -wrap17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleShow()V

    return-void
.end method

.method static synthetic -wrap18(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleVerifyUnlock()V

    return-void
.end method

.method static synthetic -wrap19(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleWakeWhenReady(I)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->hideLockForMode(I)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap20(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->recordKeyguardSettingsStatistics()V

    return-void
.end method

.method static synthetic -wrap21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->recordLockScreenMagazine()V

    return-void
.end method

.method static synthetic -wrap22(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->registerFaceUnlockContentObserver()V

    return-void
.end method

.method static synthetic -wrap23(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sendKeyEvent()V

    return-void
.end method

.method static synthetic -wrap24(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sendReEntryFaceUnlockNotification()V

    return-void
.end method

.method static synthetic -wrap25(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setBLEStatusBarIcon(I)V

    return-void
.end method

.method static synthetic -wrap26(Lcom/android/keyguard/MiuiKeyguardViewMediator;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setLockScreenPropertiesForA7(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap27(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showLocked()V

    return-void
.end method

.method static synthetic -wrap28(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateInputRestrictedLocked()V

    return-void
.end method

.method static synthetic -wrap29(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->verifyBLEDeviceRssi()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecondFingerprint(I)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap4(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->needRefreshSmartCoverWindow(I)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap5(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->adjustStatusBarLocked()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/keyguard/MiuiKeyguardViewMediator;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->connectBLEDevice(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->disconnectBleDeviceIfNecessary()V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleHide()V

    return-void
.end method

.method static synthetic -wrap9(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleHide(Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sFingerprintRunningState:I

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "keyguard-work-thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sWorkerThread:Landroid/os/HandlerThread;

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sWorker:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 20

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupAndSleepSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSuppressNextLockSound:Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExternallyEnabled:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardDone:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupByPickUp:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOn:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsFullScreenGestureOpened:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mReadyForKeyEvent:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSendKeyEventScreenOn:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockByFringerPrint:Z

    sget-object v2, Lmiui/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPhoneState:Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mBLEState:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowNavigationBar:Z

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStateChangeCallback:Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$2;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mListener:Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$3;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDevicePowerReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$5;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$6;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOffRunnable:Ljava/lang/Runnable;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$7;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator$7;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockFeatureObserver:Landroid/database/ContentObserver;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$8;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator$8;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockApplyObserver:Landroid/database/ContentObserver;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$9;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator$9;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFullScreenGestureObserver:Landroid/database/ContentObserver;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$10;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$10;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$11;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$11;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMusicStateChangeListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$12;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$12;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mGlobalBluetoothBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$13;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$13;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mInitAllRunnable:Ljava/lang/Runnable;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mBroadCastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mYearOfStatistics:I

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDayOfStatistics:I

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$16;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$16;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupAndSleepSensorListener:Landroid/hardware/SensorEventListener;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFingerprintStateCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShouldListenForFingerprint:Z

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/AnalyticsHelper;->init(Landroid/content/Context;)V

    new-instance v2, Lcom/android/keyguard/KeyguardViewMediatorHelper;

    invoke-direct {v2}, Lcom/android/keyguard/KeyguardViewMediatorHelper;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

    const-string/jumbo v2, "sys.keyguard.bleunlock"

    const-string/jumbo v3, "true"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    new-instance v2, Lcom/android/keyguard/FingerprintHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/keyguard/FingerprintHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    const-string/jumbo v2, "power"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    const-string/jumbo v3, "show keyguard"

    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    const-string/jumbo v3, "keyguardWakeAndHandOff"

    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v2, "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "xiaomi.intent.action.SHOW_SECURE_KEYGUARD"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.action.PHONE_STATE"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "com.miui.lockscreen.navigation_bar_visibility"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "lockscreen.action.LOCKSCREEN_MAGAZINE_ENTRANCE"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "face_unlock_release"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "miui.intent.action.MIUI_REGION_CHANGED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mBroadCastReceiver:Landroid/content/BroadcastReceiver;

    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    const-string/jumbo v2, "alarm"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mAlarmManager:Landroid/app/AlarmManager;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    invoke-virtual {v2, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerInfoCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMusicStateChangeListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;

    invoke-virtual {v2, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerMusicStateChangeListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;)V

    new-instance v2, Landroid/security/MiuiLockPatternUtils;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    new-instance v2, Lcom/android/keyguard/LockPatternUtilsWrapper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    invoke-direct {v2, v3}, Lcom/android/keyguard/LockPatternUtilsWrapper;-><init>(Lcom/android/internal/widget/LockPatternUtils;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/keyguard/LockPatternUtilsWrapper;->setCurrentUser(I)V

    new-instance v2, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v4, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;-><init>(Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->shouldWaitForProvisioning()Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v2}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isLockScreenDisabled()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateShowingLocked(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateInputRestrictedLocked()V

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->reportKeyguardShowingChanged()V

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateActivityLockScreenState()V

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/android/keyguard/MiuiKeyguardViewManager;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/MiuiKeyguardViewProperties;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mActivityManager:Landroid/app/ActivityManager;

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.USER_PRESENT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v16

    new-instance v2, Landroid/media/SoundPool;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v6}, Landroid/media/SoundPool;-><init>(III)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    const-string/jumbo v2, "lock_sound"

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSoundId:I

    :cond_0
    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSoundId:I

    :cond_1
    const-string/jumbo v2, "unlock_sound"

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockSoundId:I

    :cond_2
    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockSoundId:I

    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x10e0057

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v17

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    div-int/lit8 v4, v17, 0x14

    int-to-double v6, v4

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSoundVolume:F

    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v2, "miui.intent.action.ACTION_SHOW_LOCK_SCREEN"

    invoke-virtual {v9, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "miui.intent.action.SMART_COVER"

    invoke-virtual {v9, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDevicePowerReceiver:Landroid/content/BroadcastReceiver;

    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string/jumbo v10, "android.permission.DEVICE_POWER"

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string/jumbo v6, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v13, Landroid/content/IntentFilter;

    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v2, "com.miui.keyguard.bluetoothdeviceunlock"

    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "com.miui.keyguard.bluetoothdeviceunlock.disable"

    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "com.xiaomi.hm.health.ACTION_DEVICE_UNBIND_APPLICATION"

    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mGlobalBluetoothBroadcastReceiver:Landroid/content/BroadcastReceiver;

    sget-object v12, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v10 .. v15}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v2}, Landroid/security/MiuiLockPatternUtils;->getBluetoothAddressToUnlock()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->connectBLEDevice(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "statusbar"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/StatusBarManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mTriggerListener:Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerSimStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    invoke-virtual {v2, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerWallpaperChangeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;)V

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->registerFaceUnlockContentObserver()V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p0

    invoke-static {v2, v3, v0}, Lcom/android/keyguard/MiuiGxzwManager;->init(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    :cond_4
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->hasNavigationBar()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "force_fsg_nav_bar"

    invoke-static {v3}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFullScreenGestureObserver:Landroid/database/ContentObserver;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFullScreenGestureObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/WindowManager;

    invoke-interface/range {v19 .. v19}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDisplay:Landroid/view/Display;

    return-void

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private adjustStatusBarLocked()V
    .locals 12

    const/high16 v11, 0x800000

    const/high16 v10, 0x10000

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v7, "statusbar"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/StatusBarManager;

    iput-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    :cond_0
    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    if-eqz v6, :cond_8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_7

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v6

    if-eqz v6, :cond_1

    const/high16 v6, 0x2080000

    or-int/lit8 v2, v6, 0x10

    :cond_1
    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const/4 v7, -0x2

    invoke-static {v6, v7}, Landroid/app/MiuiStatusBarManager;->isExpandableUnderKeyguardForUser(Landroid/content/Context;I)Z

    move-result v6

    if-nez v6, :cond_2

    or-int/2addr v2, v10

    :cond_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isDefaultLockScreenTheme()Z

    move-result v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v7, "audio"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v6}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isMusicActive()Z

    move-result v3

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "show_lock_before_unlock"

    sget-boolean v8, Landroid/provider/MiuiSettings$System;->SHOW_LOCK_BEFORE_UNLOCK_DEFAULT:Z

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v4

    const/4 v1, 0x0

    if-nez v3, :cond_3

    const/4 v1, 0x1

    :cond_3
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v6

    if-eqz v6, :cond_4

    xor-int/lit8 v6, v4, 0x1

    if-eqz v6, :cond_4

    const/4 v1, 0x1

    :cond_4
    if-eqz v1, :cond_5

    or-int/2addr v2, v11

    :cond_5
    :goto_0
    iget-boolean v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDisableNaviation:Z

    if-eqz v6, :cond_6

    iget-boolean v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowNavigationBar:Z

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_6

    const/high16 v6, 0x1000000

    or-int/2addr v2, v6

    const/high16 v6, 0x200000

    or-int/2addr v2, v6

    const/high16 v6, 0x400000

    or-int/2addr v2, v6

    :cond_6
    iget-boolean v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDisableInfo:Z

    if-eqz v6, :cond_7

    const/high16 v6, 0x100000

    or-int/2addr v2, v6

    or-int/2addr v2, v10

    :cond_7
    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    invoke-virtual {v6, v2}, Landroid/app/StatusBarManager;->disable(I)V

    :cond_8
    return-void

    :cond_9
    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x110a0019

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-nez v5, :cond_5

    or-int/2addr v2, v11

    goto :goto_0
.end method

.method private cancelTriggerSensor()V
    .locals 3

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "jason"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->getPickUpSensor()Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isTriggerSensorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mTriggerListener:Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->getPickUpSensor()Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->cancelTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    :cond_0
    return-void
.end method

.method private connectBLEDevice(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isSecure()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v1}, Landroid/security/MiuiLockPatternUtils;->getBluetoothUnlockEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->FAILED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    invoke-virtual {v1}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v1, Lmiui/bluetooth/ble/MiBleUnlockProfile;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStateChangeCallback:Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;

    invoke-direct {v1, v2, p1, v3}, Lmiui/bluetooth/ble/MiBleUnlockProfile;-><init>(Landroid/content/Context;Ljava/lang/String;Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    invoke-virtual {v1}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->connect()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "KeyguardViewMediator"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private disableFullScreenGesture(Z)V
    .locals 3

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsFullScreenGestureOpened:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.android.systemui"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "com.android.systemui.fsgesture"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "typeFrom"

    const-string/jumbo v2, "typefrom_keyguard"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "isEnter"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private disconnectBleDeviceIfNecessary()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    invoke-virtual {v1}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->unregisterUnlockListener()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    invoke-virtual {v1}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->disconnect()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->FAILED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    const-string/jumbo v2, "ble_unlock_mode"

    invoke-virtual {v1, v2}, Landroid/app/StatusBarManager;->removeIcon(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "KeyguardViewMediator"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private doKeyguardLocked()Z
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExternallyEnabled:Z

    if-nez v3, :cond_0

    const-string/jumbo v3, "KeyguardViewMediator"

    const-string/jumbo v4, "doKeyguard: not showing because externally disabled"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v5

    :cond_0
    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiKeyguardViewManager;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v3, "KeyguardViewMediator"

    const-string/jumbo v4, "doKeyguard: not showing because it is already showing"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isLockedOrMissing()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->shouldWaitForProvisioning()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v3, "KeyguardViewMediator"

    const-string/jumbo v4, "doKeyguard: not showing because device isn\'t provisioned and the sim is not locked or missing"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v5

    :cond_2
    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v3}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isLockScreenDisabled()Z

    move-result v3

    if-eqz v3, :cond_3

    xor-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_3

    const-string/jumbo v3, "KeyguardViewMediator"

    const-string/jumbo v4, "doKeyguard: not showing because lockscreen is off"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v5

    :cond_3
    const-string/jumbo v3, "sys.keyguard.screen_off_by_lid"

    const-string/jumbo v4, "false"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string/jumbo v3, "persist.sys.smartcover_mode"

    const/4 v4, -0x1

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->hideLockForMode(I)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v1, :cond_5

    xor-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_5

    const-string/jumbo v3, "KeyguardViewMediator"

    const-string/jumbo v4, "Not showing lock screen since in smart cover mode"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eqz v3, :cond_4

    invoke-direct {p0, v6}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleHide(Z)V

    :cond_4
    return v5

    :cond_5
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showLocked()V

    return v6
.end method

.method private getPickUpSensor()Landroid/hardware/Sensor;
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorPickUp:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorPickUp:Landroid/hardware/Sensor;

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorPickUp:Landroid/hardware/Sensor;

    return-object v0
.end method

.method private handleHide()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleHide(Z)V

    return-void
.end method

.method private handleHide(Z)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "KeyguardViewMediator"

    const-string/jumbo v1, "attempt to hide the keyguard while waking, ignored"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    :try_start_2
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isScreenTurnOnDelayed()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "is_fingerprint_unlock"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewManager;->clearWindowExitAnimation()V

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardViewMediator$28;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$28;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->runOnFaceUnlockWorkerThread(Ljava/lang/Runnable;)V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    if-nez v0, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_3

    invoke-static {}, Lcom/android/keyguard/KeyguardCompatibilityHelperForN;->keyguardGoingAway()V

    :cond_3
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewManager;->hide()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->setScreenTurnOnDelayed(Z)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->stopFingerprintIdentifyImmediately()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->FAILED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    invoke-virtual {v0}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->unregisterUnlockListener()V

    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateShowingLocked(Z)V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockByFringerPrint:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isTopActivitySystemApp(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockByFringerPrint:Z

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mReadyForKeyEvent:Z

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sendKeyEvent()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mReadyForKeyEvent:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSendKeyEventScreenOn:Z

    :cond_5
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateActivityLockScreenState()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->adjustStatusBarLocked()V

    const-string/jumbo v0, "KeyguardDismissLoggerCallback"

    const-string/jumbo v1, "onDismissSucceeded"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/keyguard/KeyguardCompatibilityHelperForO;->isDismissCallbackExisted()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardViewMediator$29;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$29;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->runOnWorkerThread(Ljava/lang/Runnable;J)V

    :cond_6
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->disableFullScreenGesture(Z)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->unregisterWakeupAndSleepSensor()V

    sget-object v0, Lmiui/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPhoneState:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    xor-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_7

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardViewMediator$30;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$30;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->runOnWorkerThread(Ljava/lang/Runnable;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_7
    monitor-exit p0

    return-void

    :cond_8
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSendKeyEventScreenOn:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private handleKeyguardDone(Z)V
    .locals 10

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleHide()V

    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/PowerManager;->userActivity(JZ)V

    :cond_0
    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    const-string/jumbo v4, "ble_unlock_mode"

    invoke-virtual {v3, v4}, Landroid/app/StatusBarManager;->removeIcon(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    new-instance v5, Landroid/os/UserHandle;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v6

    invoke-direct {v5, v6}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "com.miui.app.ExtraStatusBarManager.action_remove_keyguard_notification"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/os/UserHandle;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v3}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->getPendingIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iput-object v7, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    :cond_2
    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportFaceUnlock(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string/jumbo v3, "face_unlock"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "keyguard dismiss time="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-wide v8, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->sStageInFaceUnlockTime:J

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v3, "face_unlock"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "face unlock time="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-wide v8, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sScreenTurnedOnTime:J

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v3, Lcom/android/keyguard/MiuiKeyguardViewMediator$24;

    invoke-direct {v3, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$24;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-wide/16 v4, 0x0

    invoke-static {v3, v4, v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->runOnWorkerThread(Ljava/lang/Runnable;J)V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiGxzwManager;->keyguardDone()V

    :cond_4
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private handleKeyguardDoneDrawing()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->notifyAll()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private handleNotifyScreenOff(I)V
    .locals 3

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mReadyForKeyEvent:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSendKeyEventScreenOn:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->requestTriggerSensor()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardViewManager;->onScreenTurnedOff()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->onScreenTurnedOff()V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiGxzwManager;->onScreenTurnedOff()V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    invoke-virtual {v1}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->unregisterUnlockListener()V

    :cond_1
    const/4 v1, 0x4

    if-eq p1, v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "security"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManager;

    if-eqz v0, :cond_2

    const-string/jumbo v1, "*"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lmiui/security/SecurityManager;->removeAccessControlPassAsUser(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private handleNotifyScreenOn(Ljava/lang/Object;)V
    .locals 6

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mReadyForKeyEvent:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->cancelTriggerSensor()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOffRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/smartcover/SmartCoverView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOffRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/keyguard/smartcover/SmartCoverView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->refreshSmartCover()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewManager;->onScreenTurnedOn(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->onScreenTurnedOn()V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwManager;->onScreenTurnedOn()V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->FAILED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardViewMediator$32;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$32;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->runOnWorkerThread(Ljava/lang/Runnable;J)V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSendKeyEventScreenOn:Z

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$33;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$33;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-ge v0, v3, :cond_3

    const/16 v0, 0x12c

    :goto_0
    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private handleReset()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewManager;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private handleSetCurrentUser(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->setCurrentUser(I)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->adjustStatusBarLocked()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->handleReset()V

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->setUserAuthenticatedSinceBootSecond()V

    :cond_0
    return-void
.end method

.method private handleSetHidden(Z)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    if-eq v2, p1, :cond_1

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-ge v2, v3, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateActivityLockScreenState()V

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->adjustStatusBarLocked()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->refreshSmartCover()V

    if-eqz p1, :cond_2

    const/4 v2, -0x1

    :goto_0
    int-to-long v0, v2

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v2, v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewManager;->setUserActivityTimeout(J)V

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    invoke-virtual {v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewManager;->setHidden(Z)V

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->stopFaceUnlock()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUtils;->isTopActivitySystemApp(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v2, "miui_keyguard_fingerprint"

    const-string/jumbo v3, "stop fingerprint identify in handleSetHidden"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->stopFingerprintIdentify()V

    :goto_1
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->disableFullScreenGesture(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    const/16 v2, 0x2710

    goto :goto_0

    :cond_3
    :try_start_1
    const-string/jumbo v2, "miui_keyguard_fingerprint"

    const-string/jumbo v3, "start fingerprint identify in handleSetHidden"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFingerprintIdentify()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_4
    :try_start_2
    const-string/jumbo v2, "miui_keyguard_fingerprint"

    const-string/jumbo v3, "start fingerprint identify in not hidden state"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFingerprintIdentify()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardViewManager;->startFaceUnlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private handleShow()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSystemReady:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardViewMediator$27;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$27;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->runOnWorkerThread(Ljava/lang/Runnable;J)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowNavigationBar:Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardViewManager;->show()V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateShowingLocked(Z)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateActivityLockScreenState()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->adjustStatusBarLocked()V

    invoke-static {}, Lcom/android/keyguard/KeyguardCompatibilityHelper;->closeSystemDialogs()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->disableFullScreenGesture(Z)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->registerWakeupAndSleepSensor()V

    const-string/jumbo v2, "miui_keyguard_time"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " handleShow :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private handleVerifyUnlock()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateShowingLocked(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewManager;->verifyUnlock()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateActivityLockScreenState()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private handleWakeWhenReady(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewManager;->wakeWhenReadyTq(I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "KeyguardViewMediator"

    const-string/jumbo v1, "mKeyguardViewManager.wakeWhenReadyTq did not poke wake lock, so poke it ourselves"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->pokeWakelock()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private hasStartFingerprintIdentify()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-ge v2, v3, :cond_0

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v3, "ido"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    sget v2, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sFingerprintRunningState:I

    if-ne v2, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    return v1
.end method

.method private hideLockForMode(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq v0, p1, :cond_0

    const/4 v1, 0x3

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x4

    if-eq v1, p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideLocked()V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private isInCallOrClockShowing()Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v2, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    return v3

    :cond_0
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_1

    move v2, v3

    :goto_0
    return v2

    :cond_1
    const-string/jumbo v2, "com.android.incallui.InCallActivity"

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "com.android.deskclock.activity.AlarmAlertFullScreenActivity"

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_0
.end method

.method private isKeyguardPasswordSecured()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getActivePasswordQuality()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isSecondFingerprint(I)Z
    .locals 6

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "second_user_id"

    const/16 v5, -0x2710

    invoke-static {v3, v4, v5, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Landroid/security/FingerprintIdUtils;->getUserFingerprintIds(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    :cond_0
    return v2
.end method

.method private isTriggerSensorEnabled()Z
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "pick_up_gesture_wakeup_mode"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    return v0
.end method

.method private isWakeKeyWhenKeyguardShowing(IZ)Z
    .locals 1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x1

    return v0

    :sswitch_0
    return p2

    :sswitch_1
    const/4 v0, 0x0

    return v0

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1b -> :sswitch_1
        0x4f -> :sswitch_1
        0x55 -> :sswitch_1
        0x56 -> :sswitch_1
        0x57 -> :sswitch_1
        0x58 -> :sswitch_1
        0x59 -> :sswitch_1
        0x5a -> :sswitch_1
        0x5b -> :sswitch_1
        0x7e -> :sswitch_1
        0x7f -> :sswitch_1
        0x82 -> :sswitch_1
        0xa4 -> :sswitch_0
    .end sparse-switch
.end method

.method private needRefreshSmartCoverWindow(I)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x3

    if-eq v1, p1, :cond_0

    const/4 v1, 0x4

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyScreenOffLocked(I)V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private notifyScreenOnLocked(Ljava/lang/Object;)V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private onUserSwitched(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->setCurrentUser(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockApplyObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockFeatureObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/database/ContentObserver;->onChange(Z)V

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->resetStateLocked()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private recordKeyguardSettingsStatistics()V
    .locals 14

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mYearOfStatistics:I

    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    if-ne v9, v10, :cond_0

    iget v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDayOfStatistics:I

    const/4 v10, 0x6

    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    if-ne v9, v10, :cond_0

    return-void

    :cond_0
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    iput v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mYearOfStatistics:I

    const/4 v9, 0x6

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    iput v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDayOfStatistics:I

    const-string/jumbo v7, "unsecure"

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v9}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getActivePasswordQuality()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_1
    :goto_0
    sget-object v9, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SECURE_TYPE:Ljava/lang/String;

    invoke-static {v9, v7}, Lcom/android/keyguard/AnalyticsHelper;->recordEnum(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportFaceUnlock(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string/jumbo v8, "unsecure"

    iget-boolean v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHasFaceUnlockData:Z

    if-eqz v9, :cond_6

    iget-boolean v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockApplyLock:Z

    if-eqz v9, :cond_6

    const-string/jumbo v8, "enable"

    :cond_2
    :goto_1
    sget-object v9, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_STATE:Ljava/lang/String;

    invoke-static {v9, v8}, Lcom/android/keyguard/AnalyticsHelper;->recordEnum(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v9, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_NOTIFICATION_TOGGLE:Ljava/lang/String;

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "face_unlock_by_notification_screen_on"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v12

    const/4 v13, 0x0

    invoke-static {v10, v11, v13, v12}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/keyguard/AnalyticsHelper;->recordEnum(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "wakeup_for_keyguard_notification"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v11

    const/4 v12, 0x1

    invoke-static {v9, v10, v12, v11}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v6

    sget-object v9, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SCREEN_ON_BY_NOTIFICATION_TOGGLE:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/keyguard/AnalyticsHelper;->recordEnum(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/security/MiuiLockPatternUtils;

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-direct {v1, v9}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    new-instance v9, Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-direct {v9, v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;-><init>(Lcom/android/internal/widget/LockPatternUtils;)V

    invoke-virtual {v9}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isOwnerInfoEnabled()Z

    move-result v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/security/MiuiLockPatternUtils;->getOwnerInfo(I)Ljava/lang/String;

    move-result-object v3

    sget-object v10, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_HAS_OWNER_INFO:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    :goto_2
    invoke-static {v9}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v9

    invoke-static {v10, v9}, Lcom/android/keyguard/AnalyticsHelper;->recordEnum(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "screen_off_timeout"

    const-wide/16 v12, 0x7530

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v11

    invoke-static {v9, v10, v12, v13, v11}, Landroid/provider/Settings$System;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    move-result-wide v4

    sget-object v9, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_AUTO_LOCK_TIME_VALUE:Ljava/lang/String;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/keyguard/AnalyticsHelper;->recordEnum(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerprintHardwareAvailable(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_5

    const-string/jumbo v8, "not_secure"

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "miui_keyguard"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v11

    const/4 v12, 0x2

    invoke-static {v9, v10, v12, v11}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_8

    const-string/jumbo v8, "enabled"

    :cond_4
    :goto_3
    sget-object v9, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_FINGERPRINT_STATE:Ljava/lang/String;

    invoke-static {v9, v8}, Lcom/android/keyguard/AnalyticsHelper;->recordEnum(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void

    :sswitch_0
    const-string/jumbo v7, "pattern"

    goto/16 :goto_0

    :sswitch_1
    const-string/jumbo v7, "numeric"

    goto/16 :goto_0

    :sswitch_2
    const-string/jumbo v7, "mixed"

    goto/16 :goto_0

    :cond_6
    iget-boolean v9, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHasFaceUnlockData:Z

    if-eqz v9, :cond_2

    const-string/jumbo v8, "disable"

    goto/16 :goto_1

    :cond_7
    const/4 v9, 0x0

    goto :goto_2

    :cond_8
    const-string/jumbo v8, "disabled"

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x30000 -> :sswitch_1
        0x40000 -> :sswitch_2
        0x50000 -> :sswitch_2
        0x60000 -> :sswitch_2
    .end sparse-switch
.end method

.method private recordLockScreenMagazine()V
    .locals 9

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v6}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v0, v5, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->authority:Ljava/lang/String;

    iget-boolean v6, v5, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->isLockScreenMagazine:Z

    if-eqz v6, :cond_0

    const-string/jumbo v6, "com.xiaomi.ad.LockScreenAdProvider"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "content://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    move-result-object v4

    if-eqz v4, :cond_0

    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string/jumbo v6, "key"

    iget-object v7, v5, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->key:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v6, "event"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v6, "request_json"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "recordEvent"

    const/4 v8, 0x0

    invoke-interface {v4, v6, v7, v8, v2}, Landroid/content/IContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    goto :goto_0

    :catchall_0
    move-exception v6

    iget-object v7, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    throw v6
.end method

.method private registerFaceUnlockContentObserver()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportFaceUnlock(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "face_unlock_has_feature"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockFeatureObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockFeatureObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "face_unlcok_apply_for_lock"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockApplyObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockApplyObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    :cond_0
    return-void
.end method

.method private registerWakeupAndSleepSensor()V
    .locals 4

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "polaris"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "dipper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isTriggerSensorEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "KeyguardViewMediator"

    const-string/jumbo v1, "registerWakeupAndSleepSensor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa263e

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupAndSleepSensor:Landroid/hardware/Sensor;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupAndSleepSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupAndSleepSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_1
    return-void
.end method

.method private reportKeyguardShowingChanged()V
    .locals 4

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardViewMediator$26;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$26;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->runOnWorkerThread(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private requestTriggerSensor()V
    .locals 3

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "jason"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->getPickUpSensor()Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isTriggerSensorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mTriggerListener:Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->getPickUpSensor()Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->requestTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    :cond_0
    return-void
.end method

.method private resetStateLocked()V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public static runOnWorkerThread(Ljava/lang/Runnable;J)V
    .locals 3

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    if-ne v0, v1, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sWorker:Landroid/os/Handler;

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private sendKeyEvent()V
    .locals 1

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardViewMediator$31;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$31;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$31;->start()V

    return-void
.end method

.method private sendReEntryFaceUnlockNotification()V
    .locals 7

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v1, "com.android.keyguard"

    const-string/jumbo v2, "com.android.keyguard.settings.MiuiFaceDataInput"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0099

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const v3, 0x7f0b009a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, "com.android.settings"

    const v3, 0x7f02006c

    const/4 v5, 0x1

    invoke-static/range {v0 .. v6}, Lcom/android/keyguard/KeyguardCompatibilityHelperForO;->sendNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;ZLjava/lang/String;)V

    return-void
.end method

.method private sendReEntryFingerprintNotificationIfNeed()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardViewMediator$19;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$19;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private setBLEStatusBarIcon(I)V
    .locals 5

    const/4 v4, 0x0

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mBLEState:I

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isSecure()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v1}, Landroid/security/MiuiLockPatternUtils;->getBluetoothUnlockEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v1}, Landroid/security/MiuiLockPatternUtils;->getBluetoothAddressToUnlock()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isOwnerUser()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isLightWallpaperTop()Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f020005

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    const-string/jumbo v2, "ble_unlock_mode"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v4, v3}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f020004

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_4

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isLightWallpaperTop()Z

    move-result v1

    if-eqz v1, :cond_3

    const v0, 0x7f020009

    goto :goto_0

    :cond_3
    const v0, 0x7f020008

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isLightWallpaperTop()Z

    move-result v1

    if-eqz v1, :cond_5

    const v0, 0x7f020007

    goto :goto_0

    :cond_5
    const v0, 0x7f020006

    goto :goto_0
.end method

.method public static setFingerprintRunningState(I)V
    .locals 0

    sput p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sFingerprintRunningState:I

    return-void
.end method

.method private setLockScreenPropertiesForA7(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "capricorn"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "sys.qfp.lockscreen"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private shouldWaitForProvisioning()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDeviceProvisioned()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showLocked()V
    .locals 3

    const-string/jumbo v1, "miui_keyguard_fingerprint"

    const-string/jumbo v2, "start fingerprint identify in showLocked"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardDone:Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFingerprintIdentify()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->shouldListenForFaceUnlock()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mInitAllRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->runOnFaceUnlockWorkerThread(Ljava/lang/Runnable;)V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public static showTryAgainMessageInDefault()Z
    .locals 1

    sget-boolean v0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showTryAgainMessageInDefault:Z

    return v0
.end method

.method private stopFaceUnlock()V
    .locals 2

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->setScreenTurnOnDelayed(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardViewMediator$22;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$22;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->runOnFaceUnlockWorkerThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private unregisterWakeupAndSleepSensor()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "polaris"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "dipper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupAndSleepSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "KeyguardViewMediator"

    const-string/jumbo v1, "unregisterWakeupAndSleepSensor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupAndSleepSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupAndSleepSensor:Landroid/hardware/Sensor;

    :cond_1
    return-void
.end method

.method private updateActivityLockScreenState()V
    .locals 4

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardViewMediator$25;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$25;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->runOnWorkerThread(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private updateInputRestrictedLocked()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isInputRestricted()Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mInputRestricted:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mInputRestricted:Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/KeyguardViewMediatorHelper;->updateInputRestrictedLocked(Z)V

    :cond_0
    return-void
.end method

.method private updateShowingLocked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/KeyguardViewMediatorHelper;->updateShowingLocked(Z)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateInputRestrictedLocked()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->reportKeyguardShowingChanged()V

    :cond_0
    return-void
.end method

.method private verifyBLEDeviceRssi()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setBLEStatusBarIcon(I)V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v0}, Landroid/security/MiuiLockPatternUtils;->getBluetoothUnlockEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v0}, Landroid/security/MiuiLockPatternUtils;->getBluetoothAddressToUnlock()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v0}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isSecure()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockProfile:Lmiui/bluetooth/ble/MiBleUnlockProfile;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mListener:Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;

    invoke-virtual {v0, v1}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->registerUnlockListener(Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "KeyguardViewMediator"

    const-string/jumbo v1, "connectBLEDevice..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtils:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v0}, Landroid/security/MiuiLockPatternUtils;->getBluetoothAddressToUnlock()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->connectBLEDevice(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private verifyUnlockLocked()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private wakeWhenReadyLocked(I)V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method public OnDoubleClickHome()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardViewMediator$23;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$23;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public addStateMonitorCallback(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/KeyguardViewMediatorHelper;->addStateMonitorCallback(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mInputRestricted:Z

    invoke-virtual {v0, v1}, Lcom/android/keyguard/KeyguardViewMediatorHelper;->updateInputRestrictedLocked(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    invoke-virtual {v0, v1}, Lcom/android/keyguard/KeyguardViewMediatorHelper;->updateShowingLocked(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimLocked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/KeyguardViewMediatorHelper;->updateSimStateLocked(Z)V

    return-void
.end method

.method public disableInfo(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDisableInfo:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDisableInfo:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->adjustStatusBarLocked()V

    :cond_0
    return-void
.end method

.method public disableNavigation(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDisableNaviation:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDisableNaviation:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->adjustStatusBarLocked()V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->disableFullScreenGesture(Z)V

    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardViewMediator$18;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$18;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "KeyguardDismissLoggerCallback"

    const-string/jumbo v1, "onDismissError"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public dispatch(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public doKeyguardTimeout()V
    .locals 3

    const/16 v2, 0xd

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public doKeyguardTimeout(Landroid/os/Bundle;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->doKeyguardTimeout()V

    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x2

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "miui_keyguard"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v6

    invoke-static {v4, v5, v7, v6}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    if-ne v4, v7, :cond_2

    const/4 v1, 0x1

    :goto_0
    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    invoke-virtual {v4}, Lcom/android/keyguard/FingerprintHelper;->getFingerprintIds()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    xor-int/lit8 v2, v4, 0x1

    :goto_1
    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isKeyguardFingerprintDiabled(Landroid/content/Context;)Z

    move-result v0

    const-string/jumbo v4, "  mSystemReady: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSystemReady:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  isKeyguardPasswordSecured: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isKeyguardPasswordSecured()Z

    move-result v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  fingerprintEnableForKeyguard: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mExternallyEnabled: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExternallyEnabled:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mNeedToReshowWhenReenabled: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mShowing: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mInputRestricted: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mInputRestricted:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mHidden: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mDelayedShowingSequence: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDelayedShowingSequence:I

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v4, "  fingerprintExisted: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  diableFingerprintForKeyguard: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mFingerprintHardwareAvailable: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerprintHardwareAvailable(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mWaitingUntilKeyguardVisible: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mCharging: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mCharging:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mLevel: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLevel:I

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v4, "  mIsSmartCoverClosed: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverClosed:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mScreenOn: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOn:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  mShowNavigationBar: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowNavigationBar:Z

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  useid: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v4, "  isDefaultLockScreenTheme: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isDefaultLockScreenTheme()Z

    move-result v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v4, "  sFingerprintRunningState: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    sget v4, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sFingerprintRunningState:I

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    if-eqz v4, :cond_0

    const-string/jumbo v4, "  isDeviceProvisioned: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDeviceProvisioned()Z

    move-result v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    :cond_0
    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    if-eqz v4, :cond_1

    const-string/jumbo v4, "  isLockScreenDisabled: "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v4}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isLockScreenDisabled()Z

    move-result v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public isDismissable()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isFaceUnlockInited()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    invoke-virtual {v0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->hasInit()Z

    move-result v0

    return v0
.end method

.method public isHiddenByThirdApps(Landroid/content/Context;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUtils;->isTopActivitySystemApp(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInputRestricted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isInputRestrictedByDeviceProvisioned(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method isLockedOrMissing()Z
    .locals 3

    const/4 v1, 0x0

    const-string/jumbo v2, "keyguard.no_require_sim"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    xor-int/lit8 v0, v2, 0x1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimLocked()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimMissing()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public isScreenOn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOn:Z

    return v0
.end method

.method public isSecure()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewProperties:Lcom/android/keyguard/MiuiKeyguardViewProperties;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardViewProperties;->isSecure()Z

    move-result v0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    return v0
.end method

.method public isShowingAndNotHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHidden:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keyguardDone(Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->keyguardDone(ZZ)V

    return-void
.end method

.method public keyguardDone(ZZ)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->WAITING_FOR_INPUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-virtual {v4, v5}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setFingerprintIdentificationState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)V

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardDone:Z

    invoke-static {}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->setUserAuthenticatedSinceBoot()V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShouldListenForFingerprint:Z

    const v4, 0x11170

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(II)I

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    if-eqz p2, :cond_2

    :goto_0
    iput v2, v1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->clearFailedAttempts()V

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    :try_start_1
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;

    invoke-interface {v2, p1}, Lcom/android/internal/policy/IKeyguardExitCallback;->onKeyguardExitResult(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    const/4 v2, 0x0

    :try_start_2
    iput-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExternallyEnabled:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateInputRestrictedLocked()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string/jumbo v2, "KeyguardViewMediator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Failed to call onKeyguardExitResult("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public keyguardDoneDrawing()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public launchCamera()V
    .locals 0

    return-void
.end method

.method public onBootCompleted()V
    .locals 0

    return-void
.end method

.method public onDreamingStarted()V
    .locals 0

    return-void
.end method

.method public onDreamingStopped()V
    .locals 0

    return-void
.end method

.method public onScreenTurnedOff(I)V
    .locals 3

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    sput-boolean v1, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showTryAgainMessageInDefault:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOn:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWakeupByPickUp:Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardViewManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFingerprintIdentify()V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    :try_start_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/android/internal/policy/IKeyguardExitCallback;->onKeyguardExitResult(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExternallyEnabled:Z

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->hideLocked()V

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForO;->doKeyguardForChildProfilesLocked(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->notifyScreenOffLocked(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string/jumbo v1, "KeyguardViewMediator"

    const-string/jumbo v2, "Failed to call onKeyguardExitResult(false)"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_2
    :try_start_4
    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->resetStateLocked()V

    goto :goto_1

    :cond_3
    const/4 v1, 0x4

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_4

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSuppressNextLockSound:Z

    :cond_4
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->doKeyguardLocked()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public onScreenTurnedOn(Ljava/lang/Object;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOn:Z

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDelayedShowingSequence:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mDelayedShowingSequence:I

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->notifyScreenOnLocked(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    return-void
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMediatorHelper:Lcom/android/keyguard/KeyguardViewMediatorHelper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimLocked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/KeyguardViewMediatorHelper;->updateSimStateLocked(Z)V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-getcom-android-internal-telephony-IccCardConstants$StateSwitchesValues()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDeviceProvisioned()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->doKeyguardLocked()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_1
    monitor-exit p0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->resetStateLocked()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    monitor-enter p0

    :try_start_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "trigger_restart_min_framework"

    const-string/jumbo v1, "vold.decrypt"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->doKeyguardLocked()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_3
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->stopFaceUnlock()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->resetStateLocked()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :pswitch_2
    monitor-enter p0

    :try_start_4
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->doKeyguardLocked()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_5
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->resetStateLocked()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_1

    :pswitch_3
    monitor-enter p0

    :try_start_6
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->resetStateLocked()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onSystemReady()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSystemReady:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFingerprintStateCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerFingerprintStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->doKeyguardLocked()Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->sendReEntryFingerprintNotificationIfNeed()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onWakeKeyWhenKeyguardShowingTq(IZ)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isWakeKeyWhenKeyguardShowing(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->wakeWhenReadyLocked(I)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onWakeMotionWhenKeyguardShowingTq()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->wakeWhenReadyLocked(I)V

    const/4 v0, 0x1

    return v0
.end method

.method protected playSounds(Z)V
    .locals 8

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string/jumbo v0, "sys.in_shutdown_progress"

    invoke-static {v0, v5}, Lmiui/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_0

    const-string/jumbo v0, "KeyguardViewMediator"

    const-string/jumbo v2, "this device is being shut down, do not play sound"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    sget-boolean v0, Lcom/android/keyguard/AwesomeLockScreen;->sSuppressNextLockSound:Z

    if-eqz v0, :cond_1

    sput-boolean v5, Lcom/android/keyguard/AwesomeLockScreen;->sSuppressNextLockSound:Z

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSuppressNextLockSound:Z

    :cond_1
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSuppressNextLockSound:Z

    if-eqz v0, :cond_2

    iput-boolean v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSuppressNextLockSound:Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v0, "lockscreen_sounds_enabled"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v2

    invoke-static {v7, v0, v4, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_7

    if-eqz p1, :cond_3

    iget v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSoundId:I

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSoundStreamId:I

    invoke-virtual {v0, v2}, Landroid/media/SoundPool;->stop(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_4

    return-void

    :cond_3
    iget v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUnlockSoundId:I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->getMasterStreamType(Landroid/media/AudioManager;)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMasterStreamType:I

    :cond_5
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mMasterStreamType:I

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v0

    if-eqz v0, :cond_6

    return-void

    :cond_6
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSoundVolume:F

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSoundVolume:F

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLockSoundStreamId:I

    :cond_7
    return-void
.end method

.method public pokeWakelock()V
    .locals 1

    const/16 v0, 0x2710

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->pokeWakelock(I)V

    return-void
.end method

.method public pokeWakelock(I)V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/os/PowerManager;->userActivity(JZ)V

    return-void
.end method

.method public refreshSmartCover()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverLatticeMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverFullMode:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverClosed:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isInCallOrClockShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showSmartCover(Z)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverClosed:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isInCallOrClockShowing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showSmartCover(Z)V

    goto :goto_0
.end method

.method public setCurrentUser(I)V
    .locals 4

    const/16 v3, 0xf

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public setHidden(Z)V
    .locals 5

    const/16 v4, 0xc

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v4, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public setKeyguardEnabled(Z)V
    .locals 6

    monitor-enter p0

    :try_start_0
    const-string/jumbo v2, "KeyguardViewMediator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setKeyguardEnabled("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExternallyEnabled:Z

    if-nez p1, :cond_2

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShowing:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateInputRestrictedLocked()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->hideLocked()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    if-eqz p1, :cond_1

    :try_start_2
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->updateInputRestrictedLocked()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_3

    :try_start_3
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/android/internal/policy/IKeyguardExitCallback;->onKeyguardExitResult(Z)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    const/4 v2, 0x0

    :try_start_4
    iput-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->resetStateLocked()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :catch_0
    move-exception v0

    :try_start_5
    const-string/jumbo v2, "KeyguardViewMediator"

    const-string/jumbo v3, "Failed to call onKeyguardExitResult(false)"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->showLocked()V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x7d0

    const/16 v3, 0xa

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_2
    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_1

    :try_start_6
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->wait()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v1

    :try_start_7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2
.end method

.method public setKeyguardScreenCallback(Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    return-void
.end method

.method public shouldListenForFaceUnlock()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHasFaceUnlockData:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockApplyLock:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportFaceUnlock(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isOwnerUser()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldListenForFingerprint()Z
    .locals 12

    const/4 v11, 0x2

    const/4 v7, 0x0

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardDone:Z

    if-eqz v8, :cond_0

    const-string/jumbo v8, "miui_keyguard_fingerprint"

    const-string/jumbo v9, "should not listen for finger print when keyguard done"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v7

    :cond_0
    iget-boolean v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShouldListenForFingerprint:Z

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v8}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPasswordLockout()Z

    move-result v8

    xor-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_1

    const/4 v7, 0x1

    return v7

    :cond_1
    iget-object v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "miui_keyguard"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v10

    invoke-static {v8, v9, v11, v10}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v8

    if-ne v8, v11, :cond_6

    const/4 v1, 0x1

    :goto_0
    iget-object v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    invoke-virtual {v8}, Lcom/android/keyguard/FingerprintHelper;->getFingerprintIds()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v8

    xor-int/lit8 v2, v8, 0x1

    :goto_1
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isKeyguardPasswordSecured()Z

    move-result v5

    iget-object v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isKeyguardFingerprintDiabled(Landroid/content/Context;)Z

    move-result v0

    iget-object v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v8}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPasswordLockout()Z

    move-result v6

    iget-object v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerprintHardwareAvailable(Landroid/content/Context;)Z

    move-result v3

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSystemReady:Z

    if-eqz v8, :cond_3

    xor-int/lit8 v8, v5, 0x1

    if-nez v8, :cond_3

    xor-int/lit8 v8, v1, 0x1

    if-nez v8, :cond_3

    if-nez v2, :cond_2

    if-nez v3, :cond_3

    :cond_2
    if-nez v0, :cond_3

    if-eqz v6, :cond_4

    :cond_3
    const-string/jumbo v8, "miui_keyguard_fingerprint"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "mSystemReady="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSystemReady:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";isKeyguardPasswordSecured="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";fingerprintEnableForKeyguard="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";fingerprintExisted="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";diableFingerprintForKeyguard="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ";isPasswordLockout="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-boolean v8, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSystemReady:Z

    if-eqz v8, :cond_5

    if-eqz v3, :cond_5

    if-eqz v2, :cond_5

    if-eqz v5, :cond_5

    if-eqz v1, :cond_5

    xor-int/lit8 v8, v0, 0x1

    if-eqz v8, :cond_5

    xor-int/lit8 v7, v6, 0x1

    :cond_5
    iput-boolean v7, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShouldListenForFingerprint:Z

    iget-boolean v7, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mShouldListenForFingerprint:Z

    return v7

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public showAssistant()V
    .locals 0

    return-void
.end method

.method public showSmartCover(Z)V
    .locals 7

    const/4 v1, -0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mIsSmartCoverFullMode:Z

    if-eqz v2, :cond_1

    const v2, 0x7f030001

    :goto_0
    invoke-static {v3, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/smartcover/SmartCoverView;

    iput-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    const/16 v3, 0x1300

    invoke-virtual {v2, v3}, Lcom/android/keyguard/smartcover/SmartCoverView;->setSystemUiVisibility(I)V

    :cond_0
    if-eqz p1, :cond_2

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d9

    const v4, 0x5030500

    const/4 v5, 0x1

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const-string/jumbo v1, "smart_cover"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    invoke-interface {v6, v1, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mCharging:Z

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFull:Z

    iget v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mLevel:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/keyguard/smartcover/SmartCoverView;->onBatteryInfoRefresh(ZZI)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOffRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/smartcover/SmartCoverView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOffRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Lcom/android/keyguard/smartcover/SmartCoverView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_1
    return-void

    :cond_1
    const v2, 0x7f03001f

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mScreenOffRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/smartcover/SmartCoverView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    invoke-interface {v6, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mSmartCoverView:Lcom/android/keyguard/smartcover/SmartCoverView;

    goto :goto_1
.end method

.method public startFaceUnlock()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isFaceUnlockInited()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mKeyguardViewManager:Lcom/android/keyguard/MiuiKeyguardViewManager;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewManager;->startFaceUnlock()V

    :cond_0
    return-void
.end method

.method public startFingerprintIdentify()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardViewMediator$21;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$21;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public stopFingerprintIdentification()V
    .locals 2

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "stop fingerprint identify in stopFingerprintIdentification"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->stopFingerprintIdentify()V

    return-void
.end method

.method public stopFingerprintIdentify()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardViewMediator$20;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator$20;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public stopFingerprintIdentifyImmediately()V
    .locals 3

    :try_start_0
    const-string/jumbo v1, "0"

    invoke-direct {p0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setLockScreenPropertiesForA7(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->stopFingerprintIdentify()V

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setFingerprintRunningState(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "keyguard_fingerprint"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected switchUser(I)V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, -0x2710

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "second_user_id"

    invoke-static {v3, v4, v5, v6}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    const/4 v2, 0x0

    if-eq v1, v5, :cond_0

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Landroid/security/FingerprintIdUtils;->getUserFingerprintIds(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_0
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    if-eq v3, v2, :cond_1

    invoke-static {v2}, Lmiui/securityspace/SecuritySpaceEcryptManager;->spaceSwitchUser(I)Z

    :cond_1
    return-void
.end method

.method public verifyUnlock(Lcom/android/internal/policy/IKeyguardExitCallback;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDeviceProvisioned()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :try_start_1
    invoke-interface {p1, v1}, Lcom/android/internal/policy/IKeyguardExitCallback;->onKeyguardExitResult(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string/jumbo v1, "KeyguardViewMediator"

    const-string/jumbo v2, "Failed to call onKeyguardExitResult(false)"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    :try_start_3
    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExternallyEnabled:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "KeyguardViewMediator"

    const-string/jumbo v2, "verifyUnlock called when not externally disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v1, 0x0

    :try_start_4
    invoke-interface {p1, v1}, Lcom/android/internal/policy/IKeyguardExitCallback;->onKeyguardExitResult(Z)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_5
    const-string/jumbo v1, "KeyguardViewMediator"

    const-string/jumbo v2, "Failed to call onKeyguardExitResult(false)"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :try_start_6
    invoke-interface {p1, v1}, Lcom/android/internal/policy/IKeyguardExitCallback;->onKeyguardExitResult(Z)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_7
    const-string/jumbo v1, "KeyguardViewMediator"

    const-string/jumbo v2, "Failed to call onKeyguardExitResult(false)"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mExitSecureCallback:Lcom/android/internal/policy/IKeyguardExitCallback;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->verifyUnlockLocked()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0
.end method
