.class public Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;
.super Ljava/lang/Object;
.source "MiuiSimPUKUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiSimPUKUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "StateMachine"
.end annotation


# instance fields
.field final CONFIRM_PIN:I

.field final DONE:I

.field final ENTER_PIN:I

.field final ENTER_PUK:I

.field protected state:I

.field final synthetic this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;


# direct methods
.method protected constructor <init>(Lcom/android/keyguard/MiuiSimPUKUnlockScreen;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->ENTER_PUK:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->ENTER_PIN:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->CONFIRM_PIN:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->DONE:I

    iput v1, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    return-void
.end method


# virtual methods
.method public next()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->checkPuk()Z

    move-result v2

    if-eqz v2, :cond_2

    iput v3, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    const v1, 0x7f0b0013

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    iget-object v2, v2, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    iget-object v2, v2, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setHint(I)V

    :cond_1
    if-eqz v0, :cond_7

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    invoke-virtual {v2, v0}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->displayErrorMsg(I)V

    :goto_1
    return-void

    :cond_2
    const v0, 0x7f0b003d

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->checkPin()Z

    move-result v2

    if-eqz v2, :cond_4

    iput v4, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    const v1, 0x7f0b002e

    goto :goto_0

    :cond_4
    const v0, 0x7f0b003c

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->confirmPin()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x3

    iput v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->-wrap0(Lcom/android/keyguard/MiuiSimPUKUnlockScreen;)V

    goto :goto_0

    :cond_6
    iput v3, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    const v0, 0x7f0b002f

    const v1, 0x7f0b0013

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    iget-object v2, v2, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mErrorMsg:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method reset()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    const-string/jumbo v1, ""

    iput-object v1, v0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mPinText:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    const-string/jumbo v1, ""

    iput-object v1, v0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mPukText:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->state:I

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen$StateMachine;->this$0:Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method
