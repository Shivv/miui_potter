.class Lcom/android/keyguard/MiuiKeyguardViewMediator$14;
.super Landroid/content/BroadcastReceiver;
.source "MiuiKeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v5, "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string/jumbo v5, "seq"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get3(Lcom/android/keyguard/MiuiKeyguardViewMediator;)I

    move-result v5

    if-ne v5, v3, :cond_0

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    const/4 v7, 0x1

    invoke-static {v5, v7}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set13(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap0(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit v6

    :cond_1
    :goto_1
    return-void

    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    :cond_2
    const-string/jumbo v5, "android.intent.action.PHONE_STATE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    const-string/jumbo v6, "state"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set9(Lcom/android/keyguard/MiuiKeyguardViewMediator;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    monitor-enter v6

    :try_start_1
    sget-object v5, Lmiui/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v7}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get16(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get5(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap0(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v5

    monitor-exit v6

    throw v5

    :cond_3
    const-string/jumbo v5, "xiaomi.intent.action.SHOW_SECURE_KEYGUARD"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get18(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v1

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get7(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/os/Handler;

    move-result-object v5

    new-instance v6, Lcom/android/keyguard/MiuiKeyguardViewMediator$14$1;

    invoke-direct {v6, p0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator$14$1;-><init>(Lcom/android/keyguard/MiuiKeyguardViewMediator$14;Z)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_4
    const-string/jumbo v5, "com.miui.lockscreen.navigation_bar_visibility"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    const-string/jumbo v6, "is_show"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static {v5, v6}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set12(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap5(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v5, "face_unlock_release"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardUtils;->isSupportFaceUnlock(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v5, v5, Lcom/android/keyguard/MiuiKeyguardViewMediator;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    invoke-virtual {v5}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->release()V

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v5, "lockscreen.action.LOCKSCREEN_MAGAZINE_ENTRANCE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string/jumbo v5, "com.mfashiongallery.emag"

    invoke-virtual {p2}, Landroid/content/Intent;->getSender()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    const-string/jumbo v5, "com.mfashiongallery.emag"

    invoke-virtual {p2}, Landroid/content/Intent;->getSender()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_7
    const-string/jumbo v5, "lockscreen.extra.IS_SHOW"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "show_lockscreen_magazine_entrance"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v7

    invoke-static {v5, v6, v4, v7}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v5, "miui.intent.action.MIUI_REGION_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070002

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get8(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v5

    if-eqz v5, :cond_9

    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v5, :cond_9

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "face_unlock_has_feature"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v7

    invoke-static {v5, v6, v8, v7}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->deleteFeature()V

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->release()V

    :cond_9
    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v5, :cond_1

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$14;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap22(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    goto/16 :goto_1
.end method
