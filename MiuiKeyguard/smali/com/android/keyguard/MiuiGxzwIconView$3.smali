.class Lcom/android/keyguard/MiuiGxzwIconView$3;
.super Landroid/hardware/TriggerEventListener;
.source "MiuiGxzwIconView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiGxzwIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiGxzwIconView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiGxzwIconView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiGxzwIconView$3;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-direct {p0}, Landroid/hardware/TriggerEventListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTrigger(Landroid/hardware/TriggerEvent;)V
    .locals 3

    const/16 v2, 0x3e9

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "Stationary: onTrigger"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Landroid/hardware/TriggerEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwIconView;->-get0()I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "onTrigger: TYPE_STATIONARY_DETECT"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$3;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->-get1(Lcom/android/keyguard/MiuiGxzwIconView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$3;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->-get1(Lcom/android/keyguard/MiuiGxzwIconView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
