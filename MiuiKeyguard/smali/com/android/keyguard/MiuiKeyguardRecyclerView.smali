.class public Lcom/android/keyguard/MiuiKeyguardRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source "MiuiKeyguardRecyclerView.java"

# interfaces
.implements Lcom/android/keyguard/SwipeHelper$Callback;


# instance fields
.field private mRemovedItemViewPosition:I

.field private mSwipeHelper:Lcom/android/keyguard/SwipeHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    new-instance v0, Lcom/android/keyguard/SwipeHelper;

    const/4 v1, 0x0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/keyguard/SwipeHelper;-><init>(ILcom/android/keyguard/SwipeHelper$Callback;FFI)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->mSwipeHelper:Lcom/android/keyguard/SwipeHelper;

    return-void
.end method


# virtual methods
.method public canChildBeBlock(Landroid/view/View;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public canChildBeLeftDismissed(Landroid/view/View;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getChildAtPosition(FF)Landroid/view/View;
    .locals 9

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v7

    int-to-float v6, v7

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v7

    int-to-float v7, v7

    add-float v0, v6, v7

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int v4, v3, v7

    cmpl-float v7, p2, v6

    if-ltz v7, :cond_0

    cmpg-float v7, p2, v0

    if-gtz v7, :cond_0

    int-to-float v7, v3

    cmpl-float v7, p1, v7

    if-ltz v7, :cond_0

    int-to-float v7, v4

    cmpg-float v7, p1, v7

    if-gtz v7, :cond_0

    return-object v5

    :cond_2
    const/4 v7, 0x0

    return-object v7
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getChildAtPosition(FF)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getChildContentView(Landroid/view/View;)Landroid/view/View;
    .locals 0

    return-object p1
.end method

.method public getRemovedItemViewPosition()I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->mRemovedItemViewPosition:I

    return v0
.end method

.method public isBlockShow(Landroid/view/View;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 4

    const v2, 0x7f0d0001

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->mRemovedItemViewPosition:I

    const/high16 v2, 0x7f0d0000

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.action_remove_keyguard_notification"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.extra_notification_key"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.extra_notification_click"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method

.method public onDragCancelled(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->mSwipeHelper:Lcom/android/keyguard/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->mSwipeHelper:Lcom/android/keyguard/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/SwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onWindowFocusChanged(Z)V

    return-void
.end method
