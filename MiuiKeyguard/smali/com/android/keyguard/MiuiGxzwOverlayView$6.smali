.class Lcom/android/keyguard/MiuiGxzwOverlayView$6;
.super Landroid/os/AsyncTask;
.source "MiuiGxzwOverlayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiGxzwOverlayView;->openHBMAndUpdateAlpha()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiGxzwOverlayView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiGxzwOverlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$6;->this$0:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$6;->this$0:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiGxzwOverlayView;->-wrap0(Lcom/android/keyguard/MiuiGxzwOverlayView;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sget-object v1, Lcom/android/keyguard/MiuiGxzwOverlayView;->BRIGHTNESS_TO_ALPHA:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_1

    sget-object v1, Lcom/android/keyguard/MiuiGxzwOverlayView;->BRIGHTNESS_TO_ALPHA:[F

    array-length v0, v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiGxzwOverlayView$6;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$6;->this$0:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiGxzwOverlayView;->-set0(Lcom/android/keyguard/MiuiGxzwOverlayView;I)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$6;->this$0:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->-get0(Lcom/android/keyguard/MiuiGxzwOverlayView;)Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;->scheduleVsync()V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiGxzwOverlayView$6;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
