.class Lcom/android/keyguard/MiuiDefaultLockScreen$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiDefaultLockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    const/4 v7, 0x0

    const-string/jumbo v5, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set15(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    const-string/jumbo v5, "status"

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v5, "plugged"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x5

    if-eq v2, v5, :cond_0

    const/4 v5, 0x2

    if-ne v2, v5, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const-string/jumbo v5, "level"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v5, "voltage"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string/jumbo v5, "temperature"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    new-instance v5, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;

    invoke-direct {v5, p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$1;I)V

    sget-object v6, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v7, v7, [Ljava/lang/Void;

    invoke-virtual {v5, v6, v7}, Lcom/android/keyguard/MiuiDefaultLockScreen$1$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get42(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v5

    if-eq v3, v5, :cond_1

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v5

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v5

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get3(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setChargingInfo(Ljava/lang/String;II)V

    :cond_1
    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get28(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v5

    if-ne v1, v5, :cond_2

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get0(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v5

    if-eq v0, v5, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$1;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5, v1, v0, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap7(Lcom/android/keyguard/MiuiDefaultLockScreen;ZII)V

    :cond_3
    return-void

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method
