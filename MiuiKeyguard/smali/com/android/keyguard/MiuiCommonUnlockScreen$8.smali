.class Lcom/android/keyguard/MiuiCommonUnlockScreen$8;
.super Ljava/lang/Object;
.source "MiuiCommonUnlockScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x4

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get3(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v3, v4}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-wrap4(Lcom/android/keyguard/MiuiCommonUnlockScreen;I)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get4(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0020

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$8$1;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen$8;)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get4(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0d0099

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-static {v1, v0, v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$8;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get4(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0d009a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
