.class Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;
.super Landroid/database/ContentObserver;
.source "MiuiKeyguardNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardNotificationManager;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardNotificationManager;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;->this$0:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 13

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;->this$0:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->-set1(Lcom/android/keyguard/MiuiKeyguardNotificationManager;J)J

    const/4 v7, 0x0

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;->this$0:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->-get0(Lcom/android/keyguard/MiuiKeyguardNotificationManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lmiui/provider/KeyguardNotification;->URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    :goto_0
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    invoke-direct {v9}, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;-><init>()V

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    array-length v0, v6

    const/4 v1, 0x0

    invoke-static {v6, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->icon:Landroid/graphics/Bitmap;

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->title:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->content:Ljava/lang/String;

    iget-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->content:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->content:Ljava/lang/String;

    :cond_0
    const/4 v0, 0x7

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->key:I

    const/4 v0, 0x4

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->time:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->pkgName:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;->this$0:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->-get0(Lcom/android/keyguard/MiuiKeyguardNotificationManager;)Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelper;->getProfileDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->profile:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x5

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->info:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {v10, v0, v9}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v8

    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;->this$0:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    invoke-static {v0, v10}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->-set0(Lcom/android/keyguard/MiuiKeyguardNotificationManager;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;->this$0:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->-get2(Lcom/android/keyguard/MiuiKeyguardNotificationManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$1;->this$0:Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->-get1(Lcom/android/keyguard/MiuiKeyguardNotificationManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v11, v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;->onChange(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method
