.class Lcom/android/keyguard/AwesomeLockScreen;
.super Landroid/widget/FrameLayout;
.source "AwesomeLockScreen.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardScreen;
.implements Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;
.implements Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot$LockscreenCallback;


# static fields
.field private static final COMMAND_PAUSE:Ljava/lang/String; = "pause"

.field private static final COMMAND_RESUME:Ljava/lang/String; = "resume"

.field private static final DBG:Z = true

.field private static final OWNER_INFO_VAR:Ljava/lang/String; = "owner_info"

.field private static final PASSWORD_MODE_LOCKOUT:I = -0x1

.field private static final PASSWORD_MODE_NONE:I = 0x0

.field private static final PASSWORD_MODE_NUMERIC:I = 0x1

.field private static final PASSWORD_MODE_SET:I = 0xa

.field private static final TAG:Ljava/lang/String; = "AwesomeLockScreen"

.field private static final VAR_NAME_PASSWORD_MODE:Ljava/lang/String; = "__password_mode"

.field private static mRootHolder:Lcom/android/keyguard/RootHolder;

.field private static mThemeChanged:I

.field private static sStartTime:J

.field static sSuppressNextLockSound:Z

.field private static sTotalWakenTime:J


# instance fields
.field private isPaused:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

.field private mInitSuccessful:Z

.field private mIsFocus:Z

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

.field private mLockscreenView:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

.field private mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

.field private mPasswordMode:I

.field private mPasswordVerify:Lcom/android/keyguard/PasswordVerifyHelper;

.field private mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field private mWakeStartTime:J


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/AwesomeLockScreen;)Lcom/android/keyguard/MiuiKeyguardScreenCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/keyguard/RootHolder;

    invoke-direct {v0}, Lcom/android/keyguard/RootHolder;-><init>()V

    sput-object v0, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V
    .locals 14

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->isPaused:Z

    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mIsFocus:Z

    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    new-instance v9, Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget-object v10, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v9, v10}, Lcom/android/keyguard/LockPatternUtilsWrapper;-><init>(Lcom/android/internal/widget/LockPatternUtils;)V

    iput-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/android/keyguard/AwesomeLockScreen;->setFocusable(Z)V

    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/android/keyguard/AwesomeLockScreen;->setFocusableInTouchMode(Z)V

    const-string/jumbo v9, "audio"

    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/media/AudioManager;

    iput-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v9, v2, Landroid/content/res/Configuration;->extraConfig:Landroid/content/res/MiuiConfiguration;

    iget v8, v9, Landroid/content/res/MiuiConfiguration;->themeChanged:I

    sget v9, Lcom/android/keyguard/AwesomeLockScreen;->mThemeChanged:I

    if-le v8, v9, :cond_0

    invoke-static {}, Lcom/android/keyguard/AwesomeLockScreen;->clearCache()V

    sput v8, Lcom/android/keyguard/AwesomeLockScreen;->mThemeChanged:I

    :cond_0
    sget-object v9, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    iget-object v10, p0, Lcom/android/keyguard/AwesomeLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v10, p0}, Lcom/android/keyguard/RootHolder;->init(Landroid/content/Context;Lcom/android/keyguard/AwesomeLockScreen;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string/jumbo v9, "AwesomeLockScreen"

    const-string/jumbo v10, "fail to init RootHolder"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    new-instance v7, Landroid/security/MiuiLockPatternUtils;

    invoke-direct {v7, p1}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    iget-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v9}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isOwnerInfoEnabled()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v7}, Landroid/security/MiuiLockPatternUtils;->getOwnerInfo()Ljava/lang/String;

    move-result-object v5

    :goto_0
    const-string/jumbo v9, "owner_info"

    sget-object v10, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v10}, Lcom/android/keyguard/RootHolder;->getContext()Lmiui/maml/ScreenContext;

    move-result-object v10

    iget-object v10, v10, Lmiui/maml/ScreenContext;->mVariables:Lmiui/maml/data/Variables;

    invoke-static {v9, v10, v5}, Lmiui/maml/util/Utils;->putVariableString(Ljava/lang/String;Lmiui/maml/data/Variables;Ljava/lang/String;)V

    new-instance v3, Lcom/android/keyguard/HeiHeiGestureView;

    iget-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mContext:Landroid/content/Context;

    invoke-direct {v3, v9}, Lcom/android/keyguard/HeiHeiGestureView;-><init>(Landroid/content/Context;)V

    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct {v9, v10, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v9}, Lcom/android/keyguard/HeiHeiGestureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v9, Lcom/android/keyguard/AwesomeLockScreen$1;

    invoke-direct {v9, p0}, Lcom/android/keyguard/AwesomeLockScreen$1;-><init>(Lcom/android/keyguard/AwesomeLockScreen;)V

    invoke-virtual {v3, v9}, Lcom/android/keyguard/HeiHeiGestureView;->setOnTriggerListener(Lcom/android/keyguard/HeiHeiGestureView$OnTriggerListener;)V

    invoke-virtual {p0, v3}, Lcom/android/keyguard/AwesomeLockScreen;->addView(Landroid/view/View;)V

    new-instance v9, Lcom/android/keyguard/PasswordVerifyHelper;

    iget-object v10, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v11, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v12, p0, Lcom/android/keyguard/AwesomeLockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-direct {v9, v10, v11, v12}, Lcom/android/keyguard/PasswordVerifyHelper;-><init>(Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V

    iput-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordVerify:Lcom/android/keyguard/PasswordVerifyHelper;

    invoke-virtual {p0}, Lcom/android/keyguard/AwesomeLockScreen;->getPasswordMode()I

    move-result v9

    iput v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    sget-object v9, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v9}, Lcom/android/keyguard/RootHolder;->getContext()Lmiui/maml/ScreenContext;

    move-result-object v9

    iget-object v9, v9, Lmiui/maml/ScreenContext;->mVariables:Lmiui/maml/data/Variables;

    const-string/jumbo v10, "__password_mode"

    iget v11, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    int-to-double v12, v11

    invoke-virtual {v9, v10, v12, v13}, Lmiui/maml/data/Variables;->put(Ljava/lang/String;D)V

    sget-object v9, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v9}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v9

    invoke-virtual {v9, p0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->setLockscreenCallback(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot$LockscreenCallback;)V

    sget-object v9, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    iget-object v10, p0, Lcom/android/keyguard/AwesomeLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v10}, Lcom/android/keyguard/RootHolder;->createView(Landroid/content/Context;)Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    move-result-object v9

    iput-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockscreenView:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    iget-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockscreenView:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    if-eqz v9, :cond_2

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v9, -0x1

    const/4 v10, -0x1

    invoke-direct {v6, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockscreenView:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-virtual {v3, v9, v6}, Lcom/android/keyguard/HeiHeiGestureView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    :cond_2
    iget-object v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v9, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerInfoCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;)V

    sget-wide v10, Lcom/android/keyguard/AwesomeLockScreen;->sStartTime:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-nez v9, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    sput-wide v10, Lcom/android/keyguard/AwesomeLockScreen;->sStartTime:J

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    iput-wide v10, p0, Lcom/android/keyguard/AwesomeLockScreen;->mWakeStartTime:J

    invoke-virtual {p0}, Lcom/android/keyguard/AwesomeLockScreen;->onPause()V

    sget-object v9, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v9}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v10

    iget v9, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    if-eqz v9, :cond_5

    const/high16 v9, -0x1000000

    :goto_1
    invoke-virtual {v10, v9}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->setBgColor(I)V

    return-void

    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public static clearCache()V
    .locals 1

    sget-object v0, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v0}, Lcom/android/keyguard/RootHolder;->clear()V

    return-void
.end method

.method private pause()V
    .locals 6

    iget-boolean v2, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockscreenView:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-virtual {v2}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->onPause()V

    sget-object v2, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v2}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v2

    const-string/jumbo v3, "pause"

    invoke-virtual {v2, v3}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->onCommand(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iget-wide v4, p0, Lcom/android/keyguard/AwesomeLockScreen;->mWakeStartTime:J

    sub-long v0, v2, v4

    sget-wide v2, Lcom/android/keyguard/AwesomeLockScreen;->sTotalWakenTime:J

    add-long/2addr v2, v0

    sput-wide v2, Lcom/android/keyguard/AwesomeLockScreen;->sTotalWakenTime:J

    return-void
.end method

.method private resume()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerInfoCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;)V

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockscreenView:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->onResume()V

    sget-object v0, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v0}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v0

    const-string/jumbo v1, "resume"

    invoke-virtual {v0, v1}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->onCommand(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mWakeStartTime:J

    return-void
.end method

.method private sendLockscreenIntentTypeAnalytics(Landroid/content/Intent;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "eventId"

    const-string/jumbo v2, "lockscreen_intent_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "eventObj"

    if-nez p1, :cond_0

    const-string/jumbo v1, ""

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private updateStatusBarColormode()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {p0}, Lcom/android/keyguard/AwesomeLockScreen;->isTopLightColorMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateStatusBarColorMode(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public allowScreenRotation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v0}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->allowScreenRotation()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cleanUp()V
    .locals 1

    sget-object v0, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v0, p0}, Lcom/android/keyguard/RootHolder;->cleanUp(Lcom/android/keyguard/AwesomeLockScreen;)V

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    return-void
.end method

.method public cleanUpView()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockscreenView:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->cleanUp(Z)V

    return-void
.end method

.method public getPasswordMode()I
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x1

    iget-object v3, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordVerify:Lcom/android/keyguard/PasswordVerifyHelper;

    invoke-virtual {v3}, Lcom/android/keyguard/PasswordVerifyHelper;->isLockout()Z

    move-result v3

    if-eqz v3, :cond_0

    return v4

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v3, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getSimState(I)Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v2, v3, :cond_2

    :cond_1
    return v4

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v3}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getKeyguardStoredPasswordQuality()I

    move-result v1

    if-nez v1, :cond_4

    return v5

    :cond_4
    const/high16 v3, 0x20000

    if-eq v1, v3, :cond_5

    const/high16 v3, 0x30000

    if-ne v1, v3, :cond_6

    :cond_5
    const/4 v3, 0x1

    return v3

    :cond_6
    const/16 v3, 0xa

    return v3
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v0}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->getRawAttr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public haptic(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/AwesomeLockScreen;->performHapticFeedback(I)Z

    return-void
.end method

.method public isSecure()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v0}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isSecure()Z

    move-result v0

    return v0
.end method

.method public isSoundEnable()Z
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/keyguard/AwesomeLockScreen;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "lockscreen_sounds_enabled"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    const/4 v5, 0x1

    invoke-static {v2, v3, v5, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/keyguard/AwesomeLockScreen;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public isTopLightColorMode()Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/keyguard/AwesomeLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needsInput()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/android/keyguard/AwesomeLockScreen;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iput-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mParentView:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-direct {p0}, Lcom/android/keyguard/AwesomeLockScreen;->updateStatusBarColormode()V

    return-void
.end method

.method public onClockVisibilityChanged()V
    .locals 0

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onDevicePolicyManagerStateChanged()V
    .locals 0

    return-void
.end method

.method public onDeviceProvisioned()V
    .locals 0

    return-void
.end method

.method public onFaceUnlockFailed()V
    .locals 0

    return-void
.end method

.method public onFaceUnlockHelp(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onFaceUnlockStart()V
    .locals 0

    return-void
.end method

.method public onFaceUnlockSuccess()V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 2

    const-string/jumbo v0, "AwesomeLockScreen"

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->isPaused:Z

    invoke-direct {p0}, Lcom/android/keyguard/AwesomeLockScreen;->pause()V

    return-void
.end method

.method public onPhoneStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onPhoneStateChanged(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onRefreshBatteryInfo(ZZI)V
    .locals 3

    const-string/jumbo v0, "AwesomeLockScreen"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onRefreshBatteryInfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v0}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->onRefreshBatteryInfo(ZZI)V

    return-void
.end method

.method public onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 3

    const-string/jumbo v0, "AwesomeLockScreen"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onRefreshCarrierInfo("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 0

    return-void
.end method

.method public onResume(Z)V
    .locals 2

    const-string/jumbo v0, "AwesomeLockScreen"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->isPaused:Z

    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mIsFocus:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/AwesomeLockScreen;->resume()V

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/AwesomeLockScreen;->updateStatusBarColormode()V

    return-void
.end method

.method public onRingerModeChanged(I)V
    .locals 0

    return-void
.end method

.method public onTimeChanged()V
    .locals 0

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onUserChanged(I)V
    .locals 0

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    iput-boolean p1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mIsFocus:Z

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/AwesomeLockScreen;->pause()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->isPaused:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/AwesomeLockScreen;->resume()V

    goto :goto_0
.end method

.method public pokeWakelock()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    return-void
.end method

.method public pokeWakelock(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock(I)V

    return-void
.end method

.method public rebindView()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mInitSuccessful:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->shouldShowBatteryInfo()Z

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDevicePluggedIn()Z

    move-result v1

    iget-object v2, p0, Lcom/android/keyguard/AwesomeLockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getBatteryLevel()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/keyguard/AwesomeLockScreen;->onRefreshBatteryInfo(ZZI)V

    sget-object v0, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v0}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->setLockscreenCallback(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot$LockscreenCallback;)V

    iget-object v0, p0, Lcom/android/keyguard/AwesomeLockScreen;->mLockscreenView:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->rebindRoot()V

    return-void
.end method

.method public setHidden(Z)V
    .locals 0

    return-void
.end method

.method public unlockVerify(Ljava/lang/String;I)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v6, -0x1

    invoke-virtual {p0}, Lcom/android/keyguard/AwesomeLockScreen;->getPasswordMode()I

    move-result v1

    iput v1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    sget-object v1, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v1}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->getVariables()Lmiui/maml/data/Variables;

    move-result-object v1

    const-string/jumbo v2, "__password_mode"

    iget v3, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    int-to-double v4, v3

    invoke-virtual {v1, v2, v4, v5}, Lmiui/maml/data/Variables;->put(Ljava/lang/String;D)V

    iget v1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    if-ne v1, v6, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    const/4 v1, 0x1

    return v1

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordVerify:Lcom/android/keyguard/PasswordVerifyHelper;

    invoke-virtual {v1, p1, p2}, Lcom/android/keyguard/PasswordVerifyHelper;->unlockVerify(Ljava/lang/String;I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordVerify:Lcom/android/keyguard/PasswordVerifyHelper;

    invoke-virtual {v1}, Lcom/android/keyguard/PasswordVerifyHelper;->isLockout()Z

    move-result v1

    if-eqz v1, :cond_2

    iput v6, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    sget-object v1, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v1}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->getVariables()Lmiui/maml/data/Variables;

    move-result-object v1

    const-string/jumbo v2, "__password_mode"

    iget v3, p0, Lcom/android/keyguard/AwesomeLockScreen;->mPasswordMode:I

    int-to-double v4, v3

    invoke-virtual {v1, v2, v4, v5}, Lmiui/maml/data/Variables;->put(Ljava/lang/String;D)V

    :cond_2
    if-eqz v0, :cond_3

    sget-object v1, Lcom/android/keyguard/AwesomeLockScreen;->mRootHolder:Lcom/android/keyguard/RootHolder;

    invoke-virtual {v1}, Lcom/android/keyguard/RootHolder;->getRoot()Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;->setBgColor(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/AwesomeLockScreen;->postInvalidate()V

    :cond_3
    return v0
.end method

.method public unlocked(Landroid/content/Intent;I)V
    .locals 8

    invoke-direct {p0, p1}, Lcom/android/keyguard/AwesomeLockScreen;->sendLockscreenIntentTypeAnalytics(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/android/keyguard/AwesomeLockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1, p1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->setPendingIntent(Landroid/content/Intent;)V

    new-instance v0, Lcom/android/keyguard/AwesomeLockScreen$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/AwesomeLockScreen$2;-><init>(Lcom/android/keyguard/AwesomeLockScreen;)V

    int-to-long v2, p2

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/keyguard/AwesomeLockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string/jumbo v1, "AwesomeLockScreen"

    const-string/jumbo v2, "lockscreen awake time: [%d sec] in time range: [%d sec]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    sget-wide v4, Lcom/android/keyguard/AwesomeLockScreen;->sTotalWakenTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    sget-wide v6, Lcom/android/keyguard/AwesomeLockScreen;->sStartTime:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
