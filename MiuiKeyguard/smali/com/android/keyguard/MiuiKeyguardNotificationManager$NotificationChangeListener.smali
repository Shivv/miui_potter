.class public interface abstract Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;
.super Ljava/lang/Object;
.source "MiuiKeyguardNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NotificationChangeListener"
.end annotation


# virtual methods
.method public abstract onChange(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onRegistered(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation
.end method
