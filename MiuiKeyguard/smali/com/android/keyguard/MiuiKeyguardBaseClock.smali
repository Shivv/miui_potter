.class public Lcom/android/keyguard/MiuiKeyguardBaseClock;
.super Landroid/widget/LinearLayout;
.source "MiuiKeyguardBaseClock.java"


# instance fields
.field public mContext:Landroid/content/Context;

.field public mDisplaySimCardInfo:Z

.field public mOwnerInfoString:Ljava/lang/String;

.field public mPhoneCount:I

.field public mRealCarrier:[Ljava/lang/String;

.field public mRealCarrierObserver:[Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mOwnerInfoString:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mDisplaySimCardInfo:Z

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public displaySimCardInfo(Landroid/content/Context;I)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "status_bar_show_custom_carrier"

    invoke-static {}, Landroid/provider/MiuiSettings$System;->getShowCustomCarrierDefault()I

    move-result v3

    invoke-static {v1, v2, v3, p2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public hasOwnerInfo()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mOwnerInfoString:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public initCarrier(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardBaseClock$1;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2, p1}, Lcom/android/keyguard/MiuiKeyguardBaseClock$1;-><init>(Lcom/android/keyguard/MiuiKeyguardBaseClock;Landroid/os/Handler;I)V

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/ContentObserver;->onChange(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mDisplaySimCardInfo:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    array-length v2, v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mPhoneCount:I

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_bar_real_carrier"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    aget-object v3, v3, v0

    invoke-virtual {v2, v1, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mPhoneCount:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    const/4 v3, 0x0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    new-instance v2, Landroid/security/MiuiLockPatternUtils;

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    invoke-static {v2, v4}, Lcom/android/keyguard/KeyguardCompatibilityHelperForN;->getOwnerInfo(Landroid/security/MiuiLockPatternUtils;I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mOwnerInfoString:Ljava/lang/String;

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v4

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mPhoneCount:I

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->displaySimCardInfo(Landroid/content/Context;I)Z

    move-result v3

    :cond_0
    iput-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mDisplaySimCardInfo:Z

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mDisplaySimCardInfo:Z

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mPhoneCount:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrier:[Ljava/lang/String;

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mPhoneCount:I

    new-array v3, v3, [Landroid/database/ContentObserver;

    iput-object v3, p0, Lcom/android/keyguard/MiuiKeyguardBaseClock;->mRealCarrierObserver:[Landroid/database/ContentObserver;

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    return-void
.end method

.method public updateClockView()V
    .locals 0

    return-void
.end method

.method public updateColorByWallpaper(Z)V
    .locals 0

    return-void
.end method

.method public updateOwnerInfo()V
    .locals 0

    return-void
.end method

.method public updateSimCardInfo()V
    .locals 0

    return-void
.end method
