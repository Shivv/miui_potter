.class final enum Lcom/android/keyguard/MiuiGxzwAnimView$TextState;
.super Ljava/lang/Enum;
.source "MiuiGxzwAnimView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiGxzwAnimView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TextState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/keyguard/MiuiGxzwAnimView$TextState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

.field public static final enum MORE_PRESSURE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

.field public static final enum NONE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

.field public static final enum TRY_AGAIN:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

.field public static final enum USE_PASSWORD:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->NONE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    const-string/jumbo v1, "TRY_AGAIN"

    invoke-direct {v0, v1, v3}, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->TRY_AGAIN:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    const-string/jumbo v1, "MORE_PRESSURE"

    invoke-direct {v0, v1, v4}, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->MORE_PRESSURE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    const-string/jumbo v1, "USE_PASSWORD"

    invoke-direct {v0, v1, v5}, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->USE_PASSWORD:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    sget-object v1, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->NONE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->TRY_AGAIN:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->MORE_PRESSURE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->USE_PASSWORD:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->$VALUES:[Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/keyguard/MiuiGxzwAnimView$TextState;
    .locals 1

    const-class v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    return-object v0
.end method

.method public static values()[Lcom/android/keyguard/MiuiGxzwAnimView$TextState;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->$VALUES:[Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    return-object v0
.end method
