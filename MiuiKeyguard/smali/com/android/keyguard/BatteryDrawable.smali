.class public Lcom/android/keyguard/BatteryDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "BatteryDrawable.java"


# instance fields
.field private mBattery:Landroid/graphics/drawable/Drawable;

.field private mBatteryFg:Landroid/graphics/drawable/Drawable;

.field private mBatteryIng:Landroid/graphics/drawable/Drawable;

.field private mBatteryLevel:I

.field private mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

.field private mPluggedIn:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    const v2, 0x7f020003

    invoke-virtual {p1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryIng:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f020001

    invoke-virtual {p1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBattery:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f020002

    invoke-virtual {p1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f02000e

    invoke-virtual {p1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/ClipDrawable;

    iput-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

    iget-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/BatteryDrawable;->mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/ClipDrawable;->getIntrinsicWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v0, v2, 0x2

    iget-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/BatteryDrawable;->mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/ClipDrawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v1, v2, 0x2

    iget-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

    iget-object v3, p0, Lcom/android/keyguard/BatteryDrawable;->mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/ClipDrawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/android/keyguard/BatteryDrawable;->mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/ClipDrawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/ClipDrawable;->setBounds(IIII)V

    iget-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {p0, v5, v5, v2, v3}, Lcom/android/keyguard/BatteryDrawable;->setBounds(IIII)V

    iget-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBattery:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/keyguard/BatteryDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/keyguard/BatteryDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryIng:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/keyguard/BatteryDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/BatteryDrawable;->mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

    iget v1, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryLevel:I

    mul-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    iget-object v0, p0, Lcom/android/keyguard/BatteryDrawable;->mClipDrawable:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ClipDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/keyguard/BatteryDrawable;->mPluggedIn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryIng:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryFg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setBatteryInfo(ZI)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/BatteryDrawable;->mPluggedIn:Z

    iput p2, p0, Lcom/android/keyguard/BatteryDrawable;->mBatteryLevel:I

    invoke-virtual {p0}, Lcom/android/keyguard/BatteryDrawable;->invalidateSelf()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method
