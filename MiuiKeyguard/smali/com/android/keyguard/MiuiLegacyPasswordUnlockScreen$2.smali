.class Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$2;
.super Ljava/lang/Object;
.source "MiuiLegacyPasswordUnlockScreen.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->-get1(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->-get1(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
