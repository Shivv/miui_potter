.class public final Lcom/android/keyguard/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final aod_clock_min_top_margin_hour:I = 0x7f0c00a0

.field public static final aod_clock_padding_top:I = 0x7f0c009f

.field public static final badget_text_size:I = 0x7f0c00a7

.field public static final battery_charging_old_progress_view_text_size:I = 0x7f0c0037

.field public static final battery_charging_progress_view_bottom_margin:I = 0x7f0c0036

.field public static final battery_charging_progress_view_large_text_size:I = 0x7f0c003a

.field public static final battery_charging_progress_view_msg_size:I = 0x7f0c003b

.field public static final battery_charging_progress_view_text_size:I = 0x7f0c0039

.field public static final battery_charging_wireless_progress_view_text_size:I = 0x7f0c0038

.field public static final clock_container_margin_top:I = 0x7f0c00a1

.field public static final clock_container_translation_x:I = 0x7f0c00a2

.field public static final clock_container_translation_y:I = 0x7f0c00a3

.field public static final clock_text_size:I = 0x7f0c00a6

.field public static final content_text_size:I = 0x7f0c00a5

.field public static final date_margin_bottom:I = 0x7f0c009e

.field public static final date_padding_end:I = 0x7f0c00a4

.field public static final full_smart_cover_battery_text_size:I = 0x7f0c0076

.field public static final full_smart_cover_date_size:I = 0x7f0c0074

.field public static final full_smart_cover_num_size:I = 0x7f0c0075

.field public static final full_smart_cover_time_colon_size:I = 0x7f0c0073

.field public static final full_smart_cover_time_size:I = 0x7f0c0072

.field public static final gxzw_anim_height:I = 0x7f0c00bd

.field public static final gxzw_anim_width:I = 0x7f0c00bc

.field public static final horizontal_time_magin_top_in_small_window:I = 0x7f0c00b7

.field public static final icon_margin_start:I = 0x7f0c00a8

.field public static final icons_margin_top:I = 0x7f0c00a9

.field public static final keyboard_key_text_size:I = 0x7f0c0000

.field public static final keyguard_ble_unlock_text_size:I = 0x7f0c003e

.field public static final keyguard_ble_unlock_textview_margin_top:I = 0x7f0c003d

.field public static final keyguard_bottom_icon_init_alpha:I = 0x7f0c0048

.field public static final keyguard_charging_info_back_arrow_bottom_margin:I = 0x7f0c0089

.field public static final keyguard_charging_info_back_arrow_top_margin:I = 0x7f0c0088

.field public static final keyguard_charging_info_charging_current_layout_height:I = 0x7f0c0084

.field public static final keyguard_charging_info_charging_middle_list_item_layout_height:I = 0x7f0c0085

.field public static final keyguard_charging_info_charging_num_margin_top:I = 0x7f0c008e

.field public static final keyguard_charging_info_content_text_size:I = 0x7f0c008d

.field public static final keyguard_charging_info_data_text_size:I = 0x7f0c008a

.field public static final keyguard_charging_info_des_size:I = 0x7f0c008b

.field public static final keyguard_charging_info_first_list_height:I = 0x7f0c007e

.field public static final keyguard_charging_info_horizontal_separator_top_margin:I = 0x7f0c0083

.field public static final keyguard_charging_info_last_list_layout_height:I = 0x7f0c0087

.field public static final keyguard_charging_info_layout_margin_end:I = 0x7f0c0080

.field public static final keyguard_charging_info_layout_margin_start:I = 0x7f0c007f

.field public static final keyguard_charging_info_list_top_margin:I = 0x7f0c007d

.field public static final keyguard_charging_info_middle_list_icon_margin_end:I = 0x7f0c0086

.field public static final keyguard_charging_info_title_text_size:I = 0x7f0c008c

.field public static final keyguard_charging_info_vertical_separator_layout_height:I = 0x7f0c0082

.field public static final keyguard_charging_info_vertical_separator_layout_width:I = 0x7f0c0081

.field public static final keyguard_charging_view_after_enlarge_top_margin:I = 0x7f0c007c

.field public static final keyguard_charging_view_bottom_margin:I = 0x7f0c007a

.field public static final keyguard_charging_view_height:I = 0x7f0c0078

.field public static final keyguard_charging_view_height_after_enlarge:I = 0x7f0c007b

.field public static final keyguard_charging_view_line_width:I = 0x7f0c0077

.field public static final keyguard_charging_view_width:I = 0x7f0c0079

.field public static final keyguard_date_info_top_margin:I = 0x7f0c00b1

.field public static final keyguard_digital_clock_text_left_margin:I = 0x7f0c0058

.field public static final keyguard_digital_clock_text_padding_top:I = 0x7f0c0059

.field public static final keyguard_digital_time_width:I = 0x7f0c009d

.field public static final keyguard_edittext_horizontal_margin:I = 0x7f0c001c

.field public static final keyguard_face_input_camera_height:I = 0x7f0c009c

.field public static final keyguard_face_unlock_icon_bottom:I = 0x7f0c005b

.field public static final keyguard_horizontal_gesture_slop:I = 0x7f0c0044

.field public static final keyguard_horizontal_time_margin_top:I = 0x7f0c00ae

.field public static final keyguard_horizontal_time_text_size:I = 0x7f0c00af

.field public static final keyguard_left_view_item_margin_bottom:I = 0x7f0c00b8

.field public static final keyguard_lockscreen_status_line_font_size:I = 0x7f0c0002

.field public static final keyguard_move_left_item_layout_height:I = 0x7f0c008f

.field public static final keyguard_move_left_item_margin:I = 0x7f0c0095

.field public static final keyguard_move_left_layout_left_margint_twoorone:I = 0x7f0c0092

.field public static final keyguard_move_left_layout_margin_left_right:I = 0x7f0c0094

.field public static final keyguard_move_left_layout_right_margint_twoorone:I = 0x7f0c0093

.field public static final keyguard_move_left_layout_top_margint_fiveorthree:I = 0x7f0c0090

.field public static final keyguard_move_left_layout_top_margint_twoorone:I = 0x7f0c0091

.field public static final keyguard_move_left_layout_torchligth_bottom:I = 0x7f0c0096

.field public static final keyguard_move_left_litem_image_margin_end:I = 0x7f0c0098

.field public static final keyguard_move_left_litem_image_margin_start:I = 0x7f0c0097

.field public static final keyguard_move_left_litem_textview_margin_end:I = 0x7f0c0099

.field public static final keyguard_move_left_litem_textview_name_size:I = 0x7f0c009a

.field public static final keyguard_move_left_litem_textview_num_size:I = 0x7f0c009b

.field public static final keyguard_music_controller_height_in_lite_mode:I = 0x7f0c004b

.field public static final keyguard_notification_list_view_bottom_margin:I = 0x7f0c0049

.field public static final keyguard_notification_list_view_bottom_margin_in_small_window:I = 0x7f0c004c

.field public static final keyguard_notification_list_view_top_margin:I = 0x7f0c004a

.field public static final keyguard_notification_magin_top:I = 0x7f0c00b5

.field public static final keyguard_notification_magin_top_without_owner_info:I = 0x7f0c00b6

.field public static final keyguard_notification_number_view_height:I = 0x7f0c001d

.field public static final keyguard_notification_number_view_padding:I = 0x7f0c00bf

.field public static final keyguard_notification_number_view_width:I = 0x7f0c001e

.field public static final keyguard_owner_info_left_margin:I = 0x7f0c00b2

.field public static final keyguard_owner_info_text_width:I = 0x7f0c00b4

.field public static final keyguard_owner_info_top_margin:I = 0x7f0c00b3

.field public static final keyguard_slide_hint_text_bounce_animation_offset:I = 0x7f0c0046

.field public static final keyguard_slide_icon_view_padding:I = 0x7f0c0047

.field public static final keyguard_slide_icon_view_size:I = 0x7f0c005a

.field public static final keyguard_time_digital_text_size:I = 0x7f0c0056

.field public static final keyguard_time_digital_unlock_text_size:I = 0x7f0c0057

.field public static final keyguard_time_margin_top_without_navigationBar:I = 0x7f0c00aa

.field public static final keyguard_torch_cover_bottom_padding:I = 0x7f0c0045

.field public static final keyguard_unlock_face_unlock_icon_bottom:I = 0x7f0c005c

.field public static final keyguard_vertical_gesture_slop:I = 0x7f0c0042

.field public static final keyguard_vertical_gesture_slop_in_suspected_mode:I = 0x7f0c0043

.field public static final keyguard_vertical_time_margin_top:I = 0x7f0c00ab

.field public static final keyguard_vertical_time_min_top_margin_hour:I = 0x7f0c00ac

.field public static final keyguard_vertical_time_text_size:I = 0x7f0c00ad

.field public static final loading_item_padding:I = 0x7f0c0052

.field public static final lock_screen_date_and_battery_left_margin:I = 0x7f0c002c

.field public static final lock_screen_date_and_battery_padding_left:I = 0x7f0c00be

.field public static final lock_screen_date_and_battery_top_margin:I = 0x7f0c002b

.field public static final lock_screen_digital_clock_left_margin:I = 0x7f0c002a

.field public static final lock_screen_notification_icon_margin_right:I = 0x7f0c0015

.field public static final lock_screen_notification_icon_size:I = 0x7f0c0014

.field public static final lock_screen_notification_list_horizontal_margin:I = 0x7f0c0011

.field public static final lock_screen_notification_number_view_text_size:I = 0x7f0c0033

.field public static final lock_screen_notification_padding_left:I = 0x7f0c0012

.field public static final lock_screen_notification_padding_right:I = 0x7f0c0013

.field public static final lock_screen_notification_time_margin_left:I = 0x7f0c0029

.field public static final lock_screen_numeric_keyboard_alphabet_text_size:I = 0x7f0c0031

.field public static final lock_screen_numeric_keyboard_item_height:I = 0x7f0c002f

.field public static final lock_screen_numeric_keyboard_number_text_size:I = 0x7f0c0030

.field public static final lock_screen_wallpaper_preview_right_margin:I = 0x7f0c002e

.field public static final lock_screen_wallpaper_preview_top_margin:I = 0x7f0c002d

.field public static final miui_account_unlock_screen_horizontal_margin:I = 0x7f0c001f

.field public static final miui_account_unlock_screen_name_size:I = 0x7f0c0016

.field public static final miui_avatar_size_in_unlock_screen:I = 0x7f0c000c

.field public static final miui_common_unlock_screen_common_msg_text_size:I = 0x7f0c0003

.field public static final miui_common_unlock_screen_common_unlock_msg_text_size:I = 0x7f0c0004

.field public static final miui_common_unlock_screen_date_text_size:I = 0x7f0c00b0

.field public static final miui_common_unlock_screen_head_bottom_margin:I = 0x7f0c0018

.field public static final miui_common_unlock_screen_tip_shake_distance:I = 0x7f0c0005

.field public static final miui_common_unlock_screen_top_margin:I = 0x7f0c0007

.field public static final miui_default_lock_screen_bottom_margin:I = 0x7f0c000b

.field public static final miui_default_lock_screen_notification_item_click_to_enter_text_size:I = 0x7f0c0027

.field public static final miui_default_lock_screen_notification_item_msg_text_size:I = 0x7f0c0026

.field public static final miui_default_lock_screen_notification_item_time_text_size:I = 0x7f0c0024

.field public static final miui_default_lock_screen_notification_item_title_text_size:I = 0x7f0c0025

.field public static final miui_default_lock_screen_top_margin:I = 0x7f0c0006

.field public static final miui_default_lock_screen_unlock_hint_text_size:I = 0x7f0c0028

.field public static final miui_mixed_password_input_text_size:I = 0x7f0c0017

.field public static final miui_numeric_keyboard_emergency_text_size:I = 0x7f0c000a

.field public static final miui_numeric_keyboard_row_height:I = 0x7f0c0008

.field public static final miui_pattern_lock_pattern_view_horizontal_margin:I = 0x7f0c0009

.field public static final miui_sim_unlock_error_msg_font_size:I = 0x7f0c0041

.field public static final miui_unlock_owner_info_max_width:I = 0x7f0c0021

.field public static final miui_unlock_owner_info_text_size:I = 0x7f0c0022

.field public static final miui_unlock_owner_info_unlock_text_size:I = 0x7f0c0023

.field public static final miui_unlock_pattern_cell_view_size:I = 0x7f0c0020

.field public static final mixed_password_emergency_call_text_size:I = 0x7f0c0051

.field public static final mixed_password_unlock_screen_edittext_top_margin:I = 0x7f0c0032

.field public static final notification_block_width:I = 0x7f0c0055

.field public static final notification_item_height:I = 0x7f0c0054

.field public static final notification_item_margin_bottom:I = 0x7f0c0053

.field public static final numeric_cell_horizontal_margin:I = 0x7f0c0010

.field public static final numeric_cell_padding:I = 0x7f0c000e

.field public static final numeric_cell_vertical_margin:I = 0x7f0c000f

.field public static final numeric_encrypt_dot_size:I = 0x7f0c000d

.field public static final numeric_password_screen_dots_bottom_margin:I = 0x7f0c001a

.field public static final numeric_password_screen_dots_padding:I = 0x7f0c001b

.field public static final numeric_password_screen_dots_top_margin:I = 0x7f0c0019

.field public static final password_unlock_screen_bottom_margin:I = 0x7f0c0040

.field public static final password_unlock_screen_input_pad_horizontal_margin:I = 0x7f0c003f

.field public static final pattern_unlock_screen_bottom_margin:I = 0x7f0c0034

.field public static final pattern_unlock_screen_emergency_text_size:I = 0x7f0c0035

.field public static final secure_keyguard_fingerprint_hint_msg_text_size:I = 0x7f0c004e

.field public static final secure_keyguard_fingerprint_hint_msg_top_margin:I = 0x7f0c0050

.field public static final secure_keyguard_fingerprint_hint_title_text_size:I = 0x7f0c004d

.field public static final secure_keyguard_fingerprint_hint_title_top_margin:I = 0x7f0c004f

.field public static final smart_cover_battery_padding:I = 0x7f0c0071

.field public static final smart_cover_battery_text_size:I = 0x7f0c0070

.field public static final smart_cover_num_size:I = 0x7f0c006f

.field public static final smart_cover_time_size:I = 0x7f0c006e

.field public static final status_bar_height:I = 0x7f0c0001

.field public static final unlock_pattern_view_diameter_factor:I = 0x7f0c003c

.field public static final vertical_clock_date_translate_extra:I = 0x7f0c00b9

.field public static final vertical_clock_sim_card_magin_top:I = 0x7f0c00ba

.field public static final wallpaper_des_content_text_size:I = 0x7f0c005e

.field public static final wallpaper_des_more_text_size:I = 0x7f0c005f

.field public static final wallpaper_des_title_padding:I = 0x7f0c0060

.field public static final wallpaper_des_title_text_size:I = 0x7f0c005d

.field public static final wallpaper_global_des_bottom_notify_area_min_height:I = 0x7f0c006a

.field public static final wallpaper_global_des_content_text_margin_top:I = 0x7f0c0065

.field public static final wallpaper_global_des_content_text_size:I = 0x7f0c0066

.field public static final wallpaper_global_des_drop_down_mark_height:I = 0x7f0c006c

.field public static final wallpaper_global_des_drop_down_mark_width:I = 0x7f0c006b

.field public static final wallpaper_global_des_drop_down_notify_text_size:I = 0x7f0c006d

.field public static final wallpaper_global_des_from_container_margin_top:I = 0x7f0c0067

.field public static final wallpaper_global_des_from_logo_margin_left:I = 0x7f0c0069

.field public static final wallpaper_global_des_from_text_size:I = 0x7f0c0068

.field public static final wallpaper_global_des_text_area_margin_left:I = 0x7f0c0062

.field public static final wallpaper_global_des_text_area_margin_right:I = 0x7f0c0063

.field public static final wallpaper_global_des_text_area_margin_top:I = 0x7f0c0061

.field public static final wallpaper_global_des_title_text_size:I = 0x7f0c0064

.field public static final wireless_chagre_slowly_dialog_button_height:I = 0x7f0c00bb


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
