.class public Lcom/android/keyguard/UnlockSimHelper;
.super Ljava/lang/Object;
.source "UnlockSimHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkPin(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v1, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    invoke-direct {v1}, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;-><init>()V

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, p2, p1}, Lmiui/telephony/TelephonyManagerEx;->supplyPinReportResultForSlot(ILjava/lang/String;)[I

    move-result-object v0

    aget v3, v0, v4

    if-nez v3, :cond_1

    iput-boolean v5, v1, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->unlockSuccess:Z

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    aget v3, v0, v4

    if-ne v3, v5, :cond_0

    iput-boolean v4, v1, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->unlockSuccess:Z

    aget v3, v0, v5

    iput v3, v1, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->attemptsRemaining:I

    goto :goto_0
.end method

.method public static checkPuk(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v1, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    invoke-direct {v1}, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;-><init>()V

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, p3, p2, p1}, Lmiui/telephony/TelephonyManagerEx;->supplyPukReportResultForSlot(ILjava/lang/String;Ljava/lang/String;)[I

    move-result-object v0

    aget v3, v0, v4

    if-nez v3, :cond_1

    iput-boolean v5, v1, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->unlockSuccess:Z

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    aget v3, v0, v4

    if-ne v3, v5, :cond_0

    iput-boolean v4, v1, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->unlockSuccess:Z

    aget v3, v0, v5

    iput v3, v1, Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;->attemptsRemaining:I

    goto :goto_0
.end method
