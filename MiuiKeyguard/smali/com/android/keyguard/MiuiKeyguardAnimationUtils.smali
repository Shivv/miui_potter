.class public Lcom/android/keyguard/MiuiKeyguardAnimationUtils;
.super Ljava/lang/Object;
.source "MiuiKeyguardAnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiKeyguardAnimationUtils$1;
    }
.end annotation


# static fields
.field public static final ANIMATION_DURATION:I = 0x1f4

.field public static final SCROLLER_INTERPOLATOR:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardAnimationUtils$1;

    invoke-direct {v0}, Lcom/android/keyguard/MiuiKeyguardAnimationUtils$1;-><init>()V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardAnimationUtils;->SCROLLER_INTERPOLATOR:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static computeScrollDuration(FFZ)F
    .locals 4

    const/high16 v1, 0x43fa0000    # 500.0f

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v2, p0

    const/high16 v3, 0x40800000    # 4.0f

    mul-float/2addr v2, v3

    div-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    if-eqz p2, :cond_0

    move v0, v1

    :cond_0
    return v0
.end method
