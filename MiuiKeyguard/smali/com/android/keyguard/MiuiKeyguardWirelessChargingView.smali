.class public Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;
.super Landroid/view/View;
.source "MiuiKeyguardWirelessChargingView.java"


# static fields
.field private static final CHANGE_VALUE:F = 0.9f

.field private static final CHANGE_VALUE_RATIO:F

.field private static final DELAY_TIME:I = 0xc8

.field private static final START_EXIT_ANIM_TIME:I = 0x20f8

.field private static final START_STAY_ANIM_TIME:I = 0x5a0

.field private static final STAY_LIGHT_BREATH_COUNT:I = 0x3

.field public static final TOTAL_ENTER_ANIM_TIME:I = 0x5a0

.field public static final TOTAL_EXIT_ANIM_TIME:I = 0x3e8

.field private static final TOTAL_STAY_ANIM_LAST_TIME:I = 0x1f4

.field public static final TOTAL_STAY_ANIM_TIME:I = 0x1b58


# instance fields
.field private final COUNT:I

.field private mBitmapPaint:Landroid/graphics/Paint;

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mLevel:I

.field private mLightingBitmap:Landroid/graphics/Bitmap;

.field private mSineEaseInOutInterpolator:Landroid/view/animation/Interpolator;

.field private mTextNumPaint:Landroid/graphics/Paint;

.field private mTextSize:I

.field private mTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide v0, 0x3fb9999a00000000L    # 0.10000002384185791

    const-wide v2, 0x3fd5555555555555L    # 0.3333333333333333

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double v0, v2, v0

    double-to-float v0, v0

    sput v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->CHANGE_VALUE_RATIO:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x15

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->COUNT:I

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Lmiui/maml/animation/interpolater/SineEaseInOutInterpolater;

    invoke-direct {v0}, Lmiui/maml/animation/interpolater/SineEaseInOutInterpolater;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mSineEaseInOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x15

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->COUNT:I

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Lmiui/maml/animation/interpolater/SineEaseInOutInterpolater;

    invoke-direct {v0}, Lmiui/maml/animation/interpolater/SineEaseInOutInterpolater;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mSineEaseInOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x15

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->COUNT:I

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Lmiui/maml/animation/interpolater/SineEaseInOutInterpolater;

    invoke-direct {v0}, Lmiui/maml/animation/interpolater/SineEaseInOutInterpolater;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mSineEaseInOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->init()V

    return-void
.end method

.method private drawBitmapAnim(Landroid/graphics/Canvas;)V
    .locals 18

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCenterX()I

    move-result v3

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCenterY()I

    move-result v4

    const/high16 v6, 0x43480000    # 200.0f

    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v13, 0x5a0

    if-ge v12, v13, :cond_4

    const/16 v9, 0xc8

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v12, v12, -0xc8

    int-to-float v12, v12

    const/high16 v13, 0x43480000    # 200.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v12, v12, v13

    if-lez v12, :cond_2

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_0
    const/4 v12, 0x0

    cmpg-float v12, v1, v12

    if-gez v12, :cond_0

    const/4 v1, 0x0

    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mBitmapPaint:Landroid/graphics/Paint;

    const/high16 v13, 0x437f0000    # 255.0f

    mul-float/2addr v13, v1

    float-to-int v13, v13

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v12, v12, -0xc8

    int-to-float v12, v12

    const/high16 v13, 0x43480000    # 200.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x3f000000    # 0.5f

    add-float/2addr v12, v13

    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v12, v12, v13

    if-lez v12, :cond_3

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_1
    const/high16 v12, 0x3f000000    # 0.5f

    cmpg-float v12, v7, v12

    if-gez v12, :cond_1

    const/high16 v7, 0x3f000000    # 0.5f

    :cond_1
    :goto_2
    new-instance v8, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLightingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLightingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct {v8, v14, v15, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v5, Landroid/graphics/Rect;

    int-to-float v12, v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLightingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    int-to-float v13, v13

    mul-float/2addr v13, v7

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    int-to-float v13, v4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLightingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v7

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    sub-float/2addr v13, v14

    float-to-int v13, v13

    int-to-float v14, v3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLightingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    int-to-float v15, v15

    mul-float/2addr v15, v7

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    float-to-int v14, v14

    int-to-float v15, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLightingBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v7

    const/high16 v17, 0x40000000    # 2.0f

    div-float v16, v16, v17

    add-float v15, v15, v16

    float-to-int v15, v15

    invoke-direct {v5, v12, v13, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLightingBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v8, v5, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void

    :cond_2
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v12, v12, -0xc8

    int-to-float v12, v12

    const/high16 v13, 0x43480000    # 200.0f

    div-float v1, v12, v13

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v12, v12, -0xc8

    int-to-float v12, v12

    const/high16 v13, 0x43480000    # 200.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x3f000000    # 0.5f

    add-float v7, v12, v13

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v13, 0x5a0

    if-le v12, v13, :cond_7

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v13, 0x20f8

    if-gt v12, v13, :cond_7

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v13, 0x5a0

    if-le v12, v13, :cond_6

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v13, 0x1f04

    if-gt v12, v13, :cond_6

    const/16 v9, 0x5a0

    const/16 v2, 0x876

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v12, v12, -0x5a0

    rem-int/lit16 v11, v12, 0x876

    const/16 v12, 0x43b

    if-gt v11, v12, :cond_5

    int-to-float v12, v11

    const/high16 v13, 0x3f800000    # 1.0f

    mul-float/2addr v12, v13

    const/16 v13, 0x43b

    int-to-float v13, v13

    div-float/2addr v12, v13

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v1, v13, v12

    :goto_3
    const-string/jumbo v12, "alphaRatio"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "alphaRatio = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getSineEaseInOutInterpolation(F)F

    move-result v13

    const v14, 0x3f4ccccd    # 0.8f

    mul-float/2addr v13, v14

    const v14, 0x3e4ccccd    # 0.2f

    add-float/2addr v13, v14

    const/high16 v14, 0x437f0000    # 255.0f

    mul-float/2addr v13, v14

    float-to-int v13, v13

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    const/high16 v7, 0x3f800000    # 1.0f

    goto/16 :goto_2

    :cond_5
    add-int/lit16 v12, v11, -0x43b

    int-to-float v12, v12

    const/high16 v13, 0x3f800000    # 1.0f

    mul-float/2addr v12, v13

    const/16 v13, 0x43b

    int-to-float v13, v13

    div-float v1, v12, v13

    goto :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mBitmapPaint:Landroid/graphics/Paint;

    const/16 v13, 0xff

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    const/high16 v7, 0x3f800000    # 1.0f

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v13, 0x20f8

    if-le v12, v13, :cond_1

    const/high16 v6, 0x44480000    # 800.0f

    const/16 v9, 0x20f8

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v12, v12, -0x20f8

    int-to-float v12, v12

    const/high16 v13, 0x44480000    # 800.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v12, v12, v13

    if-lez v12, :cond_8

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getSineEaseInOutInterpolation(F)F

    move-result v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mBitmapPaint:Landroid/graphics/Paint;

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v1

    const/high16 v14, 0x437f0000    # 255.0f

    mul-float/2addr v13, v14

    float-to-int v13, v13

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v12, v12, -0x20f8

    int-to-float v12, v12

    const/high16 v13, 0x44480000    # 800.0f

    div-float/2addr v12, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getSineEaseInOutInterpolation(F)F

    move-result v10

    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, v10, v12

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v12, v13, v12

    const/high16 v13, 0x3f000000    # 0.5f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_9

    const/high16 v7, 0x3f000000    # 0.5f

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v12, v12, -0x20f8

    int-to-float v12, v12

    const/high16 v13, 0x44480000    # 800.0f

    div-float v1, v12, v13

    goto :goto_4

    :cond_9
    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, v10, v12

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v7, v13, v12

    goto/16 :goto_2
.end method

.method private drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V
    .locals 18

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCenterX()I

    move-result v3

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCenterY()I

    move-result v4

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    sub-int v15, v15, p2

    if-gez v15, :cond_1

    const/4 v12, 0x0

    :goto_0
    int-to-float v15, v12

    div-float v15, v15, p3

    const/high16 v16, 0x3f800000    # 1.0f

    cmpl-float v15, v15, v16

    if-lez v15, :cond_2

    const/high16 v11, 0x3f800000    # 1.0f

    :goto_1
    move/from16 v0, p4

    int-to-float v15, v0

    sub-int v16, p5, p4

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getInterpolation(F)F

    move-result v17

    mul-float v16, v16, v17

    add-float v10, v15, v16

    move/from16 v0, p5

    int-to-float v15, v0

    cmpl-float v15, v10, v15

    if-lez v15, :cond_3

    move/from16 v0, p5

    int-to-float v5, v0

    :goto_2
    int-to-float v15, v12

    div-float v15, v15, p3

    const/high16 v16, 0x3f800000    # 1.0f

    cmpl-float v15, v15, v16

    if-lez v15, :cond_4

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_3
    sub-float v15, p7, p6

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getInterpolation(F)F

    move-result v16

    mul-float v15, v15, v16

    add-float v6, p6, v15

    const/16 v9, 0x80

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    move/from16 v0, p2

    if-gt v15, v0, :cond_5

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    div-int v15, v15, p2

    int-to-float v2, v15

    const/high16 v15, 0x3f800000    # 1.0f

    cmpl-float v15, v2, v15

    if-lez v15, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    :cond_0
    const/high16 v15, 0x43000000    # 128.0f

    mul-float/2addr v15, v2

    float-to-int v1, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v15, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    :goto_4
    const/4 v8, 0x0

    :goto_5
    const/16 v15, 0x15

    if-ge v8, v15, :cond_7

    int-to-float v15, v8

    const/high16 v16, 0x43b40000    # 360.0f

    mul-float v15, v15, v16

    const/high16 v16, 0x41a80000    # 21.0f

    div-float v15, v15, v16

    add-float v15, v15, p8

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v15}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCircleCoordinateX(IFF)F

    move-result v13

    int-to-float v15, v8

    const/high16 v16, 0x43b40000    # 360.0f

    mul-float v15, v15, v16

    const/high16 v16, 0x41a80000    # 21.0f

    div-float v15, v15, v16

    add-float v15, v15, p8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v15}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCircleCoordinateY(IFF)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v6, v15}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    :cond_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    sub-int v12, v15, p2

    goto/16 :goto_0

    :cond_2
    int-to-float v15, v12

    div-float v11, v15, p3

    goto/16 :goto_1

    :cond_3
    move v5, v10

    goto :goto_2

    :cond_4
    int-to-float v15, v12

    div-float v7, v15, p3

    goto :goto_3

    :cond_5
    div-int/lit8 v15, v12, 0x64

    int-to-float v2, v15

    const/high16 v15, 0x3f800000    # 1.0f

    cmpl-float v15, v2, v15

    if-lez v15, :cond_6

    const/high16 v2, 0x3f800000    # 1.0f

    :cond_6
    const/16 v15, 0x7f

    int-to-float v15, v15

    mul-float/2addr v15, v2

    float-to-int v15, v15

    add-int/lit16 v1, v15, 0x80

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v15, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_4

    :cond_7
    return-void
.end method

.method private drawEnterAnim(Landroid/graphics/Canvas;)V
    .locals 11

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v7, 0x41600000    # 14.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v2, 0xc8

    const/high16 v3, 0x43960000    # 300.0f

    const/16 v4, 0x61

    const/16 v5, 0x8b

    const/4 v10, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v7, 0x41300000    # 11.0f

    const/4 v9, 0x0

    const/16 v2, 0x12c

    const/high16 v3, 0x43960000    # 300.0f

    const/16 v4, 0x8b

    const/16 v5, 0xb5

    const v8, 0x41092492

    const/4 v10, 0x2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v7, 0x40e00000    # 7.0f

    const/4 v8, 0x0

    const/high16 v9, 0x40c00000    # 6.0f

    const/16 v2, 0x190

    const/high16 v3, 0x442f0000    # 700.0f

    const/16 v4, 0xb5

    const/16 v5, 0xdf

    const/4 v10, 0x3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v9, 0x41f00000    # 30.0f

    const/16 v2, 0x1f4

    const/high16 v3, 0x44820000    # 1040.0f

    const/16 v4, 0xd2

    const/16 v5, 0x10a

    const/high16 v7, 0x40900000    # 4.5f

    const v8, 0x41092492

    const/4 v10, 0x4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawTextAnim(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawBitmapAnim(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private drawExitAnim(Landroid/graphics/Canvas;)V
    .locals 9

    const/high16 v6, 0x41600000    # 14.0f

    const/high16 v7, 0x41200000    # 10.0f

    const/4 v8, 0x0

    const/16 v2, 0x20f8

    const/high16 v3, 0x44b40000    # 1440.0f

    const/16 v4, 0x8b

    const/16 v5, 0x61

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawExitAnim(Landroid/graphics/Canvas;IFIIFFF)V

    const/high16 v6, 0x41300000    # 11.0f

    const/high16 v7, 0x41000000    # 8.0f

    const/16 v2, 0x215c

    const v3, 0x44a78000    # 1340.0f

    const/16 v4, 0xb5

    const/16 v5, 0x8b

    const v8, 0x41092492

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawExitAnim(Landroid/graphics/Canvas;IFIIFFF)V

    const/high16 v6, 0x40e00000    # 7.0f

    const/high16 v7, 0x40c00000    # 6.0f

    const/4 v8, 0x0

    const/16 v2, 0x21c0

    const/high16 v3, 0x449b0000    # 1240.0f

    const/16 v4, 0xdf

    const/16 v5, 0xb5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawExitAnim(Landroid/graphics/Canvas;IFIIFFF)V

    const/high16 v7, 0x40400000    # 3.0f

    const/16 v2, 0x2224

    const v3, 0x448e8000    # 1140.0f

    const/16 v4, 0x10a

    const/16 v5, 0xd2

    const/high16 v6, 0x40900000    # 4.5f

    const v8, 0x41092492

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawExitAnim(Landroid/graphics/Canvas;IFIIFFF)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawTextAnim(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawBitmapAnim(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private drawExitAnim(Landroid/graphics/Canvas;IFIIFFF)V
    .locals 17

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCenterX()I

    move-result v2

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCenterY()I

    move-result v3

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    sub-int v14, v14, p2

    if-gez v14, :cond_0

    const/4 v11, 0x0

    :goto_0
    int-to-float v14, v11

    div-float v14, v14, p3

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getSineEaseInOutInterpolation(F)F

    move-result v14

    sub-float v15, p7, p6

    mul-float/2addr v14, v15

    add-float v5, v14, p6

    cmpg-float v14, v5, p7

    if-gez v14, :cond_1

    move/from16 v6, p7

    :goto_1
    int-to-float v14, v11

    div-float v14, v14, p3

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getSineEaseInOutInterpolation(F)F

    move-result v14

    sub-int v15, p5, p4

    int-to-float v15, v15

    mul-float/2addr v14, v15

    move/from16 v0, p4

    int-to-float v15, v0

    add-float v10, v14, v15

    move/from16 v0, p5

    int-to-float v14, v0

    cmpg-float v14, v10, v14

    if-gez v14, :cond_2

    move/from16 v0, p5

    int-to-float v4, v0

    :goto_2
    const/16 v9, 0x20f8

    const/high16 v7, 0x44480000    # 800.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v14, v14, -0x20f8

    int-to-float v14, v14

    const/high16 v15, 0x44480000    # 800.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x3f800000    # 1.0f

    cmpl-float v14, v14, v15

    if-lez v14, :cond_3

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getSineEaseInOutInterpolation(F)F

    move-result v1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v15, v1

    const/high16 v16, 0x437f0000    # 255.0f

    mul-float v15, v15, v16

    float-to-int v15, v15

    invoke-virtual {v14, v15}, Landroid/graphics/Paint;->setAlpha(I)V

    const/4 v8, 0x0

    :goto_4
    const/16 v14, 0x15

    if-ge v8, v14, :cond_4

    int-to-float v14, v8

    const/high16 v15, 0x43b40000    # 360.0f

    mul-float/2addr v14, v15

    const/high16 v15, 0x41a80000    # 21.0f

    div-float/2addr v14, v15

    add-float v14, v14, p8

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v14}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCircleCoordinateX(IFF)F

    move-result v12

    int-to-float v14, v8

    const/high16 v15, 0x43b40000    # 360.0f

    mul-float/2addr v14, v15

    const/high16 v15, 0x41a80000    # 21.0f

    div-float/2addr v14, v15

    add-float v14, v14, p8

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v14}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCircleCoordinateY(IFF)F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13, v6, v14}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    sub-int v11, v14, p2

    goto/16 :goto_0

    :cond_1
    move v6, v5

    goto :goto_1

    :cond_2
    move v4, v10

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v14, v14, -0x20f8

    int-to-float v14, v14

    const/high16 v15, 0x44480000    # 800.0f

    div-float v1, v14, v15

    goto :goto_3

    :cond_4
    return-void
.end method

.method private drawStayAnim(Landroid/graphics/Canvas;)V
    .locals 11

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v7, 0x41600000    # 14.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v2, 0xc8

    const/high16 v3, 0x43960000    # 300.0f

    const/16 v4, 0x61

    const/16 v5, 0x8b

    const/4 v10, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v7, 0x41300000    # 11.0f

    const/4 v9, 0x0

    const/16 v2, 0x12c

    const/high16 v3, 0x43960000    # 300.0f

    const/16 v4, 0x8b

    const/16 v5, 0xb5

    const v8, 0x41092492

    const/4 v10, 0x2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v7, 0x40e00000    # 7.0f

    const/4 v8, 0x0

    const/high16 v9, 0x40c00000    # 6.0f

    const/16 v2, 0x190

    const/high16 v3, 0x442f0000    # 700.0f

    const/16 v4, 0xb5

    const/16 v5, 0xdf

    const/4 v10, 0x3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v9, 0x41f00000    # 30.0f

    const/16 v2, 0x1f4

    const/high16 v3, 0x44820000    # 1040.0f

    const/16 v4, 0xd2

    const/16 v5, 0x10a

    const/high16 v7, 0x40900000    # 4.5f

    const v8, 0x41092492

    const/4 v10, 0x4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAndStayAnim(Landroid/graphics/Canvas;IFIIFFFFI)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawTextAnim(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawBitmapAnim(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private drawTextAnim(Landroid/graphics/Canvas;)V
    .locals 14

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCenterX()I

    move-result v1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getCenterY()I

    move-result v10

    add-int/lit16 v10, v10, 0x10a

    add-int/lit8 v2, v10, 0x5a

    const/high16 v3, 0x43480000    # 200.0f

    iget v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v11, 0x5a0

    if-ge v10, v11, :cond_3

    const/16 v4, 0xc8

    iget v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v10, v10, -0xc8

    int-to-float v10, v10

    const/high16 v11, 0x43480000    # 200.0f

    div-float/2addr v10, v11

    const/high16 v11, 0x3f800000    # 1.0f

    cmpl-float v10, v10, v11

    if-lez v10, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    const/4 v10, 0x0

    cmpg-float v10, v0, v10

    if-gez v10, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    const/high16 v11, 0x437f0000    # 255.0f

    mul-float/2addr v11, v0

    float-to-int v11, v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    iget v11, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextSize:I

    int-to-float v11, v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    :cond_1
    :goto_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f0b000e

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLevel:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v7

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v10}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v11}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v10, v11

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    add-double/2addr v10, v12

    double-to-int v6, v10

    int-to-float v10, v1

    const/high16 v11, 0x40000000    # 2.0f

    div-float v11, v7, v11

    sub-float v8, v10, v11

    div-int/lit8 v10, v6, 0x4

    add-int/2addr v10, v2

    int-to-float v9, v10

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v8, v9, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void

    :cond_2
    iget v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v10, v10, -0xc8

    int-to-float v10, v10

    const/high16 v11, 0x43480000    # 200.0f

    div-float v0, v10, v11

    goto :goto_0

    :cond_3
    iget v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v11, 0x5a0

    if-le v10, v11, :cond_4

    iget v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v11, 0x20f8

    if-gt v10, v11, :cond_4

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    const/16 v11, 0xff

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    iget v11, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextSize:I

    int-to-float v11, v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    goto/16 :goto_1

    :cond_4
    iget v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    const/16 v11, 0x20f8

    if-le v10, v11, :cond_1

    const/high16 v3, 0x44480000    # 800.0f

    const/16 v4, 0x20f8

    iget v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v10, v10, -0x20f8

    int-to-float v10, v10

    const/high16 v11, 0x44480000    # 800.0f

    div-float/2addr v10, v11

    const/high16 v11, 0x3f800000    # 1.0f

    cmpl-float v10, v10, v11

    if-lez v10, :cond_5

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_2
    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getSineEaseInOutInterpolation(F)F

    move-result v0

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v0

    const/high16 v12, 0x437f0000    # 255.0f

    mul-float/2addr v11, v12

    float-to-int v11, v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    iget v11, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextSize:I

    int-to-float v11, v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    goto/16 :goto_1

    :cond_5
    iget v10, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    add-int/lit16 v10, v10, -0x20f8

    int-to-float v10, v10

    const/high16 v11, 0x44480000    # 800.0f

    div-float v0, v10, v11

    goto :goto_2
.end method

.method private getCenterX()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private getCenterY()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x69

    return v0
.end method

.method private getCircleCoordinateX(IFF)F
    .locals 8

    int-to-float v0, p1

    float-to-double v2, p2

    float-to-double v4, p3

    const-wide v6, 0x40091eb851eb851fL    # 3.14

    mul-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L    # 180.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private getCircleCoordinateY(IFF)F
    .locals 8

    int-to-float v0, p1

    float-to-double v2, p2

    float-to-double v4, p3

    const-wide v6, 0x40091eb851eb851fL    # 3.14

    mul-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L    # 180.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private getInterpolation(F)F
    .locals 6

    const/high16 v3, 0x3f800000    # 1.0f

    sget v1, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->CHANGE_VALUE_RATIO:F

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    sub-float v1, v3, p1

    float-to-double v2, v1

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v2, v4, v2

    double-to-float v1, v2

    return v1

    :cond_0
    sget v1, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->CHANGE_VALUE_RATIO:F

    sub-float v1, p1, v1

    sget v2, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->CHANGE_VALUE_RATIO:F

    sub-float v2, v3, v2

    div-float v0, v1, v2

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    const v2, 0x3dccccd0    # 0.100000024f

    mul-float/2addr v1, v2

    const v2, 0x3f666666    # 0.9f

    add-float/2addr v1, v2

    return v1
.end method

.method private getSineEaseInOutInterpolation(F)F
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mSineEaseInOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method

.method private init()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextSize:I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020080

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLightingBitmap:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mBitmapPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTextNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const/16 v2, 0x20f8

    const/16 v1, 0x5a0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    if-gt v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawEnterAnim(Landroid/graphics/Canvas;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    if-le v0, v1, :cond_2

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    if-gt v0, v2, :cond_2

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawStayAnim(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    if-le v0, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->drawExitAnim(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public setChargingProgress(I)V
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mLevel:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->invalidate()V

    return-void
.end method

.method public setTime(I)V
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->mTime:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->invalidate()V

    return-void
.end method
