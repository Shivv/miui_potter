.class public Lcom/android/keyguard/PhoneSignalController;
.super Ljava/lang/Object;
.source "PhoneSignalController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/PhoneSignalController$1;,
        Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBroadCastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

.field private mPhoneCount:I

.field private mPhoneSignalAvailable:[Z

.field private mSignalChangeCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/keyguard/PhoneSignalController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/PhoneSignalController;)[Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneSignalAvailable:[Z

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/PhoneSignalController;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mSignalChangeCallbacks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/PhoneSignalController;Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/PhoneSignalController;->notifyPhoneSignalChangeCallback(Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/PhoneSignalController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/PhoneSignalController;->unregisterPhoneStateListener()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/keyguard/PhoneSignalController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/keyguard/PhoneSignalController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mSignalChangeCallbacks:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/keyguard/PhoneSignalController$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/PhoneSignalController$1;-><init>(Lcom/android/keyguard/PhoneSignalController;)V

    iput-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mBroadCastReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/keyguard/PhoneSignalController;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneCount:I

    iget v0, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneCount:I

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneSignalAvailable:[Z

    iget v0, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneCount:I

    new-array v0, v0, [Landroid/telephony/PhoneStateListener;

    iput-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

    invoke-virtual {p0}, Lcom/android/keyguard/PhoneSignalController;->registerPhoneStateListener()V

    new-instance v3, Landroid/content/IntentFilter;

    const-string/jumbo v0, "android.intent.action.ACTION_SUBINFO_RECORD_UPDATED"

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/keyguard/PhoneSignalController;->mBroadCastReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method private getPhoneStateListener(JI)Landroid/telephony/PhoneStateListener;
    .locals 3

    new-instance v0, Lcom/android/keyguard/PhoneSignalController$2;

    invoke-static {p1, p2}, Lcom/android/keyguard/CompatibilityL;->convertSubId(J)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p0, v1, p3}, Lcom/android/keyguard/PhoneSignalController$2;-><init>(Lcom/android/keyguard/PhoneSignalController;Ljava/lang/Integer;I)V

    return-object v0
.end method

.method private notifyPhoneSignalChangeCallback(Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneSignalAvailable:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneSignalAvailable:[Z

    aget-boolean v2, v2, v0

    or-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/android/keyguard/PhoneSignalController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "signalAvailable="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1, v1}, Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;->onSignalChange(Z)V

    return-void
.end method

.method private unregisterPhoneStateListener()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneCount:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/PhoneSignalController;->mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/PhoneSignalController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/PhoneSignalController;->mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

    aget-object v2, v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public registerPhoneSignalChangeCallback(Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mSignalChangeCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mSignalChangeCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/android/keyguard/PhoneSignalController;->notifyPhoneSignalChangeCallback(Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V

    :cond_0
    return-void
.end method

.method registerPhoneStateListener()V
    .locals 7

    const/4 v6, 0x0

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneCount:I

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/keyguard/PhoneSignalController;->mPhoneSignalAvailable:[Z

    const/4 v3, 0x0

    aput-boolean v3, v2, v0

    invoke-static {v0}, Lcom/android/keyguard/CompatibilityL;->getSubId(I)Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/android/keyguard/PhoneSignalController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "registerPhoneStateListener subId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/android/keyguard/PhoneSignalController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "registerPhoneStateListener slotId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/PhoneSignalController;->mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v4, v5, v0}, Lcom/android/keyguard/PhoneSignalController;->getPhoneStateListener(JI)Landroid/telephony/PhoneStateListener;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/android/keyguard/PhoneSignalController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/PhoneSignalController;->mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

    aget-object v3, v3, v0

    const/16 v4, 0x100

    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/keyguard/PhoneSignalController;->mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

    aput-object v6, v2, v0

    goto :goto_1

    :cond_2
    return-void
.end method

.method public removeCallback(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/PhoneSignalController;->mSignalChangeCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
