.class Lcom/android/keyguard/MiuiDefaultLockScreen$4;
.super Landroid/database/ContentObserver;
.source "MiuiDefaultLockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$4;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$4;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$4;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/MiuiSettings$System;->isInSmallWindowMode(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set5(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$4;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$4;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get43(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
