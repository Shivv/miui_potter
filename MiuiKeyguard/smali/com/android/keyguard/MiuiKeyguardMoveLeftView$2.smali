.class Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;
.super Landroid/os/AsyncTask;
.source "MiuiKeyguardMoveLeftView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->initLeftView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 8

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get13(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/hardware/ConsumerIrManager;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v6, "consumer_ir"

    invoke-virtual {v2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/ConsumerIrManager;

    invoke-static {v5, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set2(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Landroid/hardware/ConsumerIrManager;)Landroid/hardware/ConsumerIrManager;

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get13(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/hardware/ConsumerIrManager;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get13(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/hardware/ConsumerIrManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/hardware/ConsumerIrManager;->hasIrEmitter()Z

    move-result v5

    invoke-static {v2, v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set3(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Z)Z

    :cond_1
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v1

    if-nez v1, :cond_3

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2, v4}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set5(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Z)Z

    :cond_2
    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    return-object v2

    :cond_3
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v5

    invoke-static {v2, v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set5(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Z)Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get19(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v2

    if-eqz v2, :cond_2

    :try_start_0
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v6, "com.miui.tsmclient"

    invoke-static {v2, v6}, Lcom/android/keyguard/MiuiKeyguardUtils;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v6, "com.miui.tsmclient"

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v6, 0x12

    if-lt v2, v6, :cond_4

    move v2, v3

    :goto_1
    invoke-static {v5, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set8(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "MiuiKeyguardMoveLeftView"

    const-string/jumbo v4, "cannot find TSMClient Package"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_1

    :cond_5
    move v2, v4

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 5

    const/16 v3, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get15(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get20(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get16(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set4(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;I)I

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get28(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get19(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get5(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get9(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get16(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set4(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;I)I

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-wrap0(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get25(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get16(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set4(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;I)I

    :cond_3
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-wrap1(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get17(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get16(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set4(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;I)I

    :cond_4
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get16(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_5

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get29(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I

    move-result v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get31(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get30(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get3(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_5
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get18(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Lcom/android/keyguard/MiuiDefaultLockScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->initKeyguardShortcut()V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$2;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
