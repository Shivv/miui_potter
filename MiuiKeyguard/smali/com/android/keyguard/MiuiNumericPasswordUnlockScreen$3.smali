.class Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;
.super Ljava/lang/Object;
.source "MiuiNumericPasswordUnlockScreen.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleWrongPassword(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

.field final synthetic val$timeoutMs:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    iput p2, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->val$timeoutMs:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-wrap0(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->-get0(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Lcom/android/keyguard/MiuiNumericInputView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiNumericInputView;->setEnabled(Z)V

    iget v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->val$timeoutMs:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    iget v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->val$timeoutMs:I

    int-to-long v2, v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleAttemptLockout(J)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    iget v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;->val$timeoutMs:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->writePasswordOutTime(J)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
