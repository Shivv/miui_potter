.class Lcom/android/keyguard/MiuiKeyguardViewMediator$1;
.super Ljava/lang/Object;
.source "MiuiKeyguardViewMediator.java"

# interfaces
.implements Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onState(I)V
    .locals 3

    const-string/jumbo v0, "KeyguardViewMediator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Ble state change onState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get20(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lmiui/bluetooth/ble/MiBleUnlockProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get20(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lmiui/bluetooth/ble/MiBleUnlockProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get13(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->registerUnlockListener(Lmiui/bluetooth/ble/MiBleUnlockProfile$OnUnlockStateChangeListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get20(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lmiui/bluetooth/ble/MiBleUnlockProfile;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get20(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lmiui/bluetooth/ble/MiBleUnlockProfile;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/bluetooth/ble/MiBleUnlockProfile;->unregisterUnlockListener()V

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v0

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->FAILED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$1;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap25(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V

    goto :goto_0
.end method
