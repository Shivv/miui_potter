.class Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;
.super Landroid/os/AsyncTask;
.source "MiuiLockPatternKeyguardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiLockPatternKeyguardView;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardWindowController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isDefaultLockScreenTheme()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-set0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUtils;->isLiveWallpaper(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap10(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap7(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
