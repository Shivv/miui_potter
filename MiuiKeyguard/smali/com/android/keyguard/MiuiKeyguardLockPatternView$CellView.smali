.class Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;
.super Landroid/view/View;
.source "MiuiKeyguardLockPatternView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardLockPatternView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CellView"
.end annotation


# instance fields
.field private mPartOfPattern:Z

.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;


# direct methods
.method public constructor <init>(Lcom/android/keyguard/MiuiKeyguardLockPatternView;Landroid/content/Context;II)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, 0x3f800000    # 1.0f

    iget-boolean v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->mPartOfPattern:Z

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get5(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get7(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-result-object v9

    sget-object v10, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    if-eq v9, v10, :cond_2

    :cond_0
    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get1(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_0
    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get3(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I

    move-result v8

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get2(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I

    move-result v0

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get10(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)F

    move-result v5

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get9(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)F

    move-result v4

    int-to-float v9, v8

    sub-float v9, v5, v9

    div-float/2addr v9, v12

    float-to-int v2, v9

    int-to-float v9, v0

    sub-float v9, v4, v9

    div-float/2addr v9, v12

    float-to-int v3, v9

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get10(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)F

    move-result v9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get3(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-static {v9, v11}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get9(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)F

    move-result v9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get2(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-static {v9, v11}, Ljava/lang/Math;->min(FF)F

    move-result v7

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get4(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Matrix;

    move-result-object v9

    int-to-float v10, v2

    int-to-float v11, v3

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Matrix;->setTranslate(FF)V

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get4(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Matrix;

    move-result-object v9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get3(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get2(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get4(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Matrix;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get4(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Matrix;

    move-result-object v9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get3(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I

    move-result v10

    neg-int v10, v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get2(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I

    move-result v11

    neg-int v11, v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    if-eqz v1, :cond_1

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get4(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Matrix;

    move-result-object v9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get6(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Paint;

    move-result-object v10

    invoke-virtual {p1, v1, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    :cond_1
    return-void

    :cond_2
    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get8(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get1(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_0

    :cond_3
    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get7(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-result-object v9

    sget-object v10, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    if-ne v9, v10, :cond_4

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get0(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_0

    :cond_4
    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get7(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-result-object v9

    sget-object v10, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    if-eq v9, v10, :cond_5

    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get7(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-result-object v9

    sget-object v10, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    if-ne v9, v10, :cond_6

    :cond_5
    iget-object v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get1(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_0

    :cond_6
    new-instance v9, Ljava/lang/IllegalStateException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "unknown display mode "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get7(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9
.end method

.method protected onMeasure(II)V
    .locals 4

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get10(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v2, p1, v3}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-wrap0(Lcom/android/keyguard/MiuiKeyguardLockPatternView;II)I

    move-result v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->this$0:Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-get9(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v2, p2, v3}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->-wrap0(Lcom/android/keyguard/MiuiKeyguardLockPatternView;II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    invoke-virtual {p0, v0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setPartOfPattern(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->mPartOfPattern:Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->invalidate()V

    return-void
.end method
