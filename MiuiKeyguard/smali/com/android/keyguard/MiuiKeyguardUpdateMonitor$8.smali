.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;
.super Landroid/database/ContentObserver;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f090000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set8(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;I)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "battery_level_low_customized"

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get11(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)I

    move-result v3

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set8(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;I)I

    return-void
.end method
