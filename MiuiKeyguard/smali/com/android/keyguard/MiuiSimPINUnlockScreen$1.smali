.class Lcom/android/keyguard/MiuiSimPINUnlockScreen$1;
.super Ljava/lang/Object;
.source "MiuiSimPINUnlockScreen.java"

# interfaces
.implements Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiSimPINUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiSimPINUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInput(I)V
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->getFocusedEditText()Landroid/widget/EditText;

    move-result-object v0

    const/16 v1, 0xa

    if-ne p1, v1, :cond_1

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->removeLast(Landroid/widget/EditText;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    add-int/lit8 v1, p1, 0x30

    int-to-char v1, v1

    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
