.class Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;
.super Landroid/view/DisplayEventReceiver;
.source "MiuiGxzwOverlayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiGxzwOverlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UIDisplayEventReceiver"
.end annotation


# instance fields
.field private mHBMRunnable:Ljava/lang/Runnable;

.field private mUIRunnable:Ljava/lang/Runnable;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;->mHBMRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private constructor <init>(Landroid/os/Looper;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/DisplayEventReceiver;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;->mUIRunnable:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;->mHBMRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Looper;Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;-><init>(Landroid/os/Looper;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public onVsync(JII)V
    .locals 15

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    const-wide/32 v4, 0xf4240

    div-long v4, v10, v4

    const-wide/32 v12, 0xf4240

    div-long v12, p1, v12

    sub-long v8, v4, v12

    const-wide/16 v4, 0x5

    cmp-long v2, v8, v4

    if-lez v2, :cond_0

    const-wide/16 v4, 0x10

    :try_start_0
    rem-long v4, v8, v4

    const-wide/16 v12, 0x10

    sub-long v4, v12, v4

    const-wide/16 v12, 0x5

    add-long/2addr v4, v12

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    iget-object v2, p0, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;->mUIRunnable:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    new-instance v1, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;

    move-object v2, p0

    move-wide/from16 v4, p1

    invoke-direct/range {v1 .. v7}, Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver$1;-><init>(Lcom/android/keyguard/MiuiGxzwOverlayView$UIDisplayEventReceiver;Ljava/util/Timer;JJ)V

    const-wide/16 v4, 0xe

    invoke-virtual {v3, v1, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void

    :cond_0
    const-wide/16 v4, 0x5

    sub-long/2addr v4, v8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
