.class public Lcom/megvii/facepp/sdk/Lite;
.super Ljava/lang/Object;
.source "Lite.java"


# static fields
.field public static final FEATURE_SIZE:I = 0x2710

.field public static final IMAGE_SIZE:I = 0x9c40

.field public static final MG_ATTR_BLUR:I = 0x14

.field public static final MG_ATTR_EYE_CLOSE:I = 0x16

.field public static final MG_ATTR_EYE_OCCLUSION:I = 0x15

.field public static final MG_ATTR_MOUTH_OCCLUSION:I = 0x17

.field public static final MG_UNLOCK_BAD_LIGHT:I = 0x1a

.field public static final MG_UNLOCK_COMPARE_FAILURE:I = 0xc

.field public static final MG_UNLOCK_DARKLIGHT:I = 0x1e

.field public static final MG_UNLOCK_FACE_BLUR:I = 0x1c

.field public static final MG_UNLOCK_FACE_DOWN:I = 0x12

.field public static final MG_UNLOCK_FACE_MULTI:I = 0x1b

.field public static final MG_UNLOCK_FACE_NOT_COMPLETE:I = 0x1d

.field public static final MG_UNLOCK_FACE_NOT_FOUND:I = 0x5

.field public static final MG_UNLOCK_FACE_OFFSET_BOTTOM:I = 0xb

.field public static final MG_UNLOCK_FACE_OFFSET_LEFT:I = 0x8

.field public static final MG_UNLOCK_FACE_OFFSET_RIGHT:I = 0xa

.field public static final MG_UNLOCK_FACE_OFFSET_TOP:I = 0x9

.field public static final MG_UNLOCK_FACE_QUALITY:I = 0x4

.field public static final MG_UNLOCK_FACE_RISE:I = 0x10

.field public static final MG_UNLOCK_FACE_ROTATED_LEFT:I = 0xf

.field public static final MG_UNLOCK_FACE_ROTATED_RIGHT:I = 0x11

.field public static final MG_UNLOCK_FACE_SCALE_TOO_LARGE:I = 0x7

.field public static final MG_UNLOCK_FACE_SCALE_TOO_SMALL:I = 0x6

.field public static final MG_UNLOCK_FAILED:I = 0x3

.field public static final MG_UNLOCK_FEATURE_MISS:I = 0x18

.field public static final MG_UNLOCK_FEATURE_VERSION_ERROR:I = 0x19

.field public static final MG_UNLOCK_HALF_SHADOW:I = 0x20

.field public static final MG_UNLOCK_HIGHLIGHT:I = 0x1f

.field public static final MG_UNLOCK_INVALID_ARGUMENT:I = 0x1

.field public static final MG_UNLOCK_INVALID_HANDLE:I = 0x2

.field public static final MG_UNLOCK_KEEP:I = 0x13

.field public static final MG_UNLOCK_LIVENESS_FAILURE:I = 0xe

.field public static final MG_UNLOCK_LIVENESS_WARNING:I = 0xd

.field public static final MG_UNLOCK_OK:I = 0x0

.field public static final RESULT_SIZE:I = 0x14

.field private static sInstance:Lcom/megvii/facepp/sdk/Lite;


# instance fields
.field private handle:J

.field private mEncryptor:Lcom/megvii/facepp/sdk/UnlockEncryptor;

.field private mFeatureRestoreHelper:Lcom/megvii/facepp/sdk/FeatureRestoreHelper;

.field private mPath:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    new-instance v0, Lcom/megvii/facepp/sdk/FeatureRestoreHelper;

    invoke-direct {v0}, Lcom/megvii/facepp/sdk/FeatureRestoreHelper;-><init>()V

    iput-object v0, p0, Lcom/megvii/facepp/sdk/Lite;->mFeatureRestoreHelper:Lcom/megvii/facepp/sdk/FeatureRestoreHelper;

    return-void
.end method

.method public static final getInstance()Lcom/megvii/facepp/sdk/Lite;
    .locals 1

    sget-object v0, Lcom/megvii/facepp/sdk/Lite;->sInstance:Lcom/megvii/facepp/sdk/Lite;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/megvii/facepp/sdk/Lite;->sInstance:Lcom/megvii/facepp/sdk/Lite;

    return-object v0

    :cond_0
    new-instance v0, Lcom/megvii/facepp/sdk/Lite;

    invoke-direct {v0}, Lcom/megvii/facepp/sdk/Lite;-><init>()V

    sput-object v0, Lcom/megvii/facepp/sdk/Lite;->sInstance:Lcom/megvii/facepp/sdk/Lite;

    goto :goto_0
.end method


# virtual methods
.method public checkFeatureValid(I)I
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1, p1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeCheckFeatureValid(JI)I

    move-result v0

    return v0
.end method

.method public compare([BIIIZZ[I)I
    .locals 11

    move-object/from16 v0, p7

    array-length v2, v0

    const/16 v3, 0x14

    if-lt v2, v3, :cond_0

    iget-wide v2, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    move-object v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    invoke-static/range {v2 .. v10}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeCompare(J[BIIIZZ[I)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public compareFeatures([B[FIZ)I
    .locals 6

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeCompareFeatures(J[B[FIZ)I

    move-result v0

    return v0
.end method

.method public deleteFeature()I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/megvii/facepp/sdk/Lite;->deleteFeature(I)I

    move-result v0

    return v0
.end method

.method public deleteFeature(I)I
    .locals 4

    iget-wide v2, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v2, v3, p1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeDeleteFeature(JI)I

    move-result v0

    iget-object v1, p0, Lcom/megvii/facepp/sdk/Lite;->mFeatureRestoreHelper:Lcom/megvii/facepp/sdk/FeatureRestoreHelper;

    iget-object v2, p0, Lcom/megvii/facepp/sdk/Lite;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/megvii/facepp/sdk/FeatureRestoreHelper;->deleteRestoreImage(Ljava/lang/String;I)V

    return v0
.end method

.method public getFeature([BIII[B)I
    .locals 7

    array-length v0, p5

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_0

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeGetFeature(J[BIII[B)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public getFeatureCount()I
    .locals 1

    invoke-static {}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeGetFeatureCount()I

    move-result v0

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeGetVersion(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initAll(Ljava/lang/String;Ljava/lang/String;[B)I
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1, p1, p2, p3}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeInitAll(JLjava/lang/String;Ljava/lang/String;[B)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public initHandle(Ljava/lang/String;)V
    .locals 4

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeInitHandle(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    iput-object p1, p0, Lcom/megvii/facepp/sdk/Lite;->mPath:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public initHandle(Ljava/lang/String;Lcom/megvii/facepp/sdk/UnlockEncryptor;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/megvii/facepp/sdk/Lite;->initHandle(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/megvii/facepp/sdk/Lite;->mEncryptor:Lcom/megvii/facepp/sdk/UnlockEncryptor;

    iget-object v0, p0, Lcom/megvii/facepp/sdk/Lite;->mFeatureRestoreHelper:Lcom/megvii/facepp/sdk/FeatureRestoreHelper;

    iget-object v1, p0, Lcom/megvii/facepp/sdk/Lite;->mEncryptor:Lcom/megvii/facepp/sdk/UnlockEncryptor;

    invoke-virtual {v0, v1}, Lcom/megvii/facepp/sdk/FeatureRestoreHelper;->setUnlockEncryptor(Lcom/megvii/facepp/sdk/UnlockEncryptor;)V

    return-void
.end method

.method public intDetect([B)I
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1, p1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeInitDetect(J[B)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public intLive(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1, p1, p2}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeInitLive(JLjava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public prepare()I
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativePrepare(J)I

    move-result v0

    return v0
.end method

.method public release()V
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeRelease(J)J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    return-void
.end method

.method public releaseDetect()I
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeReleaseDetect(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public releaseLive()I
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeReleaseLive(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public reset()I
    .locals 2

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    invoke-static {v0, v1}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeReset(J)I

    move-result v0

    return v0
.end method

.method public restoreFeature()I
    .locals 2

    iget-object v0, p0, Lcom/megvii/facepp/sdk/Lite;->mFeatureRestoreHelper:Lcom/megvii/facepp/sdk/FeatureRestoreHelper;

    iget-object v1, p0, Lcom/megvii/facepp/sdk/Lite;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/megvii/facepp/sdk/FeatureRestoreHelper;->restoreAllFeature(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public saveFeature([BIIIZ[B[B)I
    .locals 9

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/megvii/facepp/sdk/Lite;->updateFeature([BIIIZ[B[BI)I

    move-result v0

    return v0
.end method

.method public saveFeature([BIIIZ[B[B[I)I
    .locals 13

    move-object/from16 v0, p7

    array-length v2, v0

    const v3, 0x9c40

    if-ge v2, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    return v2

    :cond_1
    move-object/from16 v0, p6

    array-length v2, v0

    const/16 v3, 0x2710

    if-lt v2, v3, :cond_0

    iget-wide v2, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    if-nez p5, :cond_2

    const/4 v8, 0x0

    :goto_0
    move-object v4, p1

    move v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-static/range {v2 .. v11}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeSaveFeature(J[BIIII[B[B[I)I

    move-result v12

    if-eqz v12, :cond_3

    :goto_1
    return v12

    :cond_2
    const/4 v8, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/megvii/facepp/sdk/Lite;->mFeatureRestoreHelper:Lcom/megvii/facepp/sdk/FeatureRestoreHelper;

    iget-object v3, p0, Lcom/megvii/facepp/sdk/Lite;->mPath:Ljava/lang/String;

    const/4 v4, 0x0

    aget v4, p8, v4

    move-object/from16 v0, p7

    invoke-virtual {v2, v0, v3, v4}, Lcom/megvii/facepp/sdk/FeatureRestoreHelper;->saveRestoreImage([BLjava/lang/String;I)V

    goto :goto_1
.end method

.method public setConfig(FFF)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setConfig(FFFF)I
    .locals 7

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    const/4 v6, 0x0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v6}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeSetConfig(JFFFFZ)I

    move-result v0

    return v0
.end method

.method public setConfig(FFFFZ)I
    .locals 7

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeSetConfig(JFFFFZ)I

    move-result v0

    return v0
.end method

.method public setDetectArea(IIII)I
    .locals 6

    iget-wide v0, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeSetDetectArea(JIIII)I

    move-result v0

    return v0
.end method

.method public updateFeature([BIIIZ[B[BI)I
    .locals 13

    move-object/from16 v0, p7

    array-length v2, v0

    const v3, 0x9c40

    if-ge v2, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    return v2

    :cond_1
    move-object/from16 v0, p6

    array-length v2, v0

    const/16 v3, 0x2710

    if-lt v2, v3, :cond_0

    iget-wide v2, p0, Lcom/megvii/facepp/sdk/Lite;->handle:J

    if-nez p5, :cond_2

    const/4 v8, 0x0

    :goto_0
    move-object v4, p1

    move v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move/from16 v11, p8

    invoke-static/range {v2 .. v11}, Lcom/megvii/facepp/sdk/jni/LiteApi;->nativeUpdateFeature(J[BIIII[B[BI)I

    move-result v12

    if-eqz v12, :cond_3

    :goto_1
    return v12

    :cond_2
    const/4 v8, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/megvii/facepp/sdk/Lite;->mFeatureRestoreHelper:Lcom/megvii/facepp/sdk/FeatureRestoreHelper;

    iget-object v3, p0, Lcom/megvii/facepp/sdk/Lite;->mPath:Ljava/lang/String;

    move-object/from16 v0, p7

    move/from16 v1, p8

    invoke-virtual {v2, v0, v3, v1}, Lcom/megvii/facepp/sdk/FeatureRestoreHelper;->saveRestoreImage([BLjava/lang/String;I)V

    goto :goto_1
.end method
