.class Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$3;
.super Ljava/lang/Object;
.source "MiuiUnlockScreenDigitalClock.java"

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;


# direct methods
.method constructor <init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$3;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public makeView()Landroid/view/View;
    .locals 3

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$3;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    iget-object v1, v1, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$3;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$3;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-static {v1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->-get6(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-object v0
.end method
