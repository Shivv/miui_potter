.class Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;
.super Ljava/lang/Object;
.source "MiuiUnlockScreenDigitalClock.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;


# direct methods
.method constructor <init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updateNotificationList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-static {v0, p1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->-set2(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    new-instance v1, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5$1;

    invoke-direct {v1, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5$1;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;)V

    invoke-virtual {v0, v1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public onChange(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;->updateNotificationList(Ljava/util/List;)V

    return-void
.end method

.method public onRegistered(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;->updateNotificationList(Ljava/util/List;)V

    return-void
.end method
