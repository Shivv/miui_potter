.class Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;
.super Landroid/database/ContentObserver;
.source "MiuiUnlockScreenDigitalClock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;


# direct methods
.method constructor <init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    iget-object v1, v1, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/MiuiSettings$System;->isInSmallWindowMode(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->-set1(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;Z)Z

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;->this$0:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    new-instance v1, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4$1;

    invoke-direct {v1, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4$1;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;)V

    invoke-virtual {v0, v1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
