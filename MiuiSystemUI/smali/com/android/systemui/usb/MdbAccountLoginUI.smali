.class public Lcom/android/systemui/usb/MdbAccountLoginUI;
.super Landroid/app/Activity;
.source "MdbAccountLoginUI.java"


# instance fields
.field private misFirstStart:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi"

    const-string/jumbo v2, "micloud"

    move-object v4, v3

    move-object v5, p0

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/usb/MdbAccountLoginUI;->misFirstStart:Z

    return-void
.end method

.method protected onStart()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/usb/MdbAccountLoginUI;->misFirstStart:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/usb/MdbAccountLoginUI;->finish()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/usb/MdbAccountLoginUI;->misFirstStart:Z

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method
