.class public final enum Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;
.super Ljava/lang/Enum;
.source "OldmanHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/OldmanHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MobileMarkShownMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

.field private static final synthetic -com-android-systemui-OldmanHelper$MobileMarkShownModeSwitchesValues:[I

.field public static final enum BOTH_ALWAYS_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

.field public static final enum BOTH_ALWAYS_SHOWN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

.field public static final enum BOTH_SHOWN_ONLY_WHEN_BOTH:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

.field public static final enum SLOT0_AUTO_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;


# direct methods
.method private static synthetic -getcom-android-systemui-OldmanHelper$MobileMarkShownModeSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->-com-android-systemui-OldmanHelper$MobileMarkShownModeSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->-com-android-systemui-OldmanHelper$MobileMarkShownModeSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->values()[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_ALWAYS_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    invoke-virtual {v1}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_ALWAYS_SHOWN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    invoke-virtual {v1}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_SHOWN_ONLY_WHEN_BOTH:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    invoke-virtual {v1}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->SLOT0_AUTO_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    invoke-virtual {v1}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    sput-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->-com-android-systemui-OldmanHelper$MobileMarkShownModeSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    const-string/jumbo v1, "BOTH_ALWAYS_SHOWN"

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_ALWAYS_SHOWN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    new-instance v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    const-string/jumbo v1, "BOTH_ALWAYS_HIDDEN"

    invoke-direct {v0, v1, v3}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_ALWAYS_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    new-instance v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    const-string/jumbo v1, "BOTH_SHOWN_ONLY_WHEN_BOTH"

    invoke-direct {v0, v1, v4}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_SHOWN_ONLY_WHEN_BOTH:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    new-instance v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    const-string/jumbo v1, "SLOT0_AUTO_HIDDEN"

    invoke-direct {v0, v1, v5}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->SLOT0_AUTO_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    sget-object v1, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_ALWAYS_SHOWN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_ALWAYS_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->BOTH_SHOWN_ONLY_WHEN_BOTH:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->SLOT0_AUTO_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->$VALUES:[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;
    .locals 1

    const-class v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    return-object v0
.end method

.method public static values()[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;
    .locals 1

    sget-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->$VALUES:[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    return-object v0
.end method


# virtual methods
.method public isMobileMarkShown(IZZ)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->-getcom-android-systemui-OldmanHelper$MobileMarkShownModeSwitchesValues()[I

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    return v0

    :pswitch_0
    return v3

    :pswitch_1
    return v0

    :pswitch_2
    if-eqz p2, :cond_0

    :goto_0
    return p3

    :cond_0
    move p3, v0

    goto :goto_0

    :pswitch_3
    packed-switch p1, :pswitch_data_1

    return v0

    :pswitch_4
    if-eqz p2, :cond_1

    :goto_1
    return p3

    :cond_1
    move p3, v0

    goto :goto_1

    :pswitch_5
    return v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
