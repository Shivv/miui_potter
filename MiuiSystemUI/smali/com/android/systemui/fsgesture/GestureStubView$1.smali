.class Lcom/android/systemui/fsgesture/GestureStubView$1;
.super Ljava/lang/Object;
.source "GestureStubView.java"

# interfaces
.implements Lcom/android/systemui/fsgesture/GesturesBackController$GesturesBackCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/fsgesture/GestureStubView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/fsgesture/GestureStubView;


# direct methods
.method constructor <init>(Lcom/android/systemui/fsgesture/GestureStubView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSwipeProcess(ZF)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v0}, Lcom/android/systemui/fsgesture/GestureStubView;->-get4(Lcom/android/systemui/fsgesture/GestureStubView;)Lcom/android/systemui/fsgesture/GestureBackArrowView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->setReadyFinish(Z)V

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v0}, Lcom/android/systemui/fsgesture/GestureStubView;->-get4(Lcom/android/systemui/fsgesture/GestureStubView;)Lcom/android/systemui/fsgesture/GestureBackArrowView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->onActionMove(F)V

    return-void
.end method

.method public onSwipeStart(ZF)V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1, v3}, Lcom/android/systemui/fsgesture/GestureStubView;->-set1(Lcom/android/systemui/fsgesture/GestureStubView;Z)Z

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-wrap2(Lcom/android/systemui/fsgesture/GestureStubView;)V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-get4(Lcom/android/systemui/fsgesture/GestureStubView;)Lcom/android/systemui/fsgesture/GestureBackArrowView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-wrap6(Lcom/android/systemui/fsgesture/GestureStubView;)V

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-get9(Lcom/android/systemui/fsgesture/GestureStubView;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1, p2}, Lcom/android/systemui/fsgesture/GestureStubView;->-wrap0(Lcom/android/systemui/fsgesture/GestureStubView;F)[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-get4(Lcom/android/systemui/fsgesture/GestureStubView;)Lcom/android/systemui/fsgesture/GestureBackArrowView;

    move-result-object v1

    aget v2, v0, v2

    int-to-float v2, v2

    aget v3, v0, v3

    int-to-float v3, v3

    const/4 v4, 0x2

    aget v4, v0, v4

    int-to-float v4, v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->onActionDown(FFF)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-get4(Lcom/android/systemui/fsgesture/GestureStubView;)Lcom/android/systemui/fsgesture/GestureBackArrowView;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v1, p2, v2, v3}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->onActionDown(FFF)V

    goto :goto_0
.end method

.method public onSwipeStop(ZF)V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-set1(Lcom/android/systemui/fsgesture/GestureStubView;Z)Z

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v0}, Lcom/android/systemui/fsgesture/GestureStubView;->-get6(Lcom/android/systemui/fsgesture/GestureStubView;)Lcom/android/systemui/fsgesture/GestureStubView$H;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-get6(Lcom/android/systemui/fsgesture/GestureStubView;)Lcom/android/systemui/fsgesture/GestureStubView$H;

    move-result-object v1

    const/16 v2, 0x102

    invoke-virtual {v1, v2}, Lcom/android/systemui/fsgesture/GestureStubView$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/systemui/fsgesture/GestureStubView$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-static {v0}, Lcom/android/systemui/fsgesture/GestureStubView;->-get4(Lcom/android/systemui/fsgesture/GestureStubView;)Lcom/android/systemui/fsgesture/GestureBackArrowView;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/fsgesture/GestureStubView$1$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/fsgesture/GestureStubView$1$1;-><init>(Lcom/android/systemui/fsgesture/GestureStubView$1;)V

    invoke-virtual {v0, p2, v1}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->onActionUp(FLandroid/animation/Animator$AnimatorListener;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-wrap4(Lcom/android/systemui/fsgesture/GestureStubView;I)V

    :cond_0
    return-void
.end method

.method public onSwipeStopDirect()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-set1(Lcom/android/systemui/fsgesture/GestureStubView;Z)Z

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureStubView$1;->this$0:Lcom/android/systemui/fsgesture/GestureStubView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/systemui/fsgesture/GestureStubView;->-wrap4(Lcom/android/systemui/fsgesture/GestureStubView;I)V

    return-void
.end method
