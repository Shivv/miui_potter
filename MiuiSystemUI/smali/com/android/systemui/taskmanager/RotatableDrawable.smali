.class public Lcom/android/systemui/taskmanager/RotatableDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "RotatableDrawable.java"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mContentIsPort:Z

.field private mDstRect:Landroid/graphics/Rect;

.field private mIsDoNotRotate:Z

.field private mPaint:Landroid/graphics/Paint;

.field private mRotation:I

.field private mSrcRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mDstRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mSrcRect:Landroid/graphics/Rect;

    return-void
.end method

.method private fillDstRect()V
    .locals 7

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mIsDoNotRotate:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mDstRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mDstRect:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    iget v5, v0, Landroid/graphics/Rect;->left:I

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->isPortNow()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    :goto_1
    add-int/2addr v5, v1

    iget v6, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->isPortNow()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    :goto_2
    add-int/2addr v1, v6

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    goto :goto_2
.end method

.method private isPortNow()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mRotation:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mRotation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    const/high16 v3, 0x43340000    # 180.0f

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x67000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mIsDoNotRotate:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->isPortNow()Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mContentIsPort:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mRotation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v1, 0x42b40000    # 90.0f

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mDstRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mDstRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mSrcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mDstRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mContentIsPort:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mRotation:I

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto :goto_1
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->isPortNow()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->isPortNow()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->fillDstRect()V

    return-void
.end method

.method public releaseBitmap()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method

.method public setup(Landroid/graphics/Bitmap;IZZ)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iput p2, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mRotation:I

    iput-boolean p3, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mContentIsPort:Z

    iput-boolean p4, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mIsDoNotRotate:Z

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RotatableDrawable;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->fillDstRect()V

    return-void
.end method
