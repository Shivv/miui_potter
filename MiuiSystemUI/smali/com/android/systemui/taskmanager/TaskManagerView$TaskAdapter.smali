.class Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;
.super Landroid/widget/BaseAdapter;
.source "TaskManagerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/taskmanager/TaskManagerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TaskAdapter"
.end annotation


# instance fields
.field mTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/systemui/taskmanager/TaskManagerView;


# direct methods
.method public constructor <init>(Lcom/android/systemui/taskmanager/TaskManagerView;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->mTasks:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskInfo;

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->persistentTaskId:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v2}, Lcom/android/systemui/taskmanager/TaskManagerView;->-get0(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/content/Context;

    move-result-object v3

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v4, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v3, v2, v4}, Lcom/android/systemui/taskmanager/TaskItemView;->createTaskItemView(Landroid/content/Context;Lcom/android/systemui/taskmanager/TaskInfo;Lcom/android/systemui/taskmanager/TaskManagerView;)Lcom/android/systemui/taskmanager/TaskItemView;

    move-result-object v0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/TaskManagerView;->mClickListenerForQuit:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/android/systemui/taskmanager/TaskItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskItemView;->refreshScreenshotMode()V

    return-object v0
.end method
