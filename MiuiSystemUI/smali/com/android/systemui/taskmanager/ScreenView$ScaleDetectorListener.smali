.class Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;
.super Ljava/lang/Object;
.source "ScreenView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/taskmanager/ScreenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleDetectorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/ScreenView;


# direct methods
.method private constructor <init>(Lcom/android/systemui/taskmanager/ScreenView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/systemui/taskmanager/ScreenView;Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;-><init>(Lcom/android/systemui/taskmanager/ScreenView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    const/4 v0, 0x0

    const v2, 0x3f4ccccd    # 0.8f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-virtual {v2, p1}, Lcom/android/systemui/taskmanager/ScreenView;->onPinchIn(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    :cond_0
    const v2, 0x3f99999a    # 1.2f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-virtual {v2, p1}, Lcom/android/systemui/taskmanager/ScreenView;->onPinchOut(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    :cond_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Lcom/android/systemui/taskmanager/ScreenView;->setTouchState(Landroid/view/MotionEvent;I)V

    :cond_2
    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/ScreenView;->-get3(Lcom/android/systemui/taskmanager/ScreenView;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0

    return-void
.end method
