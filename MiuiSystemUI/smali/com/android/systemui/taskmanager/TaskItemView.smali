.class public Lcom/android/systemui/taskmanager/TaskItemView;
.super Landroid/widget/FrameLayout;
.source "TaskItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/taskmanager/TaskItemView$1;,
        Lcom/android/systemui/taskmanager/TaskItemView$2;,
        Lcom/android/systemui/taskmanager/TaskItemView$3;
    }
.end annotation


# instance fields
.field mBtnIconDetails:Landroid/widget/ImageButton;

.field mBtnIconLock:Landroid/widget/ImageButton;

.field mBtnScreenDetails:Landroid/widget/ImageButton;

.field mBtnScreenLock:Landroid/widget/ImageButton;

.field mDetailsBtnClickListener:Landroid/view/View$OnClickListener;

.field mIconMenuContainer:Landroid/view/ViewGroup;

.field mLockBtnClickListener:Landroid/view/View$OnClickListener;

.field mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

.field mScreenshotMenuContainer:Landroid/view/ViewGroup;

.field mTask:Lcom/android/systemui/taskmanager/TaskInfo;

.field mTaskItemLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

.field mTextScreenLock:Landroid/widget/TextView;

.field mTitleContainer:Landroid/view/ViewGroup;

.field private mTitleInIconMode:Landroid/widget/TextView;

.field protected mTitleInScreenshotMode:Landroid/widget/TextView;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/taskmanager/TaskItemView;)Lcom/android/systemui/taskmanager/TaskManagerView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/taskmanager/TaskItemView;)Lcom/android/systemui/taskmanager/TasksView;
    .locals 1

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getTasksView()Lcom/android/systemui/taskmanager/TasksView;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/systemui/taskmanager/TaskItemView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskItemView$1;-><init>(Lcom/android/systemui/taskmanager/TaskItemView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mLockBtnClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/systemui/taskmanager/TaskItemView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskItemView$2;-><init>(Lcom/android/systemui/taskmanager/TaskItemView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskItemLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/android/systemui/taskmanager/TaskItemView$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskItemView$3;-><init>(Lcom/android/systemui/taskmanager/TaskItemView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mDetailsBtnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/systemui/taskmanager/TaskItemView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskItemView$1;-><init>(Lcom/android/systemui/taskmanager/TaskItemView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mLockBtnClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/systemui/taskmanager/TaskItemView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskItemView$2;-><init>(Lcom/android/systemui/taskmanager/TaskItemView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskItemLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/android/systemui/taskmanager/TaskItemView$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskItemView$3;-><init>(Lcom/android/systemui/taskmanager/TaskItemView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mDetailsBtnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static createTaskItemView(Landroid/content/Context;Lcom/android/systemui/taskmanager/TaskInfo;Lcom/android/systemui/taskmanager/TaskManagerView;)Lcom/android/systemui/taskmanager/TaskItemView;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/android/systemui/taskmanager/TaskManagerView;->getItemViewFactory()Lcom/android/systemui/taskmanager/TaskItemViewFactory;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Lcom/android/systemui/taskmanager/TaskItemViewFactory;->getTaskItemView(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/android/systemui/taskmanager/TaskItemView;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/TaskItemView;->setAlpha(F)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/TaskItemView;->setTranslationY(F)V

    invoke-virtual {v1, p2}, Lcom/android/systemui/taskmanager/TaskItemView;->setTaskManagerView(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    iget-object v2, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v2, p2, v1, p1}, Lcom/android/systemui/taskmanager/MultiTaskView;->setup(Lcom/android/systemui/taskmanager/TaskManagerView;Lcom/android/systemui/taskmanager/TaskItemView;Lcom/android/systemui/taskmanager/TaskInfo;)V

    iput-object p1, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-direct {v1}, Lcom/android/systemui/taskmanager/TaskItemView;->initMenu()V

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/TaskItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {}, Lmiui/content/res/IconCustomizer;->getCustomizedIconWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {}, Lmiui/content/res/IconCustomizer;->getCustomizedIconHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v2, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInScreenshotMode:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInIconMode:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInScreenshotMode:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInIconMode:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object v1
.end method

.method private getTasksView()Lcom/android/systemui/taskmanager/TasksView;
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TasksView;

    return-object v0
.end method

.method private initMenu()V
    .locals 3

    const v2, 0x7f0d016b

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    const v1, 0x7f0f01cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mIconMenuContainer:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    const v1, 0x7f0f01d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mScreenshotMenuContainer:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    const v1, 0x7f0f01d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconDetails:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    const v1, 0x7f0f01d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconLock:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    const v1, 0x7f0f01d3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnScreenLock:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    const v1, 0x7f0f01d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTextScreenLock:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    const v1, 0x7f0f01d5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnScreenDetails:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->showIconOrScreenshotMenuContainer()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnScreenDetails:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconDetails:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->refreshLockState()V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->setMenuClickListener()V

    return-void
.end method

.method private setMenuClickListener()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnScreenLock:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mLockBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnScreenDetails:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mDetailsBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconLock:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mLockBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconDetails:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mDetailsBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInIconMode:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public loadScreenshot()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->loadScreenshot()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->releaseScreenshot()V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getItemViewFactory()Lcom/android/systemui/taskmanager/TaskItemViewFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/systemui/taskmanager/TaskItemViewFactory;->recycle(Lcom/android/systemui/taskmanager/TaskItemView;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f01c5

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/TaskItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/MultiTaskView;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    const v0, 0x7f0f01c8

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/TaskItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInIconMode:Landroid/widget/TextView;

    const v0, 0x7f0f01c7

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/TaskItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInScreenshotMode:Landroid/widget/TextView;

    const v0, 0x7f0f01c6

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/TaskItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getY()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 10

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v8, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v8, 0x4

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/taskmanager/TaskItemView;->measureChildWithMargins(Landroid/view/View;IIII)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getMeasuredWidth()I

    move-result v0

    iget v1, v6, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    iget v1, v6, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getPaddingRight()I

    move-result v1

    add-int v7, v0, v1

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mIconMenuContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconDetails:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mIconMenuContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconDetails:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getTasksView()Lcom/android/systemui/taskmanager/TasksView;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/systemui/taskmanager/TasksView;->setTouchedDownChild(Lcom/android/systemui/taskmanager/TaskItemView;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public refreshLockState()V
    .locals 6

    const v1, 0x7f0201c9

    const v2, 0x7f0201c4

    const v3, 0x7f0d016a

    const v4, 0x7f0d0169

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v5, v5, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    invoke-virtual {v0, v5}, Lcom/android/systemui/taskmanager/MultiTaskView;->setIsShowLockImg(Z)V

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnScreenLock:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconLock:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v5, v5, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    if-eqz v5, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTextScreenLock:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    if-eqz v0, :cond_2

    move v0, v3

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnScreenLock:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    if-eqz v0, :cond_3

    move v0, v3

    :goto_3
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mBtnIconLock:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v2, v2, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    if-eqz v2, :cond_4

    :goto_4
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    move v0, v4

    goto :goto_2

    :cond_3
    move v0, v4

    goto :goto_3

    :cond_4
    move v3, v4

    goto :goto_4
.end method

.method public refreshScreenshotMode()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const v4, 0x3f333333    # 0.7f

    const v2, 0x3f333333    # 0.7f

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v3}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotRatio()F

    move-result v0

    cmpg-float v3, v0, v4

    if-gez v3, :cond_0

    sub-float v3, v4, v0

    div-float v1, v3, v4

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInIconMode:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInIconMode:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setAlpha(F)V

    :goto_0
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v0, v3, v0

    cmpg-float v3, v0, v4

    if-gez v3, :cond_1

    sub-float v3, v4, v0

    div-float v1, v3, v4

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInScreenshotMode:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInScreenshotMode:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setAlpha(F)V

    :goto_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->requestLayout()V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v3}, Lcom/android/systemui/taskmanager/MultiTaskView;->requestLayout()V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInIconMode:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInScreenshotMode:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setScaleX(F)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setScaleX(F)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    :cond_0
    return-void
.end method

.method public setTaskManagerView(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    return-void
.end method

.method public setTranslationY(F)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskItemView;->invalidate()V

    return-void
.end method

.method showIconOrScreenshotMenuContainer()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mIconMenuContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mScreenshotMenuContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mIconMenuContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView;->mScreenshotMenuContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method
