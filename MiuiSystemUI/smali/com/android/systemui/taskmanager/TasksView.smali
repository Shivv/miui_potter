.class public Lcom/android/systemui/taskmanager/TasksView;
.super Lcom/android/systemui/taskmanager/ScreenView;
.source "TasksView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;,
        Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;
    }
.end annotation


# static fields
.field private static final FADEIN_DISTANCE:F

.field private static final REMOVE_DISTANCE:I


# instance fields
.field private mAdapter:Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;

.field mChildrenAnimatingData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;",
            ">;"
        }
    .end annotation
.end field

.field private mConfirmVerticalScrollRatio:F

.field private mDstScrollX:F

.field private mFlingDeleteThreshold:I

.field private mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

.field private mScrollLockThreshold:I

.field private mSrcScrollX:F

.field private mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

.field private mTouchSlopY:I

.field private mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

.field mVTracker:Landroid/view/VelocityTracker;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mAdapter:Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/taskmanager/TasksView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/RecentTasksManager;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/TaskManagerView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43160000    # 150.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/systemui/taskmanager/TasksView;->REMOVE_DISTANCE:I

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43960000    # 300.0f

    mul-float/2addr v0, v1

    sput v0, Lcom/android/systemui/taskmanager/TasksView;->FADEIN_DISTANCE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;-><init>(Landroid/content/Context;)V

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mConfirmVerticalScrollRatio:F

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/taskmanager/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mConfirmVerticalScrollRatio:F

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/taskmanager/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mConfirmVerticalScrollRatio:F

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    return-void
.end method

.method private checkNeedScroll(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getTouchState()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/TasksView;->scrolledFarEnoughY(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/taskmanager/TasksView;->setTouchState(Landroid/view/MotionEvent;I)V

    return v2

    :cond_0
    return v1
.end method

.method private createRemoveAnimator(Landroid/view/View;)Landroid/animation/Animator;
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    sget v5, Lcom/android/systemui/taskmanager/TasksView;->REMOVE_DISTANCE:I

    int-to-float v5, v5

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v6

    add-float/2addr v5, v6

    invoke-static {v8, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    sget v5, Lcom/android/systemui/taskmanager/TasksView;->REMOVE_DISTANCE:I

    int-to-float v5, v5

    div-float v5, v3, v5

    const/high16 v6, 0x43160000    # 150.0f

    mul-float/2addr v5, v6

    float-to-int v4, v5

    sget-object v5, Lcom/android/systemui/taskmanager/TasksView;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v10, [F

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v7

    sub-float/2addr v7, v3

    aput v7, v6, v9

    invoke-static {p1, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    sget-object v5, Lcom/android/systemui/taskmanager/TasksView;->ALPHA:Landroid/util/Property;

    new-array v6, v10, [F

    aput v8, v6, v9

    invoke-static {p1, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    int-to-long v6, v4

    invoke-virtual {v1, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    return-object v1
.end method

.method private getScreenIndex(Landroid/view/View;)I
    .locals 3

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    return v2
.end method

.method private getShowingChildHeadTail()[I
    .locals 10

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v6, 0x2

    new-array v4, v6, [I

    aput v9, v4, v8

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScrollX()I

    move-result v7

    add-int v2, v6, v7

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v0, :cond_2

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v6

    if-le v6, v1, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v6

    if-ge v6, v2, :cond_1

    aget v6, v4, v8

    if-ne v6, v9, :cond_0

    aput v5, v4, v8

    :cond_0
    const/4 v6, 0x1

    aput v5, v4, v6

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    return-object v4
.end method

.method private loadAndDrawScreenshots(Landroid/graphics/Canvas;)V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TasksView;->getScrollX()I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TasksView;->getWidth()I

    move-result v15

    add-int v12, v11, v15

    const/4 v3, -0x1

    const/4 v9, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_3

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v15

    invoke-virtual {v7}, Landroid/view/View;->getPaddingRight()I

    move-result v16

    sub-int v15, v15, v16

    if-le v15, v11, :cond_2

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v15

    invoke-virtual {v7}, Landroid/view/View;->getPaddingLeft()I

    move-result v16

    add-int v15, v15, v16

    if-ge v15, v12, :cond_3

    const/4 v15, -0x1

    if-ne v3, v15, :cond_1

    move v3, v6

    :cond_1
    move v9, v6

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    const/4 v15, -0x1

    if-ne v3, v15, :cond_4

    return-void

    :cond_4
    const/4 v10, 0x1

    add-int/lit8 v14, v3, -0x1

    add-int/lit8 v13, v9, 0x1

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v14, :cond_5

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v15, v15, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v15}, Lcom/android/systemui/taskmanager/MultiTaskView;->releaseScreenshot()V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_5
    add-int/lit8 v6, v13, 0x1

    :goto_2
    if-ge v6, v2, :cond_6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v15, v15, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v15}, Lcom/android/systemui/taskmanager/MultiTaskView;->releaseScreenshot()V

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TasksView;->getDrawingTime()J

    move-result-wide v4

    move v6, v3

    :goto_3
    if-gt v6, v9, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v8}, Lcom/android/systemui/taskmanager/TaskItemView;->loadScreenshot()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v8, v4, v5}, Lcom/android/systemui/taskmanager/TasksView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v15}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v15}, Ljava/util/HashMap;->size()I

    move-result v15

    if-nez v15, :cond_9

    if-ltz v14, :cond_8

    move v6, v14

    :goto_4
    if-ge v6, v3, :cond_8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/taskmanager/TasksView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iget-object v0, v15, Lcom/android/systemui/taskmanager/RecentTasksManager;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v15, v15, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->startLoad(Lcom/android/systemui/taskmanager/TaskInfo;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v15

    if-ge v13, v15, :cond_9

    add-int/lit8 v6, v9, 0x1

    :goto_5
    if-gt v6, v13, :cond_9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/taskmanager/TasksView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iget-object v0, v15, Lcom/android/systemui/taskmanager/RecentTasksManager;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v15, v15, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->startLoad(Lcom/android/systemui/taskmanager/TaskInfo;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_9
    return-void
.end method

.method public static queryAppConfigure(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    const/4 v2, 0x0

    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v9

    const-string/jumbo v3, "pkgName = ? AND userId = ?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v4, v1

    const-string/jumbo v6, ""

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "content://com.miui.powerkeeper.configure"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string/jumbo v5, "userTable"

    invoke-static {v1, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "bgControl"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v6
.end method

.method private reloadAdapter()V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->removeAllScreens()V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mAdapter:Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;

    if-nez v3, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mAdapter:Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;

    invoke-virtual {v3}, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->getCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mAdapter:Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;

    invoke-virtual {v3, v2, v5, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const/4 v3, -0x2

    const/4 v4, -0x1

    invoke-virtual {p0, v0, v3, v4}, Lcom/android/systemui/taskmanager/TasksView;->addView(Landroid/view/View;II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private removeTask()V
    .locals 5

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-direct {p0, v3}, Lcom/android/systemui/taskmanager/TasksView;->getScreenIndex(Landroid/view/View;)I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->createRemoveAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    new-instance v3, Lcom/android/systemui/taskmanager/TasksView$1;

    invoke-direct {v3, p0, v2, v1, v0}, Lcom/android/systemui/taskmanager/TasksView$1;-><init>(Lcom/android/systemui/taskmanager/TasksView;Lcom/android/systemui/taskmanager/TaskItemView;ILandroid/animation/Animator;)V

    invoke-virtual {v0, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    iget-object v3, v3, Lcom/android/systemui/taskmanager/TaskManagerView;->mAnimatorsNeedForceEnd:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v3}, Lcom/android/systemui/taskmanager/TaskManagerView;->freeze()V

    const-string/jumbo v3, "systemui_taskmanager_kill_oneapp"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method private resetPaddingTop(III)V
    .locals 8

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v0, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, p2, v2, v6}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-virtual {v2}, Landroid/util/TypedValue;->getFloat()F

    move-result v4

    sub-int v5, v0, p1

    int-to-float v5, v5

    mul-float/2addr v5, v4

    float-to-int v1, v5

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    sub-int v5, v0, v1

    sub-int v3, v5, p1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getPaddingRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getPaddingBottom()I

    move-result v7

    invoke-virtual {p0, v5, v3, v6, v7}, Lcom/android/systemui/taskmanager/TasksView;->setPaddingRelative(IIII)V

    return-void
.end method

.method private resetScreenshotMode()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v1

    if-ne v1, v5, :cond_2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getCurrentScreenIndex()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getCurrentScreenIndex()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->setCurrentScreenInner(I)V

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotHeight()I

    move-result v2

    const v3, 0x7f0b0026

    const v4, 0x7f0b006a

    invoke-direct {p0, v2, v3, v4}, Lcom/android/systemui/taskmanager/TasksView;->resetPaddingTop(III)V

    invoke-virtual {p0, v6}, Lcom/android/systemui/taskmanager/TasksView;->setScrollWholeScreen(Z)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/TasksView;->setMultiScreenScroll(Z)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/TasksView;->setCanClickWhenScrolling(Z)V

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->setScreenAlignment(I)V

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotHeight()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3dcccccd    # 0.1f

    mul-float/2addr v2, v3

    neg-float v2, v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mFlingDeleteThreshold:I

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotHeight()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3e4ccccd    # 0.2f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mScrollLockThreshold:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getCurrentScreenIndex()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_3

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getCurrentScreenIndex()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->setCurrentScreenInner(I)V

    :cond_3
    invoke-static {}, Lmiui/content/res/IconCustomizer;->getCustomizedIconHeight()I

    move-result v2

    const v3, 0x7f0b0027

    const v4, 0x7f0b006b

    invoke-direct {p0, v2, v3, v4}, Lcom/android/systemui/taskmanager/TasksView;->resetPaddingTop(III)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/TasksView;->setScrollWholeScreen(Z)V

    invoke-virtual {p0, v6}, Lcom/android/systemui/taskmanager/TasksView;->setMultiScreenScroll(Z)V

    invoke-virtual {p0, v6}, Lcom/android/systemui/taskmanager/TasksView;->setCanClickWhenScrolling(Z)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/TasksView;->setScreenAlignment(I)V

    invoke-static {}, Lmiui/content/res/IconCustomizer;->getCustomizedIconHeight()I

    move-result v0

    neg-int v2, v0

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mFlingDeleteThreshold:I

    iput v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mScrollLockThreshold:I

    goto :goto_0
.end method

.method private scrolledFarEnoughY(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mLastMotionX:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    iget v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mLastMotionY:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    return v2

    :cond_0
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget v4, p0, Lcom/android/systemui/taskmanager/TasksView;->mLastMotionX:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget v4, p0, Lcom/android/systemui/taskmanager/TasksView;->mLastMotionY:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mConfirmVerticalScrollRatio:F

    mul-float/2addr v3, v0

    cmpl-float v3, v1, v3

    if-lez v3, :cond_1

    iget v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchSlopY:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    mul-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v3, v1, v3

    if-lez v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public static updateAppConfigure(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v3, "userId"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "pkgName"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "bgControl"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "content://com.miui.powerkeeper.configure"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string/jumbo v5, "userTable"

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string/jumbo v5, "userTableupdate"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected allChildrenFlingToRest(Lcom/android/systemui/taskmanager/TaskItemView;)V
    .locals 3

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TasksView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskItemView;

    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_1

    :cond_0
    iget-object v2, v0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/MultiTaskView;->flingToRest()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public computeScroll()V
    .locals 4

    invoke-super {p0}, Lcom/android/systemui/taskmanager/ScreenView;->computeScroll()V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->getModeAnimatingOriginalRatio()F

    move-result v0

    iget v1, p0, Lcom/android/systemui/taskmanager/TasksView;->mSrcScrollX:F

    iget v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mDstScrollX:F

    iget v3, p0, Lcom/android/systemui/taskmanager/TasksView;->mSrcScrollX:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TasksView;->setScrollX(I)V

    :cond_0
    return-void
.end method

.method public destroyAllScreenshot()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/MultiTaskView;->releaseScreenshot()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/RecentTasksManager;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->destroyAll()V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-super {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->dispatchDraw(Landroid/graphics/Canvas;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/TasksView;->loadAndDrawScreenshots(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public endScreenshotModeAnimating()V
    .locals 3

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TasksView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskItemView;->showIconOrScreenshotMenuContainer()V

    iget-object v2, v0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/MultiTaskView;->forceFinished()V

    iget-object v2, v0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/MultiTaskView;->initChildren()V

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskItemView;->refreshLockState()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->allChildrenFlingToRest(Lcom/android/systemui/taskmanager/TaskItemView;)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->requestLayout()V

    return-void
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method mTouchEvent(Landroid/view/MotionEvent;F)Z
    .locals 12

    const/4 v7, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    return v5

    :pswitch_0
    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/TasksView;->checkNeedScroll(Landroid/view/MotionEvent;)Z

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getTouchState()I

    move-result v5

    const/4 v6, 0x6

    if-ne v5, v6, :cond_2

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    if-eqz v5, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v5, p2, v9

    if-gtz v5, :cond_1

    sget v5, Lcom/android/systemui/taskmanager/TasksView;->REMOVE_DISTANCE:I

    int-to-float v5, v5

    div-float v5, p2, v5

    add-float/2addr v5, v11

    invoke-static {v5, v9}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v5, p2}, Lcom/android/systemui/taskmanager/TaskItemView;->setTranslationY(F)V

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v5, v0}, Lcom/android/systemui/taskmanager/TaskItemView;->setAlpha(F)V

    :goto_1
    return v10

    :cond_1
    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v5, v9}, Lcom/android/systemui/taskmanager/TaskItemView;->setTranslationY(F)V

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v5, v11}, Lcom/android/systemui/taskmanager/TaskItemView;->setAlpha(F)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v7}, Lcom/android/systemui/taskmanager/TasksView;->allChildrenFlingToRest(Lcom/android/systemui/taskmanager/TaskItemView;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getTouchState()I

    move-result v5

    const/4 v6, 0x6

    if-eq v5, v6, :cond_3

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getTouchState()I

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    if-eqz v5, :cond_0

    :cond_3
    const/4 v3, 0x0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    const/16 v6, 0x3e8

    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v4

    neg-float v5, p2

    sget v6, Lcom/android/systemui/taskmanager/TasksView;->REMOVE_DISTANCE:I

    int-to-float v6, v6

    const v7, 0x3f19999a    # 0.6f

    mul-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-gez v5, :cond_4

    iget v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mFlingDeleteThreshold:I

    int-to-float v5, v5

    cmpg-float v5, p2, v5

    if-gez v5, :cond_5

    const/high16 v5, -0x3c060000    # -500.0f

    cmpg-float v5, v4, v5

    if-gez v5, :cond_5

    :cond_4
    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TasksView;->removeTask()V

    :cond_5
    if-nez v3, :cond_6

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v5}, Lcom/android/systemui/taskmanager/TaskItemView;->invalidate()V

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    sget-object v6, Lcom/android/systemui/taskmanager/TasksView;->TRANSLATION_Y:Landroid/util/Property;

    new-array v7, v10, [F

    aput v9, v7, v8

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v6, 0x64

    invoke-virtual {v2, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    sget-object v6, Lcom/android/systemui/taskmanager/TasksView;->ALPHA:Landroid/util/Property;

    new-array v7, v10, [F

    aput v11, v7, v8

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v6, 0x64

    invoke-virtual {v1, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    :cond_6
    invoke-virtual {p0, p1, v8}, Lcom/android/systemui/taskmanager/TasksView;->setTouchState(Landroid/view/MotionEvent;I)V

    const/4 v5, 0x3

    invoke-virtual {p0, v8, v5}, Lcom/android/systemui/taskmanager/TasksView;->snapByVelocity(II)V

    return v10

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/systemui/taskmanager/ScreenView;->onFinishInflate()V

    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TasksView;->setScreenTransitionType(I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TasksView;->setOvershootTension(F)V

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->setScreenLayoutMode(I)V

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->setClipToPadding(Z)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchSlopY:I

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/taskmanager/TasksView;->findChild(FF)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskItemView;

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/TasksView;->checkNeedScroll(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/TasksView;->allChildrenFlingToRest(Lcom/android/systemui/taskmanager/TaskItemView;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->isReset()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iget v1, p0, Lcom/android/systemui/taskmanager/TasksView;->mLastMotionY:F

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    :cond_3
    return v2

    :cond_4
    const/4 v0, 0x1

    return v0

    :cond_5
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    iget-object v9, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    if-nez v9, :cond_0

    invoke-super/range {p0 .. p5}, Lcom/android/systemui/taskmanager/ScreenView;->onLayout(ZIIII)V

    :goto_0
    return-void

    :cond_0
    iget-object v9, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v9}, Lcom/android/systemui/taskmanager/TaskManagerView;->getModeAnimatingOriginalRatio()F

    move-result v5

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getChildCount()I

    move-result v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v0, :cond_1

    invoke-virtual {p0, v4}, Lcom/android/systemui/taskmanager/TasksView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v9, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v9, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;

    iget v9, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcX:I

    int-to-float v9, v9

    iget v10, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstX:I

    iget v11, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcX:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v5

    add-float/2addr v9, v10

    float-to-int v7, v9

    iget v9, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcY:I

    int-to-float v9, v9

    iget v10, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstY:I

    iget v11, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcY:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v5

    add-float/2addr v9, v10

    float-to-int v8, v9

    iget v9, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcW:I

    int-to-float v9, v9

    iget v10, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstW:I

    iget v11, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcW:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v5

    add-float/2addr v9, v10

    float-to-int v6, v9

    iget v9, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcH:I

    int-to-float v9, v9

    iget v10, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstH:I

    iget v11, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcH:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v5

    add-float/2addr v9, v10

    float-to-int v3, v9

    add-int v9, v7, v6

    add-int v10, v8, v3

    invoke-virtual {v1, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->invalidate()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11

    const/high16 v10, 0x40000000    # 2.0f

    iget-object v7, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    if-nez v7, :cond_1

    invoke-super {p0, p1, p2}, Lcom/android/systemui/taskmanager/ScreenView;->onMeasure(II)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getMeasuredHeight()I

    move-result v8

    invoke-virtual {p0, v7, v8}, Lcom/android/systemui/taskmanager/TasksView;->setMeasuredDimension(II)V

    iget-object v7, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v7}, Lcom/android/systemui/taskmanager/TaskManagerView;->getModeAnimatingOriginalRatio()F

    move-result v5

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getChildCount()I

    move-result v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/android/systemui/taskmanager/TasksView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v7, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;

    iget v7, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcW:I

    int-to-float v7, v7

    iget v8, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstW:I

    iget v9, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcW:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    float-to-int v6, v7

    iget v7, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcH:I

    int-to-float v7, v7

    iget v8, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstH:I

    iget v9, v2, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcH:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    float-to-int v3, v7

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/view/View;->measure(II)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected onPinchIn(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->gotoIconMode()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onPinchOut(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->gotoScreenshotMode()V

    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mLastMotionY:F

    sub-float v0, v1, v2

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/taskmanager/TasksView;->mTouchEvent(Landroid/view/MotionEvent;F)Z

    move-result v1

    return v1
.end method

.method public playClearAllAnimator(Ljava/lang/Runnable;)V
    .locals 10

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v7

    if-gtz v7, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TasksView;->getShowingChildHeadTail()[I

    move-result-object v1

    aget v0, v1, v8

    const/4 v7, 0x1

    aget v5, v1, v7

    const/4 v7, -0x1

    if-ne v0, v7, :cond_1

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void

    :cond_1
    const/4 v4, 0x0

    const/4 v3, 0x0

    move v2, v0

    :goto_0
    if-gt v2, v5, :cond_3

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v7, v6, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v7, v7, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    if-nez v7, :cond_2

    iget-object v7, v6, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v7, v7, Lcom/android/systemui/taskmanager/TaskInfo;->mTopRunning:Z

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_2

    invoke-direct {p0, v6}, Lcom/android/systemui/taskmanager/TasksView;->createRemoveAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v3

    mul-int/lit16 v7, v4, 0x96

    int-to-float v7, v7

    const v8, 0x3f2b851f    # 0.67f

    mul-float/2addr v7, v8

    float-to-int v7, v7

    int-to-long v8, v7

    invoke-virtual {v3, v8, v9}, Landroid/animation/Animator;->setStartDelay(J)V

    invoke-virtual {v3}, Landroid/animation/Animator;->start()V

    add-int/lit8 v4, v4, 0x1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    if-nez v3, :cond_4

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_1
    return-void

    :cond_4
    new-instance v7, Lcom/android/systemui/taskmanager/TasksView$3;

    invoke-direct {v7, p0, p1}, Lcom/android/systemui/taskmanager/TasksView$3;-><init>(Lcom/android/systemui/taskmanager/TasksView;Ljava/lang/Runnable;)V

    invoke-virtual {v3, v7}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_1
.end method

.method public refreshScreenshotModeAnimating()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/TaskItemView;->refreshScreenshotMode()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->requestLayout()V

    return-void
.end method

.method public setAdapter(Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TasksView;->mAdapter:Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TasksView;->reloadAdapter()V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TasksView;->resetScreenshotMode()V

    return-void
.end method

.method public setLoader(Lcom/android/systemui/taskmanager/RecentTasksManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TasksView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    return-void
.end method

.method public setTaskManagerView(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    return-void
.end method

.method public setTouchedDownChild(Lcom/android/systemui/taskmanager/TaskItemView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TasksView;->mTouchedDownView:Lcom/android/systemui/taskmanager/TaskItemView;

    return-void
.end method

.method public show()V
    .locals 10

    const/4 v9, 0x0

    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v4, v9}, Lcom/android/systemui/taskmanager/TaskItemView;->setAlpha(F)V

    sget v5, Lcom/android/systemui/taskmanager/TasksView;->FADEIN_DISTANCE:F

    invoke-virtual {v4, v5}, Lcom/android/systemui/taskmanager/TaskItemView;->setTranslationY(F)V

    invoke-virtual {v4}, Lcom/android/systemui/taskmanager/TaskItemView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    sget-object v6, Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;->Instance:Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    mul-int/lit8 v6, v2, 0x32

    add-int/lit16 v6, v6, 0x96

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Lcom/android/systemui/taskmanager/TasksView$4;

    invoke-direct {v6, p0, v4}, Lcom/android/systemui/taskmanager/TasksView$4;-><init>(Lcom/android/systemui/taskmanager/TasksView;Lcom/android/systemui/taskmanager/TaskItemView;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewPropertyAnimator;->start()V

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    mul-int/lit8 v5, v2, 0x32

    add-int/lit16 v5, v5, 0x96

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public startScreenshotModeAnimating()V
    .locals 12

    const/high16 v10, 0x40000000    # 2.0f

    iget-object v8, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScrollX()I

    move-result v8

    int-to-float v8, v8

    iput v8, p0, Lcom/android/systemui/taskmanager/TasksView;->mSrcScrollX:F

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getChildCount()I

    move-result v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v0, :cond_0

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/TasksView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    new-instance v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;

    invoke-direct {v4}, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;-><init>()V

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v8

    iput v8, v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcX:I

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v8

    iput v8, v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcY:I

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v8

    iput v8, v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcW:I

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v8

    iput v8, v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->srcH:I

    invoke-virtual {v3, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    iget-object v8, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v8}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_1

    const/4 v6, 0x1

    :goto_1
    iget-object v8, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v8, v6}, Lcom/android/systemui/taskmanager/TaskManagerView;->setScreenshotMode(I)V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TasksView;->resetScreenshotMode()V

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v0, :cond_2

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/TasksView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/TaskItemView;->refreshLockState()V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_1
    const/4 v6, 0x3

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/android/systemui/taskmanager/TasksView;->allChildrenFlingToRest(Lcom/android/systemui/taskmanager/TaskItemView;)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getWidth()I

    move-result v8

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getHeight()I

    move-result v9

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/android/systemui/taskmanager/TasksView;->measure(II)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getLeft()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getTop()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getRight()I

    move-result v10

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getBottom()I

    move-result v11

    invoke-virtual {p0, v8, v9, v10, v11}, Lcom/android/systemui/taskmanager/TasksView;->layout(IIII)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->computeScroll()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TasksView;->getScrollX()I

    move-result v8

    int-to-float v8, v8

    iput v8, p0, Lcom/android/systemui/taskmanager/TasksView;->mDstScrollX:F

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v0, :cond_3

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/TasksView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v8

    iput v8, v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstX:I

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v8

    iput v8, v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstY:I

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v8

    iput v8, v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstW:I

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v8

    iput v8, v4, Lcom/android/systemui/taskmanager/TasksView$ChildAnimatingData;->dstH:I

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_3
    iget-object v8, p0, Lcom/android/systemui/taskmanager/TasksView;->mChildrenAnimatingData:Ljava/util/HashMap;

    invoke-virtual {v8, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iget-object v8, p0, Lcom/android/systemui/taskmanager/TasksView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v8, v7}, Lcom/android/systemui/taskmanager/TaskManagerView;->setScreenshotMode(I)V

    return-void
.end method

.method toggleTaskItemLock(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 4

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getCompoundedTasks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v2, v0, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    xor-int/lit8 v2, v2, 0x1

    iput-boolean v2, v0, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/RecentTasksManager;->mBgHandler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/RecentTasksManager;->mBgHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/systemui/taskmanager/TasksView$2;

    invoke-direct {v3, p0, p1}, Lcom/android/systemui/taskmanager/TasksView$2;-><init>(Lcom/android/systemui/taskmanager/TasksView;Lcom/android/systemui/taskmanager/TaskInfo;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method
