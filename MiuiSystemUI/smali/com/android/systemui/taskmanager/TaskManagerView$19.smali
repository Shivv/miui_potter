.class Lcom/android/systemui/taskmanager/TaskManagerView$19;
.super Landroid/animation/AnimatorListenerAdapter;
.source "TaskManagerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/TaskManagerView;->startScreenshotFadeAnimator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/TaskManagerView;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView$19;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$19;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->-get5(Lcom/android/systemui/taskmanager/TaskManagerView;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$19;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->-set3(Lcom/android/systemui/taskmanager/TaskManagerView;I)I

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$19;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->-get7(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201c7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$19;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TasksView;->endScreenshotModeAnimating()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$19;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->-get5(Lcom/android/systemui/taskmanager/TaskManagerView;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$19;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->-set3(Lcom/android/systemui/taskmanager/TaskManagerView;I)I

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$19;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->-get7(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201c3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
