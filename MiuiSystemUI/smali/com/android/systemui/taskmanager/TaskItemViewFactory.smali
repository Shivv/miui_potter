.class public Lcom/android/systemui/taskmanager/TaskItemViewFactory;
.super Ljava/lang/Object;
.source "TaskItemViewFactory.java"


# instance fields
.field private mCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/CharSequence;",
            "Lcom/android/systemui/taskmanager/TaskItemView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemViewFactory;->mCache:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public getTaskItemView(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/android/systemui/taskmanager/TaskItemView;
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskItemViewFactory;->mCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03006e

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/taskmanager/TaskItemView;

    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskItemViewFactory;->mCache:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/TaskItemView;

    if-eqz v1, :cond_1

    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskItemViewFactory;->mCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/taskmanager/TaskItemView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    throw v2
.end method

.method public recycle(Lcom/android/systemui/taskmanager/TaskItemView;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemViewFactory;->mCache:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskItemView;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
