.class public Lcom/android/systemui/taskmanager/MultiTaskView;
.super Landroid/widget/FrameLayout;
.source "MultiTaskView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;
    }
.end annotation


# static fields
.field private static final OVER_SCROLL_Y:I


# instance fields
.field mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

.field mFlingToResting:Z

.field mGestureDetector:Landroid/view/GestureDetector;

.field private mLastMotionY:F

.field mLastP:F

.field mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

.field mPreviewIconViewList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/taskmanager/PreviewIconView;",
            ">;"
        }
    .end annotation
.end field

.field mScroll:F

.field mScroller:Landroid/widget/OverScroller;

.field private mTask:Lcom/android/systemui/taskmanager/TaskInfo;

.field mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

.field private mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

.field mTaskMenuView:Landroid/view/View;

.field mTaskViewTransforms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/taskmanager/TaskViewTransform;",
            ">;"
        }
    .end annotation
.end field

.field private mTouchSlopY:I

.field mVTracker:Landroid/view/VelocityTracker;


# direct methods
.method static synthetic -get0()I
    .locals 1

    sget v0, Lcom/android/systemui/taskmanager/MultiTaskView;->OVER_SCROLL_Y:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/systemui/taskmanager/MultiTaskView;)Lcom/android/systemui/taskmanager/TaskManagerView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/taskmanager/MultiTaskView;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->isIconMode()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/systemui/taskmanager/MultiTaskView;)F
    .locals 1

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getBottomScroll()F

    move-result v0

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41a00000    # 20.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/systemui/taskmanager/MultiTaskView;->OVER_SCROLL_Y:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/taskmanager/MultiTaskView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/taskmanager/MultiTaskView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mFlingToResting:Z

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mVTracker:Landroid/view/VelocityTracker;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskViewTransforms:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030070

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    new-instance v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    invoke-direct {v0}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->init()V

    return-void
.end method

.method private doScrolling()V
    .locals 6

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v1

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getBottomScroll()F

    move-result v2

    sget v3, Lcom/android/systemui/taskmanager/MultiTaskView;->OVER_SCROLL_Y:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    int-to-float v3, v1

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    iget-object v4, v4, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v4

    sget v5, Lcom/android/systemui/taskmanager/MultiTaskView;->OVER_SCROLL_Y:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->invalidate()V

    :cond_1
    return-void
.end method

.method private getBottomScroll()F
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    iget v0, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMinScrollP:F

    return v0
.end method

.method private getImageToScreenBottom(III)I
    .locals 6

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, p2, v2, v5}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-virtual {v2}, Landroid/util/TypedValue;->getFloat()F

    move-result v3

    sub-int v4, v0, p1

    int-to-float v4, v4

    mul-float/2addr v4, v3

    float-to-int v1, v4

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int v4, v1, p1

    return v4
.end method

.method private init()V
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Landroid/widget/OverScroller;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroller:Landroid/widget/OverScroller;

    new-instance v1, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/android/systemui/taskmanager/MultiTaskView$1;

    invoke-direct {v3, p0}, Lcom/android/systemui/taskmanager/MultiTaskView$1;-><init>(Lcom/android/systemui/taskmanager/MultiTaskView;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mGestureDetector:Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, v4}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mGestureDetector:Landroid/view/GestureDetector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTouchSlopY:I

    invoke-virtual {p0, v4}, Lcom/android/systemui/taskmanager/MultiTaskView;->setClipChildren(Z)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/MultiTaskView;->setImportantForAccessibility(I)V

    return-void
.end method

.method private isIconMode()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private layoutChildren()V
    .locals 11

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v4}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    :goto_0
    if-ltz v2, :cond_6

    iget-object v8, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->isIconMode()Z

    move-result v4

    if-eqz v4, :cond_3

    move v6, v7

    :goto_1
    iget v9, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskViewTransforms:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/systemui/taskmanager/TaskViewTransform;

    iget-object v5, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v5}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v2, v5, :cond_4

    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v8, v6, v9, v4, v5}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->getStackTransform(IFLcom/android/systemui/taskmanager/TaskViewTransform;Lcom/android/systemui/taskmanager/TaskViewTransform;)Lcom/android/systemui/taskmanager/TaskViewTransform;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-boolean v4, v3, Lcom/android/systemui/taskmanager/TaskViewTransform;->visible:Z

    if-eqz v4, :cond_5

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget v4, v3, Lcom/android/systemui/taskmanager/TaskViewTransform;->translationY:I

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getMultiTaskCount()I

    move-result v4

    const/4 v5, 0x1

    if-gt v4, v5, :cond_0

    iget v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v5

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setPivotX(F)V

    iget v4, v3, Lcom/android/systemui/taskmanager/TaskViewTransform;->scale:F

    invoke-virtual {v0, v4}, Landroid/view/View;->setScaleX(F)V

    iget v4, v3, Lcom/android/systemui/taskmanager/TaskViewTransform;->scale:F

    invoke-virtual {v0, v4}, Landroid/view/View;->setScaleY(F)V

    :cond_1
    :goto_3
    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v4}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v2, v4, :cond_2

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    add-int/lit8 v5, v2, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/systemui/taskmanager/PreviewIconView;

    iget-boolean v5, v3, Lcom/android/systemui/taskmanager/TaskViewTransform;->visible:Z

    invoke-virtual {v4, v5}, Lcom/android/systemui/taskmanager/PreviewIconView;->setIsShowShadow(Z)V

    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_3
    move v6, v2

    goto :goto_1

    :cond_4
    iget-object v5, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskViewTransforms:Ljava/util/ArrayList;

    add-int/lit8 v10, v2, 0x1

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/systemui/taskmanager/TaskViewTransform;

    goto :goto_2

    :cond_5
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_6
    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v5

    iget v6, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    sub-float/2addr v5, v6

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v6

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getBottomScroll()F

    move-result v7

    sub-float/2addr v6, v7

    div-float/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    iget-object v5, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v5}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v4, v4, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/view/View;->getScaleY()F

    move-result v6

    mul-float/2addr v5, v6

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setTranslationY(F)V

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v4}, Lcom/android/systemui/taskmanager/TaskManagerView;->refreshMemoryAndClearContainerAlpha()V

    return-void
.end method

.method private scrollAnimator(F)V
    .locals 4

    const/4 v1, 0x2

    new-array v1, v1, [F

    iget v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    const/4 v3, 0x0

    aput v2, v1, v3

    const/4 v2, 0x1

    aput p1, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v1}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lcom/android/systemui/taskmanager/MultiTaskView$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/MultiTaskView$2;-><init>(Lcom/android/systemui/taskmanager/MultiTaskView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->computeScroll()V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->layoutChildren()V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->doScrolling()V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TaskItemView;->getTranslationY()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method flingToRest()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->isReset()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mFlingToResting:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mFlingToResting:Z

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->scrollAnimator(F)V

    :cond_0
    return-void
.end method

.method forceFinished()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroller:Landroid/widget/OverScroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->forceFinished(Z)V

    :cond_0
    return-void
.end method

.method public getMemoryAndClearContainerAlpha()F
    .locals 4

    const v3, 0x3dcccccd    # 0.1f

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v1

    iget v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    sub-float/2addr v1, v2

    sub-float v1, v3, v1

    div-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public getMultiTaskCount()I
    .locals 1

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->isIconMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getTopScroll()F
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    iget v0, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMaxScrollP:F

    return v0
.end method

.method public initChildren()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->removeAllViews()V

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskMenuView:Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v3}, Lcom/android/systemui/taskmanager/MultiTaskView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, p0}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->isIconMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/systemui/taskmanager/MultiTaskView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/TaskManagerView;->mImgPreviewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/TaskItemView;->mTaskItemLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->initScrollAlgorithm()V

    return-void
.end method

.method public initScrollAlgorithm()V
    .locals 8

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->isIconMode()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {}, Lmiui/content/res/IconCustomizer;->getCustomizedIconHeight()I

    move-result v4

    const v5, 0x7f0b0027

    const v6, 0x7f0b006b

    invoke-direct {p0, v4, v5, v6}, Lcom/android/systemui/taskmanager/MultiTaskView;->getImageToScreenBottom(III)I

    move-result v3

    invoke-static {}, Lmiui/content/res/IconCustomizer;->getCustomizedIconHeight()I

    move-result v2

    :goto_0
    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v7, v7, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v4, v2, v3, v5}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->computeRects(IILandroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getMultiTaskCount()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->computeMinMaxScroll(I)V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->isIconMode()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    iget v5, v4, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMinScrollP:F

    const v6, 0x3dcccccd    # 0.1f

    add-float/2addr v5, v6

    iput v5, v4, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMinScrollP:F

    :cond_0
    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    iget v4, v4, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mInitialScrollP:F

    iput v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v4}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotHeight()I

    move-result v4

    const v5, 0x7f0b0026

    const v6, 0x7f0b006a

    invoke-direct {p0, v4, v5, v6}, Lcom/android/systemui/taskmanager/MultiTaskView;->getImageToScreenBottom(III)I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v4}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotWidth()I

    move-result v2

    goto :goto_0
.end method

.method public isReset()Z
    .locals 4

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v0

    iget v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadScreenshot()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/PreviewIconView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/PreviewIconView;->loadScreenshot()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    xor-int/lit8 v2, v1, 0x1

    return v2

    :pswitch_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/MultiTaskView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iput v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mLastMotionY:F

    const/4 v2, 0x0

    return v2

    :pswitch_1
    iget v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mLastMotionY:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTouchSlopY:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    return v4

    :pswitch_2
    iget v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mLastMotionY:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTouchSlopY:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    return v4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x459c4000    # 5000.0f

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    return v6

    :pswitch_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->screenYToCurveProgress(I)F

    move-result v2

    iput v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mLastP:F

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->forceFinished()V

    iput-boolean v5, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mFlingToResting:Z

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/MultiTaskView;->startScrolling(I)V

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mVTracker:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getMultiTaskCount()I

    move-result v2

    if-ne v2, v6, :cond_1

    const/4 v0, 0x0

    const v2, -0x3ac48000    # -3000.0f

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_3

    cmpg-float v2, v1, v4

    if-gtz v2, :cond_3

    iget v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v3

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getBottomScroll()F

    move-result v4

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getBottomScroll()F

    move-result v0

    :goto_1
    invoke-direct {p0, v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->scrollAnimator(F)V

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mVTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v0

    goto :goto_1

    :cond_3
    cmpl-float v2, v1, v4

    if-lez v2, :cond_4

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getBottomScroll()F

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public releaseScreenshot()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/PreviewIconView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/PreviewIconView;->releaseScreenshot()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setAdapter(Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->initChildren()V

    return-void
.end method

.method public setIsShowLockImg(Z)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/PreviewIconView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/PreviewIconView;->setIsShowLockImg(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mAdapter:Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/PreviewIconView;

    invoke-virtual {v1, p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->setIsShowLockImg(Z)V

    return-void
.end method

.method public setup(Lcom/android/systemui/taskmanager/TaskManagerView;Lcom/android/systemui/taskmanager/TaskItemView;Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 2

    iput-object p1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    iput-object p2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    iput-object p3, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    new-instance v0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TaskInfo;->getCompoundedTasks()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;-><init>(Lcom/android/systemui/taskmanager/MultiTaskView;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->setAdapter(Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;)V

    return-void
.end method

.method startScrolling(I)V
    .locals 11

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroller:Landroid/widget/OverScroller;

    iget v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    iget-object v3, v3, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getBottomScroll()F

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    iget-object v4, v4, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v7, v3

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    iget-object v4, v4, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v8, v3

    sget v10, Lcom/android/systemui/taskmanager/MultiTaskView;->OVER_SCROLL_Y:I

    move v3, v1

    move v4, p1

    move v5, v1

    move v6, v1

    move v9, v1

    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView;->invalidate()V

    return-void
.end method
