.class Lcom/android/systemui/taskmanager/TasksView$2;
.super Ljava/lang/Object;
.source "TasksView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/TasksView;->toggleTaskItemLock(Lcom/android/systemui/taskmanager/TaskInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/TasksView;

.field final synthetic val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/TasksView;Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TasksView$2;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    iput-object p2, p0, Lcom/android/systemui/taskmanager/TasksView$2;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView$2;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/TasksView;->-get2(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/RecentTasksManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView$2;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/RecentTasksManager;->toggleLockTask(Lcom/android/systemui/taskmanager/TaskInfo;)V

    const-string/jumbo v0, "noRestrict"

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView$2;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v1, v1, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView$2;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TasksView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView$2;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v3, "noRestrict"

    invoke-static {v1, v2, v3}, Lcom/android/systemui/taskmanager/TasksView;->updateAppConfigure(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "systemui_taskmanager_lock"

    invoke-static {v1, v4}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView$2;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    new-instance v2, Lcom/android/systemui/taskmanager/TasksView$2$1;

    invoke-direct {v2, p0}, Lcom/android/systemui/taskmanager/TasksView$2$1;-><init>(Lcom/android/systemui/taskmanager/TasksView$2;)V

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/TasksView;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "noRestrict"

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView$2;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/TasksView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TasksView$2;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v3, v3, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/systemui/taskmanager/TasksView;->queryAppConfigure(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView$2;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TasksView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TasksView$2;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v3, "miuiAuto"

    invoke-static {v1, v2, v3}, Lcom/android/systemui/taskmanager/TasksView;->updateAppConfigure(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string/jumbo v1, "systemui_taskmanager_unlock"

    invoke-static {v1, v4}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TasksView$2;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    new-instance v2, Lcom/android/systemui/taskmanager/TasksView$2$2;

    invoke-direct {v2, p0}, Lcom/android/systemui/taskmanager/TasksView$2$2;-><init>(Lcom/android/systemui/taskmanager/TasksView$2;)V

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/TasksView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
