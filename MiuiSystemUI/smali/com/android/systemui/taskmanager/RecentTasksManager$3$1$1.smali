.class Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "RecentTasksManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/RecentTasksManager$3$1;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/systemui/taskmanager/RecentTasksManager$3$1;

.field final synthetic val$clearButton:Lmiui/widget/CircleProgressBar;

.field final synthetic val$freeAtFirst:I

.field final synthetic val$freeAtLast:I

.field final synthetic val$wm:Landroid/view/WindowManager;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/RecentTasksManager$3$1;Landroid/view/WindowManager;Lmiui/widget/CircleProgressBar;II)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->this$2:Lcom/android/systemui/taskmanager/RecentTasksManager$3$1;

    iput-object p2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->val$wm:Landroid/view/WindowManager;

    iput-object p3, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->val$clearButton:Lmiui/widget/CircleProgressBar;

    iput p4, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->val$freeAtFirst:I

    iput p5, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->val$freeAtLast:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 8

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->val$wm:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->val$clearButton:Lmiui/widget/CircleProgressBar;

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->this$2:Lcom/android/systemui/taskmanager/RecentTasksManager$3$1;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1;->this$1:Lcom/android/systemui/taskmanager/RecentTasksManager$3;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/RecentTasksManager$3;->this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-static {v2}, Lcom/android/systemui/taskmanager/RecentTasksManager;->-get0(Lcom/android/systemui/taskmanager/RecentTasksManager;)Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->val$freeAtFirst:I

    int-to-long v4, v3

    iget v3, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->val$freeAtLast:I

    int-to-long v6, v3

    invoke-static {v2, v4, v5, v6, v7}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getToastMsg(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1$1;->this$2:Lcom/android/systemui/taskmanager/RecentTasksManager$3$1;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/RecentTasksManager$3$1;->this$1:Lcom/android/systemui/taskmanager/RecentTasksManager$3;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/RecentTasksManager$3;->this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-static {v2}, Lcom/android/systemui/taskmanager/RecentTasksManager;->-get0(Lcom/android/systemui/taskmanager/RecentTasksManager;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    const/16 v2, 0x7d6

    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setType(I)V

    invoke-virtual {v1}, Landroid/widget/Toast;->getWindowParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method
