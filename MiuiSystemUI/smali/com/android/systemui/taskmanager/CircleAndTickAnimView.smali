.class public Lcom/android/systemui/taskmanager/CircleAndTickAnimView;
.super Landroid/view/View;
.source "CircleAndTickAnimView.java"


# instance fields
.field private isNormalDrawableShow:Z

.field private mAnimatorSet:Landroid/animation/AnimatorSet;

.field private mBackDrawable:Landroid/graphics/drawable/Drawable;

.field private mCancelBtnAlpha:F

.field private mCancelBtnAnimator:Landroid/animation/ValueAnimator;

.field private mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

.field private mCircleAnimator:Landroid/animation/ValueAnimator;

.field private mCircleRotateDegrees:F

.field private mDiameter:I

.field private mNormalDrawable:Landroid/graphics/drawable/Drawable;

.field private mTickAnimDrawable:Landroid/graphics/drawable/Drawable;

.field private mTickAnimator:Landroid/animation/ValueAnimator;

.field private mTickShowRight:F

.field private mTickShowWidthRatio:F


# direct methods
.method static synthetic -get0(Lcom/android/systemui/taskmanager/CircleAndTickAnimView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mDiameter:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/systemui/taskmanager/CircleAndTickAnimView;F)F
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAlpha:F

    return p1
.end method

.method static synthetic -set1(Lcom/android/systemui/taskmanager/CircleAndTickAnimView;F)F
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleRotateDegrees:F

    return p1
.end method

.method static synthetic -set2(Lcom/android/systemui/taskmanager/CircleAndTickAnimView;F)F
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickShowRight:F

    return p1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, 0x2

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAlpha:F

    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickShowWidthRatio:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->isNormalDrawableShow:Z

    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v1}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/systemui/taskmanager/CircleAndTickAnimView$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView$1;-><init>(Lcom/android/systemui/taskmanager/CircleAndTickAnimView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-array v0, v4, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lmiui/view/animation/CubicEaseInInterpolator;

    invoke-direct {v1}, Lmiui/view/animation/CubicEaseInInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/systemui/taskmanager/CircleAndTickAnimView$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView$2;-><init>(Lcom/android/systemui/taskmanager/CircleAndTickAnimView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-array v0, v4, [F

    fill-array-data v0, :array_2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v1}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/systemui/taskmanager/CircleAndTickAnimView$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView$3;-><init>(Lcom/android/systemui/taskmanager/CircleAndTickAnimView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->stopAnimator()V

    return-void

    nop

    :array_0
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
    .end array-data

    :array_1
    .array-data 4
        0x0
        -0x3b790000    # -1080.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f333333    # 0.7f
    .end array-data
.end method

.method private getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v3

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private getIntrinsicHeight()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_3
    return v0
.end method

.method private getIntrinsicWidth()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_3
    return v0
.end method


# virtual methods
.method public animatorStart(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->stopAnimator()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->isNormalDrawableShow:Z

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    const/high16 v4, 0x437f0000    # 255.0f

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->isNormalDrawableShow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getAlpha()F

    move-result v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getAlpha()F

    move-result v1

    mul-float/2addr v1, v4

    iget v2, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAlpha:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getAlpha()F

    move-result v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleRotateDegrees:F

    iget v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mDiameter:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mDiameter:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_2
    const/16 v0, 0x10

    invoke-virtual {p1, v3, v3, v0}, Landroid/graphics/Canvas;->saveLayer(Landroid/graphics/RectF;Landroid/graphics/Paint;I)I

    iget v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickShowRight:F

    iget v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickShowWidthRatio:F

    iget v2, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mDiameter:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickShowRight:F

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getAlpha()F

    move-result v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getIntrinsicWidth()I

    move-result v0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mDiameter:I

    iget v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mDiameter:I

    iget v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mDiameter:I

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setBackDrawable(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setBackDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setBackDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mBackDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setCircleAnimDrawable(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setCircleAnimDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setCircleAnimDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleAnimDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setDrawables(IIII)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setNormalDrawable(I)V

    invoke-virtual {p0, p2}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setBackDrawable(I)V

    invoke-virtual {p0, p3}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setCircleAnimDrawable(I)V

    invoke-virtual {p0, p4}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setTickAnimDrawable(I)V

    return-void
.end method

.method public setNormalDrawable(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setNormalDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setNormalDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setTickAnimDrawable(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setTickAnimDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setTickAnimDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickAnimDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public stopAnimator()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    iput v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCircleRotateDegrees:F

    iput v1, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mTickShowRight:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->mCancelBtnAlpha:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->isNormalDrawableShow:Z

    return-void
.end method
