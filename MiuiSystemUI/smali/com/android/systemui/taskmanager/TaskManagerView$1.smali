.class Lcom/android/systemui/taskmanager/TaskManagerView$1;
.super Ljava/lang/Object;
.source "TaskManagerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/taskmanager/TaskManagerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/TaskManagerView;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView$1;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    move-object v0, p1

    check-cast v0, Lcom/android/systemui/taskmanager/PreviewIconView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/MultiTaskView;->isReset()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "systemui_taskmanager_toggleapp_backtask"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView$1;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v1, v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->-wrap7(Lcom/android/systemui/taskmanager/TaskManagerView;Lcom/android/systemui/taskmanager/PreviewIconView;)V

    return-void
.end method
