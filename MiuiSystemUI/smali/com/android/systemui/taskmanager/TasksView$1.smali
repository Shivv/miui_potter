.class Lcom/android/systemui/taskmanager/TasksView$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "TasksView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/TasksView;->removeTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/TasksView;

.field final synthetic val$removeAnimator:Landroid/animation/Animator;

.field final synthetic val$removeIndex:I

.field final synthetic val$removeView:Lcom/android/systemui/taskmanager/TaskItemView;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/TasksView;Lcom/android/systemui/taskmanager/TaskItemView;ILandroid/animation/Animator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    iput-object p2, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeView:Lcom/android/systemui/taskmanager/TaskItemView;

    iput p3, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeIndex:I

    iput-object p4, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeAnimator:Landroid/animation/Animator;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 10

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeView:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v6, v6, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v6}, Lcom/android/systemui/taskmanager/TaskInfo;->getCompoundedTasks()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-static {v6}, Lcom/android/systemui/taskmanager/TasksView;->-get2(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/RecentTasksManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/android/systemui/taskmanager/RecentTasksManager;->removeTask(Lcom/android/systemui/taskmanager/TaskInfo;)V

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeView:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v6, v6, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v6, v6, Lcom/android/systemui/taskmanager/TaskInfo;->mTopRunning:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-static {v6}, Lcom/android/systemui/taskmanager/TasksView;->-get3(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/TaskManagerView;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/systemui/taskmanager/TaskManagerView;->setFirstTaskIsTopRunning(Z)V

    :cond_1
    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    iget v3, v6, Lcom/android/systemui/taskmanager/TasksView;->mCurrentScreen:I

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    iget v7, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeIndex:I

    invoke-virtual {v6, v7}, Lcom/android/systemui/taskmanager/TasksView;->removeScreen(I)V

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-static {v6}, Lcom/android/systemui/taskmanager/TasksView;->-get0(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->notifyDataSetChanged()V

    iget v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeIndex:I

    iget-object v7, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v7}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v7

    if-ne v6, v7, :cond_3

    iget v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeIndex:I

    if-ne v6, v3, :cond_3

    :cond_2
    const/4 v2, 0x0

    :goto_1
    iget v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeIndex:I

    if-ge v2, v6, :cond_4

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v6, v2}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v1

    sget-object v6, Lcom/android/systemui/taskmanager/TasksView;->TRANSLATION_X:Landroid/util/Property;

    const/4 v7, 0x2

    new-array v7, v7, [F

    iget-object v8, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    iget v8, v8, Lcom/android/systemui/taskmanager/TasksView;->mChildScreenWidth:I

    neg-int v8, v8

    int-to-float v8, v8

    const/4 v9, 0x0

    aput v8, v7, v9

    const/4 v8, 0x0

    const/4 v9, 0x1

    aput v8, v7, v9

    invoke-static {v1, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v6, 0x96

    invoke-virtual {v0, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeIndex:I

    if-lt v6, v3, :cond_2

    iget v2, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeIndex:I

    :goto_2
    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v6}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v6

    if-ge v2, v6, :cond_4

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v6, v2}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v1

    sget-object v6, Lcom/android/systemui/taskmanager/TasksView;->TRANSLATION_X:Landroid/util/Property;

    const/4 v7, 0x2

    new-array v7, v7, [F

    iget-object v8, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    iget v8, v8, Lcom/android/systemui/taskmanager/TasksView;->mChildScreenWidth:I

    int-to-float v8, v8

    const/4 v9, 0x0

    aput v8, v7, v9

    const/4 v8, 0x0

    const/4 v9, 0x1

    aput v8, v7, v9

    invoke-static {v1, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v6, 0x96

    invoke-virtual {v0, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v6}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    new-instance v7, Lcom/android/systemui/taskmanager/TasksView$1$1;

    invoke-direct {v7, p0}, Lcom/android/systemui/taskmanager/TasksView$1$1;-><init>(Lcom/android/systemui/taskmanager/TasksView$1;)V

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v7, v8, v9}, Lcom/android/systemui/taskmanager/TasksView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_5
    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-static {v6}, Lcom/android/systemui/taskmanager/TasksView;->-get3(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/TaskManagerView;

    move-result-object v6

    const/16 v7, 0x400

    invoke-virtual {v6, v7}, Lcom/android/systemui/taskmanager/TaskManagerView;->refreshMemoryInfo(I)V

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-static {v6}, Lcom/android/systemui/taskmanager/TasksView;->-get3(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/TaskManagerView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/systemui/taskmanager/TaskManagerView;->unfreeze()V

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TasksView$1;->this$0:Lcom/android/systemui/taskmanager/TasksView;

    invoke-static {v6}, Lcom/android/systemui/taskmanager/TasksView;->-get3(Lcom/android/systemui/taskmanager/TasksView;)Lcom/android/systemui/taskmanager/TaskManagerView;

    move-result-object v6

    iget-object v6, v6, Lcom/android/systemui/taskmanager/TaskManagerView;->mAnimatorsNeedForceEnd:Ljava/util/List;

    iget-object v7, p0, Lcom/android/systemui/taskmanager/TasksView$1;->val$removeAnimator:Landroid/animation/Animator;

    invoke-interface {v6, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
