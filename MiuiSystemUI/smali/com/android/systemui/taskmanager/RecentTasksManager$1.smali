.class Lcom/android/systemui/taskmanager/RecentTasksManager$1;
.super Landroid/os/AsyncTask;
.source "RecentTasksManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/RecentTasksManager;->loadThumbnailsInBackground(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

.field final synthetic val$descriptions:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/RecentTasksManager;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iput-object p2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->val$descriptions:Ljava/util/ArrayList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11

    const/4 v9, 0x0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v8

    invoke-static {v8}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v6

    invoke-static {v9}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v1, 0x1

    :goto_0
    iget-object v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->val$descriptions:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v1, v8, :cond_1

    iget-object v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->val$descriptions:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-virtual {v8, v7}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadThumbnail(Lcom/android/systemui/taskmanager/TaskInfo;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const-wide/16 v8, 0x0

    add-long/2addr v2, v8

    cmp-long v8, v2, v4

    if-lez v8, :cond_0

    sub-long v8, v2, v4

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_1
    invoke-static {v6}, Landroid/os/Process;->setThreadPriority(I)V

    const/4 v8, 0x0

    return-object v8

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    monitor-enter v7

    const/4 v8, 0x1

    :try_start_1
    new-array v8, v8, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x0

    aput-object v9, v8, v10

    invoke-virtual {p0, v8}, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v8, 0x1f4

    :try_start_2
    invoke-virtual {v7, v8, v9}, Lcom/android/systemui/taskmanager/TaskInfo;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    monitor-exit v7

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_2

    :catchall_0
    move-exception v8

    monitor-exit v7

    throw v8
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->val$descriptions:Ljava/util/ArrayList;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/RecentTasksManager;->-get4(Lcom/android/systemui/taskmanager/RecentTasksManager;)Lcom/android/systemui/taskmanager/RecentTasksManager$ThumbnailLoadedCallback;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/RecentTasksManager;->-get4(Lcom/android/systemui/taskmanager/RecentTasksManager;)Lcom/android/systemui/taskmanager/RecentTasksManager$ThumbnailLoadedCallback;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/android/systemui/taskmanager/RecentTasksManager$ThumbnailLoadedCallback;->onTaskThumbnailLoaded(Lcom/android/systemui/taskmanager/TaskInfo;)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/RecentTasksManager;->-get1(Lcom/android/systemui/taskmanager/RecentTasksManager;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->this$0:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/RecentTasksManager;->-get1(Lcom/android/systemui/taskmanager/RecentTasksManager;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/systemui/taskmanager/RecentTasksManager$1$1;

    invoke-direct {v2, p0, v0}, Lcom/android/systemui/taskmanager/RecentTasksManager$1$1;-><init>(Lcom/android/systemui/taskmanager/RecentTasksManager$1;Lcom/android/systemui/taskmanager/TaskInfo;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/RecentTasksManager$1;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
