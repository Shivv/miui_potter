.class Lcom/android/systemui/TorchServiceView$6;
.super Landroid/telephony/PhoneStateListener;
.source "TorchServiceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/TorchServiceView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/TorchServiceView;


# direct methods
.method constructor <init>(Lcom/android/systemui/TorchServiceView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/TorchServiceView$6;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "TorchServiceView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/TorchServiceView$6;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v2}, Lcom/android/systemui/TorchServiceView;->-get5(Lcom/android/systemui/TorchServiceView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView$6;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v0}, Lcom/android/systemui/TorchServiceView;->-get5(Lcom/android/systemui/TorchServiceView;)I

    move-result v0

    if-nez v0, :cond_1

    if-ne p1, v4, :cond_1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView$6;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v0}, Lcom/android/systemui/TorchServiceView;->-get1(Lcom/android/systemui/TorchServiceView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "flash_when_ring_enabled"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView$6;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v0, v4}, Lcom/android/systemui/TorchServiceView;->-wrap0(Lcom/android/systemui/TorchServiceView;Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/systemui/TorchServiceView$6;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v0, p1}, Lcom/android/systemui/TorchServiceView;->-set0(Lcom/android/systemui/TorchServiceView;I)I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView$6;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v0, v3}, Lcom/android/systemui/TorchServiceView;->-wrap2(Lcom/android/systemui/TorchServiceView;I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/TorchServiceView$6;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v0, v3}, Lcom/android/systemui/TorchServiceView;->-wrap0(Lcom/android/systemui/TorchServiceView;Z)V

    goto :goto_0
.end method
