.class Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;
.super Ljava/lang/Thread;
.source "CameraKeyActionAppPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/settings/CameraKeyActionAppPicker;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/settings/CameraKeyActionAppPicker;


# direct methods
.method constructor <init>(Lcom/android/systemui/settings/CameraKeyActionAppPicker;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;->this$0:Lcom/android/systemui/settings/CameraKeyActionAppPicker;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x0

    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v8, "android.intent.action.MAIN"

    invoke-direct {v3, v8, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string/jumbo v8, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v8, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;->this$0:Lcom/android/systemui/settings/CameraKeyActionAppPicker;

    invoke-virtual {v8}, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v3, v12}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    new-instance v8, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1$1;

    invoke-direct {v8, p0, v4}, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1$1;-><init>(Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;Landroid/content/pm/PackageManager;)V

    invoke-static {v2, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v8, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;->this$0:Lcom/android/systemui/settings/CameraKeyActionAppPicker;

    invoke-static {v8}, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->-get0(Lcom/android/systemui/settings/CameraKeyActionAppPicker;)Landroid/preference/PreferenceGroup;

    move-result-object v8

    invoke-virtual {v8}, Landroid/preference/PreferenceGroup;->removeAll()V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v1, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v1, :cond_0

    new-instance v5, Landroid/preference/Preference;

    iget-object v8, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;->this$0:Lcom/android/systemui/settings/CameraKeyActionAppPicker;

    invoke-direct {v5, v8, v11}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {v1, v4}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v10, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;->this$0:Lcom/android/systemui/settings/CameraKeyActionAppPicker;

    invoke-static {v8}, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->-get0(Lcom/android/systemui/settings/CameraKeyActionAppPicker;)Landroid/preference/PreferenceGroup;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;->this$0:Lcom/android/systemui/settings/CameraKeyActionAppPicker;

    invoke-static {v8, v12}, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->-set0(Lcom/android/systemui/settings/CameraKeyActionAppPicker;Z)Z

    return-void
.end method
