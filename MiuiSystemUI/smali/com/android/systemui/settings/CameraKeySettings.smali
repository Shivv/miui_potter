.class public Lcom/android/systemui/settings/CameraKeySettings;
.super Lmiui/preference/PreferenceActivity;
.source "CameraKeySettings.java"


# instance fields
.field private mApp:Landroid/preference/Preference;

.field private mNone:Landroid/preference/Preference;

.field private mResolver:Landroid/content/ContentResolver;

.field private mShortcut:Landroid/preference/Preference;

.field private mToggle:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private updateState()V
    .locals 15

    const/4 v5, 0x0

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v13, "camera_key_preferred_action_type"

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v9, 0x0

    const/4 v12, 0x1

    if-ne v0, v12, :cond_0

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v13, "camera_key_preferred_action_shortcut_id"

    const/4 v14, -0x1

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :cond_0
    :goto_0
    if-eqz v9, :cond_3

    const/4 v5, 0x1

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mShortcut:Landroid/preference/Preference;

    const v13, 0x7f020086

    invoke-virtual {v12, v13}, Landroid/preference/Preference;->setIcon(I)V

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mShortcut:Landroid/preference/Preference;

    invoke-virtual {v12, v9}, Landroid/preference/Preference;->setSummary(I)V

    :goto_1
    const/4 v11, 0x0

    const/4 v12, 0x2

    if-ne v0, v12, :cond_1

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v13, "camera_key_preferred_action_toggle_id"

    const/4 v14, -0x1

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    invoke-static {v10}, Lmiui/app/ToggleManager;->getName(I)I

    move-result v11

    :cond_1
    if-eqz v11, :cond_4

    const/4 v5, 0x1

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mToggle:Landroid/preference/Preference;

    const v13, 0x7f020086

    invoke-virtual {v12, v13}, Landroid/preference/Preference;->setIcon(I)V

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mToggle:Landroid/preference/Preference;

    invoke-virtual {v12, v11}, Landroid/preference/Preference;->setSummary(I)V

    :goto_2
    const/4 v1, 0x0

    const/4 v12, 0x3

    if-ne v0, v12, :cond_2

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v13, "camera_key_preferred_action_app_component"

    invoke-static {v12, v13}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/systemui/settings/CameraKeySettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/4 v12, 0x0

    invoke-virtual {v7, v2, v12}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_2
    :goto_3
    if-eqz v1, :cond_5

    const/4 v5, 0x1

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mApp:Landroid/preference/Preference;

    const v13, 0x7f020086

    invoke-virtual {v12, v13}, Landroid/preference/Preference;->setIcon(I)V

    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mApp:Landroid/preference/Preference;

    invoke-virtual {v12, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_4
    iget-object v13, p0, Lcom/android/systemui/settings/CameraKeySettings;->mNone:Landroid/preference/Preference;

    if-nez v5, :cond_6

    const v12, 0x7f020086

    :goto_5
    invoke-virtual {v13, v12}, Landroid/preference/Preference;->setIcon(I)V

    return-void

    :pswitch_0
    const v9, 0x7f0d0291

    goto :goto_0

    :pswitch_1
    const v9, 0x7f0d0292

    goto :goto_0

    :pswitch_2
    const v9, 0x7f0d0293

    goto :goto_0

    :pswitch_3
    const v9, 0x7f0d0294

    goto :goto_0

    :pswitch_4
    const v9, 0x7f0d0295

    goto :goto_0

    :cond_3
    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mShortcut:Landroid/preference/Preference;

    const v13, 0x7f020095

    invoke-virtual {v12, v13}, Landroid/preference/Preference;->setIcon(I)V

    goto :goto_1

    :cond_4
    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mToggle:Landroid/preference/Preference;

    const v13, 0x7f020095

    invoke-virtual {v12, v13}, Landroid/preference/Preference;->setIcon(I)V

    goto :goto_2

    :cond_5
    iget-object v12, p0, Lcom/android/systemui/settings/CameraKeySettings;->mApp:Landroid/preference/Preference;

    const v13, 0x7f020095

    invoke-virtual {v12, v13}, Landroid/preference/Preference;->setIcon(I)V

    goto :goto_4

    :cond_6
    const v12, 0x7f020095

    goto :goto_5

    :catch_0
    move-exception v4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f060003

    invoke-virtual {p0, v0}, Lcom/android/systemui/settings/CameraKeySettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "camera_key_action_none"

    invoke-virtual {p0, v0}, Lcom/android/systemui/settings/CameraKeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/settings/CameraKeySettings;->mNone:Landroid/preference/Preference;

    const-string/jumbo v0, "camera_key_action_shortcut"

    invoke-virtual {p0, v0}, Lcom/android/systemui/settings/CameraKeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/settings/CameraKeySettings;->mShortcut:Landroid/preference/Preference;

    const-string/jumbo v0, "camera_key_action_toggle"

    invoke-virtual {p0, v0}, Lcom/android/systemui/settings/CameraKeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/settings/CameraKeySettings;->mToggle:Landroid/preference/Preference;

    const-string/jumbo v0, "camera_key_action_app"

    invoke-virtual {p0, v0}, Lcom/android/systemui/settings/CameraKeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/settings/CameraKeySettings;->mApp:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/systemui/settings/CameraKeySettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/settings/CameraKeySettings;->mResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/settings/CameraKeySettings;->mNone:Landroid/preference/Preference;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/settings/CameraKeySettings;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "camera_key_preferred_action_type"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/systemui/settings/CameraKeySettings;->updateState()V

    :cond_0
    invoke-super {p0, p1, p2}, Lmiui/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    invoke-direct {p0}, Lcom/android/systemui/settings/CameraKeySettings;->updateState()V

    return-void
.end method
