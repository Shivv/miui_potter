.class public Lcom/android/systemui/qs/QSSecurityFooter$VpnSpan;
.super Landroid/text/style/ClickableSpan;
.source "QSSecurityFooter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/qs/QSSecurityFooter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "VpnSpan"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/qs/QSSecurityFooter;


# direct methods
.method protected constructor <init>(Lcom/android/systemui/qs/QSSecurityFooter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/qs/QSSecurityFooter$VpnSpan;->this$0:Lcom/android/systemui/qs/QSSecurityFooter;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/systemui/qs/QSSecurityFooter$VpnSpan;

    return v0
.end method

.method public hashCode()I
    .locals 1

    const v0, 0x12b9b099

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    iget-object v3, p0, Lcom/android/systemui/qs/QSSecurityFooter$VpnSpan;->this$0:Lcom/android/systemui/qs/QSSecurityFooter;

    invoke-static {v3}, Lcom/android/systemui/qs/QSSecurityFooter;->-get1(Lcom/android/systemui/qs/QSSecurityFooter;)Lmiui/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/app/AlertDialog;->dismiss()V

    invoke-static {}, Lcom/android/systemui/CompatibilityO;->getQsFooterVpnSettingsActionIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/high16 v3, 0x14000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v2, Landroid/app/ActivityOptions;

    iget-object v3, p0, Lcom/android/systemui/qs/QSSecurityFooter$VpnSpan;->this$0:Lcom/android/systemui/qs/QSSecurityFooter;

    invoke-virtual {v3}, Lcom/android/systemui/qs/QSSecurityFooter;->getActivityOptions()Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ActivityOptions;-><init>(Landroid/os/Bundle;)V

    :try_start_0
    iget-object v3, p0, Lcom/android/systemui/qs/QSSecurityFooter$VpnSpan;->this$0:Lcom/android/systemui/qs/QSSecurityFooter;

    invoke-static {v3}, Lcom/android/systemui/qs/QSSecurityFooter;->-get0(Lcom/android/systemui/qs/QSSecurityFooter;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v4

    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v3, v1, v4, v5}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    iget-object v3, p0, Lcom/android/systemui/qs/QSSecurityFooter$VpnSpan;->this$0:Lcom/android/systemui/qs/QSSecurityFooter;

    iget-object v3, v3, Lcom/android/systemui/qs/QSSecurityFooter;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/qs/QSSecurityFooter$VpnSpan;->this$0:Lcom/android/systemui/qs/QSSecurityFooter;

    iget-object v3, v3, Lcom/android/systemui/qs/QSSecurityFooter;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->animateCollapseAndUnlock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
