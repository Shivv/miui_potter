.class public final Lcom/android/systemui/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final DeadZone:[I

.field public static final KeyButtonView:[I

.field public static final NotificationLinearLayout:[I

.field public static final NotificationRowLayout:[I

.field public static final RecentsPanelView:[I

.field public static final RestrictedPreference:[I

.field public static final RestrictedSwitchPreference:[I

.field public static final ToggleSlider:[I

.field public static final UsageView:[I

.field public static final WifiEncryptionState:[I

.field public static final WifiMeteredState:[I

.field public static final WifiSavedState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v0, 0x7f010013

    const v1, 0x7f010014

    const v2, 0x7f010015

    const v3, 0x7f010016

    const v4, 0x7f010017

    filled-new-array {v0, v1, v2, v3, v4}, [I

    move-result-object v0

    sput-object v0, Lcom/android/systemui/R$styleable;->DeadZone:[I

    const v0, 0x1010273

    const v1, 0x7f01000c

    const v2, 0x7f01000d

    const v3, 0x7f01000e

    filled-new-array {v0, v1, v2, v3}, [I

    move-result-object v0

    sput-object v0, Lcom/android/systemui/R$styleable;->KeyButtonView:[I

    new-array v0, v6, [I

    const v1, 0x7f010010

    aput v1, v0, v5

    sput-object v0, Lcom/android/systemui/R$styleable;->NotificationLinearLayout:[I

    new-array v0, v6, [I

    const v1, 0x7f010011

    aput v1, v0, v5

    sput-object v0, Lcom/android/systemui/R$styleable;->NotificationRowLayout:[I

    new-array v0, v6, [I

    const v1, 0x7f010012

    aput v1, v0, v5

    sput-object v0, Lcom/android/systemui/R$styleable;->RecentsPanelView:[I

    const/high16 v0, 0x7f010000

    const v1, 0x7f010001

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/systemui/R$styleable;->RestrictedPreference:[I

    const v0, 0x7f010002

    const v1, 0x7f010003

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/systemui/R$styleable;->RestrictedSwitchPreference:[I

    new-array v0, v6, [I

    const v1, 0x7f01000f

    aput v1, v0, v5

    sput-object v0, Lcom/android/systemui/R$styleable;->ToggleSlider:[I

    const v0, 0x10100af

    const v1, 0x1010435

    const v2, 0x7f010009

    const v3, 0x7f01000a

    const v4, 0x7f01000b

    filled-new-array {v0, v1, v2, v3, v4}, [I

    move-result-object v0

    sput-object v0, Lcom/android/systemui/R$styleable;->UsageView:[I

    new-array v0, v6, [I

    const v1, 0x7f010004

    aput v1, v0, v5

    sput-object v0, Lcom/android/systemui/R$styleable;->WifiEncryptionState:[I

    new-array v0, v6, [I

    const v1, 0x7f010005

    aput v1, v0, v5

    sput-object v0, Lcom/android/systemui/R$styleable;->WifiMeteredState:[I

    new-array v0, v6, [I

    const v1, 0x7f010006

    aput v1, v0, v5

    sput-object v0, Lcom/android/systemui/R$styleable;->WifiSavedState:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
