.class public Lcom/android/systemui/statusbar/OldmanStatusPane;
.super Landroid/widget/FrameLayout;
.source "OldmanStatusPane.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/OldmanStatusPane$1;,
        Lcom/android/systemui/statusbar/OldmanStatusPane$2;,
        Lcom/android/systemui/statusbar/OldmanStatusPane$3;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileDataTypeSwitchesValues:[I

.field private static final synthetic -com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileStrengthSwitchesValues:[I

.field private static final synthetic -com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanWifiStrengthSwitchesValues:[I

.field private static final ms_mobileMarkResIdArr:[I

.field private static final ms_mobileMarkResIdArrRev:[I


# instance fields
.field private final APPLY_R:Ljava/lang/Runnable;

.field private final commonReceiversCallback:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

.field private mCallState:Ljava/lang/String;

.field private mRecorderState:I

.field private mRecorderTitle:Ljava/lang/String;

.field private m_airplaneBox:Landroid/view/ViewGroup;

.field private m_airplaneMode:Z

.field private m_airplaneModeUpdated:Z

.field private m_airplaneTextView:Landroid/widget/TextView;

.field private m_alarmBox:Landroid/view/ViewGroup;

.field private m_alarmImageView:Landroid/widget/ImageView;

.field private m_alarmUpdated:Z

.field private m_attachedToWindow:Z

.field private m_backgroundTransparent:Z

.field private m_backgroundUpdated:Z

.field private m_basicLayer:Landroid/view/ViewGroup;

.field private m_basicRightPart:Landroid/view/ViewGroup;

.field private m_batteryBox:Landroid/view/ViewGroup;

.field private m_batteryImageView:Landroid/widget/ImageView;

.field private m_batteryPercent:I

.field private m_batteryPlugged:I

.field private m_batteryStatus:I

.field private m_batteryUpdated:Z

.field private m_bluetoothBox:Landroid/view/ViewGroup;

.field private m_bluetoothConnectionState:I

.field private m_bluetoothImageView:Landroid/widget/ImageView;

.field private m_bluetoothState:I

.field private m_bluetoothUpdated:Z

.field private m_clockText:Ljava/lang/CharSequence;

.field private m_clockUpdated:Z

.field private m_clockView:Lcom/android/systemui/statusbar/policy/OldmanClockView;

.field private m_clockViewOnInRecorder:Lcom/android/systemui/statusbar/policy/OldmanClockView;

.field private m_clockViewOnIncall:Lcom/android/systemui/statusbar/policy/OldmanClockView;

.field private m_commonReceivers:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

.field private m_darkMode:Z

.field private m_hasSystemAlarm:Z

.field private m_hasThirdPartyAlarm:Z

.field private m_headsetBox:Landroid/view/ViewGroup;

.field private m_headsetImageView:Landroid/widget/ImageView;

.field private m_headsetPlug:Z

.field private m_headsetUpdated:Z

.field private m_headsetWithMic:Z

.field private m_iconsDarkArea:Landroid/graphics/Rect;

.field private m_inKeyguard:Z

.field private m_inKeyguardUpdated:Z

.field private m_incallLayer:Landroid/view/ViewGroup;

.field private m_incallLeaveOrResume:Z

.field private m_incallRightPart:Landroid/view/ViewGroup;

.field private m_incallTextView:Landroid/widget/TextView;

.field private m_incallUpdated:Z

.field private m_mobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

.field private m_mobileActivityImageViewArr:[Landroid/widget/ImageView;

.field private m_mobileBoxArr:[Landroid/view/ViewGroup;

.field private m_mobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field private m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

.field private m_mobileMarkImageViewArr:[Landroid/widget/ImageView;

.field private m_mobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field private m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

.field private m_mobileUpdatedArr:[Z

.field private m_mobileVisibleArr:[Z

.field private m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

.field private m_mobileVolteImageViewArr:[Landroid/widget/ImageView;

.field private m_mobileVolteRegistedArr:[Z

.field private m_quietMode:Z

.field private m_quietModeBox:Landroid/view/ViewGroup;

.field private m_quietModeImageView:Landroid/widget/ImageView;

.field private m_quietModeUpdated:Z

.field private m_recorderLayer:Landroid/view/ViewGroup;

.field private m_recorderTextView:Landroid/widget/TextView;

.field private m_ringerMode:I

.field private m_ringerModeBox:Landroid/view/ViewGroup;

.field private m_ringerModeImageView:Landroid/widget/ImageView;

.field private m_ringerModeUpdated:Z

.field private m_roamingArr:[Z

.field private m_wifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

.field private m_wifiBox:Landroid/view/ViewGroup;

.field private m_wifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field private m_wifiStrengthImageView:Landroid/widget/ImageView;

.field private m_wifiUpdated:Z

.field private m_wifiVisible:Z

.field public final ncSignalCluster:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileUpdatedArr:[Z

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVisibleArr:[Z

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteRegistedArr:[Z

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_roamingArr:[Z

    return-object v0
.end method

.method private static synthetic -getcom-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileDataTypeSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileDataTypeSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileDataTypeSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->values()[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->E:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->H:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->HP:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->X1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    sput-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileDataTypeSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1

    :catch_7
    move-exception v1

    goto :goto_0
.end method

.method private static synthetic -getcom-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileStrengthSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileStrengthSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileStrengthSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->values()[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_DISCONNECTED:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_IDLE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    sput-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileStrengthSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1

    :catch_8
    move-exception v1

    goto :goto_0
.end method

.method private static synthetic -getcom-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanWifiStrengthSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanWifiStrengthSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanWifiStrengthSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->values()[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_USB_SHARE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    sput-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->-com-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanWifiStrengthSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1

    :catch_7
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneMode:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneModeUpdated:Z

    return p1
.end method

.method static synthetic -set10(Lcom/android/systemui/statusbar/OldmanStatusPane;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockText:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic -set11(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockUpdated:Z

    return p1
.end method

.method static synthetic -set12(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_hasSystemAlarm:Z

    return p1
.end method

.method static synthetic -set13(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_hasThirdPartyAlarm:Z

    return p1
.end method

.method static synthetic -set14(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetPlug:Z

    return p1
.end method

.method static synthetic -set15(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetUpdated:Z

    return p1
.end method

.method static synthetic -set16(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetWithMic:Z

    return p1
.end method

.method static synthetic -set17(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLeaveOrResume:Z

    return p1
.end method

.method static synthetic -set18(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallUpdated:Z

    return p1
.end method

.method static synthetic -set19(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietMode:Z

    return p1
.end method

.method static synthetic -set2(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmUpdated:Z

    return p1
.end method

.method static synthetic -set20(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeUpdated:Z

    return p1
.end method

.method static synthetic -set21(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerMode:I

    return p1
.end method

.method static synthetic -set22(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeUpdated:Z

    return p1
.end method

.method static synthetic -set23(Lcom/android/systemui/statusbar/OldmanStatusPane;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    return-object p1
.end method

.method static synthetic -set24(Lcom/android/systemui/statusbar/OldmanStatusPane;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object p1
.end method

.method static synthetic -set25(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiUpdated:Z

    return p1
.end method

.method static synthetic -set26(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiVisible:Z

    return p1
.end method

.method static synthetic -set3(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryPercent:I

    return p1
.end method

.method static synthetic -set4(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryPlugged:I

    return p1
.end method

.method static synthetic -set5(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryStatus:I

    return p1
.end method

.method static synthetic -set6(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryUpdated:Z

    return p1
.end method

.method static synthetic -set7(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothConnectionState:I

    return p1
.end method

.method static synthetic -set8(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothState:I

    return p1
.end method

.method static synthetic -set9(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothUpdated:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->apply()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/OldmanStatusPane;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->doApply()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/systemui/statusbar/OldmanStatusPane;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->tryInvokeTelephonyShowInCallScreen(Landroid/content/Context;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const v0, 0x7f02013e

    const v1, 0x7f020140

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->ms_mobileMarkResIdArr:[I

    const v0, 0x7f02013f

    const v1, 0x7f020141

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->ms_mobileMarkResIdArrRev:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v4, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_commonReceivers:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_attachedToWindow:Z

    new-array v0, v1, [Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    new-array v0, v1, [Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteImageViewArr:[Landroid/widget/ImageView;

    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityImageViewArr:[Landroid/widget/ImageView;

    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileMarkImageViewArr:[Landroid/widget/ImageView;

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothState:I

    iput v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothConnectionState:I

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiVisible:Z

    new-array v0, v1, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVisibleArr:[Z

    new-array v0, v1, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-array v0, v1, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteRegistedArr:[Z

    new-array v0, v1, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneMode:Z

    new-array v0, v1, [Z

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_roamingArr:[Z

    iput v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryPlugged:I

    iput v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryStatus:I

    const/16 v0, 0x64

    iput v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryPercent:I

    iput v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerMode:I

    iput-object v4, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockText:Ljava/lang/CharSequence;

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLeaveOrResume:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_hasSystemAlarm:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_hasThirdPartyAlarm:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetPlug:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetWithMic:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietMode:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_inKeyguard:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundTransparent:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_darkMode:Z

    iput-object v4, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_iconsDarkArea:Landroid/graphics/Rect;

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiUpdated:Z

    new-array v0, v1, [Z

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileUpdatedArr:[Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneModeUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_inKeyguardUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetUpdated:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeUpdated:Z

    new-instance v0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/OldmanStatusPane$1;-><init>(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->ncSignalCluster:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;

    new-instance v0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/OldmanStatusPane$2;-><init>(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->commonReceiversCallback:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    new-instance v0, Lcom/android/systemui/statusbar/OldmanStatusPane$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/OldmanStatusPane$3;-><init>(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->APPLY_R:Ljava/lang/Runnable;

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x1t
        0x1t
    .end array-data
.end method

.method private _doResetAllUpdatedFlags(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiUpdated:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileUpdatedArr:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileUpdatedArr:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneModeUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_inKeyguardUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetUpdated:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeUpdated:Z

    return-void
.end method

.method private _refreshBackground()V
    .locals 3

    const-string/jumbo v0, "InCallScreenView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "_refreshBackground, incallstats = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundTransparent:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f02033c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->showReturnToInCallScreenView(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f020338

    const v2, 0x7f020339

    invoke-direct {p0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private _selectResId(II)I
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_darkMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundTransparent:Z

    if-eqz v0, :cond_0

    return p2

    :cond_0
    return p1
.end method

.method private apply()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_attachedToWindow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->APPLY_R:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/systemui/statusbar/OldmanStatusPane;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method private doApply()V
    .locals 27

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_attachedToWindow:Z

    move/from16 v22, v0

    if-nez v22, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_refreshBackground()V

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_inKeyguardUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_refreshBackground()V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_inKeyguard:Z

    move/from16 v22, v0

    xor-int/lit8 v15, v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicRightPart:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v15, :cond_a

    const/16 v22, 0x0

    :goto_0
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallRightPart:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v15, :cond_b

    const/16 v22, 0x0

    :goto_1
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothState:I

    move/from16 v22, v0

    const/16 v23, 0xc

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_c

    const/4 v13, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothBox:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v13, :cond_d

    const/16 v22, 0x0

    :goto_3
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothConnectionState:I

    move/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_e

    const/4 v7, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothImageView:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v7, :cond_f

    const v22, 0x7f020135

    const v24, 0x7f020136

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v22

    :goto_5
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiVisible:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiBox:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v21, :cond_10

    const/16 v22, 0x0

    :goto_6
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v21, :cond_4

    invoke-static {}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-getcom-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanWifiStrengthSwitchesValues()[I

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->ordinal()I

    move-result v23

    aget v22, v22, v23

    packed-switch v22, :pswitch_data_0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiBox:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_4
    :goto_7
    const/16 v20, 0x0

    const/4 v10, 0x0

    :goto_8
    const/16 v22, 0x2

    move/from16 v0, v22

    if-ge v10, v0, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileUpdatedArr:[Z

    move-object/from16 v22, v0

    aget-boolean v22, v22, v10

    if-nez v22, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneModeUpdated:Z

    move/from16 v22, v0

    if-nez v22, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_9

    :cond_5
    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneMode:Z

    move/from16 v22, v0

    if-nez v22, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVisibleArr:[Z

    move-object/from16 v22, v0

    aget-boolean v21, v22, v10

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    aget-object v23, v22, v10

    if-eqz v21, :cond_12

    const/16 v22, 0x0

    :goto_a
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v21, :cond_6

    invoke-static {}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-getcom-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileStrengthSwitchesValues()[I

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-object/from16 v23, v0

    aget-object v23, v23, v10

    invoke-virtual/range {v23 .. v23}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->ordinal()I

    move-result v23

    aget v22, v22, v23

    packed-switch v22, :pswitch_data_1

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_6
    :goto_b
    if-eqz v21, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    if-eqz v22, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    sget-object v23, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiBox:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v22

    if-eqz v22, :cond_13

    const/16 v17, 0x1

    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v23, v22, v10

    if-eqz v17, :cond_15

    const/16 v22, 0x0

    :goto_d
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v17, :cond_7

    invoke-static {}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-getcom-android-systemui-statusbar-policy-IOldmanSignalCluster$OldmanMobileDataTypeSwitchesValues()[I

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    move-object/from16 v23, v0

    aget-object v23, v23, v10

    invoke-virtual/range {v23 .. v23}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->ordinal()I

    move-result v23

    aget v22, v22, v23

    packed-switch v22, :pswitch_data_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f02014a

    const v24, 0x7f02014b

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_7
    :goto_e
    if-eqz v21, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteRegistedArr:[Z

    move-object/from16 v22, v0

    aget-boolean v18, v22, v10

    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    aget-object v23, v22, v10

    if-eqz v18, :cond_17

    const/16 v22, 0x0

    :goto_10
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020189

    const v24, 0x7f02018a

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_8
    if-eqz v17, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    if-eqz v22, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    sget-object v23, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_18

    const/16 v16, 0x1

    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v23, v22, v10

    if-eqz v16, :cond_1a

    const/16 v22, 0x0

    :goto_12
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v16, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020142

    const v24, 0x7f020143

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_9
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_8

    :cond_a
    const/16 v22, 0x8

    goto/16 :goto_0

    :cond_b
    const/16 v22, 0x8

    goto/16 :goto_1

    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_2

    :cond_d
    const/16 v22, 0x8

    goto/16 :goto_3

    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_4

    :cond_f
    const v22, 0x7f020134

    const v24, 0x7f020137

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v22

    goto/16 :goto_5

    :cond_10
    const/16 v22, 0x8

    goto/16 :goto_6

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrengthImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f02018b

    const v24, 0x7f02018c

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrengthImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f02018d

    const v24, 0x7f02018e

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrengthImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f02018f

    const v24, 0x7f020190

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrengthImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f020191

    const v24, 0x7f020192

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrengthImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f020193

    const v24, 0x7f020194

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    :cond_11
    const/16 v21, 0x0

    goto/16 :goto_9

    :cond_12
    const/16 v22, 0x8

    goto/16 :goto_a

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020150

    const v24, 0x7f020151

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_b

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020152

    const v24, 0x7f020153

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_b

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020154

    const v24, 0x7f020155

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_b

    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020156

    const v24, 0x7f020157

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_b

    :pswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020158

    const v24, 0x7f020159

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_b

    :pswitch_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020150

    const v24, 0x7f020151

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_b

    :cond_13
    const/16 v17, 0x0

    goto/16 :goto_c

    :cond_14
    const/16 v17, 0x0

    goto/16 :goto_c

    :cond_15
    const/16 v22, 0x8

    goto/16 :goto_d

    :pswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020146

    const v24, 0x7f020147

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_e

    :pswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020144

    const v24, 0x7f020145

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_e

    :pswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f02014e

    const v24, 0x7f02014f

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_e

    :pswitch_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f02014c

    const v24, 0x7f02014d

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_e

    :pswitch_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v10

    const v23, 0x7f020148

    const v24, 0x7f020149

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_e

    :cond_16
    const/16 v18, 0x0

    goto/16 :goto_f

    :cond_17
    const/16 v22, 0x8

    goto/16 :goto_10

    :cond_18
    const/16 v16, 0x0

    goto/16 :goto_11

    :cond_19
    const/16 v16, 0x0

    goto/16 :goto_11

    :cond_1a
    const/16 v22, 0x8

    goto/16 :goto_12

    :cond_1b
    if-eqz v20, :cond_20

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/android/systemui/OldmanHelper;->getMobileMarkShownMode(Landroid/content/Context;)Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    move-result-object v19

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v12, v0, [Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aget-object v22, v22, v23

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v22

    if-nez v22, :cond_1d

    const/16 v22, 0x1

    :goto_13
    const/16 v23, 0x0

    aput-boolean v22, v12, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    aget-object v22, v22, v23

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v22

    if-nez v22, :cond_1e

    const/16 v22, 0x1

    :goto_14
    const/16 v23, 0x1

    aput-boolean v22, v12, v23

    const/4 v9, 0x0

    :goto_15
    const/16 v22, 0x2

    move/from16 v0, v22

    if-ge v9, v0, :cond_20

    aget-boolean v22, v12, v9

    if-eqz v22, :cond_1c

    const/16 v22, 0x0

    aget-boolean v22, v12, v22

    const/16 v23, 0x1

    aget-boolean v23, v12, v23

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v9, v1, v2}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->isMobileMarkShown(IZZ)Z

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileMarkImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v23, v22, v9

    if-eqz v11, :cond_1f

    const/16 v22, 0x0

    :goto_16
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v11, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileMarkImageViewArr:[Landroid/widget/ImageView;

    move-object/from16 v22, v0

    aget-object v22, v22, v9

    sget-object v23, Lcom/android/systemui/statusbar/OldmanStatusPane;->ms_mobileMarkResIdArr:[I

    aget v23, v23, v9

    sget-object v24, Lcom/android/systemui/statusbar/OldmanStatusPane;->ms_mobileMarkResIdArrRev:[I

    aget v24, v24, v9

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1c
    add-int/lit8 v9, v9, 0x1

    goto :goto_15

    :cond_1d
    const/16 v22, 0x0

    goto :goto_13

    :cond_1e
    const/16 v22, 0x0

    goto :goto_14

    :cond_1f
    const/16 v22, 0x8

    goto :goto_16

    :cond_20
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneModeUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_21

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneMode:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneBox:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v21, :cond_29

    const/16 v22, 0x0

    :goto_17
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v21, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneTextView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, 0x7f09008e

    const v24, 0x7f09008f

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_21
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryPlugged:I

    move/from16 v22, v0

    if-nez v22, :cond_22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryStatus:I

    move/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_2a

    :cond_22
    const/4 v6, 0x1

    :goto_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryImageView:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    if-eqz v6, :cond_2b

    const v22, 0x7f020130

    const v24, 0x7f020132

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v22

    :goto_19
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryImageView:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryPercent:I

    move/from16 v22, v0

    const/16 v24, 0x1

    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_2c

    const/16 v22, 0x1

    :goto_1a
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageLevel(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v6, :cond_23

    if-eqz v5, :cond_23

    instance-of v0, v5, Landroid/graphics/drawable/LayerDrawable;

    move/from16 v22, v0

    if-eqz v22, :cond_23

    move-object v8, v5

    check-cast v8, Landroid/graphics/drawable/LayerDrawable;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v8, v0}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_23

    instance-of v0, v4, Landroid/graphics/drawable/AnimationDrawable;

    move/from16 v22, v0

    if-eqz v22, :cond_23

    move-object v3, v4

    check-cast v3, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    :cond_23
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerMode:I

    move/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_2d

    const/16 v21, 0x1

    :goto_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeBox:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v21, :cond_2e

    const/16 v22, 0x0

    :goto_1c
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v21, :cond_24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerMode:I

    move/from16 v22, v0

    packed-switch v22, :pswitch_data_3

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeBox:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_24
    :goto_1d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockView:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockText:Ljava/lang/CharSequence;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockView:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_darkMode:Z

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_iconsDarkArea:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    invoke-static/range {v24 .. v26}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v24

    invoke-virtual/range {v22 .. v24}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->setClockText(Ljava/lang/CharSequence;Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnIncall:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockText:Ljava/lang/CharSequence;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnIncall:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_darkMode:Z

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_iconsDarkArea:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    invoke-static/range {v24 .. v26}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v24

    invoke-virtual/range {v22 .. v24}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->setClockText(Ljava/lang/CharSequence;Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnInRecorder:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockText:Ljava/lang/CharSequence;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnInRecorder:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_darkMode:Z

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_iconsDarkArea:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    invoke-static/range {v24 .. v26}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v24

    invoke-virtual/range {v22 .. v24}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->setClockText(Ljava/lang/CharSequence;Z)V

    :cond_25
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_26

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_hasSystemAlarm:Z

    move/from16 v22, v0

    if-nez v22, :cond_2f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_hasThirdPartyAlarm:Z

    move/from16 v21, v0

    :goto_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmBox:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v21, :cond_30

    const/16 v22, 0x0

    :goto_1f
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v21, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f020121

    const v24, 0x7f020122

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_26
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_27

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetPlug:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetBox:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v21, :cond_31

    const/16 v22, 0x0

    :goto_20
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v21, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f02013c

    const v24, 0x7f02013d

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_27
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeUpdated:Z

    move/from16 v22, v0

    if-eqz v22, :cond_28

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietMode:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeBox:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    if-eqz v21, :cond_32

    const/16 v22, 0x0

    :goto_21
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v21, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f020182

    const v24, 0x7f020183

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_28
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_doResetAllUpdatedFlags(Z)V

    return-void

    :cond_29
    const/16 v22, 0x8

    goto/16 :goto_17

    :cond_2a
    const/4 v6, 0x0

    goto/16 :goto_18

    :cond_2b
    const v22, 0x7f020123

    const v24, 0x7f020133

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v22

    goto/16 :goto_19

    :cond_2c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryPercent:I

    move/from16 v22, v0

    goto/16 :goto_1a

    :cond_2d
    const/16 v21, 0x0

    goto/16 :goto_1b

    :cond_2e
    const/16 v22, 0x8

    goto/16 :goto_1c

    :pswitch_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f020184

    const v24, 0x7f020185

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1d

    :pswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeImageView:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f020186

    const v24, 0x7f020187

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1d

    :cond_2f
    const/16 v21, 0x1

    goto/16 :goto_1e

    :cond_30
    const/16 v22, 0x8

    goto/16 :goto_1f

    :cond_31
    const/16 v22, 0x8

    goto/16 :goto_20

    :cond_32
    const/16 v22, 0x8

    goto/16 :goto_21

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_f
        :pswitch_c
        :pswitch_b
        :pswitch_e
        :pswitch_d
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private hideReturnToRecorderView()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderLayer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private showReturnToRecorderView(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderLayer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private tryInvokeTelephonyShowInCallScreen(Landroid/content/Context;)V
    .locals 4

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->commonReceiversCallback:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onIncallScreenLeaveOrResume(Z)V

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.MAIN"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v2, 0x10840000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string/jumbo v2, "com.android.incallui"

    const-string/jumbo v3, "com.android.incallui.InCallActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method


# virtual methods
.method public hideReturnToInCallScreenView()V
    .locals 3

    const-string/jumbo v0, "InCallScreenView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "hide view, mCallState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    const v0, 0x7f0f00e0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v0, 0x7f0f0102

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    const v0, 0x7f0f0107

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderLayer:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f0100

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicRightPart:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f0105

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallRightPart:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f00f6

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f00f4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiBox:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v2, 0x7f0f00e6

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v2, 0x7f0f00ed

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v1, v3

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v2, 0x7f0f00eb

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v2, 0x7f0f00f2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v1, v3

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f00e4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f00e2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f00f8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f00fa

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f00fc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f00fe

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f0101

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockView:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00f7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00f5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrengthImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v4

    const v2, 0x7f0f00e7

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v3

    const v2, 0x7f0f00ee

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v3

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v4

    const v2, 0x7f0f00e8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v3

    const v2, 0x7f0f00ef

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v3

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v4

    const v2, 0x7f0f00e9

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v3

    const v2, 0x7f0f00f0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v3

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileMarkImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v4

    const v2, 0x7f0f00ea

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileMarkImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v3

    const v2, 0x7f0f00f1

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v3

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v4

    const v2, 0x7f0f00ec

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteImageViewArr:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    aget-object v0, v0, v3

    const v2, 0x7f0f00f3

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v3

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00e5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00e3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00f9

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f0106

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnIncall:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f0104

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00fb

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00fd

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00ff

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_quietModeImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f0109

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderLayer:Landroid/view/ViewGroup;

    const v1, 0x7f0f010b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnInRecorder:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_attachedToWindow:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockView:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->setSmallStyle(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnIncall:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->setSmallStyle(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnInRecorder:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->setSmallStyle(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    new-instance v1, Lcom/android/systemui/statusbar/OldmanStatusPane$4;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/OldmanStatusPane$4;-><init>(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderLayer:Landroid/view/ViewGroup;

    new-instance v1, Lcom/android/systemui/statusbar/OldmanStatusPane$5;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/OldmanStatusPane$5;-><init>(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_commonReceivers:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_commonReceivers:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_commonReceivers:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->commonReceiversCallback:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->addCallback(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;)V

    :cond_0
    new-instance v0, Lcom/android/systemui/statusbar/OldmanStatusPane$6;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/OldmanStatusPane$6;-><init>(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string/jumbo v0, ""

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->showReturnToInCallScreenView(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_commonReceivers:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->release()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_commonReceivers:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_attachedToWindow:Z

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderLayer:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicRightPart:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallRightPart:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothBox:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileBoxArr:[Landroid/view/ViewGroup;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteBoxArr:[Landroid/view/ViewGroup;

    aput-object v1, v0, v3

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneBox:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryBox:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeBox:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmBox:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetBox:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockView:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_bluetoothImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_wifiStrengthImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileMarkImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileStrengthImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileDataTypeImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileVolteImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileActivityImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_mobileMarkImageViewArr:[Landroid/widget/ImageView;

    aput-object v1, v0, v3

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_airplaneTextView:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_batteryImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_ringerModeImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnIncall:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallTextView:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_alarmImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_headsetImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_recorderTextView:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_clockViewOnInRecorder:Lcom/android/systemui/statusbar/policy/OldmanClockView;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public saveRecorderLayerState(ILjava/lang/String;)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mRecorderState:I

    iput-object p2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mRecorderTitle:Ljava/lang/String;

    return-void
.end method

.method public setStatus(ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.action_status_recorder"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->hideReturnToRecorderView()V

    goto :goto_0

    :pswitch_2
    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.extra_recorder_timer_on_off"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mRecorderState:I

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.extra_recorder_title"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->showReturnToRecorderView(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showReturnToInCallScreenView(Ljava/lang/String;)V
    .locals 4

    const-string/jumbo v1, "InCallScreenView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "show view : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->mCallState:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    return-void

    :cond_0
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    const v2, 0x7f0202e4

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallTextView:Landroid/widget/TextView;

    const v2, 0x7f090090

    const v3, 0x7f090091

    invoke-direct {p0, v2, v3}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_selectResId(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallTextView:Landroid/widget/TextView;

    const v2, 0x7f0d0281

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_basicLayer:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallLayer:Landroid/view/ViewGroup;

    const v2, 0x7f0202da

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallTextView:Landroid/widget/TextView;

    const v2, 0x7f0d0283

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallTextView:Landroid/widget/TextView;

    const v2, 0x7f0d0282

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_incallTextView:Landroid/widget/TextView;

    const v2, 0x7f0d0280

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public updateState(ZZZLandroid/graphics/Rect;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_inKeyguard:Z

    if-eq p1, v2, :cond_0

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_inKeyguardUpdated:Z

    const/4 v1, 0x1

    :cond_0
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundTransparent:Z

    if-eq p2, v2, :cond_1

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundUpdated:Z

    const/4 v1, 0x1

    :cond_1
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_darkMode:Z

    if-eq p3, v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    iget-object v2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_iconsDarkArea:Landroid/graphics/Rect;

    if-eq p4, v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    iput-boolean p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_inKeyguard:Z

    iput-boolean p2, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_backgroundTransparent:Z

    iput-boolean p3, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_darkMode:Z

    iput-object p4, p0, Lcom/android/systemui/statusbar/OldmanStatusPane;->m_iconsDarkArea:Landroid/graphics/Rect;

    if-eqz v0, :cond_4

    invoke-direct {p0, v3}, Lcom/android/systemui/statusbar/OldmanStatusPane;->_doResetAllUpdatedFlags(Z)V

    const/4 v1, 0x1

    :cond_4
    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->apply()V

    :cond_5
    return-void
.end method
