.class public interface abstract Lcom/android/systemui/statusbar/CommandQueue$Callbacks;
.super Ljava/lang/Object;
.source "CommandQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/CommandQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract addIcon(Ljava/lang/String;IILcom/android/systemui/statusbar/ExpandedIcon;)V
.end method

.method public abstract addNotification(Landroid/os/IBinder;Lcom/android/systemui/statusbar/ExpandedNotification;)V
.end method

.method public abstract addQsTile(Landroid/content/ComponentName;)V
.end method

.method public abstract animateCollapse(I)V
.end method

.method public abstract animateExpand()V
.end method

.method public abstract appTransitionFinished()V
.end method

.method public abstract appTransitionStarting(JJ)V
.end method

.method public abstract cancelPreloadRecentApps()V
.end method

.method public abstract clickTile(Landroid/content/ComponentName;)V
.end method

.method public abstract disable(I)V
.end method

.method public abstract hideRecentApps(ZZ)V
.end method

.method public abstract preloadRecentApps()V
.end method

.method public abstract remQsTile(Landroid/content/ComponentName;)V
.end method

.method public abstract removeIcon(Ljava/lang/String;II)V
.end method

.method public abstract removeNotification(Landroid/os/IBinder;)V
.end method

.method public abstract setHardKeyboardStatus(ZZ)V
.end method

.method public abstract setImeWindowStatus(Landroid/os/IBinder;II)V
.end method

.method public abstract setNavigationIconHints(I)V
.end method

.method public abstract setStatus(ILjava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract setSystemUiVisibility(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
.end method

.method public abstract setWindowState(II)V
.end method

.method public abstract showPictureInPictureMenu()V
.end method

.method public abstract showRecentApps(ZZ)V
.end method

.method public abstract showScreenPinningRequest()V
.end method

.method public abstract showScreenPinningRequest(I)V
.end method

.method public abstract startAssist(Landroid/os/Bundle;)V
.end method

.method public abstract toggleRecentApps()V
.end method

.method public abstract toggleSplitScreen()V
.end method

.method public abstract topAppWindowChanged(Z)V
.end method

.method public abstract updateIcon(Ljava/lang/String;IILcom/android/systemui/statusbar/ExpandedIcon;Lcom/android/systemui/statusbar/ExpandedIcon;)V
.end method

.method public abstract updateNotification(Landroid/os/IBinder;Lcom/android/systemui/statusbar/ExpandedNotification;)V
.end method
