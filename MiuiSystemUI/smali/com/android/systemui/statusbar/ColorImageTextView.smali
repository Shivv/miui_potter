.class public Lcom/android/systemui/statusbar/ColorImageTextView;
.super Lmiui/widget/ImageTextView;
.source "ColorImageTextView.java"


# instance fields
.field mColor:I

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiui/widget/ImageTextView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiui/widget/ImageTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/android/systemui/statusbar/ColorImageTextView;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public setColor(I)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/android/systemui/statusbar/ColorImageTextView;->mColor:I

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ColorImageTextView;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/ColorImageTextView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p1, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/widget/ImageTextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/android/systemui/statusbar/ColorImageTextView;->mColor:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ColorImageTextView;->setColor(I)V

    return-void
.end method
