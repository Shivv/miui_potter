.class Lcom/android/systemui/statusbar/NetworkSpeedView$1;
.super Landroid/database/ContentObserver;
.source "NetworkSpeedView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/NetworkSpeedView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/NetworkSpeedView;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$1;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$1;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$1;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/NetworkSpeedView;->-get0(Lcom/android/systemui/statusbar/NetworkSpeedView;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v1, v2}, Landroid/app/MiuiStatusBarManager;->isShowNetworkSpeedForUser(Landroid/content/Context;I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/NetworkSpeedView;->-set0(Lcom/android/systemui/statusbar/NetworkSpeedView;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$1;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$1;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/NetworkSpeedView;->-get0(Lcom/android/systemui/statusbar/NetworkSpeedView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "status_bar_network_speed_interval"

    const/16 v3, 0xfa0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/NetworkSpeedView;->-set2(Lcom/android/systemui/statusbar/NetworkSpeedView;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$1;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/NetworkSpeedView;->-wrap4(Lcom/android/systemui/statusbar/NetworkSpeedView;)V

    return-void
.end method
