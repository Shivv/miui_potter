.class Lcom/android/systemui/statusbar/InCallNotificationView$2;
.super Landroid/content/BroadcastReceiver;
.source "InCallNotificationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/InCallNotificationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/InCallNotificationView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/InCallNotificationView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$2;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const-string/jumbo v1, "state"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$2;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/InCallNotificationView;->-get1(Lcom/android/systemui/statusbar/InCallNotificationView;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$2;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/InCallNotificationView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->goInCallScreen()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$2;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/InCallNotificationView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->exitFloatingNotification(Z)V

    :cond_0
    return-void
.end method
