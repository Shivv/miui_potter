.class Lcom/android/systemui/statusbar/StatusBar$3;
.super Landroid/content/BroadcastReceiver;
.source "StatusBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/StatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/StatusBar;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/StatusBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    const/16 v8, 0x8

    const/4 v7, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, "StatusBar"

    const-string/jumbo v9, "onReceive AreaInfo"

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v6, "android.cellbroadcastreceiver.CB_AREA_INFO_RECEIVED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v6, v6, Lcom/android/systemui/statusbar/StatusBar;->mAreaInfo1:Landroid/widget/TextView;

    if-nez v6, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v6, v6, Lcom/android/systemui/statusbar/StatusBar;->mAreaInfo2:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    const-string/jumbo v6, "message"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/CellBroadcastMessage;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/telephony/CellBroadcastMessage;->getServiceCategory()I

    move-result v6

    const/16 v9, 0x32

    if-ne v6, v9, :cond_2

    invoke-virtual {v1}, Landroid/telephony/CellBroadcastMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Lcom/android/systemui/CompatibilityL;->getPhoneId(Landroid/telephony/CellBroadcastMessage;)I

    move-result v5

    const-string/jumbo v6, "br"

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v9

    invoke-virtual {v9}, Lmiui/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v6

    if-ne v5, v6, :cond_3

    iget-object v6, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v6, v6, Lcom/android/systemui/statusbar/StatusBar;->mAreaInfo1:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v6, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v9, v6, Lcom/android/systemui/statusbar/StatusBar;->mAreaInfo1:Landroid/widget/TextView;

    if-eqz v3, :cond_4

    iget-object v6, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v6, v6, Lcom/android/systemui/statusbar/StatusBar;->mAreaInfo1:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_4

    move v6, v7

    :goto_1
    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v6, v6, Lcom/android/systemui/statusbar/StatusBar;->mAreaInfo2:Landroid/widget/TextView;

    if-eqz v3, :cond_5

    iget-object v9, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v9, v9, Lcom/android/systemui/statusbar/StatusBar;->mAreaInfo2:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    if-eqz v9, :cond_5

    :goto_2
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    const-string/jumbo v6, "StatusBar"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v8

    invoke-virtual {v8}, Lmiui/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    iget-object v6, p0, Lcom/android/systemui/statusbar/StatusBar$3;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v6, v6, Lcom/android/systemui/statusbar/StatusBar;->mAreaInfo2:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    move v6, v8

    goto :goto_1

    :cond_5
    move v7, v8

    goto :goto_2
.end method
