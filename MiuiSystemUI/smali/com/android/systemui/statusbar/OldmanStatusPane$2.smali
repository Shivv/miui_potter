.class Lcom/android/systemui/statusbar/OldmanStatusPane$2;
.super Ljava/lang/Object;
.source "OldmanStatusPane.java"

# interfaces
.implements Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/OldmanStatusPane;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/OldmanStatusPane;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAlarmChanged(ZZ)V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v0, "IOldmanStatusPaneCommonCallback"

    const-string/jumbo v1, "onAlarmChanged(%s, %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set12(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set13(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, v5}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set2(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public onBatteryStateChanged(III)V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v0, "NetworkControllerGeminiOldman"

    const-string/jumbo v1, "onBatteryStateChanged(%d, %d, %d)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set4(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set5(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p3}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set3(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, v5}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set6(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public onBluetoothStateChanged(II)V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v0, "NetworkControllerGeminiOldman"

    const-string/jumbo v1, "onBluetoothStateChanged(%d, %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set8(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set7(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, v5}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set9(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public onClockTextChanged(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set10(Lcom/android/systemui/statusbar/OldmanStatusPane;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set11(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public onHeadsetChanged(ZZ)V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v0, "IOldmanStatusPaneCommonCallback"

    const-string/jumbo v1, "onHeadsetChanged(%s, %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set14(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set16(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, v5}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set15(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public onIncallScreenLeaveOrResume(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set17(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set18(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public onQuietModeChanged(Z)V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v0, "IOldmanStatusPaneCommonCallback"

    const-string/jumbo v1, "onQuietModeChanged(%s)"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set19(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, v5}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set20(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public onRingerModeChanged(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set21(Lcom/android/systemui/statusbar/OldmanStatusPane;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set22(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$2;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public onSyncStateChanged(ZZ)V
    .locals 0

    return-void
.end method
