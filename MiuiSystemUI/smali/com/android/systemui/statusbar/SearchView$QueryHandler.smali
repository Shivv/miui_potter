.class final Lcom/android/systemui/statusbar/SearchView$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "SearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/SearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "QueryHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/SearchView$QueryHandler$CatchingWorkerHandler;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/SearchView;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/SearchView;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/SearchView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected createHandler(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1

    new-instance v0, Lcom/android/systemui/statusbar/SearchView$QueryHandler$CatchingWorkerHandler;

    invoke-direct {v0, p0, p1}, Lcom/android/systemui/statusbar/SearchView$QueryHandler$CatchingWorkerHandler;-><init>(Lcom/android/systemui/statusbar/SearchView$QueryHandler;Landroid/os/Looper;)V

    return-object v0
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-static {v0, p3}, Lcom/android/systemui/statusbar/SearchView;->-wrap6(Lcom/android/systemui/statusbar/SearchView;Landroid/database/Cursor;)V

    invoke-static {}, Lcom/android/systemui/statusbar/SearchView;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "SearchView"

    const-string/jumbo v1, "search hint query complete."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
