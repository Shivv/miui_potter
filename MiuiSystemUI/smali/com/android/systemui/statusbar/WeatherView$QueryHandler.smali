.class final Lcom/android/systemui/statusbar/WeatherView$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "WeatherView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/WeatherView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "QueryHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/WeatherView$QueryHandler$CatchingWorkerHandler;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/WeatherView;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/WeatherView;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected createHandler(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1

    new-instance v0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler$CatchingWorkerHandler;

    invoke-direct {v0, p0, p1}, Lcom/android/systemui/statusbar/WeatherView$QueryHandler$CatchingWorkerHandler;-><init>(Lcom/android/systemui/statusbar/WeatherView$QueryHandler;Landroid/os/Looper;)V

    return-object v0
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v0, p3}, Lcom/android/systemui/statusbar/WeatherView;->-wrap8(Lcom/android/systemui/statusbar/WeatherView;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/WeatherView;->-wrap7(Lcom/android/systemui/statusbar/WeatherView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/WeatherView;->-wrap2(Lcom/android/systemui/statusbar/WeatherView;)V

    const-string/jumbo v0, "WeatherView"

    const-string/jumbo v1, "onQueryComplete mWeatherType=%d mTemperature=%s mTemperatureUnit=%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v3}, Lcom/android/systemui/statusbar/WeatherView;->-get4(Lcom/android/systemui/statusbar/WeatherView;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v3}, Lcom/android/systemui/statusbar/WeatherView;->-get2(Lcom/android/systemui/statusbar/WeatherView;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v3}, Lcom/android/systemui/statusbar/WeatherView;->-get3(Lcom/android/systemui/statusbar/WeatherView;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
