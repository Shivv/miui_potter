.class Lcom/android/systemui/statusbar/InCallNotificationView$4;
.super Ljava/lang/Object;
.source "InCallNotificationView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/InCallNotificationView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/InCallNotificationView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/InCallNotificationView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$4;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$4;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-static {v1, v4}, Lcom/android/systemui/statusbar/InCallNotificationView;->-set0(Lcom/android/systemui/statusbar/InCallNotificationView;Z)Z

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$4;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/InCallNotificationView;->-get3(Lcom/android/systemui/statusbar/InCallNotificationView;)Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManagerEx;->answerRingingCall()V

    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "systemUI.answer"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$4;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/InCallNotificationView;->-get0(Lcom/android/systemui/statusbar/InCallNotificationView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "gb_handsfree"

    const/4 v3, -0x2

    invoke-static {v1, v2, v5, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eq v1, v4, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$4;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/InCallNotificationView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->goInCallScreen(Landroid/os/Bundle;)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$4;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/InCallNotificationView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1, v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->exitFloatingNotification(Z)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$4;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/InCallNotificationView;->-get2(Lcom/android/systemui/statusbar/InCallNotificationView;)Lcom/miui/voiptalk/service/MiuiVoipManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/voiptalk/service/MiuiVoipManager;->answerRingingCall()V

    goto :goto_0
.end method
