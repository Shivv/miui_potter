.class Lcom/android/systemui/statusbar/WeatherView$3;
.super Landroid/content/BroadcastReceiver;
.source "WeatherView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/WeatherView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/WeatherView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/WeatherView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/WeatherView$3;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.miui.weather2"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView$3;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/systemui/statusbar/WeatherView;->-wrap0(Lcom/android/systemui/statusbar/WeatherView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView$3;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/WeatherView;->-wrap3(Lcom/android/systemui/statusbar/WeatherView;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView$3;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/WeatherView;->-wrap4(Lcom/android/systemui/statusbar/WeatherView;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView$3;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/systemui/statusbar/WeatherView;->-wrap0(Lcom/android/systemui/statusbar/WeatherView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView$3;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/WeatherView;->-wrap5(Lcom/android/systemui/statusbar/WeatherView;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView$3;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/WeatherView;->-wrap2(Lcom/android/systemui/statusbar/WeatherView;)V

    goto :goto_0
.end method
