.class public Lcom/android/systemui/statusbar/phone/FloatNotificationInfo;
.super Ljava/lang/Object;
.source "FloatNotificationInfo.java"


# instance fields
.field private mFloatTime:I

.field private mFloatWhen:J


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationInfo;->mFloatTime:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationInfo;->mFloatWhen:J

    return-void
.end method


# virtual methods
.method public getFloatTime()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationInfo;->mFloatTime:I

    return v0
.end method

.method public getFloatWhen()J
    .locals 2

    iget-wide v0, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationInfo;->mFloatWhen:J

    return-wide v0
.end method
