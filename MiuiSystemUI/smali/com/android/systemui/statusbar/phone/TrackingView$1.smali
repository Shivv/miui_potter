.class Lcom/android/systemui/statusbar/phone/TrackingView$1;
.super Ljava/lang/Object;
.source "TrackingView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/TrackingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/TrackingView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/TrackingView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/TrackingView;->-get0(Lcom/android/systemui/statusbar/phone/TrackingView;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavBarSize()I

    move-result v4

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginTop:I

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginBottom:I

    add-int/2addr v2, v4

    :goto_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->getHeight()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v3, Lcom/android/systemui/statusbar/phone/TrackingView;->mBottomHeight:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget v3, v3, Lcom/android/systemui/statusbar/phone/TrackingView;->mBottomHeight:I

    sub-int v0, v2, v3

    iget v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    if-eq v2, v0, :cond_0

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedVisible:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setY(F)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setClipBounds(Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/TrackingView;->-get0(Lcom/android/systemui/statusbar/phone/TrackingView;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getRealNavBarSize()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginBottom:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setY(F)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setTranslationY(F)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/TrackingView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginTop:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getRealNavBarSize()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView$1;->this$0:Lcom/android/systemui/statusbar/phone/TrackingView;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setX(F)V

    goto :goto_1
.end method
