.class Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;
.super Ljava/lang/Object;
.source "NotificationPanelView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getExpandTogglesButton()Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202e3

    invoke-virtual {v0, v1, v2}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->setBitmap(Landroid/content/res/Resources;I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getExpandTogglesButton()Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202e2

    invoke-virtual {v0, v1, v2}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->setBitmap(Landroid/content/res/Resources;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->-wrap0(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
