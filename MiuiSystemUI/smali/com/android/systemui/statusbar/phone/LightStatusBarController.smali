.class public Lcom/android/systemui/statusbar/phone/LightStatusBarController;
.super Ljava/lang/Object;
.source "LightStatusBarController.java"


# instance fields
.field private mDockedLight:Z

.field private mDockedStackVisibility:I

.field private mFullscreenLight:Z

.field private mFullscreenStackVisibility:I

.field private final mLastDockedBounds:Landroid/graphics/Rect;

.field private final mLastFullscreenBounds:Landroid/graphics/Rect;

.field private mLastStatusBarMode:I

.field public mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mLastFullscreenBounds:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mLastDockedBounds:Landroid/graphics/Rect;

    return-void
.end method

.method private isLight(II)Z
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x4

    if-eq p2, v4, :cond_0

    const/4 v4, 0x6

    if-ne p2, v4, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    and-int/lit16 v4, p1, 0x2000

    if-eqz v4, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v0, :cond_3

    :goto_2
    return v2

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method

.method private update(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    xor-int/lit8 v1, v3, 0x1

    :goto_0
    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mFullscreenLight:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mDockedLight:Z

    if-nez v3, :cond_1

    :cond_0
    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mFullscreenLight:Z

    if-eqz v3, :cond_3

    xor-int/lit8 v3, v1, 0x1

    if-eqz v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v3, v5, v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->setTargetDarkMode(ZLandroid/graphics/Rect;)V

    :goto_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mFullscreenLight:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mDockedLight:Z

    xor-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_5

    :cond_4
    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mFullscreenLight:Z

    if-nez v3, :cond_6

    xor-int/lit8 v3, v1, 0x1

    if-eqz v3, :cond_6

    :cond_5
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->setTargetDarkMode(ZLandroid/graphics/Rect;)V

    goto :goto_1

    :cond_6
    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mFullscreenLight:Z

    if-eqz v3, :cond_9

    move-object v0, p1

    :goto_2
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_7
    move-object v0, v2

    :cond_8
    invoke-virtual {v3, v5, v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->setTargetDarkMode(ZLandroid/graphics/Rect;)V

    goto :goto_1

    :cond_9
    move-object v0, p2

    goto :goto_2
.end method


# virtual methods
.method public onSystemUiVisibilityChanged(IIILandroid/graphics/Rect;Landroid/graphics/Rect;ZI)V
    .locals 8

    iget v5, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mFullscreenStackVisibility:I

    not-int v6, p3

    and-int/2addr v6, v5

    and-int v7, p1, p3

    or-int v3, v6, v7

    xor-int v1, v3, v5

    iget v4, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mDockedStackVisibility:I

    not-int v6, p3

    and-int/2addr v6, v4

    and-int v7, p2, p3

    or-int v2, v6, v7

    xor-int v0, v2, v4

    and-int/lit16 v6, v1, 0x2000

    if-nez v6, :cond_0

    and-int/lit16 v6, v0, 0x2000

    if-eqz v6, :cond_2

    :cond_0
    :goto_0
    invoke-direct {p0, v3, p7}, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->isLight(II)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mFullscreenLight:Z

    invoke-direct {p0, v2, p7}, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->isLight(II)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mDockedLight:Z

    invoke-direct {p0, p4, p5}, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->update(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    :cond_1
    iput v3, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mFullscreenStackVisibility:I

    iput v2, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mDockedStackVisibility:I

    iput p7, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mLastStatusBarMode:I

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mLastFullscreenBounds:Landroid/graphics/Rect;

    invoke-virtual {v6, p4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mLastDockedBounds:Landroid/graphics/Rect;

    invoke-virtual {v6, p5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    return-void

    :cond_2
    if-nez p6, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mLastFullscreenBounds:Landroid/graphics/Rect;

    invoke-virtual {v6, p4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/LightStatusBarController;->mLastDockedBounds:Landroid/graphics/Rect;

    invoke-virtual {v6, p5}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1

    goto :goto_0
.end method
