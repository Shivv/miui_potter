.class Lcom/android/systemui/statusbar/phone/PhoneStatusBar$60;
.super Ljava/lang/Object;
.source "PhoneStatusBar.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getEditModeChangeAnimator()Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$60;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$60;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->onEditModeAnimationUpdate(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$60;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedHeaderView:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->onEditModeAnimationUpdate(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$60;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mFlowStatusBar:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->onEditModeAnimationUpdate(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$60;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mNotificationHeader:Lcom/android/systemui/statusbar/phone/NotificationHeaderView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$60;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mNotificationHeader:Lcom/android/systemui/statusbar/phone/NotificationHeaderView;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->onEditModeAnimationUpdate(F)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$60;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-get21(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->onEditModeAnimationUpdate(F)V

    return-void
.end method
