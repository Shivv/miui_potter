.class Lcom/android/systemui/statusbar/phone/FlowStatusBar$4;
.super Landroid/database/ContentObserver;
.source "FlowStatusBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/FlowStatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/FlowStatusBar;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$4;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$4;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$4;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-get0(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v1, v2}, Landroid/app/MiuiStatusBarManager;->isShowFlowInfoForUser(Landroid/content/Context;I)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-set0(Lcom/android/systemui/statusbar/phone/FlowStatusBar;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$4;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->updateVisibility()V

    return-void
.end method
