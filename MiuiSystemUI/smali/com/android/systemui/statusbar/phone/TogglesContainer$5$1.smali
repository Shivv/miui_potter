.class Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;
.super Ljava/lang/Object;
.source "TogglesContainer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

.field final synthetic val$id:I

.field final synthetic val$imageView:Landroid/widget/ImageView;

.field final synthetic val$isOpen:Z


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/TogglesContainer$5;IZLandroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->this$1:Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

    iput p2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    iput-boolean p3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$isOpen:Z

    iput-object p4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$imageView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    invoke-static {v0}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->this$1:Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v1, v0, :cond_1

    const-string/jumbo v0, "SystemUI.toggleContainer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateToggleImage again for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    invoke-static {v2}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " last:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->this$1:Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " isOpen:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$isOpen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->this$1:Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$imageView:Landroid/widget/ImageView;

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    iget v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    invoke-static {v3}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->-wrap0(Lcom/android/systemui/statusbar/phone/TogglesContainer;Landroid/widget/ImageView;IZ)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->this$1:Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget v1, v1, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastClickToggle:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->this$1:Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mNeedCollapseMap:Ljava/util/HashMap;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->val$id:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;->this$1:Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->animateCollapse()V

    goto :goto_0
.end method
