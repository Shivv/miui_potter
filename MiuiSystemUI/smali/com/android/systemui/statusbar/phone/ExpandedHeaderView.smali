.class public Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;
.super Landroid/widget/LinearLayout;
.source "ExpandedHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;,
        Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$2;,
        Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;
    }
.end annotation


# static fields
.field private static sDuration:I

.field private static sMaxRadius:I

.field private static sNotch:Z

.field private static sOrientation:I

.field private static sPx:I

.field private static sPy:I

.field private static sStartRadius:I


# instance fields
.field private mColorChangeListener:Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

.field private mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

.field private mEditMode:Landroid/widget/ImageView;

.field private mExpandableTogglesButton:Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

.field private mHeight:I

.field private mInit:Z

.field private mIsEditMode:Z

.field private mNotificationBlocker:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

.field private mPaint:Landroid/graphics/Paint;

.field private mQsDetailHeader:Landroid/widget/TextView;

.field private final mQsPanelCallback:Lcom/android/systemui/statusbar/phone/PanelView$Callback;

.field private mRadius:F

.field private mSearch:Lcom/android/systemui/statusbar/SearchView;

.field mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mSettingsButton:Landroid/view/View;

.field private mShowSearch:Z

.field private mShowingDetail:Z

.field mUpdateBackground:Ljava/lang/Runnable;

.field private mWeatherView:Lcom/android/systemui/statusbar/WeatherView;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsDetailHeader:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)Lcom/android/systemui/statusbar/SearchView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)Lcom/android/systemui/statusbar/WeatherView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;Lcom/android/systemui/statusbar/ColorSuits;)Lcom/android/systemui/statusbar/ColorSuits;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;F)F
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mRadius:F

    return p1
.end method

.method static synthetic -set2(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mShowingDetail:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setBackground()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setWeatherViewTopMargin(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x320

    sput v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sDuration:I

    const/4 v0, 0x1

    sput v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sOrientation:I

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sNotch:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->DEFAULT:Lcom/android/systemui/statusbar/ColorSuits;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mRadius:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mInit:Z

    new-instance v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorChangeListener:Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

    new-instance v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$2;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mUpdateBackground:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsPanelCallback:Lcom/android/systemui/statusbar/phone/PanelView$Callback;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sPx:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sPy:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sStartRadius:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sMaxRadius:I

    return-void
.end method

.method private setBackground()V
    .locals 6

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v4

    sget v5, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sOrientation:I

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/ColorSuits;->getBgDrawable(Landroid/content/Context;Landroid/graphics/drawable/Drawable;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private setSettingsButtonLeftMargin()V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSettingsButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0165

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSettingsButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private setWeatherViewTopMargin(I)V
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/WeatherView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/WeatherView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private transition(Landroid/view/View;Z)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->hasOverlappingRendering()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$7;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$7;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method


# virtual methods
.method public getExpandTogglesButton()Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mExpandableTogglesButton:Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    return-object v0
.end method

.method public handleShowingFold(Z)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    xor-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->transition(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsDetailHeader:Landroid/widget/TextView;

    invoke-direct {p0, v0, p1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->transition(Landroid/view/View;Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsDetailHeader:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d033f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsDetailHeader:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f0d030f

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0, v3, v4}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateBackground(ZZ)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v3, v3}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateBackground(ZZ)V

    goto :goto_0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hideWeatherView(F)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->setAlpha(F)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSettingsButton:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sTogglesInListStyle:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mExpandableTogglesButton:Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sTogglesInListStyle:Z

    if-eqz v2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorChangeListener:Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->registerColorChangeListener(Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    const/4 v1, 0x4

    goto :goto_1
.end method

.method public onChangeToSearchEnd()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mNotificationBlocker:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->setAlpha(F)V

    return-void
.end method

.method public onChangeToSearchStart()V
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mHeight:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/SearchView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b007b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int v1, v2, v3

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    new-array v2, v9, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mNotificationBlocker:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    const-string/jumbo v4, "alpha"

    new-array v5, v9, [F

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v5, v7

    const/4 v6, 0x0

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-wide/16 v4, 0x64

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v7

    const-string/jumbo v3, "height"

    new-array v4, v9, [I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v5

    aput v5, v4, v7

    aput v1, v4, v8

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v3

    sget v4, Lcom/android/systemui/statusbar/SearchView;->sDuartion:I

    add-int/lit16 v4, v4, -0xc8

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->reorient()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateResource()V

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorChangeListener:Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->unregisterColorChangeListener(Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/ColorSuits;->clearCache()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mShowingDetail:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sPx:I

    int-to-float v0, v0

    sget v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sPy:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mRadius:F

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public onEditModeAnimationEnd(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mIsEditMode:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mIsEditMode:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->setEnabled(Z)V

    return-void
.end method

.method public onEditModeAnimationStart(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mIsEditMode:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mIsEditMode:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/SearchView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mIsEditMode:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->setEnabled(Z)V

    return-void
.end method

.method public onEditModeAnimationUpdate(F)V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    sub-float v1, v2, p1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/SearchView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    sub-float v1, v2, p1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsDetailHeader:Landroid/widget/TextView;

    sub-float v1, v2, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sNotch:Z

    if-eqz v2, :cond_2

    const v2, 0x7f030029

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->addView(Landroid/view/View;)V

    const v2, 0x7f0f0035

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/WeatherView;

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    const v2, 0x7f0f0032

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/SearchView;

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    const v2, 0x7f0f0192

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mNotificationBlocker:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mShowSearch:Z

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mShowSearch:Z

    :cond_0
    const v2, 0x7f0f0039

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSettingsButton:Landroid/view/View;

    const v2, 0x7f0f003a

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mExpandableTogglesButton:Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    const v2, 0x7f0f003b

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsDetailHeader:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsDetailHeader:Landroid/widget/TextView;

    new-instance v3, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$4;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$4;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mPaint:Landroid/graphics/Paint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    sput v2, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sOrientation:I

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sNotch:Z

    if-eqz v2, :cond_1

    const v2, 0x7f0f01b1

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mEditMode:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mEditMode:Landroid/widget/ImageView;

    new-instance v3, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$5;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$5;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mNotificationBlocker:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mEditMode:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->addEnterModeView(Landroid/view/View;)V

    :cond_1
    return-void

    :cond_2
    const v2, 0x7f030006

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/systemui/statusbar/phone/PanelView;->mIsDetailOpened:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v1

    iget-boolean v0, v1, Lcom/android/systemui/statusbar/phone/PanelView;->mIsFoldOpened:Z

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateBackground(ZZ)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public reorient()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    sput v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sOrientation:I

    return-void
.end method

.method public selectTab(I)V
    .locals 10

    const-wide/16 v8, 0xc8

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mNotificationBlocker:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mNotificationBlocker:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    const-string/jumbo v5, "transitionAlpha"

    new-array v6, v2, [F

    if-eqz v0, :cond_2

    move v1, v2

    :goto_1
    int-to-float v1, v1

    aput v1, v6, v3

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mNotificationBlocker:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->getEnterEditModeView()Landroid/view/View;

    move-result-object v1

    const-string/jumbo v4, "transitionAlpha"

    new-array v5, v2, [F

    if-eqz v0, :cond_3

    :goto_2
    int-to-float v2, v2

    aput v2, v5, v3

    invoke-static {v1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method

.method public setPanelView(Lcom/android/systemui/statusbar/phone/PanelView;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mQsPanelCallback:Lcom/android/systemui/statusbar/phone/PanelView$Callback;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/phone/PanelView;->setCallback(Lcom/android/systemui/statusbar/phone/PanelView$Callback;)V

    :cond_0
    return-void
.end method

.method public setShowConnectSuccess()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateBackground(ZZ)V

    const/4 v1, 0x2

    new-array v1, v1, [I

    sget v2, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sStartRadius:I

    aput v2, v1, v3

    sget v2, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sMaxRadius:I

    aput v2, v1, v4

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    sget v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    new-instance v1, Lmiui/maml/animation/interpolater/SineEaseOutInterpolater;

    invoke-direct {v1}, Lmiui/maml/animation/interpolater/SineEaseOutInterpolater;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$8;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$8;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$9;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$9;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method public themeCompatibility(Z)V
    .locals 5

    const v4, 0xffffff

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f090009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    and-int v3, v0, v4

    if-ne v3, v4, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    if-eqz v3, :cond_0

    xor-int/lit8 v3, v1, 0x1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    invoke-virtual {v3, v0}, Lcom/android/systemui/statusbar/SearchView;->setColor(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    invoke-virtual {v3, v0}, Lcom/android/systemui/statusbar/WeatherView;->setColor(I)V

    :cond_0
    return-void

    :cond_1
    const v3, 0x7f090008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method updateBackground(ZZ)V
    .locals 7

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mUpdateBackground:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->removeCallbacks(Ljava/lang/Runnable;)Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->hideWeatherView(F)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mRadius:F

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f02005d

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_4

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mInit:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mUpdateBackground:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->post(Ljava/lang/Runnable;)Z

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mInit:Z

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mUpdateBackground:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setBackground()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/ColorSuits;->enableImageColorDye(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/ColorSuits;->getBgColor()I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v5

    sget v6, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sOrientation:I

    const v3, 0x7f02011c

    invoke-virtual/range {v0 .. v6}, Lcom/android/systemui/statusbar/ColorSuits;->getThemeBgDrawable(Landroid/content/Context;Landroid/graphics/drawable/Drawable;IIII)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public updateResource()V
    .locals 7

    const v6, 0x7f0b007c

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mShowSearch:Z

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v2

    if-eqz v2, :cond_0

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mShowSearch:Z

    :cond_0
    invoke-virtual {p0, v4}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sNotch:Z

    if-eqz v2, :cond_2

    sget v2, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->sOrientation:I

    if-ne v2, v5, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0078

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    :goto_0
    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setSettingsButtonLeftMargin()V

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/systemui/statusbar/phone/PanelView;->mIsDetailOpened:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v2

    iget-boolean v1, v2, Lcom/android/systemui/statusbar/phone/PanelView;->mIsFoldOpened:Z

    :goto_2
    invoke-virtual {p0, v5, v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateBackground(ZZ)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    invoke-virtual {v2, v4}, Lcom/android/systemui/statusbar/SearchView;->setBackgroundResource(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    const v3, 0x7f020066

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/SearchView;->setBackgroundResource(I)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mShowSearch:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    invoke-virtual {v2, v4}, Lcom/android/systemui/statusbar/SearchView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0077

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    new-instance v3, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$6;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$6;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/SearchView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mSearch:Lcom/android/systemui/statusbar/SearchView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/SearchView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-direct {p0, v4}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setWeatherViewTopMargin(I)V

    goto :goto_1

    :cond_4
    const/4 v1, 0x1

    goto :goto_2
.end method
