.class Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$9;
.super Ljava/lang/Object;
.source "ExpandedHeaderView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setShowConnectSuccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$9;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$9;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/phone/PanelView;->mIsDetailOpened:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$9;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->-set1(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;F)F

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$9;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$9;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->-get0(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method
