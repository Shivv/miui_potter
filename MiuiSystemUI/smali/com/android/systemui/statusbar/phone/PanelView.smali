.class public abstract Lcom/android/systemui/statusbar/phone/PanelView;
.super Landroid/widget/FrameLayout;
.source "PanelView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/PanelView$1;,
        Lcom/android/systemui/statusbar/phone/PanelView$2;,
        Lcom/android/systemui/statusbar/phone/PanelView$Callback;,
        Lcom/android/systemui/statusbar/phone/PanelView$H;,
        Lcom/android/systemui/statusbar/phone/PanelView$Record;,
        Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;
    }
.end annotation


# instance fields
.field private mAttachedFromWindow:Z

.field private mCallback:Lcom/android/systemui/statusbar/phone/PanelView$Callback;

.field private mClipper:Lcom/android/systemui/qs/QSDetailClipper;

.field private mDetail:Landroid/view/View;

.field private mDetailContent:Landroid/view/ViewGroup;

.field private mDetailDoneButton:Landroid/widget/TextView;

.field private mDetailRecord:Lcom/android/systemui/statusbar/phone/PanelView$Record;

.field private mDetailSettingsButton:Landroid/widget/TextView;

.field private final mHandler:Lcom/android/systemui/statusbar/phone/PanelView$H;

.field public mIsDetailOpened:Z

.field public mIsFoldOpened:Z

.field protected mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private final mShowCompleteListener:Landroid/animation/AnimatorListenerAdapter;

.field private final mTeardownDetailWhenDone:Landroid/animation/AnimatorListenerAdapter;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/PanelView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/PanelView;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailContent:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/PanelView;Lcom/android/systemui/statusbar/phone/PanelView$Record;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/statusbar/phone/PanelView;->handleShowDetail(Lcom/android/systemui/statusbar/phone/PanelView$Record;ZZ)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/phone/PanelView;Lcom/android/systemui/statusbar/phone/PanelView$Record;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->setDetailRecord(Lcom/android/systemui/statusbar/phone/PanelView$Record;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/systemui/statusbar/phone/PanelView$H;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/phone/PanelView$H;-><init>(Lcom/android/systemui/statusbar/phone/PanelView;Lcom/android/systemui/statusbar/phone/PanelView$H;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mHandler:Lcom/android/systemui/statusbar/phone/PanelView$H;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mAttachedFromWindow:Z

    new-instance v0, Lcom/android/systemui/statusbar/phone/PanelView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/PanelView$1;-><init>(Lcom/android/systemui/statusbar/phone/PanelView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mShowCompleteListener:Landroid/animation/AnimatorListenerAdapter;

    new-instance v0, Lcom/android/systemui/statusbar/phone/PanelView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/PanelView$2;-><init>(Lcom/android/systemui/statusbar/phone/PanelView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mTeardownDetailWhenDone:Landroid/animation/AnimatorListenerAdapter;

    return-void
.end method

.method private fireShowingDetail(Lcom/android/systemui/qs/QSTile$DetailAdapter;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mCallback:Lcom/android/systemui/statusbar/phone/PanelView$Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mCallback:Lcom/android/systemui/statusbar/phone/PanelView$Callback;

    invoke-interface {v0, p1}, Lcom/android/systemui/statusbar/phone/PanelView$Callback;->onShowingDetail(Lcom/android/systemui/qs/QSTile$DetailAdapter;)V

    :cond_0
    return-void
.end method

.method private handleShowDetail(Lcom/android/systemui/statusbar/phone/PanelView$Record;ZZ)V
    .locals 6

    instance-of v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/statusbar/phone/PanelView;->handleShowDetailTile(Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;ZZ)V

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz p1, :cond_1

    iget v3, p1, Lcom/android/systemui/statusbar/phone/PanelView$Record;->x:I

    iget v4, p1, Lcom/android/systemui/statusbar/phone/PanelView$Record;->y:I

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/systemui/statusbar/phone/PanelView;->handleShowDetailImpl(Lcom/android/systemui/statusbar/phone/PanelView$Record;ZIIZ)V

    goto :goto_0
.end method

.method private handleShowDetailImpl(Lcom/android/systemui/statusbar/phone/PanelView$Record;ZIIZ)V
    .locals 10

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailRecord:Lcom/android/systemui/statusbar/phone/PanelView$Record;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eq v0, p2, :cond_1

    const/4 v9, 0x1

    :goto_1
    if-nez v9, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailRecord:Lcom/android/systemui/statusbar/phone/PanelView$Record;

    if-ne v0, p1, :cond_2

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    const/4 v4, 0x0

    if-eqz p2, :cond_7

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mIsDetailOpened:Z

    iget-object v6, p1, Lcom/android/systemui/statusbar/phone/PanelView$Record;->detailAdapter:Lcom/android/systemui/qs/QSTile$DetailAdapter;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mContext:Landroid/content/Context;

    iget-object v1, p1, Lcom/android/systemui/statusbar/phone/PanelView$Record;->detailView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailContent:Landroid/view/ViewGroup;

    invoke-interface {v6, v0, v1, v2}, Lcom/android/systemui/qs/QSTile$DetailAdapter;->createDetailView(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$Record;->detailView:Landroid/view/View;

    iget-object v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$Record;->detailView:Landroid/view/View;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must return detail view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-interface {v6}, Lcom/android/systemui/qs/QSTile$DetailAdapter;->getSettingsIntent()Landroid/content/Intent;

    move-result-object v7

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailSettingsButton:Landroid/widget/TextView;

    if-eqz v7, :cond_6

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailSettingsButton:Landroid/widget/TextView;

    new-instance v1, Lcom/android/systemui/statusbar/phone/PanelView$4;

    invoke-direct {v1, p0, v7}, Lcom/android/systemui/statusbar/phone/PanelView$4;-><init>(Lcom/android/systemui/statusbar/phone/PanelView;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {v6}, Lcom/android/systemui/qs/QSTile$DetailAdapter;->getTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    const v2, 0x7f0d030f

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/PanelView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetail:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailContent:Landroid/view/ViewGroup;

    iget-object v1, p1, Lcom/android/systemui/statusbar/phone/PanelView$Record;->detailView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->setDetailRecord(Lcom/android/systemui/statusbar/phone/PanelView$Record;)V

    instance-of v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    if-eqz v0, :cond_4

    if-eqz v9, :cond_4

    check-cast p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->openingDetail:Z

    :cond_4
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mShowCompleteListener:Landroid/animation/AnimatorListenerAdapter;

    :goto_3
    if-eqz p2, :cond_8

    :goto_4
    invoke-direct {p0, v6}, Lcom/android/systemui/statusbar/phone/PanelView;->fireShowingDetail(Lcom/android/systemui/qs/QSTile$DetailAdapter;)V

    if-eqz v9, :cond_5

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mAttachedFromWindow:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mClipper:Lcom/android/systemui/qs/QSDetailClipper;

    move v1, p3

    move v2, p4

    move v3, p2

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/qs/QSDetailClipper;->animateCircularClip(IIZLandroid/animation/Animator$AnimatorListener;Z)V

    :cond_5
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->resizeTileItems()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedHeaderView()Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateResource()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0, p2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->showTileDetail(Z)V

    return-void

    :cond_6
    const/16 v0, 0x8

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mIsDetailOpened:Z

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mTeardownDetailWhenDone:Landroid/animation/AnimatorListenerAdapter;

    goto :goto_3

    :cond_8
    const/4 v6, 0x0

    goto :goto_4
.end method

.method private handleShowDetailTile(Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;ZZ)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailRecord:Lcom/android/systemui/statusbar/phone/PanelView$Record;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-ne v0, p2, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailRecord:Lcom/android/systemui/statusbar/phone/PanelView$Record;

    if-ne v0, p1, :cond_1

    return-void

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    invoke-virtual {v0}, Lcom/android/systemui/qs/QSTile;->getDetailAdapter()Lcom/android/systemui/qs/QSTile$DetailAdapter;

    move-result-object v0

    iput-object v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->detailAdapter:Lcom/android/systemui/qs/QSTile$DetailAdapter;

    iget-object v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->detailAdapter:Lcom/android/systemui/qs/QSTile$DetailAdapter;

    if-nez v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    invoke-virtual {v0, p2}, Lcom/android/systemui/qs/QSTile;->setDetailListening(Z)V

    iget-object v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v1, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    iget-object v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v1, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v4, v0, v1

    iget v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->x:I

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->y:I

    if-eqz v0, :cond_3

    iget v3, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->x:I

    iget v4, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->y:I

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/systemui/statusbar/phone/PanelView;->handleShowDetailImpl(Lcom/android/systemui/statusbar/phone/PanelView$Record;ZIIZ)V

    return-void
.end method

.method private setDetailRecord(Lcom/android/systemui/statusbar/phone/PanelView$Record;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailRecord:Lcom/android/systemui/statusbar/phone/PanelView$Record;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailRecord:Lcom/android/systemui/statusbar/phone/PanelView$Record;

    return-void
.end method

.method private updateDetailText()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailDoneButton:Landroid/widget/TextView;

    const v1, 0x7f0d022b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailSettingsButton:Landroid/widget/TextView;

    const v1, 0x7f0d032e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateResources()V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->updateDetailText()V

    return-void
.end method


# virtual methods
.method public closeDetail()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/PanelView;->closeDetail(Z)V

    return-void
.end method

.method public closeDetail(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailRecord:Lcom/android/systemui/statusbar/phone/PanelView$Record;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$Record;Z)V

    return-void
.end method

.method public getNotificationContainer()Landroid/view/ViewGroup;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public makeExpandedVisible(Z)V
    .locals 0

    return-void
.end method

.method public markAllSeen(Lcom/android/systemui/statusbar/NotificationData;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mAttachedFromWindow:Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mAttachedFromWindow:Z

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f019b

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/PanelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetail:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetail:Landroid/view/View;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailContent:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetail:Landroid/view/View;

    const v1, 0x102001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailSettingsButton:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetail:Landroid/view/View;

    const v1, 0x1020019

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailDoneButton:Landroid/widget/TextView;

    new-instance v0, Lcom/android/systemui/qs/QSDetailClipper;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetail:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/android/systemui/qs/QSDetailClipper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mClipper:Lcom/android/systemui/qs/QSDetailClipper;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->updateResources()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mDetailDoneButton:Landroid/widget/TextView;

    new-instance v1, Lcom/android/systemui/statusbar/phone/PanelView$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/PanelView$3;-><init>(Lcom/android/systemui/statusbar/phone/PanelView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public performCollapse()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/PanelView;->closeDetail(Z)V

    return-void
.end method

.method public performExpand()V
    .locals 0

    return-void
.end method

.method public prepareTracking(IZ)V
    .locals 0

    return-void
.end method

.method public resetFoldState()V
    .locals 0

    return-void
.end method

.method public resetResWhenOrientationChanged()V
    .locals 0

    return-void
.end method

.method public resizeTileItems()V
    .locals 0

    return-void
.end method

.method public setCallback(Lcom/android/systemui/statusbar/phone/PanelView$Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mCallback:Lcom/android/systemui/statusbar/phone/PanelView$Callback;

    return-void
.end method

.method public setFoldState()V
    .locals 0

    return-void
.end method

.method public setService(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    return-void
.end method

.method public showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$Record;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/systemui/statusbar/phone/PanelView;->showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$Record;Z)V

    return-void
.end method

.method public showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$Record;Z)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/PanelView;->mHandler:Lcom/android/systemui/statusbar/phone/PanelView$H;

    if-eqz p1, :cond_1

    move v2, v1

    :goto_0
    if-eqz p3, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v3, v1, v2, v0, p2}, Lcom/android/systemui/statusbar/phone/PanelView$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_1
    move v2, v0

    goto :goto_0
.end method

.method public stopTracking()V
    .locals 0

    return-void
.end method

.method public updateExpandedViewHeight(I)V
    .locals 0

    return-void
.end method
