.class Lcom/android/systemui/statusbar/phone/TogglesContainer$3;
.super Ljava/lang/Object;
.source "TogglesContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/TogglesContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$3;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    const v2, 0x7f0f001c

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    if-eqz v1, :cond_0

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$3;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget v2, v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->toggleId:I

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleView(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$3;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    invoke-virtual {v1, v0, v2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->setRecordCoordinate(Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$3;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$TileRecord;)V

    iget v1, v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->toggleId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/systemui/AnalyticsHelper;->trackTogglesSubpageClick(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
