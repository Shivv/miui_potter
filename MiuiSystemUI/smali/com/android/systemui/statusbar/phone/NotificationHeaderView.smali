.class public Lcom/android/systemui/statusbar/phone/NotificationHeaderView;
.super Landroid/widget/FrameLayout;
.source "NotificationHeaderView.java"


# instance fields
.field mBlockerContainer:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

.field mDisableNotificationContent:Landroid/widget/TextView;

.field mDisableNotificationDesContainer:Landroid/view/View;

.field mDisableNotificationTitle:Landroid/widget/TextView;

.field mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public changeState(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mBlockerContainer:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->changeState(I)V

    return-void
.end method

.method public onEditModeAnimationEnd(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mDisableNotificationDesContainer:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public onEditModeAnimationStart(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mDisableNotificationDesContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onEditModeAnimationUpdate(F)V
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    mul-float v2, v5, p1

    sub-float v0, v2, v4

    cmpg-float v2, v0, v3

    if-gez v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    mul-float v2, v5, p1

    sub-float v1, v4, v2

    cmpg-float v2, v1, v3

    if-gez v2, :cond_1

    const/4 v1, 0x0

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mDisableNotificationDesContainer:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method public onEnterConfirmMode(I)V
    .locals 5

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f110004

    invoke-virtual {v1, v3, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mDisableNotificationTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->changeState(I)V

    return-void
.end method

.method public onExitConfirmMode(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mDisableNotificationTitle:Landroid/widget/TextView;

    const v1, 0x7f0d02c3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    if-eqz p1, :cond_0

    sget v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->EXIT_EDIT_MODE_STATE:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->changeState(I)V

    return-void

    :cond_0
    sget v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->ENTER_EDIT_MODE_STATE:I

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f018f

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mDisableNotificationDesContainer:Landroid/view/View;

    const v0, 0x7f0f0190

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mDisableNotificationTitle:Landroid/widget/TextView;

    const v0, 0x7f0f0191

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mDisableNotificationContent:Landroid/widget/TextView;

    const v0, 0x7f0f0192

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationHeaderView;->mBlockerContainer:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    return-void
.end method

.method public themeCompatibility(Z)V
    .locals 0

    return-void
.end method
