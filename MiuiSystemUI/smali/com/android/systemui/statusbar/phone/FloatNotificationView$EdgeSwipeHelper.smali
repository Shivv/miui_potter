.class Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;
.super Ljava/lang/Object;
.source "FloatNotificationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/FloatNotificationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EdgeSwipeHelper"
.end annotation


# instance fields
.field private mConsuming:Z

.field private mFirstX:F

.field private mFirstY:F

.field private final mTouchSlop:F

.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/FloatNotificationView;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/phone/FloatNotificationView;F)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->this$0:Lcom/android/systemui/statusbar/phone/FloatNotificationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mTouchSlop:F

    return-void
.end method

.method private move(Landroid/view/MotionEvent;)V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mConsuming:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mFirstX:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mFirstY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float v1, v2, v3

    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    iget v2, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mTouchSlop:F

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->this$0:Lcom/android/systemui/statusbar/phone/FloatNotificationView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2, v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->exitFloatingAndUpdateInCall(Z)V

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mConsuming:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mConsuming:Z

    return v0

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mFirstX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mFirstY:F

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mConsuming:Z

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->move(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mConsuming:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->move(Landroid/view/MotionEvent;)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->mConsuming:Z

    return v0
.end method
