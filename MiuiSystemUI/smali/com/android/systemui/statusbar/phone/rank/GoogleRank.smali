.class public Lcom/android/systemui/statusbar/phone/rank/GoogleRank;
.super Ljava/lang/Object;
.source "GoogleRank.java"

# interfaces
.implements Lcom/android/systemui/statusbar/phone/rank/IRank;


# static fields
.field private static final SUPPORT_HIGH_PRIORITY:Z


# instance fields
.field private mHighPriorityMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;->SUPPORT_HIGH_PRIORITY:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;->mHighPriorityMap:Ljava/util/HashMap;

    return-void
.end method

.method private static isUidSystem(I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v3, 0x3e8

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3e9

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p0, :cond_0

    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public compare(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I
    .locals 16

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v8}, Lcom/android/systemui/statusbar/ExpandedNotification;->getScore()I

    move-result v12

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/ExpandedNotification;->getScore()I

    move-result v13

    sub-int v5, v12, v13

    if-eqz v5, :cond_0

    return v5

    :cond_0
    invoke-virtual {v8}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v12

    iget v12, v12, Landroid/app/Notification;->priority:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_3

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v12

    iget v12, v12, Landroid/app/Notification;->priority:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_3

    invoke-virtual {v8}, Lcom/android/systemui/statusbar/ExpandedNotification;->getUid()I

    move-result v12

    invoke-static {v12}, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;->isUidSystem(I)Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v9}, Lcom/android/systemui/statusbar/ExpandedNotification;->getUid()I

    move-result v12

    invoke-static {v12}, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;->isUidSystem(I)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v3, 0x1

    :goto_1
    sub-int v12, v2, v3

    if-eqz v12, :cond_3

    sub-int v12, v2, v3

    return v12

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v8}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v12

    iget-wide v12, v12, Landroid/app/Notification;->when:J

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v14

    iget-wide v14, v14, Landroid/app/Notification;->when:J

    sub-long v10, v12, v14

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->firstWhen:J

    move-object/from16 v0, p2

    iget-wide v14, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->firstWhen:J

    sub-long v6, v12, v14

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    const-wide/16 v14, 0x2710

    cmp-long v12, v12, v14

    if-lez v12, :cond_5

    const-wide/16 v12, 0x0

    cmp-long v12, v10, v12

    if-lez v12, :cond_4

    const/4 v4, 0x1

    :goto_2
    return v4

    :cond_4
    const/4 v4, -0x1

    goto :goto_2

    :cond_5
    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-eqz v12, :cond_7

    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-lez v12, :cond_6

    const/4 v4, 0x1

    goto :goto_2

    :cond_6
    const/4 v4, -0x1

    goto :goto_2

    :cond_7
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->key:Landroid/os/IBinder;

    invoke-virtual {v12}, Ljava/lang/Object;->hashCode()I

    move-result v12

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->key:Landroid/os/IBinder;

    invoke-virtual {v13}, Ljava/lang/Object;->hashCode()I

    move-result v13

    sub-int v4, v12, v13

    goto :goto_2
.end method

.method public getPercent()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
