.class public Lcom/android/systemui/statusbar/phone/rank/RankLruCache;
.super Landroid/util/LruCache;
.source "RankLruCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/util/LruCache",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "RankLruCache"

    sput-object v0, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method public entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTK;TV;TV;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    instance-of v1, p3, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    if-eqz v1, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->shouldDelete()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/android/systemui/statusbar/NotificationSortHelper;->asyncDeleteDataMonthAgo(Lcom/android/systemui/statusbar/phone/rank/PackageEntity;)V

    :cond_0
    sget-object v1, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "make space"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "put elements"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
