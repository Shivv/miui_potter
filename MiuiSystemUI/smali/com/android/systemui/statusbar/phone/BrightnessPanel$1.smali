.class Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;
.super Ljava/lang/Object;
.source "BrightnessPanel.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/BrightnessPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get0(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get0(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setValue(I)V

    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get3(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lmiui/app/ToggleManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lmiui/app/ToggleManager;->applyBrightness(IZ)V

    :cond_1
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get0(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get0(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get2(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Landroid/widget/SeekBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setPressed(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get1(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get1(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->showMirror(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get3(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lmiui/app/ToggleManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/app/ToggleManager;->isBrightnessAutoMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get1(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->stopObserverBrightness()V

    :cond_1
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get0(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get0(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get2(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Landroid/widget/SeekBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setPressed(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get1(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get1(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->hideMirror()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get3(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lmiui/app/ToggleManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/app/ToggleManager;->isBrightnessAutoMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get1(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->startObserverBrightness(J)V

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;->this$0:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->-get3(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lmiui/app/ToggleManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lmiui/app/ToggleManager;->applyBrightness(IZ)V

    return-void
.end method
