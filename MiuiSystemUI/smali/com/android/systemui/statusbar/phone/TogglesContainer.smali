.class public Lcom/android/systemui/statusbar/phone/TogglesContainer;
.super Lcom/android/systemui/statusbar/phone/PanelView;
.source "TogglesContainer.java"

# interfaces
.implements Lcom/android/systemui/qs/QSTile$Host$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lmiui/app/ToggleManager$OnToggleChangedListener;
.implements Lmiui/app/ToggleManager$OnToggleOrderChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/TogglesContainer$1;,
        Lcom/android/systemui/statusbar/phone/TogglesContainer$2;,
        Lcom/android/systemui/statusbar/phone/TogglesContainer$3;
    }
.end annotation


# instance fields
.field mClickListener:Landroid/view/View$OnClickListener;

.field mCollapseAfterClicked:Z

.field private mCollapseAfterClickedObserver:Landroid/database/ContentObserver;

.field private mColorChangeListener:Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

.field protected mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

.field mEnabled:Z

.field mHandler:Landroid/os/Handler;

.field private mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

.field mLastClickToggle:I

.field mLastIsOpenMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field mNeedCollapseMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mNeedShowMiDropHint:Z

.field protected final mRecords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;",
            ">;"
        }
    .end annotation
.end field

.field mToggleAnimators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/TogglesContainer;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/TogglesContainer;Landroid/widget/ImageView;IZ)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->startAnimatorIfNeed(Landroid/widget/ImageView;IZ)Z

    move-result v0

    return v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/statusbar/phone/PanelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHandler:Landroid/os/Handler;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mToggleAnimators:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mNeedCollapseMap:Ljava/util/HashMap;

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastClickToggle:I

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mNeedShowMiDropHint:Z

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->DEFAULT:Lcom/android/systemui/statusbar/ColorSuits;

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v1, Lcom/android/systemui/statusbar/phone/TogglesContainer$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer$1;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorChangeListener:Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

    new-instance v1, Lcom/android/systemui/statusbar/phone/TogglesContainer$2;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/android/systemui/statusbar/phone/TogglesContainer$2;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mCollapseAfterClickedObserver:Landroid/database/ContentObserver;

    new-instance v1, Lcom/android/systemui/statusbar/phone/TogglesContainer$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer$3;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mClickListener:Landroid/view/View$OnClickListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mRecords:Ljava/util/ArrayList;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "pref_need_show_midrop_hint"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mNeedShowMiDropHint:Z

    return-void
.end method

.method private cantCollopseToggle(I)Z
    .locals 1

    const/16 v0, 0x16

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setListening(Z)V
    .locals 4

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mRecords:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    iget-object v3, v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->callback:Lcom/android/systemui/qs/QSTile$Callback;

    invoke-virtual {v2, v3, p1}, Lcom/android/systemui/qs/QSTile;->setListening(Ljava/lang/Object;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private startAnimatorIfNeed(Landroid/widget/ImageView;IZ)Z
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_0

    return v5

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v1, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mIsLiteMode:Z

    if-eqz v1, :cond_1

    return v4

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mToggleAnimators:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    if-nez v0, :cond_2

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mToggleAnimators:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "SystemUI.toggleContainer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip updateToggleImage for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " status:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " last:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isOpen:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v5

    :cond_3
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    new-instance v1, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;Landroid/widget/ImageView;IZ)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v1, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;IZLandroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return v4

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public OnToggleChanged(I)V
    .locals 8

    const/4 v4, 0x1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleTextView(I)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->hasDetail(I)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    :goto_0
    invoke-static {v1, v3}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleImageView(I)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleView(I)Landroid/view/View;

    move-result-object v2

    if-eqz v1, :cond_1

    if-nez v0, :cond_3

    :cond_1
    const-string/jumbo v3, "SystemUI.toggleContainer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "view is null:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    const/4 v3, 0x2

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_1

    invoke-static {p1, v1}, Lmiui/app/ToggleManager;->updateTextView(ILandroid/widget/TextView;)V

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lmiui/app/ToggleManager;->isDisabled(I)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v3, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mIsLiteMode:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v5}, Lcom/android/systemui/statusbar/ColorSuits;->enableImageColorDye(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string/jumbo v3, "SystemUI.toggleContainer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateToggleImage color lite mode for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " status:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " last:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v3

    invoke-static {p1, v0, v3}, Lmiui/app/ToggleManager;->updateImageView(ILandroid/widget/ImageView;I)V

    :cond_4
    :goto_1
    invoke-static {p1}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    if-eq p1, v4, :cond_5

    const/16 v3, 0x18

    if-ne p1, v3, :cond_9

    :cond_5
    :goto_2
    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_a

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_3
    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    :cond_6
    invoke-static {p1}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v4

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v4, v3, :cond_7

    invoke-static {p1}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v3

    invoke-direct {p0, v0, p1, v3}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->startAnimatorIfNeed(Landroid/widget/ImageView;IZ)Z

    :cond_7
    return-void

    :cond_8
    const-string/jumbo v3, "SystemUI.toggleContainer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateToggleImage lite mode for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " status:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " last:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, v0}, Lmiui/app/ToggleManager;->updateImageView(ILandroid/widget/ImageView;)V

    goto :goto_1

    :cond_9
    const/16 v3, 0x1b

    if-eq p1, v3, :cond_5

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v3, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mIsLiteMode:Z

    if-eqz v3, :cond_6

    goto :goto_2

    :cond_a
    const/high16 v3, 0x3f000000    # 0.5f

    goto :goto_3
.end method

.method public OnToggleOrderChanged()V
    .locals 0

    return-void
.end method

.method protected addTile(Lcom/android/systemui/qs/QSTile;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/systemui/qs/QSTile",
            "<*>;Z)V"
        }
    .end annotation

    new-instance v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    invoke-direct {v3}, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;-><init>()V

    iput-object p1, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    invoke-virtual {p0, p1, p2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->createTileView(Lcom/android/systemui/qs/QSTile;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    new-instance v0, Lcom/android/systemui/statusbar/phone/TogglesContainer$7;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer$7;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;)V

    iget-object v4, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    invoke-virtual {v4, v0}, Lcom/android/systemui/qs/QSTile;->addCallback(Lcom/android/systemui/qs/QSTile$Callback;)V

    iput-object v0, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->callback:Lcom/android/systemui/qs/QSTile$Callback;

    new-instance v1, Lcom/android/systemui/statusbar/phone/TogglesContainer$8;

    invoke-direct {v1, p0, v3}, Lcom/android/systemui/statusbar/phone/TogglesContainer$8;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;)V

    new-instance v2, Lcom/android/systemui/statusbar/phone/TogglesContainer$9;

    invoke-direct {v2, p0, v3}, Lcom/android/systemui/statusbar/phone/TogglesContainer$9;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;)V

    iget-object v4, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tileView:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v4, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    invoke-virtual {v4}, Lcom/android/systemui/qs/QSTile;->refreshState()V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mRecords:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected bindDetail(ILandroid/widget/TextView;)V
    .locals 8

    const v7, 0x7f0f001c

    const/16 v6, 0xf

    const/4 v5, 0x2

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->hasDetail(I)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    invoke-direct {v1}, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;-><init>()V

    iput p1, v1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->toggleId:I

    if-ne p1, v6, :cond_4

    new-instance v2, Lcom/android/systemui/qs/tiles/WifiTile;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-direct {v2, v3, v4}, Lcom/android/systemui/qs/tiles/WifiTile;-><init>(Lcom/android/systemui/qs/QSTile$Host;Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V

    iput-object v2, v1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleArrowView(I)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setMaxWidth(I)V

    if-eq p1, v6, :cond_1

    if-ne p1, v5, :cond_2

    :cond_1
    invoke-virtual {p2, v7, v1}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v0, v7, v1}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    :cond_2
    instance-of v2, p2, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;

    if-eqz v2, :cond_3

    move-object v2, p2

    check-cast v2, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->enableLargerTouchArea()V

    :cond_3
    :goto_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_4
    if-ne p1, v5, :cond_0

    new-instance v2, Lcom/android/systemui/qs/tiles/BluetoothTile;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-direct {v2, v3, v4}, Lcom/android/systemui/qs/tiles/BluetoothTile;-><init>(Lcom/android/systemui/qs/QSTile$Host;Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V

    iput-object v2, v1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleWidth()I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setMaxWidth(I)V

    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleArrowView(I)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public clickTile(Landroid/content/ComponentName;)V
    .locals 4

    invoke-static {p1}, Lcom/android/systemui/qs/external/CustomTile;->toSpec(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mRecords:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mRecords:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    invoke-virtual {v3}, Lcom/android/systemui/qs/QSTile;->getTileSpec()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mRecords:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    invoke-virtual {v3}, Lcom/android/systemui/qs/QSTile;->click()V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected createTileView(Lcom/android/systemui/qs/QSTile;Z)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/systemui/qs/QSTile",
            "<*>;Z)",
            "Landroid/view/View;"
        }
    .end annotation

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public dyeColor()V
    .locals 9

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lmiui/app/ToggleManager;->getUserSelectedToggleOrder(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleImageView(I)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/android/systemui/statusbar/ColorSuits;->enableImageColorDye(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v6

    invoke-static {v2, v3, v6}, Lmiui/app/ToggleManager;->updateImageView(ILandroid/widget/ImageView;I)V

    :goto_1
    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleTextView(I)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/android/systemui/statusbar/ColorSuits;->enableTextColorDye(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/ColorSuits;->getTextColor()I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleArrowView(I)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/android/systemui/statusbar/ColorSuits;->enableTextColorDye(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v7}, Lcom/android/systemui/statusbar/ColorSuits;->getTextColor()I

    move-result v7

    sget-object v8, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v2, v3}, Lmiui/app/ToggleManager;->updateImageView(ILandroid/widget/ImageView;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public getHost()Lcom/android/systemui/statusbar/phone/QSTileHost;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    return-object v0
.end method

.method protected getToggleArrowView(I)Landroid/widget/ImageView;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleView(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const v1, 0x7f0f019e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    return-object v1
.end method

.method protected getToggleImageView(I)Landroid/widget/ImageView;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleView(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleView(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f019f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method protected getToggleTextView(I)Landroid/widget/TextView;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getToggleView(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const v1, 0x7f0f019d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    return-object v1
.end method

.method protected getToggleView(I)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getToggleWidth()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected hasDetail(I)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0xf

    if-ne p1, v2, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-ge v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/phone/QSTileHost;->addCallback(Lcom/android/systemui/qs/QSTile$Host$Callback;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/QSTileHost;->getTiles()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->setTiles(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v0, p0}, Lmiui/app/ToggleManager;->setOnToggleChangedListener(Lmiui/app/ToggleManager$OnToggleChangedListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v0, p0}, Lmiui/app/ToggleManager;->setOnToggleOrderChangeListener(Lmiui/app/ToggleManager$OnToggleOrderChangedListener;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->OnToggleOrderChanged()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "status_bar_collapse_after_clicked"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mCollapseAfterClickedObserver:Landroid/database/ContentObserver;

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mCollapseAfterClickedObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v4}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorChangeListener:Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->registerColorChangeListener(Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v6, 0x1b

    if-ne v2, v6, :cond_0

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mNeedShowMiDropHint:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-boolean v7, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mNeedShowMiDropHint:Z

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string/jumbo v8, "pref_need_show_midrop_hint"

    invoke-interface {v6, v8, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v6, "android.intent.action.MAIN"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v6, 0x14000000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string/jumbo v6, "com.xiaomi.midrop"

    const-string/jumbo v8, "com.xiaomi.midrop.IntroActivity"

    invoke-virtual {v3, v6, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    sget-object v8, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v6, v3, v8}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->animateCollapseAndUnlock()V

    :cond_0
    const/16 v6, 0x12

    if-ne v6, v2, :cond_3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v7, "statusbar"

    invoke-static {v6, v7}, Lcom/android/systemui/screenshot/StatHelper;->recordCountEvent(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->animateCollapse()V

    new-instance v6, Lcom/android/systemui/statusbar/phone/TogglesContainer$6;

    invoke-direct {v6, p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer$6;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;)V

    invoke-virtual {p0, v6}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/systemui/AnalyticsHelper;->trackTogglesClick(Ljava/lang/String;)V

    const/16 v6, 0x1d

    if-ne v6, v2, :cond_2

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->animateCollapseAndUnlock()V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lmiui/app/ToggleManager;->isListStyle(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_8

    const-string/jumbo v6, "from_single_page_toggle"

    invoke-static {v6}, Lcom/android/systemui/AnalyticsHelper;->trackSettingsTogglePositions(Ljava/lang/String;)V

    :cond_2
    :goto_2
    return-void

    :catch_0
    move-exception v1

    const-string/jumbo v6, "SystemUI.toggleContainer"

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mToggleAnimators:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1

    :cond_4
    iput v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastClickToggle:I

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v6, v6, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v6, v2}, Lmiui/app/ToggleManager;->performToggle(I)Z

    move-result v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mNeedCollapseMap:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    if-nez v4, :cond_6

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mCollapseAfterClicked:Z

    if-eqz v6, :cond_7

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->cantCollopseToggle(I)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    :goto_3
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v8, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v4, :cond_5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v6, v6, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mIsLiteMode:Z

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mCollapseAfterClicked:Z

    if-eqz v6, :cond_1

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->cantCollopseToggle(I)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1

    :cond_5
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->animateCollapse()V

    goto/16 :goto_1

    :cond_6
    const/4 v6, 0x1

    goto :goto_3

    :cond_7
    move v6, v7

    goto :goto_3

    :cond_8
    const-string/jumbo v6, "from_double_page_toggle"

    invoke-static {v6}, Lcom/android/systemui/AnalyticsHelper;->trackSettingsTogglePositions(Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v0, p0}, Lmiui/app/ToggleManager;->removeToggleChangedListener(Lmiui/app/ToggleManager$OnToggleChangedListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v0, p0}, Lmiui/app/ToggleManager;->removeToggleOrderChangeListener(Lmiui/app/ToggleManager$OnToggleOrderChangedListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mCollapseAfterClickedObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mWeatherView:Lcom/android/systemui/statusbar/WeatherView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorChangeListener:Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->unregisterColorChangeListener(Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/QSTileHost;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quitSafely()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/phone/QSTileHost;->removeCallback(Lcom/android/systemui/qs/QSTile$Host$Callback;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/QSTileHost;->destroy()V

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lmiui/app/ToggleManager;->startLongClickAction(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->animateCollapseAndUnlock()V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/systemui/AnalyticsHelper;->trackTogglesLongPress(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onTileClick(Lcom/android/systemui/qs/QSTile;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/systemui/qs/QSTile",
            "<*>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/android/systemui/qs/QSTile;->click()V

    return-void
.end method

.method public onTilesChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/QSTileHost;->getTiles()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->setTiles(Ljava/util/Collection;)V

    return-void
.end method

.method public performCollapse()V
    .locals 1

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->performCollapse()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->setListening(Z)V

    return-void
.end method

.method public performExpand()V
    .locals 1

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->performExpand()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->setListening(Z)V

    return-void
.end method

.method public setRecordCoordinate(Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public setService(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->setService(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/phone/QSTileHost;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/phone/QSTileHost;-><init>(Landroid/content/Context;Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHost:Lcom/android/systemui/statusbar/phone/QSTileHost;

    :cond_0
    return-void
.end method

.method public setTiles(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/systemui/qs/QSTile",
            "<*>;>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->setTiles(Ljava/util/Collection;Z)V

    return-void
.end method

.method public setTiles(Ljava/util/Collection;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/systemui/qs/QSTile",
            "<*>;>;Z)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mRecords:Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;

    iget-object v4, v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->tile:Lcom/android/systemui/qs/QSTile;

    iget-object v5, v0, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->callback:Lcom/android/systemui/qs/QSTile$Callback;

    invoke-virtual {v4, v5}, Lcom/android/systemui/qs/QSTile;->removeCallback(Lcom/android/systemui/qs/QSTile$Callback;)V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mRecords:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/qs/QSTile;

    invoke-virtual {p0, v2, p2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->addTile(Lcom/android/systemui/qs/QSTile;Z)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$TileRecord;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/systemui/statusbar/phone/PanelView;->showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$Record;)V

    return-void
.end method
