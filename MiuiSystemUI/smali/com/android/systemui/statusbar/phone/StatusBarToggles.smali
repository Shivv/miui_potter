.class public Lcom/android/systemui/statusbar/phone/StatusBarToggles;
.super Lcom/android/systemui/statusbar/phone/TogglesContainer;
.source "StatusBarToggles.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/StatusBarToggles$1;,
        Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;
    }
.end annotation


# instance fields
.field private mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

.field private mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

.field private mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

.field private mCurrentBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

.field private mID2Toggle:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mItemHeightLand:I

.field private mItemHeightPort:I

.field private mLandScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

.field private mLandToggleGroup:Landroid/widget/LinearLayout;

.field private mLandToggleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mLandView:Landroid/view/View;

.field private mPortScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

.field private mPortToggleGroup:Landroid/widget/LinearLayout;

.field private mPortToggleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mPortView:Landroid/view/View;

.field private mQSDetatil:Landroid/view/View;

.field private mToggleContainer:Landroid/view/View;

.field private mToggleContainerLand:Landroid/view/View;

.field private mToggleImage:Landroid/widget/ImageView;

.field private mToggleImageLand:Landroid/widget/ImageView;

.field private mToggleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mToggleRowLand:I

.field private mToggleRowPort:I

.field private mToggleText:Landroid/view/View;

.field private mToggleTextLand:Landroid/view/View;

.field public mTrackingView:Lcom/android/systemui/statusbar/phone/TrackingView;

.field private mUpdater:Ljava/lang/Runnable;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/StatusBarToggles;)Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/StatusBarToggles;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mCurrentBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/StatusBarToggles;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->setToggleItemHeight()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/statusbar/phone/TogglesContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortToggleList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandToggleList:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mID2Toggle:Landroid/util/SparseArray;

    new-instance v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles$1;-><init>(Lcom/android/systemui/statusbar/phone/StatusBarToggles;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mUpdater:Ljava/lang/Runnable;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mEnabled:Z

    return-void
.end method

.method private bindToggle(Landroid/view/View;I)V
    .locals 4

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mID2Toggle:Landroid/util/SparseArray;

    invoke-virtual {v3, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1, v3}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getToggleTextView(I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p0, p2, v2}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->bindDetail(ILandroid/widget/TextView;)V

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getToggleImageView(I)Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;

    invoke-direct {v1, p0, p1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;-><init>(Lcom/android/systemui/statusbar/phone/StatusBarToggles;Landroid/view/View;)V

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-direct {p0, p2}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->initToggleStatus(I)V

    return-void
.end method

.method private getCurrentScrollToggles()Lcom/android/systemui/statusbar/phone/SnapScrollView;
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    return-object v0
.end method

.method private getTogglesScrollY()I
    .locals 1

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getCurrentScrollToggles()Lcom/android/systemui/statusbar/phone/SnapScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollY()I

    move-result v0

    return v0
.end method

.method private inflateToggles(Ljava/util/ArrayList;Landroid/widget/LinearLayout;II)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "II)V"
        }
    .end annotation

    if-gez p3, :cond_0

    return-void

    :cond_0
    const/4 v2, 0x0

    :goto_0
    int-to-double v8, v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    int-to-double v10, v7

    move/from16 v0, p3

    int-to-double v12, v0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    cmpg-double v7, v8, v10

    if-gez v7, :cond_3

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v4, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    new-instance v6, Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    move/from16 v0, p4

    int-to-float v7, v0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v4, v8, v9, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    new-instance v5, Landroid/widget/Space;

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-direct {v5, v7}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v3, 0x0

    :goto_1
    move/from16 v0, p3

    if-ge v3, v0, :cond_2

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f030061

    invoke-virtual {v7, v8, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    add-int/lit8 v7, p3, -0x1

    if-eq v3, v7, :cond_1

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x42c80000    # 100.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v4, v8, v9, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    new-instance v5, Landroid/widget/Space;

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-direct {v5, v7}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    move/from16 v0, p4

    int-to-float v7, v0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v4, v8, v9, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    new-instance v5, Landroid/widget/Space;

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-direct {v5, v7}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private initToggleList(Landroid/widget/LinearLayout;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_3

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f0f019c

    if-ne v5, v6, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private initToggleStatus(I)V
    .locals 5

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLastIsOpenMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p1}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->OnToggleChanged(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getToggleImageView(I)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/ColorSuits;->enableImageColorDye(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v2

    invoke-static {p1, v0, v2}, Lmiui/app/ToggleManager;->updateImageView(ILandroid/widget/ImageView;I)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getToggleTextView(I)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/ColorSuits;->enableTextColorDye(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/ColorSuits;->getTextColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    return-void

    :cond_1
    invoke-static {p1, v0}, Lmiui/app/ToggleManager;->updateImageView(ILandroid/widget/ImageView;)V

    goto :goto_0
.end method

.method private refreshBrightnessPanel()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->refreshBrightness()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->refreshBrightness()V

    :cond_0
    return-void
.end method

.method private setToggleItemHeight()V
    .locals 26

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v22

    move-object/from16 v0, v22

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    const/4 v9, 0x1

    :goto_0
    if-eqz v9, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getMeasuredHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mItemHeightPort:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortToggleGroup:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleRowPort:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleContainer:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleText:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    :goto_1
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v18

    sub-int v22, v20, v4

    add-int v23, v16, v18

    mul-int v23, v23, v10

    sub-int v22, v22, v23

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    mul-double v22, v22, v24

    add-int/lit8 v24, v10, 0x1

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v12, v0

    add-int v22, v12, v16

    add-int v6, v22, v18

    sub-int v22, v20, v4

    mul-int v23, v6, v10

    sub-int v13, v22, v23

    if-eq v7, v6, :cond_3

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v15, v0, v1, v2, v13}, Landroid/widget/LinearLayout;->setPaddingRelative(IIII)V

    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v15}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v22

    move/from16 v0, v22

    if-ge v5, v0, :cond_2

    invoke-virtual {v15, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iput v6, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_0
    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getMeasuredHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mItemHeightLand:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandToggleGroup:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleRowLand:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleContainerLand:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleTextLand:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleImageLand:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    goto/16 :goto_1

    :cond_2
    if-eqz v9, :cond_4

    move-object/from16 v0, p0

    iput v6, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mItemHeightPort:I

    :goto_3
    invoke-virtual {v11, v6}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->setItemHeight(I)V

    :cond_3
    return-void

    :cond_4
    move-object/from16 v0, p0

    iput v6, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mItemHeightLand:I

    goto :goto_3
.end method


# virtual methods
.method public OnToggleChanged(I)V
    .locals 1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x16

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->refreshBrightnessPanel()V

    :cond_1
    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->OnToggleChanged(I)V

    return-void
.end method

.method public OnToggleOrderChanged()V
    .locals 4

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lmiui/app/ToggleManager;->getUserSelectedToggleOrder(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mID2Toggle:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->bindToggle(Landroid/view/View;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public canScrollUp()Z
    .locals 2

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getCurrentScrollToggles()Lcom/android/systemui/statusbar/phone/SnapScrollView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->canScrollVertically(I)Z

    move-result v0

    return v0
.end method

.method public dyeColor()V
    .locals 2

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->dyeColor()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->dyeColor(Lcom/android/systemui/statusbar/ColorSuits;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->dyeColor(Lcom/android/systemui/statusbar/ColorSuits;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getMirror()Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getMirror()Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->dyeColor(Lcom/android/systemui/statusbar/ColorSuits;)V

    :cond_0
    return-void
.end method

.method protected getToggleView(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mID2Toggle:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public getToggleWidth()I
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->updateResources()V

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->onAttachedToWindow()V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->updateResources()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->OnToggleOrderChanged()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 10

    const v9, 0x7f0f01a1

    const v8, 0x7f0f019f

    const v7, 0x7f0f019d

    const v6, 0x7f0f019c

    const v5, 0x7f0f0180

    const v3, 0x7f0f008d

    invoke-virtual {p0, v3}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    const v3, 0x7f0f0096

    invoke-virtual {p0, v3}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    const v4, 0x7f0f01a2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortToggleGroup:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    const v4, 0x7f0f01a2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandToggleGroup:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/phone/SnapScrollView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/phone/SnapScrollView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    const v3, 0x7f0f019b

    invoke-virtual {p0, v3}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mQSDetatil:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lmiui/app/ToggleManager;->getUserSelectedToggleOrder(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x11070007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f10000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortToggleGroup:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->inflateToggles(Ljava/util/ArrayList;Landroid/widget/LinearLayout;II)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x11070008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f10000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandToggleGroup:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->inflateToggles(Ljava/util/ArrayList;Landroid/widget/LinearLayout;II)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandToggleGroup:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandToggleList:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v4, v2}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->initToggleList(Landroid/widget/LinearLayout;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortToggleGroup:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortToggleList:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v4, v2}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->initToggleList(Landroid/widget/LinearLayout;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleContainer:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleText:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleImage:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleContainerLand:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleTextLand:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleImageLand:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f10000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleRowPort:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f10000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleRowLand:I

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->onFinishInflate()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    invoke-super/range {p0 .. p5}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->onLayout(ZIIII)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mUpdater:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public scrollToTop()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    invoke-virtual {v0, v1, v1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollTo(II)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandScrollToggles:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    invoke-virtual {v0, v1, v1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollTo(II)V

    return-void
.end method

.method public setBrightnessMirror(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V
    .locals 2

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getMirror()Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setMirror(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setMirrorController(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getMirror()Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setMirror(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setMirrorController(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V

    :cond_0
    return-void
.end method

.method public setRecordCoordinate(Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;Landroid/view/View;)V
    .locals 4

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getTogglesScrollY()I

    move-result v3

    sub-int v1, v2, v3

    iput v0, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->x:I

    iput v1, p1, Lcom/android/systemui/statusbar/phone/PanelView$TileRecord;->y:I

    return-void
.end method

.method public showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$TileRecord;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mTrackingView:Lcom/android/systemui/statusbar/phone/TrackingView;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/phone/TrackingView;->showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$Record;)V

    return-void
.end method

.method public showScrollHint()V
    .locals 1

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getCurrentScrollToggles()Lcom/android/systemui/statusbar/phone/SnapScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->showScrollBarHint()V

    return-void
.end method

.method public updateResources()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->updateResources(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortToggleList:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mCurrentBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mQSDetatil:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->updateTogglePanelVisibility(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mCurrentBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getMirror()Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setMirror(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mCurrentBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setMirrorController(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->updateResources(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandToggleList:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mToggleList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mBrightnessLandPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mCurrentBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    goto :goto_0
.end method

.method public updateTogglePanelVisibility(Z)V
    .locals 3

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mPortView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mLandView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
