.class public Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;
.super Landroid/widget/ImageView;
.source "BatteryStatusIconView.java"

# interfaces
.implements Lcom/android/systemui/statusbar/policy/BatteryController$BatteryStateChangeCallback;


# instance fields
.field private mDemoMode:Z

.field private mDisabled:Z

.field private mEnableDarkMode:Z

.field private mIconId:I

.field private mIconLevel:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private getDemoModeIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    const/16 v1, 0x64

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mEnableDarkMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/phone/BatteryIcon;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getGraphicIconDarkMode(I)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/phone/BatteryIcon;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getGraphicIcon(I)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public disable(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDisabled:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDisabled:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->updateVisibility()V

    return-void
.end method

.method public dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v0, "demo_mode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "BatteryStatusIconView mDemoMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDemoMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", command = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDemoMode:Z

    if-nez v0, :cond_1

    const-string/jumbo v0, "enter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDemoMode:Z

    invoke-virtual {p0, v3}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->update()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDemoMode:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "exit"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDemoMode:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->updateVisibility()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->update()V

    goto :goto_0
.end method

.method protected getIcon(I)Landroid/graphics/drawable/Drawable;
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/systemui/statusbar/policy/BatteryController;->getBatteryStyle()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return-object v2

    :cond_1
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    return-object v2

    :pswitch_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mEnableDarkMode:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/phone/BatteryIcon;

    move-result-object v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getGraphicPowerSaveIconDarkMode(I)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/phone/BatteryIcon;

    move-result-object v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getGraphicPowerSaveIcon(I)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mEnableDarkMode:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/phone/BatteryIcon;

    move-result-object v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getGraphicChargeIconDarkMode(I)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_3
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/phone/BatteryIcon;

    move-result-object v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getGraphicChargeIcon(I)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mEnableDarkMode:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/phone/BatteryIcon;

    move-result-object v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getGraphicIconDarkMode(I)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/phone/BatteryIcon;

    move-result-object v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BatteryIcon;->getGraphicIcon(I)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x7f070004
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBatteryChanged(II)V
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconId:I

    if-eq v0, p2, :cond_1

    :cond_0
    iput p1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    iput p2, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconId:I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->update()V

    :cond_1
    return-void
.end method

.method public update()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDemoMode:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->getDemoModeIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDemoMode:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x64

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->setImageLevel(I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->invalidate()V

    return-void

    :cond_0
    iget v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconId:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->getIcon(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mIconLevel:I

    goto :goto_1
.end method

.method public updateDarkMode(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mEnableDarkMode:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mEnableDarkMode:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->update()V

    :cond_0
    return-void
.end method

.method protected updateVisibility()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->mDisabled:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->setVisibility(I)V

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method
