.class Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;
.super Ljava/lang/Object;
.source "ExpandedHeaderView.java"

# interfaces
.implements Lcom/android/systemui/statusbar/phone/PanelView$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;


# direct methods
.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;Lcom/android/systemui/qs/QSTile$DetailAdapter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->handleShowingDetail(Lcom/android/systemui/qs/QSTile$DetailAdapter;)V

    return-void
.end method

.method constructor <init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private handleShowingDetail(Lcom/android/systemui/qs/QSTile$DetailAdapter;)V
    .locals 4

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->-get3(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)Lcom/android/systemui/statusbar/WeatherView;

    move-result-object v1

    xor-int/lit8 v2, v0, 0x1

    invoke-direct {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->transition(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->-get1(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->transition(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-static {v1, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->-set2(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;Z)Z

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->-get1(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-interface {p1}, Lcom/android/systemui/qs/QSTile$DetailAdapter;->getTitle()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateBackground(ZZ)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private transition(Landroid/view/View;Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->hasOverlappingRendering()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3$3;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method


# virtual methods
.method public onShowingDetail(Lcom/android/systemui/qs/QSTile$DetailAdapter;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    new-instance v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3$2;

    invoke-direct {v1, p0, p1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3$2;-><init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$3;Lcom/android/systemui/qs/QSTile$DetailAdapter;)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
