.class public Lcom/android/systemui/statusbar/phone/TrackingView;
.super Lcom/android/systemui/statusbar/phone/PanelView;
.source "TrackingView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/TrackingView$1;
    }
.end annotation


# instance fields
.field private mActionPerformed:Z

.field mBottomHeight:I

.field mClearAllButton:Landroid/widget/ImageView;

.field mClearAllButtonMarginBottom:I

.field mClearAllButtonMarginTop:I

.field private mClipRect:Landroid/graphics/Rect;

.field mClosePaddingBottom:I

.field mDisplay:Landroid/view/Display;

.field mExpandedBg:Landroid/view/View;

.field mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

.field private mFirstDown:Z

.field mFixed:Landroid/view/View;

.field mFlowStatusBar:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

.field private mGap:F

.field mHandler:Landroid/os/Handler;

.field mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

.field private mInitialTouchX:F

.field private mInitialTouchY:F

.field private mIntercepted:Z

.field private mMaximumFlingVelocity:I

.field private mMoveDirection:I

.field private mNotificationCanScroll:Z

.field mNotificationHeight:I

.field private mNotificationTouched:Z

.field mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

.field mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

.field private mShowClearButtonInBottom:Z

.field mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

.field mStatusbarContent:Landroid/view/View;

.field mStatusbarView:Landroid/view/View;

.field mTabButton:Landroid/view/View;

.field mTabs:[Landroid/view/View;

.field private mToggleCanScroll:Z

.field private mToggleTouched:Z

.field private mTouchSlop:I

.field private mUpdater:Ljava/lang/Runnable;

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/TrackingView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mShowClearButtonInBottom:Z

    return v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/statusbar/phone/PanelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabs:[Landroid/view/View;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFirstDown:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    new-instance v1, Lcom/android/systemui/statusbar/phone/TrackingView$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/TrackingView$1;-><init>(Lcom/android/systemui/statusbar/phone/TrackingView;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mUpdater:Ljava/lang/Runnable;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mDisplay:Landroid/view/Display;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMaximumFlingVelocity:I

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTouchSlop:I

    return-void
.end method

.method private calculateDerection(FF)V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x3

    iget v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    if-nez v1, :cond_3

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTouchSlop:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTouchSlop:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    cmpl-float v1, p1, v4

    if-lez v1, :cond_4

    iput v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    :goto_0
    iget v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    if-ne v1, v5, :cond_1

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mNotificationTouched:Z

    if-nez v1, :cond_2

    :cond_1
    iget v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    if-ne v1, v6, :cond_7

    :cond_2
    const/4 v0, 0x1

    :goto_1
    xor-int/lit8 v1, v0, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mIntercepted:Z

    :cond_3
    return-void

    :cond_4
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    goto :goto_0

    :cond_5
    cmpl-float v1, p2, v4

    if-lez v1, :cond_6

    iput v6, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    goto :goto_0

    :cond_6
    iput v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    goto :goto_0

    :cond_7
    iget v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    if-ne v1, v3, :cond_8

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mNotificationTouched:Z

    if-eqz v1, :cond_8

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mNotificationCanScroll:Z

    if-nez v1, :cond_2

    :cond_8
    iget v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    if-ne v1, v3, :cond_9

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mToggleTouched:Z

    if-eqz v1, :cond_9

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mToggleCanScroll:Z

    goto :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static interpolate(FFF)F
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p0

    mul-float/2addr v0, p1

    mul-float v1, p0, p2

    add-float/2addr v0, v1

    return v0
.end method

.method private isInside(FFLandroid/view/View;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p3, :cond_0

    return v3

    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [I

    invoke-virtual {p3, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v1, v0, v3

    int-to-float v1, v1

    sub-float/2addr p1, v1

    aget v1, v0, v2

    int-to-float v1, v1

    sub-float/2addr p2, v1

    cmpl-float v1, p1, v4

    if-lez v1, :cond_1

    cmpl-float v1, p2, v4

    if-lez v1, :cond_1

    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gez v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, p2, v4

    if-gez v4, :cond_3

    :goto_1
    and-int v3, v1, v2

    :cond_1
    return v3

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private refreshButtonVisible(I)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mClearButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mSettingsButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mClearButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mSettingsButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private reorient()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->updateResources()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->updateViewsInExpandedStatusBar()V

    return-void
.end method

.method private setPileLayers(I)V
    .locals 14

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->getChildCount()I

    move-result v2

    packed-switch p1, :pswitch_data_0

    :cond_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    invoke-virtual {v9, v3}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, p1, v10}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v9, 0x2

    new-array v5, v9, [I

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/phone/TrackingView;->getLocationInWindow([I)V

    const/4 v9, 0x0

    aget v4, v5, v9

    const/4 v9, 0x1

    aget v7, v5, v9

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->getWidth()I

    move-result v9

    add-int v6, v4, v9

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v9

    add-int v0, v7, v9

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_0

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    invoke-virtual {v9, v3}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v9, 0x0

    aget v9, v5, v9

    const/4 v10, 0x1

    aget v10, v5, v10

    const/4 v11, 0x0

    aget v11, v5, v11

    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    move-result v12

    add-int/2addr v11, v12

    const/4 v12, 0x1

    aget v12, v5, v12

    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v13

    add-int/2addr v12, v13

    invoke-virtual {v1, v9, v10, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {v1, v4, v7, v6, v0}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x0

    invoke-virtual {v8, p1, v9}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private setupTab(IIZ)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/View;->setSelected(Z)V

    new-instance v1, Lcom/android/systemui/statusbar/phone/TrackingView$4;

    invoke-direct {v1, p0, p2}, Lcom/android/systemui/statusbar/phone/TrackingView$4;-><init>(Lcom/android/systemui/statusbar/phone/TrackingView;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabs:[Landroid/view/View;

    aput-object v0, v1, p2

    return-void
.end method


# virtual methods
.method public closeDetail()V
    .locals 2

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->closeDetail()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->onToggleDetailShow(Z)V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x1

    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    const v2, 0x7f0d030d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public getNotificationContainer()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    return-object v0
.end method

.method public makeExpandedVisible(Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->makeExpandedVisible(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->scrollToTop()V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    invoke-virtual {v0, v1, v1}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->scrollTo(II)V

    return-void
.end method

.method public markAllSeen(Lcom/android/systemui/statusbar/NotificationData;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->getMarkAllSeenResult()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setY(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->setY(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->reorient()V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->reorient()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    const v4, 0x7f0f0174

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->onFinishInflate()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mNotificationHeight:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0098

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginTop:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0099

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginBottom:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mShowClearButtonInBottom:Z

    invoke-virtual {p0, v4}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    const v0, 0x7f0f0188

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    const v0, 0x7f0f017f

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarView:Landroid/view/View;

    const v0, 0x7f0f017e

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButton:Landroid/widget/ImageView;

    const v0, 0x7f0f017d

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    const v0, 0x7f0f0173

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBg:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarContent:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarContent:Landroid/view/View;

    new-instance v1, Lcom/android/systemui/statusbar/phone/TrackingView$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/TrackingView$2;-><init>(Lcom/android/systemui/statusbar/phone/TrackingView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iput v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClosePaddingBottom:I

    const v0, 0x7f0f0179

    invoke-direct {p0, v0, v2, v3}, Lcom/android/systemui/statusbar/phone/TrackingView;->setupTab(IIZ)V

    const v0, 0x7f0f017a

    invoke-direct {p0, v0, v3, v2}, Lcom/android/systemui/statusbar/phone/TrackingView;->setupTab(IIZ)V

    const v0, 0x7f0f0176

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    iput-object p0, v0, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->mTrackingView:Lcom/android/systemui/statusbar/phone/TrackingView;

    const v0, 0x7f0f0184

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFlowStatusBar:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    const v0, 0x7f0f0177

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    const v0, 0x7f0f0189

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    const v0, 0x7f0f018a

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    if-nez v2, :cond_4

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mIntercepted:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mActionPerformed:Z

    iput-boolean v6, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFirstDown:Z

    iput v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchY:F

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedTabIndex:I

    if-nez v2, :cond_3

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchX:F

    iget v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchY:F

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v5, v5, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    invoke-direct {p0, v2, v4, v5}, Lcom/android/systemui/statusbar/phone/TrackingView;->isInside(FFLandroid/view/View;)Z

    move-result v2

    :goto_0
    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mNotificationTouched:Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->canScrollVertically(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mNotificationCanScroll:Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedTabIndex:I

    if-ne v2, v6, :cond_0

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchX:F

    iget v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchY:F

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-direct {p0, v2, v3, v4}, Lcom/android/systemui/statusbar/phone/TrackingView;->isInside(FFLandroid/view/View;)Z

    move-result v3

    :cond_0
    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mToggleTouched:Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->canScrollUp()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mToggleCanScroll:Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mBottomHeight:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchY:F

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mGap:F

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    :cond_1
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_2
    :goto_1
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mIntercepted:Z

    return v2

    :cond_3
    move v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x2

    if-ne v3, v2, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchX:F

    sub-float v0, v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchY:F

    sub-float v1, v2, v3

    invoke-direct {p0, v0, v1}, Lcom/android/systemui/statusbar/phone/TrackingView;->calculateDerection(FF)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    if-ne v2, v6, :cond_2

    :cond_6
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    const/4 v11, 0x2

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-boolean v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mActionPerformed:Z

    if-nez v5, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    iget v6, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchX:F

    sub-float v1, v5, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    iget v6, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mInitialTouchY:F

    sub-float v2, v5, v6

    invoke-direct {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/TrackingView;->calculateDerection(FF)V

    iget v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_0

    iget v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_4

    :cond_0
    iget-boolean v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFirstDown:Z

    if-eqz v5, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v5, v11, :cond_6

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->setAction(I)V

    iget v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mGap:F

    invoke-virtual {v0, v7, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v5, v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->interceptTouchEventForPanelView(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :goto_0
    iput-boolean v8, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFirstDown:Z

    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v5, v9, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v10, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_3
    return v9

    :cond_4
    iget v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMoveDirection:I

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v5, v11, :cond_1

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    iget v6, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mMaximumFlingVelocity:I

    int-to-float v6, v6

    const/16 v7, 0x3e8

    invoke-virtual {v5, v7, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    sget-boolean v5, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sTogglesInListStyle:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v5, v5, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mInEditMode:Z

    if-nez v5, :cond_5

    iget-boolean v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mIsDetailOpened:Z

    if-eqz v5, :cond_7

    :cond_5
    return v9

    :cond_6
    iget v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mGap:F

    invoke-virtual {p1, v7, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v5, p1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->interceptTouchEventForPanelView(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :cond_7
    const/high16 v5, 0x43fa0000    # 500.0f

    cmpl-float v5, v3, v5

    if-lez v5, :cond_8

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->flingToPreviousTab()V

    iput-boolean v9, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mActionPerformed:Z

    goto :goto_1

    :cond_8
    const/high16 v5, -0x3c060000    # -500.0f

    cmpg-float v5, v3, v5

    if-gez v5, :cond_1

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->flingToNextTab()V

    iput-boolean v9, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mActionPerformed:Z

    goto/16 :goto_1
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->onWindowVisibilityChanged(I)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/systemui/statusbar/phone/TrackingView$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/TrackingView$3;-><init>(Lcom/android/systemui/statusbar/phone/TrackingView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public performCollapse()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->performCollapse()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->performCollapse()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    invoke-virtual {v1, v3}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->setMarkAllSeenResult(Z)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setY(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->setY(F)V

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mShowClearButtonInBottom:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTranslationX(F)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    invoke-virtual {v1, v3}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->setLayoutTransitionsEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->resetFoldState()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setX(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method

.method public performExpand()V
    .locals 2

    invoke-super {p0}, Lcom/android/systemui/statusbar/phone/PanelView;->performExpand()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->performExpand()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->setLayoutTransitionsEnabled(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->onStatusBarPerformExpand()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->markAllSeen(I)V

    return-void
.end method

.method public prepareTracking(IZ)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/systemui/statusbar/phone/PanelView;->prepareTracking(IZ)V

    if-eqz p2, :cond_0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mShowClearButtonInBottom:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/phone/TrackingView;->setPileLayers(I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->setLayoutTransitionsEnabled(Z)V

    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->showScrollHint()V

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFlowStatusBar:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->updateVisibility()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->onStatusBarPrepareTracking()V

    :cond_2
    return-void
.end method

.method public resetFoldState()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mIsFoldOpened:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->handleShowingFold(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->resetFoldState()V

    return-void
.end method

.method public resetResWhenOrientationChanged()V
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mScrollView:Lcom/android/systemui/statusbar/phone/NotificationScrollView;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->setBackgroundResource(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFlowStatusBar:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->setBackgroundResource(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    invoke-virtual {v2, v1}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public selectTab(II)V
    .locals 4

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabs:[Landroid/view/View;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabs:[Landroid/view/View;

    aget-object v3, v1, v0

    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1, p1, p2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->onTabClick(II)V

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/TrackingView;->refreshButtonVisible(I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->updateViewsInExpandedStatusBar()V

    return-void
.end method

.method public setBrightnessMirror(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->setBrightnessMirror(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V

    return-void
.end method

.method public setFoldState()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mIsFoldOpened:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->handleShowingFold(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->setFoldState()V

    return-void
.end method

.method public setService(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/phone/PanelView;->setService(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusBarToggles:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/phone/StatusBarToggles;->setService(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mPile:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    iput-object p1, v0, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    return-void
.end method

.method public showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$Record;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/systemui/statusbar/phone/PanelView;->showDetail(ZLcom/android/systemui/statusbar/phone/PanelView$Record;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->onToggleDetailShow(Z)V

    return-void
.end method

.method public stopTracking()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/TrackingView;->setPileLayers(I)V

    return-void
.end method

.method public updateClip()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getY()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x14

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x14

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipBounds(Landroid/graphics/Rect;)V

    return-void
.end method

.method public updateExpandedViewHeight(I)V
    .locals 9

    const/4 v1, 0x0

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    int-to-float v3, p1

    mul-float/2addr v3, v7

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v4

    iget v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mNotificationHeight:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-static {v3, v4, v8}, Lcom/android/systemui/statusbar/phone/TrackingView;->interpolate(FFF)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setY(F)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    int-to-float v3, p1

    mul-float/2addr v3, v7

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mShowClearButtonInBottom:Z

    if-eqz v6, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getRealNavBarSize()I

    move-result v1

    :cond_0
    sub-int v1, v5, v1

    int-to-float v1, v1

    invoke-static {v3, v4, v1}, Lcom/android/systemui/statusbar/phone/TrackingView;->interpolate(FFF)F

    move-result v1

    invoke-virtual {v2, v1}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->setY(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getY()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int v1, p1, v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int v2, p1, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->updateClip()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v1

    sub-int v0, p1, v1

    if-lez v0, :cond_1

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mShowClearButtonInBottom:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setTranslationX(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    int-to-float v2, v0

    mul-float/2addr v2, v7

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2, v7}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getRealNavBarSize()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginBottom:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButton:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/android/systemui/statusbar/phone/TrackingView;->interpolate(FFF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    :cond_1
    :goto_1
    invoke-static {}, Lcom/android/systemui/Util;->useDimLayer()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBg:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setTranslationY(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    int-to-float v2, v0

    mul-float/2addr v2, v7

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2, v7}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginTop:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getRealNavBarSize()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButton:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/android/systemui/statusbar/phone/TrackingView;->interpolate(FFF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setX(F)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBg:Landroid/view/View;

    int-to-float v2, p1

    mul-float/2addr v2, v7

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getExpandedViewMaxHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x4

    div-int/lit8 v3, v3, 0x5

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_2
.end method

.method public updateResources()V
    .locals 5

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a000d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mShowClearButtonInBottom:Z

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0098

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginTop:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0099

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mClearAllButtonMarginBottom:I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00a0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/TrackingView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100027

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-eq v3, v2, :cond_0

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v3, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-eq v3, v2, :cond_1

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mFixed:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-eq v3, v2, :cond_2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mExpandedBottomView:Lcom/android/systemui/statusbar/phone/ExpandedBottomView;

    invoke-virtual {v3, v0}, Lcom/android/systemui/statusbar/phone/ExpandedBottomView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-eq v3, v2, :cond_3

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mStatusbarView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mShowClearButtonInBottom:Z

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_0
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mTabButton:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TrackingView;->mUpdater:Ljava/lang/Runnable;

    invoke-virtual {p0, v3}, Lcom/android/systemui/statusbar/phone/TrackingView;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_4
    const/16 v3, 0x10

    goto :goto_0
.end method
