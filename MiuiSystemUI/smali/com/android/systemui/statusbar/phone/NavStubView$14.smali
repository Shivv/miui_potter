.class Lcom/android/systemui/statusbar/phone/NavStubView$14;
.super Ljava/lang/Object;
.source "NavStubView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/NavStubView;->startAppAnimation(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

.field final synthetic val$finalCurScale:F

.field final synthetic val$finalCurTranslationX:F

.field final synthetic val$initX:I

.field final synthetic val$initY:I

.field final synthetic val$type:I


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/NavStubView;IIIFF)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iput p2, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$type:I

    iput p3, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initX:I

    iput p4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initY:I

    iput p5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$finalCurTranslationX:F

    iput p6, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$finalCurScale:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 10

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v5, v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set2(Lcom/android/systemui/statusbar/phone/NavStubView;F)F

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    iget v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$type:I

    packed-switch v4, :pswitch_data_0

    :goto_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->invalidate()V

    return-void

    :pswitch_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get32(Lcom/android/systemui/statusbar/phone/NavStubView;)I

    move-result v4

    div-int/lit8 v3, v4, 0x2

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get20(Lcom/android/systemui/statusbar/phone/NavStubView;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get31(Lcom/android/systemui/statusbar/phone/NavStubView;)I

    move-result v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get32(Lcom/android/systemui/statusbar/phone/NavStubView;)I

    move-result v5

    if-ge v4, v5, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get21(Lcom/android/systemui/statusbar/phone/NavStubView;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initX:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initX:I

    sub-int v6, v3, v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set9(Lcom/android/systemui/statusbar/phone/NavStubView;I)I

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initY:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v6}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get31(Lcom/android/systemui/statusbar/phone/NavStubView;)I

    move-result v6

    iget v7, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initY:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set10(Lcom/android/systemui/statusbar/phone/NavStubView;I)I

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$finalCurTranslationX:F

    iget v6, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$finalCurScale:F

    invoke-static {v4, v5, v6, v2}, Lcom/android/systemui/statusbar/phone/NavStubView;->-wrap0(Lcom/android/systemui/statusbar/phone/NavStubView;FFF)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get24(Lcom/android/systemui/statusbar/phone/NavStubView;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get24(Lcom/android/systemui/statusbar/phone/NavStubView;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, v5, Lcom/android/systemui/statusbar/phone/NavStubView;->targetBgAlpha:I

    int-to-float v5, v5

    sub-float v6, v9, v2

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v5, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->setBackgroundColor(I)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get8(Lcom/android/systemui/statusbar/phone/NavStubView;)Landroid/graphics/RectF;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get8(Lcom/android/systemui/statusbar/phone/NavStubView;)Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get8(Lcom/android/systemui/statusbar/phone/NavStubView;)Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v0, v4, v5

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get8(Lcom/android/systemui/statusbar/phone/NavStubView;)Landroid/graphics/RectF;

    move-result-object v4

    iget v1, v4, Landroid/graphics/RectF;->bottom:F

    :cond_2
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initX:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initX:I

    int-to-float v6, v6

    sub-float v6, v0, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set9(Lcom/android/systemui/statusbar/phone/NavStubView;I)I

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initY:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initY:I

    int-to-float v6, v6

    sub-float v6, v1, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set10(Lcom/android/systemui/statusbar/phone/NavStubView;I)I

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get35(Lcom/android/systemui/statusbar/phone/NavStubView;)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set12(Lcom/android/systemui/statusbar/phone/NavStubView;I)I

    goto/16 :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initX:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v6}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get30(Lcom/android/systemui/statusbar/phone/NavStubView;)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    iget v7, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initX:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set9(Lcom/android/systemui/statusbar/phone/NavStubView;I)I

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initY:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-static {v6}, Lcom/android/systemui/statusbar/phone/NavStubView;->-get29(Lcom/android/systemui/statusbar/phone/NavStubView;)I

    move-result v6

    mul-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x4

    iget v7, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->val$initY:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set10(Lcom/android/systemui/statusbar/phone/NavStubView;I)I

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    iget v5, v5, Lcom/android/systemui/statusbar/phone/NavStubView;->targetBgAlpha:I

    int-to-float v5, v5

    sub-float v6, v9, v2

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v5, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NavStubView$14;->this$0:Lcom/android/systemui/statusbar/phone/NavStubView;

    sub-float v5, v9, v2

    invoke-static {v4, v5}, Lcom/android/systemui/statusbar/phone/NavStubView;->-set1(Lcom/android/systemui/statusbar/phone/NavStubView;F)F

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
