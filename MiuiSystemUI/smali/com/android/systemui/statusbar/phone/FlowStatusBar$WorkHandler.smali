.class final Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;
.super Landroid/os/Handler;
.source "FlowStatusBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/FlowStatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/FlowStatusBar;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    const v3, 0x186a0

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-get2(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-get7(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Z

    move-result v1

    :goto_1
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-wrap0(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-get1(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Z

    move-result v2

    and-int/2addr v1, v2

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-get3(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput v3, v0, Landroid/os/Message;->what:I

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    :goto_2
    iput v2, v0, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-get3(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
