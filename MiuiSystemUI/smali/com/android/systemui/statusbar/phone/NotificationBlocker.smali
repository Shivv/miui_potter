.class public Lcom/android/systemui/statusbar/phone/NotificationBlocker;
.super Landroid/widget/FrameLayout;
.source "NotificationBlocker.java"


# static fields
.field public static CONFIRM_STATE:I

.field public static ENTER_EDIT_MODE_STATE:I

.field public static EXIT_EDIT_MODE_STATE:I

.field public static NO_NOTIFICATION_STATE:I

.field public static sNotch:Z


# instance fields
.field mConfirmView:Landroid/view/View;

.field mCurrentState:I

.field mEnterEditModeView:Landroid/view/View;

.field mExitEditModeView:Landroid/view/View;

.field mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field mStateViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/statusbar/phone/NotificationBlockerState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/NotificationBlocker;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->NO_NOTIFICATION_STATE:I

    const/4 v0, 0x1

    sput v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->ENTER_EDIT_MODE_STATE:I

    const/4 v0, 0x2

    sput v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->EXIT_EDIT_MODE_STATE:I

    const/4 v0, 0x3

    sput v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->sNotch:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->NO_NOTIFICATION_STATE:I

    iput v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mCurrentState:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->NO_NOTIFICATION_STATE:I

    iput v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mCurrentState:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->NO_NOTIFICATION_STATE:I

    iput v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mCurrentState:I

    return-void
.end method

.method private updateBlockerLayout()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    const/4 v4, -0x2

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->performLayoutNow(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->getMeasuredWidth()I

    move-result v1

    if-eq v2, v1, :cond_0

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v2, v3, v4

    const/4 v4, 0x1

    aput v1, v3, v4

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v3

    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v3, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v3}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v3, Lcom/android/systemui/statusbar/phone/NotificationBlocker$4;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker$4;-><init>(Lcom/android/systemui/statusbar/phone/NotificationBlocker;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v3, Lcom/android/systemui/statusbar/phone/NotificationBlocker$5;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker$5;-><init>(Lcom/android/systemui/statusbar/phone/NotificationBlocker;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_0
    return-void
.end method


# virtual methods
.method public addEnterModeView(Landroid/view/View;)V
    .locals 4

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mEnterEditModeView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mEnterEditModeView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sTogglesInListStyle:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->NO_NOTIFICATION_STATE:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->ENTER_EDIT_MODE_STATE:I

    new-instance v2, Lcom/android/systemui/statusbar/phone/ImageState;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mEnterEditModeView:Landroid/view/View;

    invoke-direct {v2, v3}, Lcom/android/systemui/statusbar/phone/ImageState;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->EXIT_EDIT_MODE_STATE:I

    new-instance v2, Lcom/android/systemui/statusbar/phone/ImageState;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mExitEditModeView:Landroid/view/View;

    invoke-direct {v2, v3}, Lcom/android/systemui/statusbar/phone/ImageState;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    new-instance v2, Lcom/android/systemui/statusbar/phone/TextState;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mConfirmView:Landroid/view/View;

    invoke-direct {v2, v3}, Lcom/android/systemui/statusbar/phone/TextState;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public changeState(I)V
    .locals 8

    const v7, 0x7f020063

    const/16 v4, 0x8

    const/4 v5, 0x0

    sget v6, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->NO_NOTIFICATION_STATE:I

    if-lt p1, v6, :cond_0

    sget v6, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    if-le p1, v6, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v6, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mCurrentState:I

    if-eq p1, v6, :cond_0

    sget-boolean v6, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->sNotch:Z

    if-eqz v6, :cond_5

    sget v6, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->EXIT_EDIT_MODE_STATE:I

    if-eq p1, v6, :cond_2

    sget v6, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    if-ne p1, v6, :cond_4

    :cond_2
    invoke-virtual {p0, v7}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->setBackgroundResource(I)V

    :goto_0
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    iget v7, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mCurrentState:I

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;

    if-eqz v2, :cond_3

    if-ne v2, v1, :cond_6

    const/4 v6, 0x4

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->setVisibility(I)V

    :goto_2
    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->reset()V

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->setBackgroundResource(I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v7}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->setBackgroundResource(I)V

    goto :goto_0

    :cond_6
    invoke-virtual {v2, v4}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->setVisibility(I)V

    goto :goto_2

    :cond_7
    if-nez v1, :cond_a

    :goto_3
    invoke-virtual {p0, v4}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->updateBlockerLayout()V

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->outAnimation()V

    :cond_8
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->inAnimation()V

    :cond_9
    iput p1, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mCurrentState:I

    return-void

    :cond_a
    move v4, v5

    goto :goto_3
.end method

.method public getEnterEditModeView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mEnterEditModeView:Landroid/view/View;

    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f0194

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mExitEditModeView:Landroid/view/View;

    const v0, 0x7f0f0195

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mConfirmView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mExitEditModeView:Landroid/view/View;

    new-instance v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker$1;-><init>(Lcom/android/systemui/statusbar/phone/NotificationBlocker;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mConfirmView:Landroid/view/View;

    new-instance v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker$2;-><init>(Lcom/android/systemui/statusbar/phone/NotificationBlocker;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->sNotch:Z

    if-nez v0, :cond_0

    const v0, 0x7f0f0193

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mEnterEditModeView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mEnterEditModeView:Landroid/view/View;

    new-instance v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker$3;-><init>(Lcom/android/systemui/statusbar/phone/NotificationBlocker;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->NO_NOTIFICATION_STATE:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->ENTER_EDIT_MODE_STATE:I

    new-instance v2, Lcom/android/systemui/statusbar/phone/ImageState;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mEnterEditModeView:Landroid/view/View;

    invoke-direct {v2, v3}, Lcom/android/systemui/statusbar/phone/ImageState;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->EXIT_EDIT_MODE_STATE:I

    new-instance v2, Lcom/android/systemui/statusbar/phone/ImageState;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mExitEditModeView:Landroid/view/View;

    invoke-direct {v2, v3}, Lcom/android/systemui/statusbar/phone/ImageState;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mStateViews:Ljava/util/ArrayList;

    sget v1, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->CONFIRM_STATE:I

    new-instance v2, Lcom/android/systemui/statusbar/phone/TextState;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->mConfirmView:Landroid/view/View;

    invoke-direct {v2, v3}, Lcom/android/systemui/statusbar/phone/TextState;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public performLayoutNow(Landroid/view/View;)V
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    :cond_0
    return-void
.end method
