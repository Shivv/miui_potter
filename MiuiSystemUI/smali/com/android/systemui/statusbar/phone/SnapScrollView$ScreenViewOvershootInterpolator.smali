.class Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;
.super Ljava/lang/Object;
.source "SnapScrollView.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/SnapScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenViewOvershootInterpolator"
.end annotation


# instance fields
.field private mTension:F

.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/SnapScrollView;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/phone/SnapScrollView;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;->this$0:Lcom/android/systemui/statusbar/phone/SnapScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->-get0(Lcom/android/systemui/statusbar/phone/SnapScrollView;)F

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;->mTension:F

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr p1, v3

    mul-float v0, p1, p1

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;->mTension:F

    add-float/2addr v1, v3

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;->mTension:F

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    return v0
.end method
