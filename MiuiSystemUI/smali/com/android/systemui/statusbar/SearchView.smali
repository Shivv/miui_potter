.class public Lcom/android/systemui/statusbar/SearchView;
.super Landroid/widget/LinearLayout;
.source "SearchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/SearchView$1;,
        Lcom/android/systemui/statusbar/SearchView$2;,
        Lcom/android/systemui/statusbar/SearchView$3;,
        Lcom/android/systemui/statusbar/SearchView$4;,
        Lcom/android/systemui/statusbar/SearchView$QueryHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field public static final PEFERRER_URI:Landroid/net/Uri;

.field public static sDuartion:I


# instance fields
.field mAnimationStartedListener:Landroid/app/ActivityOptions$OnAnimationStartedListener;

.field mColor:I

.field private mDelegateTargeted:Z

.field private mHintView:Landroid/widget/TextView;

.field private mIsRTL:Z

.field private mIsScannerChecked:Z

.field private mNeedHideTotalCount:I

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field private mQueryHandler:Lcom/android/systemui/statusbar/SearchView$QueryHandler;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mScannerRect:Landroid/graphics/Rect;

.field private mScannerView:Landroid/widget/ImageView;

.field mSearchHintObserver:Landroid/database/ContentObserver;

.field public mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mShowSearch:Z


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/systemui/statusbar/SearchView;->DEBUG:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/SearchView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/SearchView;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerRect:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/SearchView;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->isSearchRunning()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/SearchView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->checkNeedHide()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/systemui/statusbar/SearchView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->onBootCompleted()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/systemui/statusbar/SearchView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->startQuery()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/systemui/statusbar/SearchView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->startScannerActivity()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/systemui/statusbar/SearchView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->startSearchActivity()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/systemui/statusbar/SearchView;Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/SearchView;->updateHintView(Landroid/database/Cursor;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "SearchView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/statusbar/SearchView;->DEBUG:Z

    const-string/jumbo v0, "android-app://com.android.systemui"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/SearchView;->PEFERRER_URI:Landroid/net/Uri;

    const/16 v0, 0x258

    sput v0, Lcom/android/systemui/statusbar/SearchView;->sDuartion:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/SearchView;->mIsScannerChecked:Z

    new-instance v0, Lcom/android/systemui/statusbar/SearchView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/SearchView$1;-><init>(Lcom/android/systemui/statusbar/SearchView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/systemui/statusbar/SearchView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/SearchView$2;-><init>(Lcom/android/systemui/statusbar/SearchView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/systemui/statusbar/SearchView$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/SearchView$3;-><init>(Lcom/android/systemui/statusbar/SearchView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mSearchHintObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/systemui/statusbar/SearchView$4;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/SearchView$4;-><init>(Lcom/android/systemui/statusbar/SearchView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mAnimationStartedListener:Landroid/app/ActivityOptions$OnAnimationStartedListener;

    return-void
.end method

.method private checkNeedHide()V
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/SearchView;->mNeedHideTotalCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/systemui/statusbar/SearchView;->mNeedHideTotalCount:I

    iget v0, p0, Lcom/android/systemui/statusbar/SearchView;->mNeedHideTotalCount:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->onChangeToSearchEnd()V

    :cond_0
    return-void
.end method

.method private checkScanner()V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/SearchView;->mIsScannerChecked:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->getScannerIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    const/high16 v4, 0x10000

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerView:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerView:Landroid/widget/ImageView;

    new-instance v3, Lcom/android/systemui/statusbar/SearchView$5;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/SearchView$5;-><init>(Lcom/android/systemui/statusbar/SearchView;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerView:Landroid/widget/ImageView;

    new-instance v3, Lcom/android/systemui/statusbar/SearchView$6;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/SearchView$6;-><init>(Lcom/android/systemui/statusbar/SearchView;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/SearchView;->mIsScannerChecked:Z

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerView:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method private getScannerIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "com.xiaomi.scanner"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.REFERRER"

    sget-object v2, Lcom/android/systemui/statusbar/SearchView;->PEFERRER_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method private initResource()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/SearchView;->mShowSearch:Z

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/SearchView;->mShowSearch:Z

    :cond_0
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/SearchView;->mShowSearch:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/SearchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v0, p0, Lcom/android/systemui/statusbar/SearchView;->mColor:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/SearchView;->setColor(I)V

    :cond_1
    sget-boolean v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sBootCompleted:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->onBootCompleted()V

    :cond_2
    return-void
.end method

.method private isSearchRunning()Z
    .locals 6

    iget-object v4, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "activity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    const-string/jumbo v4, "com.android.quicksearchbox"

    iget-object v5, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    return v4

    :cond_1
    const/4 v4, 0x0

    return v4
.end method

.method public static maybeAddUserId(Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 4

    const/4 v3, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_0

    return-object p0

    :cond_0
    if-nez p0, :cond_1

    return-object v3

    :cond_1
    const/4 v1, -0x2

    if-eq p1, v1, :cond_2

    const-string/jumbo v1, "content"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/android/systemui/statusbar/SearchView;->uriHasUserId(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1

    :cond_2
    return-object p0
.end method

.method private onBootCompleted()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/SearchView;->mShowSearch:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->checkScanner()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->registerObserver()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->startQuery()V

    :cond_0
    return-void
.end method

.method private registerObserver()V
    .locals 5

    const/4 v4, -0x2

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mQueryHandler:Lcom/android/systemui/statusbar/SearchView$QueryHandler;

    if-nez v0, :cond_0

    const-string/jumbo v0, "content://com.android.quicksearchbox.xiaomi/suggest_last_access_hint"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/android/systemui/Util;->isProviderAccess(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/SearchView$QueryHandler;

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/SearchView$QueryHandler;-><init>(Lcom/android/systemui/statusbar/SearchView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mQueryHandler:Lcom/android/systemui/statusbar/SearchView$QueryHandler;

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "content://com.android.quicksearchbox.xiaomi/suggest_last_access_hint"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mSearchHintObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    :cond_0
    return-void
.end method

.method private startQuery()V
    .locals 8

    const/16 v1, 0x64

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mQueryHandler:Lcom/android/systemui/statusbar/SearchView$QueryHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mQueryHandler:Lcom/android/systemui/statusbar/SearchView$QueryHandler;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/SearchView$QueryHandler;->cancelOperation(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mQueryHandler:Lcom/android/systemui/statusbar/SearchView$QueryHandler;

    const-string/jumbo v3, "content://com.android.quicksearchbox.xiaomi/suggest_last_access_hint"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/SearchView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mCurrentUserId:I

    invoke-static {v3, v4}, Lcom/android/systemui/statusbar/SearchView;->maybeAddUserId(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/systemui/statusbar/SearchView$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private startScannerActivity()V
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/android/systemui/SystemUICompatibility;->dismissKeyguardOnNextActivity()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->getScannerIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const-string/jumbo v2, "scanner"

    invoke-static {v2}, Lcom/android/systemui/AnalyticsHelper;->trackClickLocation(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private startSearchActivity()V
    .locals 8

    :try_start_0
    invoke-static {}, Lcom/android/systemui/SystemUICompatibility;->dismissKeyguardOnNextActivity()V

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SEARCH"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "com.android.quicksearchbox"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v3, "qsb://query?close_web_page=true"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.extra.REFERRER"

    sget-object v4, Lcom/android/systemui/statusbar/SearchView;->PEFERRER_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/SearchView;->getHandler()Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/SearchView;->mAnimationStartedListener:Landroid/app/ActivityOptions$OnAnimationStartedListener;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v6, v7, v4, v5}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;IILandroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v3, v2, v0, v4}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    const/4 v3, 0x2

    iput v3, p0, Lcom/android/systemui/statusbar/SearchView;->mNeedHideTotalCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private unregisterObserver()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mQueryHandler:Lcom/android/systemui/statusbar/SearchView$QueryHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView;->mSearchHintObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mQueryHandler:Lcom/android/systemui/statusbar/SearchView$QueryHandler;

    :cond_0
    return-void
.end method

.method private updateHintView(Landroid/database/Cursor;)V
    .locals 3

    const-string/jumbo v1, ""

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "hint"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :cond_0
    if-eqz p1, :cond_1

    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/SearchView;->mHintView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p1, :cond_1

    :try_start_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v2

    if-eqz p1, :cond_2

    :try_start_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_1
    throw v2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static uriHasUserId(Landroid/net/Uri;)Z
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getUserInfo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/SearchView;->isLayoutRtl()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/SearchView;->mIsRTL:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->initResource()V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView;->mReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->initResource()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/SearchView;->isLayoutRtl()Z

    move-result v0

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/SearchView;->mIsRTL:Z

    if-eq v4, v0, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerRect:Landroid/graphics/Rect;

    if-eqz v4, :cond_0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/SearchView;->mIsRTL:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/SearchView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerRect:Landroid/graphics/Rect;

    iget v1, v4, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerRect:Landroid/graphics/Rect;

    iget v2, v4, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerRect:Landroid/graphics/Rect;

    sub-int v5, v3, v2

    iput v5, v4, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerRect:Landroid/graphics/Rect;

    sub-int v5, v3, v1

    iput v5, v4, Landroid/graphics/Rect;->right:I

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SearchView;->unregisterObserver()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/SearchView;->mShowSearch:Z

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/SearchView;->mShowSearch:Z

    :cond_0
    iget-boolean v1, p0, Lcom/android/systemui/statusbar/SearchView;->mShowSearch:Z

    if-eqz v1, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/SearchView;->setVisibility(I)V

    const v0, 0x7f0f0033

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mHintView:Landroid/widget/TextView;

    const v0, 0x7f0f0034

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerView:Landroid/widget/ImageView;

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v6, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v4, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v5, v7

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :goto_0
    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    invoke-virtual {p1, v7, v8}, Landroid/view/MotionEvent;->setLocation(FF)V

    invoke-virtual {v1, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :cond_0
    if-nez v2, :cond_1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    :cond_1
    return v6

    :pswitch_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mScannerRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_2

    iput-boolean v6, p0, Lcom/android/systemui/statusbar/SearchView;->mDelegateTargeted:Z

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    iput-boolean v8, p0, Lcom/android/systemui/statusbar/SearchView;->mDelegateTargeted:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v3, p0, Lcom/android/systemui/statusbar/SearchView;->mDelegateTargeted:Z

    goto :goto_0

    :pswitch_2
    iget-boolean v3, p0, Lcom/android/systemui/statusbar/SearchView;->mDelegateTargeted:Z

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/SearchView;->mDelegateTargeted:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setColor(I)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/android/systemui/statusbar/SearchView;->mColor:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/SearchView;->mHintView:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/systemui/statusbar/SearchView;->mColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/SearchView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    return-void
.end method
