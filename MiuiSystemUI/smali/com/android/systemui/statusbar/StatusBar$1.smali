.class Lcom/android/systemui/statusbar/StatusBar$1;
.super Landroid/service/notification/NotificationListenerService;
.source "StatusBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/StatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mKeyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Binder;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/systemui/statusbar/StatusBar;


# direct methods
.method static synthetic -wrap0(Lcom/android/systemui/statusbar/StatusBar$1;Landroid/service/notification/StatusBarNotification;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/StatusBar$1;->handleNotification(Landroid/service/notification/StatusBarNotification;)V

    return-void
.end method

.method constructor <init>(Lcom/android/systemui/statusbar/StatusBar;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-direct {p0}, Landroid/service/notification/NotificationListenerService;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/StatusBar$1;->mKeyMap:Ljava/util/HashMap;

    return-void
.end method

.method private handleNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 9

    iget-object v4, p0, Lcom/android/systemui/statusbar/StatusBar$1;->mKeyMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v3

    invoke-static {v3}, Lcom/android/systemui/CompatibilityM;->hasSmallIcon(Landroid/app/Notification;)Z

    move-result v1

    const-string/jumbo v4, "StatusBar"

    const-string/jumbo v5, "GroupChild: %b IsGroupSummary: %b IsUpdate: %b hasIcon: %b"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/app/Notification;->isGroupChild()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v6, v8

    invoke-virtual {v3}, Landroid/app/Notification;->isGroupSummary()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x1

    aput-object v7, v6, v8

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x2

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x3

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Landroid/app/Notification;->isGroupSummary()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v4, v4, Lcom/android/systemui/statusbar/StatusBar;->mGroupManager:Lcom/android/systemui/statusbar/phone/NotificationGroupManager;

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/NotificationGroupManager;->isPkgWontAutoBundle(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/systemui/statusbar/StatusBar$1;->processForRemoteInput(Landroid/app/Notification;)V

    if-eqz v2, :cond_2

    iget-object v4, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v5, v4, Lcom/android/systemui/statusbar/StatusBar;->mCommandQueue:Lcom/android/systemui/statusbar/CommandQueue;

    iget-object v4, p0, Lcom/android/systemui/statusbar/StatusBar$1;->mKeyMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/IBinder;

    invoke-virtual {v5, v4, p1}, Lcom/android/systemui/statusbar/CommandQueue;->updateNotification(Landroid/os/IBinder;Landroid/service/notification/StatusBarNotification;)V

    :goto_0
    return-void

    :cond_2
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iget-object v4, p0, Lcom/android/systemui/statusbar/StatusBar$1;->mKeyMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v4, v4, Lcom/android/systemui/statusbar/StatusBar;->mCommandQueue:Lcom/android/systemui/statusbar/CommandQueue;

    invoke-virtual {v4, v0, p1}, Lcom/android/systemui/statusbar/CommandQueue;->addNotification(Landroid/os/IBinder;Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0
.end method

.method private processForRemoteInput(Landroid/app/Notification;)V
    .locals 13

    const/4 v10, 0x0

    iget-object v9, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    if-eqz v9, :cond_5

    iget-object v9, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string/jumbo v11, "android.wearable.EXTENSIONS"

    invoke-virtual {v9, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, p1, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    if-eqz v9, :cond_0

    iget-object v9, p1, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    array-length v9, v9

    if-nez v9, :cond_5

    :cond_0
    const/4 v7, 0x0

    new-instance v8, Landroid/app/Notification$WearableExtender;

    invoke-direct {v8, p1}, Landroid/app/Notification$WearableExtender;-><init>(Landroid/app/Notification;)V

    invoke-virtual {v8}, Landroid/app/Notification$WearableExtender;->getActions()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_4

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Notification$Action;

    if-nez v0, :cond_2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/app/Notification$Action;->getRemoteInputs()[Landroid/app/RemoteInput;

    move-result-object v5

    if-eqz v5, :cond_1

    array-length v11, v5

    move v9, v10

    :goto_1
    if-ge v9, v11, :cond_3

    aget-object v6, v5, v9

    invoke-virtual {v6}, Landroid/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v12

    if-eqz v12, :cond_6

    move-object v7, v0

    :cond_3
    if-eqz v7, :cond_1

    :cond_4
    if-eqz v7, :cond_5

    iget-object v9, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v9, v9, Lcom/android/systemui/statusbar/StatusBar;->mContext:Landroid/content/Context;

    invoke-static {v9, p1}, Landroid/app/Notification$Builder;->recoverBuilder(Landroid/content/Context;Landroid/app/Notification;)Landroid/app/Notification$Builder;

    move-result-object v4

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/app/Notification$Action;

    aput-object v7, v9, v10

    invoke-virtual {v4, v9}, Landroid/app/Notification$Builder;->setActions([Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    :cond_5
    return-void

    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onListenerConnected()V
    .locals 3

    const-string/jumbo v1, "StatusBar"

    const-string/jumbo v2, "onListenerConnected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/StatusBar;->-set0(Lcom/android/systemui/statusbar/StatusBar;Z)Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/StatusBar$1;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/StatusBar;->mHandler:Lcom/android/systemui/statusbar/BaseStatusBar$H;

    new-instance v2, Lcom/android/systemui/statusbar/StatusBar$1$1;

    invoke-direct {v2, p0, v0}, Lcom/android/systemui/statusbar/StatusBar$1$1;-><init>(Lcom/android/systemui/statusbar/StatusBar$1;[Landroid/service/notification/StatusBarNotification;)V

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/BaseStatusBar$H;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onNotificationPosted(Landroid/service/notification/StatusBarNotification;Landroid/service/notification/NotificationListenerService$RankingMap;)V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-static {v0}, Lcom/android/systemui/statusbar/StatusBar;->-get3(Lcom/android/systemui/statusbar/StatusBar;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    const-string/jumbo v0, "StatusBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNotificationPosted:  Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " GroupKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getGroupKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Connected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/StatusBar;->-get0(Lcom/android/systemui/statusbar/StatusBar;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-static {v0}, Lcom/android/systemui/statusbar/StatusBar;->-get0(Lcom/android/systemui/statusbar/StatusBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/StatusBar;->mHandler:Lcom/android/systemui/statusbar/BaseStatusBar$H;

    new-instance v1, Lcom/android/systemui/statusbar/StatusBar$1$2;

    invoke-direct {v1, p0, p1}, Lcom/android/systemui/statusbar/StatusBar$1$2;-><init>(Lcom/android/systemui/statusbar/StatusBar$1;Landroid/service/notification/StatusBarNotification;)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/BaseStatusBar$H;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onNotificationRankingUpdate(Landroid/service/notification/NotificationListenerService$RankingMap;)V
    .locals 2

    const-string/jumbo v0, "StatusBar"

    const-string/jumbo v1, "onNotificationRankingUpdate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/StatusBar;->mHandler:Lcom/android/systemui/statusbar/BaseStatusBar$H;

    new-instance v1, Lcom/android/systemui/statusbar/StatusBar$1$4;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/StatusBar$1$4;-><init>(Lcom/android/systemui/statusbar/StatusBar$1;)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/BaseStatusBar$H;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onNotificationRemoved(Landroid/service/notification/StatusBarNotification;Landroid/service/notification/NotificationListenerService$RankingMap;)V
    .locals 4

    if-nez p1, :cond_0

    const-string/jumbo v0, "StatusBar"

    const-string/jumbo v1, "onNotificationRemoved: sbn == null"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-static {v0}, Lcom/android/systemui/statusbar/StatusBar;->-get3(Lcom/android/systemui/statusbar/StatusBar;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    const-string/jumbo v0, "StatusBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNotificationRemoved:  Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/StatusBar$1;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/StatusBar;->mHandler:Lcom/android/systemui/statusbar/BaseStatusBar$H;

    new-instance v1, Lcom/android/systemui/statusbar/StatusBar$1$3;

    invoke-direct {v1, p0, p1}, Lcom/android/systemui/statusbar/StatusBar$1$3;-><init>(Lcom/android/systemui/statusbar/StatusBar$1;Landroid/service/notification/StatusBarNotification;)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/BaseStatusBar$H;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
