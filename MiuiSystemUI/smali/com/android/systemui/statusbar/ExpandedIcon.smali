.class public Lcom/android/systemui/statusbar/ExpandedIcon;
.super Lcom/android/internal/statusbar/StatusBarIcon;
.source "ExpandedIcon.java"


# direct methods
.method public constructor <init>(Lcom/android/internal/statusbar/StatusBarIcon;)V
    .locals 7

    iget-object v1, p1, Lcom/android/internal/statusbar/StatusBarIcon;->user:Landroid/os/UserHandle;

    iget-object v2, p1, Lcom/android/internal/statusbar/StatusBarIcon;->pkg:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/internal/statusbar/StatusBarIcon;->icon:Landroid/graphics/drawable/Icon;

    iget v4, p1, Lcom/android/internal/statusbar/StatusBarIcon;->iconLevel:I

    iget v5, p1, Lcom/android/internal/statusbar/StatusBarIcon;->number:I

    iget-object v6, p1, Lcom/android/internal/statusbar/StatusBarIcon;->contentDescription:Ljava/lang/CharSequence;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/internal/statusbar/StatusBarIcon;-><init>(Landroid/os/UserHandle;Ljava/lang/String;Landroid/graphics/drawable/Icon;IILjava/lang/CharSequence;)V

    iget-boolean v0, p1, Lcom/android/internal/statusbar/StatusBarIcon;->visible:Z

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->visible:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/android/internal/statusbar/StatusBarIcon;
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedIcon;->clone()Lcom/android/systemui/statusbar/ExpandedIcon;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/android/systemui/statusbar/ExpandedIcon;
    .locals 2

    new-instance v0, Lcom/android/systemui/statusbar/ExpandedIcon;

    invoke-super {p0}, Lcom/android/internal/statusbar/StatusBarIcon;->clone()Lcom/android/internal/statusbar/StatusBarIcon;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/systemui/statusbar/ExpandedIcon;-><init>(Lcom/android/internal/statusbar/StatusBarIcon;)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedIcon;->clone()Lcom/android/systemui/statusbar/ExpandedIcon;

    move-result-object v0

    return-object v0
.end method

.method public equalIcons(Lcom/android/systemui/statusbar/ExpandedIcon;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return v2

    :cond_0
    if-ne p0, p1, :cond_1

    return v1

    :cond_1
    iget-object v0, p1, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    iget-object v3, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    if-ne v3, v0, :cond_2

    return v1

    :cond_2
    iget-object v3, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    invoke-virtual {v3}, Landroid/graphics/drawable/Icon;->getType()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Icon;->getType()I

    move-result v4

    if-eq v3, v4, :cond_3

    return v2

    :cond_3
    iget-object v3, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    invoke-virtual {v3}, Landroid/graphics/drawable/Icon;->getType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    return v2

    :pswitch_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    invoke-virtual {v3}, Landroid/graphics/drawable/Icon;->getResPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Icon;->getResPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    invoke-virtual {v3}, Landroid/graphics/drawable/Icon;->getResId()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Icon;->getResId()I

    move-result v4

    if-ne v3, v4, :cond_4

    :goto_0
    return v1

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    invoke-virtual {v1}, Landroid/graphics/drawable/Icon;->getUriString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Icon;->getUriString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getDrawable(Landroid/content/Context;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->user:Landroid/os/UserHandle;

    invoke-virtual {v1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    invoke-virtual {v1, p1, v0}, Landroid/graphics/drawable/Icon;->loadDrawableAsUser(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    return-object v1
.end method

.method public getIconId()I
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    invoke-virtual {v0}, Landroid/graphics/drawable/Icon;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->icon:Landroid/graphics/drawable/Icon;

    invoke-virtual {v0}, Landroid/graphics/drawable/Icon;->getResId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedIcon;->pkg:Ljava/lang/String;

    return-object v0
.end method
