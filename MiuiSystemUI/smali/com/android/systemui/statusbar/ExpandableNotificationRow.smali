.class public Lcom/android/systemui/statusbar/ExpandableNotificationRow;
.super Landroid/widget/FrameLayout;
.source "ExpandableNotificationRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/ExpandableNotificationRow$1;,
        Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;
    }
.end annotation


# static fields
.field private static mSetOutlineProvider:Ljava/lang/reflect/Method;

.field private static mViewOutlineProviderClazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private mAppName:Ljava/lang/String;

.field mClipTopAmount:I

.field private mContent:Lcom/android/systemui/statusbar/LatestItemView;

.field private mDisabled:Z

.field private mExpandClickListener:Landroid/view/View$OnClickListener;

.field private mExpandedNotification:Lcom/android/systemui/statusbar/ExpandedNotification;

.field private mFilter:Landroid/view/View;

.field private mFilterImage:Landroid/widget/ImageView;

.field private mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

.field private mFilterTouchListener:Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;

.field private mFilterViewWidth:I

.field private mGroupManager:Lcom/android/systemui/statusbar/phone/NotificationGroupManager;

.field private mGroupSummaryStub:Landroid/view/ViewStub;

.field private mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

.field private mImportanceTextView:Landroid/widget/TextView;

.field private mPrivateLayout:Landroid/view/View;

.field private mPublicLayout:Landroid/view/View;

.field private mRowLayout:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

.field private mSelected:Z

.field private mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mSetImportView:Landroid/view/View;

.field private mShowingPublic:Z

.field private mUserExpanded:Z

.field private mVeto:Landroid/view/View;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mAppName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Lcom/android/systemui/statusbar/LatestItemView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Lcom/android/systemui/statusbar/ExpandedNotification;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandedNotification:Lcom/android/systemui/statusbar/ExpandedNotification;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Lcom/android/systemui/statusbar/policy/FilterInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Lcom/android/systemui/statusbar/phone/NotificationGroupManager;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupManager:Lcom/android/systemui/statusbar/phone/NotificationGroupManager;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Lcom/android/systemui/statusbar/policy/NotificationRowLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mRowLayout:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    return-object v0
.end method

.method static synthetic -get9(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)Lcom/android/systemui/statusbar/phone/PhoneStatusBar;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/ExpandableNotificationRow;Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;)Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    return-object p1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterTouchListener:Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;

    new-instance v0, Lcom/android/systemui/statusbar/ExpandableNotificationRow$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$1;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterTouchListener:Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;

    new-instance v0, Lcom/android/systemui/statusbar/ExpandableNotificationRow$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$1;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterTouchListener:Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;

    new-instance v0, Lcom/android/systemui/statusbar/ExpandableNotificationRow$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$1;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private clearViewOutlineProvider()V
    .locals 10

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-ge v5, v6, :cond_0

    return-void

    :cond_0
    sget-object v5, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSetOutlineProvider:Ljava/lang/reflect/Method;

    if-nez v5, :cond_1

    :try_start_0
    const-string/jumbo v5, "android.view.ViewOutlineProvider"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    sput-object v5, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mViewOutlineProviderClazz:Ljava/lang/Class;

    const-class v5, Landroid/view/View;

    const-string/jumbo v6, "setOutlineProvider"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    sget-object v8, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mViewOutlineProviderClazz:Ljava/lang/Class;

    const/4 v9, 0x0

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSetOutlineProvider:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :try_start_1
    sget-object v6, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSetOutlineProvider:Ljava/lang/reflect/Method;

    const/4 v5, 0x1

    new-array v7, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    check-cast v5, Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-virtual {v6, p0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    return-void

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    return-void

    :catch_2
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method private setLayoutParamsIfNeed(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method private updateContentVisibility()V
    .locals 5

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getGroupChildrenCount()I

    move-result v1

    const/4 v4, 0x1

    if-le v1, v4, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mPublicLayout:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mShowingPublic:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mPrivateLayout:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mShowingPublic:Z

    if-nez v1, :cond_3

    xor-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mShowingPublic:Z

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    :goto_3
    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3
.end method

.method private updateVetoVisibility()V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getGroupChildren()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/NotificationData$Entry;

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandedNotification;->isClearable()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v3, 0x1

    :cond_1
    iget-object v5, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mVeto:Landroid/view/View;

    if-eqz v3, :cond_2

    const/16 v4, 0x8

    :goto_0
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public canSetImportance()Z
    .locals 2

    invoke-static {}, Lcom/android/systemui/Util;->isUserFold()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "com.android.systemui"

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-static {}, Lcom/android/systemui/Util;->isUserFold()Z

    move-result v0

    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mDisabled:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getClipTopAmount()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mClipTopAmount:I

    return v0
.end method

.method public getGroupChildren()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/statusbar/NotificationData$Entry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupManager:Lcom/android/systemui/statusbar/phone/NotificationGroupManager;

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandedNotification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/NotificationGroupManager;->getGroupChildren(Lcom/android/systemui/statusbar/ExpandedNotification;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getGroupChildrenCount()I
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getGroupChildren()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getGroupSummaryView()Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    return-object v0
.end method

.method public getGroupSummaryViewHeight()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getNaturalHeight()I

    move-result v0

    :cond_0
    return v0
.end method

.method public getMessageId()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/FilterInfo;->getMessageId()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getNotification()Lcom/android/systemui/statusbar/ExpandedNotification;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandedNotification:Lcom/android/systemui/statusbar/ExpandedNotification;

    return-object v0
.end method

.method public getPackageInfo()Lcom/android/systemui/statusbar/policy/FilterInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/FilterInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getPrivateLayout()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mPrivateLayout:Landroid/view/View;

    return-object v0
.end method

.method public getPublicLayout()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mPublicLayout:Landroid/view/View;

    return-object v0
.end method

.method public getUid()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/FilterInfo;->getUid()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOverlappingRendering()Z
    .locals 2

    const-string/jumbo v0, "support_alpha_optimized"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isBlockShow()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSetImportView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onEditModeAnimationEnd(Z)V
    .locals 6

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mDisabled:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0d0316

    :goto_0
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mAppName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, ""

    :goto_1
    aput-object v1, v5, v2

    invoke-virtual {v4, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    if-eqz p1, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const v0, 0x7f0d0315

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mAppName:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/16 v0, 0x8

    goto :goto_2
.end method

.method public onEditModeAnimationStart(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterImage:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mDisabled:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->EDIT_MODE_ALPHA:F

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_0
    return-void

    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public onEditModeAnimationUpdate(F)V
    .locals 4

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSelected:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mDisabled:Z

    if-eqz v3, :cond_1

    :cond_0
    sget v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->EDIT_MODE_ALPHA:F

    sub-float v3, v1, v3

    mul-float/2addr v3, p1

    sub-float/2addr v1, v3

    :cond_1
    invoke-virtual {v2, v1}, Lcom/android/systemui/statusbar/LatestItemView;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setAlpha(F)V

    iget v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterViewWidth:I

    int-to-float v1, v1

    mul-float v0, v1, p1

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getLayoutDirection()I

    move-result v2

    if-nez v2, :cond_2

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/LatestItemView;->setTranslationX(F)V

    return-void

    :cond_2
    neg-float v0, v0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f01b3

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mVeto:Landroid/view/View;

    const v0, 0x7f0f01b7

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mPublicLayout:Landroid/view/View;

    const v0, 0x7f0f01b8

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mPrivateLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterViewWidth:I

    const v0, 0x7f0f0031

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/LatestItemView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    const v0, 0x7f0f01b4

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSetImportView:Landroid/view/View;

    const v0, 0x7f0f01b5

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mImportanceTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSetImportView:Landroid/view/View;

    invoke-virtual {v0, p0, v1}, Lcom/android/systemui/statusbar/LatestItemView;->setViewForBlock(Landroid/view/View;Landroid/view/View;)V

    const v0, 0x7f0f01bb

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    new-instance v1, Lcom/android/systemui/statusbar/ExpandableNotificationRow$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$2;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterTouchListener:Lcom/android/systemui/statusbar/ExpandableNotificationRow$FilterTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    const v1, 0x7f0f01bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterImage:Landroid/widget/ImageView;

    const v0, 0x7f0f01b9

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryStub:Landroid/view/ViewStub;

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryStub:Landroid/view/ViewStub;

    new-instance v1, Lcom/android/systemui/statusbar/ExpandableNotificationRow$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$3;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->clearViewOutlineProvider()V

    return-void
.end method

.method public onGroupChanged()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getGroupChildrenCount()I

    move-result v1

    if-le v1, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryStub:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setLayoutParamsIfNeed(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->updateVetoVisibility()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->updateContentVisibility()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->refresh()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public refreshFilterView(Z)V
    .locals 3

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mRowLayout:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    iget-object v2, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->containFilterPackage(Lcom/android/systemui/statusbar/policy/FilterInfo;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSelected:Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSelected:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mDisabled:Z

    if-eqz v0, :cond_2

    :cond_0
    sget v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->EDIT_MODE_ALPHA:F

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/systemui/statusbar/LatestItemView;->setAlpha(F)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterImage:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSelected:Z

    if-eqz v0, :cond_3

    const v0, 0x7f020061

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterImage:Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mDisabled:Z

    if-eqz v2, :cond_1

    sget v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->EDIT_MODE_ALPHA:F

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const v0, 0x7f020062

    goto :goto_1
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mAppName:Ljava/lang/String;

    return-void
.end method

.method public setClipTopAmount(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mClipTopAmount:I

    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x2

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandedNotification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->updateNotification()V

    return-void
.end method

.method public setPackageInfo(Lcom/android/systemui/statusbar/policy/FilterInfo;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/systemui/Util;->cantBeBlock(Landroid/content/Context;Lcom/android/systemui/statusbar/policy/FilterInfo;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mDisabled:Z

    return-void
.end method

.method public setRowLayout(Landroid/view/ViewGroup;)V
    .locals 1

    instance-of v0, p1, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    iput-object p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mRowLayout:Lcom/android/systemui/statusbar/policy/NotificationRowLayout;

    :cond_0
    return-void
.end method

.method public setService(Lcom/android/systemui/statusbar/BaseStatusBar;)V
    .locals 1

    instance-of v0, p1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iput-object p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getGroupManager()Lcom/android/systemui/statusbar/phone/NotificationGroupManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupManager:Lcom/android/systemui/statusbar/phone/NotificationGroupManager;

    :cond_0
    return-void
.end method

.method public setShowingPublic(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mShowingPublic:Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mShowingPublic:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->updateContentVisibility()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->refresh()V

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mShowingPublic:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setLayoutParamsIfNeed(Z)V

    :cond_0
    return-void
.end method

.method public setUserExpanded(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setUserExpanded(ZZ)V

    return-void
.end method

.method public setUserExpanded(ZZ)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mUserExpanded:Z

    return-void
.end method

.method public showLargeView()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandedNotification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->showLargeNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)V

    return-void
.end method

.method public switchToEditModeIfNeeded(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilter:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    iget v1, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mFilterViewWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/LatestItemView;->setTranslationX(F)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->refreshFilterView(Z)V

    :cond_0
    return-void
.end method

.method public updateNotification()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mExpandedNotification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandedNotification;->isFold()Z

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mSetImportView:Landroid/view/View;

    new-instance v5, Lcom/android/systemui/statusbar/ExpandableNotificationRow$4;

    invoke-direct {v5, p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow$4;-><init>(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/android/systemui/Util;->isUserFold()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/LatestItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d02c9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v3, :cond_2

    const v2, 0x7f02000e

    :goto_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v5

    invoke-virtual {v1, v7, v7, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mImportanceTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v6, v1, v6, v6}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mImportanceTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/LatestItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0340

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mContent:Lcom/android/systemui/statusbar/LatestItemView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/LatestItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0341

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v2, 0x7f02000f

    goto :goto_1
.end method

.method public updateTitle()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->mGroupSummaryView:Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->updateTitle()V

    :cond_0
    return-void
.end method
