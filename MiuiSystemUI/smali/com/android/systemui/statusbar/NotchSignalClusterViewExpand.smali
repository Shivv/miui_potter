.class public Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;
.super Lcom/android/systemui/statusbar/BaseSignalClusterView;
.source "NotchSignalClusterViewExpand.java"


# instance fields
.field private mMobileSignalUpgradeId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgradeId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/statusbar/BaseSignalClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgradeId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/statusbar/BaseSignalClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgradeId:I

    return-void
.end method


# virtual methods
.method protected apply()V
    .locals 7

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mDemoMode:Z

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiGroup:Landroid/view/ViewGroup;

    if-nez v2, :cond_1

    return-void

    :cond_1
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiVisible:Z

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifi:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiStrengthId:I

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiActivity:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiActivityId:I

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiActivity:Landroid/widget/ImageView;

    iget v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiActivityId:I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiApConnectMark:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_9

    :cond_2
    move v2, v4

    :goto_0
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiGroup:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiDescription:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileVisible:[Z

    aget-boolean v2, v2, v3

    if-eqz v2, :cond_e

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mIsAirplaneMode:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobile:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileStrengthId:[I

    aget v5, v5, v3

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileActivity:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileActivityId:[I

    aget v5, v5, v3

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgradeId:I

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileGroup:Landroid/view/ViewGroup;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileTypeDescription:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileDescription:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileTypeId:[I

    aget v2, v2, v3

    invoke-static {v2}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "4G+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileType:Landroid/widget/TextView;

    const-string/jumbo v5, "4G"

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const/4 v0, 0x1

    :cond_4
    :goto_2
    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileType:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiVisible:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileTypeId:[I

    aget v2, v2, v3

    if-nez v2, :cond_c

    :cond_5
    move v2, v4

    :goto_3
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileActivity:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    :goto_4
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_5
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mIsAirplaneMode:Z

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mAirplane:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mAirplaneIconId:I

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    :goto_6
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileVisibleCdma:[Z

    aget-boolean v2, v2, v3

    if-eqz v2, :cond_11

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mIsAirplaneMode:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileGroupCdma:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileEvdo:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileStrengthIdEvdo:[I

    aget v5, v5, v3

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileCdma:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileStrengthIdCdma:[I

    aget v5, v5, v3

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileActivityCdma:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileActivityIdCdma:[I

    aget v5, v5, v3

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgradeCdma:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgradeId:I

    invoke-virtual {p0, v2, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileActivityCdma:Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiVisible:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileActivityIdCdma:[I

    aget v2, v2, v3

    if-nez v2, :cond_10

    :cond_6
    move v2, v4

    :goto_7
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_8
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiNeedVisible:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_7
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mIsAirplaneMode:Z

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mAirplaneModeNeedVisible:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_8
    sget-boolean v2, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mIsSimMissing:[Z

    aget-boolean v2, v2, v3

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWcdmaCardSlot:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mCardSlot:I

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWcdmaCardSlot:Landroid/widget/ImageView;

    const v3, 0x7f02020e

    invoke-virtual {p0, v2, v3}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    :goto_9
    return-void

    :cond_9
    move v2, v3

    goto/16 :goto_0

    :cond_a
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_1

    :cond_b
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_c
    move v2, v3

    goto/16 :goto_3

    :cond_d
    move v2, v4

    goto/16 :goto_4

    :cond_e
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_5

    :cond_f
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_10
    move v2, v3

    goto :goto_7

    :cond_11
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileGroupCdma:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_8

    :cond_12
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWcdmaCardSlot:Landroid/widget/ImageView;

    const v3, 0x7f020210

    invoke-virtual {p0, v2, v3}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateIcon(Landroid/widget/ImageView;I)V

    goto :goto_9

    :cond_13
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mWcdmaCardSlot:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_9
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->onFinishInflate()V

    const v0, 0x7f0f00a5

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    const v0, 0x7f020299

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgradeId:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method
