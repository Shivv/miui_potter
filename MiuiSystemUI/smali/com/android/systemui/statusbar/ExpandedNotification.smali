.class public Lcom/android/systemui/statusbar/ExpandedNotification;
.super Lcom/android/systemui/statusbar/AbstractExpandedNotification;
.source "ExpandedNotification.java"


# static fields
.field public static DEFUALT_TYPE:I

.field public static FLOAT_NOTIFICATION_TYPE:I

.field public static KEYGUARD_NOTIFICATION_TYPE:I

.field private static mWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAboveThreshold:Z

.field private mBelowFoldLimit:Z

.field private mImportance:I

.field private mIsFoldTips:Z

.field private mLocalScore:D

.field private mNewlyNotification:Z

.field private mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

.field private mNotificationType:I

.field private mPushScore:D

.field private mScoreForRank:D

.field private mShowSum:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput v0, Lcom/android/systemui/statusbar/ExpandedNotification;->DEFUALT_TYPE:I

    const/4 v0, 0x1

    sput v0, Lcom/android/systemui/statusbar/ExpandedNotification;->FLOAT_NOTIFICATION_TYPE:I

    const/4 v0, 0x2

    sput v0, Lcom/android/systemui/statusbar/ExpandedNotification;->KEYGUARD_NOTIFICATION_TYPE:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/systemui/statusbar/ExpandedNotification;->mWhiteList:Ljava/util/List;

    sget-object v0, Lcom/android/systemui/statusbar/ExpandedNotification;->mWhiteList:Ljava/util/List;

    const-string/jumbo v1, "com.miui.securitycenter"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/systemui/statusbar/ExpandedNotification;->mWhiteList:Ljava/util/List;

    const-string/jumbo v1, "com.lbe.security.miui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/service/notification/StatusBarNotification;)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/AbstractExpandedNotification;-><init>(Landroid/service/notification/StatusBarNotification;)V

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mAboveThreshold:Z

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mIsFoldTips:Z

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mBelowFoldLimit:Z

    invoke-static {p0, p1}, Lcom/android/systemui/CompatibilityN;->setOverrideGroupKey(Lcom/android/systemui/statusbar/ExpandedNotification;Landroid/service/notification/StatusBarNotification;)V

    new-instance v0, Lcom/android/systemui/analytics/NotificationEvent;

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/analytics/NotificationEvent;-><init>(Landroid/app/Notification;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->caculateScore()V

    return-void
.end method

.method private caculateScore()V
    .locals 8

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-static {v1}, Lcom/android/systemui/PushEvents;->getScoreInfo(Landroid/app/Notification;)Lcom/android/systemui/ScoreInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/systemui/ScoreInfo;->getServerScore()D

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/systemui/statusbar/ExpandedNotification;->setPushScore(D)V

    invoke-virtual {v0}, Lcom/android/systemui/ScoreInfo;->getThreshold()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v1, v4, v6

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/systemui/ScoreInfo;->getServerScore()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/android/systemui/ScoreInfo;->getThreshold()D

    move-result-wide v6

    cmpg-double v1, v4, v6

    if-gez v1, :cond_2

    move v1, v2

    :goto_0
    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/ExpandedNotification;->setAboveThreshold(Z)V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->generateScoreForRank()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->isFold()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/android/systemui/analytics/NotificationEvent;->setFold(Z)V

    invoke-static {p0}, Lcom/android/systemui/Util;->isFoldTips(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mIsFoldTips:Z

    iget-object v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mIsFoldTips:Z

    invoke-virtual {v1, v4}, Lcom/android/systemui/analytics/NotificationEvent;->setFoldTips(Z)V

    sget-boolean v1, Lcom/android/systemui/analytics/NotificationEventSender;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "ExpandedNotification"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "scoreInfo == null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v0, :cond_5

    :goto_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "push score:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPushScore()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", local score:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getLocalScore()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", pkg:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "com.xiaomi.xmsf"

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getBasePkg()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->noScore()V

    :cond_4
    invoke-static {p0}, Lcom/android/systemui/statusbar/LocalAlgoModel;->getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/systemui/statusbar/ExpandedNotification;->setLocalScore(D)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getLocalScore()D

    move-result-wide v4

    invoke-static {}, Lcom/android/systemui/statusbar/LocalAlgoModel;->getThreshold()D

    move-result-wide v6

    cmpg-double v1, v4, v6

    if-gez v1, :cond_0

    invoke-static {}, Lcom/android/systemui/statusbar/LocalAlgoModel;->isLocalModelAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/ExpandedNotification;->setAboveThreshold(Z)V

    goto/16 :goto_1

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method private checkWhiteList()Z
    .locals 2

    sget-object v0, Lcom/android/systemui/statusbar/ExpandedNotification;->mWhiteList:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/ExpandedNotification;->mWhiteList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private generateScoreForRank()V
    .locals 4

    iget-wide v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mPushScore:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mPushScore:D

    iput-wide v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mScoreForRank:D

    :goto_0
    return-void

    :cond_0
    iget-wide v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mLocalScore:D

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/LocalAlgoModel;->getScoreForRank(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mScoreForRank:D

    goto :goto_0
.end method

.method private setAboveThreshold(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mAboveThreshold:Z

    return-void
.end method

.method private setLocalScore(D)V
    .locals 1

    iput-wide p1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mLocalScore:D

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/analytics/NotificationEvent;->setLocalScore(D)V

    return-void
.end method

.method private setPushScore(D)V
    .locals 5

    iput-wide p1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mPushScore:D

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iget-wide v2, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mPushScore:D

    invoke-virtual {v0, v2, v3}, Lcom/android/systemui/analytics/NotificationEvent;->setPushScore(D)V

    return-void
.end method


# virtual methods
.method public getBasePkg()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/android/systemui/statusbar/AbstractExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImportance()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mImportance:I

    return v0
.end method

.method public getLocalScore()D
    .locals 2

    iget-wide v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mLocalScore:D

    return-wide v0
.end method

.method public getNotificationEvent()Lcom/android/systemui/analytics/NotificationEvent;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    return-object v0
.end method

.method public getNotificationKey()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/systemui/CompatibilityL;->getKey(Lcom/android/systemui/statusbar/ExpandedNotification;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationType()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationType:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iget-object v0, v0, Landroid/app/Notification;->extraNotification:Landroid/app/MiuiNotification;

    invoke-virtual {v0}, Landroid/app/MiuiNotification;->getTargetPkg()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iget-object v0, v0, Landroid/app/Notification;->extraNotification:Landroid/app/MiuiNotification;

    invoke-virtual {v0}, Landroid/app/MiuiNotification;->getTargetPkg()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/android/systemui/statusbar/AbstractExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPushScore()D
    .locals 2

    iget-wide v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mPushScore:D

    return-wide v0
.end method

.method public getScoreForRank()D
    .locals 2

    iget-wide v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mScoreForRank:D

    return-wide v0
.end method

.method public getShowSum()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mShowSum:I

    return v0
.end method

.method public isFold()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mIsFoldTips:Z

    if-eqz v1, :cond_0

    return v0

    :cond_0
    invoke-static {}, Lcom/android/systemui/Util;->isUserFold()Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    :cond_1
    iget v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mImportance:I

    if-nez v1, :cond_3

    invoke-direct {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->checkWhiteList()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    iget v1, v1, Landroid/app/Notification;->priority:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    return v0

    :cond_2
    iget-boolean v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mBelowFoldLimit:Z

    if-eqz v1, :cond_5

    return v0

    :cond_3
    iget v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mImportance:I

    if-lez v1, :cond_4

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->isClearable()Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    :cond_6
    const-string/jumbo v1, "android"

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "com.android.systemui"

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    return v0

    :cond_8
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mAboveThreshold:Z

    return v0
.end method

.method public isNewlyNotification()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNewlyNotification:Z

    return v0
.end method

.method public noScore()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    invoke-virtual {v0}, Lcom/android/systemui/analytics/NotificationEvent;->setNoScore()V

    return-void
.end method

.method public setBelowFoldLimit(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mBelowFoldLimit:Z

    return-void
.end method

.method public setImportance(I)V
    .locals 2

    iput p1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mImportance:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iget v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mImportance:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/analytics/NotificationEvent;->setImportance(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/ExpandedNotification;->isFold()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/analytics/NotificationEvent;->setFold(Z)V

    return-void
.end method

.method public setNewlyNotification(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNewlyNotification:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNewlyNotification:Z

    invoke-virtual {v0, v1}, Lcom/android/systemui/analytics/NotificationEvent;->setNewly(Z)V

    return-void
.end method

.method public setNotificationType(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mNotificationType:I

    return-void
.end method

.method public setShowSum(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/ExpandedNotification;->mShowSum:I

    return-void
.end method
