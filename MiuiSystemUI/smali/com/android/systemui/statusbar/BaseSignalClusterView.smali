.class public Lcom/android/systemui/statusbar/BaseSignalClusterView;
.super Landroid/widget/LinearLayout;
.source "BaseSignalClusterView.java"

# interfaces
.implements Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;


# static fields
.field private static sFilterColor:I

.field private static sMapping:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected static sNotch:Z


# instance fields
.field cm:Landroid/net/ConnectivityManager;

.field mAirplane:Landroid/widget/ImageView;

.field protected mAirplaneIconId:I

.field protected mAirplaneModeNeedVisible:Z

.field protected mCardSlot:I

.field mCarrierCdmaLabel:Landroid/widget/TextView;

.field mCarrierLabel:Landroid/widget/TextView;

.field protected mDemoMode:Z

.field protected mEnableDarkMode:Z

.field protected mIsAirplaneMode:Z

.field protected mIsSimMissing:[Z

.field mMobile:Landroid/widget/ImageView;

.field mMobileActivity:Landroid/widget/ImageView;

.field mMobileActivityCdma:Landroid/widget/ImageView;

.field protected mMobileActivityId:[I

.field protected mMobileActivityIdCdma:[I

.field mMobileCdma:Landroid/widget/ImageView;

.field protected mMobileDescription:[Ljava/lang/String;

.field mMobileEvdo:Landroid/widget/ImageView;

.field mMobileGroup:Landroid/view/ViewGroup;

.field mMobileGroupCdma:Landroid/view/ViewGroup;

.field mMobileRoam:Landroid/widget/ImageView;

.field mMobileSignalUpgrade:Landroid/widget/ImageView;

.field mMobileSignalUpgradeCdma:Landroid/widget/ImageView;

.field protected mMobileStrengthId:[I

.field protected mMobileStrengthIdCdma:[I

.field protected mMobileStrengthIdEvdo:[I

.field mMobileType:Landroid/widget/TextView;

.field mMobileTypeCdma:Landroid/widget/TextView;

.field protected mMobileTypeDescription:[Ljava/lang/String;

.field mMobileTypeEvdo:Landroid/widget/TextView;

.field protected mMobileTypeEvdoId:[I

.field protected mMobileTypeId:[I

.field protected mMobileVisible:[Z

.field protected mMobileVisibleCdma:[Z

.field mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

.field private mNotchExpanded:Z

.field mNotchVolte:Landroid/widget/ImageView;

.field mSpeechHd:Landroid/widget/ImageView;

.field private mSupportCA:Z

.field protected mViewId:I

.field protected mViewId2:I

.field mVolte:Landroid/widget/TextView;

.field mVolteNoService:Landroid/widget/ImageView;

.field mWcdmaCardSlot:Landroid/widget/ImageView;

.field mWifi:Landroid/widget/ImageView;

.field mWifiActivity:Landroid/widget/ImageView;

.field protected mWifiActivityId:I

.field mWifiAp:Landroid/widget/ImageView;

.field mWifiApConnectMark:Landroid/widget/ImageView;

.field protected mWifiDescription:Ljava/lang/String;

.field mWifiGroup:Landroid/view/ViewGroup;

.field mWifiLabel:Landroid/widget/TextView;

.field protected mWifiNeedVisible:Z

.field protected mWifiStrengthId:I

.field protected mWifiVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sMapping:Landroid/util/SparseArray;

    sget-object v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sMapping:Landroid/util/SparseArray;

    const v1, 0x7f020273

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020271

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sMapping:Landroid/util/SparseArray;

    const v1, 0x7f020277

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020275

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sMapping:Landroid/util/SparseArray;

    const v1, 0x7f02027b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020279

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sMapping:Landroid/util/SparseArray;

    const v1, 0x7f02027f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f02027d

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sMapping:Landroid/util/SparseArray;

    const v1, 0x7f020283

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020281

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sMapping:Landroid/util/SparseArray;

    const v1, 0x7f020287

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020285

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sNotch:Z

    const/4 v0, 0x0

    sput v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sFilterColor:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mViewId:I

    iput v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mViewId2:I

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiNeedVisible:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiVisible:Z

    iput v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiStrengthId:I

    iput v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiActivityId:I

    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileVisible:[Z

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileStrengthId:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileActivityId:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeId:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeEvdoId:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileStrengthIdEvdo:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileStrengthIdCdma:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileActivityIdCdma:[I

    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileVisibleCdma:[Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mAirplaneModeNeedVisible:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mIsAirplaneMode:Z

    iput v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mAirplaneIconId:I

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileDescription:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeDescription:[Ljava/lang/String;

    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mIsSimMissing:[Z

    iput v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCardSlot:I

    const-string/jumbo v0, "support_ca"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mSupportCA:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->cm:Landroid/net/ConnectivityManager;

    return-void
.end method


# virtual methods
.method protected apply()V
    .locals 0

    return-void
.end method

.method public dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    const-string/jumbo v0, "demo_mode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SignalClusterView mDemoMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", command = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-nez v0, :cond_1

    const-string/jumbo v0, "enter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiAp:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifi:Landroid/widget/ImageView;

    const v1, 0x7f0202c8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mEnableDarkMode:Z

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mSpeechHd:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileRoam:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobile:Landroid/widget/ImageView;

    const v1, 0x7f020281

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mEnableDarkMode:Z

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWcdmaCardSlot:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroupCdma:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolte:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolteNoService:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "exit"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    sget-boolean v0, Lmiui/os/Build;->IS_CTA_BUILD:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCardSlot:I

    invoke-virtual {v0, p0, v1}, Lcom/android/systemui/statusbar/policy/NetworkController;->refreshSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->apply()V

    goto :goto_0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileVisible:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public getViewId()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mViewId:I

    return v0
.end method

.method public getViewId2()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mViewId:I

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->apply()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifi:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobile:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileType:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroupCdma:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileCdma:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileActivityCdma:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeCdma:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileEvdo:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeEvdo:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileRoam:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolte:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolteNoService:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mSpeechHd:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiLabel:Landroid/widget/TextView;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0f00b2

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    const v0, 0x7f0f00b3

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifi:Landroid/widget/ImageView;

    const v0, 0x7f0f00b4

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    const v0, 0x7f0f00b1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiAp:Landroid/widget/ImageView;

    const v0, 0x7f0f00b5

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    const v0, 0x7f0f009b

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    const v0, 0x7f0f009d

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobile:Landroid/widget/ImageView;

    const v0, 0x7f0f00a4

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    const v0, 0x7f0f00a3

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileType:Landroid/widget/TextView;

    const v0, 0x7f0f00a7

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileGroupCdma:Landroid/view/ViewGroup;

    const v0, 0x7f0f00aa

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileCdma:Landroid/widget/ImageView;

    const v0, 0x7f0f00ad

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileActivityCdma:Landroid/widget/ImageView;

    const v0, 0x7f0f00ab

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeCdma:Landroid/widget/TextView;

    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileEvdo:Landroid/widget/ImageView;

    const v0, 0x7f0f00a9

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeEvdo:Landroid/widget/TextView;

    const v0, 0x7f0f00a6

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    const v0, 0x7f0f009a

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileRoam:Landroid/widget/ImageView;

    const v0, 0x7f0f00af

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolte:Landroid/widget/TextView;

    const v0, 0x7f0f00b0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolteNoService:Landroid/widget/ImageView;

    const v0, 0x7f0f009c

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    const v0, 0x7f0f0099

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mSpeechHd:Landroid/widget/ImageView;

    const v0, 0x7f0f00a2

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    const v0, 0x7f0f00ac

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    const v0, 0x7f0f00b6

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiLabel:Landroid/widget/TextView;

    const v0, 0x7f0f009e

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWcdmaCardSlot:Landroid/widget/ImageView;

    return-void
.end method

.method public setAirplaneModeNeedVisible(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mAirplaneModeNeedVisible:Z

    return-void
.end method

.method public setCardNum(I)V
    .locals 0

    return-void
.end method

.method public setCardSlot(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCardSlot:I

    return-void
.end method

.method public setIsActiveNetworkMetered(Z)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    if-eqz p1, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    iget v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiActivityId:I

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public setIsAirplaneMode(ZI)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mIsAirplaneMode:Z

    iput p2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mAirplaneIconId:I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->apply()V

    return-void
.end method

.method public setIsImsRegisted(Z)V
    .locals 3

    const/16 v1, 0x8

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v2, :cond_0

    return-void

    :cond_0
    sget-boolean v2, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sNotch:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolte:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public setIsRoaming(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileRoam:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setMobileDataIndicators(IZIIILjava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    move-object v6, p7

    move/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->setMobileDataIndicators(ZIIILjava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public setMobileDataIndicators(ZIIILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileVisible:[Z

    aput-boolean p1, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileVisibleCdma:[Z

    aput-boolean v1, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileStrengthId:[I

    aput p2, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileActivityId:[I

    aput p3, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeId:[I

    aput p4, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileDescription:[Ljava/lang/String;

    aput-object p5, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeDescription:[Ljava/lang/String;

    aput-object p6, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mIsSimMissing:[Z

    aput-boolean p7, v0, v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->apply()V

    return-void
.end method

.method public setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V
    .locals 2

    iput-object p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/NetworkController;->hasMobileDataFeature()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/NetworkController;->addMobileLabelView(Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/NetworkController;->addMobileLabelView(Landroid/widget/TextView;)V

    sget-boolean v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sNotch:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/NetworkController;->addMobileUpgradeView(Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileSignalUpgradeCdma:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/NetworkController;->addMobileUpgradeView(Landroid/widget/ImageView;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/policy/NetworkController;->addSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/NetworkController;->addWifiLabelView(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method public setNotchExpandedStatusbar(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNotchExpanded:Z

    return-void
.end method

.method public setSlotId(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setSpeechHd(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mSpeechHd:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setViewId(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mViewId:I

    return-void
.end method

.method public setVisible(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->setVisibility(I)V

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setVolteNoService(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolteNoService:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setWifiApEnabled(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiAp:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiNeedVisible:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setWifiIndicators(ZIILjava/lang/String;)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiVisible:Z

    iput p2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiStrengthId:I

    iput p3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiActivityId:I

    iput-object p4, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiDescription:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->apply()V

    return-void
.end method

.method public setWifiNeedVisible(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiNeedVisible:Z

    return-void
.end method

.method public updateDarkMode(Z)V
    .locals 3

    const v0, 0x7f090008

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mEnableDarkMode:Z

    if-eq v1, p1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mEnableDarkMode:Z

    sget-boolean v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sMiuiOptimizationDisabled:Z

    if-eqz v1, :cond_4

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-le v1, v2, :cond_4

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f090025

    :cond_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeEvdo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileTypeCdma:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileType:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolte:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileRoam:Landroid/widget/ImageView;

    const v1, 0x7f020246

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mSpeechHd:Landroid/widget/ImageView;

    const v1, 0x7f02029d

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    const v1, 0x7f0202b3

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiAp:Landroid/widget/ImageView;

    const v1, 0x7f0202b5

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    const v1, 0x7f02028d

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    sget-boolean v0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sNotch:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    const v1, 0x7f020299

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mVolteNoService:Landroid/widget/ImageView;

    const v1, 0x7f0202af

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifi:Landroid/widget/ImageView;

    const v1, 0x7f0202c8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mEnableDarkMode:Z

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mMobile:Landroid/widget/ImageView;

    const v1, 0x7f020285

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mEnableDarkMode:Z

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->apply()V

    :cond_3
    return-void

    :cond_4
    iget-object v1, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_5

    const v0, 0x7f090009

    :cond_5
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0
.end method

.method protected updateIcon(Landroid/widget/ImageView;I)V
    .locals 4

    const/4 v2, 0x0

    if-nez p2, :cond_0

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mEnableDarkMode:Z

    invoke-static {v2, v3}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mEnableDarkMode:Z

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sMiuiOptimizationDisabled:Z

    if-eqz v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x19

    if-le v2, v3, :cond_2

    sget v2, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sFilterColor:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sFilterColor:I

    :cond_1
    sget v2, Lcom/android/systemui/statusbar/BaseSignalClusterView;->sFilterColor:I

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_2
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public updateLabelVisible(Z)V
    .locals 5

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mCarrierCdmaLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiLabel:Landroid/widget/TextView;

    if-eqz p1, :cond_4

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseSignalClusterView;->mWifiLabel:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_4

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method
