.class Lcom/android/systemui/statusbar/SearchView$5;
.super Ljava/lang/Object;
.source "SearchView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/SearchView;->checkScanner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/SearchView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/SearchView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/SearchView$5;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v3, p0, Lcom/android/systemui/statusbar/SearchView$5;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-static {v3}, Lcom/android/systemui/statusbar/SearchView;->-get1(Lcom/android/systemui/statusbar/SearchView;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v3, p0, Lcom/android/systemui/statusbar/SearchView$5;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-virtual {v3, v2}, Lcom/android/systemui/statusbar/SearchView;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v3, p0, Lcom/android/systemui/statusbar/SearchView$5;->this$0:Lcom/android/systemui/statusbar/SearchView;

    new-instance v4, Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v1

    iget v6, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v1

    iget-object v7, p0, Lcom/android/systemui/statusbar/SearchView$5;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-virtual {v7}, Lcom/android/systemui/statusbar/SearchView;->getHeight()I

    move-result v7

    const/4 v8, 0x0

    invoke-direct {v4, v5, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v3, v4}, Lcom/android/systemui/statusbar/SearchView;->-set0(Lcom/android/systemui/statusbar/SearchView;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    return-void
.end method
