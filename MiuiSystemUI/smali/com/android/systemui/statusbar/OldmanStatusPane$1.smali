.class Lcom/android/systemui/statusbar/OldmanStatusPane$1;
.super Ljava/lang/Object;
.source "OldmanStatusPane.java"

# interfaces
.implements Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/OldmanStatusPane;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/OldmanStatusPane;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private _slotIdToIndex(I)I
    .locals 0

    return p1
.end method


# virtual methods
.method public setIsAirplaneMode(Z)V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v0, "NetworkControllerGeminiOldman"

    const-string/jumbo v1, "setIsAirplaneMode(%s)"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set0(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, v5}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set1(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public setIsImsRegisted(IZ)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->_slotIdToIndex(I)I

    move-result v0

    if-gez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get5(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z

    move-result-object v1

    aput-boolean p2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get3(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z

    move-result-object v1

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public setIsRoaming(IZ)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->_slotIdToIndex(I)I

    move-result v0

    if-gez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get6(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z

    move-result-object v1

    aput-boolean p2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get3(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z

    move-result-object v1

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void
.end method

.method public setMobileDataIndicators(IZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;ZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string/jumbo v1, "NetworkControllerGeminiOldman"

    const-string/jumbo v2, "setMobileDataIndicators(%s, %s, %s, %s, %s)"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    aput-object p3, v3, v4

    const/4 v4, 0x3

    aput-object p5, v3, v4

    const/4 v4, 0x4

    aput-object p6, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->_slotIdToIndex(I)I

    move-result v0

    if-gez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get4(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z

    move-result-object v1

    aput-boolean p2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get2(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-result-object v1

    if-eqz p3, :cond_1

    :goto_0
    aput-object p3, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get1(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    move-result-object v1

    if-eqz p4, :cond_2

    if-eqz p6, :cond_2

    :goto_1
    aput-object p6, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get0(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-result-object v1

    if-eqz p4, :cond_3

    if-eqz p5, :cond_3

    :goto_2
    aput-object p5, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-get3(Lcom/android/systemui/statusbar/OldmanStatusPane;)[Z

    move-result-object v1

    aput-boolean v6, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void

    :cond_1
    sget-object p3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    goto :goto_0

    :cond_2
    sget-object p6, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    goto :goto_1

    :cond_3
    sget-object p5, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    goto :goto_2
.end method

.method public setWifiIndicators(ZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;)V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v0, "NetworkControllerGeminiOldman"

    const-string/jumbo v1, "setWifiIndicators(%s, %s, %s)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    aput-object p2, v2, v5

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set26(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    if-eqz p2, :cond_0

    :goto_0
    invoke-static {v0, p2}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set24(Lcom/android/systemui/statusbar/OldmanStatusPane;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    if-eqz p3, :cond_1

    :goto_1
    invoke-static {v0, p3}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set23(Lcom/android/systemui/statusbar/OldmanStatusPane;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0, v5}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-set25(Lcom/android/systemui/statusbar/OldmanStatusPane;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/OldmanStatusPane$1;->this$0:Lcom/android/systemui/statusbar/OldmanStatusPane;

    invoke-static {v0}, Lcom/android/systemui/statusbar/OldmanStatusPane;->-wrap0(Lcom/android/systemui/statusbar/OldmanStatusPane;)V

    return-void

    :cond_0
    sget-object p2, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    goto :goto_0

    :cond_1
    sget-object p3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    goto :goto_1
.end method
