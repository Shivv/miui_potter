.class public Lcom/android/systemui/statusbar/EntryComparator;
.super Ljava/lang/Object;
.source "EntryComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/systemui/statusbar/NotificationData$Entry;",
        ">;"
    }
.end annotation


# static fields
.field private static final SUPPORT_HIGH_PRIORITY:Z

.field private static sGap:J

.field private static sSoftDelay:J

.field private static sTime:J


# instance fields
.field private mHighPriorityMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mUsbNotificationController:Lcom/android/systemui/statusbar/policy/UsbNotificationController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/systemui/statusbar/EntryComparator;->sTime:J

    sget-wide v0, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->sGap:J

    sput-wide v0, Lcom/android/systemui/statusbar/EntryComparator;->sGap:J

    sget-wide v0, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->sNewNotification:J

    sput-wide v0, Lcom/android/systemui/statusbar/EntryComparator;->sSoftDelay:J

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/systemui/statusbar/EntryComparator;->SUPPORT_HIGH_PRIORITY:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/EntryComparator;->mHighPriorityMap:Ljava/util/HashMap;

    return-void
.end method

.method private getHighPriority(Ljava/lang/String;I)Z
    .locals 1

    sget-boolean v0, Lcom/android/systemui/statusbar/EntryComparator;->SUPPORT_HIGH_PRIORITY:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/EntryComparator;->mHighPriorityMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/android/systemui/statusbar/EntryComparator;->updatePriority(Ljava/lang/String;I)Z

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/EntryComparator;->mHighPriorityMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static isNewNotification(Lcom/android/systemui/statusbar/NotificationData$Entry;J)Z
    .locals 7

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    iget-wide v2, v1, Landroid/app/Notification;->when:J

    sub-long v2, p1, v2

    sget-wide v4, Lcom/android/systemui/statusbar/EntryComparator;->sSoftDelay:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    const/4 v1, 0x0

    return v1
.end method


# virtual methods
.method public compare(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I
    .locals 24

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    move-object/from16 v16, v0

    if-nez v15, :cond_0

    if-nez v16, :cond_0

    const/16 v20, 0x0

    return v20

    :cond_0
    if-nez v15, :cond_1

    const/16 v20, -0x1

    return v20

    :cond_1
    if-nez v16, :cond_2

    const/16 v20, 0x1

    return v20

    :cond_2
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/systemui/Util;->isFoldTips(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v20

    if-eqz v20, :cond_3

    const/16 v20, -0x1

    return v20

    :cond_3
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/systemui/Util;->isFoldTips(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v20

    if-eqz v20, :cond_4

    const/16 v20, 0x1

    return v20

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/EntryComparator;->mUsbNotificationController:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->isUsbNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v20

    if-eqz v20, :cond_5

    const/4 v9, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/EntryComparator;->mUsbNotificationController:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->isUsbNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v20

    if-eqz v20, :cond_6

    const/4 v13, 0x1

    :goto_1
    sub-int v20, v9, v13

    if-eqz v20, :cond_7

    sub-int v20, v9, v13

    invoke-static/range {v20 .. v20}, Lcom/android/systemui/statusbar/phone/rank/BooleanCheck;->check(I)I

    move-result v20

    return v20

    :cond_5
    const/4 v9, 0x0

    goto :goto_0

    :cond_6
    const/4 v13, 0x0

    goto :goto_1

    :cond_7
    invoke-virtual {v15}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual {v15}, Lcom/android/systemui/statusbar/ExpandedNotification;->getUid()I

    move-result v21

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/EntryComparator;->getHighPriority(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_8

    const/4 v8, 0x1

    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v16 .. v16}, Lcom/android/systemui/statusbar/ExpandedNotification;->getUid()I

    move-result v21

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/EntryComparator;->getHighPriority(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_9

    const/4 v12, 0x1

    :goto_3
    sub-int v20, v8, v12

    if-eqz v20, :cond_a

    sub-int v20, v8, v12

    invoke-static/range {v20 .. v20}, Lcom/android/systemui/statusbar/phone/rank/BooleanCheck;->check(I)I

    move-result v20

    return v20

    :cond_8
    const/4 v8, 0x0

    goto :goto_2

    :cond_9
    const/4 v12, 0x0

    goto :goto_3

    :cond_a
    sget-wide v20, Lcom/android/systemui/statusbar/EntryComparator;->sTime:J

    move-object/from16 v0, p1

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Lcom/android/systemui/statusbar/EntryComparator;->isNewNotification(Lcom/android/systemui/statusbar/NotificationData$Entry;J)Z

    move-result v17

    sget-wide v20, Lcom/android/systemui/statusbar/EntryComparator;->sTime:J

    move-object/from16 v0, p2

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Lcom/android/systemui/statusbar/EntryComparator;->isNewNotification(Lcom/android/systemui/statusbar/NotificationData$Entry;J)Z

    move-result v18

    if-eqz v17, :cond_b

    if-eqz v18, :cond_b

    invoke-static/range {p1 .. p2}, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->compareByTime(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I

    move-result v20

    return v20

    :cond_b
    if-eqz v17, :cond_c

    const/16 v20, 0x1

    return v20

    :cond_c
    if-eqz v18, :cond_d

    const/16 v20, -0x1

    return v20

    :cond_d
    sget-wide v20, Lcom/android/systemui/statusbar/EntryComparator;->sTime:J

    sget-wide v22, Lcom/android/systemui/statusbar/EntryComparator;->sGap:J

    move-object/from16 v0, p1

    move-wide/from16 v1, v20

    move-wide/from16 v3, v22

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->getGapSize(Lcom/android/systemui/statusbar/NotificationData$Entry;JJ)J

    move-result-wide v6

    sget-wide v20, Lcom/android/systemui/statusbar/EntryComparator;->sTime:J

    sget-wide v22, Lcom/android/systemui/statusbar/EntryComparator;->sGap:J

    move-object/from16 v0, p2

    move-wide/from16 v1, v20

    move-wide/from16 v3, v22

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->getGapSize(Lcom/android/systemui/statusbar/NotificationData$Entry;JJ)J

    move-result-wide v10

    cmp-long v20, v6, v10

    if-eqz v20, :cond_f

    cmp-long v20, v6, v10

    if-gez v20, :cond_e

    const/16 v20, 0x1

    :goto_4
    return v20

    :cond_e
    const/16 v20, -0x1

    goto :goto_4

    :cond_f
    invoke-static {}, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->getInstance()Lcom/android/systemui/statusbar/phone/rank/RankConfig;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->getRankAlogrithm()Lcom/android/systemui/statusbar/phone/rank/IRank;

    move-result-object v14

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v14, v0, v1}, Lcom/android/systemui/statusbar/phone/rank/IRank;->compare(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Lcom/android/systemui/statusbar/phone/rank/BooleanCheck;->check(I)I

    move-result v20

    return v20
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/systemui/statusbar/NotificationData$Entry;

    check-cast p2, Lcom/android/systemui/statusbar/NotificationData$Entry;

    invoke-virtual {p0, p1, p2}, Lcom/android/systemui/statusbar/EntryComparator;->compare(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I

    move-result v0

    return v0
.end method

.method public setUsbNotificationController(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/EntryComparator;->mUsbNotificationController:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    return-void
.end method

.method public updatePriority(Ljava/lang/String;I)Z
    .locals 5

    sget-boolean v3, Lcom/android/systemui/statusbar/EntryComparator;->SUPPORT_HIGH_PRIORITY:Z

    if-nez v3, :cond_0

    const/4 v3, 0x0

    return v3

    :cond_0
    :try_start_0
    invoke-static {p1, p2}, Lcom/android/systemui/SystemUICompatibility;->isHighPriority(Ljava/lang/String;I)Z

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/systemui/statusbar/EntryComparator;->mHighPriorityMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/systemui/statusbar/EntryComparator;->mHighPriorityMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public updateTime()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/systemui/statusbar/EntryComparator;->sTime:J

    sget-wide v0, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->sGap:J

    sput-wide v0, Lcom/android/systemui/statusbar/EntryComparator;->sGap:J

    sget-wide v0, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->sNewNotification:J

    sput-wide v0, Lcom/android/systemui/statusbar/EntryComparator;->sSoftDelay:J

    return-void
.end method
