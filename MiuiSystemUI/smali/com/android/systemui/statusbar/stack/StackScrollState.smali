.class public Lcom/android/systemui/statusbar/stack/StackScrollState;
.super Ljava/lang/Object;
.source "StackScrollState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;
    }
.end annotation


# instance fields
.field private final mClipRect:Landroid/graphics/Rect;

.field private final mHostView:Landroid/view/ViewGroup;

.field private mStateMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mClipRect:Landroid/graphics/Rect;

    iput-object p1, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mHostView:Landroid/view/ViewGroup;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mStateMap:Ljava/util/Map;

    return-void
.end method

.method private updateChildClip(Landroid/view/View;IF)V
    .locals 4

    int-to-float v1, p2

    sub-float/2addr v1, p3

    float-to-int v0, v1

    iget-object v1, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v0, v2, p2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/view/View;->setClipBounds(Landroid/graphics/Rect;)V

    return-void
.end method

.method private updateChildClippingAndBackground(Lcom/android/systemui/statusbar/ExpandableNotificationRow;IFI)V
    .locals 1

    int-to-float v0, p2

    cmpl-float v0, v0, p3

    if-lez v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/statusbar/stack/StackScrollState;->updateChildClip(Landroid/view/View;IF)V

    :goto_0
    if-le p2, p4, :cond_1

    sub-int v0, p2, p4

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setClipTopAmount(I)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setClipBounds(Landroid/graphics/Rect;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setClipTopAmount(I)V

    goto :goto_1
.end method


# virtual methods
.method public apply()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/stack/StackScrollState;->apply(Z)V

    return-void
.end method

.method public apply(Z)V
    .locals 27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mHostView:Landroid/view/ViewGroup;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v15

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v18, 0x0

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v15, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mHostView:Landroid/view/ViewGroup;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    const/4 v4, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v22

    instance-of v0, v0, Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    move/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mStateMap:Ljava/util/Map;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mHostView:Landroid/view/ViewGroup;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    :cond_0
    if-nez v21, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_2
    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->gone:Z

    move/from16 v25, v0

    if-nez v25, :cond_1

    move-object/from16 v0, v21

    iget v9, v0, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->height:I

    move-object/from16 v0, v21

    iget v13, v0, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->yTranslation:F

    if-eqz p1, :cond_7

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getAlpha()F

    move-result v2

    invoke-static {v4}, Lcom/android/systemui/SystemUICompatibility;->getTranslationZ(Landroid/view/View;)F

    move-result v24

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getTranslationY()F

    move-result v23

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getScaleX()F

    move-result v20

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getHeight()I

    move-result v6

    move-object/from16 v0, v21

    iget v8, v0, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->alpha:F

    move-object/from16 v0, v21

    iget v14, v0, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->zTranslation:F

    move-object/from16 v0, v21

    iget v11, v0, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->scale:F

    const/16 v25, 0x0

    cmpl-float v25, v8, v25

    if-nez v25, :cond_8

    const/4 v3, 0x1

    :goto_2
    cmpl-float v25, v2, v8

    if-eqz v25, :cond_3

    if-nez v3, :cond_3

    invoke-virtual {v4, v8}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setAlpha(F)V

    :cond_3
    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getVisibility()I

    move-result v16

    if-eqz v3, :cond_9

    const/4 v12, 0x4

    :goto_3
    move/from16 v0, v16

    if-eq v12, v0, :cond_4

    invoke-virtual {v4, v12}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setVisibility(I)V

    :cond_4
    cmpl-float v25, v23, v13

    if-eqz v25, :cond_5

    invoke-virtual {v4, v13}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setTranslationY(F)V

    :cond_5
    cmpl-float v25, v24, v14

    if-eqz v25, :cond_6

    invoke-static {v4, v14}, Lcom/android/systemui/SystemUICompatibility;->setTranslationZ(Landroid/view/View;F)V

    :cond_6
    cmpl-float v25, v20, v11

    if-eqz v25, :cond_7

    invoke-virtual {v4, v11}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setScaleX(F)V

    invoke-virtual {v4, v11}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setScaleY(F)V

    :cond_7
    int-to-float v0, v9

    move/from16 v25, v0

    add-float v10, v13, v25

    if-eqz v18, :cond_a

    int-to-float v5, v9

    :goto_4
    int-to-float v0, v9

    move/from16 v25, v0

    sub-float v26, v19, v13

    sub-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v4, v9, v5, v1}, Lcom/android/systemui/statusbar/stack/StackScrollState;->updateChildClippingAndBackground(Lcom/android/systemui/statusbar/ExpandableNotificationRow;IFI)V

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getClipTopAmount()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    add-float v19, v13, v25

    move/from16 v17, v10

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getTranslationX()F

    move-result v25

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-eqz v25, :cond_b

    const/16 v18, 0x1

    goto/16 :goto_1

    :cond_8
    const/4 v3, 0x0

    goto :goto_2

    :cond_9
    const/4 v12, 0x0

    goto :goto_3

    :cond_a
    sub-float v5, v10, v17

    goto :goto_4

    :cond_b
    const/16 v18, 0x0

    goto/16 :goto_1

    :cond_c
    return-void
.end method

.method public getHostView()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mHostView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewStateForView(Landroid/view/View;)Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mStateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;

    return-object v0
.end method

.method public removeViewStateForView(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mStateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public resetViewStates()V
    .locals 6

    iget-object v4, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mHostView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v4, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mHostView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v4, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mStateMap:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;

    if-nez v3, :cond_0

    new-instance v3, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;

    invoke-direct {v3}, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;-><init>()V

    iget-object v4, p0, Lcom/android/systemui/statusbar/stack/StackScrollState;->mStateMap:Ljava/util/Map;

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    iput v4, v3, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->height:I

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    :goto_1
    iput-boolean v4, v3, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->gone:Z

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, v3, Lcom/android/systemui/statusbar/stack/StackScrollState$ViewState;->alpha:F

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    return-void
.end method
