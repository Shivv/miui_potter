.class public Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;
.super Ljava/lang/Object;
.source "BaseStatusBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/BaseStatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "BaseNotificationClicker"
.end annotation


# instance fields
.field protected mIndex:I

.field protected mIntent:Landroid/app/PendingIntent;

.field protected mSbn:Lcom/android/systemui/statusbar/ExpandedNotification;

.field protected mSessionIndex:I

.field final synthetic this$0:Lcom/android/systemui/statusbar/BaseStatusBar;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/BaseStatusBar;Landroid/app/PendingIntent;Lcom/android/systemui/statusbar/ExpandedNotification;Z)V
    .locals 1

    const/4 v0, -0x1

    iput-object p1, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->mSessionIndex:I

    iput v0, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->mIndex:I

    iput-object p2, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->mIntent:Landroid/app/PendingIntent;

    iput-object p3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->mSbn:Lcom/android/systemui/statusbar/ExpandedNotification;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-virtual {v2, p1}, Lcom/android/systemui/statusbar/BaseStatusBar;->showRawNotification(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->mIntent:Landroid/app/PendingIntent;

    if-nez v2, :cond_1

    return-void

    :cond_1
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/app/IActivityManager;->resumeAppSwitches()V

    invoke-static {}, Lcom/android/systemui/SystemUICompatibility;->dismissKeyguardOnNextActivity()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v2, v2, Lcom/android/systemui/statusbar/BaseStatusBar;->mNotificationData:Lcom/android/systemui/statusbar/NotificationData;

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->mSbn:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/NotificationData;->findByNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Lcom/android/systemui/statusbar/NotificationData$Entry;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v3, v3, Lcom/android/systemui/statusbar/BaseStatusBar;->mNotificationData:Lcom/android/systemui/statusbar/NotificationData;

    iget-object v4, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->mSbn:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/NotificationData;->findByNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Lcom/android/systemui/statusbar/NotificationData$Entry;

    move-result-object v3

    iget-object v3, v3, Lcom/android/systemui/statusbar/NotificationData$Entry;->key:Landroid/os/IBinder;

    iput-object v3, v2, Lcom/android/systemui/statusbar/BaseStatusBar;->mPendingKey:Landroid/os/IBinder;

    iget-object v2, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-static {v2}, Lcom/android/systemui/Util;->isFoldTips(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-virtual {v2, v5}, Lcom/android/systemui/statusbar/BaseStatusBar;->animateCollapse(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/BaseStatusBar$BaseNotificationClicker;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-virtual {v2, v5}, Lcom/android/systemui/statusbar/BaseStatusBar;->visibilityChanged(Z)V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
