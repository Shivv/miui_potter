.class Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;
.super Landroid/database/ContentObserver;
.source "UsbNotificationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/UsbNotificationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/UsbNotificationController;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get1(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "disable_usb_by_sim"

    invoke-static {}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get0()I

    move-result v5

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-set0(Lcom/android/systemui/statusbar/policy/UsbNotificationController;Z)Z

    sget-boolean v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->SUPPORT_DISABLE_USB_BY_SIM:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get2(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "UsbNotificationController"

    const-string/jumbo v3, "not support disable usb by sim!"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v0, v2}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-set0(Lcom/android/systemui/statusbar/policy/UsbNotificationController;Z)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get1(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "disable_usb_by_sim"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get2(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get3(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get5(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get5(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;->this$0:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-static {v3}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-get4(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    :goto_1
    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->-wrap0(Lcom/android/systemui/statusbar/policy/UsbNotificationController;Z)V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method
