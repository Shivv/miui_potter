.class Lcom/android/systemui/statusbar/policy/NetworkController$1;
.super Landroid/telephony/PhoneStateListener;
.source "NetworkController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/policy/NetworkController;->getPhoneStateListener(JI)Landroid/telephony/PhoneStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/NetworkController;

.field final synthetic val$phoneId:I


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/NetworkController;Ljava/lang/Integer;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/NetworkController;

    iput p3, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->val$phoneId:I

    invoke-direct {p0, p2}, Landroid/telephony/PhoneStateListener;-><init>(Ljava/lang/Integer;)V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget v1, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->val$phoneId:I

    invoke-virtual {v0, p1, v1}, Lcom/android/systemui/statusbar/policy/NetworkController;->onCallStateChanged(II)V

    return-void
.end method

.method public onDataActivity(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget v1, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->val$phoneId:I

    invoke-virtual {v0, v1, p1}, Lcom/android/systemui/statusbar/policy/NetworkController;->onDataActivity(II)V

    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget v1, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->val$phoneId:I

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/systemui/statusbar/policy/NetworkController;->onDataConnectionStateChanged(III)V

    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget v1, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->val$phoneId:I

    invoke-virtual {v0, v1, p1}, Lcom/android/systemui/statusbar/policy/NetworkController;->onServiceStateChanged(ILandroid/telephony/ServiceState;)V

    return-void
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget v1, p0, Lcom/android/systemui/statusbar/policy/NetworkController$1;->val$phoneId:I

    invoke-virtual {v0, v1, p1}, Lcom/android/systemui/statusbar/policy/NetworkController;->onSignalStrengthsChanged(ILandroid/telephony/SignalStrength;)V

    return-void
.end method
