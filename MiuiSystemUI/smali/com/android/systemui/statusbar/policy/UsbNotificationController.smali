.class public Lcom/android/systemui/statusbar/policy/UsbNotificationController;
.super Ljava/lang/Object;
.source "UsbNotificationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;,
        Lcom/android/systemui/statusbar/policy/UsbNotificationController$2;
    }
.end annotation


# static fields
.field private static final DISBALE_USB_BY_SIM_DEFAULT:I

.field public static final SUPPORT_DISABLE_USB_BY_SIM:Z

.field private static volatile sInstance:Lcom/android/systemui/statusbar/policy/UsbNotificationController;


# instance fields
.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCdInstallNotificationId:I

.field private mChargingNotificationId:I

.field private mChargintNotificationShowing:Z

.field private mContext:Landroid/content/Context;

.field private mDisableUsbBySim:Z

.field private final mDisableUsbObserver:Landroid/database/ContentObserver;

.field private mEnableUsbModeSeletion:Z

.field private mHandler:Landroid/os/Handler;

.field private mHasSystemChargingNotification:Z

.field private mIsDialogShowing:Z

.field private mIsScreenshotMode:Z

.field private mMtpNotificationId:I

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPlugType:I

.field private mPtpNotificationId:I

.field private mUsbAlert:Landroid/app/AlertDialog;

.field private mUsbManager:Landroid/hardware/usb/UsbManager;


# direct methods
.method static synthetic -get0()I
    .locals 1

    sget v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->DISBALE_USB_BY_SIM_DEFAULT:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mDisableUsbBySim:Z

    return v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mIsDialogShowing:Z

    return v0
.end method

.method static synthetic -get4(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mPlugType:I

    return v0
.end method

.method static synthetic -get5(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbAlert:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/policy/UsbNotificationController;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mDisableUsbBySim:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/systemui/statusbar/policy/UsbNotificationController;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mIsDialogShowing:Z

    return p1
.end method

.method static synthetic -set2(Lcom/android/systemui/statusbar/policy/UsbNotificationController;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mPlugType:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/policy/UsbNotificationController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->refreshWhenUsbConnectChanged(Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    :goto_0
    sput-boolean v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->SUPPORT_DISABLE_USB_BY_SIM:Z

    sget-boolean v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->SUPPORT_DISABLE_USB_BY_SIM:Z

    if-eqz v0, :cond_1

    :goto_1
    sput v1, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->DISBALE_USB_BY_SIM_DEFAULT:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mHandler:Landroid/os/Handler;

    iput v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mPlugType:I

    new-instance v1, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v3}, Lcom/android/systemui/statusbar/policy/UsbNotificationController$1;-><init>(Lcom/android/systemui/statusbar/policy/UsbNotificationController;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mDisableUsbObserver:Landroid/database/ContentObserver;

    new-instance v1, Lcom/android/systemui/statusbar/policy/UsbNotificationController$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController$2;-><init>(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/app/MiuiThemeHelper;->isScreenshotMode()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mIsScreenshotMode:Z

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "usb"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbManager;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbManager:Landroid/hardware/usb/UsbManager;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x11080042

    invoke-static {v1, v3}, Lmiui/util/ResourceMapper;->resolveReference(Landroid/content/res/Resources;I)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mPtpNotificationId:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x11080043

    invoke-static {v1, v3}, Lmiui/util/ResourceMapper;->resolveReference(Landroid/content/res/Resources;I)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mMtpNotificationId:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x11080044

    invoke-static {v1, v3}, Lmiui/util/ResourceMapper;->resolveReference(Landroid/content/res/Resources;I)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mCdInstallNotificationId:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x110a0010

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mEnableUsbModeSeletion:Z

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v3, "usb_charging_notification_title"

    const-string/jumbo v4, "string"

    const-string/jumbo v5, "com.mediatek"

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargingNotificationId:I

    iget v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargingNotificationId:I

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v1, v3, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v3, "usb_charging_notification_title"

    const-string/jumbo v4, "string"

    const-string/jumbo v5, "android"

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargingNotificationId:I

    :cond_1
    iget v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargingNotificationId:I

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mHasSystemChargingNotification:Z

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v3, "disable_usb_by_sim"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mDisableUsbObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mDisableUsbObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public static getChargingIconRes()I
    .locals 1

    const v0, 0x7f02011f

    return v0
.end method

.method private getFunction()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "usb_function"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/policy/UsbNotificationController;
    .locals 2

    sget-object v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->sInstance:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    if-nez v0, :cond_1

    const-class v1, Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->sInstance:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->sInstance:Lcom/android/systemui/statusbar/policy/UsbNotificationController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->sInstance:Lcom/android/systemui/statusbar/policy/UsbNotificationController;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private isChargingNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getId()I

    move-result v0

    const-string/jumbo v3, "android"

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mHasSystemChargingNotification:Z

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargingNotificationId:I

    if-eq v0, v3, :cond_0

    const/16 v3, 0x20

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const v3, 0x7f0d027f

    if-eq v0, v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method private isMtpSwitcherNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getId()I

    move-result v0

    const-string/jumbo v3, "android"

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mPtpNotificationId:I

    if-eq v0, v3, :cond_0

    iget v3, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mMtpNotificationId:I

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v3, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mCdInstallNotificationId:I

    if-eq v0, v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method private isUsbModeNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mEnableUsbModeSeletion:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "com.android.systemui"

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/systemui/CompatibilityM;->isStorageNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshWhenUsbConnectChanged(Z)V
    .locals 5

    const/4 v4, 0x1

    sget-boolean v2, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->SUPPORT_DISABLE_USB_BY_SIM:Z

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mDisableUsbBySim:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mIsDialogShowing:Z

    if-nez v2, :cond_1

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mIsDialogShowing:Z

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    sget v3, Lmiui/R$style;->Theme_Light_Dialog_Alert:I

    invoke-direct {v0, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0d027d

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0d027e

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x1010355

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x104000a

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbAlert:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbAlert:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbAlert:Landroid/app/AlertDialog;

    new-instance v3, Lcom/android/systemui/statusbar/policy/UsbNotificationController$3;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController$3;-><init>(Lcom/android/systemui/statusbar/policy/UsbNotificationController;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbAlert:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbManager:Landroid/hardware/usb/UsbManager;

    invoke-static {v2}, Lcom/android/systemui/CompatibilityM;->getCurrentFunction(Landroid/hardware/usb/UsbManager;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "charging"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbManager:Landroid/hardware/usb/UsbManager;

    const-string/jumbo v3, "charging"

    invoke-static {v2, v3}, Lcom/android/systemui/CompatibilityM;->setCurrentFunction(Landroid/hardware/usb/UsbManager;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->setFunction(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "adb_enabled"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->showChargingNotificationIfNeeded(Z)V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->revertStatusIfNeeded()V

    goto :goto_0
.end method

.method private revertStatusIfNeeded()V
    .locals 2

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->getFunction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "charging"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->setFunction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbManager:Landroid/hardware/usb/UsbManager;

    invoke-static {v1, v0}, Lcom/android/systemui/CompatibilityM;->setCurrentFunction(Landroid/hardware/usb/UsbManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private setFunction(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "usb_function"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private showChargingNotificationIfNeeded(Z)V
    .locals 11

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mHasSystemChargingNotification:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mDisableUsbBySim:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mUsbManager:Landroid/hardware/usb/UsbManager;

    invoke-static {v0}, Lcom/android/systemui/CompatibilityM;->inChargingMode(Landroid/hardware/usb/UsbManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargintNotificationShowing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    const v1, 0x7f0d027f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    const v1, 0x1040661

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v1, "com.android.settings"

    const-string/jumbo v3, "com.android.settings.UsbSettings"

    invoke-direct {v0, v1, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/content/Intent;->makeRestartActivityTask(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    move-result-object v8

    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v10}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    const v1, 0x10807b9

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    const-string/jumbo v1, "usb"

    const-string/jumbo v3, "usb"

    invoke-static {v0, v1, v3, v6}, Lcom/android/systemui/CompatibilityO;->addNotificationChannel(Landroid/app/NotificationManager;Ljava/lang/String;Ljava/lang/String;Landroid/app/Notification$Builder;)V

    invoke-virtual {v6}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    iget-object v0, v7, Landroid/app/Notification;->extraNotification:Landroid/app/MiuiNotification;

    const-string/jumbo v1, "android"

    invoke-virtual {v0, v1}, Landroid/app/MiuiNotification;->setTargetPkg(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v3, 0x0

    const v4, 0x7f0d027f

    invoke-virtual {v0, v3, v4, v7, v1}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargintNotificationShowing:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargintNotificationShowing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    const v1, 0x7f0d027f

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mChargintNotificationShowing:Z

    goto :goto_0
.end method


# virtual methods
.method public isUsbNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->isMtpSwitcherNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->isUsbModeNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->isChargingNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public needDisableUsbNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mDisableUsbBySim:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->mIsScreenshotMode:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->isUsbNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
