.class Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver$1;
.super Ljava/lang/Object;
.source "BrightnessMirrorController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver$1;->this$1:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver$1;->this$1:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get1(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver$1;->this$1:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    iget-object v1, v1, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getScreenBrightness()I

    move-result v1

    sget v2, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver$1;->this$1:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->startWatching()V

    return-void
.end method
