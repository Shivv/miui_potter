.class Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;
.super Ljava/lang/Object;
.source "BrightnessMirrorController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x96

    invoke-static {v0, v1, v2, v3}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-wrap1(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Landroid/view/ViewPropertyAnimator;J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getHeight()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/android/systemui/ViewHelper;->performLayoutNow(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->setLocation()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get3(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-wrap2(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get7(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-wrap0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_1
    return-void
.end method
