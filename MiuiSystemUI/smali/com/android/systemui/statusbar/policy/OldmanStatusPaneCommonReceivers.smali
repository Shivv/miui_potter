.class public Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;
.super Ljava/lang/Object;
.source "OldmanStatusPaneCommonReceivers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$1;,
        Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;,
        Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;,
        Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;,
        Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;
    }
.end annotation


# instance fields
.field private final bluetoothReceiver:Landroid/content/BroadcastReceiver;

.field private final clockReceiver:Landroid/content/BroadcastReceiver;

.field private final commonReceiver:Landroid/content/BroadcastReceiver;

.field private mCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mQuietModeEnableListener:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mCallbacks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->updateAlarm(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->updateBattery(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->updateHeadset(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->updateIncall(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->updateRingerMode(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->updateSyncState(Landroid/content/Intent;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mCallbacks:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$1;-><init>(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mQuietModeEnableListener:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;

    new-instance v1, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;-><init>(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->clockReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;-><init>(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;-><init>(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->commonReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->clockReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3, p2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.ALARM_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.SYNC_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->commonReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mQuietModeEnableListener:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;

    invoke-static {v1, v2}, Lmiui/provider/ExtraTelephony;->registerQuietModeEnableListener(Landroid/content/Context;Lmiui/provider/ExtraTelephony$QuietModeEnableListener;)V

    return-void
.end method

.method private updateAlarm(Landroid/content/Intent;)V
    .locals 8

    const/4 v7, 0x0

    const-string/jumbo v6, "alarmSet"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string/jumbo v6, "alarmSystem"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz v1, :cond_0

    move v4, v0

    :goto_0
    iget-object v6, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mCallbacks:Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    invoke-interface {v2, v4, v5}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onAlarmChanged(ZZ)V

    goto :goto_1

    :cond_0
    move v5, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateBattery(Landroid/content/Intent;)V
    .locals 10

    const/4 v9, 0x0

    const-string/jumbo v7, "plugged"

    invoke-virtual {p1, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string/jumbo v7, "status"

    const/4 v8, 0x1

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string/jumbo v7, "level"

    invoke-virtual {p1, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v7, "scale"

    const/16 v8, 0x64

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    mul-int/lit8 v7, v2, 0x64

    div-int v3, v7, v5

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mCallbacks:Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    invoke-interface {v0, v4, v6, v3}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onBatteryStateChanged(III)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateHeadset(Landroid/content/Intent;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string/jumbo v4, "microphone"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v6, :cond_0

    const/4 v3, 0x1

    :goto_0
    const-string/jumbo v4, "state"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v6, :cond_1

    const/4 v2, 0x1

    :goto_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mCallbacks:Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    invoke-interface {v0, v2, v3}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onHeadsetChanged(ZZ)V

    goto :goto_2

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    return-void
.end method

.method private updateIncall(Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method private updateRingerMode(Landroid/content/Intent;)V
    .locals 6

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "audio"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mCallbacks:Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    invoke-interface {v1, v3}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onRingerModeChanged(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateSyncState(Landroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    const-string/jumbo v4, "active"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string/jumbo v4, "failing"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mCallbacks:Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    invoke-interface {v0, v2, v3}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onSyncStateChanged(ZZ)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addCallback(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->clockReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mQuietModeEnableListener:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->isQuietModeEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-interface {v0, v1}, Lmiui/provider/ExtraTelephony$QuietModeEnableListener;->onQuietModeEnableChange(Z)V

    return-void
.end method

.method public release()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->clockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->commonReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->mQuietModeEnableListener:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;

    invoke-static {v0, v1}, Lmiui/provider/ExtraTelephony;->unRegisterQuietModeEnableListener(Landroid/content/Context;Lmiui/provider/ExtraTelephony$QuietModeEnableListener;)V

    return-void
.end method
