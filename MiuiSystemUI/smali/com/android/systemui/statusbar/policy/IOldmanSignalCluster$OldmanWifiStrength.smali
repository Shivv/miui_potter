.class public final enum Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;
.super Ljava/lang/Enum;
.source "IOldmanSignalCluster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OldmanWifiStrength"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field public static final enum LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field public static final enum LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field public static final enum LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field public static final enum LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field public static final enum LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field public static final enum LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field public static final enum LV_USB_SHARE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field public static final enum UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const-string/jumbo v1, "UNK"

    invoke-direct {v0, v1, v3}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const-string/jumbo v1, "LV_0"

    invoke-direct {v0, v1, v4}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const-string/jumbo v1, "LV_1"

    invoke-direct {v0, v1, v5}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const-string/jumbo v1, "LV_2"

    invoke-direct {v0, v1, v6}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const-string/jumbo v1, "LV_3"

    invoke-direct {v0, v1, v7}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const-string/jumbo v1, "LV_4"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const-string/jumbo v1, "LV_NULL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const-string/jumbo v1, "LV_USB_SHARE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_USB_SHARE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_USB_SHARE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->$VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromLevel(I)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;
    .locals 1

    packed-switch p0, :pswitch_data_0

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object v0

    :pswitch_4
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;
    .locals 1

    const-class v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object v0
.end method

.method public static values()[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;
    .locals 1

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->$VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    return-object v0
.end method
