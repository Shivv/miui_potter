.class public Lcom/android/systemui/statusbar/policy/FilterInfo;
.super Ljava/lang/Object;
.source "FilterInfo.java"


# instance fields
.field private mCategory:Ljava/lang/String;

.field private mMessageId:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mSbn:Lcom/android/systemui/statusbar/ExpandedNotification;

.field private mSubstName:Ljava/lang/String;

.field private mUid:I


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/ExpandedNotification;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/systemui/Util;->getUid(Lcom/android/systemui/statusbar/ExpandedNotification;)I

    move-result v4

    iput v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mUid:I

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mSbn:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, v3, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    if-eqz v4, :cond_0

    iget-object v2, v3, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string/jumbo v4, "miui.category"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    const-string/jumbo v4, "miui.substName"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mSubstName:Ljava/lang/String;

    const-string/jumbo v4, "message_id"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mMessageId:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mMessageId:Ljava/lang/String;

    if-nez v4, :cond_0

    const-string/jumbo v4, "adid"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mMessageId:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-ne p0, p1, :cond_0

    return v4

    :cond_0
    if-nez p1, :cond_1

    return v3

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/FilterInfo;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_2

    return v3

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/android/systemui/statusbar/policy/FilterInfo;

    iget v1, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mUid:I

    iget v2, v0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mUid:I

    if-eq v1, v2, :cond_3

    return v3

    :cond_3
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    if-eqz v1, :cond_5

    return v3

    :cond_4
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    return v3

    :cond_5
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    if-nez v1, :cond_6

    iget-object v1, v0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    if-eqz v1, :cond_7

    return v3

    :cond_6
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    return v3

    :cond_7
    return v4
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mMessageId:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getSbn()Lcom/android/systemui/statusbar/ExpandedNotification;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mSbn:Lcom/android/systemui/statusbar/ExpandedNotification;

    return-object v0
.end method

.method public getSubstName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mSubstName:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mUid:I

    return v0
.end method

.method public hashCode()I
    .locals 5

    const/4 v3, 0x0

    const/16 v0, 0x1f

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mUid:I

    add-int v1, v2, v4

    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public isHybridCategory()Z
    .locals 2

    const-string/jumbo v0, "com.miui.hybrid"

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isXmsfCategory()Z
    .locals 2

    const-string/jumbo v0, "com.xiaomi.xmsf"

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/FilterInfo;->mCategory:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
