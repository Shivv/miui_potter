.class public final enum Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;
.super Ljava/lang/Enum;
.source "IOldmanSignalCluster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OldmanMobileStrength"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum LV_DISCONNECTED:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum LV_IDLE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field public static final enum UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "UNK"

    invoke-direct {v0, v1, v3}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "LV_0"

    invoke-direct {v0, v1, v4}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "LV_1"

    invoke-direct {v0, v1, v5}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "LV_2"

    invoke-direct {v0, v1, v6}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "LV_3"

    invoke-direct {v0, v1, v7}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "LV_4"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "LV_NULL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "LV_IDLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_IDLE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const-string/jumbo v1, "LV_DISCONNECTED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_DISCONNECTED:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_IDLE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_DISCONNECTED:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->$VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromLevel(I)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;
    .locals 1

    packed-switch p0, :pswitch_data_0

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_2:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0

    :pswitch_4
    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static isZero(Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;)Z
    .locals 2

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_0:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_IDLE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_DISCONNECTED:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    if-eq p0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;
    .locals 1

    const-class v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0
.end method

.method public static values()[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;
    .locals 1

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->$VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    return-object v0
.end method
