.class public abstract Lcom/android/systemui/statusbar/policy/BaseNetworkController;
.super Landroid/content/BroadcastReceiver;
.source "BaseNetworkController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;,
        Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;,
        Lcom/android/systemui/statusbar/policy/BaseNetworkController$WifiHandler;,
        Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;
    }
.end annotation


# static fields
.field static final NO_SERVICE_DALAY_TIMEOUT:I


# instance fields
.field mAirplaneMode:Z

.field private mBgHandler:Landroid/os/Handler;

.field mBluetoothTetherIconId:I

.field mBluetoothTethered:Z

.field mCallState:[I

.field mCarrierSimpleNames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mCombinedLabelViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field mCombinedSignalIconViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mConnected:Z

.field mConnectedNetworkType:I

.field mConnectedNetworkTypeName:Ljava/lang/String;

.field mContentDescriptionCombinedSignal:Ljava/lang/String;

.field mContentDescriptionDataType:Ljava/lang/String;

.field mContentDescriptionPhoneSignal:Ljava/lang/String;

.field mContentDescriptionWifi:Ljava/lang/String;

.field mContentDescriptionWimax:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mCustomCarrier:[Ljava/lang/String;

.field mCustomCarrierObserver:[Landroid/database/ContentObserver;

.field mDataActivity:[I

.field mDataAndWifiStacked:Z

.field mDataConnected:[Z

.field mDataDirectionOverlayIconViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mDataNetType:[I

.field mDataSignalIconId:[I

.field mDataState:[I

.field mDataTypeIconId:[I

.field mDataTypeIconViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mEmergencyOnlyString:Ljava/lang/String;

.field private mEnableVolte:Z

.field private mEnhancedObserver:Landroid/database/ContentObserver;

.field protected mFilter:Landroid/content/IntentFilter;

.field mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field mHasMobileDataFeature:Z

.field mHspaDataDistinguishable:Z

.field mImsRegisted:[Z

.field mInetCondition:I

.field mIsActiveNetworkMetered:Z

.field private mIsFirstSimStateChange:Z

.field mIsRoaming:[Z

.field mIsSimMissing:[Z

.field mIsWimaxEnabled:Z

.field mLabelViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field mLastAirplaneMode:Z

.field mLastCombinedLabel:Ljava/lang/String;

.field mLastCombinedSignalIconId:I

.field mLastConnectedNetworkType:I

.field mLastDataDirectionIconId:I

.field mLastDataDirectionOverlayIconId:[I

.field mLastDataTypeIconId:[I

.field mLastHasService:[Z

.field mLastImsRegisted:[Z

.field mLastIsRoaming:[Z

.field mLastLabel:Ljava/lang/String;

.field mLastPhoneSignalIconId:[I

.field mLastPhoneSignalIconIdEvdo:[I

.field mLastPhoneType:[I

.field mLastServiceStateDataType:[I

.field mLastSignalLevel:[I

.field private mLastSimActive:[Z

.field mLastSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

.field mLastSpeechHd:[Z

.field mLastWifiIconId:I

.field mLastWimaxIconId:I

.field mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

.field private mMccNncList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mMobileActivityIconId:[I

.field mMobileLabel:[Ljava/lang/String;

.field mMobileLabelViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field mMobileUpgradeViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkControllerCompatibility:Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;

.field mNetworkName:[Ljava/lang/String;

.field mNetworkNameDefault:Ljava/lang/String;

.field mNetworkNameOriginal:[Ljava/lang/String;

.field mNetworkNameSeparator:Ljava/lang/String;

.field mNetworkPolicyListener:Landroid/net/INetworkPolicyListener;

.field mNetworkTypeName:[Ljava/lang/String;

.field mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

.field mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field private mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field mOldmanMobileStrengthWimax:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

.field mOldmanSignalClusters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;",
            ">;"
        }
    .end annotation
.end field

.field mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

.field mOldmanWifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

.field private mOosEnhancementEnabled:Z

.field final mPhoneCount:I

.field mPhoneSignalIconId:[I

.field mPhoneSignalIconIdEvdo:[I

.field mPhoneSignalIconViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mPhoneType:[I

.field mPlmn:[Ljava/lang/String;

.field mRealCarrier:[Ljava/lang/String;

.field mServiceState:[Landroid/telephony/ServiceState;

.field mServiceStateDataType:[I

.field mShowAtLeastThreeGees:Z

.field private mShowCarrierStyle:I

.field mShowPhoneRSSIForData:Z

.field mShowPlmn:[Z

.field mShowSpn:[Z

.field mSignalClusters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;",
            ">;"
        }
    .end annotation
.end field

.field mSignalInetCondition:I

.field mSignalStrength:[Landroid/telephony/SignalStrength;

.field private mSimActive:[Z

.field mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

.field mSpeechHd:[Z

.field mSpn:[Ljava/lang/String;

.field private mSupportCA:Z

.field private mSupportDualVolte:Z

.field mTempNetworkName:[Ljava/lang/String;

.field mTempServiceState:[Landroid/telephony/ServiceState;

.field mVirtualSimName:Ljava/lang/String;

.field mVolteNoService:[Z

.field mWifiActivity:I

.field mWifiActivityIconId:I

.field mWifiApEnabled:Z

.field mWifiChannel:Lcom/android/internal/util/AsyncChannel;

.field mWifiConnected:Z

.field mWifiEnabled:Z

.field mWifiIconId:I

.field mWifiIconViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mWifiLabelViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field mWifiLevel:I

.field final mWifiManager:Landroid/net/wifi/WifiManager;

.field mWifiRssi:I

.field mWifiSsid:Ljava/lang/String;

.field mWimaxConnected:Z

.field mWimaxExtraState:I

.field mWimaxIconId:I

.field mWimaxIconViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mWimaxIdle:Z

.field mWimaxSignal:I

.field mWimaxState:I

.field mWimaxSupported:Z

.field sNotch:Z


# direct methods
.method static synthetic -set0(Lcom/android/systemui/statusbar/policy/BaseNetworkController;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mEnableVolte:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/policy/BaseNetworkController;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->readCarrierSettings(I)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/policy/BaseNetworkController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "ro.sys.oosenhance.timer"

    const v1, 0x9c40

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->NO_SERVICE_DALAY_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 13

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPhoneRSSIForData:Z

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowAtLeastThreeGees:Z

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    const/4 v8, 0x0

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    const/4 v8, 0x0

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivity:I

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBluetoothTethered:Z

    const v8, 0x7f0202ab

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBluetoothTetherIconId:I

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSupported:Z

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsWimaxEnabled:Z

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIdle:Z

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    const/4 v8, 0x0

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthWimax:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSignal:I

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxState:I

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxExtraState:I

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnected:Z

    const/4 v8, -0x1

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    const/4 v8, 0x1

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastConnectedNetworkType:I

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mInetCondition:I

    const/4 v8, 0x0

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalInetCondition:I

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastAirplaneMode:Z

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataDirectionOverlayIconViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCombinedSignalIconViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLabelViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCombinedLabelViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabelViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileUpgradeViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLabelViews:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanSignalClusters:Ljava/util/ArrayList;

    const/4 v8, -0x1

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionIconId:I

    const/4 v8, -0x1

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWifiIconId:I

    const/4 v8, -0x1

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWimaxIconId:I

    const/4 v8, -0x1

    iput v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedSignalIconId:I

    const-string/jumbo v8, ""

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastLabel:Ljava/lang/String;

    const-string/jumbo v8, ""

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedLabel:Ljava/lang/String;

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataAndWifiStacked:Z

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->sNotch:Z

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCarrierSimpleNames:Ljava/util/HashMap;

    const-string/jumbo v8, "support_ca"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSupportCA:Z

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsFirstSimStateChange:Z

    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v8

    invoke-virtual {v8}, Lmiui/telephony/TelephonyManager;->isDualVolteSupported()Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSupportDualVolte:Z

    new-instance v8, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;

    invoke-direct {v8, p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;-><init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;)V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v8

    invoke-virtual {v8}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-instance v8, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;

    invoke-direct {v8, p0}, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;-><init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;)V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkControllerCompatibility:Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0003

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_1

    sget-boolean v8, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    xor-int/lit8 v8, v8, 0x1

    :goto_0
    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHspaDataDistinguishable:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v9, 0x7f0d0172

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameSeparator:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v9, 0x7f0d029a

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v9, 0x10401d6

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mEmergencyOnlyString:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x1109000f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMccNncList:Ljava/util/List;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Lcom/android/internal/telephony/IccCardConstants$State;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Lcom/android/internal/telephony/IccCardConstants$State;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataState:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataActivity:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Landroid/telephony/ServiceState;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Landroid/telephony/ServiceState;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempServiceState:[Landroid/telephony/ServiceState;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Landroid/telephony/SignalStrength;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempNetworkName:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimActive:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastHasService:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceStateDataType:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastServiceStateDataType:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSignalLevel:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconId:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionOverlayIconId:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataTypeIconId:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconIdEvdo:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconIdEvdo:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowSpn:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPlmn:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpn:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPlmn:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Landroid/telephony/PhoneStateListener;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMSimPhoneStateListener:[Landroid/telephony/PhoneStateListener;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastIsRoaming:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSpeechHd:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mRealCarrier:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCustomCarrier:[Ljava/lang/String;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Landroid/database/ContentObserver;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCustomCarrierObserver:[Landroid/database/ContentObserver;

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneType:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneType:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCallState:[I

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastImsRegisted:[Z

    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    new-array v8, v8, [Z

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVolteNoService:[Z

    const/4 v4, 0x0

    :goto_1
    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v4, v8, :cond_2

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    const/4 v9, 0x0

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    sget-object v9, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    sget-object v9, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataState:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataActivity:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    new-instance v9, Landroid/telephony/ServiceState;

    invoke-direct {v9}, Landroid/telephony/ServiceState;-><init>()V

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempServiceState:[Landroid/telephony/ServiceState;

    new-instance v9, Landroid/telephony/ServiceState;

    invoke-direct {v9}, Landroid/telephony/ServiceState;-><init>()V

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    const/4 v9, 0x0

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    iget-object v9, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    const-string/jumbo v9, ""

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempNetworkName:[Ljava/lang/String;

    iget-object v9, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    iget-object v9, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    const/4 v9, 0x1

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    const/4 v9, 0x0

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v9

    invoke-virtual {v9, v4}, Lmiui/telephony/TelephonyManager;->hasIccCard(I)Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    const/4 v9, 0x0

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    const-string/jumbo v9, ""

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastHasService:[Z

    const/4 v9, 0x0

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/4 v9, 0x0

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/4 v9, 0x0

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceStateDataType:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastServiceStateDataType:[I

    const/4 v9, -0x1

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const/4 v9, 0x0

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    const/4 v9, 0x0

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSignalLevel:[I

    const/4 v9, -0x1

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconId:[I

    const/4 v9, -0x1

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionOverlayIconId:[I

    const/4 v9, -0x1

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataTypeIconId:[I

    const/4 v9, -0x1

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconIdEvdo:[I

    const/4 v9, -0x1

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimActive:[Z

    const/4 v9, 0x0

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastIsRoaming:[Z

    const/4 v9, 0x1

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    const/4 v9, 0x0

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastImsRegisted:[Z

    const/4 v9, 0x1

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVolteNoService:[Z

    const/4 v9, 0x0

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSpeechHd:[Z

    const/4 v9, 0x1

    aput-boolean v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneType:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconIdEvdo:[I

    const/4 v9, 0x0

    aput v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    const/4 v9, 0x0

    aput-object v9, v8, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_1
    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_2
    new-instance v8, Landroid/os/HandlerThread;

    const-string/jumbo v9, "StatusBar.NetworkController"

    invoke-direct {v8, v9}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandlerThread:Landroid/os/HandlerThread;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v8}, Landroid/os/HandlerThread;->start()V

    new-instance v8, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;

    iget-object v9, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v9}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;-><init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;Landroid/os/Looper;)V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const-string/jumbo v9, "connectivity"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    const v8, 0x7f0a0005

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPhoneRSSIForData:Z

    const v8, 0x7f0a0006

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowAtLeastThreeGees:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateWifiIcons()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateWimaxIcons()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->registerPhoneStateListener()V

    const-string/jumbo v8, "wifi"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/WifiManager;

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiManager:Landroid/net/wifi/WifiManager;

    new-instance v3, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WifiHandler;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WifiHandler;-><init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;)V

    new-instance v8, Lcom/android/internal/util/AsyncChannel;

    invoke-direct {v8}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiChannel:Lcom/android/internal/util/AsyncChannel;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getWifiServiceMessenger()Landroid/os/Messenger;

    move-result-object v7

    if-eqz v7, :cond_3

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiChannel:Lcom/android/internal/util/AsyncChannel;

    iget-object v9, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v9, v3, v7}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    :cond_3
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getNetworkPolicyListener()Landroid/net/INetworkPolicyListener;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkPolicyListener:Landroid/net/INetworkPolicyListener;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->registerNetworkPolicyListener()V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/4 v4, 0x1

    :goto_2
    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v4, v8, :cond_4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_4
    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.conn.INET_CONDITION_ACTION"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.intent.action.ACTION_IMS_REGISTED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.intent.action.ACTION_SPEECH_CODEC_IS_HD"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.intent.action.RADIO_TECHNOLOGY"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x110a0002

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSupported:Z

    iget-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSupported:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.fourG.wimax.WIMAX_NETWORK_STATE_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.wimax.SIGNAL_LEVEL_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mFilter:Landroid/content/IntentFilter;

    const-string/jumbo v9, "android.net.fourG.NET_4G_STATE_CHANGED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateAirplaneMode()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isMsim()Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "status_bar_custom_carrier"

    const/4 v10, -0x2

    invoke-static {v8, v9, v10}, Landroid/provider/MiuiSettings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "status_bar_custom_carrier0"

    const/4 v10, -0x2

    invoke-static {v8, v9, v0, v10}, Landroid/provider/MiuiSettings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "status_bar_custom_carrier"

    const/4 v10, 0x0

    const/4 v11, -0x2

    invoke-static {v8, v9, v10, v11}, Landroid/provider/MiuiSettings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    :cond_6
    const/4 v4, 0x0

    :goto_3
    iget v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v4, v8, :cond_7

    move v6, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCustomCarrierObserver:[Landroid/database/ContentObserver;

    new-instance v9, Lcom/android/systemui/statusbar/policy/BaseNetworkController$2;

    new-instance v10, Landroid/os/Handler;

    invoke-direct {v10}, Landroid/os/Handler;-><init>()V

    invoke-direct {v9, p0, v10, v6}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$2;-><init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;Landroid/os/Handler;I)V

    aput-object v9, v8, v4

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "status_bar_custom_carrier"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iget-object v10, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCustomCarrierObserver:[Landroid/database/ContentObserver;

    aget-object v10, v10, v4

    const/4 v11, 0x0

    const/4 v12, -0x1

    invoke-virtual {v8, v9, v11, v10, v12}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    invoke-direct {p0, v4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->readCarrierSettings(I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_7
    const-string/jumbo v8, "ro.sys.oosenhance.enable"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    sget-boolean v8, Lmiui/os/Build;->IS_HONGMI_TWOX:Z

    if-nez v8, :cond_8

    sget-boolean v8, Lmiui/os/Build;->IS_MIFIVE:Z

    if-nez v8, :cond_8

    sget-boolean v8, Lmiui/os/Build;->IS_HONGMI_THREE_LTE:Z

    if-nez v8, :cond_8

    sget-boolean v8, Lmiui/os/Build;->IS_MIFOUR_LTE_CM:Z

    if-nez v8, :cond_8

    sget-boolean v8, Lmiui/os/Build;->IS_MIFOUR_LTE_CU:Z

    if-eqz v8, :cond_9

    :cond_8
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    :cond_9
    iget-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    sget-boolean v9, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    xor-int/lit8 v9, v9, 0x1

    and-int/2addr v8, v9

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    iget-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    sget-boolean v9, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    xor-int/lit8 v9, v9, 0x1

    and-int/2addr v8, v9

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    iget-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    sget-boolean v9, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z

    xor-int/lit8 v9, v9, 0x1

    and-int/2addr v8, v9

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    iget-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    const-string/jumbo v9, "debug.oosenhance.turnon"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    and-int/2addr v8, v9

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    const-string/jumbo v8, "StatusBar.NetworkController"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "mOosEnhancementEnabled = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Lcom/android/systemui/statusbar/policy/BaseNetworkController$3;

    iget-object v9, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-direct {v8, p0, v9, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$3;-><init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mEnhancedObserver:Landroid/database/ContentObserver;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mEnhancedObserver:Landroid/database/ContentObserver;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "volte_vt_enabled"

    invoke-static {v9}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iget-object v10, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mEnhancedObserver:Landroid/database/ContentObserver;

    const/4 v11, 0x0

    const/4 v12, -0x1

    invoke-virtual {v8, v9, v11, v10, v12}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method private getCustomLabel(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCustomCarrier:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCustomCarrier:[Ljava/lang/String;

    aget-object p1, v0, p2

    :cond_0
    return-object p1
.end method

.method private getResourceName(I)Ljava/lang/String;
    .locals 3

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v0

    const-string/jumbo v2, "(unknown)"

    return-object v2

    :cond_0
    const-string/jumbo v2, "(null)"

    return-object v2
.end method

.method private huntForSsid(Landroid/net/wifi/WifiInfo;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    return-object v3

    :cond_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v5

    if-ne v4, v5, :cond_1

    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    return-object v4

    :cond_2
    return-object v6
.end method

.method private isRoaming(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdmaEri(I)Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSimValid(I)Z
    .locals 1

    sget-boolean v0, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v0, v0, p1

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSpnUpdateAction(Ljava/lang/String;)Z
    .locals 4

    const/4 v3, 0x1

    const-string/jumbo v1, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v0, v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    return v1
.end method

.method private needUpdateNetwork(I)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isMsim()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCallState:[I

    rsub-int/lit8 v1, p1, 0x1

    aget v0, v0, v1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSupportDualVolte:Z

    if-eqz v0, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private readCarrierSettings(I)V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCustomCarrier:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_bar_custom_carrier"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/MiuiSettings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p1

    return-void
.end method

.method private refreshCarrier(ILandroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 8

    const/16 v6, 0x8

    const/4 v5, 0x0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSimValid(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCustomCarrier:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/lit8 v0, v4, 0x1

    :goto_0
    iget v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowCarrierStyle:I

    const/4 v7, 0x1

    if-ne v4, v7, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isMsim()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCarrierSimpleNames:Ljava/util/HashMap;

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    aget-object v7, v7, p1

    invoke-virtual {v4, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCarrierSimpleNames:Ljava/util/HashMap;

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    aget-object v7, v7, p1

    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    xor-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_0

    const-string/jumbo v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    aget-object v7, v7, p1

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    const-string/jumbo v4, "4G+"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->sNotch:Z

    if-eqz v4, :cond_7

    invoke-virtual {p3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p2}, Landroid/widget/TextView;->getId()I

    move-result v4

    invoke-virtual {p2, v4}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    const-string/jumbo v4, "4G"

    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v3, :cond_6

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v5

    :goto_3
    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p2}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    invoke-virtual {p3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    :goto_4
    return-void

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    aget-object v4, v4, p1

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    const-string/jumbo v1, ""

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    aget-object v1, v4, p1

    goto :goto_2

    :cond_6
    move v4, v6

    goto :goto_3

    :cond_7
    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {p2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_8
    invoke-virtual {p2}, Landroid/widget/TextView;->getId()I

    move-result v4

    invoke-virtual {p2, v4}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_9

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_9

    :goto_5
    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_9
    move v5, v6

    goto :goto_5
.end method

.method private refreshCarriers()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabelViews:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabelViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/4 v2, 0x0

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->sNotch:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileUpgradeViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    :cond_0
    invoke-virtual {v3}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0, v5, v3, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshCarrier(ILandroid/widget/TextView;Landroid/widget/ImageView;)V

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isMsim()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v6, :cond_1

    invoke-direct {p0, v6, v3, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshCarrier(ILandroid/widget/TextView;Landroid/widget/ImageView;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private refreshSignalCluster()V
    .locals 10

    const/4 v9, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSimValid(I)Z

    move-result v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isMsim()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-direct {p0, v9}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSimValid(I)Z

    move-result v4

    :goto_0
    if-nez v3, :cond_1

    if-eqz v4, :cond_1

    const/4 v5, 0x1

    :goto_1
    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    const/4 v6, 0x1

    :goto_2
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanSignalClusters:Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshOldmanSignalCluster(Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;)V

    goto :goto_3

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    const/4 v6, -0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_4
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    if-ge v0, v7, :cond_7

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Lcom/android/systemui/statusbar/NotchSignalClusterView;

    if-eqz v7, :cond_6

    if-eqz v3, :cond_5

    if-eqz v4, :cond_5

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;

    const/4 v8, 0x2

    invoke-interface {v7, v8}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setCardNum(I)V

    :goto_5
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;

    invoke-virtual {p0, v7, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;I)V

    if-eqz v3, :cond_4

    if-eqz v4, :cond_4

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;

    invoke-virtual {p0, v7, v6}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;I)V

    :cond_4
    :goto_6
    add-int/lit8 v0, v0, 0x2

    goto :goto_4

    :cond_5
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;

    invoke-interface {v7, v9}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setCardNum(I)V

    goto :goto_5

    :cond_6
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;

    invoke-virtual {p0, v7, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    add-int/lit8 v8, v0, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;

    invoke-virtual {p0, v7, v6}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;I)V

    goto :goto_6

    :cond_7
    return-void
.end method

.method private refreshSpeechHd(ZI)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    aget-boolean v0, v0, p2

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    aput-boolean p1, v0, p2

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    :cond_0
    return-void
.end method

.method private removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    const/16 v3, 0x22

    const/4 v2, 0x1

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    return-object p1
.end method

.method private setDataType(II)V
    .locals 4

    const v3, 0x7f0d01a1

    const v2, 0x7f0d019f

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aput p2, v0, p1

    sparse-switch p2, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->H:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v1, 0x7f0d01a0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->H:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v1, 0x7f0d01a2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->E:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v1, 0x7f0d01a4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->X1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v1, 0x7f0d01a3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_7
        0x1 -> :sswitch_4
        0x2 -> :sswitch_5
        0x3 -> :sswitch_1
        0x4 -> :sswitch_0
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x7 -> :sswitch_3
        0x7f020326 -> :sswitch_6
    .end sparse-switch
.end method

.method private updateServiceStateDataType(I)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->needUpdateNetwork(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v0, v0, p1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceStateDataType:[I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/NetworkTypeUtils;->getDataNetTypeFromServiceState(ILandroid/telephony/ServiceState;)I

    move-result v1

    aput v1, v0, p1

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceStateDataType:[I

    const/4 v1, 0x0

    aput v1, v0, p1

    goto :goto_0
.end method


# virtual methods
.method public addMobileLabelView(Landroid/widget/TextView;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabelViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addMobileUpgradeView(Landroid/widget/ImageView;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileUpgradeViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addOldmanSignalCluster(Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanSignalClusters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster()V

    return-void
.end method

.method public addSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-interface {p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->getViewId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/systemui/statusbar/NotchSignalClusterView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-interface {p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->getViewId2()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster()V

    return-void
.end method

.method public addWifiLabelView(Landroid/widget/TextView;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLabelViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "NetworkController state:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v1, "  %s network type %d (%s)"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnected:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "CONNECTED"

    :goto_0
    aput-object v0, v2, v3

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkTypeName:Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mAirplaneMode="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  - telephony ------"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " - slotId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  hasService(slotId)="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0, p4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mHspaDataDistinguishable="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHspaDataDistinguishable:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mDataConnected="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    aget-boolean v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mSimState="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    const-string/jumbo v0, "  isSimValid="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSimValid(I)Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mSimActive="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    aget-boolean v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mIsRoaming="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    aget-boolean v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mSpeechHd="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    aget-boolean v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mIsSimMissing="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  networkOperator="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p4}, Lmiui/telephony/TelephonyManager;->getNetworkOperatorForSlot(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mServiceStateDataType="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceStateDataType:[I

    aget v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v0, "  mLastServiceStateDataType="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastServiceStateDataType:[I

    aget v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v0, "  mDataState="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataState:[I

    aget v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v0, "  mDataActivity="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataActivity:[I

    aget v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v0, "  mDataNetType="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p4

    invoke-virtual {v0, v1}, Lmiui/telephony/TelephonyManagerEx;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mImsRegisted="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    aget-boolean v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, "  isCdma="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0, p4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, "  mServiceState="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    const-string/jumbo v0, "  mSignalStrength="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    const-string/jumbo v0, "  mLastSignalLevel="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSignalLevel:[I

    aget v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v0, "  mNetworkName="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    aget-object v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mNetworkTypeName="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    aget-object v0, v0, p4

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mNetworkNameDefault="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mNetworkNameSeparator="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameSeparator:Ljava/lang/String;

    const-string/jumbo v1, "\n"

    const-string/jumbo v2, "\\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mPhoneSignalIconId=0x"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aget v0, v0, p4

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aget v0, v0, p4

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "  mDataSignalIconId="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    aget v0, v0, p4

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    aget v0, v0, p4

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mDataTypeIconId="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aget v0, v0, p4

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aget v0, v0, p4

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  - wifi ------"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mWifiEnabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiEnabled:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mWifiConnected="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mWifiRssi="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiRssi:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v0, "  mWifiLevel="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLevel:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v0, "  mWifiSsid="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiSsid:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mWifiIconId=0x%08x/%s"

    new-array v1, v5, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mWifiActivity="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivity:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSupported:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "  - wimax ------"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mIsWimaxEnabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsWimaxEnabled:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mWimaxConnected="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mWimaxIdle="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIdle:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  mWimaxIconId=0x%08x/%s"

    new-array v1, v5, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mWimaxSignal=%d"

    new-array v1, v4, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSignal:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mWimaxState=%d"

    new-array v1, v4, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxState:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mWimaxExtraState=%d"

    new-array v1, v4, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxExtraState:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v0, "  - Bluetooth ----"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mBtReverseTethered="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBluetoothTethered:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    const-string/jumbo v0, "  - connectivity ------"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mInetCondition="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mInetCondition:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    const-string/jumbo v0, "  - icons ------"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mLastPhoneSignalIconId=0x"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconId:[I

    aget v0, v0, p4

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconId:[I

    aget v0, v0, p4

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mLastDataDirectionIconId=0x"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionIconId:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionIconId:I

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mLastDataDirectionOverlayIconId=0x"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionOverlayIconId:[I

    aget v0, v0, p4

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionOverlayIconId:[I

    aget v0, v0, p4

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mLastWifiIconId=0x"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWifiIconId:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWifiIconId:I

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mLastCombinedSignalIconId=0x"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedSignalIconId:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedSignalIconId:I

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mLastDataTypeIconId=0x"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataTypeIconId:[I

    aget v0, v0, p4

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataTypeIconId:[I

    aget v0, v0, p4

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v0, "  mLastLabel="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastLabel:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, ""

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string/jumbo v0, "DISCONNECTED"

    goto/16 :goto_0
.end method

.method getCdmaLevel(I)I
    .locals 4

    const/16 v3, 0x13

    const/16 v2, 0xd

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCallState:[I

    aget v1, v1, p1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p1

    if-ne v1, v3, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getLteLevel()I

    move-result v0

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getEvdoLevel()I

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p1

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p1

    if-ne v1, v3, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getLteLevel()I

    move-result v0

    :goto_1
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    move-result v0

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v1, v1, p1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getVoiceNetworkType()I

    move-result v1

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getVoiceNetworkType()I

    move-result v1

    if-eq v1, v3, :cond_4

    :cond_6
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getEvdoLevel()I

    move-result v0

    goto :goto_1
.end method

.method getNetworkPolicyListener()Landroid/net/INetworkPolicyListener;
    .locals 1

    new-instance v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$4;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$4;-><init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;)V

    return-object v0
.end method

.method public hasMobileDataFeature()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    return v0
.end method

.method hasService(I)Z
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getVoiceRegState()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSimValid(I)Z

    move-result v1

    :cond_0
    return v1

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getDataRegState()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public init()V
    .locals 3

    const/4 v2, -0x1

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSignalLevel:[I

    aput v2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconId:[I

    aput v2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionOverlayIconId:[I

    aput v2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataTypeIconId:[I

    aput v2, v1, v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconIdEvdo:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionIconId:I

    iput v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedSignalIconId:I

    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedLabel:Ljava/lang/String;

    return-void
.end method

.method public initCarrierSimpleName()V
    .locals 8

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCarrierSimpleNames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z

    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x0

    :goto_1
    array-length v0, v6

    if-ge v5, v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCarrierSimpleNames:Ljava/util/HashMap;

    aget-object v1, v6, v5

    aget-object v2, v7, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    :goto_2
    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v5, v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowSpn:[Z

    aget-boolean v1, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpn:[Ljava/lang/String;

    aget-object v2, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPlmn:[Z

    aget-boolean v3, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPlmn:[Ljava/lang/String;

    aget-object v4, v0, v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(ZLjava/lang/String;ZLjava/lang/String;I)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method isCdma(I)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v1, v1, p1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneType:[I

    aget v1, v1, p1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isCdmaEri(I)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getCdmaEriIconIndex()I

    move-result v0

    if-eq v0, v3, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getCdmaEriIconMode()I

    move-result v1

    if-eqz v1, :cond_0

    if-ne v1, v3, :cond_1

    :cond_0
    return v3

    :cond_1
    return v4
.end method

.method isEnhancementNeedForDevice(I)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOosEnhancementEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    aget-boolean v0, v0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMsim()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method messageID(I)I
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v1

    :cond_0
    if-ne p1, v1, :cond_1

    const/4 v0, 0x2

    return v0

    :cond_1
    return v0
.end method

.method onCallStateChanged(II)V
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v0, "StatusBar.NetworkController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCallStateChanged received on slotId :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCallState:[I

    aput p1, v0, p2

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateTelephonySignalStrength(I)V

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(I)V

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    :cond_0
    if-nez p1, :cond_1

    invoke-direct {p0, v3, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSpeechHd(ZI)V

    :cond_1
    return-void
.end method

.method onDataActivity(II)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataActivity:[I

    aput p2, v0, p1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataIcon(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    return-void
.end method

.method onDataConnectionStateChanged(III)V
    .locals 3

    const-string/jumbo v0, "StatusBar.NetworkController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataConnectionStateChanged received on slotId :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataState:[I

    aput p2, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v1, v1, p1

    invoke-static {p3, v1}, Lcom/android/systemui/statusbar/NetworkTypeUtils;->getDataNetTypeFromServiceState(ILandroid/telephony/ServiceState;)I

    move-result v1

    aput v1, v0, p1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataNetType(I)V

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateServiceStateDataType(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataIcon(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateTelephonySignalStrength(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    return-void
.end method

.method onMeteredIfacesChanged()V
    .locals 2

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13

    const/4 v12, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-static {p2, v3}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v5

    const-string/jumbo v0, "StatusBar.NetworkController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "slot="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, -0xa

    invoke-static {p2, v2}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateWifiState(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string/jumbo v0, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/android/systemui/statusbar/policy/UsbNotificationController;->SUPPORT_DISABLE_USB_BY_SIM:Z

    if-eqz v0, :cond_3

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getIccCardCount()I

    move-result v0

    if-lez v0, :cond_3

    const-string/jumbo v0, "StatusBar.NetworkController"

    const-string/jumbo v1, "has sim"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "disable_usb_by_sim"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_3
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsFirstSimStateChange:Z

    if-eqz v0, :cond_4

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsFirstSimStateChange:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkControllerCompatibility:Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;->registerSimInfoLister(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_4
    invoke-virtual {p0, p2, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateSimState(Landroid/content/Intent;I)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataIcon(I)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews()V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v6}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSpnUpdateAction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowSpn:[Z

    const-string/jumbo v1, "showSpn"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    aput-boolean v1, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpn:[Ljava/lang/String;

    const-string/jumbo v1, "spn"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPlmn:[Z

    const-string/jumbo v1, "showPlmn"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    aput-boolean v1, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPlmn:[Ljava/lang/String;

    const-string/jumbo v1, "plmn"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-static {v0, v5}, Lcom/android/systemui/VirtualSimUtils;->isVirtualSim(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v12}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v12, v5, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowSpn:[Z

    aget-boolean v1, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpn:[Ljava/lang/String;

    aget-object v2, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPlmn:[Z

    aget-boolean v3, v0, v5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPlmn:[Ljava/lang/String;

    aget-object v4, v0, v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(ZLjava/lang/String;ZLjava/lang/String;I)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "android.net.conn.INET_CONDITION_ACTION"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_8
    const-string/jumbo v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/NetworkInfo;

    if-nez v9, :cond_9

    const-string/jumbo v0, "StatusBar.NetworkController"

    const-string/jumbo v1, "onReceive, ConnectivityManager.CONNECTIVITY_ACTION networkinfo is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_9
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getType()I

    move-result v11

    const-string/jumbo v0, "StatusBar.NetworkController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onReceive, ConnectivityManager.CONNECTIVITY_ACTION network type is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    if-eq v11, v0, :cond_a

    if-eqz v11, :cond_a

    const/4 v0, 0x7

    if-eq v11, v0, :cond_a

    if-eq v11, v4, :cond_a

    const/16 v0, 0x9

    if-eq v11, v0, :cond_a

    return-void

    :cond_a
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->onMeteredIfacesChanged()V

    :cond_b
    const-string/jumbo v0, "inetCondition"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {p0, v8}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->onUpdateConnectivity(I)V

    goto/16 :goto_0

    :cond_c
    const-string/jumbo v0, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    goto/16 :goto_0

    :cond_d
    const-string/jumbo v0, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateAirplaneMode()V

    const/4 v10, 0x0

    :goto_1
    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v10, v0, :cond_e

    invoke-virtual {p0, v10}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateTelephonySignalStrength(I)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_e
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews()V

    goto/16 :goto_0

    :cond_f
    const-string/jumbo v0, "android.net.fourG.NET_4G_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string/jumbo v0, "android.net.wimax.SIGNAL_LEVEL_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string/jumbo v0, "android.net.fourG.wimax.WIMAX_NETWORK_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateWimaxState(Landroid/content/Intent;)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    goto/16 :goto_0

    :cond_11
    const-string/jumbo v0, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string/jumbo v0, "StatusBar.NetworkController"

    const-string/jumbo v1, "onReceive from android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_12
    const-string/jumbo v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v1, 0x7f0d029a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v1, 0x10401d6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mEmergencyOnlyString:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    goto/16 :goto_0

    :cond_13
    const-string/jumbo v0, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string/jumbo v0, "wifi_state"

    const/16 v1, 0xe

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v7, 0x0

    :goto_2
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiApEnabled:Z

    if-eq v7, v0, :cond_1

    iput-boolean v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiApEnabled:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster()V

    goto/16 :goto_0

    :pswitch_1
    const/4 v7, 0x1

    goto :goto_2

    :pswitch_2
    const/4 v7, 0x0

    goto :goto_2

    :cond_14
    const-string/jumbo v0, "android.intent.action.ACTION_IMS_REGISTED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    const-string/jumbo v1, "state"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    aput-boolean v1, v0, v5

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    goto/16 :goto_0

    :cond_15
    const-string/jumbo v0, "android.intent.action.ACTION_SPEECH_CODEC_IS_HD"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    const-string/jumbo v0, "is_hd"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSpeechHd(ZI)V

    goto/16 :goto_0

    :cond_16
    const-string/jumbo v0, "android.intent.action.RADIO_TECHNOLOGY"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method onServiceStateChanged(ILandroid/telephony/ServiceState;)V
    .locals 7

    const-string/jumbo v1, "StatusBar.NetworkController"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onServiceStateChanged received on slotId :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "state="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " VoiceNetworkType="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getVoiceNetworkType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " DataNetworkType="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->needUpdateNetwork(I)Z

    move-result v0

    if-nez v0, :cond_3

    return-void

    :cond_0
    move-object v0, p2

    goto :goto_0

    :cond_1
    move-object v0, p2

    goto :goto_1

    :cond_2
    move-object v0, p2

    goto :goto_2

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isEnhancementNeedForDevice(I)Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v0, v0, p1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/telephony/TelephonyManager;->hasIccCard(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mEmergencyOnlyString:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    const-string/jumbo v0, "StatusBar.NetworkController"

    const-string/jumbo v1, "onServiceStateChanged: out of service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempServiceState:[Landroid/telephony/ServiceState;

    aput-object p2, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->messageID(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "StatusBar.NetworkController"

    const-string/jumbo v1, "onServiceStateChanged send delay msg"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->messageID(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    iput p1, v6, Landroid/os/Message;->arg1:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->NO_SERVICE_DALAY_TIMEOUT:I

    int-to-long v2, v1

    invoke-virtual {v0, v6, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateServiceStateChanged(I)V

    :cond_4
    :goto_3
    return-void

    :cond_5
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->messageID(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "StatusBar.NetworkController"

    const-string/jumbo v1, "onServiceStateChanged changed to IN_SERVICE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->messageID(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_6
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aput-object p2, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v1

    aput v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p1

    invoke-static {v1, p2}, Lcom/android/systemui/statusbar/NetworkTypeUtils;->getDataNetTypeFromServiceState(ILandroid/telephony/ServiceState;)I

    move-result v1

    aput v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempNetworkName:[Ljava/lang/String;

    aget-object v1, v1, p1

    aput-object v1, v0, p1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateServiceStateChanged(I)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aput-object p2, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v1

    aput v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p1

    invoke-static {v1, p2}, Lcom/android/systemui/statusbar/NetworkTypeUtils;->getDataNetTypeFromServiceState(ILandroid/telephony/ServiceState;)I

    move-result v1

    aput v1, v0, p1

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateServiceStateDataType(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateTelephonySignalStrength(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataNetType(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataIcon(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowSpn:[Z

    aget-boolean v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpn:[Ljava/lang/String;

    aget-object v2, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPlmn:[Z

    aget-boolean v3, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPlmn:[Ljava/lang/String;

    aget-object v4, v0, p1

    move-object v0, p0

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(ZLjava/lang/String;ZLjava/lang/String;I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    goto :goto_3
.end method

.method onSignalStrengthsChanged(ILandroid/telephony/SignalStrength;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aput-object p2, v0, p1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateTelephonySignalStrength(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    return-void
.end method

.method onUpdateConnectivity(I)V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public refreshOldmanSignalCluster(Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshOldmanSignalCluster(Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public refreshOldmanSignalCluster(Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;I)V
    .locals 7

    const/4 v2, 0x1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshOldmanSignalCluster(Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;)V

    return-void

    :cond_1
    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiEnabled:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    invoke-interface {p1, v0, v1, v3}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;->setWifiIndicators(ZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsWimaxEnabled:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthWimax:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    aget-boolean v4, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aget-object v5, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aget-object v6, v0, p2

    move-object v0, p1

    move v1, p2

    invoke-interface/range {v0 .. v6}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;->setMobileDataIndicators(IZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;ZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;)V

    :goto_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    aget-boolean v0, v0, p2

    invoke-interface {p1, p2, v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;->setIsImsRegisted(IZ)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    invoke-interface {p1, v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;->setIsAirplaneMode(Z)V

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0, p2}, Lmiui/telephony/TelephonyManagerEx;->isNetworkRoamingForSlot(I)Z

    move-result v0

    invoke-interface {p1, p2, v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;->setIsRoaming(IZ)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aget-object v0, v0, p2

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->isZero(Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aget-object v3, v0, p2

    :goto_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    aget-boolean v4, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aget-object v5, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aget-object v6, v0, p2

    move-object v0, p1

    move v1, p2

    invoke-interface/range {v0 .. v6}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;->setMobileDataIndicators(IZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;ZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aget-object v3, v0, p2

    goto :goto_2

    :cond_7
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPhoneRSSIForData:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aget-object v3, v0, p2

    :goto_3
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    aget-boolean v4, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aget-object v5, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aget-object v6, v0, p2

    move-object v0, p1

    move v1, p2

    invoke-interface/range {v0 .. v6}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;->setMobileDataIndicators(IZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;ZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;)V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aget-object v3, v0, p2

    goto :goto_3
.end method

.method public refreshSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;I)V
    .locals 10

    const/4 v1, 0x1

    const/4 v9, 0x0

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    invoke-interface {p1, v9}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setVisible(Z)V

    return-void

    :cond_1
    invoke-interface {p1, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setVisible(Z)V

    invoke-interface {p1, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setCardSlot(I)V

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    const/16 v2, 0x9

    if-eq v0, v2, :cond_4

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiEnabled:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    :cond_2
    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    invoke-static {v0}, Landroid/net/ConnectivityManager;->isNetworkTypeWifi(I)Z

    move-result v0

    :goto_0
    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    iget v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWifi:Ljava/lang/String;

    invoke-interface {p1, v0, v2, v3, v4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setWifiIndicators(ZIILjava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsWimaxEnabled:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    if-eqz v0, :cond_6

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    aget v3, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aget v4, v0, p2

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWimax:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v7, v0, p2

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setMobileDataIndicators(ZIIILjava/lang/String;Ljava/lang/String;Z)V

    :goto_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    aget-boolean v0, v0, p2

    invoke-interface {p1, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setIsImsRegisted(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVolteNoService:[Z

    aget-boolean v0, v0, p2

    invoke-interface {p1, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setVolteNoService(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    aget-boolean v0, v0, p2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v0, v0, p2

    xor-int/lit8 v9, v0, 0x1

    :cond_3
    invoke-interface {p1, v9}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setIsRoaming(Z)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiApEnabled:Z

    invoke-interface {p1, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setWifiApEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsActiveNetworkMetered:Z

    invoke-interface {p1, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setIsActiveNetworkMetered(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    aget-boolean v0, v0, p2

    invoke-interface {p1, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setSpeechHd(Z)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    const v1, 0x7f02028b

    invoke-interface {p1, v0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setIsAirplaneMode(ZI)V

    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v9

    goto :goto_0

    :cond_6
    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v0, v0, p2

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mEnableVolte:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    aget-boolean v0, v0, p2

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVolteNoService:[Z

    aput-boolean v9, v0, p2

    :goto_2
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->sNotch:Z

    if-eqz v0, :cond_a

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aget v3, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    aget v4, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aget v5, v0, p2

    iget-object v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v8, v0, p2

    move-object v0, p1

    move v1, p2

    invoke-interface/range {v0 .. v8}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setMobileDataIndicators(IZIIILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVolteNoService:[Z

    aput-boolean v1, v0, p2

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVolteNoService:[Z

    aput-boolean v9, v0, p2

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVolteNoService:[Z

    aput-boolean v9, v0, p2

    goto :goto_2

    :cond_a
    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aget v2, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    aget v3, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aget v4, v0, p2

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v7, v0, p2

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setMobileDataIndicators(ZIIILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVolteNoService:[Z

    aput-boolean v9, v0, p2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->sNotch:Z

    if-eqz v0, :cond_d

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPhoneRSSIForData:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aget v3, v0, p2

    :goto_3
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    aget v4, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aget v5, v0, p2

    iget-object v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v8, v0, p2

    move-object v0, p1

    move v1, p2

    invoke-interface/range {v0 .. v8}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setMobileDataIndicators(IZIIILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    aget v3, v0, p2

    goto :goto_3

    :cond_d
    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPhoneRSSIForData:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aget v2, v0, p2

    :goto_4
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    aget v3, v0, p2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aget v4, v0, p2

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v7, v0, p2

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;->setMobileDataIndicators(ZIIILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_e
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    aget v2, v0, p2

    goto :goto_4
.end method

.method public refreshViews()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method refreshViews(I)V
    .locals 21

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const/4 v8, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, ""

    const-string/jumbo v16, ""

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v18, v17, p1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    move/from16 v17, v0

    if-nez v17, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v19, v18, p1

    const/16 v18, 0x0

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v19, v18, p1

    const/16 v18, 0x0

    aput-object v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    const-string/jumbo v18, ""

    aput-object v18, v17, p1

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    move/from16 v17, v0

    if-eqz v17, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiSsid:Ljava/lang/String;

    move-object/from16 v17, v0

    if-nez v17, :cond_16

    const v17, 0x7f0d01b3

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    :goto_1
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    move-object/from16 v7, v16

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWifi:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionCombinedSignal:Ljava/lang/String;

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBluetoothTethered:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0d0173

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBluetoothTetherIconId:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0d01a5

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionCombinedSignal:Ljava/lang/String;

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    move/from16 v17, v0

    const/16 v18, 0x9

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_19

    const/4 v10, 0x1

    :goto_3
    if-eqz v10, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkTypeName:Ljava/lang/String;

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    move-object/from16 v17, v0

    aget-object v17, v17, p1

    if-eqz v17, :cond_3

    invoke-virtual/range {p0 .. p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v17

    if-nez v17, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    move-object/from16 v17, v0

    aget-object v17, v17, p1

    invoke-virtual/range {v17 .. v17}, Landroid/telephony/ServiceState;->isEmergencyOnly()Z

    move-result v17

    xor-int/lit8 v17, v17, 0x1

    if-eqz v17, :cond_1c

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0d01a6

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v19, v18, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v19, v18, p1

    const/16 v18, 0x0

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v19, v18, p1

    const/16 v18, 0x0

    aput-object v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconIdEvdo:[I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v18, v17, p1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    const-string/jumbo v18, ""

    aput-object v18, v17, p1

    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    if-nez v17, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    move-object/from16 v17, v0

    invoke-direct/range {p0 .. p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isRoaming(I)Z

    move-result v18

    aput-boolean v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileDataTypeArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v18, v17, p1

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    move/from16 v17, v0

    if-eqz v17, :cond_25

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f0d0170

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v17, p1

    :cond_6
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconIdEvdo:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconIdEvdo:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_26

    :cond_7
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastHasService:[Z

    move-object/from16 v17, v0

    invoke-virtual/range {p0 .. p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v18

    aput-boolean v18, v17, p1

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshSignalCluster()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconIdEvdo:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconIdEvdo:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    aput v18, v17, p1

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastAirplaneMode:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastAirplaneMode:Z

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimActive:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimActive:[Z

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    aput-boolean v18, v17, p1

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    move-object/from16 v17, v0

    aget-object v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    aput-object v18, v17, p1

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastIsRoaming:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastIsRoaming:[Z

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    aput-boolean v18, v17, p1

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSpeechHd:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSpeechHd:[Z

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    aput-boolean v18, v17, p1

    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastConnectedNetworkType:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastConnectedNetworkType:I

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastServiceStateDataType:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceStateDataType:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastServiceStateDataType:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceStateDataType:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    aput v18, v17, p1

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneType:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneType:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneType:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneType:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    aput v18, v17, p1

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastImsRegisted:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastImsRegisted:[Z

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    aput-boolean v18, v17, p1

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneSignalIconId:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v11, 0x0

    :goto_7
    if-ge v11, v5, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    if-nez v17, :cond_27

    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    if-eqz v17, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    aput-object v18, v17, p1

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    move-object/from16 v17, v0

    aget v8, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    move-object/from16 v17, v0

    aget v8, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataActivity:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    packed-switch v17, :pswitch_data_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    move-object/from16 v17, v0

    const v18, 0x7f020289

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v18, v17, p1

    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    aget-object v7, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    move-object/from16 v17, v0

    aget v6, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    move-object/from16 v17, v0

    aget v8, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionCombinedSignal:Ljava/lang/String;

    goto/16 :goto_0

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnected:Z

    move/from16 v17, v0

    if-eqz v17, :cond_15

    invoke-virtual/range {p0 .. p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v17

    if-eqz v17, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    aput-object v18, v17, p1

    goto :goto_9

    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    const-string/jumbo v18, ""

    aput-object v18, v17, p1

    goto/16 :goto_9

    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v17, p1

    goto/16 :goto_9

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    move-object/from16 v17, v0

    const v18, 0x7f02028f

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-object/from16 v17, v0

    sget-object v18, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->IN:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aput-object v18, v17, p1

    goto :goto_a

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    move-object/from16 v17, v0

    const v18, 0x7f020297

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-object/from16 v17, v0

    sget-object v18, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->OUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aput-object v18, v17, p1

    goto/16 :goto_a

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileActivityIconId:[I

    move-object/from16 v17, v0

    const v18, 0x7f020291

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileActivityArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    move-object/from16 v17, v0

    sget-object v18, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->INOUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aput-object v18, v17, p1

    goto/16 :goto_a

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiSsid:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivity:I

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_1

    goto/16 :goto_1

    :pswitch_3
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    goto/16 :goto_1

    :pswitch_4
    const v17, 0x7f0202b7

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    sget-object v17, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;->IN:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    goto/16 :goto_1

    :pswitch_5
    const v17, 0x7f0202bb

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    sget-object v17, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;->OUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    goto/16 :goto_1

    :pswitch_6
    const v17, 0x7f0202b9

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiActivityIconId:I

    sget-object v17, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;->INOUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiActivity:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;

    goto/16 :goto_1

    :cond_17
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    move/from16 v17, v0

    if-eqz v17, :cond_18

    const-string/jumbo v16, ""

    goto/16 :goto_2

    :cond_18
    const v17, 0x7f0d01b2

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    :cond_19
    const/4 v10, 0x0

    goto/16 :goto_3

    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1b

    const-string/jumbo v16, ""

    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionCombinedSignal:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    move-object/from16 v17, v0

    aget v8, v17, p1

    goto/16 :goto_4

    :cond_1b
    const v17, 0x7f0d01b2

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v7, v16

    goto :goto_b

    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    if-nez v17, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    move/from16 v17, v0

    xor-int/lit8 v17, v17, 0x1

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBluetoothTethered:Z

    move/from16 v17, v0

    xor-int/lit8 v17, v17, 0x1

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    move/from16 v17, v0

    xor-int/lit8 v17, v17, 0x1

    if-eqz v17, :cond_4

    const v17, 0x7f0d01b2

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    move-object/from16 v17, v0

    aget v8, v17, p1

    :goto_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    move-object/from16 v17, v0

    :goto_d
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionCombinedSignal:Ljava/lang/String;

    goto/16 :goto_4

    :cond_1d
    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    goto :goto_c

    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWifi:Ljava/lang/String;

    move-object/from16 v17, v0

    goto :goto_d

    :cond_1f
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSimValid(I)Z

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isMsim()Z

    move-result v17

    if-eqz v17, :cond_23

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSimValid(I)Z

    move-result v13

    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    if-eqz v17, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    const v18, 0x11080040

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    move-object/from16 v17, v0

    const-string/jumbo v18, ""

    aput-object v18, v17, p1

    :goto_f
    sget-boolean v17, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-eqz v17, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    if-eqz v17, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    const-string/jumbo v18, ""

    aput-object v18, v17, p1

    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    aget-object v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mRealCarrier:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mRealCarrier:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    aput-object v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mRealCarrier:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    const/16 v19, 0x5

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, p1

    move/from16 v3, v20

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/os/Message;->sendToTarget()V

    :cond_21
    invoke-virtual/range {p0 .. p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v17

    if-eqz v17, :cond_22

    invoke-direct/range {p0 .. p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isSimValid(I)Z

    move-result v17

    if-eqz v17, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getCustomLabel(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v17, p1

    :cond_22
    if-eqz v13, :cond_6

    xor-int/lit8 v17, v12, 0x1

    if-eqz v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aget-object v18, v18, v19

    const/16 v19, 0x0

    aput-object v18, v17, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aget-object v18, v18, v19

    const/16 v19, 0x0

    aput-object v18, v17, v19

    goto/16 :goto_5

    :cond_23
    const/4 v13, 0x0

    goto/16 :goto_e

    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabel:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    aput-object v18, v17, p1

    goto/16 :goto_f

    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mRealCarrier:[Ljava/lang/String;

    move-object/from16 v17, v0

    aget-object v17, v17, p1

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mRealCarrier:[Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v16, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mRealCarrier:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    const/16 v19, 0x5

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, p1

    move/from16 v3, v20

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    goto/16 :goto_5

    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionOverlayIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move/from16 v0, v17

    if-ne v0, v6, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWifiIconId:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWimaxIconId:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastServiceStateDataType:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceStateDataType:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastAirplaneMode:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastConnectedNetworkType:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimActive:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataTypeIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    move-object/from16 v17, v0

    aget-object v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    move-object/from16 v18, v0

    aget-object v18, v18, p1

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSpeechHd:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpeechHd:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastIsRoaming:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneType:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneType:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastImsRegisted:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mImsRegisted:[Z

    move-object/from16 v18, v0

    aget-boolean v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastHasService:[Z

    move-object/from16 v17, v0

    aget-boolean v17, v17, p1

    invoke-virtual/range {p0 .. p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_8

    goto/16 :goto_6

    :cond_27
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    :cond_28
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWifiIconId:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWifiIconId:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v11, 0x0

    :goto_10
    if-ge v11, v5, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    move/from16 v17, v0

    if-nez v17, :cond_29

    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_11
    add-int/lit8 v11, v11, 0x1

    goto :goto_10

    :cond_29
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWifi:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_11

    :cond_2a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWimaxIconId:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastWimaxIconId:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v11, 0x0

    :goto_12
    if-ge v11, v5, :cond_2c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    move/from16 v17, v0

    if-nez v17, :cond_2b

    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_13
    add-int/lit8 v11, v11, 0x1

    goto :goto_12

    :cond_2b
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWimax:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_13

    :cond_2c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedSignalIconId:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v8, :cond_2d

    move-object/from16 v0, p0

    iput v8, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedSignalIconId:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCombinedSignalIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v11, 0x0

    :goto_14
    if-ge v11, v5, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCombinedSignalIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    invoke-virtual {v14, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionCombinedSignal:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_14

    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataTypeIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataTypeIconId:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    move-object/from16 v18, v0

    aget v18, v18, p1

    aput v18, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v11, 0x0

    :goto_15
    if-ge v11, v5, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    if-nez v17, :cond_2e

    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_16
    add-int/lit8 v11, v11, 0x1

    goto :goto_15

    :cond_2e
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_16

    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionOverlayIconId:[I

    move-object/from16 v17, v0

    aget v17, v17, p1

    move/from16 v0, v17

    if-eq v0, v6, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastDataDirectionOverlayIconId:[I

    move-object/from16 v17, v0

    aput v6, v17, p1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataDirectionOverlayIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v11, 0x0

    :goto_17
    if-ge v11, v5, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataDirectionOverlayIconViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    if-nez v6, :cond_30

    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_18
    add-int/lit8 v11, v11, 0x1

    goto :goto_17

    :cond_30
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v14, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionDataType:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_18

    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedLabel:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_32

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastCombinedLabel:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCombinedLabelViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v11, 0x0

    :goto_19
    if-ge v11, v5, :cond_32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCombinedLabelViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    invoke-virtual {v15, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_19

    :cond_32
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHasMobileDataFeature:Z

    move/from16 v17, v0

    if-nez v17, :cond_33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_33

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    move/from16 v17, v0

    if-eqz v17, :cond_33

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getCustomLabel(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v16

    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLabelViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v11, 0x0

    :goto_1a
    if-ge v11, v5, :cond_34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLabelViews:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_1a

    :cond_34
    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshCarriers()V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method registerNetworkPolicyListener()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkPolicyListener:Landroid/net/INetworkPolicyListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkPolicyListener:Landroid/net/INetworkPolicyListener;

    invoke-virtual {v0, v1}, Landroid/net/NetworkPolicyManager;->registerListener(Landroid/net/INetworkPolicyListener;)V

    :cond_0
    return-void
.end method

.method registerPhoneStateListener()V
    .locals 0

    return-void
.end method

.method public removeViews()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalClusters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLabelViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileLabelViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMobileUpgradeViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLabelViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCombinedLabelViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataDirectionOverlayIconViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public setShowCarrierStyle(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowCarrierStyle:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshCarriers()V

    return-void
.end method

.method updateAirplaneMode()V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mAirplaneMode:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method updateConnectivity(ILandroid/net/NetworkInfo;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    :goto_0
    iput-boolean v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnected:Z

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnected:Z

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkTypeName:Ljava/lang/String;

    :goto_1
    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnected:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    iput v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    :cond_0
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v3, 0x7

    if-ne v1, v3, :cond_3

    invoke-virtual {p2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBluetoothTethered:Z

    :goto_2
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateWimaxIcons()V

    const/4 v0, 0x0

    :goto_3
    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v0, v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataNetType(I)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataIcon(I)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateTelephonySignalStrength(I)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    iput-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkTypeName:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iput-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBluetoothTethered:Z

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateWifiIcons()V

    return-void
.end method

.method public updateCustomCarrier()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v0, v2, :cond_0

    move v1, v0

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->readCarrierSettings(I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method updateDataIcon(I)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataTypeIconId:[I

    aput v3, v1, p1

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    aput-boolean v0, v1, p1

    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v1, v2, :cond_4

    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataState:[I

    aget v1, v1, p1

    if-ne v1, v4, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataState:[I

    aget v1, v1, p1

    if-ne v1, v4, :cond_6

    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateDataNetType(I)V
    .locals 8

    const/16 v7, 0xa

    const/16 v6, 0x9

    const/16 v5, 0x8

    const/4 v2, 0x6

    const/4 v4, 0x3

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsWimaxEnabled:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    if-eqz v3, :cond_0

    invoke-direct {p0, p1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdmaEri(I)Z

    move-result v3

    aput-boolean v3, v2, p1

    :goto_1
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v3, v3, p1

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/android/systemui/MCCUtils;->checkOperation(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v3, v3, p1

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowAtLeastThreeGees:Z

    if-nez v2, :cond_10

    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :pswitch_1
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowAtLeastThreeGees:Z

    if-nez v2, :cond_2

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto :goto_0

    :cond_2
    :pswitch_2
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowAtLeastThreeGees:Z

    if-nez v2, :cond_3

    const/4 v2, 0x2

    invoke-direct {p0, p1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto :goto_0

    :cond_3
    :pswitch_3
    invoke-direct {p0, p1, v4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mMccNncList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRilDataRadioTechnology()I

    move-result v2

    const/16 v3, 0x14

    if-ne v2, v3, :cond_5

    const/4 v1, 0x6

    invoke-direct {p0, p1, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    :cond_4
    :goto_3
    const-string/jumbo v2, "StatusBar.NetworkController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "datatype = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v4, v4, p1

    invoke-static {v4}, Landroid/telephony/TelephonyManager;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "; show datatype="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "; networkOperator="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v2, v2, p1

    if-eq v2, v7, :cond_6

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v2, v2, p1

    if-ne v2, v6, :cond_7

    :cond_6
    const/4 v1, 0x4

    invoke-direct {p0, p1, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v2, v2, p1

    if-eq v2, v5, :cond_8

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v2, v2, p1

    const/16 v3, 0xf

    if-ne v2, v3, :cond_4

    :cond_8
    const/4 v1, 0x5

    invoke-direct {p0, p1, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v2, v2, p1

    if-eq v2, v5, :cond_a

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v2, v2, p1

    if-ne v2, v6, :cond_b

    :cond_a
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHspaDataDistinguishable:Z

    if-eqz v2, :cond_c

    const/4 v2, 0x4

    invoke-direct {p0, p1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto/16 :goto_0

    :cond_b
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v2, v2, p1

    if-eq v2, v7, :cond_a

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHspaDataDistinguishable:Z

    if-eqz v2, :cond_d

    const/4 v2, 0x5

    invoke-direct {p0, p1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto/16 :goto_0

    :cond_c
    invoke-direct {p0, p1, v4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto/16 :goto_0

    :cond_d
    invoke-direct {p0, p1, v4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto/16 :goto_0

    :pswitch_5
    const v2, 0x7f020326

    invoke-direct {p0, p1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto/16 :goto_0

    :pswitch_6
    invoke-direct {p0, p1, v4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v3, v3, p1

    const/16 v4, 0x13

    if-eq v3, v4, :cond_e

    sget-boolean v3, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z

    if-eqz v3, :cond_f

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSupportCA:Z

    if-eqz v3, :cond_f

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v3

    if-eqz v3, :cond_f

    :cond_e
    const/4 v2, 0x7

    :cond_f
    invoke-direct {p0, p1, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto/16 :goto_0

    :cond_10
    invoke-direct {p0, p1, v4}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->setDataType(II)V

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsRoaming:[Z

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v3

    invoke-virtual {v3, p1}, Lmiui/telephony/TelephonyManagerEx;->isNetworkRoamingForSlot(I)Z

    move-result v3

    aput-boolean v3, v2, p1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method updateNetworkName(I)V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x3

    const/4 v8, 0x0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isEnhancementNeedForDevice(I)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    aget-object v4, v4, p1

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    aget-object v4, v4, p1

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempNetworkName:[Ljava/lang/String;

    aget-object v5, v5, p1

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->messageID(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempNetworkName:[Ljava/lang/String;

    aget-object v5, v5, p1

    iget-object v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    aput-object v5, v6, p1

    aput-object v5, v4, p1

    :goto_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v4, v4, p1

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getDataRegState()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getVoiceRegState()I

    move-result v4

    if-nez v4, :cond_c

    :cond_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    aget-object v5, v5, p1

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataConnected:[Z

    aget-boolean v4, v4, p1

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-eqz v4, :cond_c

    iget v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    invoke-static {v4}, Landroid/net/ConnectivityManager;->isNetworkTypeWifi(I)Z

    move-result v4

    if-eqz v4, :cond_c

    :cond_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getVoiceNetworkType()I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSlotId()I

    move-result v4

    if-eq p1, v4, :cond_5

    :cond_2
    move v0, v3

    :goto_1
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v4

    invoke-virtual {v4, v0}, Lmiui/telephony/TelephonyManagerEx;->getNetworkClass(I)I

    move-result v2

    if-ne v2, v9, :cond_8

    sget-boolean v4, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z

    if-eqz v4, :cond_7

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSupportCA:Z

    if-eqz v4, :cond_7

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    const/4 v5, 0x7

    invoke-static {v5}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, p1

    :goto_2
    const-string/jumbo v4, "StatusBar.NetworkController"

    const-string/jumbo v5, "updateNetworkName slotId=%d chosenNetType=%d mNetworkName=%s mNetworkTypeName=%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    aget-object v7, v7, p1

    aput-object v7, v6, v11

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    aget-object v7, v7, p1

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    return-void

    :cond_3
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    aget-object v5, v5, p1

    aput-object v5, v4, p1

    goto/16 :goto_0

    :cond_4
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkName:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    aget-object v5, v5, p1

    aput-object v5, v4, p1

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mCallState:[I

    aget v4, v4, p1

    if-nez v4, :cond_2

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    const/4 v5, 0x6

    invoke-static {v5}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, p1

    goto :goto_2

    :cond_8
    if-ne v2, v11, :cond_9

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    invoke-static {v9}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, p1

    goto :goto_2

    :cond_9
    sget-boolean v4, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-nez v4, :cond_a

    sget-boolean v4, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-nez v4, :cond_a

    sget-boolean v4, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-eqz v4, :cond_b

    :cond_a
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    invoke-static {v10}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, p1

    goto :goto_2

    :cond_b
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    const-string/jumbo v5, ""

    aput-object v5, v4, p1

    goto :goto_2

    :cond_c
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkTypeName:[Ljava/lang/String;

    const-string/jumbo v5, ""

    aput-object v5, v4, p1

    goto :goto_3
.end method

.method updateNetworkName(ZLjava/lang/String;ZLjava/lang/String;I)V
    .locals 8

    invoke-direct {p0, p5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->needUpdateNetwork(I)Z

    move-result v5

    if-nez v5, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v0

    const-string/jumbo v5, "StatusBar.NetworkController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateNetworkName spn="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " plmn="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " slotId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " hasService="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " showSpn="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " showPlmn="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    if-eqz p3, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_1

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    :cond_1
    if-nez v3, :cond_2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    :cond_2
    if-eqz v3, :cond_4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-static {v5, p5}, Lcom/android/systemui/VirtualSimUtils;->isVirtualSim(Landroid/content/Context;I)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVirtualSimName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_3

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVirtualSimName:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isEnhancementNeedForDevice(I)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempNetworkName:[Ljava/lang/String;

    aput-object v2, v5, p5

    :goto_1
    invoke-virtual {p0, p5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isEnhancementNeedForDevice(I)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    if-nez p5, :cond_6

    const/4 v5, 0x3

    :goto_2
    invoke-virtual {v6, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iput p5, v1, Landroid/os/Message;->arg1:I

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v5, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_3
    return-void

    :cond_4
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameDefault:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    aput-object v2, v5, p5

    goto :goto_1

    :cond_6
    const/4 v5, 0x4

    goto :goto_2

    :cond_7
    invoke-virtual {p0, p5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(I)V

    goto :goto_3
.end method

.method updateServiceStateAndNetworkNameWhenSimDeactive(I)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const-string/jumbo v0, "StatusBar.NetworkController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateServiceStateAndNetworkNameWhenSimDeactive slotId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->messageID(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempServiceState:[Landroid/telephony/ServiceState;

    aget-object v1, v1, p1

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v1

    aput v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    aget v1, v1, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, p1

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/NetworkTypeUtils;->getDataNetTypeFromServiceState(ILandroid/telephony/ServiceState;)I

    move-result v1

    aput v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempNetworkName:[Ljava/lang/String;

    aget-object v1, v1, p1

    aput-object v1, v0, p1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateServiceStateChanged(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->messageID(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    if-nez p1, :cond_2

    move v0, v6

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowSpn:[Z

    aget-boolean v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpn:[Ljava/lang/String;

    aget-object v2, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPlmn:[Z

    aget-boolean v3, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPlmn:[Ljava/lang/String;

    aget-object v4, v0, p1

    move-object v0, p0

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(ZLjava/lang/String;ZLjava/lang/String;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    if-nez p1, :cond_3

    :goto_1
    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    return-void

    :cond_2
    move v0, v7

    goto :goto_0

    :cond_3
    move v6, v7

    goto :goto_1
.end method

.method updateServiceStateChanged(I)V
    .locals 6

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateServiceStateDataType(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateTelephonySignalStrength(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataNetType(I)V

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataIcon(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowSpn:[Z

    aget-boolean v1, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpn:[Ljava/lang/String;

    aget-object v2, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPlmn:[Z

    aget-boolean v3, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPlmn:[Ljava/lang/String;

    aget-object v4, v0, p1

    move-object v0, p0

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(ZLjava/lang/String;ZLjava/lang/String;I)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(I)V

    goto :goto_0
.end method

.method updateSimState()V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkControllerCompatibility:Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v0}, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;->isSimActivte(Landroid/content/Context;I)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    aget-boolean v2, v2, v0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    aput-boolean v3, v2, v0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimActive:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_1

    const-string/jumbo v2, "StatusBar.NetworkController"

    const-string/jumbo v3, "sim card deactived"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateServiceStateAndNetworkNameWhenSimDeactive(I)V

    :goto_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "StatusBar.NetworkController"

    const-string/jumbo v3, "sim card active"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method updateSimState(Landroid/content/Intent;I)V
    .locals 6

    const-string/jumbo v3, "ss"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "ABSENT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    :goto_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aput-object v1, v3, p2

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4, p2}, Lmiui/telephony/TelephonyManager;->hasIccCard(I)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    aput-boolean v4, v3, p2

    const-string/jumbo v3, "StatusBar.NetworkController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "mIsSimMissing["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "]="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v5, v5, p2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateTelephonySignalStrength(I)V

    return-void

    :cond_0
    const-string/jumbo v3, "READY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    goto :goto_0

    :cond_1
    const-string/jumbo v3, "NOT_READY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    goto :goto_0

    :cond_2
    const-string/jumbo v3, "LOCKED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string/jumbo v3, "reason"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "PIN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    goto :goto_0

    :cond_3
    const-string/jumbo v3, "PUK"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->NETWORK_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    goto/16 :goto_0

    :cond_5
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    goto/16 :goto_0
.end method

.method updateTelephonySignalStrength(I)V
    .locals 7

    const/4 v6, 0x0

    const v3, 0x7f020265

    const/4 v5, 0x4

    const v4, 0x7f020293

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->needUpdateNetwork(I)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsSimMissing:[Z

    aget-boolean v2, v2, p1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aput v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    aput v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconIdEvdo:[I

    aput v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->hasService(I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aput v4, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    aput v4, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconIdEvdo:[I

    aput v4, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v2, v2, p1

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aput v4, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    aput v4, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconIdEvdo:[I

    aput v4, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    sget-object v3, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;->PHONE_SIGNAL_STRENGTH:[I

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdma(I)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isCdmaEri(I)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v2, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH_ROAMING:[[I

    iget v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalInetCondition:I

    aget-object v1, v2, v3

    :goto_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSignalLevel:[I

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->getCdmaLevel(I)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    aput v0, v2, p1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->sNotch:Z

    if-eqz v2, :cond_4

    if-lt v0, v5, :cond_4

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSignalLevel:[I

    const/4 v0, 0x4

    aput v5, v2, p1

    :cond_4
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthEvdoArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->fromLevel(I)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-result-object v3

    aput-object v3, v2, p1

    :cond_5
    :goto_2
    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->isEnhancementNeedForDevice(I)Z

    move-result v2

    if-eqz v2, :cond_6

    if-nez v0, :cond_6

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->messageID(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string/jumbo v2, "StatusBar.NetworkController"

    const-string/jumbo v3, "updateTelephonySignalStrength update icon level"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :cond_6
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneSignalIconId:[I

    aget v3, v1, v0

    aput v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->fromLevel(I)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-result-object v3

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;->PHONE_SIGNAL_STRENGTH:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionPhoneSignal:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataSignalIconId:[I

    sget-object v3, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_SIGNAL_STRENGTH:[[I

    iget v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalInetCondition:I

    aget-object v3, v3, v4

    aget v3, v3, v0

    aput v3, v2, p1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthDataArr:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->fromLevel(I)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-result-object v3

    aput-object v3, v2, p1

    goto/16 :goto_0

    :cond_7
    sget-object v2, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    iget v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalInetCondition:I

    aget-object v1, v2, v3

    goto :goto_1

    :cond_8
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v2

    invoke-virtual {v2, p1}, Lmiui/telephony/TelephonyManagerEx;->isNetworkRoamingForSlot(I)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH_ROAMING:[[I

    iget v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalInetCondition:I

    aget-object v1, v2, v3

    :goto_3
    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSignalLevel:[I

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Landroid/telephony/SignalStrength;->getLevel()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    aput v0, v2, p1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->sNotch:Z

    if-eqz v2, :cond_5

    if-lt v0, v5, :cond_5

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastSignalLevel:[I

    const/4 v0, 0x4

    aput v5, v2, p1

    goto/16 :goto_2

    :cond_9
    sget-object v2, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    iget v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSignalInetCondition:I

    aget-object v1, v2, v3

    goto :goto_3
.end method

.method updateWifiIcons()V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-eqz v2, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/policy/WifiIcons;->WIFI_SIGNAL_STRENGTH:[[I

    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mInetCondition:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLevel:I

    aget v0, v0, v1

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLevel:I

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->fromLevel(I)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;->WIFI_CONNECTION_STRENGTH:[I

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLevel:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWifi:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mConnectedNetworkType:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_1

    const v0, 0x7f0202ad

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_USB_SHARE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataAndWifiStacked:Z

    if-eqz v2, :cond_2

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    :goto_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v1, 0x7f0d0055

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWifi:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiEnabled:Z

    if-eqz v2, :cond_3

    const v0, 0x7f0202cb

    :cond_3
    iput v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiIconId:I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiEnabled:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;->LV_NULL:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    :goto_2
    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanWifiStrength:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method updateWifiState(Landroid/content/Intent;)V
    .locals 9

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string/jumbo v7, "wifi_state"

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_1

    :goto_0
    iput-boolean v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiEnabled:Z

    :cond_0
    :goto_1
    const/4 v1, 0x0

    :goto_2
    iget v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v1, v5, :cond_9

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v5, v6

    goto :goto_0

    :cond_2
    const-string/jumbo v7, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    const-string/jumbo v7, "networkInfo"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/NetworkInfo;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    :cond_3
    iput-boolean v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-nez v6, :cond_4

    iget-boolean v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiEnabled:Z

    :cond_4
    iput-boolean v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiEnabled:Z

    iget-boolean v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-eqz v5, :cond_7

    xor-int/lit8 v5, v4, 0x1

    if-eqz v5, :cond_7

    const-string/jumbo v5, "wifiInfo"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiInfo;

    if-nez v2, :cond_5

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    :cond_5
    if-eqz v2, :cond_6

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->huntForSsid(Landroid/net/wifi/WifiInfo;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiSsid:Ljava/lang/String;

    goto :goto_1

    :cond_6
    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiSsid:Ljava/lang/String;

    goto :goto_1

    :cond_7
    iget-boolean v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-nez v5, :cond_0

    iput-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiSsid:Ljava/lang/String;

    goto :goto_1

    :cond_8
    const-string/jumbo v5, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string/jumbo v5, "newRssi"

    const/16 v6, -0xc8

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiRssi:I

    iget v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiRssi:I

    sget v6, Lcom/android/systemui/statusbar/policy/WifiIcons;->WIFI_LEVEL_COUNT:I

    invoke-static {v5, v6}, Landroid/net/wifi/MiuiWifiManager;->calculateSignalLevel(II)I

    move-result v5

    iput v5, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiLevel:I

    goto :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateWifiIcons()V

    return-void
.end method

.method updateWimaxIcons()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsWimaxEnabled:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIdle:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/android/systemui/statusbar/policy/WimaxIcons;->WIMAX_IDLE:I

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_IDLE:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthWimax:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;->WIMAX_CONNECTION_STRENGTH:[I

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSignal:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWimax:Ljava/lang/String;

    :goto_1
    return-void

    :cond_0
    sget-object v0, Lcom/android/systemui/statusbar/policy/WimaxIcons;->WIMAX_SIGNAL_STRENGTH:[[I

    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mInetCondition:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSignal:I

    aget v0, v0, v1

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSignal:I

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->fromLevel(I)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthWimax:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/systemui/statusbar/policy/WimaxIcons;->WIMAX_DISCONNECTED:I

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;->LV_DISCONNECTED:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthWimax:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const v1, 0x7f0d019a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContentDescriptionWimax:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIconId:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mOldmanMobileStrengthWimax:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;

    goto :goto_1
.end method

.method final updateWimaxState(Landroid/content/Intent;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    const-string/jumbo v6, "android.net.fourG.NET_4G_STATE_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string/jumbo v6, "4g_state"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v6, 0x3

    if-ne v3, v6, :cond_1

    :goto_0
    iput-boolean v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsWimaxEnabled:Z

    :cond_0
    :goto_1
    const/4 v1, 0x0

    :goto_2
    iget v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v1, v4, :cond_6

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateDataNetType(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    const-string/jumbo v6, "android.net.wimax.SIGNAL_LEVEL_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string/jumbo v4, "newSignalLevel"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxSignal:I

    goto :goto_1

    :cond_3
    const-string/jumbo v6, "android.net.fourG.wimax.WIMAX_NETWORK_STATE_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string/jumbo v6, "WimaxState"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxState:I

    const-string/jumbo v6, "WimaxStateDetail"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxExtraState:I

    iget v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxState:I

    const/4 v7, 0x7

    if-ne v6, v7, :cond_4

    move v6, v4

    :goto_3
    iput-boolean v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxConnected:Z

    iget v6, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxExtraState:I

    const/4 v7, 0x6

    if-ne v6, v7, :cond_5

    :goto_4
    iput-boolean v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWimaxIdle:Z

    goto :goto_1

    :cond_4
    move v6, v5

    goto :goto_3

    :cond_5
    move v4, v5

    goto :goto_4

    :cond_6
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateWimaxIcons()V

    return-void
.end method
