.class public Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;
.super Ljava/lang/Object;
.source "NetworkControllerCompatibility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility$1;
    }
.end annotation


# instance fields
.field public mNetworkController:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

.field private mSimInfoChangeListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility$1;-><init>(Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;->mSimInfoChangeListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;->mNetworkController:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    return-void
.end method


# virtual methods
.method public isSimActivte(Landroid/content/Context;I)Ljava/lang/Boolean;
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->isActivated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method public registerSimInfoLister(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;->mSimInfoChangeListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->enableSubscriptionsCache()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerCompatibility;->mSimInfoChangeListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-interface {v0}, Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;->onSubscriptionsChanged()V

    return-void
.end method
