.class Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;
.super Landroid/os/FileObserver;
.source "BrightnessMirrorController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BrightnessFileObserver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver$1;
    }
.end annotation


# instance fields
.field private mRunnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    const/4 v0, 0x2

    invoke-direct {p0, p2, v0}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver$1;-><init>(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->mRunnable:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 3

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get1(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getScreenBrightness()I

    move-result v1

    sget v2, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_0
    return-void
.end method

.method public startWatching(J)V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get4(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get4(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public stopWatching()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->this$0:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->-get4(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Landroid/os/FileObserver;->stopWatching()V

    return-void
.end method
