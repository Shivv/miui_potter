.class public interface abstract Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;
.super Ljava/lang/Object;
.source "BaseNetworkController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/BaseNetworkController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SignalCluster"
.end annotation


# virtual methods
.method public abstract getViewId()I
.end method

.method public abstract getViewId2()I
.end method

.method public abstract setCardNum(I)V
.end method

.method public abstract setCardSlot(I)V
.end method

.method public abstract setIsActiveNetworkMetered(Z)V
.end method

.method public abstract setIsAirplaneMode(ZI)V
.end method

.method public abstract setIsImsRegisted(Z)V
.end method

.method public abstract setIsRoaming(Z)V
.end method

.method public abstract setMobileDataIndicators(IZIIILjava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract setMobileDataIndicators(ZIIILjava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract setSpeechHd(Z)V
.end method

.method public abstract setVisible(Z)V
.end method

.method public abstract setVolteNoService(Z)V
.end method

.method public abstract setWifiApEnabled(Z)V
.end method

.method public abstract setWifiIndicators(ZIILjava/lang/String;)V
.end method
