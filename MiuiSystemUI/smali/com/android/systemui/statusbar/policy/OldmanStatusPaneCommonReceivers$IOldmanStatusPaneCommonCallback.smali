.class public interface abstract Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;
.super Ljava/lang/Object;
.source "OldmanStatusPaneCommonReceivers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IOldmanStatusPaneCommonCallback"
.end annotation


# virtual methods
.method public abstract onAlarmChanged(ZZ)V
.end method

.method public abstract onBatteryStateChanged(III)V
.end method

.method public abstract onBluetoothStateChanged(II)V
.end method

.method public abstract onClockTextChanged(Ljava/lang/CharSequence;)V
.end method

.method public abstract onHeadsetChanged(ZZ)V
.end method

.method public abstract onIncallScreenLeaveOrResume(Z)V
.end method

.method public abstract onQuietModeChanged(Z)V
.end method

.method public abstract onRingerModeChanged(I)V
.end method

.method public abstract onSyncStateChanged(ZZ)V
.end method
