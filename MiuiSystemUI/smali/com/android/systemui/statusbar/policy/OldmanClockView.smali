.class public Lcom/android/systemui/statusbar/policy/OldmanClockView;
.super Landroid/widget/RelativeLayout;
.source "OldmanClockView.java"


# static fields
.field private static final ms_clockTimePattern:Ljava/util/regex/Pattern;

.field private static final ms_normalNumCharDrawableResIdArr:[I

.field private static final ms_normalRevNumCharDrawableResIdArr:[I

.field private static final ms_smallNumCharDrawableResIdArr:[I

.field private static final ms_smallRevNumCharDrawableResIdArr:[I


# instance fields
.field private m_attachedToWindow:Z

.field private m_clockColonImageView:Landroid/widget/ImageView;

.field private m_clockHourHiImageView:Landroid/widget/ImageView;

.field private m_clockHourLoImageView:Landroid/widget/ImageView;

.field private m_clockLeadSegTextView:Landroid/widget/TextView;

.field private m_clockMinHiImageView:Landroid/widget/ImageView;

.field private m_clockMinLoImageView:Landroid/widget/ImageView;

.field private m_clockSegsBox:Landroid/view/ViewGroup;

.field private m_clockSimpleTextView:Landroid/widget/TextView;

.field private m_clockTailSegTextView:Landroid/widget/TextView;

.field private m_clockText:Ljava/lang/CharSequence;

.field private m_clockTextBox:Landroid/view/ViewGroup;

.field private m_darkMode:Z

.field private m_smallStyle:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0xa

    const-string/jumbo v0, "\\d?\\d:\\d\\d"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_clockTimePattern:Ljava/util/regex/Pattern;

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_normalNumCharDrawableResIdArr:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_normalRevNumCharDrawableResIdArr:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_smallNumCharDrawableResIdArr:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_smallRevNumCharDrawableResIdArr:[I

    return-void

    :array_0
    .array-data 4
        0x7f02015a
        0x7f02015e
        0x7f020162
        0x7f020166
        0x7f02016a
        0x7f02016e
        0x7f020172
        0x7f020176
        0x7f02017a
        0x7f02017e
    .end array-data

    :array_1
    .array-data 4
        0x7f02015b
        0x7f02015f
        0x7f020163
        0x7f020167
        0x7f02016b
        0x7f02016f
        0x7f020173
        0x7f020177
        0x7f02017b
        0x7f02017f
    .end array-data

    :array_2
    .array-data 4
        0x7f02015c
        0x7f020160
        0x7f020164
        0x7f020168
        0x7f02016c
        0x7f020170
        0x7f020174
        0x7f020178
        0x7f02017c
        0x7f020180
    .end array-data

    :array_3
    .array-data 4
        0x7f02015d
        0x7f020161
        0x7f020165
        0x7f020169
        0x7f02016d
        0x7f020171
        0x7f020175
        0x7f020179
        0x7f02017d
        0x7f020181
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_attachedToWindow:Z

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_smallStyle:Z

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_darkMode:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    return-void
.end method

.method private _refreshColonImageView(Landroid/widget/ImageView;)V
    .locals 2

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_smallStyle:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_darkMode:Z

    if-eqz v1, :cond_0

    const v0, 0x7f02013b

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockColonImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void

    :cond_0
    const v0, 0x7f02013a

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_darkMode:Z

    if-eqz v1, :cond_2

    const v0, 0x7f020139

    goto :goto_0

    :cond_2
    const v0, 0x7f020138

    goto :goto_0
.end method

.method private _refreshNumImageView(Landroid/widget/ImageView;C)V
    .locals 3

    if-nez p2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_smallStyle:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_darkMode:Z

    if-eqz v2, :cond_1

    sget-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_smallRevNumCharDrawableResIdArr:[I

    :goto_1
    add-int/lit8 v1, p2, -0x30

    aget v2, v0, v1

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_smallNumCharDrawableResIdArr:[I

    goto :goto_1

    :cond_2
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_darkMode:Z

    if-eqz v2, :cond_3

    sget-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_normalRevNumCharDrawableResIdArr:[I

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_normalNumCharDrawableResIdArr:[I

    goto :goto_1
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    const v0, 0x7f0f00d6

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTextBox:Landroid/view/ViewGroup;

    const v0, 0x7f0f00d8

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTextBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00d7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSimpleTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00d9

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockLeadSegTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00df

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTailSegTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00da

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockHourHiImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00db

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockHourLoImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00dd

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockMinHiImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00de

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockMinLoImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    const v1, 0x7f0f00dc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockColonImageView:Landroid/widget/ImageView;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_attachedToWindow:Z

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_smallStyle:Z

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->setSmallStyle(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_darkMode:Z

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->setClockText(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_attachedToWindow:Z

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTextBox:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSimpleTextView:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockLeadSegTextView:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTailSegTextView:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockHourHiImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockHourLoImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockMinHiImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockMinLoImageView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockColonImageView:Landroid/widget/ImageView;

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setClockText(Ljava/lang/CharSequence;Z)V
    .locals 11

    const/4 v10, 0x0

    const/16 v9, 0x8

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_darkMode:Z

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_attachedToWindow:Z

    if-nez v7, :cond_0

    return-void

    :cond_0
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTextBox:Landroid/view/ViewGroup;

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    if-eqz v7, :cond_3

    sget-object v7, Lcom/android/systemui/statusbar/policy/OldmanClockView;->ms_clockTimePattern:Ljava/util/regex/Pattern;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    invoke-virtual {v7, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v6

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    add-int/lit8 v8, v6, 0x0

    invoke-interface {v7, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    add-int/lit8 v8, v6, 0x1

    invoke-interface {v7, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v7, 0x3a

    if-ne v2, v7, :cond_2

    move v2, v1

    const/4 v1, 0x0

    :cond_2
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    add-int/lit8 v8, v0, -0x2

    invoke-interface {v7, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    add-int/lit8 v8, v0, -0x1

    invoke-interface {v7, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTextBox:Landroid/view/ViewGroup;

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    invoke-virtual {v7, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockLeadSegTextView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    invoke-interface {v8, v10, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTailSegTextView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    iget-object v9, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    invoke-interface {v8, v0, v9}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockHourHiImageView:Landroid/widget/ImageView;

    invoke-direct {p0, v7, v1}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->_refreshNumImageView(Landroid/widget/ImageView;C)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockHourLoImageView:Landroid/widget/ImageView;

    invoke-direct {p0, v7, v2}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->_refreshNumImageView(Landroid/widget/ImageView;C)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockMinHiImageView:Landroid/widget/ImageView;

    invoke-direct {p0, v7, v4}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->_refreshNumImageView(Landroid/widget/ImageView;C)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockMinLoImageView:Landroid/widget/ImageView;

    invoke-direct {p0, v7, v5}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->_refreshNumImageView(Landroid/widget/ImageView;C)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockColonImageView:Landroid/widget/ImageView;

    invoke-direct {p0, v7}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->_refreshColonImageView(Landroid/widget/ImageView;)V

    return-void

    :cond_3
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTextBox:Landroid/view/ViewGroup;

    invoke-virtual {v7, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSegsBox:Landroid/view/ViewGroup;

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSimpleTextView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockText:Ljava/lang/CharSequence;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSmallStyle(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_smallStyle:Z

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_attachedToWindow:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/OldmanClockView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_smallStyle:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0b017f

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockSimpleTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockLeadSegTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanClockView;->m_clockTailSegTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    return-void

    :cond_1
    const v1, 0x7f0b017e

    goto :goto_0
.end method
