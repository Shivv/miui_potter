.class public interface abstract Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;
.super Ljava/lang/Object;
.source "IOldmanSignalCluster.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;,
        Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;,
        Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;,
        Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;,
        Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;
    }
.end annotation


# virtual methods
.method public abstract setIsAirplaneMode(Z)V
.end method

.method public abstract setIsImsRegisted(IZ)V
.end method

.method public abstract setIsRoaming(IZ)V
.end method

.method public abstract setMobileDataIndicators(IZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileStrength;ZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;)V
.end method

.method public abstract setWifiIndicators(ZLcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiStrength;Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanWifiActivity;)V
.end method
