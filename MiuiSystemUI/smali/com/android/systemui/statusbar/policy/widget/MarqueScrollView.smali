.class public Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "MarqueScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView$1;
    }
.end annotation


# instance fields
.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field private mContent:Landroid/view/View;

.field private mItem1:Lcom/android/systemui/statusbar/policy/Clock;

.field private mItem2:Lcom/android/systemui/statusbar/policy/Clock;

.field private mSpace:Landroid/view/View;

.field private mUnderKeyguard:Z

.field translateRunnable:Ljava/lang/Runnable;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)Lcom/android/systemui/statusbar/policy/Clock;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)Lcom/android/systemui/statusbar/policy/Clock;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView$1;-><init>(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->translateRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView$1;-><init>(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->translateRunnable:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/policy/Clock;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/policy/Clock;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/systemui/CompatibilityO;->setDefaultFocusHighlightEnabled(Landroid/view/View;Z)V

    const v0, 0x7f0f0031

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mContent:Landroid/view/View;

    const v0, 0x7f0f01be

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/Clock;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/policy/Clock;->setMarqueView(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)V

    const v0, 0x7f0f01bf

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mSpace:Landroid/view/View;

    const v0, 0x7f0f01c0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/Clock;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/policy/Clock;->setMarqueView(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/Clock;->setVisibility(I)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x0

    return v0
.end method

.method public runTranslate()V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->translateRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public setShowDate(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setShowDate(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setShowDate(Z)V

    return-void
.end method

.method public setSimple(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setSimple(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setSimple(Z)V

    return-void
.end method

.method public setUnderKeyguard(Z)V
    .locals 4

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mUnderKeyguard:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mUnderKeyguard:Z

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mUnderKeyguard:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->translateRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setUnderKeyguard(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setUnderKeyguard(Z)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->stopTranslate()V

    goto :goto_0
.end method

.method public stopTranslate()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method public translate()V
    .locals 4

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mContent:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/policy/Clock;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mSpace:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int v0, v1, v2

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mContent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0xfa0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v2, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView$2;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView$2;-><init>(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method public updateDarkMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem1:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->updateDarkMode(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->mItem2:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->updateDarkMode(Z)V

    return-void
.end method
