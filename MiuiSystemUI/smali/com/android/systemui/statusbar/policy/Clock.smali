.class public Lcom/android/systemui/statusbar/policy/Clock;
.super Landroid/widget/TextView;
.source "Clock.java"


# instance fields
.field private mCarrier:Lcom/android/systemui/statusbar/policy/ILeftCorner;

.field private mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

.field private mContext:Landroid/content/Context;

.field private mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

.field private mDemoMode:Z

.field private mMarqueContentView:Landroid/view/View;

.field private mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

.field private mMaxWidth:I

.field private mShowCarrier:Z

.field private mSimple:Z

.field private mUnderKeyguard:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/policy/Clock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/statusbar/policy/Clock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mShowCarrier:Z

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/systemui/statusbar/policy/CarrierText;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/android/systemui/statusbar/policy/CarrierText;-><init>(Landroid/content/Context;Lcom/android/systemui/statusbar/policy/Clock;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCarrier:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    new-instance v0, Lcom/android/systemui/statusbar/policy/ClockText;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/android/systemui/statusbar/policy/ClockText;-><init>(Landroid/content/Context;Lcom/android/systemui/statusbar/policy/Clock;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/Clock;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMaxWidth:I

    return-void
.end method

.method private updateMarqueViewLayout(I)V
    .locals 11

    const v10, 0x7f0f01c0

    const v9, 0x7f0f01bf

    const/16 v8, 0x11

    const/16 v7, 0x8

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->stopTranslate()V

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ge v4, p1, :cond_3

    iget v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMaxWidth:I

    if-le p1, v4, :cond_0

    iget p1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMaxWidth:I

    :cond_0
    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v4, v1}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v4, v9}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v4, v10}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueContentView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    iget v4, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    if-ne v4, v8, :cond_6

    const/4 v0, 0x1

    :goto_1
    if-eq v0, v2, :cond_2

    if-eqz v2, :cond_7

    iput v8, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    :goto_2
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueContentView:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    return-void

    :cond_3
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-le v4, p1, :cond_1

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v4

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMaxWidth:I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/Clock;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b016e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    if-le v4, v5, :cond_5

    :cond_4
    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_3
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v4, v1}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v4, v9}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v4, v10}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    iget v4, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMaxWidth:I

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v2, 0x1

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const v4, 0x800003

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_2
.end method


# virtual methods
.method public dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    const-string/jumbo v0, "demo_mode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Clock mDemoMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/policy/Clock;->mDemoMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", command = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mDemoMode:Z

    if-nez v0, :cond_1

    const-string/jumbo v0, "enter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mDemoMode:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->showDemomode()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mDemoMode:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "exit"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mDemoMode:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->exitDemomode()V

    goto :goto_0
.end method

.method public isCarrier()Z
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCarrier:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClock()Z
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCarrier:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->registerObservers()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->registerObservers()V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mSimple:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mUnderKeyguard:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCarrier:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCarrier:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->unregisterObservers()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->unregisterObservers()V

    return-void
.end method

.method public setCornerText(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/Clock;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/Clock;->getPaddingEnd()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/Clock;->getPaddingStart()I

    move-result v3

    add-int v1, v2, v3

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/policy/Clock;->updateMarqueViewLayout(I)V

    return-void
.end method

.method public setMarqueView(Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;)V
    .locals 2

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueScrollView:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    const v1, 0x7f0f0031

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mMarqueContentView:Landroid/view/View;

    return-void
.end method

.method public setShowAmPm(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0, p1}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->setShowAmPm(Z)V

    return-void
.end method

.method public setShowDate(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0, p1}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->setShowDate(Z)V

    return-void
.end method

.method public setSimple(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mSimple:Z

    return-void
.end method

.method public setUnderKeyguard(Z)V
    .locals 2

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mUnderKeyguard:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mUnderKeyguard:Z

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mSimple:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mUnderKeyguard:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCarrier:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCarrier:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    invoke-interface {v0}, Lcom/android/systemui/statusbar/policy/ILeftCorner;->update()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mClock:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/Clock;->mCorner:Lcom/android/systemui/statusbar/policy/ILeftCorner;

    goto :goto_0
.end method

.method public updateDarkMode(Z)V
    .locals 3

    const v0, 0x7f090008

    sget-boolean v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sMiuiOptimizationDisabled:Z

    if-eqz v1, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-le v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/Clock;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_0

    const v0, 0x7f090025

    :cond_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/Clock;->setTextColor(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/Clock;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_2

    const v0, 0x7f090009

    :cond_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/policy/Clock;->setTextColor(I)V

    goto :goto_0
.end method
