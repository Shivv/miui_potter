.class Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;
.super Landroid/content/BroadcastReceiver;
.source "OldmanStatusPaneCommonReceivers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mConnectionState:I

.field private mState:I

.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mState:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mConnectionState:I

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/4 v6, 0x0

    if-nez p2, :cond_1

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v4

    iput v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mState:I

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getConnectionState()I

    move-result v4

    iput v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mConnectionState:I

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v4}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-get0(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    iget v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mState:I

    iget v5, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mConnectionState:I

    invoke-interface {v2, v4, v5}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onBluetoothStateChanged(II)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string/jumbo v4, "android.bluetooth.adapter.extra.STATE"

    const/high16 v5, -0x80000000

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mState:I

    iput v6, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mConnectionState:I

    goto :goto_0

    :cond_2
    const-string/jumbo v4, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "android.bluetooth.adapter.extra.CONNECTION_STATE"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$3;->mConnectionState:I

    goto :goto_0

    :cond_3
    return-void
.end method
