.class public Lcom/android/systemui/statusbar/NotificationContentViewUtil;
.super Ljava/lang/Object;
.source "NotificationContentViewUtil.java"


# instance fields
.field private mCachedExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

.field private mContext:Landroid/content/Context;

.field private mExpandedChild:Landroid/view/View;

.field private mExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

.field private mPreviousExpandedRemoteInputIntent:Landroid/app/PendingIntent;

.field mService:Lcom/android/systemui/statusbar/BaseStatusBar;

.field private mStatusBarNotification:Lcom/android/systemui/statusbar/ExpandedNotification;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/BaseStatusBar;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mService:Lcom/android/systemui/statusbar/BaseStatusBar;

    return-void
.end method

.method private applyRemoteInput(Landroid/view/View;Lcom/android/systemui/statusbar/NotificationData$Entry;ZLandroid/app/PendingIntent;Lcom/android/systemui/statusbar/policy/RemoteInputView;)Lcom/android/systemui/statusbar/policy/RemoteInputView;
    .locals 9

    const v6, 0x102018e

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v6, 0x102018d

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x1a

    if-lt v6, v7, :cond_0

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    instance-of v6, v1, Landroid/widget/FrameLayout;

    if-eqz v6, :cond_4

    sget-object v6, Lcom/android/systemui/statusbar/policy/RemoteInputView;->VIEW_TAG:Ljava/lang/Object;

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/policy/RemoteInputView;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->onNotificationUpdateOrReset()V

    :cond_1
    if-nez v3, :cond_2

    if-eqz p3, :cond_2

    move-object v0, v1

    check-cast v0, Landroid/widget/FrameLayout;

    if-nez p5, :cond_3

    iget-object v6, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mService:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-static {v6, v0, p2, v7}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/BaseStatusBar;)Lcom/android/systemui/statusbar/policy/RemoteInputView;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->setVisibility(I)V

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-direct {v6, v7, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    const/4 v7, 0x0

    invoke-virtual {v0, v5, v7, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move-object v3, v5

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->requestFocus()Z

    move-result v4

    :cond_2
    :goto_0
    return-object v3

    :cond_3
    const/4 v6, 0x0

    invoke-virtual {v0, p5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    invoke-virtual {p5}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->dispatchFinishTemporaryDetach()V

    invoke-virtual {p5}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->requestFocus()Z

    move-object v3, p5

    goto :goto_0

    :cond_4
    const/4 v6, 0x0

    return-object v6
.end method

.method private applyRemoteInput(Lcom/android/systemui/statusbar/NotificationData$Entry;)V
    .locals 13

    const/4 v2, 0x0

    const/4 v12, 0x0

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iget-object v7, v0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    if-eqz v7, :cond_2

    array-length v5, v7

    move v4, v2

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v6, v7, v4

    invoke-virtual {v6}, Landroid/app/Notification$Action;->getRemoteInputs()[Landroid/app/RemoteInput;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Landroid/app/Notification$Action;->getRemoteInputs()[Landroid/app/RemoteInput;

    move-result-object v9

    array-length v10, v9

    move v0, v2

    :goto_1
    if-ge v0, v10, :cond_0

    aget-object v8, v9, v0

    invoke-virtual {v8}, Landroid/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v3, 0x1

    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mExpandedChild:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mPreviousExpandedRemoteInputIntent:Landroid/app/PendingIntent;

    iget-object v5, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mCachedExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->applyRemoteInput(Landroid/view/View;Lcom/android/systemui/statusbar/NotificationData$Entry;ZLandroid/app/PendingIntent;Lcom/android/systemui/statusbar/policy/RemoteInputView;)Lcom/android/systemui/statusbar/policy/RemoteInputView;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

    :goto_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mCachedExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mCachedExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mCachedExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->dispatchFinishTemporaryDetach()V

    :cond_3
    iput-object v12, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mCachedExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

    return-void

    :cond_4
    iput-object v12, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mExpandedRemoteInput:Lcom/android/systemui/statusbar/policy/RemoteInputView;

    goto :goto_2
.end method


# virtual methods
.method public onNotificationUpdated(Landroid/content/Context;Lcom/android/systemui/statusbar/NotificationData$Entry;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mContext:Landroid/content/Context;

    iget-object v0, p2, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mStatusBarNotification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-direct {p0, p2}, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->applyRemoteInput(Lcom/android/systemui/statusbar/NotificationData$Entry;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mPreviousExpandedRemoteInputIntent:Landroid/app/PendingIntent;

    return-void
.end method

.method public setExpandChild(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->mExpandedChild:Landroid/view/View;

    return-void
.end method
