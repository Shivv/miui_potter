.class public Lcom/android/systemui/analytics/NotificationClickEvent;
.super Ljava/lang/Object;
.source "NotificationClickEvent.java"


# instance fields
.field protected final CLICK_LOCATION:Ljava/lang/String;

.field protected final CLICK_TIMESTAMP:Ljava/lang/String;

.field protected final FLOAT_NOTIFICATION:Ljava/lang/String;

.field protected final GROUP_NOTIFICATION:Ljava/lang/String;

.field protected final KEYGUARD_NOTIFICATION:Ljava/lang/String;

.field private mClickIndex:I

.field private mClickLocation:Ljava/lang/String;

.field private mClickTimestamp:J

.field private mFloatNotification:Z

.field private mGroupNotification:Z

.field private mKeyguardNotification:Z

.field private mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

.field private mSessionIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "click_timestamp"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->CLICK_TIMESTAMP:Ljava/lang/String;

    const-string/jumbo v0, "click_location"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->CLICK_LOCATION:Ljava/lang/String;

    const-string/jumbo v0, "float_notification"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->FLOAT_NOTIFICATION:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_notification"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->KEYGUARD_NOTIFICATION:Ljava/lang/String;

    const-string/jumbo v0, "group_notification"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->GROUP_NOTIFICATION:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getTinyData()Lcom/android/systemui/analytics/TinyData;
    .locals 5

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/systemui/analytics/NotificationClickEvent;->wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    new-instance v1, Lcom/android/systemui/analytics/TinyData;

    const-string/jumbo v2, "notification"

    const-string/jumbo v3, "click"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/systemui/analytics/TinyData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public setClickEvent(Lcom/android/systemui/analytics/ClickEvent;)V
    .locals 2

    iget-object v0, p1, Lcom/android/systemui/analytics/ClickEvent;->notificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iget-object v0, p1, Lcom/android/systemui/analytics/ClickEvent;->location:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickLocation:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickTimestamp:J

    iget v0, p1, Lcom/android/systemui/analytics/ClickEvent;->index:I

    iput v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickIndex:I

    iget v0, p1, Lcom/android/systemui/analytics/ClickEvent;->sessionIndex:I

    iput v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mSessionIndex:I

    iget-boolean v0, p1, Lcom/android/systemui/analytics/ClickEvent;->floatNotification:Z

    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mFloatNotification:Z

    iget-boolean v0, p1, Lcom/android/systemui/analytics/ClickEvent;->keyguardNotification:Z

    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mKeyguardNotification:Z

    iget-boolean v0, p1, Lcom/android/systemui/analytics/ClickEvent;->groupNotification:Z

    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mGroupNotification:Z

    return-void
.end method

.method public wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 6

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    invoke-virtual {v1, p1}, Lcom/android/systemui/analytics/NotificationEvent;->wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    :cond_0
    iget-wide v2, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickTimestamp:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    :try_start_0
    const-string/jumbo v1, "click_timestamp"

    iget-wide v2, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickTimestamp:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget v1, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickIndex:I

    if-lez v1, :cond_2

    :try_start_1
    const-string/jumbo v1, "index"

    iget v2, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickIndex:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_1
    iget v1, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mSessionIndex:I

    if-lez v1, :cond_3

    :try_start_2
    const-string/jumbo v1, "session_index"

    iget v2, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mSessionIndex:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickLocation:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    :try_start_3
    const-string/jumbo v1, "click_location"

    iget-object v2, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mClickLocation:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_4
    :goto_3
    :try_start_4
    const-string/jumbo v1, "float_notification"

    iget-boolean v2, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mFloatNotification:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    const-string/jumbo v1, "keyguard_notification"

    iget-boolean v2, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mKeyguardNotification:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    const-string/jumbo v1, "group_notification"

    iget-boolean v2, p0, Lcom/android/systemui/analytics/NotificationClickEvent;->mGroupNotification:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_6

    :goto_6
    :try_start_7
    const-string/jumbo v1, "event"

    const-string/jumbo v2, "click"

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_7

    :goto_7
    return-object p1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_4

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_5

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_6

    :catch_7
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_7
.end method
