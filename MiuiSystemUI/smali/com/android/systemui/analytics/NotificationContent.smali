.class public Lcom/android/systemui/analytics/NotificationContent;
.super Ljava/lang/Object;
.source "NotificationContent.java"


# instance fields
.field private mEventMessageType:Ljava/lang/String;

.field private mExtras:Landroid/os/Bundle;

.field private mHasActions:Z

.field private mHasCustomView:Z

.field private mHasLargeIcon:Z

.field private mId:Ljava/lang/String;

.field private mIsClearable:Z

.field private mNotification:Landroid/app/Notification;

.field private mPriority:I

.field private mPushId:Ljava/lang/String;

.field private mStyle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Notification;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/systemui/analytics/NotificationContent;->mNotification:Landroid/app/Notification;

    iget-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mNotification:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mExtras:Landroid/os/Bundle;

    invoke-static {p1}, Lcom/android/systemui/analytics/NotificationUtils;->generateNotificationId(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mId:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mExtras:Landroid/os/Bundle;

    const-string/jumbo v3, "android.largeIcon"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mHasLargeIcon:Z

    iget-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mExtras:Landroid/os/Bundle;

    const-string/jumbo v3, "android.contains.customView"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mHasCustomView:Z

    iget-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mNotification:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mNotification:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    array-length v0, v0

    if-lez v0, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mHasActions:Z

    invoke-direct {p0}, Lcom/android/systemui/analytics/NotificationContent;->isClearable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mIsClearable:Z

    iget v0, p1, Landroid/app/Notification;->priority:I

    iput v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mPriority:I

    invoke-static {p1}, Lcom/android/systemui/PushEvents;->getMessageId(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mPushId:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/systemui/PushEvents;->getEventMessageType(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mEventMessageType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mExtras:Landroid/os/Bundle;

    const-string/jumbo v1, "android.template"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationContent;->mStyle:Ljava/lang/String;

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private isClearable()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationContent;->mNotification:Landroid/app/Notification;

    iget v1, v1, Landroid/app/Notification;->flags:I

    and-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationContent;->mNotification:Landroid/app/Notification;

    iget v1, v1, Landroid/app/Notification;->flags:I

    and-int/lit8 v1, v1, 0x20

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationContent;->mId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    const-string/jumbo v1, "id"

    iget-object v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    const-string/jumbo v1, "largeicon"

    iget-boolean v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mHasLargeIcon:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    const-string/jumbo v1, "custom_icon"

    iget-boolean v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mHasCustomView:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    const-string/jumbo v1, "custom_action"

    iget-boolean v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mHasActions:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    const-string/jumbo v1, "clearable"

    iget-boolean v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mIsClearable:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    const-string/jumbo v1, "priority"

    iget v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mPriority:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationContent;->mPushId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_6
    const-string/jumbo v1, "push_id"

    iget-object v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mPushId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_6

    :cond_1
    :goto_6
    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationContent;->mEventMessageType:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    :try_start_7
    const-string/jumbo v1, "event_message_type"

    iget-object v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mEventMessageType:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_7

    :cond_2
    :goto_7
    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationContent;->mStyle:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    :try_start_8
    const-string/jumbo v1, "style"

    iget-object v2, p0, Lcom/android/systemui/analytics/NotificationContent;->mStyle:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_8

    :cond_3
    :goto_8
    return-object p1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_4

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_5

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_6

    :catch_7
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_7

    :catch_8
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_8
.end method
