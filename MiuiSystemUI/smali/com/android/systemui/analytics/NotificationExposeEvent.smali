.class public Lcom/android/systemui/analytics/NotificationExposeEvent;
.super Ljava/lang/Object;
.source "NotificationExposeEvent.java"


# instance fields
.field private final FLOAT_NOTIFICATION:Ljava/lang/String;

.field private final GROUP_NOTIFICATION:Ljava/lang/String;

.field private final KEYGUARD_NOTIFICATION:Ljava/lang/String;

.field private final MESSAGE_LIST:Ljava/lang/String;

.field private mFloatNotification:Z

.field private mGroupNotification:Z

.field private mKeyguardNotification:Z

.field private mMessageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/analytics/ExposeMessage;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "messageList"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->MESSAGE_LIST:Ljava/lang/String;

    const-string/jumbo v0, "float_notification"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->FLOAT_NOTIFICATION:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_notification"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->KEYGUARD_NOTIFICATION:Ljava/lang/String;

    const-string/jumbo v0, "group_notification"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->GROUP_NOTIFICATION:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getTinyData()Lcom/android/systemui/analytics/TinyData;
    .locals 5

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/systemui/analytics/NotificationExposeEvent;->wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    new-instance v1, Lcom/android/systemui/analytics/TinyData;

    const-string/jumbo v2, "notification"

    const-string/jumbo v3, "expose"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/systemui/analytics/TinyData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public setExposeEvent(Lcom/android/systemui/analytics/ExposeEvent;)V
    .locals 1

    iget-object v0, p1, Lcom/android/systemui/analytics/ExposeEvent;->notificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iget-object v0, p1, Lcom/android/systemui/analytics/ExposeEvent;->messageList:Ljava/util/List;

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mMessageList:Ljava/util/List;

    iget-boolean v0, p1, Lcom/android/systemui/analytics/ExposeEvent;->floatNotification:Z

    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mFloatNotification:Z

    iget-boolean v0, p1, Lcom/android/systemui/analytics/ExposeEvent;->keyguardNotification:Z

    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mKeyguardNotification:Z

    iget-boolean v0, p1, Lcom/android/systemui/analytics/ExposeEvent;->groupNotification:Z

    iput-boolean v0, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mGroupNotification:Z

    return-void
.end method

.method public wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 5

    iget-object v3, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    invoke-virtual {v3, p1}, Lcom/android/systemui/analytics/NotificationEvent;->wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mMessageList:Ljava/util/List;

    if-eqz v3, :cond_2

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mMessageList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mMessageList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/analytics/ExposeMessage;

    invoke-virtual {v3}, Lcom/android/systemui/analytics/ExposeMessage;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :try_start_0
    const-string/jumbo v3, "messageList"

    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    :try_start_1
    const-string/jumbo v3, "float_notification"

    iget-boolean v4, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mFloatNotification:Z

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    const-string/jumbo v3, "keyguard_notification"

    iget-boolean v4, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mKeyguardNotification:Z

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    const-string/jumbo v3, "group_notification"

    iget-boolean v4, p0, Lcom/android/systemui/analytics/NotificationExposeEvent;->mGroupNotification:Z

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    const-string/jumbo v3, "event"

    const-string/jumbo v4, "expose"

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    return-object p1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_4

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_5
.end method
