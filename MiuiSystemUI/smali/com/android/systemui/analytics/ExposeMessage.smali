.class public Lcom/android/systemui/analytics/ExposeMessage;
.super Ljava/lang/Object;
.source "ExposeMessage.java"


# instance fields
.field private final EXPOSE_TIMESTAMP:Ljava/lang/String;

.field private final INDEX:Ljava/lang/String;

.field private final LENGTH:Ljava/lang/String;

.field private final LOCATION:Ljava/lang/String;

.field private mExposeTimestamp:J

.field private mIndex:I

.field private mLength:J

.field private mLocation:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;I)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "expose_timestamp"

    iput-object v0, p0, Lcom/android/systemui/analytics/ExposeMessage;->EXPOSE_TIMESTAMP:Ljava/lang/String;

    const-string/jumbo v0, "length"

    iput-object v0, p0, Lcom/android/systemui/analytics/ExposeMessage;->LENGTH:Ljava/lang/String;

    const-string/jumbo v0, "location"

    iput-object v0, p0, Lcom/android/systemui/analytics/ExposeMessage;->LOCATION:Ljava/lang/String;

    const-string/jumbo v0, "index"

    iput-object v0, p0, Lcom/android/systemui/analytics/ExposeMessage;->INDEX:Ljava/lang/String;

    iput-wide p1, p0, Lcom/android/systemui/analytics/ExposeMessage;->mExposeTimestamp:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/systemui/analytics/ExposeMessage;->mExposeTimestamp:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/systemui/analytics/ExposeMessage;->mLength:J

    iput-object p3, p0, Lcom/android/systemui/analytics/ExposeMessage;->mLocation:Ljava/lang/String;

    iput p4, p0, Lcom/android/systemui/analytics/ExposeMessage;->mIndex:I

    return-void
.end method


# virtual methods
.method public toJSONObject()Lorg/json/JSONObject;
    .locals 6

    const-wide/16 v4, 0x0

    iget-wide v2, p0, Lcom/android/systemui/analytics/ExposeMessage;->mLength:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/analytics/ExposeMessage;->mLocation:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    iget-wide v2, p0, Lcom/android/systemui/analytics/ExposeMessage;->mExposeTimestamp:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    :try_start_0
    const-string/jumbo v2, "expose_timestamp"

    iget-wide v4, p0, Lcom/android/systemui/analytics/ExposeMessage;->mExposeTimestamp:J

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    const-string/jumbo v2, "length"

    iget-wide v4, p0, Lcom/android/systemui/analytics/ExposeMessage;->mLength:J

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    const-string/jumbo v2, "location"

    iget-object v3, p0, Lcom/android/systemui/analytics/ExposeMessage;->mLocation:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    iget v2, p0, Lcom/android/systemui/analytics/ExposeMessage;->mIndex:I

    if-lez v2, :cond_1

    :try_start_3
    const-string/jumbo v2, "index"

    iget v3, p0, Lcom/android/systemui/analytics/ExposeMessage;->mIndex:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_1
    :goto_3
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    :cond_2
    const/4 v2, 0x0

    return-object v2
.end method
