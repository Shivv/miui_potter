.class public Lcom/android/systemui/analytics/ExposeEvent;
.super Lcom/android/systemui/analytics/BaseEvent;
.source "ExposeEvent.java"


# instance fields
.field public messageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/analytics/ExposeMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/analytics/BaseEvent;-><init>()V

    return-void
.end method


# virtual methods
.method public canAdd()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/analytics/ExposeEvent;->messageList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/analytics/ExposeEvent;->messageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x200

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
