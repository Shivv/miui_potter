.class final Lcom/android/systemui/LoadAverageService$CpuTracker;
.super Lcom/android/internal/os/ProcessCpuTracker;
.source "LoadAverageService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/LoadAverageService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CpuTracker"
.end annotation


# static fields
.field public static CLUSTERS_TOTAL:I

.field private static CPU_CORES_TOTAL:I

.field private static caseThermFile:Ljava/lang/String;

.field private static clusterEndIndexes:[I

.field private static cpuComb:I

.field private static sShowCpuInfo:Z

.field private static xoThermFile:Ljava/lang/String;


# instance fields
.field private final CPU_FREQUENCY_TEMPLATE:Ljava/lang/String;

.field private final CPU_ONLINE_TEMPLATE:Ljava/lang/String;

.field private final CPU_RESULT_TEMPLATE:Ljava/lang/String;

.field private final GPU_BUSY_FILE:Ljava/lang/String;

.field private final GPU_BUSY_TEMPLATE:Ljava/lang/String;

.field private final GPU_FREQ_FILE:Ljava/lang/String;

.field private final GPU_FREQ_TEMPLATE:Ljava/lang/String;

.field private ONLINE_CPU:Ljava/lang/String;

.field private final POWER_FILE:Ljava/lang/String;

.field private final POWER_MAX_FILE:Ljava/lang/String;

.field private final THERMAL_CASE_SHOW_TEMPLATE:Ljava/lang/String;

.field public THERMAL_TOTAL:I

.field private final THERMAL_XO_SHOW_TEMPLATE:Ljava/lang/String;

.field public cpuLines:[Ljava/lang/String;

.field public cpuLinesWidth:[I

.field public gpuBusyInfo:Ljava/lang/String;

.field public gpuBusyWidth:I

.field public gpuInfo:Ljava/lang/String;

.field public gpuWidth:I

.field private mBuffer:[B

.field mLoadText:Ljava/lang/String;

.field mLoadWidth:I

.field private final mPaint:Landroid/graphics/Paint;

.field public powerInfos:[Ljava/lang/String;

.field public powerWidths:[I

.field public thermalInfos:[Ljava/lang/String;

.field public thermalWidths:[I


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->sShowCpuInfo:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x8

    sput v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->CPU_CORES_TOTAL:I

    sput v1, Lcom/android/systemui/LoadAverageService$CpuTracker;->CLUSTERS_TOTAL:I

    const-string/jumbo v0, "cpu_combination"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuComb:I

    invoke-static {}, Lcom/android/systemui/LoadAverageService$CpuTracker;->initForCpuInfo()V

    invoke-static {}, Lcom/android/systemui/LoadAverageService$CpuTracker;->initForThermal()V

    return-void
.end method

.method constructor <init>(Landroid/graphics/Paint;)V
    .locals 2

    const/4 v1, 0x2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/internal/os/ProcessCpuTracker;-><init>(Z)V

    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mBuffer:[B

    const-string/jumbo v0, "/sys/devices/system/cpu/online"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->ONLINE_CPU:Ljava/lang/String;

    const-string/jumbo v0, "/sys/devices/system/cpu/cpu{0,number}/online"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->CPU_ONLINE_TEMPLATE:Ljava/lang/String;

    const-string/jumbo v0, "/sys/devices/system/cpu/cpu{0,number}/cpufreq/scaling_cur_freq"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->CPU_FREQUENCY_TEMPLATE:Ljava/lang/String;

    const-string/jumbo v0, "CPU[{0,number}-{1,number}]: {2,number}"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->CPU_RESULT_TEMPLATE:Ljava/lang/String;

    const-string/jumbo v0, "GPU freq: {0,number}"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->GPU_FREQ_TEMPLATE:Ljava/lang/String;

    const-string/jumbo v0, "GPU busy: {0}%"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->GPU_BUSY_TEMPLATE:Ljava/lang/String;

    const-string/jumbo v0, "xo_therm: {0}"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->THERMAL_XO_SHOW_TEMPLATE:Ljava/lang/String;

    const-string/jumbo v0, "quiet_therm: {0}"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->THERMAL_CASE_SHOW_TEMPLATE:Ljava/lang/String;

    const-string/jumbo v0, "/sys/class/kgsl/kgsl-3d0/devfreq/cur_freq"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->GPU_FREQ_FILE:Ljava/lang/String;

    const-string/jumbo v0, "/sys/class/kgsl/kgsl-3d0/gpubusy"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->GPU_BUSY_FILE:Ljava/lang/String;

    const-string/jumbo v0, "/sys/class/power_supply/battery/uevent"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->POWER_FILE:Ljava/lang/String;

    const-string/jumbo v0, "/sys/class/power_supply/battery/current_now"

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->POWER_MAX_FILE:Ljava/lang/String;

    sget v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->CLUSTERS_TOTAL:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuLines:[Ljava/lang/String;

    sget v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->CLUSTERS_TOTAL:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuLinesWidth:[I

    iput v1, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->THERMAL_TOTAL:I

    iget v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->THERMAL_TOTAL:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->thermalInfos:[Ljava/lang/String;

    iget v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->THERMAL_TOTAL:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->thermalWidths:[I

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->powerInfos:[Ljava/lang/String;

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->powerWidths:[I

    iput-object p1, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method private collectThermals()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    sget-object v3, Lcom/android/systemui/LoadAverageService$CpuTracker;->xoThermFile:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/systemui/LoadAverageService$CpuTracker;->getThermalValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/android/systemui/LoadAverageService$CpuTracker;->caseThermFile:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/systemui/LoadAverageService$CpuTracker;->getThermalValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->thermalInfos:[Ljava/lang/String;

    const-string/jumbo v4, "xo_therm: {0}"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v3, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->thermalInfos:[Ljava/lang/String;

    const-string/jumbo v4, "quiet_therm: {0}"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x2

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->thermalWidths:[I

    iget-object v4, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->thermalInfos:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0, v4}, Lcom/android/systemui/LoadAverageService$CpuTracker;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v4

    aput v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getCpuWorkingFreq()V
    .locals 19

    const-string/jumbo v4, ""

    const-string/jumbo v5, ""

    const/4 v2, 0x0

    const/4 v6, 0x0

    sget-object v15, Lcom/android/systemui/LoadAverageService$CpuTracker;->clusterEndIndexes:[I

    aget v3, v15, v2

    const/4 v10, 0x0

    :goto_0
    sget v15, Lcom/android/systemui/LoadAverageService$CpuTracker;->CPU_CORES_TOTAL:I

    if-ge v10, v15, :cond_3

    const-string/jumbo v15, "/sys/devices/system/cpu/cpu{0,number}/online"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    const/16 v18, 0x0

    aput-object v17, v16, v18

    invoke-static/range {v15 .. v16}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v15, "/sys/devices/system/cpu/cpu{0,number}/cpufreq/scaling_cur_freq"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    const/16 v18, 0x0

    aput-object v17, v16, v18

    invoke-static/range {v15 .. v16}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v15}, Lcom/android/systemui/LoadAverageService$CpuTracker;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    const-string/jumbo v15, "1"

    const/16 v16, 0x0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    move-object v9, v4

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v15}, Lcom/android/systemui/LoadAverageService$CpuTracker;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_0

    const-string/jumbo v8, "0"

    :cond_0
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v0, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const-wide/16 v12, 0x0

    :try_start_0
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v12

    :goto_1
    if-lez v2, :cond_1

    sget-object v15, Lcom/android/systemui/LoadAverageService$CpuTracker;->clusterEndIndexes:[I

    add-int/lit8 v16, v2, -0x1

    aget v15, v15, v16

    add-int/lit8 v6, v15, 0x1

    :cond_1
    const-string/jumbo v15, "CPU[{0,number}-{1,number}]: {2,number}"

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    const/16 v18, 0x0

    aput-object v17, v16, v18

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    const/16 v18, 0x1

    aput-object v17, v16, v18

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    const/16 v18, 0x2

    aput-object v17, v16, v18

    invoke-static/range {v15 .. v16}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuLines:[Ljava/lang/String;

    add-int/lit8 v16, v2, 0x1

    aput-object v14, v15, v16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuLinesWidth:[I

    add-int/lit8 v16, v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/systemui/LoadAverageService$CpuTracker;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v17

    aput v17, v15, v16

    move v10, v3

    add-int/lit8 v15, v2, 0x1

    sget-object v16, Lcom/android/systemui/LoadAverageService$CpuTracker;->clusterEndIndexes:[I

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v15, v0, :cond_2

    sget-object v15, Lcom/android/systemui/LoadAverageService$CpuTracker;->clusterEndIndexes:[I

    add-int/lit8 v2, v2, 0x1

    aget v3, v15, v2

    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v11

    const-string/jumbo v15, "ProcessCpuTracker"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Unable to parse \'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, "\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_3
    return-void
.end method

.method private getGPUBusy()V
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-string/jumbo v7, "/sys/class/kgsl/kgsl-3d0/gpubusy"

    invoke-direct {p0, v7, v10}, Lcom/android/systemui/LoadAverageService$CpuTracker;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v7, ""

    iput-object v7, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->gpuBusyInfo:Ljava/lang/String;

    iput v10, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->gpuBusyWidth:I

    return-void

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "\\s+"

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v7, v6, v10

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aget-object v7, v6, v11

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_1

    int-to-float v7, v4

    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v7, v8

    int-to-float v8, v5

    div-float v2, v7, v8

    :goto_0
    new-instance v1, Ljava/text/DecimalFormat;

    const-string/jumbo v7, "0.00"

    invoke-direct {v1, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    float-to-double v8, v2

    invoke-virtual {v1, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v7, "GPU busy: {0}%"

    new-array v8, v11, [Ljava/lang/Object;

    aput-object v3, v8, v10

    invoke-static {v7, v8}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->gpuBusyInfo:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->gpuBusyInfo:Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/android/systemui/LoadAverageService$CpuTracker;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->gpuBusyWidth:I

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getGPUFreq()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string/jumbo v2, "/sys/class/kgsl/kgsl-3d0/devfreq/cur_freq"

    invoke-direct {p0, v2, v6}, Lcom/android/systemui/LoadAverageService$CpuTracker;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v7, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string/jumbo v2, "GPU freq: {0,number}"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->gpuInfo:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->gpuInfo:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/android/systemui/LoadAverageService$CpuTracker;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->gpuWidth:I

    :cond_0
    return-void
.end method

.method private getPower()Ljava/lang/String;
    .locals 11

    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Thermal: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/sys/class/power_supply/battery/uevent"

    invoke-direct {p0, v6, v10}, Lcom/android/systemui/LoadAverageService$CpuTracker;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, "\\n"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v5, v2

    if-ge v0, v5, :cond_1

    aget-object v5, v2, v0

    const-string/jumbo v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v5, "POWER_SUPPLY_SYSTEM_TEMP_LEVEL"

    aget-object v6, v1, v8

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->powerInfos:[Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "POWER_LEVEL: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v5, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->powerWidths:[I

    iget-object v6, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->powerInfos:[Ljava/lang/String;

    aget-object v6, v6, v8

    invoke-virtual {p0, v6}, Lcom/android/systemui/LoadAverageService$CpuTracker;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v6

    aput v6, v5, v8

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo v5, "/sys/class/power_supply/battery/current_now"

    invoke-direct {p0, v5, v10}, Lcom/android/systemui/LoadAverageService$CpuTracker;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->powerInfos:[Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "POWER_MAX: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v5, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->powerWidths:[I

    iget-object v6, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->powerInfos:[Ljava/lang/String;

    aget-object v6, v6, v9

    invoke-virtual {p0, v6}, Lcom/android/systemui/LoadAverageService$CpuTracker;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v6

    aput v6, v5, v9

    return-object v3
.end method

.method private getThermalValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/LoadAverageService$CpuTracker;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getWhichCpuOnline()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuLines:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "CPU online: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->ONLINE_CPU:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/android/systemui/LoadAverageService$CpuTracker;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuLinesWidth:[I

    iget-object v1, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuLines:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {p0, v1}, Lcom/android/systemui/LoadAverageService$CpuTracker;->onMeasureProcessName(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v3

    return-void
.end method

.method private static initForCpuInfo()V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v2, "ProcessCpuTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "cpu_combination: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuComb:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuComb:I

    const/16 v3, 0x9

    if-le v2, v3, :cond_0

    sget v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuComb:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_0

    const/4 v2, 0x2

    sput v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->CLUSTERS_TOTAL:I

    sput-boolean v5, Lcom/android/systemui/LoadAverageService$CpuTracker;->sShowCpuInfo:Z

    sget v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuComb:I

    div-int/lit8 v0, v2, 0xa

    sget v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->cpuComb:I

    rem-int/lit8 v1, v2, 0xa

    add-int v2, v0, v1

    sput v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->CPU_CORES_TOTAL:I

    sget v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->CLUSTERS_TOTAL:I

    new-array v2, v2, [I

    sput-object v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->clusterEndIndexes:[I

    sget-object v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->clusterEndIndexes:[I

    add-int/lit8 v3, v0, -0x1

    const/4 v4, 0x0

    aput v3, v2, v4

    sget-object v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->clusterEndIndexes:[I

    add-int v3, v0, v1

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v5

    :cond_0
    return-void
.end method

.method private static initForThermal()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string/jumbo v2, "xo_therm"

    invoke-static {v2, v5}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "case_therm"

    invoke-static {v2, v5}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    if-lez v1, :cond_0

    invoke-static {v6}, Lcom/android/systemui/LoadAverageService;->-set0(Z)Z

    :cond_0
    const-string/jumbo v2, "/sys/devices/virtual/thermal/thermal_zone{0,number}/temp"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->xoThermFile:Ljava/lang/String;

    const-string/jumbo v2, "/sys/devices/virtual/thermal/thermal_zone{0,number}/temp"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/systemui/LoadAverageService$CpuTracker;->caseThermFile:Ljava/lang/String;

    return-void
.end method

.method private readFile(Ljava/lang/String;C)Ljava/lang/String;
    .locals 10

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v6

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v7, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mBuffer:[B

    invoke-virtual {v4, v7}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    if-lez v5, :cond_2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    iget-object v7, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mBuffer:[B

    aget-byte v7, v7, v2

    if-ne v7, p2, :cond_1

    :cond_0
    new-instance v7, Ljava/lang/String;

    iget-object v8, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mBuffer:[B

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9, v2}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return-object v7

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    move-object v3, v4

    :goto_1
    const/4 v7, 0x0

    return-object v7

    :catch_0
    move-exception v1

    :goto_2
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_3
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_1

    :catchall_0
    move-exception v7

    :goto_4
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v7

    :catchall_1
    move-exception v7

    move-object v3, v4

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v3, v4

    goto :goto_3

    :catch_3
    move-exception v1

    move-object v3, v4

    goto :goto_2
.end method


# virtual methods
.method public final countCpuInfos()I
    .locals 1

    sget v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->CLUSTERS_TOTAL:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public onLoadChanged(FFF)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mLoadText:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mLoadText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mLoadWidth:I

    return-void
.end method

.method public onMeasureProcessName(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/LoadAverageService$CpuTracker;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public update()V
    .locals 1

    invoke-super {p0}, Lcom/android/internal/os/ProcessCpuTracker;->update()V

    sget-boolean v0, Lcom/android/systemui/LoadAverageService$CpuTracker;->sShowCpuInfo:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/LoadAverageService$CpuTracker;->getWhichCpuOnline()V

    invoke-direct {p0}, Lcom/android/systemui/LoadAverageService$CpuTracker;->getCpuWorkingFreq()V

    invoke-static {}, Lcom/android/systemui/LoadAverageService;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/LoadAverageService$CpuTracker;->getGPUFreq()V

    invoke-direct {p0}, Lcom/android/systemui/LoadAverageService$CpuTracker;->getGPUBusy()V

    invoke-direct {p0}, Lcom/android/systemui/LoadAverageService$CpuTracker;->collectThermals()V

    invoke-direct {p0}, Lcom/android/systemui/LoadAverageService$CpuTracker;->getPower()Ljava/lang/String;

    :cond_0
    return-void
.end method
