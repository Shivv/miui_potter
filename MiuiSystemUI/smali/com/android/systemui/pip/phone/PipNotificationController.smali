.class public Lcom/android/systemui/pip/phone/PipNotificationController;
.super Ljava/lang/Object;
.source "PipNotificationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/pip/phone/PipNotificationController$1;
    }
.end annotation


# static fields
.field private static final NOTIFICATION_TAG:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivityManager:Landroid/app/IActivityManager;

.field private mAppOpsChangedListener:Landroid/app/AppOpsManager$OnOpChangedListener;

.field private mAppOpsManager:Landroid/app/AppOpsManager;

.field private mContext:Landroid/content/Context;

.field private mDeferredNotificationPackageName:Ljava/lang/String;

.field private mMotionHelper:Lcom/android/systemui/pip/phone/PipMotionHelper;

.field private mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/pip/phone/PipNotificationController;)Landroid/app/AppOpsManager;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mAppOpsManager:Landroid/app/AppOpsManager;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/pip/phone/PipNotificationController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/pip/phone/PipNotificationController;)Lcom/android/systemui/pip/phone/PipMotionHelper;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mMotionHelper:Lcom/android/systemui/pip/phone/PipMotionHelper;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/pip/phone/PipNotificationController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/pip/phone/PipNotificationController;->unregisterAppOpsListener()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/systemui/pip/phone/PipNotificationController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/pip/phone/PipNotificationController;->TAG:Ljava/lang/String;

    const-class v0, Lcom/android/systemui/pip/phone/PipNotificationController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/pip/phone/PipNotificationController;->NOTIFICATION_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/IActivityManager;Lcom/android/systemui/pip/phone/PipMotionHelper;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/systemui/pip/phone/PipNotificationController$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/pip/phone/PipNotificationController$1;-><init>(Lcom/android/systemui/pip/phone/PipNotificationController;)V

    iput-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mAppOpsChangedListener:Landroid/app/AppOpsManager$OnOpChangedListener;

    iput-object p1, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mActivityManager:Landroid/app/IActivityManager;

    const-string/jumbo v0, "appops"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mAppOpsManager:Landroid/app/AppOpsManager;

    invoke-static {p1}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    iput-object p3, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mMotionHelper:Lcom/android/systemui/pip/phone/PipMotionHelper;

    return-void
.end method

.method private registerAppOpsListener(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mAppOpsManager:Landroid/app/AppOpsManager;

    iget-object v1, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mAppOpsChangedListener:Landroid/app/AppOpsManager$OnOpChangedListener;

    const/16 v2, 0x43

    invoke-virtual {v0, v2, p1, v1}, Landroid/app/AppOpsManager;->startWatchingMode(ILjava/lang/String;Landroid/app/AppOpsManager$OnOpChangedListener;)V

    return-void
.end method

.method private showNotificationForApp(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method private unregisterAppOpsListener()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mAppOpsManager:Landroid/app/AppOpsManager;

    iget-object v1, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mAppOpsChangedListener:Landroid/app/AppOpsManager$OnOpChangedListener;

    invoke-virtual {v0, v1}, Landroid/app/AppOpsManager;->stopWatchingMode(Landroid/app/AppOpsManager$OnOpChangedListener;)V

    return-void
.end method


# virtual methods
.method public onActivityPinned(Ljava/lang/String;Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v1, Lcom/android/systemui/pip/phone/PipNotificationController;->NOTIFICATION_TAG:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    if-eqz p2, :cond_0

    iput-object p1, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mDeferredNotificationPackageName:Ljava/lang/String;

    :goto_0
    invoke-direct {p0, p1}, Lcom/android/systemui/pip/phone/PipNotificationController;->registerAppOpsListener(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mDeferredNotificationPackageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/systemui/pip/phone/PipNotificationController;->showNotificationForApp(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onActivityUnpinned(Landroid/content/ComponentName;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/systemui/pip/phone/PipNotificationController;->unregisterAppOpsListener()V

    iput-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mDeferredNotificationPackageName:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/android/systemui/pip/phone/PipNotificationController;->onActivityPinned(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v1, Lcom/android/systemui/pip/phone/PipNotificationController;->NOTIFICATION_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onPinnedStackAnimationEnded()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mDeferredNotificationPackageName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mDeferredNotificationPackageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/systemui/pip/phone/PipNotificationController;->showNotificationForApp(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/systemui/pip/phone/PipNotificationController;->mDeferredNotificationPackageName:Ljava/lang/String;

    :cond_0
    return-void
.end method
