.class public Lcom/android/systemui/CompatibilityN;
.super Ljava/lang/Object;
.source "CompatibilityN.java"


# static fields
.field private static final FALLBACK_HOME_COMPONENT:Landroid/content/ComponentName;

.field private static sUserUnlocked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/systemui/CompatibilityN;->sUserUnlocked:Z

    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v1, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.FallbackHome"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/systemui/CompatibilityN;->FALLBACK_HOME_COMPONENT:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cacheContentViews(Lcom/android/systemui/statusbar/NotificationData$Entry;Landroid/content/Context;Landroid/app/Notification;)Z
    .locals 11

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-static {p1, p2}, Landroid/app/Notification$Builder;->recoverBuilder(Landroid/content/Context;Landroid/app/Notification;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Notification$Builder;->createContentView()Landroid/widget/RemoteViews;

    move-result-object v3

    invoke-virtual {v7}, Landroid/app/Notification$Builder;->createBigContentView()Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v7}, Landroid/app/Notification$Builder;->createHeadsUpContentView()Landroid/widget/RemoteViews;

    move-result-object v4

    invoke-virtual {v7}, Landroid/app/Notification$Builder;->makePublicContentView()Landroid/widget/RemoteViews;

    move-result-object v5

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v8}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iget-object v8, v8, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string/jumbo v9, "android.contains.customView"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iget-object v9, p2, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string/jumbo v10, "android.contains.customView"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v8, v9}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedContentView:Landroid/widget/RemoteViews;

    invoke-static {v8, v3}, Lcom/android/systemui/CompatibilityN;->compareRemoteViews(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedBigContentView:Landroid/widget/RemoteViews;

    invoke-static {v8, v2}, Lcom/android/systemui/CompatibilityN;->compareRemoteViews(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedHeadsUpContentView:Landroid/widget/RemoteViews;

    invoke-static {v8, v4}, Lcom/android/systemui/CompatibilityN;->compareRemoteViews(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedPublicContentView:Landroid/widget/RemoteViews;

    invoke-static {v8, v5}, Lcom/android/systemui/CompatibilityN;->compareRemoteViews(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z

    move-result v8

    if-eqz v8, :cond_0

    move v0, v6

    :goto_0
    iput-object v5, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedPublicContentView:Landroid/widget/RemoteViews;

    iput-object v4, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedHeadsUpContentView:Landroid/widget/RemoteViews;

    iput-object v2, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedBigContentView:Landroid/widget/RemoteViews;

    iput-object v3, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedContentView:Landroid/widget/RemoteViews;

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v8}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v8

    invoke-static {p1, v8}, Landroid/app/Notification$Builder;->recoverBuilder(Landroid/content/Context;Landroid/app/Notification;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->createContentView()Landroid/widget/RemoteViews;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->createBigContentView()Landroid/widget/RemoteViews;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedBigContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->createHeadsUpContentView()Landroid/widget/RemoteViews;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedHeadsUpContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->makePublicContentView()Landroid/widget/RemoteViews;

    move-result-object v8

    iput-object v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedPublicContentView:Landroid/widget/RemoteViews;

    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static compareRemoteViews(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static getBigContentView(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/ExpandedNotification;)Landroid/widget/RemoteViews;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedBigContentView:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method public static getContentView(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/ExpandedNotification;)Landroid/widget/RemoteViews;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->cachedContentView:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method public static getRecentTaskBound(Landroid/app/ActivityManager$RecentTaskInfo;)Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Landroid/app/ActivityManager$RecentTaskInfo;->bounds:Landroid/graphics/Rect;

    return-object v0
.end method

.method public static getRecentTaskResizeMode(Landroid/app/ActivityManager$RecentTaskInfo;)I
    .locals 1

    iget v0, p0, Landroid/app/ActivityManager$RecentTaskInfo;->resizeMode:I

    return v0
.end method

.method public static getRunningTaskResizeMode(Landroid/app/ActivityManager$RunningTaskInfo;)I
    .locals 1

    iget v0, p0, Landroid/app/ActivityManager$RunningTaskInfo;->resizeMode:I

    return v0
.end method

.method public static getRunningTaskStackId(Landroid/app/ActivityManager$RunningTaskInfo;)I
    .locals 1

    iget v0, p0, Landroid/app/ActivityManager$RunningTaskInfo;->stackId:I

    return v0
.end method

.method public static getTaskDescriptionBackgroundColor(Landroid/app/ActivityManager$TaskDescription;)I
    .locals 1

    invoke-virtual {p0}, Landroid/app/ActivityManager$TaskDescription;->getBackgroundColor()I

    move-result v0

    return v0
.end method

.method public static getTaskThumbnailInfo(Landroid/app/ActivityManager$TaskThumbnail;)Lcom/android/systemui/proxy/ActivityManager$TaskThumbnailInfo;
    .locals 2

    new-instance v0, Lcom/android/systemui/proxy/ActivityManager$TaskThumbnailInfo;

    invoke-direct {v0}, Lcom/android/systemui/proxy/ActivityManager$TaskThumbnailInfo;-><init>()V

    iget-object v1, p0, Landroid/app/ActivityManager$TaskThumbnail;->thumbnailInfo:Landroid/app/ActivityManager$TaskThumbnailInfo;

    iget v1, v1, Landroid/app/ActivityManager$TaskThumbnailInfo;->taskWidth:I

    iput v1, v0, Lcom/android/systemui/proxy/ActivityManager$TaskThumbnailInfo;->taskWidth:I

    iget-object v1, p0, Landroid/app/ActivityManager$TaskThumbnail;->thumbnailInfo:Landroid/app/ActivityManager$TaskThumbnailInfo;

    iget v1, v1, Landroid/app/ActivityManager$TaskThumbnailInfo;->taskHeight:I

    iput v1, v0, Lcom/android/systemui/proxy/ActivityManager$TaskThumbnailInfo;->taskHeight:I

    iget-object v1, p0, Landroid/app/ActivityManager$TaskThumbnail;->thumbnailInfo:Landroid/app/ActivityManager$TaskThumbnailInfo;

    iget v1, v1, Landroid/app/ActivityManager$TaskThumbnailInfo;->screenOrientation:I

    iput v1, v0, Lcom/android/systemui/proxy/ActivityManager$TaskThumbnailInfo;->screenOrientation:I

    return-object v0
.end method

.method public static getUserHandleSystem()Landroid/os/UserHandle;
    .locals 1

    sget-object v0, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    return-object v0
.end method

.method public static handleRemoteInput(Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)Z
    .locals 22

    const v18, 0x10203a6

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v14

    const/4 v8, 0x0

    instance-of v0, v14, [Landroid/app/RemoteInput;

    move/from16 v18, v0

    if-eqz v18, :cond_0

    move-object v8, v14

    check-cast v8, [Landroid/app/RemoteInput;

    :cond_0
    if-nez v8, :cond_1

    const/16 v18, 0x0

    return v18

    :cond_1
    const/4 v7, 0x0

    const/16 v18, 0x0

    array-length v0, v8

    move/from16 v19, v0

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    aget-object v5, v8, v18

    invoke-virtual {v5}, Landroid/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v20

    if-eqz v20, :cond_2

    move-object v7, v5

    :cond_2
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    :cond_3
    if-nez v7, :cond_4

    const/16 v18, 0x0

    return v18

    :cond_4
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    const/4 v12, 0x0

    :goto_1
    if-eqz v9, :cond_5

    instance-of v0, v9, Landroid/view/View;

    move/from16 v18, v0

    if-eqz v18, :cond_8

    move-object v10, v9

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->isRootNamespace()Z

    move-result v18

    if-eqz v18, :cond_8

    sget-object v18, Lcom/android/systemui/statusbar/policy/RemoteInputView;->VIEW_TAG:Ljava/lang/Object;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/android/systemui/statusbar/policy/RemoteInputView;

    :cond_5
    const/4 v13, 0x0

    :goto_2
    if-eqz v9, :cond_6

    instance-of v0, v9, Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    move/from16 v18, v0

    if-eqz v18, :cond_9

    move-object v13, v9

    check-cast v13, Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    :cond_6
    if-eqz v12, :cond_7

    if-nez v13, :cond_a

    :cond_7
    const/16 v18, 0x0

    return v18

    :cond_8
    invoke-interface {v9}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    goto :goto_1

    :cond_9
    invoke-interface {v9}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    goto :goto_2

    :cond_a
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->setUserExpanded(Z)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v17

    move-object/from16 v0, p0

    instance-of v0, v0, Landroid/widget/TextView;

    move/from16 v18, v0

    if-eqz v18, :cond_b

    move-object/from16 v15, p0

    check-cast v15, Landroid/widget/TextView;

    invoke-virtual {v15}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v18

    if-eqz v18, :cond_b

    invoke-virtual {v15}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v6, v0

    invoke-virtual {v15}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v18

    invoke-virtual {v15}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v19

    add-int v18, v18, v19

    add-int v6, v6, v18

    move/from16 v0, v17

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v17

    :cond_b
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLeft()I

    move-result v18

    div-int/lit8 v19, v17, 0x2

    add-int v2, v18, v19

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTop()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    add-int v3, v18, v19

    invoke-virtual {v12}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->getWidth()I

    move-result v16

    invoke-virtual {v12}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->getHeight()I

    move-result v4

    add-int v18, v2, v3

    sub-int v19, v4, v3

    add-int v19, v19, v2

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v18

    sub-int v19, v16, v2

    add-int v19, v19, v3

    sub-int v20, v16, v2

    sub-int v21, v4, v3

    add-int v20, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v19

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-virtual {v12, v2, v3, v11}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->setRevealParameters(III)V

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->setPendingIntent(Landroid/app/PendingIntent;)V

    invoke-virtual {v12, v8, v7}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->setRemoteInput([Landroid/app/RemoteInput;Landroid/app/RemoteInput;)V

    invoke-virtual {v12}, Lcom/android/systemui/statusbar/policy/RemoteInputView;->focusAnimated()V

    const/16 v18, 0x1

    return v18
.end method

.method public static hasDockedTask(Landroid/app/IActivityManager;)Z
    .locals 7

    const/4 v6, 0x0

    if-nez p0, :cond_0

    return v6

    :cond_0
    const/4 v3, 0x0

    const/4 v5, 0x3

    :try_start_0
    invoke-interface {p0, v5}, Landroid/app/IActivityManager;->getStackInfo(I)Landroid/app/ActivityManager$StackInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_3

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v4

    const/4 v1, 0x0

    iget-object v5, v3, Landroid/app/ActivityManager$StackInfo;->taskUserIds:[I

    array-length v5, v5

    add-int/lit8 v2, v5, -0x1

    :goto_1
    if-ltz v2, :cond_2

    xor-int/lit8 v5, v1, 0x1

    if-eqz v5, :cond_2

    iget-object v5, v3, Landroid/app/ActivityManager$StackInfo;->taskUserIds:[I

    aget v5, v5, v2

    if-ne v5, v4, :cond_1

    const/4 v1, 0x1

    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    return v1

    :cond_3
    return v6
.end method

.method public static isUserQuietModeEnabled(Landroid/content/pm/UserInfo;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/content/pm/UserInfo;->isQuietModeEnabled()Z

    move-result v0

    return v0
.end method

.method public static isUserUnlocked(Landroid/content/Context;)Z
    .locals 1

    sget-boolean v0, Lcom/android/systemui/CompatibilityN;->sUserUnlocked:Z

    if-nez v0, :cond_0

    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isUserUnlocked()Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/CompatibilityN;->sUserUnlocked:Z

    :cond_0
    sget-boolean v0, Lcom/android/systemui/CompatibilityN;->sUserUnlocked:Z

    return v0
.end method

.method public static loadTaskDescriptionIcon(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 1

    invoke-static {p0, p1}, Landroid/app/ActivityManager$TaskDescription;->loadTaskDescriptionIcon(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static queryBroadcastReceiversAsUser(Landroid/content/Context;Landroid/content/Intent;II)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "II)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v2, -0x2

    invoke-virtual {v0, p1, p2, v2}, Landroid/content/pm/PackageManager;->queryBroadcastReceiversAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public static setDrawDuringWindowsAnimating(Landroid/view/ViewRootImpl;)V
    .locals 0

    return-void
.end method

.method public static setOptionsLaunchBounds(Landroid/app/ActivityOptions;Landroid/graphics/Rect;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/app/ActivityOptions;->setLaunchBounds(Landroid/graphics/Rect;)Landroid/app/ActivityOptions;

    return-void
.end method

.method public static setOverrideGroupKey(Lcom/android/systemui/statusbar/ExpandedNotification;Landroid/service/notification/StatusBarNotification;)V
    .locals 1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getOverrideGroupKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/ExpandedNotification;->setOverrideGroupKey(Ljava/lang/String;)V

    return-void
.end method

.method public static startTaskInDockedMode(Lcom/android/systemui/recents/model/Task;ILandroid/app/IActivityManager;Landroid/content/Context;)Z
    .locals 6

    const/4 v5, 0x0

    if-nez p2, :cond_0

    return v5

    :cond_0
    :try_start_0
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/ActivityOptions;->setDockCreateMode(I)V

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/app/ActivityOptions;->setLaunchStackId(I)V

    iget-object v2, p0, Lcom/android/systemui/recents/model/Task;->key:Lcom/android/systemui/recents/model/Task$TaskKey;

    iget v2, v2, Lcom/android/systemui/recents/model/Task$TaskKey;->id:I

    invoke-virtual {v1}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {p2, v2, v3}, Landroid/app/IActivityManager;->startActivityFromRecents(ILandroid/os/Bundle;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v0

    const-string/jumbo v2, "SystemServicesProxy"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Failed to dock task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " with createMode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return v5
.end method

.method public static supportsMultiWindow()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static updateQuietState(Landroid/content/Context;I)Z
    .locals 6

    const/4 v0, 0x0

    const-string/jumbo v4, "user"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    if-nez v3, :cond_0

    const-string/jumbo v4, "PhoneStatusBarPolicy"

    const-string/jumbo v5, "updateQuietState() mUserManager == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    invoke-virtual {v3, p1}, Landroid/os/UserManager;->getEnabledProfiles(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/UserInfo;

    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isQuietModeEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    return v0
.end method

.method public static wrapExpandedLarge(Landroid/view/View;Landroid/content/Context;Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/BaseStatusBar;)V
    .locals 1

    new-instance v0, Lcom/android/systemui/statusbar/NotificationContentViewUtil;

    invoke-direct {v0, p3}, Lcom/android/systemui/statusbar/NotificationContentViewUtil;-><init>(Lcom/android/systemui/statusbar/BaseStatusBar;)V

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->setExpandChild(Landroid/view/View;)V

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/NotificationContentViewUtil;->onNotificationUpdated(Landroid/content/Context;Lcom/android/systemui/statusbar/NotificationData$Entry;)V

    return-void
.end method
