.class Lcom/android/systemui/recents/BaseRecentsImpl$3;
.super Landroid/content/BroadcastReceiver;
.source "BaseRecentsImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/recents/BaseRecentsImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/recents/BaseRecentsImpl;


# direct methods
.method constructor <init>(Lcom/android/systemui/recents/BaseRecentsImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    const-string/jumbo v2, "android.intent.action.USER_SWITCHED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v2}, Lcom/android/systemui/recents/BaseRecentsImpl;->-get5(Lcom/android/systemui/recents/BaseRecentsImpl;)Lcom/android/systemui/statusbar/phone/NavStubView;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    const-string/jumbo v2, "android.intent.extra.user_handle"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    if-ne v2, v0, :cond_2

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v2, v4}, Lcom/android/systemui/recents/BaseRecentsImpl;->-set1(Lcom/android/systemui/recents/BaseRecentsImpl;Z)Z

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v2, v2, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "force_fsg_nav_bar"

    invoke-static {v2, v3}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v2}, Lcom/android/systemui/recents/BaseRecentsImpl;->-get5(Lcom/android/systemui/recents/BaseRecentsImpl;)Lcom/android/systemui/statusbar/phone/NavStubView;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v2}, Lcom/android/systemui/recents/BaseRecentsImpl;->-wrap3(Lcom/android/systemui/recents/BaseRecentsImpl;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/systemui/recents/BaseRecentsImpl;->-set1(Lcom/android/systemui/recents/BaseRecentsImpl;Z)Z

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v2}, Lcom/android/systemui/recents/BaseRecentsImpl;->-get5(Lcom/android/systemui/recents/BaseRecentsImpl;)Lcom/android/systemui/statusbar/phone/NavStubView;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$3;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v2}, Lcom/android/systemui/recents/BaseRecentsImpl;->-wrap1(Lcom/android/systemui/recents/BaseRecentsImpl;)V

    goto :goto_0
.end method
