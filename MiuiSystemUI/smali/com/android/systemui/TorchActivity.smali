.class public Lcom/android/systemui/TorchActivity;
.super Landroid/app/Activity;
.source "TorchActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/TorchActivity$1;
    }
.end annotation


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mHint:Landroid/widget/TextView;

.field private mStateObserver:Landroid/database/ContentObserver;

.field private mTorch:Landroid/widget/ImageView;

.field private mTorchOn:Z

.field private mVibrator:Landroid/os/Vibrator;

.field private vibratePatternOff:[J

.field private vibratePatternOn:[J


# direct methods
.method static synthetic -set0(Lcom/android/systemui/TorchActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/TorchActivity;->mTorchOn:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/TorchActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/TorchActivity;->updateTorchState()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/systemui/TorchActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/TorchActivity$1;-><init>(Lcom/android/systemui/TorchActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/TorchActivity;->mStateObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method private addTorchMessageSpans(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 9

    const/4 v8, 0x0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string/jumbo v3, "[icon]"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    if-ltz v5, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020098

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual {v2, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    new-instance v6, Landroid/text/style/ImageSpan;

    invoke-direct {v6, v2, v8}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    add-int v7, v5, v4

    const/16 v8, 0x11

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    return-object v0
.end method

.method private getLongArray([I)[J
    .locals 4

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return-object v2

    :cond_0
    array-length v2, p1

    new-array v1, v2, [J

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget v2, p1, v0

    int-to-long v2, v2

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private sendAccessibilityEventIfNeed()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/TorchActivity;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/TorchActivity;->mTorch:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/android/systemui/TorchActivity;->mTorchOn:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0d0299

    invoke-virtual {p0, v0}, Lcom/android/systemui/TorchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/TorchActivity;->mTorch:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0d0298

    invoke-virtual {p0, v0}, Lcom/android/systemui/TorchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private toggleTorch()V
    .locals 3

    iget-boolean v1, p0, Lcom/android/systemui/TorchActivity;->mTorchOn:Z

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/TorchActivity;->mTorchOn:Z

    invoke-direct {p0}, Lcom/android/systemui/TorchActivity;->updateTorchState()V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.TOGGLE_TORCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "miui.intent.extra.IS_TOGGLE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/systemui/TorchActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private updateTorchState()V
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/TorchActivity;->mTorch:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/android/systemui/TorchActivity;->mTorchOn:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020311

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/android/systemui/TorchActivity;->mHint:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/android/systemui/TorchActivity;->mTorchOn:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const v0, 0x7f020310

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/16 v2, 0x400

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "accessibility"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    iput-object v1, p0, Lcom/android/systemui/TorchActivity;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/android/systemui/TorchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/android/systemui/TorchActivity;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080042

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/systemui/TorchActivity;->getLongArray([I)[J

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/TorchActivity;->vibratePatternOn:[J

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/systemui/TorchActivity;->getLongArray([I)[J

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/TorchActivity;->vibratePatternOff:[J

    const v1, 0x7f030072

    invoke-virtual {p0, v1}, Lcom/android/systemui/TorchActivity;->setContentView(I)V

    const v1, 0x7f0f01d6

    invoke-virtual {p0, v1}, Lcom/android/systemui/TorchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/TorchActivity;->mTorch:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/systemui/TorchActivity;->mTorch:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v1, 0x7f0f01d7

    invoke-virtual {p0, v1}, Lcom/android/systemui/TorchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/TorchActivity;->mHint:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/TorchActivity;->mHint:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0297

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/systemui/TorchActivity;->addTorchMessageSpans(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v4, p0, Lcom/android/systemui/TorchActivity;->mTorchOn:Z

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.TOGGLE_TORCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "miui.intent.extra.IS_ENABLE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/systemui/TorchActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/TorchActivity;->mStateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "torch_state"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/TorchActivity;->mStateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/systemui/TorchActivity;->mStateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/TorchActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "haptic_feedback_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v3, v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/TorchActivity;->mVibrator:Landroid/os/Vibrator;

    iget-boolean v0, p0, Lcom/android/systemui/TorchActivity;->mTorchOn:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/TorchActivity;->vibratePatternOff:[J

    :goto_0
    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/TorchActivity;->sendAccessibilityEventIfNeed()V

    invoke-direct {p0}, Lcom/android/systemui/TorchActivity;->toggleTorch()V

    :cond_1
    return v3

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/TorchActivity;->vibratePatternOn:[J

    goto :goto_0
.end method
