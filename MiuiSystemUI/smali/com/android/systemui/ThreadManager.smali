.class public Lcom/android/systemui/ThreadManager;
.super Ljava/lang/Object;
.source "ThreadManager.java"


# static fields
.field private static final sHandlerList:[Landroid/os/Handler;

.field private static final sThreadNameList:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x2

    new-array v0, v1, [Landroid/os/Handler;

    sput-object v0, Lcom/android/systemui/ThreadManager;->sHandlerList:[Landroid/os/Handler;

    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, "thread_ui"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "thread_shared_pref"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/ThreadManager;->sThreadNameList:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHandler(I)Landroid/os/Handler;
    .locals 5

    if-ltz p0, :cond_0

    const/4 v1, 0x2

    if-lt p0, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/security/InvalidParameterException;

    invoke-direct {v1}, Ljava/security/InvalidParameterException;-><init>()V

    throw v1

    :cond_1
    sget-object v1, Lcom/android/systemui/ThreadManager;->sHandlerList:[Landroid/os/Handler;

    aget-object v1, v1, p0

    if-nez v1, :cond_4

    sget-object v2, Lcom/android/systemui/ThreadManager;->sHandlerList:[Landroid/os/Handler;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/android/systemui/ThreadManager;->sHandlerList:[Landroid/os/Handler;

    aget-object v1, v1, p0

    if-nez v1, :cond_3

    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, Lcom/android/systemui/ThreadManager;->sThreadNameList:[Ljava/lang/String;

    aget-object v1, v1, p0

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    if-eqz p0, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/HandlerThread;->setPriority(I)V

    :cond_2
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    sget-object v1, Lcom/android/systemui/ThreadManager;->sHandlerList:[Landroid/os/Handler;

    new-instance v3, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    aput-object v3, v1, p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit v2

    :cond_4
    sget-object v1, Lcom/android/systemui/ThreadManager;->sHandlerList:[Landroid/os/Handler;

    aget-object v1, v1, p0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
