.class public Lcom/xiaomi/analytics/internal/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/analytics/internal/c;,
        Lcom/xiaomi/analytics/internal/i;,
        Lcom/xiaomi/analytics/internal/j;,
        Lcom/xiaomi/analytics/internal/k;,
        Lcom/xiaomi/analytics/internal/l;,
        Lcom/xiaomi/analytics/internal/m;
    }
.end annotation


# static fields
.field private static aC:Ljava/lang/Object;

.field private static volatile aD:Lcom/xiaomi/analytics/internal/b;

.field private static final aj:I


# instance fields
.field private aA:Lcom/xiaomi/analytics/internal/a/c;

.field private aB:Ljava/lang/Runnable;

.field private al:Ljava/lang/Runnable;

.field private am:Lcom/xiaomi/analytics/internal/a/a;

.field private an:Lcom/xiaomi/analytics/internal/e;

.field private ao:Z

.field private ap:Landroid/os/Handler;

.field private aq:Landroid/os/HandlerThread;

.field private ar:Ljava/lang/Runnable;

.field private volatile as:Z

.field private at:J

.field private au:J

.field private av:Lcom/xiaomi/analytics/internal/a/a;

.field private aw:Lcom/xiaomi/analytics/PolicyConfiguration;

.field private ax:Z

.field private ay:Landroid/content/BroadcastReceiver;

.field private az:Lcom/xiaomi/analytics/internal/c;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lcom/xiaomi/analytics/internal/util/l;->x:I

    mul-int/lit8 v0, v0, 0x1e

    sput v0, Lcom/xiaomi/analytics/internal/b;->aj:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/xiaomi/analytics/internal/b;->aw:Lcom/xiaomi/analytics/PolicyConfiguration;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/analytics/internal/b;->au:J

    iput-boolean v2, p0, Lcom/xiaomi/analytics/internal/b;->as:Z

    iput-boolean v2, p0, Lcom/xiaomi/analytics/internal/b;->ao:Z

    iput-object v3, p0, Lcom/xiaomi/analytics/internal/b;->av:Lcom/xiaomi/analytics/internal/a/a;

    new-instance v0, Lcom/xiaomi/analytics/internal/i;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/i;-><init>(Lcom/xiaomi/analytics/internal/b;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aB:Ljava/lang/Runnable;

    new-instance v0, Lcom/xiaomi/analytics/internal/j;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/j;-><init>(Lcom/xiaomi/analytics/internal/b;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->ar:Ljava/lang/Runnable;

    new-instance v0, Lcom/xiaomi/analytics/internal/k;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/k;-><init>(Lcom/xiaomi/analytics/internal/b;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->an:Lcom/xiaomi/analytics/internal/e;

    new-instance v0, Lcom/xiaomi/analytics/internal/l;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/l;-><init>(Lcom/xiaomi/analytics/internal/b;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->ay:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/xiaomi/analytics/internal/m;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/m;-><init>(Lcom/xiaomi/analytics/internal/b;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->al:Ljava/lang/Runnable;

    invoke-static {p1}, Lcom/xiaomi/analytics/internal/util/h;->z(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "connectivity"

    sput-object v0, Lcom/xiaomi/analytics/internal/b;->aC:Ljava/lang/Object;

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "api-sdkmgr"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aq:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aq:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->aq:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->ap:Landroid/os/Handler;

    new-instance v0, Lcom/xiaomi/analytics/internal/a/c;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/xiaomi/analytics/internal/a/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aA:Lcom/xiaomi/analytics/internal/a/c;

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/d;->getInstance(Landroid/content/Context;)Lcom/xiaomi/analytics/internal/d;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->an:Lcom/xiaomi/analytics/internal/e;

    invoke-virtual {v0, v1}, Lcom/xiaomi/analytics/internal/d;->bw(Lcom/xiaomi/analytics/internal/e;)V

    sget-object v0, Lcom/xiaomi/analytics/internal/util/a;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->ar:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private declared-synchronized aC()V
    .locals 6

    monitor-enter p0

    :try_start_0
    sget v0, Lcom/xiaomi/analytics/internal/util/l;->y:I

    int-to-long v0, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/analytics/internal/b;->au:J

    sub-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/analytics/internal/b;->au:J

    sget-object v0, Lcom/xiaomi/analytics/internal/util/a;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->aB:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private aD()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "analytics_asset.apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aE()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/asset_lib/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aF()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "analytics_api"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "pld"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "SdkManager"

    invoke-static {v2}, Lcom/xiaomi/analytics/internal/util/f;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "getPreviousLoadDex exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_0
.end method

.method private aG()I
    .locals 1

    sget-boolean v0, Lcom/xiaomi/analytics/internal/util/f;->i:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x2710

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/xiaomi/analytics/internal/b;->aj:I

    goto :goto_0
.end method

.method private aH()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "analytics.apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aI()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "analytics"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aJ()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/lib/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aK()Z
    .locals 4

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aG()I

    move-result v0

    iget-boolean v1, p0, Lcom/xiaomi/analytics/internal/b;->ax:Z

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/xiaomi/analytics/internal/b;->at:J

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lcom/xiaomi/analytics/internal/util/l;->U(JJ)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aL()Lcom/xiaomi/analytics/internal/a/a;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    const-string/jumbo v1, "analytics_core"

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-object v3, v2, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    aget-object v3, v2, v0

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    aget-object v4, v2, v0

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aD()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/xiaomi/analytics/internal/util/d;->j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aD()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aD()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aE()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/xiaomi/analytics/internal/util/i;->D(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/xiaomi/analytics/internal/a/b;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aD()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aE()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/analytics/internal/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "SdkManager"

    invoke-static {v1}, Lcom/xiaomi/analytics/internal/util/f;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "loadAssetAnalytics exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    return-object v6
.end method

.method private aM()V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aF()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/b;->av:Lcom/xiaomi/analytics/internal/a/a;

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aS()V

    return-void
.end method

.method private aN()Lcom/xiaomi/analytics/internal/a/a;
    .locals 4

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aH()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aJ()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/xiaomi/analytics/internal/util/i;->D(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/xiaomi/analytics/internal/a/b;

    iget-object v2, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aJ()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/xiaomi/analytics/internal/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "SdkManager"

    invoke-static {v1}, Lcom/xiaomi/analytics/internal/util/f;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "loadLocalAnalytics exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private aO()Lcom/xiaomi/analytics/internal/a/a;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aA:Lcom/xiaomi/analytics/internal/a/c;

    invoke-virtual {v0}, Lcom/xiaomi/analytics/internal/a/c;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aA:Lcom/xiaomi/analytics/internal/a/c;

    invoke-virtual {v0}, Lcom/xiaomi/analytics/internal/a/c;->ak()V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aA:Lcom/xiaomi/analytics/internal/a/c;

    return-object v0
.end method

.method private aP(Lcom/xiaomi/analytics/internal/a/a;)V
    .locals 3

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->az:Lcom/xiaomi/analytics/internal/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    sget-boolean v1, Lcom/xiaomi/analytics/internal/util/f;->i:Z

    invoke-interface {v0, v1}, Lcom/xiaomi/analytics/internal/a/a;->setDebugOn(Z)V

    const-string/jumbo v0, "SdkManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Analytics module loaded, version is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    invoke-interface {v2}, Lcom/xiaomi/analytics/internal/a/a;->getVersion()Lcom/xiaomi/analytics/internal/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/analytics/internal/util/f;->p(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->az:Lcom/xiaomi/analytics/internal/c;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    invoke-interface {v0, v1}, Lcom/xiaomi/analytics/internal/c;->onSdkCorePrepared(Lcom/xiaomi/analytics/internal/a/a;)V

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aw:Lcom/xiaomi/analytics/PolicyConfiguration;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aw:Lcom/xiaomi/analytics/PolicyConfiguration;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    invoke-virtual {v0, v1}, Lcom/xiaomi/analytics/PolicyConfiguration;->apply(Lcom/xiaomi/analytics/internal/a/a;)V

    :cond_1
    return-void
.end method

.method private aQ(J)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->ap:Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->al:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->ap:Landroid/os/Handler;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->al:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string/jumbo v0, "SdkManager"

    const-string/jumbo v1, "post dex init task"

    invoke-static {v0, v1}, Lcom/xiaomi/analytics/internal/util/f;->p(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private aR()V
    .locals 2

    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aJ()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :goto_0
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aE()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :goto_1
    return-void

    :cond_0
    invoke-static {v0}, Lcom/xiaomi/analytics/internal/util/c;->g(Ljava/io/File;)V

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/xiaomi/analytics/internal/util/c;->g(Ljava/io/File;)V

    goto :goto_1
.end method

.method private aS()V
    .locals 3

    const-string/jumbo v0, "SdkManager"

    const-string/jumbo v1, "register screen receiver"

    invoke-static {v0, v1}, Lcom/xiaomi/analytics/internal/util/f;->p(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/analytics/internal/b;->ay:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private aT(Z)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "analytics_api"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pld"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "SdkManager"

    invoke-static {v1}, Lcom/xiaomi/analytics/internal/util/f;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "savePreviousLoadDex exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic aU(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    return-object v0
.end method

.method static synthetic aV(Lcom/xiaomi/analytics/internal/b;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic aW(Lcom/xiaomi/analytics/internal/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/analytics/internal/b;->ao:Z

    return v0
.end method

.method static synthetic aX(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->av:Lcom/xiaomi/analytics/internal/a/a;

    return-object v0
.end method

.method static synthetic aY(Lcom/xiaomi/analytics/internal/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/analytics/internal/b;->ax:Z

    return v0
.end method

.method static synthetic aZ(Lcom/xiaomi/analytics/internal/b;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->ay:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic ba(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/c;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aA:Lcom/xiaomi/analytics/internal/a/c;

    return-object v0
.end method

.method static synthetic bb()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/xiaomi/analytics/internal/b;->aC:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic bc()Lcom/xiaomi/analytics/internal/b;
    .locals 1

    sget-object v0, Lcom/xiaomi/analytics/internal/b;->aD:Lcom/xiaomi/analytics/internal/b;

    return-object v0
.end method

.method static synthetic bd(Lcom/xiaomi/analytics/internal/b;Lcom/xiaomi/analytics/internal/a/a;)Lcom/xiaomi/analytics/internal/a/a;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    return-object p1
.end method

.method static synthetic be(Lcom/xiaomi/analytics/internal/b;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/analytics/internal/b;->as:Z

    return p1
.end method

.method static synthetic bf(Lcom/xiaomi/analytics/internal/b;J)J
    .locals 1

    iput-wide p1, p0, Lcom/xiaomi/analytics/internal/b;->at:J

    return-wide p1
.end method

.method static synthetic bg(Lcom/xiaomi/analytics/internal/b;Lcom/xiaomi/analytics/internal/a/a;)Lcom/xiaomi/analytics/internal/a/a;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/b;->av:Lcom/xiaomi/analytics/internal/a/a;

    return-object p1
.end method

.method static synthetic bh(Lcom/xiaomi/analytics/internal/b;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/analytics/internal/b;->ax:Z

    return p1
.end method

.method static synthetic bi(Lcom/xiaomi/analytics/internal/b;)Z
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aK()Z

    move-result v0

    return v0
.end method

.method static synthetic bj(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aL()Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bk(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aN()Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bl(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aO()Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bm(Lcom/xiaomi/analytics/internal/b;)I
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aG()I

    move-result v0

    return v0
.end method

.method static synthetic bn(Lcom/xiaomi/analytics/internal/b;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aH()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bo(Lcom/xiaomi/analytics/internal/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aC()V

    return-void
.end method

.method static synthetic bp(Lcom/xiaomi/analytics/internal/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aM()V

    return-void
.end method

.method static synthetic bq(Lcom/xiaomi/analytics/internal/b;Lcom/xiaomi/analytics/internal/a/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/analytics/internal/b;->aP(Lcom/xiaomi/analytics/internal/a/a;)V

    return-void
.end method

.method static synthetic br(Lcom/xiaomi/analytics/internal/b;J)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/analytics/internal/b;->aQ(J)V

    return-void
.end method

.method static synthetic bs(Lcom/xiaomi/analytics/internal/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aR()V

    return-void
.end method

.method static synthetic bt(Lcom/xiaomi/analytics/internal/b;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/analytics/internal/b;->aT(Z)V

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/xiaomi/analytics/internal/b;
    .locals 2

    const-class v1, Lcom/xiaomi/analytics/internal/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/xiaomi/analytics/internal/b;->aD:Lcom/xiaomi/analytics/internal/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/analytics/internal/b;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/analytics/internal/b;->aD:Lcom/xiaomi/analytics/internal/b;

    :cond_0
    sget-object v0, Lcom/xiaomi/analytics/internal/b;->aD:Lcom/xiaomi/analytics/internal/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public aA()Lcom/xiaomi/analytics/internal/a/a;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    return-object v0
.end method

.method public aB()V
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/analytics/internal/b;->as:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/b;->aC()V

    :cond_0
    return-void
.end method

.method public az(Lcom/xiaomi/analytics/internal/c;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/b;->az:Lcom/xiaomi/analytics/internal/c;

    return-void
.end method

.method public getVersion()Lcom/xiaomi/analytics/internal/g;
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/analytics/internal/b;->aA()Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/analytics/internal/b;->aA()Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/xiaomi/analytics/internal/a/a;->getVersion()Lcom/xiaomi/analytics/internal/g;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lcom/xiaomi/analytics/internal/g;

    const-string/jumbo v1, "0.0.0"

    invoke-direct {v0, v1}, Lcom/xiaomi/analytics/internal/g;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public setDontUseSystemAnalytics(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/analytics/internal/b;->ao:Z

    return-void
.end method

.method public setPolicyConfiguration(Lcom/xiaomi/analytics/PolicyConfiguration;)V
    .locals 2

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/b;->aw:Lcom/xiaomi/analytics/PolicyConfiguration;

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aw:Lcom/xiaomi/analytics/PolicyConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/b;->aw:Lcom/xiaomi/analytics/PolicyConfiguration;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/b;->am:Lcom/xiaomi/analytics/internal/a/a;

    invoke-virtual {v0, v1}, Lcom/xiaomi/analytics/PolicyConfiguration;->apply(Lcom/xiaomi/analytics/internal/a/a;)V

    :cond_0
    return-void
.end method
